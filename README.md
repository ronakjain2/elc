<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https:://www.elctoys.com/logo192.png" alt="Project logo"></a>
</p>

<h3 align="center">ELC Toys</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center"> A React project
    <br> 
</p>

## 📝 Table of Contents

-   [About](#about)
-   [Getting Started](#getting_started)
-   [Branch Structure](#branches)
-   [Built Using](#built_using)

## 🧐 About <a name = "about"></a>

ELC Toys website frontend code using React JS. Chek Live version of website <a href="https:://www.elctoys.com/">here</a>

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

-   Node JS

### Installing

A step by step series of examples that tell you how to get a development env running.

Git clone the project in your Local Machine & follow the steps

```
npm install
```

And after successful installation run command

```
npm start
```

## 🔱 Branch Structure <a name = "branches"></a>

-   LIVE Branch - [Env/LIVE](https://bitbucket.org/mindnerves_technologies/elc_orob/branches/)
-   Development and QA Branch - [Env/QA](https://bitbucket.org/mindnerves_technologies/elc_orob/branches/)
-   Maintenance Page - [feat/MaintenancePage](https://bitbucket.org/mindnerves_technologies/elc_orob/branches/)

## ⛏️ Built Using <a name = "built_using"></a>

-   [React JS](https://reactjs.org/) - Web Framework
-   [Material UI](https://material-ui.com/) - UI Framework
-   [Redux SAGA](https://redux-saga.js.org/) - Redux state management Framework
