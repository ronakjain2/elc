export const isHomePage = () => {
    const pathname = (window && window.location && window.location.pathname) || '';
    const pattern = '^((/|/home/?)|((/|/home/)(saudi-en|saudi-ar|uae-en|uae-ar)/?))$';
    if (pathname.match(pattern)) {
        return true;
    }

    return false;
};
