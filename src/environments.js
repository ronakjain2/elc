const host = window.location.host;

let active_server = 'live';

if (host.includes('elcmktdev')) {
    active_server = 'dev';
} else if (host.includes('elcmktqa')) {
    active_server = 'qa';
} else if (host.includes('elcorobdev')) {
    active_server = 'orob_dev';
} else if (host.includes('elcorobqa')) {
    active_server = 'orob_qa';
} else if (host.includes('elcfcdev')) {
    active_server = 'fc_dev';
} else if (host.includes('elcfcqa')) {
    active_server = 'fc_qa';
} else if (host.includes('react-preprod.elctoys')) {
    active_server = 'prepod';
} else if (host.includes('elctoys')) {
    active_server = 'live';
}

const ALL_ENV = {
    dev: {
        BACKEND_URL: 'https://elcmktdevm2.iksulalive.com',
        PAYFORT_URL: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
        CRYPTR_KEY: 'mihyarOrderId',
        GTM_ID: 'GTM-5HWCVSJ',
    },
    qa: {
        BACKEND_URL: ' https://elcmktqam2.iksulalive.com',
        PAYFORT_URL: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
        CRYPTR_KEY: 'mihyarOrderId',
        GTM_ID: 'GTM-5HWCVSJ',
    },
    fc_dev: {
        BACKEND_URL: 'https://elcfcdevm2.iksulalive.com',
        PAYFORT_URL: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
        CRYPTR_KEY: 'mihyarOrderId',
        GTM_ID: 'GTM-5HWCVSJ',
    },
    fc_qa: {
        BACKEND_URL: ' https://elcfcqam2.iksulalive.com',
        PAYFORT_URL: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
        CRYPTR_KEY: 'mihyarOrderId',
        GTM_ID: 'GTM-5HWCVSJ',
    },
    orob_dev: {
        BACKEND_URL: 'https://elcorobdevm2.iksulalive.com',
        PAYFORT_URL: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
        CRYPTR_KEY: 'mihyarOrderId',
        GTM_ID: 'GTM-5HWCVSJ',
    },
    orob_qa: {
        BACKEND_URL: ' https://elcorobqam2.iksulalive.com',
        PAYFORT_URL: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
        CRYPTR_KEY: 'mihyarOrderId',
        GTM_ID: 'GTM-5HWCVSJ',
    },
    prepod: {
        BACKEND_URL: 'https://cms-preprod.elctoys.com',
        PAYFORT_URL: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
        CRYPTR_KEY: 'mihyarOrderId',
        GTM_ID: 'GTM-5HWCVSJ',
    },
    live: {
        BACKEND_URL: 'https://cms.elctoys.com',
        PAYFORT_URL: 'https://checkout.payfort.com/FortAPI/paymentPage',
        CRYPTR_KEY: 'mihyarOrderId',
        GTM_ID: 'GTM-NC6Z64G',
    },
};

const ACTIVE_ENV = ALL_ENV[active_server];
export const { BACKEND_URL, PAYFORT_URL, CRYPTR_KEY, GTM_ID } = ACTIVE_ENV;
