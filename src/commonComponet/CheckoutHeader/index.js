import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import './checkoutHeader.css';

function CheckoutHeader({ history }) {
    const state = useSelector(state => state);

    const store_locale = state && state.auth && state.auth.store_locale;
    const language = state && state.auth && state.auth.language;

    const gotoCart = () => history.push(`/${store_locale}/cart`);

    return (
        <Grid container justify="center" className="checkout-header" alignItems="center">
            <Grid item xs={12}>
                <Grid container justify="space-between" alignItems="center">
                    <Grid item xs={language === 'en' ? 2 : 2} md={language === 'en' ? 4 : 2} className="arabic-right">
                        <Button
                            className="checkout-back-button"
                            startIcon={language === 'en' ? <ChevronLeft /> : <ChevronRight />}
                            onClick={() => gotoCart()}
                        >
                            <FormattedMessage id="Checkout.Back" defaultMessage="Back" />
                        </Button>
                    </Grid>
                    <Grid item xs={5} md={4}>
                        <Link to="/">
                            <img
                                width="172"
                                height="30"
                                src="/images/logo/Logo_Group.png"
                                alt="logo"
                                className="mobileSize"
                            />
                        </Link>
                    </Grid>
                    <Grid
                        item
                        xs={language === 'en' ? 4 : 3}
                        md={language === 'en' ? 2 : 4}
                        className="flex justifyFlexEnd"
                    >
                        <Button
                            className="checkout-back-button"
                            disabled
                            startIcon={<img src="/images/checkout/Icon_Checkout_Secure.svg" alt="securePayment" />}
                        >
                            <FormattedMessage id="Checkout.100SecurePayment" defaultMessage="100% Secure Payments" />
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default withRouter(CheckoutHeader);
