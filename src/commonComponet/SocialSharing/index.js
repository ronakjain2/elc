import React from 'react';
import Grid from '@material-ui/core/Grid';

export default function SocialSharing() {
    return (
        <Grid container>
            <Grid item xs={12}>
                <a href="https://www.facebook.com/elctoys" target="_blank" rel="noopener noreferrer">
                    <img
                        height="32"
                        width="32"
                        src="/images/footer/Facebook.svg"
                        alt="facebook"
                        className="footer-icon"
                    />
                </a>
                <a href="https://www.instagram.com/elctoys" target="_blank" rel="noopener noreferrer">
                    <img
                        height="32"
                        width="32"
                        src="/images/footer/instagram.svg"
                        alt="instagram"
                        className="footer-icon"
                    />
                </a>
                <a href="https://www.twitter.com/elctoysme" target="_blank" rel="noopener noreferrer">
                    <img
                        height="32"
                        width="32"
                        src="/images/footer/twitter.svg"
                        alt="twitter"
                        className="footer-icon"
                    />
                </a>
                <a href="https://www.youtube.com/elctoysme" target="_blank" rel="noopener noreferrer">
                    <img
                        height="32"
                        width="32"
                        src="/images/footer/youtube.svg"
                        alt="youtube"
                        className="footer-icon"
                    />
                </a>
                <a href="https://wa.me/971543055373" target="_blank" rel="noopener noreferrer">
                    <img
                        height="32"
                        width="32"
                        src="/images/footer/Icon_Social_whatsapp.svg"
                        alt="whatsapp"
                        className="footer-icon"
                    />
                </a>
            </Grid>
        </Grid>
    );
}
