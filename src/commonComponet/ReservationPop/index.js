import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ButtonWithLoader from 'commonComponet/ButtonWithLoader';
import { actions as AuthActions } from 'components/Global/redux/actions';
import React from 'react';
import Countdown from 'react-countdown';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import './reservation.css';

function ReservationPop({ isOpen, setOpen }) {
    const dispath = useDispatch();
    const state = useSelector(state => state);

    const store_id = state && state.auth && state.auth.currentStore;
    const quote_id = state && state.auth && state.auth.quote_id;
    const reservationStartDate = state && state.auth && state.auth.reservationStartTime;
    const reservationConfig = state && state.auth && state.auth.reservationConfig;
    const extendReservationLoader = state && state.auth && state.auth.extendReservationLoader;

    let reservationStartTime = new Date(reservationStartDate);

    reservationStartTime.setMinutes(
        reservationStartTime.getMinutes() + parseInt(reservationConfig.reservation_checkout_time || 10),
    );
    reservationStartTime = new Date(reservationStartTime);

    const handleCloseOutSide = () => {
        setOpen(true);
    };

    const onClickCheckout = () => {
        dispath(
            AuthActions.extendReservationTimeRequest({
                store_id: store_id,
                quote_id: quote_id,
                reservation_type: 'checkout',
            }),
        );
    };

    /*const onClickContinueShopping = () => {
        dispath(AuthActions.removeReservationRequest({
            store_id: store_id,
            quote_id: quote_id,
            reservation_type: "checkout"
        }))
    }*/

    const closePOP = () => {
        dispath(
            AuthActions.removeReservationRequest({
                store_id: store_id,
                quote_id: quote_id,
                reservation_type: 'checkout',
            }),
        );
    };
    return (
        <>
            {isOpen ? (
                <Dialog
                    open={isOpen}
                    onClose={handleCloseOutSide}
                    className="country-pop-container"
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogContent>
                        <Grid container className="reservation-pop" alignItems="center">
                            <Countdown date={reservationStartTime} onComplete={() => closePOP()} />
                            <Grid item xs={12}>
                                <Typography className="pop-title">
                                    <FormattedMessage id="Reservation.Pop.Title" defaultMessage="Are you still here?" />
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container justify="space-around" alignItems="center">
                                    <div>
                                        <ButtonWithLoader
                                            onClick={onClickCheckout}
                                            className="checkout-button"
                                            loader={extendReservationLoader}
                                        >
                                            <FormattedMessage
                                                id="Reservation.POP.Checkout.Button"
                                                defaultMessage="I want to proceed with shopping"
                                            />
                                        </ButtonWithLoader>
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    </DialogContent>
                </Dialog>
            ) : null}
        </>
    );
}

export default withRouter(ReservationPop);
