import Skeleton from '@material-ui/lab/Skeleton';
import Image from 'material-ui-image';
import React, { useState } from 'react';
import LazyLoad from 'react-lazyload';

const IMAGE_STYLE = { height: 'auto', background: 'rgb(240 240 240)', borderRadius: 'inherit' };
const CONTAINER_STYLE = { background: 'rgb(240 240 240)', borderRadius: 'inherit' };

const Img = ({
    src,
    alt,
    width,
    height,
    className = '',
    style = {},
    imageStyle = {},
    imageClassName = '',
    containerStyle = {},
    lazy = true,
}) => {
    const [styleContainer, setStyleContainer] = useState({ paddingTop: `${height}px` });

    const onLoad = e => {
        const _height = e && e.target && e.target.offsetHeight;
        if (!_height) return;
        setStyleContainer({ paddingTop: `${_height}px` });
    };

    if (!lazy) {
        return (
            <>
                <div className={className}>
                    <Image
                        src={src}
                        alt={alt}
                        width={width}
                        height={height}
                        style={{ ...CONTAINER_STYLE, ...containerStyle, ...styleContainer }}
                        imageStyle={{ ...IMAGE_STYLE, ...imageStyle }}
                        className={imageClassName}
                        onLoad={onLoad}
                        loading={<Skeleton onLoad={onLoad} variant="rect" />}
                    />
                </div>
            </>
        );
    }

    return (
        <>
            <LazyLoad height={height + 'px'} className={className} style={style} offset={200} once={true}>
                <Image
                    src={src}
                    alt={alt}
                    width={width}
                    height={height}
                    style={{ ...CONTAINER_STYLE, ...containerStyle, ...styleContainer }}
                    imageStyle={{ ...IMAGE_STYLE, ...imageStyle }}
                    className={imageClassName}
                    onLoad={onLoad}
                    loading={<Skeleton onLoad={onLoad} variant="rect" />}
                />
            </LazyLoad>
        </>
    );
};

export default Img;
