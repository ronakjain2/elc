import React, { useState, useCallback, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { injectIntl } from 'react-intl';

function PageTitles({ intl }) {
    const pathname =
        (window &&
            window.location &&
            window.location.pathname &&
            window.location.pathname.split('/')[window.location.pathname.split('/').length - 1]) ||
        'NA';
    const parentname =
        (window &&
            window.location &&
            window.location.pathname &&
            window.location.pathname.split('/') &&
            window.location.pathname.split('/').length >= 2 &&
            window.location.pathname.split('/')[window.location.pathname.split('/').length - 2]) ||
        'NA';

    const [title, setTitle] = useState('');
    const [keyword, setKeyword] = useState('');
    const [description, setDescription] = useState('');

    const state = useSelector((state) => state);

    const country = state && state.auth && state.auth.country;

    const getPageTitle = useCallback(
        (title) => {
            const mainTitle =
                title !== 'NA'
                    ? intl.formatMessage({ id: `PageTitle.${title}`, defaultMessage: 'ELC' })
                    : intl.formatMessage({
                          id: 'PageTitle.Home',
                          defaultMessage: 'elctoys.com | ELC Online store | Official Website | Early Learning Center',
                      });

            setTitle(
                `${mainTitle} | ${intl.formatMessage({
                    id: `PageTitle.elc.${country}`,
                    defaultMessage: 'elctoys.com | ELC Online store | Official Website | Early Learning Center',
                })}`,
            );
            return `${mainTitle} | ${intl.formatMessage({
                id: `PageTitle.elc.${country}`,
                defaultMessage: 'elctoys.com | ELC Online store | Official Website | Early Learning Center',
            })}`;
        },
        [country, intl],
    );

    const getKeywords = useCallback(
        (keyword) => {
            const mainKeyword =
                keyword !== 'NA'
                    ? intl.formatMessage({ id: `PageTitle.${keyword}.keyword`, defaultMessage: 'ELC' })
                    : intl.formatMessage({
                          id: 'PageTitle.Home.keyword',
                          defaultMessage:
                              'ELC, Early Learning Center, Early Learning Centre, Toys, Baby Toys, Wooden Toys, Educational Toys',
                      });

            setKeyword(mainKeyword);
            return mainKeyword;
        },
        [intl],
    );

    const getDescriptions = useCallback(
        (description) => {
            const mainDescription =
                description !== 'NA'
                    ? intl.formatMessage({ id: `PageTitle.${description}.description`, defaultMessage: 'ELC' })
                    : intl.formatMessage({
                          id: 'PageTitle.Home.description',
                          defaultMessage:
                              'Shop online for baby toys, dolls houses, wooden toys and more at ELC. Choose from big brands including LeapFrog, VTech, Smart Trike and more.',
                      });

            setDescription(mainDescription);
            return mainDescription;
        },
        [intl],
    );

    useEffect(() => {
        try {
            if (pathname === '') {
                getPageTitle('NA');
                getKeywords('NA');
                getDescriptions('NA');
            } else if (parentname !== 'products-details') {
                getPageTitle(pathname);
                getKeywords(pathname);
                getDescriptions(pathname);
            } else {
                getPageTitle('NA');
                getKeywords('NA');
                getDescriptions('NA');
            }
        } catch (err) {}
    }, [pathname, parentname, country, getPageTitle, getKeywords, getDescriptions]);

    return (
        <Helmet>
            <title>{title}</title>
            <meta name="keywords" content={keyword} />
            <meta name="description" content={description} />
        </Helmet>
    );
}

export default injectIntl(PageTitles);
