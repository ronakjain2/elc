import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { actions as familyClubActions } from 'components/FamilyClub/redux/actions';
import React, { useEffect } from 'react';
import { defineMessages, FormattedMessage, injectIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import './FCPopup.css';

const messages = defineMessages({
    signInMessage: {
        id: 'SignInSignUp.FcInfoText',
        defaultMessage:
            'Hello, join our Family Club to avail exciting promotions and exclusive offers just by paying AED 99',
    },
    signUpMessage: {
        id: 'SignUpSignIn.FcInfoText',
        defaultMessage:
            'Hello, Thanks for joining ELC. For more exciting promotions and exclusive offers please join our Family Club',
    },
    signUpXstoreFc: {
        id: 'signUpXstoreFc',
        defaultMessage:
            'Congratulations, You are now a member of our Family Club. Enjoy exciting offers and benefits exclusively for you',
    },
    signUpWithFc: {
        id: 'signUpWithFc',
        defaultMessage: 'Family Club membership has been added to the cart',
    },
    fcPrivateCatMessage: {
        id: 'fcCategoryOnlyMess',
        defaultMessage: 'These products are exclusively available for the Family Club Members',
    },
});

function FCPopup({ history, intl }) {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const formatMessage = intl.formatMessage;

    const store_locale = state && state.auth && state.auth.store_locale;
    const quote_id = state && state.auth && state.auth.quote_id;
    const store_id = state && state.auth && state.auth.currentStore;
    const loginUser = state && state.auth && state.auth.loginUser;
    const customer_type = loginUser && loginUser.customer_type;
    const fcSignInPopup = state && state.signInSignUp && state.signInSignUp.fcSignInPopup;
    const fcSignInWithXStorePopup = state && state.signInSignUp && state.signInSignUp.fcSignInWithXStorePopup;
    const fcSignUpPopup = state && state.signInSignUp && state.signInSignUp.fcSignUpPopup;
    const fcSignUpWithFCPopup = state && state.signInSignUp && state.signInSignUp.fcSignUpWithFCPopup;
    const fcPrivateCat = state && state.signInSignUp && state.signInSignUp.fcPrivateCat;

    const [open, setAfterOpen] = React.useState(false);
    const showJoinButton = React.useRef(true);
    const popupMsg = React.useRef('');

    useEffect(() => {
        if (customer_type === 'ecom_club_user') {
            showJoinButton.current = false;
        } else {
            showJoinButton.current = true;
        }
    }, [customer_type]);

    useEffect(() => {
        if (!fcSignInPopup) return;
        popupMsg.current = formatMessage(messages.signInMessage);
        setAfterOpen(true);
    }, [fcSignInPopup, formatMessage]);

    useEffect(() => {
        if (!fcSignInWithXStorePopup) return;
        popupMsg.current = formatMessage(messages.signUpXstoreFc);
        setAfterOpen(true);
    }, [fcSignInPopup, fcSignInWithXStorePopup, formatMessage]);

    useEffect(() => {
        if (!fcSignUpPopup) return;

        if (customer_type === 'ecom_club_user') {
            popupMsg.current = formatMessage(messages.signUpXstoreFc);
        } else {
            popupMsg.current = formatMessage(messages.signUpMessage);
        }
        setAfterOpen(true);
    }, [customer_type, fcSignUpPopup, formatMessage]);

    useEffect(() => {
        if (!fcSignUpWithFCPopup) return;

        if (customer_type === 'ecom_club_user') {
            popupMsg.current = formatMessage(messages.signUpXstoreFc);
        } else {
            showJoinButton.current = false;
            popupMsg.current = formatMessage(messages.signUpWithFc);
        }
        setAfterOpen(true);
    }, [customer_type, fcSignUpWithFCPopup, formatMessage]);

    useEffect(() => {
        if (!fcPrivateCat) return;
        popupMsg.current = formatMessage(messages.fcPrivateCatMessage);
        setAfterOpen(true);
    }, [fcPrivateCat, formatMessage]);

    const onKnowMoreClick = () => {
        closePopup();
        if (customer_type === 'ecom_club_user') {
            return history.push(`/${store_locale}/my-club`);
        } else {
            return history.push(`/${store_locale}/family-club`);
        }
    };

    const onClickJoin = () => {
        closePopup();

        if (!loginUser) {
            return history.push(`/${store_locale}/sign-in-register`);
        }
        if (loginUser) {
            const payload = {
                quote_id,
                store_id,
                customer_id: loginUser.customer_id,
            };
            dispatch(familyClubActions.setFamilyClubRequest(payload));
        }
    };

    const closePopup = () => {
        setAfterOpen(false);
        showJoinButton.current = true;
        popupMsg.current = '';
    };

    return (
        <Dialog
            open={open}
            onClose={closePopup}
            className={'familyClubDialog'}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <div className="d-flex justify-content-end">
                <IconButton onClick={closePopup} aria-label="delete">
                    <CloseIcon />
                </IconButton>
            </div>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">{popupMsg.current}</DialogContentText>
                <div className="fcPopup-buttons-container">
                    <Button onClick={onKnowMoreClick} variant="contained" color="primary">
                        <FormattedMessage id="fcPopup.KnowMore" defaultMessage="Know More" />
                    </Button>
                    {showJoinButton.current && (
                        <Button onClick={onClickJoin} variant="contained" color="primary">
                            <FormattedMessage id="fcPopup.JoinNow" defaultMessage="Join now" />
                        </Button>
                    )}
                </div>
            </DialogContent>
        </Dialog>
    );
}

export default withRouter(injectIntl(FCPopup));
