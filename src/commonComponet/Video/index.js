import React, { useEffect, useState } from 'react';

const nullFunction = () => null;
function Video({
    src,
    onClick = nullFunction,
    className = '',
    autoPlay = false,
    playsInline = true,
    loop = true,
    muted = true,
    controls = false,
    height = null,
    width = null,
}) {
    const [videoRef, setvideoRef] = useState(null);
    useEffect(() => {
        if (!videoRef) return;
        let observer = new IntersectionObserver((entries, observer) => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    entry.target.play();
                } else {
                    entry.target.pause();
                }
            });
        }, {});
        observer.observe(videoRef);
    }, [videoRef]);

    return (
        <>
            <video
                ref={ref => setvideoRef(ref)}
                onClick={onClick}
                className={className}
                src={src}
                autoPlay={autoPlay}
                playsInline={playsInline}
                loop={loop}
                muted={muted}
                controls={controls}
                height={height}
                width={width}
            />
        </>
    );
}

export default Video;
