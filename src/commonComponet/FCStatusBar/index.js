import Grid from '@material-ui/core/Grid';
import { actions as GlobalActions } from 'components/Global/redux/actions';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './style.css';


const FCStatusBar = ({ show, history }) => {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const store_locale = state && state.auth && state.auth.store_locale;
    const loginUser = state && state.auth && state.auth.loginUser && state.auth.loginUser;
    const fc_status = state && state.auth && state.auth.fc_status;
    const firstname = loginUser && loginUser.firstname;
    const customer_type = loginUser && loginUser.customer_type;
    const fc_status_message = loginUser && loginUser.fc_status_message;

    useEffect(() => {
        dispatch(GlobalActions.getFCConfigReq());
    }, [dispatch]);

    show = fc_status && customer_type === 'ecom_club_user';
    if (!show) return null;

    const gotoClub = () => history.push(`/${store_locale}/my-club`);

    return (
        <Grid onClick={gotoClub} className="fc-details-bar" container alignItems="center" justify="space-between">
            <Grid>
                Hi,&nbsp;{firstname}&nbsp;
                <img
                    height="16"
                    width="16"
                    src="/images/header/top/family.svg"
                    alt={fc_status_message || 'Family Club'}
                />
            </Grid>
            <Grid>
                <img height="16" width="16" src="/images/header/qrcode.svg" alt={fc_status_message || 'Family Club'} />
                &nbsp;{fc_status_message}
            </Grid>
        </Grid>
    );
};

export default withRouter(FCStatusBar);
