import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import React, { useEffect, useState } from 'react';
import IntlTelInput from 'react-intl-tel-input';
import 'react-intl-tel-input/dist/main.css';
import { useSelector } from 'react-redux';
import countries from './country_code';
import './styles.css';

const PHONE_NUMBER_DATA = {
    UAE: {
        flagImage: '/images/header/top/Icon_Flag_UAE_v1.svg',
        regionCodes: [50, 52, 54, 55, 56, 58],
        carrierCode: '+971',
        countryData: {
            country_name: 'United Arab Emirates (‫الإمارات العربية المتحدة‬‎)',
            country_id: 'ae',
            dialCode: '971',
        },
    },
    KSA: {
        flagImage: '/images/header/top/Icon_Flag_KSA_v1.svg',
        regionCodes: [50, 51, 53, 54, 55, 56, 57, 58, 59],
        carrierCode: '+966',
        countryData: {
            country_name: 'Saudi Arabia (‫المملكة العربية السعودية‬‎)',
            country_id: 'sa',
            dialCode: '966',
        },
    },
};

export default function PhoneNumber({
    parentPhoneNumberChangeEvent,
    carrier_code,
    phoneNo,
    forInternational = false,
    disabled = false,
}) {
    const state = useSelector(state => state);

    const userSelectedCountry = countries.filter(c => c.dialCode === carrier_code)[0];

    const country = state && state.auth && state.auth.country;

    const [regionId, setregionId] = useState(50);
    const [phoneNumber, setphoneNumber] = useState('');
    const [compDisabled, setCompDisabled] = useState(false);

    const [phoneContain, setPhoneContain] = useState({
        phone: '',
        dialCode: '',
        phoneValid: '',
    });

    const setInitialPhoneNumber = () => {
        setPhoneContain({
            phone: phoneNo || '',
            dialCode: carrier_code || '',
            phoneValid: true,
        });

        onPhoneNumberBlur({
            status: true,
            value: phoneNo,
            countryData: PHONE_NUMBER_DATA[country].countryData || {},
        });

        setCompDisabled(disabled);
    };

    const Initiate = () => {
        let _regionId = phoneNo && phoneNo.slice(0, 2);
        let _phoneNumber = phoneNo && phoneNo.slice(2);

        if (
            country === 'KSA' &&
            carrier_code === '966' &&
            PHONE_NUMBER_DATA.KSA.regionCodes.includes(parseInt(_regionId)) &&
            phoneNo &&
            phoneNo.length === 9
        ) {
            setregionId(_regionId);
            setphoneNumber(phoneNo.slice(2));
            setInitialPhoneNumber(_regionId, _phoneNumber, '966');
        } else if (
            country === 'UAE' &&
            carrier_code === '971' &&
            PHONE_NUMBER_DATA.UAE.regionCodes.includes(parseInt(_regionId)) &&
            phoneNo &&
            phoneNo.length === 9
        ) {
            setregionId(_regionId);
            setphoneNumber(phoneNo.slice(2));
            setInitialPhoneNumber(_regionId, _phoneNumber, '971');
        } else if (forInternational || (country !== 'KSA' && country !== 'UAE')) {
            setPhoneContain({
                phone: (phoneNo && `${phoneNo}`.replace(/ /g, '')) || '',
                dialCode: carrier_code || '',
                phoneValid: true,
            });
            setCompDisabled(disabled);
        } else {
            onPhoneNumberBlur({
                status: false,
                value: '',
                countryData: null,
            });
        }
    };

    useEffect(Initiate, []);

    const onSelectFlag = (num, country) => {
        setPhoneContain({
            phone: '',
            dialCode: country.dialCode || '',
        });
    };

    const onPhoneNumberChange = (value, status, countryData) => {
        try {
            setPhoneContain({
                phone: value.replace(/ /g, ''),
                dialCode: countryData && countryData.dialCode,
                phoneValid: status,
            });
            if (status) {
                return parentPhoneNumberChangeEvent({
                    phone: `${(value && parseInt(value.replace(/ /g, ''))) || ''}`,
                    dialCode: countryData && countryData.dialCode,
                    phoneValid: status,
                });
            }
        } catch (err) {
            console.log(err);
        }
    };

    const onPhoneNumberBlur = ({ status, value, countryData }) => {
        try {
            if (!value.match(/^[0-9]*$/)) {
                status = false;
            }
            setPhoneContain({
                phone: `${(value && parseInt(value)) || ''}`,
                dialCode: countryData && countryData.dialCode,
                phoneValid: status,
            });
            return parentPhoneNumberChangeEvent({
                phone: `${(value && parseInt(value)) || ''}`,
                dialCode: countryData && countryData.dialCode,
                phoneValid: status,
            });
        } catch (err) {
            console.log('errr', err);
        }
    };

    const sendDefaultCountryCode = () => {
        if (userSelectedCountry) {
            return userSelectedCountry.country_id;
        } else {
            if (!carrier_code || carrier_code === '') {
                if (country === 'UAE') {
                    return 'ae';
                } else {
                    return 'sa';
                }
            }
        }
    };

    const internationalPhoneComp = () => {
        return (
            <Grid item xs={12}>
                {phoneContain.dialCode && phoneContain.dialCode !== '' && (
                    <IntlTelInput
                        fieldId="Phone-number-plugin"
                        containerClassName="intl-tel-input phoneNumberInputContainer"
                        inputClassName="form-control phoneNumberInputBox"
                        separateDialCode
                        preferredCountries={[sendDefaultCountryCode()]}
                        defaultCountry={sendDefaultCountryCode()}
                        onPhoneNumberChange={(status, value, countryData, number, id) =>
                            onPhoneNumberChange(value, status, countryData)
                        }
                        onSelectFlag={(num, country) => onSelectFlag(num, country)}
                        onPhoneNumberBlur={(status, value, countryData, number, id) =>
                            onPhoneNumberBlur({ status, value, countryData })
                        }
                        value={phoneContain.phone}
                        disabled={compDisabled}
                    />
                )}
                {(!phoneContain.dialCode || phoneContain.dialCode === '') && (
                    <IntlTelInput
                        fieldId="Phone-number-plugin"
                        containerClassName="intl-tel-input phoneNumberInputContainer"
                        inputClassName="form-control phoneNumberInputBox"
                        separateDialCode
                        preferredCountries={[sendDefaultCountryCode()]}
                        defaultCountry={sendDefaultCountryCode()}
                        onPhoneNumberChange={(status, value, countryData, number, id) => onPhoneNumberChange(value)}
                        onSelectFlag={(num, country) => onSelectFlag(num, country)}
                        onPhoneNumberBlur={(status, value, countryData, number, id) =>
                            onPhoneNumberBlur({ status, value, countryData })
                        }
                        value={phoneContain.phone}
                        disabled={compDisabled}
                    />
                )}
            </Grid>
        );
    };

    const onChangeHelper = (value, type) => {
        let phoneObj = {
            status: false,
            countryData: PHONE_NUMBER_DATA[country].countryData,
        };

        if (type === 'regionId') {
            if (value) {
                setregionId(value);
                phoneObj.value = `${value}${phoneNumber}`.replace(/ /g, '');
                if (phoneNumber && phoneNumber.length === 7) {
                    phoneObj.status = true;
                }
                onPhoneNumberBlur(phoneObj);
            }
        } else if (type === 'phoneNumber') {
            if (!value.match(/^[0-9]*$/)) {
                return;
            }
            setphoneNumber(value);
            phoneObj.value = `${regionId}${value}`.replace(/ /g, '');
            if (value && value.length === 7) {
                phoneObj.status = true;
            }
            onPhoneNumberBlur(phoneObj);
        }
    };

    const onBlurHelper = () => {
        if (regionId && phoneNumber && phoneNumber.length === 7) {
            onPhoneNumberBlur({
                status: true,
                value: `${regionId}${phoneNumber}`.replace(/ /g, ''),
                countryData: PHONE_NUMBER_DATA[country].countryData,
            });
        } else {
            onPhoneNumberBlur({
                status: false,
                value: `${regionId}${phoneNumber}`.replace(/ /g, ''),
                countryData: PHONE_NUMBER_DATA[country].countryData,
            });
        }
    };

    const customPhoneComp = () => {
        return (
            <Grid item xs={12} className="d-flex custom-phone dir-ltr">
                <TextField
                    variant="outlined"
                    value={PHONE_NUMBER_DATA[country].carrierCode}
                    className="inputBox carrierCode"
                    disabled={true}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <div
                                    className="country-image-div-inp"
                                    style={{
                                        background: `url(${PHONE_NUMBER_DATA[country].flagImage})`,
                                        backgroundRepeat: 'no-repeat',
                                    }}
                                />
                            </InputAdornment>
                        ),
                    }}
                />
                <Box className="phone-box">
                    <TextField
                        select
                        value={regionId}
                        onChange={e => onChangeHelper(e.target.value, 'regionId')}
                        onBlur={onBlurHelper}
                        variant="outlined"
                        className="regionId"
                        disabled={compDisabled}
                    >
                        {country &&
                            PHONE_NUMBER_DATA[country].regionCodes.map(option => (
                                <MenuItem key={option} value={option}>
                                    0{option}
                                </MenuItem>
                            ))}
                    </TextField>
                    <TextField
                        disabled={compDisabled}
                        variant="outlined"
                        value={phoneNumber}
                        onChange={e => onChangeHelper(e.target.value, 'phoneNumber')}
                        onBlur={onBlurHelper}
                        fullWidth
                        className="inputBox phoneNumber"
                        inputProps={{ maxLength: 7 }}
                    />
                </Box>
            </Grid>
        );
    };

    if (forInternational) {
        return <Grid container>{internationalPhoneComp()}</Grid>;
    }

    return (
        <Grid container>{country === 'KSA' || country === 'UAE' ? customPhoneComp() : internationalPhoneComp()}</Grid>
    );
}
