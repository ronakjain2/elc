import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import React from 'react';
import './style.css';

const ButtonWithLoader = ({
    children = '',
    onClick = null,
    type = 'button',
    disabled = false,
    loader = false,
    className = '',
    loaderClassName = '',
}) => {
    return (
        <>
            <Button
                type={type}
                disabled={disabled || loader}
                className={`button-v2 ${className}`}
                onClick={e => onClick(e)}
                data-loader={Boolean(loader)}
            >
                {loader ? <CircularProgress className={loaderClassName} /> : <>{children}</>}
            </Button>
        </>
    );
};

export default ButtonWithLoader;
