import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.SUBMIT_SUBSCRIBE_EMAIL_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.SUBMIT_SUBSCRIBE_EMAIL_REQUEST_SUCCESS]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.SUBMIT_SUBSCRIBE_EMAIL_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
});
