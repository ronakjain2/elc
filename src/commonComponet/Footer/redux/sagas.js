import { call, put, takeLatest } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import { actions, types } from './actions';
import api from '../api';

const subScribeEmailReq = function* subScribeEmailReq({ payload }) {
    try {
        const { data } = yield call(api.subScribeEmail, payload);
        if (data && data.status) {
            toast.success(data && data.message);
        } else {
            toast.error((data && data.message) || 'Something went to wrong');
        }
        yield put(actions.submitSubscribeEmailRequestSuccess());
    } catch (err) {
        toast.error('Something went to wrong');
        yield put(actions.submitSubscribeEmailRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.SUBMIT_SUBSCRIBE_EMAIL_REQUEST, subScribeEmailReq);
}
