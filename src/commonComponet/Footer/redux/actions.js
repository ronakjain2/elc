import { createAction } from 'redux-actions';

// action types
const SUBMIT_SUBSCRIBE_EMAIL_REQUEST = 'ELC@OROB/SUBMIT_SUBSCRIBE_EMAIL_REQUEST';
const SUBMIT_SUBSCRIBE_EMAIL_REQUEST_SUCCESS = 'ELC@OROB/SUBMIT_SUBSCRIBE_EMAIL_REQUEST_SUCCESS';
const SUBMIT_SUBSCRIBE_EMAIL_REQUEST_FAILED = 'ELC@OROB/SUBMIT_SUBSCRIBE_EMAIL_REQUEST_FAILED';

// action method
const submitSubscribeEmailRequest = createAction(SUBMIT_SUBSCRIBE_EMAIL_REQUEST);
const submitSubscribeEmailRequestSuccess = createAction(SUBMIT_SUBSCRIBE_EMAIL_REQUEST_SUCCESS);
const submitSubscribeEmailRequestFailed = createAction(SUBMIT_SUBSCRIBE_EMAIL_REQUEST_FAILED);

// actions
export const actions = {
    submitSubscribeEmailRequest,
    submitSubscribeEmailRequestSuccess,
    submitSubscribeEmailRequestFailed,
};

// types
export const types = {
    SUBMIT_SUBSCRIBE_EMAIL_REQUEST,
    SUBMIT_SUBSCRIBE_EMAIL_REQUEST_SUCCESS,
    SUBMIT_SUBSCRIBE_EMAIL_REQUEST_FAILED,
};
