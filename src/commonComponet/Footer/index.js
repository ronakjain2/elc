import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import InputBase from '@material-ui/core/InputBase';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import { actions as footerActions } from 'commonComponet/Footer/redux/actions';
import { isEmail } from 'components/SignIn_SignUp/utils';
import React, { lazy, Suspense, useCallback, useEffect, useState } from 'react';
import Collapsible from 'react-collapsible';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import { Link, useLocation } from 'react-router-dom';
import './css/footer.css';

const SocialSharing = lazy(() => import('commonComponet/SocialSharing'));

const useStyles = makeStyles(theme => ({
    root: {
        padding: '0px 0px 0px 4px',
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        height: 45,
        border: '1px solid #0D943F',
        boxShadow: 'none',
    },
    rootM: {
        padding: '0px 0px 0px 4px',
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        height: 45,
        border: '1px solid #0D943F',
        boxShadow: 'none',
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },
    button: {
        height: 45,
        padding: '0px 25px',
        background: '#0D943F',
        borderRadius: 0,
        color: '#fff',
        textTransform: 'capitalize',
        '&:hover': {
            background: '#0D943F',
            color: '#fff',
        },
        '& .MuiButton-label span': {
            position: 'relative',
            top: 2,
        },
    },
}));

function Footer({ history, isCheckoutProcess }) {
    const classes = useStyles();

    const state = useSelector(state => state);
    const dispatch = useDispatch();
    const location = useLocation();

    const store_locale = state && state.auth && state.auth.store_locale;
    const language = state && state.auth && state.auth.language;
    const loginUser = state && state.auth && state.auth.loginUser;
    const country = state && state.auth && state.auth.country;

    const [subScribeEmail, setSubScribeEmail] = useState('');
    const [isValidate, setIsValidate] = useState(false);

    const gotoAboutUs = () => history.push(`/${store_locale}/about-us`);

    const gotoHelpAndFaq = () => history.push(`/${store_locale}/help-and-faq`);

    const gotoTermAndConditions = () => history.push(`/${store_locale}/terms-and-conditions`);

    const gotoPrivacyPolicy = () => history.push(`/${store_locale}/privacy-policy`);

    const gotoContactUs = () => history.push(`/${store_locale}/contact-us`);

    const gotoPlaystore = () =>
        window.open('https://play.google.com/store/apps/details?id=com.elctoys.app', '_blank').focus();

    const gotoAppstore = () => window.open('https://apps.apple.com/us/app/elc-toys/id1488769478', '_blank').focus();

    const insiderObject = useCallback(() => {
        let spUID = JSON.parse(localStorage.getItem('spUID'));

        if (loginUser && loginUser.customer_id) {
            window.insider_object = {
                user: {
                    user: loginUser.customer_id,
                    name: loginUser.firstname,
                    surname: loginUser.lastname,
                    email: loginUser.email,
                    email_optin: loginUser.email ? true : false,
                    phone_number:
                        loginUser.phone_number !== null && loginUser.carrier_code !== null
                            ? `${loginUser.carrier_code}${loginUser.phone_number}`
                            : '',
                    sms_optin: loginUser.phone_number ? true : false,
                    gdpr_optin: true,
                    language: language,
                },
                spUID: spUID,
            };
        } else {
            window.insider_object = {
                user: {
                    gdpr_optin: null,
                    language: language,
                },
                spUID: spUID,
            };
        }
    }, [language, loginUser]);

    useEffect(() => {
        const currentPath = location && location.pathname;
        const waitForZopim = setInterval(function() {
            if (window.$zopim === undefined || window.$zopim.livechat === undefined) {
                return;
            }

            window.$zopim(function() {
                window.$zopim.livechat.sendVisitorPath({
                    url: `${window.location.host}${currentPath}`,
                    title: 'elc',
                });
            });

            clearInterval(waitForZopim);
        }, 100);

        insiderObject();
    }, [location, insiderObject]);

    const submitEmail = () => {
        setIsValidate(true);
        if (!isEmail(subScribeEmail)) {
            return;
        }
        setIsValidate(false);
        dispatch(
            footerActions.submitSubscribeEmailRequest({
                email: subScribeEmail,
                store_id: state && state.auth && state.auth.currentStore,
            }),
        );
        setSubScribeEmail('');
    };
    const closeOther = value => {
        let collapses = document.getElementsByClassName('open');
        for (let i = 0; i < collapses.length; i++) {
            collapses[i].click();
        }
    };

    return (
        <Grid container justify="center" className="footer-t-m">
            <Hidden only={['sm', 'xs', 'md']}>
                <Suspense fallback="">
                    {!isCheckoutProcess() && (
                        <Grid item xs={11} className="footer-line">
                            <Grid container>
                                <Grid item xs={2} className="px-2 text-align-rtl">
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <Typography className="footer-title">
                                                <FormattedMessage id="Footer.AboutELC" defaultMessage="About ELC" />
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography
                                                className="footer-subTitle cursor-pointer"
                                                onClick={() => gotoAboutUs()}
                                            >
                                                <FormattedMessage id="Footer.About" defaultMessage="About" />
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography
                                                className="footer-subTitle cursor-pointer"
                                                onClick={() => gotoHelpAndFaq()}
                                            >
                                                <FormattedMessage id="Footer.FAQ" defaultMessage="FAQ" />
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography
                                                className="footer-subTitle cursor-pointer"
                                                onClick={() => gotoContactUs()}
                                            >
                                                <FormattedMessage id="Footer.ContactUs" defaultMessage="Contact Us" />
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={2} className="px-2 text-align-rtl">
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <Typography className="footer-title">
                                                <FormattedMessage id="Footer.Legal" defaultMessage="Legal" />
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography
                                                className="footer-subTitle cursor-pointer"
                                                onClick={() => gotoTermAndConditions()}
                                            >
                                                <FormattedMessage
                                                    id="Footer.TermsConditions"
                                                    defaultMessage="Terms & Conditions"
                                                />
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography
                                                className="footer-subTitle cursor-pointer"
                                                onClick={() => gotoPrivacyPolicy()}
                                            >
                                                <FormattedMessage
                                                    id="Footer.PrivacyPolicy"
                                                    defaultMessage="Privacy Policy"
                                                />
                                            </Typography>
                                        </Grid>
                                        {/* <Grid item xs={12}>
                                        <Typography className="footer-subTitle">
                                            <FormattedMessage
                                                id="Footer.ConsumerRights"
                                                defaultMessage="Consumer Rights"
                                            />
                                        </Typography>
                                    </Grid> */}
                                    </Grid>
                                </Grid>
                                <Grid item xs={2} className="px-2 text-align-rtl">
                                    <Grid item xs={12}>
                                        <Typography className="footer-title">
                                            <FormattedMessage
                                                id="Footer.downloadApp"
                                                defaultMessage="Download ELC App"
                                            />
                                        </Typography>

                                        <Grid item xs={12} mb={2}>
                                            <Grid
                                                container
                                                justify="center"
                                                alignItems="center"
                                                className="cursor-pointer"
                                                onClick={() => gotoPlaystore()}
                                            >
                                                <Grid item xs={2} className="appstore-icons">
                                                    <img
                                                        width="24"
                                                        height="24"
                                                        src="/images/footer/google-play-brands.svg"
                                                        alt="App store"
                                                    />
                                                </Grid>
                                                <Grid item xs={10}>
                                                    <Typography className="footer-subTitle">
                                                        <FormattedMessage
                                                            id="Footer.playstore"
                                                            defaultMessage="Playstore"
                                                        />
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>

                                        <Grid item xs={12} mb={2}>
                                            <Grid
                                                container
                                                justify="center"
                                                alignItems="center"
                                                className="cursor-pointer"
                                                onClick={() => gotoAppstore()}
                                            >
                                                <Grid item xs={2} className="appstore-icons">
                                                    <img
                                                        width="24"
                                                        height="24"
                                                        src="/images/footer/app-store-ios-brands.svg"
                                                        alt="App store"
                                                    />
                                                </Grid>
                                                <Grid item xs={10}>
                                                    <Typography className="footer-subTitle">
                                                        <FormattedMessage
                                                            id="Footer.appstore"
                                                            defaultMessage="Appstore"
                                                        />
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={3} className="px-2 text-align-rtl">
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <Typography className="footer-title">
                                                <FormattedMessage
                                                    id="Footer.FollowUsOn"
                                                    defaultMessage="Follow us on"
                                                />
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={12}>
                                            <Suspense fallback="">
                                                <SocialSharing />
                                            </Suspense>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={3} className="px-2 text-align-rtl">
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <Typography className="footer-title">
                                                <FormattedMessage
                                                    id="Footer.SignUpForOurLatestNews"
                                                    defaultMessage="Sign up for our latest news and offers"
                                                />
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Paper component="form" className={clsx(classes.root, 'emailBox')}>
                                                <InputBase
                                                    className={classes.input}
                                                    placeholder={
                                                        language === 'en'
                                                            ? 'Enter your email address'
                                                            : 'أدخل بريدك الإلكتروني'
                                                    }
                                                    inputProps={{ 'aria-label': 'search google maps' }}
                                                    value={subScribeEmail}
                                                    onChange={e => setSubScribeEmail(e.target.value)}
                                                    error={isValidate && !isEmail(subScribeEmail)}
                                                />

                                                {state && state.footer && !state.footer.loader && (
                                                    <Button
                                                        className={classes.button}
                                                        aria-label="directions"
                                                        onClick={() => submitEmail()}
                                                    >
                                                        <FormattedMessage id="Footer.Submit" defaultMessage="Submit" />
                                                    </Button>
                                                )}
                                                {state && state.footer && state.footer.loader && (
                                                    <Button className={classes.button} aria-label="directions">
                                                        <CircularProgress style={{ color: '#fff', fontSize: '1rem' }} />
                                                    </Button>
                                                )}
                                            </Paper>
                                            <Grid container>
                                                <Grid item xs={12}>
                                                    {isValidate && !isEmail(subScribeEmail) && (
                                                        <FormHelperText
                                                            style={{ textAlign: 'start' }}
                                                            className="input-error"
                                                        >
                                                            <FormattedMessage
                                                                id="Common.Email.Error"
                                                                defaultMessage="Please Enter Valid Email Address"
                                                            />
                                                        </FormHelperText>
                                                    )}
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    )}
                    <Grid
                        item
                        xs={11}
                        style={{ padding: '10px 0px' }}
                        className={isCheckoutProcess() ? 'footer-line-c' : ''}
                    >
                        {country === 'UAE' && (
                            <Grid container justify="space-between" alignItems="center">
                                <Grid item xs={4} className="text-align-start">
                                    <Typography style={{ fontSize: '0.6rem' }}>
                                        <FormattedMessage
                                            id="Footer.Bottom.Info"
                                            defaultMessage="© Website is operated by Kamal Osman Jamjoom LLC, trading as Early Learning Centre"
                                        />
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} style={{ textAlign: 'center' }}>
                                    <img
                                        width="96"
                                        height="101"
                                        src="/images/footer/consumer_right_v1.png"
                                        className="consumer-right"
                                        alt="footer"
                                    />
                                </Grid>
                                <Grid item xs={4} className="text-align-end">
                                    <img
                                        width="170"
                                        height="24"
                                        src={
                                            language === 'en'
                                                ? '/images/footer/logoEn_v1.png'
                                                : '/images/footer/logoAr_v1.png'
                                        }
                                        alt="footer"
                                    />
                                </Grid>
                            </Grid>
                        )}

                        {country !== 'UAE' && (
                            <Grid container justify="space-between" alignItems="center">
                                <div className="text-align-start">
                                    <Typography style={{ fontSize: '0.6rem' }}>
                                        <FormattedMessage
                                            id="Footer.Bottom.Info"
                                            defaultMessage="© Website is operated by Kamal Osman Jamjoom LLC, trading as Early Learning Centre"
                                        />
                                    </Typography>
                                </div>
                                <div className="text-align-end">
                                    <img
                                        width="170"
                                        height="24"
                                        src={
                                            language === 'en'
                                                ? '/images/footer/logoEn_v1.png'
                                                : '/images/footer/logoAr_v1.png'
                                        }
                                        alt="footer"
                                    />
                                </div>
                            </Grid>
                        )}
                    </Grid>
                </Suspense>
            </Hidden>

            <Hidden only={['lg', 'xl']}>
                <Suspense fallback="">
                    <Grid container justify="center">
                        {!isCheckoutProcess() && (
                            <Grid item xs={10}>
                                <Grid item xs={12}>
                                    <Typography style={{ textAlign: 'center' }} className="footer-title">
                                        <FormattedMessage
                                            id="Footer.SignUpForOurLatestNews"
                                            defaultMessage="Sign up for our latest news and offers"
                                        />
                                    </Typography>
                                </Grid>
                                <Paper
                                    style={{ width: '100%', marginBottom: 10 }}
                                    component="form"
                                    className={clsx(classes.root, 'emailBox')}
                                >
                                    <InputBase
                                        className={classes.input}
                                        style={{ width: '100%' }}
                                        placeholder={
                                            language === 'en' ? 'Enter your email address' : 'أدخل بريدك الإلكتروني'
                                        }
                                        inputProps={{ 'aria-label': 'search google maps' }}
                                        value={subScribeEmail}
                                        onChange={e => setSubScribeEmail(e.target.value)}
                                        error={isValidate && !isEmail(subScribeEmail)}
                                    />

                                    {state && state.footer && !state.footer.loader && (
                                        <Button
                                            className={classes.button}
                                            aria-label="directions"
                                            onClick={() => submitEmail()}
                                        >
                                            <FormattedMessage id="Footer.Submit" defaultMessage="Submit" />
                                        </Button>
                                    )}
                                    {state && state.footer && state.footer.loader && (
                                        <Button className={classes.button} aria-label="directions">
                                            <CircularProgress style={{ color: '#fff', fontSize: '1rem' }} />
                                        </Button>
                                    )}
                                </Paper>
                                <Grid container>
                                    <Grid item xs={10}>
                                        {isValidate && !isEmail(subScribeEmail) && (
                                            <FormHelperText
                                                style={{ textAlign: 'start', paddingBottom: 10 }}
                                                className="input-error"
                                            >
                                                <FormattedMessage
                                                    id="Common.Email.Error"
                                                    defaultMessage="Please Enter Valid Email Address"
                                                />
                                            </FormHelperText>
                                        )}
                                    </Grid>
                                </Grid>
                                <Grid className="footer-css" item xs={12}>
                                    <Collapsible
                                        trigger={
                                            <div onClick={() => closeOther(0)} className="Collapsible_text_container">
                                                <div className="Collapsible_text">
                                                    <FormattedMessage id="footer.AboutELC" defaultMessage="About ELC" />
                                                </div>
                                                <div className="Collapsible_arrow_container">
                                                    <img
                                                        width="15"
                                                        height="15"
                                                        src="/images/footer/PlusIcon.png"
                                                        alt=""
                                                    />
                                                </div>
                                            </div>
                                        }
                                        triggerWhenOpen={
                                            <div className="Collapsible_text_container open">
                                                <div className="Collapsible_text">
                                                    <FormattedMessage id="footer.AboutELC" defaultMessage="About ELC" />
                                                </div>
                                                <div className="Collapsible_arrow_container">
                                                    <img
                                                        width="15"
                                                        height="24"
                                                        src="/images/footer/minusIcon.png"
                                                        alt=""
                                                    />
                                                </div>
                                            </div>
                                        }
                                    >
                                        <div style={{ textAlign: 'start' }}>
                                            <ul className="footer_ul_amrc">
                                                <li className="text-color">
                                                    <Link
                                                        to={`/${store_locale}/about-us`}
                                                        style={{ textDecoration: 'none' }}
                                                    >
                                                        <FormattedMessage
                                                            id="footer.aboutElc"
                                                            defaultMessage="About ELC Toys"
                                                        />
                                                    </Link>
                                                </li>
                                                <li className="text-color">
                                                    <Link
                                                        to={`/${store_locale}/help-and-faq`}
                                                        style={{ textDecoration: 'none' }}
                                                    >
                                                        <FormattedMessage id="footer.helpFaqs" defaultMessage="FAQ" />
                                                    </Link>
                                                </li>
                                                <li className="text-color">
                                                    <Link
                                                        to={`/${store_locale}/contact-us`}
                                                        style={{ textDecoration: 'none' }}
                                                    >
                                                        <FormattedMessage
                                                            id="footer.contactUs"
                                                            defaultMessage="Contact Us"
                                                        />
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </Collapsible>
                                    <Collapsible
                                        trigger={
                                            <div onClick={() => closeOther(0)} className="Collapsible_text_container">
                                                <div className="Collapsible_text">
                                                    <FormattedMessage id="footer.Legal" defaultMessage="Legal" />
                                                </div>
                                                <div className="Collapsible_arrow_container">
                                                    <img
                                                        width="15"
                                                        height="15"
                                                        src="/images/footer/PlusIcon.png"
                                                        alt=""
                                                    />
                                                </div>
                                            </div>
                                        }
                                        triggerWhenOpen={
                                            <div className="Collapsible_text_container open">
                                                <div className="Collapsible_text">
                                                    <FormattedMessage id="footer.Legal" defaultMessage="Legal" />
                                                </div>
                                                <div className="Collapsible_arrow_container">
                                                    <img
                                                        width="15"
                                                        height="24"
                                                        src="/images/footer/minusIcon.png"
                                                        alt=""
                                                    />
                                                </div>
                                            </div>
                                        }
                                    >
                                        <div style={{ textAlign: 'start' }}>
                                            <ul className="footer_ul_amrc">
                                                <li className="text-color">
                                                    <Link
                                                        to={`/${store_locale}/terms-and-conditions`}
                                                        style={{ textDecoration: 'none' }}
                                                    >
                                                        <FormattedMessage
                                                            id="footer.termsAndConditions"
                                                            defaultMessage="Terms & Conditions"
                                                        />
                                                    </Link>
                                                </li>
                                                <li className="text-color">
                                                    <Link
                                                        to={`/${store_locale}/privacy-policy`}
                                                        style={{ textDecoration: 'none' }}
                                                    >
                                                        <FormattedMessage
                                                            id="footer.PrivacyPolicy"
                                                            defaultMessage="Privacy Policy"
                                                        />
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </Collapsible>

                                    <Collapsible
                                        trigger={
                                            <div onClick={() => closeOther(0)} className="Collapsible_text_container">
                                                <div className="Collapsible_text">
                                                    <FormattedMessage
                                                        id="Footer.downloadApp"
                                                        defaultMessage="Download ELC App"
                                                    />
                                                </div>
                                                <div className="Collapsible_arrow_container">
                                                    <img
                                                        width="15"
                                                        height="15"
                                                        src="/images/footer/PlusIcon.png"
                                                        alt=""
                                                    />
                                                </div>
                                            </div>
                                        }
                                        triggerWhenOpen={
                                            <div className="Collapsible_text_container open">
                                                <div className="Collapsible_text">
                                                    <FormattedMessage
                                                        id="Footer.downloadApp"
                                                        defaultMessage="Download ELC App"
                                                    />
                                                </div>
                                                <div className="Collapsible_arrow_container">
                                                    <img
                                                        width="15"
                                                        height="24"
                                                        src="/images/footer/minusIcon.png"
                                                        alt=""
                                                    />
                                                </div>
                                            </div>
                                        }
                                    >
                                        <div style={{ textAlign: 'start' }}>
                                            <ul className="footer_ul_amrc">
                                                <li className="text-color">
                                                    <Box display="flex" className="p-0" onClick={() => gotoPlaystore()}>
                                                        <img
                                                            width="24"
                                                            height="24"
                                                            src="/images/footer/google-play-brands.svg"
                                                            alt="App store"
                                                        />
                                                        &nbsp;&nbsp;
                                                        <Typography className="footer-subTitle">
                                                            <FormattedMessage
                                                                id="Footer.playstore"
                                                                defaultMessage="Playstore"
                                                            />
                                                        </Typography>
                                                    </Box>
                                                </li>
                                                <li className="text-color">
                                                    <Box display="flex" className="p-0" onClick={() => gotoAppstore()}>
                                                        <img
                                                            width="24"
                                                            height="24"
                                                            src="/images/footer/app-store-ios-brands.svg"
                                                            alt="App store"
                                                        />
                                                        &nbsp;&nbsp;
                                                        <Typography className="footer-subTitle">
                                                            <FormattedMessage
                                                                id="Footer.appstore"
                                                                defaultMessage="Appstore"
                                                            />
                                                        </Typography>
                                                    </Box>
                                                </li>
                                            </ul>
                                        </div>
                                    </Collapsible>
                                </Grid>
                            </Grid>
                        )}
                        {!isCheckoutProcess() && (
                            <Grid item xs={11} className="footer-line">
                                <Grid container>
                                    <Grid item xs={12} className="m-t-20">
                                        <Grid container>
                                            <Grid item xs={12}>
                                                <Typography className="footer-title textAlignCenter">
                                                    <FormattedMessage
                                                        id="Footer.FollowUsOn"
                                                        defaultMessage="Follow us on"
                                                    />
                                                </Typography>
                                            </Grid>

                                            <Grid item xs={12} className="textAlignCenter">
                                                <Suspense fallback="">
                                                    <SocialSharing />
                                                </Suspense>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        )}

                        <Grid item xs={11} className={isCheckoutProcess() ? 'footer-line-c' : ''}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item xs={11} className="m-t-20">
                                    <Typography style={{ fontSize: '0.7rem', textAlign: 'center' }}>
                                        <FormattedMessage
                                            id="Footer.Bottom.Info"
                                            defaultMessage="© Website is operated by Kamal Osman Jamjoom LLC, trading as Early Learning Centre"
                                        />
                                    </Typography>
                                </Grid>
                                {country === 'UAE' && (
                                    <Grid item xs={11} className="textAlignCenter m-t-20">
                                        <img
                                            width="96"
                                            height="101"
                                            src="/images/footer/consumer_right_v1.png"
                                            className="consumer-right"
                                            alt="footer"
                                        />
                                    </Grid>
                                )}
                                <Grid item xs={11} className="textAlignCenter m-t-20 m-b-20">
                                    <img
                                        width="170"
                                        height="24"
                                        src={
                                            language === 'en'
                                                ? '/images/footer/logoEn_v1.png'
                                                : '/images/footer/logoAr_v1.png'
                                        }
                                        alt="footer"
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Suspense>
            </Hidden>
        </Grid>
    );
}

export default withRouter(Footer);
