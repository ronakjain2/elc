import api from 'apiServices';

const subScribeEmail = (payload) => api.post('/index.php/rest/V1/app/subscribetonewsletter', payload);

export default {
    subScribeEmail,
};
