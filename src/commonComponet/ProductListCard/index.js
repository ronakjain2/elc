import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Img from 'commonComponet/Image';
import { showDiscountPriceOnPDPProductSuggestion } from 'components/PDP/utils';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import './css/productListCard.css';

function ProductListCard({ data, currency, history }) {
    const state = useSelector(state => state);
    const store_locale = state && state.auth && state.auth.store_locale;

    const gotoPDP = () => history.push(`/${store_locale}/products-details/${data.url_key}`);

    return (
        <Grid item xs={12} className="BestSellerItem ">
            <div className="productsItem position-relative">
                {data.labels && (
                    <Typography
                        variant="span"
                        className="label position-absolute"
                        style={{
                            background: data.label_bg_color || '#aa272f',
                            color: data.label_text_color || '#fff',
                        }}
                    >
                        {data.labels}
                    </Typography>
                )}
                <Link to={`/${store_locale}/products-details/${data.url_key}`}>
                    <Img
                        width="184"
                        height="175"
                        src={data.productImageUrl[0]}
                        alt={data.name}
                    />
                    <div className="text-height">
                        {data.name.length > 45 ? (
                            <Typography className="p_title">{`${data.name.substring(0, 45)}...`}</Typography>
                        ) : (
                            <Typography className="p_title">{data.name}</Typography>
                        )}
                    </div>
                    <Typography className="p_amt">{showDiscountPriceOnPDPProductSuggestion(data)}</Typography>
                </Link>
                <Button className="addToBasket" style={{ marginTop: 0 }} onClick={() => gotoPDP(data)}>
                    <FormattedMessage id="Product.AddToBasket" defaultMessage="Add to basket" />
                </Button>
            </div>
        </Grid>
    );
}

export default withRouter(ProductListCard);
