import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import './style.css';

function FCBenfitTag({ show = false, className = '', showImage = true }) {
    if (!show) return null;
    return (
        <>
            <Grid container justify="flex-start" className={className}>
                <Typography className="fc-benefit-tag">
                    {showImage && (
                        <span>
                            <img
                                src="/images/pdp/icon_fc_benefit.svg"
                                alt="family club benefit applied"
                                style={{
                                    height: '16px',
                                    width: '16px',
                                }}
                            />
                            &nbsp;
                        </span>
                    )}
                    <FormattedMessage id="cart.FamilyClubbenefitapplied" defaultMessage="Family Club benefit applied" />
                </Typography>
            </Grid>
        </>
    );
}

export default FCBenfitTag;
