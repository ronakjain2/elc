import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import { makeStyles } from '@material-ui/core/styles';
import Spinner from 'commonComponet/Spinner';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
        justifyContent: 'flex-start',
    },
    netIssue: {
        margin: '0 auto',
    },
}));

export default function SimpleBackdrop({ open, netIssue }) {
    const classes = useStyles();
    return (
        <div>
            <Backdrop className={classes.backdrop} open={open}>
                <Spinner />
            </Backdrop>
            {netIssue && (
                <Backdrop className={classes.backdrop} open={netIssue}>
                    <Typography className={classes.netIssue}>Please Check internate connection</Typography>
                </Backdrop>
            )}
        </div>
    );
}
