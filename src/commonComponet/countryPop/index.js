import React, { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import './styles.css';
import { createAuthObjectOnLangAndCountryChange } from 'commonComponet/Header/utils';
import { useDispatch, useSelector } from 'react-redux';
import { actions as AuthActions } from 'components/Global/redux/actions';
import { Close } from '@material-ui/icons';

export default function CountryPOP({ isOpen, setOpen }) {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const handleCloseOutSide = () => {
        setOpen(true);
    };

    const [selectedCountry, setSelectedCountry] = useState('UAE');
    const [selectedLanguage, setSelectedLanguage] = useState('en');

    const onChangeCountry = country => {
        setSelectedCountry(country);
    };

    const onChangeLanaguage = lang => {
        setSelectedLanguage(lang);
    };

    const onSubmit = () => {
        if (!selectedCountry || selectedCountry === '' || !selectedLanguage || selectedLanguage === '') {
            return;
        }
        const obj = createAuthObjectOnLangAndCountryChange(selectedCountry, selectedLanguage, state && state.auth);
        setOpen(false);
        dispatch(AuthActions.getStoreInfoRequest(obj));
    };

    const onClickClose = () => {
        setOpen(false);
        dispatch(AuthActions.setDefaultCountryPop(false));
    };

    return (
        <Dialog
            open={isOpen}
            onClose={handleCloseOutSide}
            className="country-pop-container"
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogContent>
                <Grid container className="country-pop">
                    <div className="close-icon">
                        <IconButton onClick={() => onClickClose()}>
                            <Close />
                        </IconButton>
                    </div>
                    <Grid item xs={12}>
                        <Grid container justify="center" alignItems="center">
                            <Typography className="title">Select Your Country</Typography>
                        </Grid>
                    </Grid>

                    <Grid item xs={12}>
                        <Grid container justify="space-around" alignItems="center">
                            <div
                                onClick={() => onChangeCountry('KSA')}
                                className={
                                    selectedCountry === 'KSA'
                                        ? 'country-flag-container active-container'
                                        : 'country-flag-container'
                                }
                            >
                                <img
                                    width="30"
                                    height="20"
                                    src="/images/header/top/Icon_Flag_KSA_v1.svg"
                                    alt="KSAFlag"
                                />
                            </div>
                            <div
                                onClick={() => onChangeCountry('UAE')}
                                className={
                                    selectedCountry === 'UAE'
                                        ? 'country-flag-container active-container'
                                        : 'country-flag-container'
                                }
                            >
                                <img
                                    width="30"
                                    height="20"
                                    src="/images/header/top/Icon_Flag_UAE_v1.svg"
                                    alt="UAEFlag"
                                />
                            </div>
                        </Grid>
                    </Grid>

                    <Grid item xs={12} className="language-p-t">
                        <Grid container justify="center" alignItems="center">
                            <Typography className="title">Select your language</Typography>
                        </Grid>
                    </Grid>

                    <Grid item xs={12}>
                        <Grid container justify="space-around" alignItems="center">
                            <div
                                onClick={() => onChangeLanaguage('ar')}
                                className={
                                    selectedLanguage === 'ar'
                                        ? 'country-flag-container lan-text-container-p-t-b active-container'
                                        : 'country-flag-container lan-text-container-p-t-b'
                                }
                            >
                                <Typography className="language-text-pop">العربية</Typography>
                            </div>
                            <div
                                onClick={() => onChangeLanaguage('en')}
                                className={
                                    selectedLanguage === 'en'
                                        ? 'country-flag-container lan-text-container-p-t-b active-container'
                                        : 'country-flag-container lan-text-container-p-t-b'
                                }
                            >
                                <Typography className="language-text-pop">English</Typography>
                            </div>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container justify="center" alignItems="center">
                            <Grid item xs={6} md={4}>
                                <Button className="submit-button-c-pop" onClick={() => onSubmit()}>
                                    Submit
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    );
}
