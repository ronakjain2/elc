import api from 'apiServices';

const getMenu = storeId => api.get(`/index.php/rest/V1/app/orob/menu_v1?store=${storeId}`);
const getSearchSuggestions = payload => api.post('/rest/V1/app/search', payload);

export default {
    getMenu,
    getSearchSuggestions,
};
