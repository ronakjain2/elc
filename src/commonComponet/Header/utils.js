export const createAuthObjectOnLangChange = (lang, auth) => {
    const { country } = auth;
    const { quote_id } = auth;

    const obj = {
        storeInfo: {},
        storeData: '',
        storeChnageObj: '',
    };
    if (country === 'KSA') {
        if (lang === 'en') {
            obj.storeInfo = {
                language: 'en',
                currentStore: 2,
                country: 'KSA',
                region: 'saudi-en',
                store_locale: 'saudi-en',
            };
            obj.storeData = 'KSA_en';
            obj.storeChnageObj = {
                store_id: 2,
                quote_id,
            };
        }
        if (lang === 'ar') {
            obj.storeInfo = {
                language: 'ar',
                currentStore: 1,
                country: 'KSA',
                region: 'saudi-ar',
                store_locale: 'saudi-ar',
            };
            obj.storeData = 'KSA_ar';
            obj.storeChnageObj = {
                store_id: 1,
                quote_id,
            };
        }
    }

    if (country === 'UAE') {
        if (lang === 'en') {
            obj.storeInfo = {
                language: 'en',
                currentStore: 4,
                country: 'UAE',
                region: 'uae-en',
                store_locale: 'uae-en',
            };
            obj.storeData = 'UAE_en';
            obj.storeChnageObj = {
                store_id: 4,
                quote_id,
            };
        }
        if (lang === 'ar') {
            obj.storeInfo = {
                language: 'ar',
                currentStore: 3,
                country: 'UAE',
                region: 'uae-ar',
                store_locale: 'uae-ar',
            };
            obj.storeData = 'UAE_ar';
            obj.storeChnageObj = {
                store_id: 3,
                quote_id,
            };
        }
    }

    return obj;
};

export const createAuthObjectOnCountryChnage = (country, auth) => {
    const { language } = auth;
    const { quote_id } = auth;

    const obj = {
        storeInfo: {},
        storeData: '',
        storeChnageObj: '',
        countryChange: true,
    };
    if (language === 'en') {
        if (country === 'KSA') {
            obj.storeInfo = {
                language: 'en',
                currentStore: 2,
                country: 'KSA',
                region: 'saudi-en',
                store_locale: 'saudi-en',
            };
            obj.storeData = 'KSA_en';
            obj.storeChnageObj = {
                store_id: 2,
                quote_id,
            };
        }
        if (country === 'UAE') {
            obj.storeInfo = {
                language: 'en',
                currentStore: 4,
                country: 'UAE',
                region: 'uae-en',
                store_locale: 'uae-en',
            };
            obj.storeData = 'UAE_en';
            obj.storeChnageObj = {
                store_id: 4,
                quote_id,
            };
        }
    }

    if (language === 'ar') {
        if (country === 'KSA') {
            obj.storeInfo = {
                language: 'ar',
                currentStore: 1,
                country: 'KSA',
                region: 'saudi-ar',
                store_locale: 'saudi-ar',
            };
            obj.storeData = 'KSA_ar';
            obj.storeChnageObj = {
                store_id: 1,
                quote_id,
            };
        }
        if (country === 'UAE') {
            obj.storeInfo = {
                language: 'ar',
                currentStore: 3,
                country: 'UAE',
                region: 'uae-ar',
                store_locale: 'uae-ar',
            };
            obj.storeData = 'UAE_ar';
            obj.storeChnageObj = {
                store_id: 3,
                quote_id,
            };
        }
    }

    return obj;
};

export const createAuthObjectOnLangAndCountryChange = (country, lang, auth) => {
    const language = lang;
    const { quote_id } = auth;

    const obj = {
        storeInfo: {},
        storeData: '',
        storeChnageObj: '',
    };
    if (language === 'en') {
        if (country === 'KSA') {
            obj.storeInfo = {
                language: 'en',
                currentStore: 2,
                country: 'KSA',
                region: 'saudi-en',
                store_locale: 'saudi-en',
            };
            obj.storeData = 'KSA_en';
            obj.storeChnageObj = {
                store_id: 2,
                quote_id,
            };
        }
        if (country === 'UAE') {
            obj.storeInfo = {
                language: 'en',
                currentStore: 4,
                country: 'UAE',
                region: 'uae-en',
                store_locale: 'uae-en',
            };
            obj.storeData = 'UAE_en';
            obj.storeChnageObj = {
                store_id: 4,
                quote_id,
            };
        }
    }

    if (language === 'ar') {
        if (country === 'KSA') {
            obj.storeInfo = {
                language: 'ar',
                currentStore: 1,
                country: 'KSA',
                region: 'saudi-ar',
                store_locale: 'saudi-ar',
            };
            obj.storeData = 'KSA_ar';
            obj.storeChnageObj = {
                store_id: 1,
                quote_id,
            };
        }
        if (country === 'UAE') {
            obj.storeInfo = {
                language: 'ar',
                currentStore: 3,
                country: 'UAE',
                region: 'uae-ar',
                store_locale: 'uae-ar',
            };
            obj.storeData = 'UAE_ar';
            obj.storeChnageObj = {
                store_id: 3,
                quote_id,
            };
        }
    }

    return obj;
};

export const changeBrowserUrl = (storeId) => {
    const array = (window && window.location && window.location.href && window.location.href.split('/')) || [];
    let storeName = 'uae-ar';
    if (storeId === 1) {
        storeName = 'saudi-ar';
    }
    if (storeId === 2) {
        storeName = 'saudi-en';
    }
    if (storeId === 3) {
        storeName = 'uae-ar';
    }
    if (storeId === 4) {
        storeName = 'uae-en';
    }
    if (array.length > 0) {
        if (array.length === 4) {
            window.history.pushState('', '', `/${storeName}/`);
        } else if (array.length > 4) {
            let url = `/${storeName}`;
            for (let i = 4; i < array.length; i++) {
                url = `${url}/${array[i]}`;
            }
            window.history.pushState('', '', `${url}`);
        }
    }
};
