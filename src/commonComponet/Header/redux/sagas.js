import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import api from '../api';
import { actions, types } from './actions';

const getMenuReq = function* getMenuReq({ payload }) {
    try {
        const { storeId } = payload;
        const resopnse = yield call(api.getMenu, storeId);
        if (resopnse && resopnse.data && resopnse.data.status) {
            yield put(actions.getMenuRequestSuccess(resopnse.data.data));
        } else {
            yield put(actions.getMenuRequestFailed());
        }
    } catch (err) {
        yield put(actions.getMenuRequestFailed());
    }
};

const getAutosuggestSearchProductReq = function* getAutosuggestSearchProductReq({ payload }) {
    try {
        const { data } = yield call(api.getSearchSuggestions, payload);
        if (data && data.data) {
            yield put(actions.getAutoSuggestSearchRequestSuccess(data.data));
        } else {
            yield put(actions.getAutoSuggestSearchRequestFailed());
        }
    } catch (err) {
        yield put(actions.getAutoSuggestSearchRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_MENU_REQUEST, getMenuReq);
    yield takeEvery(types.GET_AUTOSUGGEST_SEARCH_REQUEST, getAutosuggestSearchProductReq);
}
