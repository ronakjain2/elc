import { createAction } from 'redux-actions';

// actions types
const GET_MENU_REQUEST = 'ELC@OROB/GET_MENU_REQUEST';
const GET_MENU_REQUEST_SUCCESS = 'ELC@OROB/GET_MENU_REQUEST_SUCCESS';
const GET_MENU_REQUEST_FAILED = 'ELC@OROB/GET_MENU_REQUEST_FAILED';

const GET_AUTOSUGGEST_SEARCH_REQUEST = 'ELC@OROB/GET_AUTOSUGGEST_SEARCH_REQUEST';
const GET_AUTOSUGGEST_SEARCH_REQUEST_SUCCESS = 'ELC@OROB/GET_AUTOSUGGEST_SEARCH_REQUEST_SUCCESS';
const GET_AUTOSUGGEST_SEARCH_REQUEST_FAILED = 'ELC@OROB/GET_AUTOSUGGEST_SEARCH_REQUEST_FAILED';

// actions methods
const getMenuRequest = createAction(GET_MENU_REQUEST);
const getMenuRequestSuccess = createAction(GET_MENU_REQUEST_SUCCESS);
const getMenuRequestFailed = createAction(GET_MENU_REQUEST_FAILED);

const getAutoSuggestSearchRequest = createAction(GET_AUTOSUGGEST_SEARCH_REQUEST);
const getAutoSuggestSearchRequestSuccess = createAction(GET_AUTOSUGGEST_SEARCH_REQUEST_SUCCESS);
const getAutoSuggestSearchRequestFailed = createAction(GET_AUTOSUGGEST_SEARCH_REQUEST_FAILED);

export const actions = {
    getMenuRequest,
    getMenuRequestSuccess,
    getMenuRequestFailed,

    getAutoSuggestSearchRequest,
    getAutoSuggestSearchRequestSuccess,
    getAutoSuggestSearchRequestFailed,
};

export const types = {
    GET_MENU_REQUEST,
    GET_MENU_REQUEST_SUCCESS,
    GET_MENU_REQUEST_FAILED,

    GET_AUTOSUGGEST_SEARCH_REQUEST,
    GET_AUTOSUGGEST_SEARCH_REQUEST_SUCCESS,
    GET_AUTOSUGGEST_SEARCH_REQUEST_FAILED,
};
