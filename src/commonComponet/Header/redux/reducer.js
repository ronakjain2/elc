import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandlers = {
    [types.GET_MENU_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_MENU_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.GET_MENU_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        menuList: payload && payload,
    }),

    [types.GET_AUTOSUGGEST_SEARCH_REQUEST]: (state) => ({
        ...state,
        searchLoader: true,
    }),
    [types.GET_AUTOSUGGEST_SEARCH_REQUEST_FAILED]: (state) => ({
        ...state,
        searchLoader: false,
        autoSuggestSearchProducts: null,
    }),
    [types.GET_AUTOSUGGEST_SEARCH_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        searchLoader: false,
        autoSuggestSearchProducts: payload,
    }),
};

export default handleActions(actionHandlers, {
    loader: false,
    menuList: [],
    autoSuggestSearchProducts: null,
});
