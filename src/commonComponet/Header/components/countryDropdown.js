import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import Typography from '@material-ui/core/Typography';
import { ExpandMore } from '@material-ui/icons';
import { actions as AuthActions } from 'components/Global/redux/actions';
import React, { useRef, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import '../css/header.css';
import { changeBrowserUrl, createAuthObjectOnCountryChnage, createAuthObjectOnLangChange } from '../utils';

export default function CountryDropDown({ mobile, setAnchor }) {
    const state = useSelector(state => state);

    const dispatch = useDispatch();

    const [open, setOpen] = useState(false);
    const anchorRef = useRef(null);

    const handleToggle = () => {
        setOpen(prevOpen => !prevOpen);
    };

    const handleClose = (event, country) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);

        if (country) {
            setAnchor && setAnchor(false);
            const obj = createAuthObjectOnCountryChnage(country, state && state.auth);
            if (obj && obj.storeInfo && obj.storeInfo.country) {
                dispatch(AuthActions.getStoreInfoRequest(obj));
            }
            changeBrowserUrl(obj && obj.storeInfo && obj.storeInfo.currentStore);
        }
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        }
    }

    const onlanguagechange = lang => {
        setAnchor && setAnchor(false);
        const obj = createAuthObjectOnLangChange(lang, state && state.auth);
        if (obj && obj.storeInfo && obj.storeInfo.country) {
            dispatch(AuthActions.getStoreInfoRequest(obj));
        }
        changeBrowserUrl(obj && obj.storeInfo && obj.storeInfo.currentStore);
    };

    return (
        <div
            className={
                mobile
                    ? 'flex justifyCenter alignItemCenter  height-100'
                    : ' flex justifyCenter alignItemCenter textStyle height-100'
            }
            style={{ padding: 0 }}
        >
            <Grid container justify="space-between" alignItems="center">
                <div>
                    <Button
                        ref={anchorRef}
                        aria-controls={open ? 'menu-list-grow' : undefined}
                        aria-haspopup="true"
                        onClick={handleToggle}
                    >
                        <Grid container justify="space-between" alignItems="center">
                            <div className="flex">
                                {state && state.auth && state.auth.country === 'UAE' && (
                                    <img
                                        src="/images/header/top/Icon_Flag_UAE_v1.svg"
                                        alt="countrylogo"
                                        className="countryflagIcon"
                                    />
                                )}
                                {state && state.auth && state.auth.country === 'KSA' && (
                                    <img
                                        src="/images/header/top/Icon_Flag_KSA_v1.svg"
                                        alt="countrylogo"
                                        className="countryflagIcon"
                                    />
                                )}
                            </div>
                            <div>
                                <ExpandMore className="dropIcon" />
                            </div>
                        </Grid>
                    </Button>
                    <Popper
                        open={open}
                        anchorEl={anchorRef.current}
                        role={undefined}
                        transition
                        disablePortal
                        style={{ zIndex: 1000 }}
                    >
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                style={{
                                    transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                                    zIndex: 1,
                                }}
                            >
                                <Paper>
                                    <ClickAwayListener onClickAway={e => handleClose(e)}>
                                        <MenuList
                                            autoFocusItem={open}
                                            id="menu-list-grow"
                                            onKeyDown={handleListKeyDown}
                                        >
                                            <MenuItem onClick={e => handleClose(e, 'UAE')}>
                                                <Grid container>
                                                    <div className="flex align-items-center">
                                                        <div
                                                            className="country-image-div"
                                                            style={{
                                                                background: `url(${'/images/header/top/Icon_Flag_UAE_v1.svg'})`,
                                                                backgroundRepeat: 'no-repeat',
                                                            }}
                                                        />
                                                    </div>
                                                    <div>
                                                        <Typography className="greenColor p-8">
                                                            <FormattedMessage id="header.uae" defaultMessage="UAE" />
                                                        </Typography>
                                                    </div>
                                                </Grid>
                                            </MenuItem>
                                            <MenuItem onClick={e => handleClose(e, 'KSA')}>
                                                <Grid container>
                                                    <div className="flex align-items-center">
                                                        <div
                                                            className="country-image-div"
                                                            style={{
                                                                background: `url(${'/images/header/top/Icon_Flag_KSA_v1.svg'})`,
                                                                backgroundRepeat: 'no-repeat',
                                                            }}
                                                        />
                                                    </div>
                                                    <div>
                                                        <Typography className="greenColor p-8">
                                                            <FormattedMessage id="header.ksa" defaultMessage="KSA" />
                                                        </Typography>
                                                    </div>
                                                </Grid>
                                            </MenuItem>
                                        </MenuList>
                                    </ClickAwayListener>
                                </Paper>
                            </Grow>
                        )}
                    </Popper>
                </div>
                <div>
                    <ul className="m-0">
                        {state && state.auth && state.auth.language !== 'en' && (
                            <li className="inline-block" onClick={() => onlanguagechange('en')}>
                                <Typography
                                    className={
                                        state && state.auth && state.auth.language === 'en'
                                            ? 'active-text language-text'
                                            : 'language-text cursor-pointer'
                                    }
                                >
                                    English
                                </Typography>
                            </li>
                        )}

                        {state && state.auth && state.auth.language !== 'ar' && (
                            <li className="inline-block" onClick={() => onlanguagechange('ar')}>
                                <Typography
                                    className={
                                        state && state.auth && state.auth.language === 'ar'
                                            ? 'active-text language-text'
                                            : 'language-text cursor-pointer'
                                    }
                                >
                                    العربية
                                </Typography>
                            </li>
                        )}
                    </ul>
                </div>
            </Grid>
        </div>
    );
}
