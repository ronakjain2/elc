import Grid from '@material-ui/core/Grid';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import React from 'react';
import { isMobile } from 'react-device-detect';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

let activeHoverIndex;
let submenuIds = [];
export default function WebNavigation() {
    const state = useSelector(state => state);

    const menu = (state && state.menu && state.menu.menuList) || [];
    const store_locale = state && state.auth && state.auth.store_locale;
    const closeWebSubMenu = () => {
        submenuIds.map((item, index) => {
            if (activeHoverIndex === index) {
                return (document.getElementById(item).style.display = 'none');
            }
            return null;
        });
        document.body.scrollTop = '140px';
    };

    const _renderSubMenuItem = (item, index) => {
        //const category = item.url_key.split('/');
        // let menu_item = 'menu.' + item.name.toUpperCase();
        let isSub_children = false;

        if (item.sub_children && item.sub_children.length > 0) {
            isSub_children = true;
        }

        let redirectToPLPPath = item && item.info && item.info.url_key && item.info.url_key;
        let display_mode = item && item.info && item.info.display_mode;

        return (
            <Grid item xs={4} key={`sumMenu${index}`}>
                <ul style={!isMobile && isSub_children === true ? { paddingBottom: 40 } : {}} key={index}>
                    <li>
                        <Link
                            to={
                                display_mode === 'PAGE'
                                    ? `/${store_locale}/landing/${redirectToPLPPath}`
                                    : `/${store_locale}/products/${redirectToPLPPath}`
                            }
                        >
                            <span onClick={() => closeWebSubMenu()}>{item.info && item.info.name}</span>
                        </Link>
                    </li>
                    {!isMobile &&
                        item.sub_children &&
                        item.sub_children &&
                        item.sub_children.map((item, index) => {
                            if (index < 5) {
                                return (
                                    <Link
                                        key={`sub_children${index}`}
                                        to={
                                            item.display_mode === 'PAGE'
                                                ? `/${store_locale}/landing/${item.url_key}`
                                                : `/${store_locale}/products/${item.url_key}`
                                        }
                                    >
                                        <li
                                            onClick={() => closeWebSubMenu()}
                                            style={{ paddingBottom: 5 }}
                                            className="l3-design"
                                        >
                                            {item.name}
                                        </li>
                                    </Link>
                                );
                            }

                            return null;
                        })}

                    {!isMobile && item.see_all && item.see_all.length !== 0 && (
                        <Link to={'/' + store_locale + '/products/' + item.see_all.url_key}>
                            <li
                                onClick={() => closeWebSubMenu()}
                                className="l3-design"
                                style={{
                                    color: '#0d943f',
                                    fontWeight: 'bold',
                                    paddingTop: 5,
                                }}
                            >
                                <FormattedMessage id="SeeAll.Text" defaultMessage="See all" />
                            </li>
                        </Link>
                    )}
                </ul>
            </Grid>
        );
    };

    const _renderSubMenuList = data => {
        const shopAllBrands =
            data.children &&
            data.children.length > 0 &&
            data.children.filter(item => item.info && item.info.url_key === 'shop-all-brands');
        return (
            <ul className="subLink text-align-rtl">
                <Grid container>
                    {data.children &&
                        data.children.length > 0 &&
                        data.children
                            .filter(item => item.info && item.info.url_key !== 'shop-all-brands')
                            .map(_renderSubMenuItem)}
                </Grid>
                {shopAllBrands.length > 0 && (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid className="shop-all-brands" item xs={4} key={`sumMenu-${shopAllBrands[0].info.name}`}>
                            <Link to={'/' + store_locale + '/browse-all-brands'}>{shopAllBrands[0].info.name}</Link>
                        </Grid>
                    </Grid>
                )}
                {data.display_mode === 'PAGE' && (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid className="shop-all-brands" item xs={4} key={`sumMenu-${data.name}`}>
                            <Link to={`/${store_locale}/landing/${data.url_key}`}>
                                <FormattedMessage id="Menu.shopAll" defaultMessage="Shop all " />
                                {data.name}
                            </Link>
                        </Grid>
                    </Grid>
                )}
            </ul>
        );
    };
    const _checkSubMenu = (item, index) => {
        let isSub_children = false;
        item.children &&
            item.children.map(item => {
                if (item.sub_children && item.sub_children.length > 0) {
                    isSub_children = true;
                }
                return isSub_children;
            });

        let submenu;
        submenu = `submenu.${index}`;
        submenuIds.push(submenu);
        return (
            <li
                id={submenu}
                className="submenu"
                style={
                    !isMobile && isSub_children === true
                        ? { backgroundColor: '#fff' }
                        : item.children && item.children.length > 0
                        ? { backgroundColor: '#fff', textAlign: 'center' }
                        : { display: 'none' }
                }
            >
                {_renderSubMenuList(item)}
            </li>
        );
    };

    const getCurrentOverIndex = currentIndex => {
        submenuIds.map((item, index) => {
            if (activeHoverIndex && currentIndex === index) {
                document.getElementById(item).style.removeProperty('display');
            }
            return null;
        });
        activeHoverIndex = currentIndex;
    };
    const _renderMenuNavigation = menu => {
        return Object.values(menu).map(_renderMenuList);
    };

    const _renderMenuList = (item, index) => {
        //let menu_item re= 'menu.' + item.name.toLowerCase();
        return (
            <>
                <li
                    onMouseOver={() => {
                        item.children && item.children.length > 0 && getCurrentOverIndex(index);
                    }}
                    key={index}
                    className="l1_menu borderForMobileManu"
                    style={{ marginTop: 0 }}
                >
                    {item && item.url_key === 'shop-all-brands' ? (
                        <Link
                            style={{ textDecoration: 'none', padding: '20px 7px' }}
                            className={item.children && item.children.length > 0 ? '' : 'removeWhite'}
                            to={'/' + store_locale + '/browse-all-brands'}
                        >
                            {item.name}
                        </Link>
                    ) : (
                        <Link
                            to={
                                item.display_mode === 'PAGE'
                                    ? `/${store_locale}/landing/${item.url_key}`
                                    : `/${store_locale}/products/${item.url_key}`
                            }
                            style={{ textDecoration: 'none', padding: '20px 10px' }}
                            className={item.children && item.children.length > 0 ? '' : 'removeWhite'}
                        >
                            {item.name}
                            {item.children && item.children.length > 0 ? <ArrowDropDown /> : <span />}
                        </Link>
                    )}

                    {item.children && item.children.length > 0 ? <span /> : <span />}
                    {_checkSubMenu(item, index)}
                </li>
            </>
        );
    };

    return (
        <div>
            <div className="webNavigation">
                <div className="MenuItems">{_renderMenuNavigation(menu)}</div>
            </div>
        </div>
    );
}
