import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Close from '@material-ui/icons/Close';
import SocialSharing from 'commonComponet/SocialSharing';
import { actions as myWishlistActions } from 'components/Account/MyWishList/redux/actions';
import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import { actions as authActions } from 'components/Global/redux/actions';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import CountryDropDown from '../countryDropdown';

let arrl1 = [];
let arrl2 = [];

function MobileNavigation({ setAnchor, history }) {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const menu = [...(state && state.menu && state.menu.menuList)] || [];
    const store_locale = state && state.auth && state.auth.store_locale;
    const loginUser = state && state.auth && state.auth.loginUser;
    const customer_type = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_type;

    const closeMenu = () => {
        setAnchor(false);
    };

    const goToReturnPolicy = () => {
        setAnchor(false);
        history.push(`/${store_locale}/return-policy`);
    };

    const goToDeliveryPolicy = () => {
        setAnchor(false);
        history.push(`/${store_locale}/delivery-policy`);
    };

    const goToHelpAndFAQ = () => {
        setAnchor(false);
        history.push(`/${store_locale}/help-and-faq`);
    };

    const onClickLogout = () => {
        setAnchor(false);
        dispatch(authActions.saveLoginUser(null));
        dispatch(authActions.saveQuoteIdRequest(null));
        dispatch(authActions.saveGuestUser(null));
        dispatch(basketActions.saveCartCountRequest(0));
        dispatch(myWishlistActions.addWishListItemRequestSuccess([]));
        history.push(`/${store_locale}/sign-in-register`);
    };

    const goToBirthDayClub = () => {
        setAnchor(false);
        if (customer_type && customer_type === 'ecom_club_user') {
            return history.push(`/${store_locale}/my-club`);
        } else {
            return history.push(`/${store_locale}/family-club`);
        }
    };

    const LoginOrMyAccount = event => {
        setAnchor(false);
        if (!loginUser) {
            history.push(`/${state.auth.store_locale}/sign-in-register`);
        }
    };

    const gotoMyAccount = () => {
        setAnchor(false);
        history.push(`/${state.auth.store_locale}/my-account`);
    };

    const gotoStoreLocator = () => {
        setAnchor(false);
        history.push(`/${state && state.auth && state.auth.store_locale}/store-locator`);
    };
    const _renderMobileMenuL1List = (item, index) => {
        let a;
        a = `u1l2.${index}`;
        arrl2.push(a);
        let b;
        b = `u1l1.${index}`;
        arrl1.push(b);
        let path = item && item.url_key && item.url_key;
        const shopAllBrands =
            (item.children &&
                item.children.length > 0 &&
                item.children.filter(item => item.info && item.info.url_key === 'shop-all-brands')) ||
            [];

        item.children = [
            ...item.children.filter(item => item.info && item.info.url_key !== 'shop-all-brands'),
            ...shopAllBrands,
        ];
        return (
            <>
                <ul style={{ width: '100%', padding: 0, margin: 0 }} className="m_Menu">
                    <div id={b}>
                        <span className="nav-linkMenu">
                            {item.children && item.children.length === 0 ? (
                                item && item.url_key === 'shop-all-brands' ? (
                                    <Link onClick={() => closeMenu()} to={'/' + store_locale + '/browse-all-brands'}>
                                        <span className="nav__linkMenuSpan">{item.name}</span>
                                    </Link>
                                ) : (
                                    <Link
                                        onClick={() => closeMenu()}
                                        to={
                                            item.display_mode === 'PAGE'
                                                ? '/' + store_locale + '/landing/' + path
                                                : '/' + store_locale + '/products/' + path
                                        }
                                    >
                                        <span className="nav__linkMenuSpan">{item.name}</span>
                                    </Link>
                                )
                            ) : (
                                <Link
                                    onClick={() => closeMenu()}
                                    to={
                                        item.display_mode === 'PAGE'
                                            ? '/' + store_locale + '/landing/' + path
                                            : '/' + store_locale + '/products/' + path
                                    }
                                    className="nav__linkMenuSpan"
                                >
                                    {item.name}
                                </Link>
                            )}
                            {item.children && item.children.length > 0 ? (
                                <span onClick={() => openL2Cat(item, index)} className="nav__link--drill__down"></span>
                            ) : (
                                <span />
                            )}
                        </span>
                    </div>
                </ul>
                <div id={a} style={{ display: 'none', width: '100%' }}>
                    {item.children && item.children.length > 0 && item.children.map(_renderL2Mobile)}
                    {item.display_mode === 'PAGE' && (
                        <ul className="shop-all-landing pad-l-l2">
                            <li style={{ textAlign: 'initial' }}>
                                <Link to={`/${store_locale}/landing/${item.url_key}`} onClick={() => closeMenu()}>
                                    <FormattedMessage id="Menu.shopAll" defaultMessage="Shop all " />
                                    {item.name}
                                </Link>
                            </li>
                        </ul>
                    )}
                </div>
            </>
        );
    };

    const openL2Cat = (item, index) => {
        if (item.children && item.children.length > 0) {
            const check = document.getElementById('l2back');
            if (check.classList.contains('activeU1')) {
                check.classList.remove('activeU1');
            } else {
                check.classList.add('activeU1');
            }

            let newu2 = `u1l2.${index}`;

            const u2 = document.getElementById(newu2);
            arrl1.map((item, currentIndex) => {
                return (document.getElementById(item).style.display = 'none');
            });
            arrl2.map((item, currentIndex) => {
                if (newu2 === item) {
                    return (u2.style.display = 'block');
                } else {
                    return (document.getElementById(item).style.display = 'none');
                }
            });
        }
    };

    const _renderL2Mobile = (item, index) => {
        let flag = false;
        flag = item.sub_children && item.sub_children.length !== 0;
        let path = item && item.info && item.info.url_key && item.info.url_key;
        let display_mode = item && item.info && item.info.display_mode;

        if (flag === true) {
            let l3 = null;
            l3 = `collapse.${index}`;
            return (
                <ul className="pad-l" id="hide">
                    {index === 0 && (
                        <li onClick={() => onClickOnBack(index)} style={{ textAlign: 'initial' }} className="m_Menu">
                            <span className="sm-back js-enquire-sub-close hidden-md hidden-lg">
                                <FormattedMessage id="Back.Text" defaultMessage="Back" />
                            </span>
                        </li>
                    )}
                    <li style={{ textAlign: 'initial' }} className="m_Menu">
                        <span className="nav-linkMenu">
                            <Link
                                to={
                                    display_mode === 'PAGE'
                                        ? `/${store_locale}/landing/${path}`
                                        : `/${store_locale}/products/${path}`
                                }
                                onClick={() => closeMenu()}
                            >
                                <span className="nav__linkMenuSpanL2">{item.info.name}</span>
                            </Link>
                            {item.sub_children && item.sub_children.length > 0 ? (
                                <span
                                    onClick={() => showhideL3Cat(index, l3)}
                                    id={l3}
                                    className="subListToggle collapsed"
                                ></span>
                            ) : (
                                <span />
                            )}
                        </span>
                    </li>
                    <li id={`open.${index}`} style={{ display: 'none' }}>
                        {Object(item.sub_children).length > 0 && Object(item.sub_children).map(_renderMobileMenuL3List)}

                        {item.see_all && item.see_all.length !== 0 && (
                            <Link
                                to={'/' + store_locale + '/products/' + item.see_all.url_key}
                                onClick={() => closeMenu()}
                            >
                                <li
                                    className="l3-design pad-l"
                                    style={{
                                        color: '#0d943f',
                                        fontWeight: 'bold',
                                        textAlign: 'initial',
                                    }}
                                >
                                    <FormattedMessage id="SeeAll.Text" defaultMessage="See all" />
                                </li>
                            </Link>
                        )}
                    </li>
                </ul>
            );
        } else {
            return (
                <ul className="pad-l">
                    {index === 0 && (
                        <li onClick={() => onClickOnBack(index)} style={{ textAlign: 'initial' }} className="m_Menu">
                            <span className="sm-back  js-enquire-sub-close hidden-md hidden-lg">
                                <FormattedMessage id="Back.Text" defaultMessage="Back" />
                            </span>
                        </li>
                    )}
                    <li className="sub-navigation-list">
                        <span className="nav__link--secondary">
                            {item.info && item.info.url_key === 'shop-all-brands' ? (
                                <Link to={'/' + store_locale + '/browse-all-brands'} onClick={() => closeMenu()}>
                                    <span className="shop-all-brands">{item.info && item.info.name}</span>
                                </Link>
                            ) : (
                                <Link
                                    to={
                                        display_mode === 'PAGE'
                                            ? `/${store_locale}/landing/${path}`
                                            : `/${store_locale}/products/${path}`
                                    }
                                    onClick={() => closeMenu()}
                                >
                                    <span>{item.info && item.info.name}</span>
                                </Link>
                            )}
                        </span>
                    </li>
                </ul>
            );
        }
    };
    const showhideL3Cat = (index, itemkey) => {
        const l3 = document.getElementById(itemkey);
        const openl3 = document.getElementById(`open.${index}`);
        if (l3.classList.contains('collapsed')) {
            l3.classList.remove('collapsed');
            openl3.style.display = 'block';
        } else {
            l3.classList.add('collapsed');
            openl3.style.display = 'none';
        }
    };
    const onClickOnBack = index => {
        arrl1.map((item, currentIndex) => {
            return (document.getElementById(item).style.display = 'block');
        });
        arrl2.map((item, currentIndex) => {
            return (document.getElementById(item).style.display = 'none');
        });
    };

    const _renderMobileMenuL3List = (item, index) => {
        return (
            <ul className="pad-l" id="subListNum0" style={{ paddingTop: 10 }} aria-expanded="true">
                <li className="nav__link--secondary-l3">
                    <Link
                        to={
                            item.display_mode === 'PAGE'
                                ? `/${store_locale}/landing/${item.url_key}`
                                : `/${store_locale}/products/${item.url_key}`
                        }
                        onClick={() => closeMenu()}
                    >
                        <span style={{ textAlign: 'initial' }}>{item.name}</span>
                    </Link>
                </li>
            </ul>
        );
    };

    return (
        <Grid container justify="center" className="mobile_menu">
            <Grid container className="mobile_menu_container" justify="space-between">
                <Grid item xs={10} className="flex alignItemFlexStart">
                    <img
                        src="/images/header/second/Icon_Header_User.svg"
                        alt="storeLocator"
                        style={{ paddingTop: 5 }}
                    />
                    <span className="secondText">
                        <span onClick={() => LoginOrMyAccount()}>
                            {(!loginUser || (loginUser && !loginUser.firstname)) && (
                                <FormattedMessage id="Header.LoginSignup" defaultMessage="Login / Signup" />
                            )}
                            {loginUser && loginUser.firstname && `Hello, ${loginUser.firstname}`}
                        </span>
                        {loginUser && loginUser.firstname && (
                            <Typography className="underline-text" onClick={() => gotoMyAccount()}>
                                <FormattedMessage id="Home.MyAccount" defaultMessage="My Account" />
                            </Typography>
                        )}
                    </span>
                </Grid>
                <Grid item xs={2} className="flex alignItemFlexStart justifyFlexEnd">
                    <IconButton onClick={() => closeMenu()} size="small">
                        <Close style={{ color: '#ffffff' }} />
                    </IconButton>
                </Grid>
            </Grid>

            <Grid container id="l2back" className="menuMobileBox">
                {menu && Object.values(menu).map(_renderMobileMenuL1List)}
            </Grid>
            <Grid container className="mobileCountryAndOtherContainer" justify="center">
                <Grid item xs={12}>
                    <CountryDropDown setAnchor={setAnchor} />
                </Grid>
                <Grid item xs={11} className="bottomBorderM" />
                <Grid item xs={11}>
                    <div className="MobileTopHederText" onClick={() => goToBirthDayClub()}>
                        <img src="/images/header/top/family.svg" className="topIcons" alt="Family club" />
                        <Typography component="span" className="topSpan">
                            <FormattedMessage id="Header.Top.FamilyClub" defaultMessage="Family Club" />
                        </Typography>
                    </div>
                </Grid>
                <Grid item xs={11}>
                    <div className="MobileTopHederText" onClick={() => gotoStoreLocator()}>
                        <img src="/images/header/second/Icon_Header_StoreLocator_Green.svg" alt="storeLocator" />
                        <Typography component="span" className="topSpan">
                            <FormattedMessage id="Header.StoreLocator" defaultMessage="Store Locator" />
                        </Typography>
                    </div>
                </Grid>
                <Grid item xs={11}>
                    <div className="MobileTopHederText" onClick={() => goToDeliveryPolicy()}>
                        <img
                            src="/images/header/top/Icon_Header_FreeShip.svg"
                            className="topIcons"
                            alt="freeDelivery"
                        />
                        <Typography component="span" className="topSpan">
                            <FormattedMessage
                                id="Header.Top.FreeStandardDelivery"
                                defaultMessage="Free Standard Delivery"
                            />
                        </Typography>
                    </div>
                </Grid>
                <Grid item xs={11}>
                    <div className="MobileTopHederText" onClick={() => goToDeliveryPolicy()}>
                        <img
                            src="/images/header/top/Icon_Header_GiftWrap.svg"
                            className="topIcons"
                            alt="FreeGiftWrapping"
                        />
                        <Typography component="span" className="topSpan">
                            <FormattedMessage id="Header.Top.FreeGiftWrapping" defaultMessage="Free Gift Wrapping" />
                        </Typography>
                    </div>
                </Grid>
                <Grid item xs={11}>
                    <div className="MobileTopHederText" onClick={() => goToReturnPolicy()}>
                        <img
                            src="/images/header/top/Icon_Header_FreeReturn.svg"
                            className="topIcons"
                            alt="FreeReturn"
                        />
                        <Typography component="span" className="topSpan noRightBorder">
                            <FormattedMessage id="Header.Top.FreeReturn" defaultMessage="Free Return" />
                        </Typography>
                    </div>
                </Grid>
                <Grid item xs={11}>
                    <div className="MobileTopHederText" onClick={() => goToHelpAndFAQ()}>
                        <img src="/images/header/top/Icon_Header_Help.svg" className="topIcons" alt="FreeReturn" />
                        <Typography component="span" className="topSpan noRightBorder">
                            <FormattedMessage id="Header.Top.NeedHelp" defaultMessage="Need Help" />
                        </Typography>
                    </div>
                </Grid>
                {loginUser && loginUser.firstname && (
                    <>
                        <Grid item xs={11} className="bottomBorderM" />
                        <Grid item xs={11}>
                            <div className="MobileTopHederText logout" onClick={() => onClickLogout()}>
                                <img
                                    src="/images/header/second/Icon_Logout.svg"
                                    className="topIcons"
                                    alt="FreeReturn"
                                />
                                <Typography component="span" className="topSpan noRightBorder">
                                    <FormattedMessage id="MyAccount.Logout" defaultMessage="Logout" />
                                </Typography>
                            </div>
                        </Grid>
                    </>
                )}

                <Grid item xs={11} className="bottomBorderM" />

                <Grid item xs={12} className="textAlignCenter m-t-20">
                    <SocialSharing />
                </Grid>
            </Grid>
        </Grid>
    );
}

export default withRouter(MobileNavigation);
