import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import '../../css/search.css';

function SearchProductList({ product, isLast, history, onClickClear }) {
    const state = useSelector(state => state);

    const gotoProductDetails = url_key => {
        onClickClear();
        return history.push(`/${state && state.auth && state.auth.store_locale}/products-details/${url_key}`);
    };

    return (
        <Link
            className="search-list-product-container"
            data-last={isLast}
            onClick={onClickClear}
            to={`/${state && state.auth && state.auth.store_locale}/products-details/${product.url_key}`}
        >
            <Grid container justify="center" alignItems="center" onClick={() => gotoProductDetails(product.url_key)}>
                <Grid item xs={9}>
                    <Typography className="search-list-product-name">
                        <span>{product && product.name}</span>
                    </Typography>
                    <Typography className="search-list-product-price">
                        <span>{product && product.currency && `${product.currency} ${product.price}`}</span>
                    </Typography>
                </Grid>
                <Grid item xs={3} className="text-align-end">
                    <img
                        src={
                            product &&
                            product.imageUrl &&
                            product.imageUrl.primaryimage &&
                            product.imageUrl.primaryimage[0]
                        }
                        alt="searchProduct"
                        className="search-list-img"
                    />
                </Grid>
            </Grid>
        </Link>
    );
}

export default withRouter(SearchProductList);
