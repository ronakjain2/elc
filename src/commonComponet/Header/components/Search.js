import { LinearProgress } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Clear from '@material-ui/icons/Clear';
import { actions as headerActions } from 'commonComponet/Header/redux/actions';
import useAutoCorrect from 'Hooks/useAutoCorrect';
import useClickOutside from 'Hooks/useClickOutside';
import useDebounce from 'Hooks/useDebounce';
import React, { useEffect, useRef, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import '../css/search.css';
import SearchProductList from './searchProductList';

// let setTimeVar = null;
function SearchInput({ history, setMobileSearch }) {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const [searchValue, setSearchValue] = useState('');
    const [showList, setShowList] = useState(false);
    const [searchLoader, setSearchLoader] = useState(false);
    const [suggestions, setSuggestions] = useState();

    const debouncedSearchQuery = useDebounce(searchValue, 300);
    const { lookup, search } = useAutoCorrect();

    const store_id = state && state.auth && state.auth.currentStore;
    const store_locale = state && state.auth && state.auth.store_locale;
    const language = state && state.auth && state.auth.language;

    // loader
    const searchL = state && state.menu && state.menu.searchLoader;
    const autoSuggestList = state && state.menu && state.menu.autoSuggestSearchProducts;
    const product_data = autoSuggestList && autoSuggestList.product_data;
    const category_data = autoSuggestList && autoSuggestList.category_data;

    useEffect(() => {
        setSearchLoader(searchL);
    }, [searchL]);

    useEffect(() => {
        if (debouncedSearchQuery) {
            dispatch(
                headerActions.getAutoSuggestSearchRequest({
                    q: debouncedSearchQuery,
                    storeid: store_id,
                    type: 'search',
                }),
            );
        } else {
            dispatch(headerActions.getAutoSuggestSearchRequestFailed());
        }
    }, [debouncedSearchQuery, dispatch, store_id]);

    const onSearchChange = event => {
        const { value } = event.target;
        if (value === '') {
            setShowList(false);
        } else {
            setShowList(true);
            if (!searchLoader) {
                setSearchLoader(true);
            }
        }
        setSearchValue(value);
        const suggestData = search(value, { depth: 6 });
        const len = suggestData.length;
        if (len > 0) {
            setSuggestions(suggestData.slice(0, 3));
        } else {
            const lookupData = lookup(value, { suggestionsLimit: 4 }).suggestions;
            setSuggestions(lookupData && lookupData.slice(0, 3));
        }
    };

    const onSearchKeyPress = event => {
        if (event.key === 'Enter') {
            history.push(`/${store_locale}/products/search?query=${searchValue}`);
            onClickClear();
        }
    };

    const onClickClear = () => {
        setShowList(false);
        setMobileSearch(false);
        setSearchValue('');
    };

    const onSuggestClick = suggestObj => {
        setSearchValue(suggestObj.word);
        setSuggestions(lookup(suggestObj.word, { suggestionsLimit: 4 }).suggestions);
    };

    let accountListDropdown = useRef(null);
    useClickOutside(accountListDropdown, () => onClickClear());

    return (
        <Grid ref={accountListDropdown} container className="position-relative">
            <Hidden only={['xs', 'sm']}>
                <TextField
                    placeholder={language === 'en' ? 'Search' : 'بحث'}
                    fullWidth
                    className="searchInputBox"
                    variant="outlined"
                    value={searchValue}
                    onKeyPress={event => onSearchKeyPress(event)}
                    onChange={onSearchChange}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="start">
                                <img
                                    height="22"
                                    width="22"
                                    src="/images/header/search.svg"
                                    alt="search"
                                    className="searchIcon"
                                />
                            </InputAdornment>
                        ),
                    }}
                />
            </Hidden>
            <Hidden only={['md', 'xl', 'lg']}>
                <TextField
                    placeholder={language === 'en' ? 'Search' : 'بحث'}
                    fullWidth
                    className="searchInputBoxM"
                    variant="outlined"
                    value={searchValue}
                    onKeyPress={event => onSearchKeyPress(event)}
                    onChange={onSearchChange}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <Grid container justify="center" alignItems="center">
                                    <div>
                                        <img
                                            height="22"
                                            width="22"
                                            src="/images/header/search.svg"
                                            alt="search"
                                            className="searchIcon"
                                        />
                                    </div>
                                    <div style={{ padding: '0px 5px' }}>
                                        <IconButton size="small" onClick={() => setMobileSearch(false)}>
                                            <Clear />
                                        </IconButton>
                                    </div>
                                </Grid>
                            </InputAdornment>
                        ),
                    }}
                />
            </Hidden>

            {/** ProductList */}
            {showList && (
                <Paper className="search-list-container">
                    <Grid container alignItems="center" className="p-3">
                        {/* <Grid item xs={12} className="close-icon-search">
                            <HighlightOff
                                onClick={() => {
                                    setShowList(false);
                                    setSearchValue('');
                                }}
                            />
                        </Grid> */}
                        {suggestions && suggestions.length > 0 && (
                            <Grid item xs={12} className="suggest-box">
                                <Typography className="search-section-title">
                                    <FormattedMessage id="search.DoYouMean?" defaultMessage="Do You Mean?" />
                                </Typography>
                                {suggestions.map(suggest => (
                                    <Link className="do-you-mean-content" onClick={() => onSuggestClick(suggest)}>
                                        <Typography>{suggest.word}</Typography>
                                    </Link>
                                ))}
                            </Grid>
                        )}
                        {category_data && category_data.name && (
                            <>
                                <Grid item xs={12}>
                                    <Typography className="search-section-title">
                                        <FormattedMessage id="search.category" defaultMessage="Category" />
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} className="category-grid">
                                    <Link
                                        onClick={onClickClear}
                                        to={`/${store_locale}/products/${category_data.url_key}`}
                                    >
                                        <Typography>{category_data.name}</Typography>
                                    </Link>
                                </Grid>
                            </>
                        )}
                        {product_data && Object.keys(product_data) && Object.keys(product_data).length > 0 && (
                            <>
                                <Grid item xs={12}>
                                    <Typography className="search-section-title">
                                        <FormattedMessage id="search.products" defaultMessage="Products" />
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} className="product-grid">
                                    {Object.keys(product_data).map((item, index) => (
                                        <Grid container key={`search_list_${index}`}>
                                            <SearchProductList
                                                product={product_data[item]}
                                                onClickClear={onClickClear}
                                                isLast={Object.keys(product_data).length === index + 1}
                                            />
                                        </Grid>
                                    ))}
                                </Grid>
                            </>
                        )}

                        {!searchLoader && !product_data && !category_data && (
                            <Grid item xs={12}>
                                <Typography className="no-search-product-available-text">
                                    <FormattedMessage
                                        id="PLP.NoDataAvailable"
                                        defaultMessage="Sorry.. No Data Available"
                                    />
                                </Typography>
                            </Grid>
                        )}
                    </Grid>
                    {searchLoader && <LinearProgress className="linear-progress" />}
                </Paper>
            )}
        </Grid>
    );
}

export default withRouter(SearchInput);
