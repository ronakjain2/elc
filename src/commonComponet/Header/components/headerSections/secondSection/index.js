import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import Menu from '@material-ui/icons/Menu';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import MyAccountList from '../../MyAccountList';
import SearchInput from '../../Search';
import './styles.css';

function HeaderSecondSection({ setAnchor, history }) {
    const [showMobileSearch, setMobileSearch] = useState(false);
    const [showMyAccountList, setShowMyAccountList] = useState(false);

    const state = useSelector(state => state);

    const LoginOrMyAccount = event => {
        if (state && state.auth && state.auth.loginUser && state.auth.loginUser.firstname) {
            setShowMyAccountList(!showMyAccountList);
        } else {
            return history.push(`/${state.auth.store_locale}/sign-in-register`);
        }
    };

    const gotoCart = () => history.push(`/${state && state.auth && state.auth.store_locale}/cart`);

    const gotoStoreLocator = () => history.push(`/${state && state.auth && state.auth.store_locale}/store-locator`);

    const cartCount = state && state.basket && state.basket.cartCount;

    return (
        <Grid container className="header-second-section">
            <Hidden only={['sm', 'xs']}>
                <Grid container justify="space-between" alignItems="center">
                    <div>
                        <Link to="/">
                            <img width="364" height="61" src="/images/logo/Logo_Group.png" alt="logo" />
                        </Link>
                    </div>

                    <Grid item xs={12} md={3} lg={4}>
                        <SearchInput setMobileSearch={setMobileSearch} />
                    </Grid>

                    <div>
                        <Grid container alignItems="center">
                            <div className="flex alignItemsCenter cursor-pointer" onClick={() => gotoStoreLocator()}>
                                <img
                                    src="/images/header/second/Icon_Header_StoreLocator.svg"
                                    width="24"
                                    height="24"
                                    alt="storeLocator"
                                />
                                <span className="secondText">
                                    <FormattedMessage id="Header.StoreLocator" defaultMessage="Store Locator" />
                                </span>
                            </div>
                            <div
                                className="flex alignItemsCenter cursor-pointer position-relative"
                                onClick={event => LoginOrMyAccount(event)}
                            >
                                <img
                                    height="20"
                                    width="20"
                                    src="/images/header/second/Icon_Header_User.svg"
                                    alt="storeLocator"
                                />
                                <span className="secondText">
                                    {state && state.auth && state.auth.loginUser && state.auth.loginUser.firstname && (
                                        <span className="showMyAccount">
                                            <FormattedMessage id="Home.MyAccount" defaultMessage="My Account" />
                                            <ArrowDropDown />
                                        </span>
                                    )}
                                    {state &&
                                        state.auth &&
                                        (!state.auth.loginUser || !state.auth.loginUser.firstname) && (
                                            <FormattedMessage id="Header.LoginSignup" defaultMessage="Login / Signup" />
                                        )}
                                </span>
                                <MyAccountList show={showMyAccountList} setshow={setShowMyAccountList} />
                            </div>
                            <div className="flex alignItemsCenter">
                                <span className="line" />
                            </div>
                            <div className="flex alignItemsCenter cursor-pointer" onClick={() => gotoCart()}>
                                <img
                                    height="20"
                                    width="20"
                                    src="/images/header/second/Icon_Header_Cart.svg"
                                    alt="storeLocator"
                                />
                                <span className="circle flex justifyCenter alignItemsCenter">
                                    <Typography component="span" className="basket-count">
                                        {cartCount}
                                    </Typography>
                                </span>
                            </div>
                        </Grid>
                    </div>
                </Grid>
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                {!showMobileSearch && (
                    <Grid container justify="center" alignItems="center">
                        <Grid item xs={12}>
                            <Grid container alignItems="center" justify="space-between">
                                <Grid item xs={7} className="flex alignItemCenter">
                                    <IconButton onClick={() => setAnchor(true)}>
                                        <Menu style={{ color: '#fff' }} />
                                    </IconButton>
                                    <div>
                                        <Link to="/">
                                            <img
                                                width="172"
                                                height="30"
                                                src="/images/logo/Logo_Group.png"
                                                alt="logo"
                                                className="mobileSize"
                                            />
                                        </Link>
                                    </div>
                                </Grid>
                                <Grid item xs={4}>
                                    <Grid item xs={12} className="flex alignItemCenter justifyFlexEnd">
                                        <IconButton onClick={() => setMobileSearch(true)}>
                                            <img
                                                src="/images/header/second/Icon_Mobile_Header_Search_Small.svg"
                                                alt="storelocator"
                                                className="second-header-iconM"
                                            />
                                        </IconButton>
                                        <div className="left-bordeM" />
                                        <IconButton onClick={() => gotoCart()}>
                                            <img
                                                src="/images/header/second/Icon_Header_Cart.svg"
                                                alt="storelocator"
                                                className="second-header-iconM"
                                            />
                                            <span className="circle flex justifyCenter alignItemCenter">
                                                <Typography component="span" className="basket-count">
                                                    {cartCount}
                                                </Typography>
                                            </span>
                                        </IconButton>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                )}
                {showMobileSearch && (
                    <Grid container justify="center" alignItems="center">
                        <Grid item xs={12}>
                            <SearchInput setMobileSearch={setMobileSearch} />
                        </Grid>
                    </Grid>
                )}
            </Hidden>
        </Grid>
    );
}

export default withRouter(HeaderSecondSection);
