import React from 'react';
import Hidden from '@material-ui/core/Hidden';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import './styles.css';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';
import { useSelector } from 'react-redux';
import CountryDropDown from '../../countryDropdown';

function HeaderTopSection({ history }) {
    const state = useSelector((state) => state);

    const store_locale = state && state.auth && state.auth.store_locale;
    const customer_type = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_type;

    const goToReturnPolicy = () => history.push(`/${store_locale}/return-policy`);

    const goToDeliveryPolicy = () => history.push(`/${store_locale}/delivery-policy`);

    const goToHelpAndFAQ = () => history.push(`/${store_locale}/help-and-faq`);

    const goToFamilyClub = () => {
        if (customer_type && customer_type === 'ecom_club_user') {
            return history.push(`/${store_locale}/my-club`);
        } else {
            return history.push(`/${store_locale}/family-club`);
        }
    };

    return (
        <Grid container alignItems="center" className="header-top-section">
            {/** Web */}
            <Hidden only={['xs', 'sm']}>
                <Grid container className="padding-l-r" justify="space-between">
                    <div>
                        <Grid container>
                            <div className="cursor-pointer" onClick={() => goToDeliveryPolicy()}>
                                <img
                                    src="/images/header/top/Icon_Header_FreeShip.svg"
                                    className="topIcons"
                                    alt="freeDelivery"
                                />
                                <Typography component="span" className="topSpan">
                                    <FormattedMessage
                                        id="Header.Top.FreeStandardDelivery"
                                        defaultMessage="Free Standard Delivery"
                                    />
                                </Typography>
                            </div>

                            <div
                                className="cursor-pointer" // onClick={() => goToDeliveryPolicy()}
                            >
                                <img
                                    src="/images/header/top/Icon_Header_GiftWrap.svg"
                                    className="topIcons"
                                    alt="FreeGiftWrapping"
                                />
                                <Typography component="span" className="topSpan">
                                    <FormattedMessage
                                        id="Header.Top.FreeGiftWrapping"
                                        defaultMessage="Free Gift Wrapping"
                                    />
                                </Typography>
                            </div>

                            <div className="cursor-pointer" onClick={() => goToReturnPolicy()}>
                                <img
                                    src="/images/header/top/Icon_Header_FreeReturn.svg"
                                    className="topIcons"
                                    alt="FreeReturn"
                                />
                                <Typography component="span" className="topSpan noRightBorder">
                                    <FormattedMessage id="Header.Top.FreeReturn" defaultMessage="Free Return" />
                                </Typography>
                            </div>
                        </Grid>
                    </div>

                    <div>
                        <Grid container justify="flex-end">
                            <div className="cursor-pointer" onClick={() => goToHelpAndFAQ()}>
                                <img src="/images/header/top/Icon_Header_Help.svg" className="topIcons" alt="help" />
                            </div>
                            <div className="flex justifyCenter alignItemsCenter">
                                <span className="RightBorder" />
                            </div>
                            <div className="cursor-pointer" onClick={() => goToFamilyClub()}>
                                <img src="/images/header/top/family.svg" className="topIcons" alt="BirthdayClub" />
                                <Typography component="span" className="topSpan">
                                    <FormattedMessage id="Header.Top.FamilyClub" defaultMessage="Family Club" />
                                </Typography>
                            </div>
                            <div>
                                <CountryDropDown />
                            </div>
                        </Grid>
                    </div>
                </Grid>
            </Hidden>
        </Grid>
    );
}

export default withRouter(HeaderTopSection);
