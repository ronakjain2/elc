import { actions as myWishlistActions } from 'components/Account/MyWishList/redux/actions';
import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import { actions as authActions } from 'components/Global/redux/actions';
import useClickOutside from 'Hooks/useClickOutside';
import React, { useRef } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import './myAccountList.css';

function MyAccountList({ history, show, setshow }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();
    let accountListDropdown = useRef(null);
    useClickOutside(accountListDropdown, () => setshow(false));
    const loginUser = state && state.auth && state.auth.loginUser;

    if (!show) return null;

    const onClickLogout = () => {
        dispatch(authActions.saveLoginUser(null));
        dispatch(authActions.saveQuoteIdRequest(null));
        dispatch(authActions.saveGuestUser(null));
        dispatch(basketActions.saveCartCountRequest(0));
        dispatch(myWishlistActions.addWishListItemRequestSuccess([]));
        history.push(`/${state && state.auth && state.auth.store_locale}/sign-in-register`);
    };

    const gotoMyAccount = () => {
        history.push(`/${state && state.auth && state.auth.store_locale}/my-account`);
    };

    const gotoProfile = () => {
        history.push(`/${state && state.auth && state.auth.store_locale}/profile`);
    };

    const gotoMyAddresses = () => {
        history.push(`/${state && state.auth && state.auth.store_locale}/my-addresses`);
    };

    const gotoWishList = () => {
        history.push(`/${state && state.auth && state.auth.store_locale}/wish-list`);
    };

    const gotoMyOrder = () => {
        history.push(`/${state && state.auth && state.auth.store_locale}/order-history`);
    };

    const gotoMyCards = () => {
        history.push(`/${state && state.auth && state.auth.store_locale}/my-cards`);
    };

    const gotoMyClub = () => {
        history.push(`/${state && state.auth && state.auth.store_locale}/my-club`);
    };

    return (
        <div ref={accountListDropdown} className="myAccoutDropDown" id="menuscontainer">
            <p className="userName" onClick={() => gotoMyAccount()}>
                Hi,
                {loginUser && loginUser.firstname}
            </p>
            <p className="subText border_Spacing" onClick={() => gotoProfile()}>
                <FormattedMessage id="MyAccount.MyProfile" defaultMessage="My Profile" />
            </p>
            <p className="subText border_Spacing" onClick={() => gotoMyOrder()}>
                <FormattedMessage id="MyAccount.MyOrder" defaultMessage="My Order" />
            </p>
            <p className="subText border_Spacing" onClick={() => gotoWishList()}>
                <FormattedMessage id="MyAccount.WishList" defaultMessage="Wishlist" />
            </p>
            <p className="subText border_Spacing" onClick={() => gotoMyAddresses()}>
                <FormattedMessage id="MyAccount.MyAddresses" defaultMessage="My Addresses" />
            </p>
            <p className="subText border_Spacing" onClick={() => gotoMyCards()}>
                <FormattedMessage id="MyAccount.MyCards.Text" defaultMessage="My Cards" />
            </p>
            <p className="subText border_Spacing" onClick={() => gotoMyClub()}>
                <FormattedMessage id="MyAccount.MyClub.Text" defaultMessage="My Club" />
            </p>
            <p className="userLogout" onClick={() => onClickLogout()}>
                <img src="/images/header/second/Icon_Logout.svg" className="logOutIcon" alt="Logout" />
                <FormattedMessage id="MyAccount.Logout" defaultMessage="Logout" />
            </p>
        </div>
    );
}

export default withRouter(MyAccountList);
