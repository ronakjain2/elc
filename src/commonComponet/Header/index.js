import { InjectIntlContext } from '@comparaonline/react-intl-hooks';
import Drawer from '@material-ui/core/Drawer';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Spinner from 'commonComponet/Spinner';
import React, { lazy, Suspense, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './css/header.css';
import { actions as MenuActions } from './redux/actions';

const PageTitles = lazy(() => import('commonComponet/PageTitles'));
const MobileNavigation = lazy(() => import('./components/nevigations/mobileNavigation'));
const HeaderTopSection = lazy(() => import('./components/headerSections/topSection'));
const HeaderSecondSection = lazy(() => import('./components/headerSections/secondSection'));
const WebNavigation = lazy(() => import('./components/nevigations/webNavigation'));

export default function Header() {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const [anchor, setAnchor] = useState(false);

    const toggleDrawer = open => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setAnchor(open);
    };

    useEffect(() => {
        try {
            dispatch(
                MenuActions.getMenuRequest({
                    storeId: state.auth.currentStore,
                }),
            );

            //  changeBrowserUrl(state.auth.currentStore);
        } catch (err) {}
    }, [dispatch, state.auth.currentStore]);

    const language = state && state.auth && state.auth.language;

    return (
        <Suspense fallback={<Spinner />}>
            <header>
                <Grid container>
                    {/** PageTitles */}
                    <InjectIntlContext>
                        <PageTitles />
                    </InjectIntlContext>
                    {/** End PageTitle */}

                    {/** Header Top */}
                    <Hidden only={['xs', 'sm']}>
                        <Grid item xs={12}>
                            <HeaderTopSection />
                        </Grid>
                    </Hidden>
                    {/** End Header Top */}

                    {/** Second Header */}
                    <HeaderSecondSection setAnchor={setAnchor} />
                    {/** End Second Header */}

                    <Grid item xs={12}>
                        <Hidden only={['xs', 'sm']}>
                            <WebNavigation />
                        </Hidden>
                        <Hidden only={['lg', 'xl', 'md']}>
                            <React.Fragment key="left">
                                <Drawer
                                    anchor={language === 'en' ? 'left' : 'right'}
                                    open={anchor}
                                    onClose={toggleDrawer(anchor, false)}
                                    style={{ zIndex: 100000000 }}
                                >
                                    <MobileNavigation setAnchor={setAnchor} />
                                </Drawer>
                            </React.Fragment>
                        </Hidden>
                    </Grid>
                </Grid>
            </header>
        </Suspense>
    );
}
