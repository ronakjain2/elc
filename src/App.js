import SimpleBackdrop from 'commonComponet/BackDrop';
import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Router, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { initializeGTM } from 'services/GTMservice';
import './css/App.css';
import { history } from './redux/store';
import AppRouter from './router';
import { actions as GlobalActions } from 'components/Global/redux/actions';

function App() {
    return <MainApp />;
}
function MainApp() {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const [isLoading, setLoading] = useState(true);
    const [internatIssue] = useState(window.navigator.onLine);

    const quote_id = state && state.auth && state.auth.quote_id;
    const store_id = state && state.auth && state.auth.currentStore;

    function fakeRequest() {
        return new Promise(resolve => setTimeout(() => resolve(), 2));
    }

    // eslint-disable-next-line
    console.warn = (message, ...args) => {};
    useEffect(() => {
        try {
            if (state.auth.language === 'ar') {
                document.getElementById('dir').classList.add('u-RTL');
                document.getElementById('dir').lang = 'ar';
                document.getElementById('dir').dir = 'rtl';
            } else {
                document.getElementById('dir').lang = 'en';
                document.getElementById('dir').classList.remove('u-RTL');
                document.getElementById('dir').removeAttribute('dir');
            }

            initializeGTM();
        } catch (err) {}
    }, [state.auth.language]);

    useEffect(() => {
        // Disabling Country Switch & Reservation Loader
        dispatch(GlobalActions.disableLoaders());
    }, [dispatch]);

    useEffect(() => {
        fakeRequest().then(() => {
            const el = document.querySelector('.loader-container');
            if (el) {
                el.remove();
                setLoading(!isLoading);
            }
        });
    }, [isLoading]);

    useEffect(() => {
        dispatch(
            basketActions.getCartCountReq({
                quote_id,
                store_id,
            }),
        );
    }, [dispatch, quote_id, store_id]);

    useEffect(() => {
        dispatch(GlobalActions.getFCConfigReq());
    }, [dispatch]);

    if (isLoading) {
        return null;
    }

    return (
        <>
            <ToastContainer />
            <SimpleBackdrop open={state && state.auth && state.auth.loader} netIssue={!internatIssue} />

            <Router history={history}>
                <Switch>
                    <Route component={AppRouter} />
                </Switch>
            </Router>
        </>
    );
}

export default App;
