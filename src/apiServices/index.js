import axios from 'axios';
import { BACKEND_URL } from '../environments';

const axiosInstance = axios.create({
    baseURL: BACKEND_URL,
});

axiosInstance.interceptors.request.use(
    (config) => {
        // Do something before request is sent

        config.headers.Authorization = 'Bearer exn50dak2a5iahy02hawo5il0y6j25ct';
        return config;
    },
    (error) => {
        Promise.reject(error);
    },
);

export default axiosInstance;
