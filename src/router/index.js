import CheckoutHeader from 'commonComponet/CheckoutHeader';
import CountryPOP from 'commonComponet/countryPop';
import FCPopup from 'commonComponet/FCPopUp/index';
import Footer from 'commonComponet/Footer';
import Header from 'commonComponet/Header';
import { changeBrowserUrl } from 'commonComponet/Header/utils.js';
import ReservationPop from 'commonComponet/ReservationPop';
import CancelOrder from 'components/Account/CancelOrder';
import CommunicationPerderences from 'components/Account/CommunicationPerderences';
import MyAccount from 'components/Account/MyAccount';
import MyAddresses from 'components/Account/MyAddresses';
import MyCards from 'components/Account/MyCards';
import MyClub from 'components/Account/MyClub';
import MyOrder from 'components/Account/MyOrder';
import MyOrderDetails from 'components/Account/MyOrderDetails';
import MyProfile from 'components/Account/MyProfile';
import MyWishList from 'components/Account/MyWishList';
import { actions as myWishlistAction } from 'components/Account/MyWishList/redux/actions';
import ReturnOrder from 'components/Account/ReturnOrder';
import BirthdayClub from 'components/BirthdayClub';
import Basket from 'components/Cart/Basket';
import { actions as basketAction } from 'components/Cart/Basket/redux/actions';
import CheckoutDeliveryDetails from 'components/Cart/Checkout/Delivery';
import CheckoutPayment from 'components/Cart/Checkout/Payment';
import PaymentProcessing from 'components/Cart/Checkout/Payment/Components/PaymentProcessing';
import CheckoutThankYouOrSorry from 'components/Cart/Checkout/ThankYouOrSorry';
import ElcCampaignTemplate from 'components/ELCCampaign';
import FamilyClub from 'components/FamilyClub';
import { actions as authActions, actions as globalActions } from 'components/Global/redux/actions';
import Home from 'components/Home';
import Landing from 'components/LandingPage';
import PasswordReset from 'components/PasswordReset';
import PDP from 'components/PDP';
import PLP from 'components/PLP';
import ShopByBrand from 'components/ShopByBrand';
import SignInSignUp from 'components/SignIn_SignUp';
import SmsConsent from 'components/SmsConsent';
import AboutUs from 'components/staticPages/components/AbountUs';
import ContactUs from 'components/staticPages/components/ContactUs';
import DeliveryPolicy from 'components/staticPages/components/DeliveryPolicy';
import HelpAndFAQ from 'components/staticPages/components/helpAndFAQ';
import PrivacyPolicy from 'components/staticPages/components/PrivacyPolicy';
import ReturnPolicy from 'components/staticPages/components/ReturnPolicy';
import TermAndConditions from 'components/staticPages/components/TermAndCondition';
import StoreLocator from 'components/StoreLocator';
import React, { useEffect, useState } from 'react';
import { addLocaleData, IntlProvider } from 'react-intl';
import ar from 'react-intl/locale-data/ar';
import en from 'react-intl/locale-data/en';
import { useDispatch, useSelector } from 'react-redux';
import { Route, useLocation } from 'react-router-dom';
import messages_ar from 'translations/ar.json';
import messages_en from 'translations/en.json';
import CheckoutLogin from '../components/Cart/Checkout/Login';

let reservationInterval = null;
export default function AppRouter() {
    const location = useLocation();

    addLocaleData([...en, ...ar]);

    const [isOpen, setOpen] = useState(false);
    const [isShowCountryPopup, setShowCountryPopup] = useState(true);

    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const [showReservationPop, setReservationPop] = useState(false);

    const loginUser = state && state.auth && state.auth.loginUser;
    const guestUser = state && state.auth && state.auth.guestUser;
    const storeId = state && state.auth && state.auth.currentStore;
    const quote_id = state && state.auth && state.auth.quote_id;
    const fc_status = state && state.auth && state.auth.fc_status;
    let quote_id_created_time = state && state.auth && state.auth.quote_id_created_time;

    const reservationConfig = state && state.auth && state.auth.reservationConfig;
    const reservationStartTime = state && state.auth && state.auth.reservationStartTime;

    const messages = {
        ar: messages_ar,
        en: messages_en,
    };

    const language = state && state.auth && state.auth.language;
    const createAuthObjectOnLangAndCountryChange = (country, lang, auth) => {
        const language = lang;
        const { quote_id } = auth;

        const obj = {
            storeInfo: {},
            storeData: '',
            storeChnageObj: '',
        };
        if (language === 'en') {
            if (country === 'KSA') {
                obj.storeInfo = {
                    language: 'en',
                    currentStore: 2,
                    country: 'KSA',
                    region: 'saudi-en',
                    store_locale: 'saudi-en',
                };
                obj.storeData = 'KSA_en';
                obj.storeChnageObj = {
                    store_id: 2,
                    quote_id,
                };
            }
            if (country === 'UAE') {
                obj.storeInfo = {
                    language: 'en',
                    currentStore: 4,
                    country: 'UAE',
                    region: 'uae-en',
                    store_locale: 'uae-en',
                };
                obj.storeData = 'UAE_en';
                obj.storeChnageObj = {
                    store_id: 4,
                    quote_id,
                };
            }
        }

        if (language === 'ar') {
            if (country === 'KSA') {
                obj.storeInfo = {
                    language: 'ar',
                    currentStore: 1,
                    country: 'KSA',
                    region: 'saudi-ar',
                    store_locale: 'saudi-ar',
                };
                obj.storeData = 'KSA_ar';
                obj.storeChnageObj = {
                    store_id: 1,
                    quote_id,
                };
            }
            if (country === 'UAE') {
                obj.storeInfo = {
                    language: 'ar',
                    currentStore: 3,
                    country: 'UAE',
                    region: 'uae-ar',
                    store_locale: 'uae-ar',
                };
                obj.storeData = 'UAE_ar';
                obj.storeChnageObj = {
                    store_id: 3,
                    quote_id,
                };
            }
        }

        return obj;
    };

    // default country selection Pop
    useEffect(() => {
        try {
            if (state.auth.defaultCountryUsed) {
                setOpen(true);
            }
        } catch (err) {}
    }, [state.auth.defaultCountryUsed]);

    const initializeLanguageCountrySwitch = () => {
        try {
            let currentPath = window.location.href;
            let splitpath = currentPath.split('/');
            if (splitpath.length >= 5) {
                setShowCountryPopup(false);
            }

            let store_locale = splitpath[3];
            let selectedCountry = '';
            let selectedLanguage = '';
            if (splitpath[3]) {
                let temp = store_locale.split('-');
                if (temp[0]) {
                    if (temp[0] === 'saudi') {
                        selectedCountry = 'KSA';
                    } else {
                        selectedCountry = temp[0].toUpperCase();
                    }
                }
                selectedLanguage = temp[1];
            }
            if (splitpath.length !== 0 && splitpath.length > 4) {
                if (selectedLanguage !== state.auth.language || selectedCountry !== state.auth.country) {
                    const obj = createAuthObjectOnLangAndCountryChange(
                        selectedCountry,
                        selectedLanguage,
                        state && state.auth,
                    );
                    dispatch(globalActions.getStoreInfoRequest(obj));
                    changeBrowserUrl(obj && obj.storeInfo && obj.storeInfo.currentStore);
                }
            }
        } catch (err) {}
    };

    useEffect(initializeLanguageCountrySwitch, [state.auth.country, state.auth.language]);

    //Reservation
    useEffect(() => {
        dispatch(authActions.getReservationConfigRequest());
    }, [dispatch]);

    //wishList
    useEffect(() => {
        if (loginUser && loginUser.customer_id) {
            dispatch(
                myWishlistAction.getAllWishListRequest({
                    store_id: storeId,
                    customerid: loginUser.customer_id,
                    quote_id: quote_id,
                }),
            );
        }
    }, [dispatch, storeId, loginUser, quote_id]);

    // guest cart
    useEffect(() => {
        try {
            if (!loginUser) {
                if (!guestUser || !guestUser.guestId || guestUser.guestId === '') {
                    dispatch(authActions.getGuestCartRequest(storeId));
                }
            }
        } catch (err) {}
    }, [dispatch, loginUser, guestUser, storeId]);

    //Remove quote id

    useEffect(() => {
        const interval = setInterval(() => {
            try {
                if (quote_id_created_time) {
                    let quoteIdCreatedTime = new Date(quote_id_created_time);

                    quoteIdCreatedTime.setHours(quoteIdCreatedTime.getHours() + 12);
                    quoteIdCreatedTime = new Date(quoteIdCreatedTime);

                    if (quoteIdCreatedTime <= new Date()) {
                        if (loginUser && loginUser.customer_id) {
                            //TODO
                        } else {
                            dispatch(authActions.saveQuoteIdRequest(null));
                            dispatch(authActions.removeReservationRequestSuccess());
                            dispatch(basketAction.saveCartCountRequest(0));
                            dispatch(authActions.saveGuestUser(null));
                        }
                    }
                }
            } catch (err) {}
        }, 60000);
        return () => clearInterval(interval);
    }, [dispatch, loginUser, quote_id_created_time]);

    // check which header we need to used;
    const isCheckoutProcess = () => {
        const pathArray = (location && location.pathname && location.pathname.split('/')) || [];
        const lastPathName = pathArray[pathArray.length - 1];
        if (
            lastPathName === 'checkout-login' ||
            lastPathName === 'delivery-details' ||
            lastPathName === 'checkout-payment' ||
            lastPathName === 'payment-processing' ||
            lastPathName === 'order-summary'
        ) {
            return true;
        }
        return false;
    };

    //Reservation Logic
    useEffect(() => {
        try {
            if (
                reservationConfig &&
                Object.keys(reservationConfig).length > 0 &&
                reservationConfig.reservation_status === 'Enable'
            ) {
                if (reservationStartTime) {
                    let reservationStartDate = new Date(reservationStartTime);
                    let reservationLimitDate = new Date(reservationStartTime);

                    let reservation20MinDate = new Date(reservationStartTime);

                    reservationStartDate.setMinutes(
                        reservationStartDate.getMinutes() +
                            (parseInt(reservationConfig.reservation_checkout_time || 10) - 2),
                    );
                    reservationStartDate = new Date(reservationStartDate);

                    reservationLimitDate.setMinutes(reservationLimitDate.getMinutes() + 40);
                    reservationLimitDate = new Date(reservationLimitDate);

                    reservation20MinDate.setMinutes(
                        reservation20MinDate.getMinutes() + parseInt(reservationConfig.reservation_checkout_time || 10),
                    );
                    reservation20MinDate = new Date(reservation20MinDate);

                    //Close POP after reservation_checkout_time completed
                    if (reservation20MinDate <= new Date()) {
                        if (reservationInterval) {
                            clearInterval(reservationInterval);
                        }

                        if (showReservationPop) {
                            setReservationPop(false);
                        }

                        dispatch(
                            authActions.removeReservationRequest({
                                store_id: storeId,
                                quote_id: quote_id,
                                reservation_type: 'checkout',
                            }),
                        );
                    }

                    if (reservationLimitDate <= new Date()) {
                        if (reservationInterval) {
                            clearInterval(reservationInterval);
                        }
                        if (showReservationPop) {
                            setReservationPop(false);
                        }
                    } else {
                        if (reservationStartDate > new Date() && showReservationPop) {
                            setReservationPop(false);
                        }

                        reservationInterval = setInterval(() => {
                            if (!reservationStartDate) {
                                clearInterval(reservationInterval);
                            }
                            if (reservationStartDate <= new Date() && !showReservationPop) {
                                setReservationPop(true);
                            }
                        }, 1000);
                    }
                } else {
                    if (reservationInterval) {
                        clearInterval(reservationInterval);
                    }
                    if (showReservationPop) {
                        setReservationPop(false);
                    }
                }
            } else {
                if (showReservationPop) {
                    setReservationPop(false);
                }
            }
        } catch (err) {}
    }, [reservationConfig, reservationStartTime, showReservationPop, dispatch, storeId, quote_id]);

    return (
        <div>
            <IntlProvider
                locale={language}
                messages={messages[language]}
                onError={err => {
                    if (err && err.code === 'MISSING_TRANSLATION') {
                        console.warn('Missing translation', err && err.message);
                        return;
                    }
                }}
            >
                <>
                    {isShowCountryPopup && <CountryPOP isOpen={isOpen} setOpen={setOpen} />}
                    <ReservationPop isOpen={showReservationPop} setOpen={setReservationPop} />
                    {fc_status && <FCPopup />}
                    {!isCheckoutProcess() && <Header />}
                    {isCheckoutProcess() && <CheckoutHeader />}
                    <div className="elc-container">
                        {/** Home */}
                        <Route path="/home" component={Home} />
                        <Route exact path="/" component={Home} />
                        <Route exact path="/:locale" component={Home} />
                        <Route exact path="/:locale/home" component={Home} />
                        {/** Sign In and Sign Up */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/sign-in-register"
                            component={SignInSignUp}
                        />
                        {/** PLP */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/products/:category_path"
                            component={PLP}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/landing/:category_l1/:category_l2?/:category_l3?"
                            component={Landing}
                        />
                        {/** PDP */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/products-details/:category"
                            component={PDP}
                        />

                        {/** static pages */}
                        {/** return policy */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/return-policy"
                            component={ReturnPolicy}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/delivery-policy"
                            component={DeliveryPolicy}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/help-and-faq"
                            component={HelpAndFAQ}
                        />
                        <Route path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/about-us" component={AboutUs} />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/terms-and-conditions"
                            component={TermAndConditions}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/privacy-policy"
                            component={PrivacyPolicy}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/contact-us"
                            component={ContactUs}
                        />
                        <Route
                            exact
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/birth-day-club"
                            component={BirthdayClub}
                        />
                        <Route
                            exact
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/family-club"
                            component={FamilyClub}
                        />
                        {/** end static pages */}

                        {/** Cart */}
                        <Route path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/cart" component={Basket} />
                        {/** Checkout */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/checkout-login"
                            component={CheckoutLogin}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/delivery-details"
                            component={CheckoutDeliveryDetails}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/checkout-payment"
                            component={CheckoutPayment}
                        />
                        <Route
                            exact
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/order-summary"
                            component={CheckoutThankYouOrSorry}
                        />
                        {/** End Checkout */}

                        {/** My Account */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/my-account"
                            render={props => <MyAccount {...props} />}
                        />
                        <Route path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/profile" component={MyProfile} />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/my-addresses"
                            component={MyAddresses}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/wish-list"
                            component={MyWishList}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/order-history"
                            component={MyOrder}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/order-details"
                            component={MyOrderDetails}
                        />
                        <Route path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/my-cards" component={MyCards} />
                        <Route path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/my-club" component={MyClub} />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/communication-perferences"
                            component={CommunicationPerderences}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/return-order"
                            component={ReturnOrder}
                        />
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/cancel-order"
                            component={CancelOrder}
                        />

                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/browse-all-brands"
                            component={ShopByBrand}
                        />
                        {/** End My Account */}
                        {/* Store Locator */}
                        <Route
                            exact
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/store-locator"
                            component={StoreLocator}
                        />
                        {/** Password Reset */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/password-rest"
                            component={PasswordReset}
                        />
                        {/** End Password Reset */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/campaign/:campaign_key_config"
                            component={ElcCampaignTemplate}
                        />

                        {/** Payfort Merchant2.0 */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/payment-processing"
                            component={PaymentProcessing}
                        />
                        {/** End Payfort Merchant2.0 */}
                        {/* SMS Consent Form */}
                        <Route
                            path="/:locale(en|ar|uae-en|uae-ar|saudi-en|saudi-ar)/consent-form"
                            component={SmsConsent}
                        />
                    </div>
                    <Footer isCheckoutProcess={isCheckoutProcess} />
                </>
            </IntlProvider>
        </div>
    );
}
