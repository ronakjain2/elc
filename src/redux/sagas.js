import footerSagas from 'commonComponet/Footer/redux/sagas';
import menuSagas from 'commonComponet/Header/redux/sagas';
import cancelOrderSagas from 'components/Account/CancelOrder/redux/sagas';
import myAddressesSagas from 'components/Account/MyAddresses/redux/sagas';
import myClubSagas from 'components/Account/MyClub/redux/sagas';
import myOrderSagas from 'components/Account/MyOrder/redux/sagas';
import myProfileSagas from 'components/Account/MyProfile/redux/sagas';
import myWishlistSagas from 'components/Account/MyWishList/redux/sagas';
import returnOrderSagas from 'components/Account/ReturnOrder/redux/sagas';
import birthdayClubSagas from 'components/BirthdayClub/redux/sagas';
import basketSagas from 'components/Cart/Basket/redux/sagas';
import checkoutDeliverySagas from 'components/Cart/Checkout/Delivery/redux/sagas';
import checkoutLoginSagas from 'components/Cart/Checkout/Login/redux/sagas';
import checkoutPaymentSagas from 'components/Cart/Checkout/Payment/redux/sagas';
import checkoutThankYouSagas from 'components/Cart/Checkout/ThankYouOrSorry/redux/sagas';
import elcCampaignTemplateSagas from 'components/ELCCampaign/redux/sagas';
import familyClubSagas from 'components/FamilyClub/redux/sagas';
import authSagas from 'components/Global/redux/sagas';
import homePageSagas from 'components/Home/redux/sagas';
import landingPageSagas from 'components/LandingPage/redux/sagas';
import passwordResetSagas from 'components/PasswordReset/redux/sagas';
import PDPSagas from 'components/PDP/redux/sagas';
import PLPSagas from 'components/PLP/redux/sagas';
import shopbybrandsagas from 'components/ShopByBrand/redux/sagas';
import signInSignUpSagas from 'components/SignIn_SignUp/redux/sagas';
import smsConsentSagas from 'components/SmsConsent/redux/sagas';
import staticPageSagas from 'components/staticPages/redux/sagas';
import storeLocatorSagas from 'components/StoreLocator/redux/sagas';
import { all, fork, select, takeEvery } from 'redux-saga/effects';

function* watchAndLog() {
    yield takeEvery('*', function* logger(action) {
        const state = yield select();
        console.debug('action', action);
        console.debug('state after', state);
    });
}

export default function* root() {
    yield all([
        fork(watchAndLog),
        fork(authSagas),
        fork(homePageSagas),
        fork(PLPSagas),
        fork(menuSagas),
        fork(PDPSagas),
        fork(signInSignUpSagas),
        fork(staticPageSagas),
        fork(basketSagas),
        fork(footerSagas),
        fork(checkoutLoginSagas),
        fork(checkoutDeliverySagas),
        fork(checkoutPaymentSagas),
        fork(checkoutThankYouSagas),
        fork(myAddressesSagas),
        fork(myClubSagas),
        fork(myProfileSagas),
        fork(myWishlistSagas),
        fork(birthdayClubSagas),
        fork(familyClubSagas),
        fork(myOrderSagas),
        fork(returnOrderSagas),
        fork(storeLocatorSagas),
        fork(cancelOrderSagas),
        fork(passwordResetSagas),
        fork(shopbybrandsagas),
        fork(elcCampaignTemplateSagas),
        fork(smsConsentSagas),
        fork(landingPageSagas),
    ]);
}
