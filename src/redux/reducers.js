import footer from 'commonComponet/Footer/redux/reducer';
import menu from 'commonComponet/Header/redux/reducer';
import cancelOrder from 'components/Account/CancelOrder/redux/reducer';
import myAddresses from 'components/Account/MyAddresses/redux/reducer';
import myClub from 'components/Account/MyClub/redux/reducer';
import myOrder from 'components/Account/MyOrder/redux/reducer';
import myProfile from 'components/Account/MyProfile/redux/reducer';
import myWishlist from 'components/Account/MyWishList/redux/reducer';
import returnOrder from 'components/Account/ReturnOrder/redux/reducer';
import birthdayClub from 'components/BirthdayClub/redux/reducer';
import basket from 'components/Cart/Basket/redux/reducer';
import checkoutDelivery from 'components/Cart/Checkout/Delivery/redux/reducer';
import checkoutLogin from 'components/Cart/Checkout/Login/redux/reducer';
import checkoutPayment from 'components/Cart/Checkout/Payment/redux/reducer';
import checkoutThankYou from 'components/Cart/Checkout/ThankYouOrSorry/redux/reducer';
import elcCampaignTemplateReducer from 'components/ELCCampaign/redux/reducers';
import familyClub from 'components/FamilyClub/redux/reducer';
import auth from 'components/Global/redux/reduces';
import homePage from 'components/Home/redux/reducer';
import landingPage from 'components/LandingPage/redux/reducer';
import passwordReset from 'components/PasswordReset/redux/reducer';
import PDP from 'components/PDP/redux/reducer';
import PLP from 'components/PLP/redux/reducer';
import shopbybrandreducer from 'components/ShopByBrand/redux/reducers';
import signInSignUp from 'components/SignIn_SignUp/redux/reducer';
import smsConsent from 'components/SmsConsent/redux/reducer';
import staticPage from 'components/staticPages/redux/reducer';
import storeLocator from 'components/StoreLocator/redux/reducer';
import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';

const createRootReducer = history =>
    combineReducers({
        router: connectRouter(history),
        homePage,
        auth,
        PLP,
        menu,
        PDP,
        signInSignUp,
        staticPage,
        basket,
        footer,
        checkoutLogin,
        checkoutDelivery,
        checkoutPayment,
        checkoutThankYou,
        myAddresses,
        myProfile,
        myWishlist,
        birthdayClub,
        familyClub,
        myOrder,
        myClub,
        returnOrder,
        storeLocator,
        cancelOrder,
        passwordReset,
        shopbybrandreducer,
        elcCampaignTemplateReducer,
        smsConsent,
        landingPage,
    });

export default createRootReducer;
