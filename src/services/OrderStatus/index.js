export const orderStatus = {
    delivered: 'DELIVERED',
    confirmed: 'CONFIRMED',
    Created: 'CREATED',
    PaymentPending: 'PAYMENT PENDING',
    PaymentSuccess: 'PAYMENT SUCCESS',
    Accepted: 'ACCEPTED',
    Processing: 'PROCESSING',
    Dispatched: 'DISPATCHED',
    Cancelled: 'CANCELED',
    open: 'OPEN',
    complete: 'COMPLETE',
};
