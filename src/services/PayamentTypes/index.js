export const paymentTypes = {
    PayByCard: 'Pay By Card',
    CashOnDelivery: 'Cash On Delivery',
    ApplePay: 'Apple Pay',
};
