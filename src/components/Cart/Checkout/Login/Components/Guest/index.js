import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import { isEmail, scrollToElement } from 'components/SignIn_SignUp/utils';
import { withRouter } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { actions as checkoutLoginActions } from 'components/Cart/Checkout/Login/redux/actions';

function CheckoutGuest({ history }) {
    const state = useSelector((state) => state);

    const dispatch = useDispatch();

    const store_locale = state && state.auth && state.auth.store_locale;

    const [values, setValues] = useState({
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
    });

    const [isValidate, setIsValidate] = useState(false);

    const onChangeHandler = (name) => (event) => {
        if (name === 'firstName' || name === 'lastName') {
            const re = /^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g;
            if (event.target.value && !re.test(event.target.value)) {
                return '';
            }
        }
        const re_1 = /^(?!\s*$).+/g;
        if (event.target.value && !re_1.test(event.target.value)) {
            return '';
        }
        setValues({ ...values, [name]: event.target.value });
    };

    const onSubmit = () => {
        setIsValidate(true);
        if (
            !values.firstName ||
            values.firstName === '' ||
            !values.lastName ||
            values.lastName === '' ||
            !values.email ||
            values.email === '' ||
            !isEmail(values.email)
        ) {
            scrollToElement('.input-error');
            return;
        }
        dispatch(
            checkoutLoginActions.saveGuestDetailsRequest({
                ...values,
            }),
        );
        return history.push(`/${store_locale}/delivery-details`);
    };

    return (
        <Grid container>
            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.FirstName" defaultMessage="First Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.firstName}
                    onChange={onChangeHandler('firstName')}
                    fullWidth
                    className="inputBox"
                    error={isValidate && values.firstName === ''}
                    inputProps={{ maxLength: 20 }}
                />
                {isValidate && values.firstName === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.FirstName.Error" defaultMessage="Please enter first name" />
                    </FormHelperText>
                )}
            </Grid>
            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.LastName" defaultMessage="Last Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.lastName}
                    onChange={onChangeHandler('lastName')}
                    fullWidth
                    className="inputBox"
                    error={isValidate && values.lastName === ''}
                    inputProps={{ maxLength: 19 }}
                />
                {isValidate && values.lastName === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.LastName.Error" defaultMessage="Please enter last name" />
                    </FormHelperText>
                )}
            </Grid>
            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSingUp.Email" defaultMessage="Email*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.email}
                    onChange={onChangeHandler('email')}
                    fullWidth
                    className="inputBox"
                    error={isValidate && !isEmail(values.email)}
                />
                {isValidate && !isEmail(values.email) && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Email.Error" defaultMessage="Please Enter Valid Email Address" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="arabic-right">
                <Button className="login-signup-login-button m-t-15" onClick={() => onSubmit()}>
                    <FormattedMessage id="Checkout.GuestAndProceed" defaultMessage="Guest & Proceed" />
                </Button>
            </Grid>
        </Grid>
    );
}

export default withRouter(CheckoutGuest);
