import React, { useState, lazy, Suspense, useCallback, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';
import { FormattedMessage } from 'react-intl';
import PhoneNumber from 'commonComponet/PhoneNumber';
import { useSelector, useDispatch } from 'react-redux';
import { isEmail, scrollToElement } from 'components/SignIn_SignUp/utils';
import { actions as checkoutLoginAction } from 'components/Cart/Checkout/Login/redux/actions';
import CircularProgress from '@material-ui/core/CircularProgress';
const ForgotPassword = lazy(() => import('components/SignIn_SignUp/components/Login/POP/ForgotPassword'));

export default function ExistingCustomer() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const guestUser = state && state.auth && state.auth.guestUser;
    const loginLoader = state && state.checkoutLogin && state.checkoutLogin.loginLoader;
    const quote_id = state && state.auth && state.auth.quote_id;
    const reservationLoader = state && state.auth && state.auth.reservationLoader;

    const [values, setValues] = useState({
        email: '',
        password: '',
        carrier_code: '',
        phoneNumber: '',
    });
    const [open, setOpen] = useState(false);
    const [isValidate, setIsValidate] = useState(false);
    const phonePopupValue = state && state.checkoutLogin && state.checkoutLogin.phonePopupFlag;
    const [phonePopupFlag, setPhonePopupFlag] = useState(false);
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const onChangeHandler = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value });
    };

    const onSubmit = () => {
        setIsValidate(true);
        if (values.carrier_code === '' && values.phoneNumber === '') {
            if (!isEmail(values.email) || values.password === '') {
                scrollToElement('.input-error');
                return;
            }
        }
        if (values.carrier_code !== '' && values.phoneNumber !== '') {
            if (
                !isEmail(values.email) ||
                values.password === '' ||
                values.phoneNumber === '' ||
                values.carrier_code === '' ||
                !isCorrectPhone
            ) {
                scrollToElement('.input-error');
                return;
            }
        }
        setIsValidate(false);
        dispatch(
            checkoutLoginAction.checkoutLoginRequest({
                email: values.email,
                password: values.password,
                guestquote: quote_id ? quote_id : guestUser && guestUser.guestId,
                carrier_code: values.carrier_code ? values.carrier_code : '',
                contact_number: values.phoneNumber ? values.phoneNumber : '',
            }),
        );
    };

    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...values,
            phoneNumber: phoneNo.phone,
            carrier_code: phoneNo.dialCode,
        };
        setValues(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };
    const checkPhoneExistStatus = useCallback(() => {
        if (phonePopupValue === false) {
            setPhonePopupFlag(true);
        }
    }, [phonePopupValue]);

    useEffect(() => {
        try {
            checkPhoneExistStatus();
        } catch (err) {}
    }, [checkPhoneExistStatus]);

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            onSubmit();
        }
    };

    return (
        <Grid container justify="center">
            <Suspense fallback={`Loading....`}>
                <ForgotPassword open={open} setOpen={setOpen} />
            </Suspense>
            <Hidden only={['xs', 'sm']}>
                <Grid item xs={11} md={10}>
                    <Typography className="newCustomer">
                        <FormattedMessage id="Checkout.ExistingCustomers" defaultMessage="Existing Customers" />
                    </Typography>
                </Grid>
            </Hidden>
            <Grid item xs={11} md={10} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.EmailAddress" defaultMessage="Email Address*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.email}
                    onChange={onChangeHandler('email')}
                    fullWidth
                    onKeyDown={handleKeyDown}
                    className="inputBox"
                    error={isValidate && !isEmail(values.email)}
                />
                {isValidate && !isEmail(values.email) && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Email.Error" defaultMessage="Please Enter Valid Email Address" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} md={10} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.Password" defaultMessage="Password*" />
                </Typography>
                <TextField
                    variant="outlined"
                    type="password"
                    className="inputBox"
                    value={values.password}
                    onChange={onChangeHandler('password')}
                    onKeyDown={handleKeyDown}
                    fullWidth
                    error={isValidate && values.password === ''}
                />

                {isValidate && values.password === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Password.Error" defaultMessage="Please Enter Valid password" />
                    </FormHelperText>
                )}
            </Grid>
            {phonePopupFlag === true && (
                <Grid item xs={11} md={10} style={{ paddingBottom: 0 }} className="padding-t-input-box">
                    <Typography className="login-sigup-input-text">
                        <FormattedMessage id="SignInSignUp.ContactNumber" defaultMessage="Contact Number*" />
                    </Typography>
                    <PhoneNumber
                        parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                        carrier_code={values.carrier_code}
                        phoneNo={values.phoneNumber}
                    />
                    {isValidate && values.phoneNumber === '' && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="Common.PhoneNumber.Empty"
                                defaultMessage="Please enter contact number"
                            />
                        </FormHelperText>
                    )}
                    {isValidate && values.phoneNumber !== '' && !isCorrectPhone && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="Common.PhoneNumber.InValid"
                                defaultMessage="Please enter contact number"
                            />
                        </FormHelperText>
                    )}
                </Grid>
            )}

            <Grid item xs={11} md={10}>
                <Typography className="login-signup-forgort-password" onClick={() => setOpen(true)}>
                    <FormattedMessage id="SignInSignUp.ForgotPassword" defaultMessage="Forgot password?" />
                </Typography>
            </Grid>

            <Grid item xs={11} md={10} className="arabic-right">
                {!loginLoader && !reservationLoader && (
                    <Button className="login-signup-login-button" onClick={() => onSubmit()}>
                        <FormattedMessage id="SignInSignUp.Button.Login" defaultMessage="Login" />
                    </Button>
                )}
                {(loginLoader || reservationLoader) && <CircularProgress />}
            </Grid>
        </Grid>
    );
}
