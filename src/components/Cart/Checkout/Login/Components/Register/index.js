import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import PhoneNumber from 'commonComponet/PhoneNumber';
import { isEmail, checkPasswordContain, checkRegisterForm, scrollToElement } from 'components/SignIn_SignUp/utils';
import { actions as checkoutLoginAction } from 'components/Cart/Checkout/Login/redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';

export default function CheckoutRegister() {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    //const guestUser = state && state.auth && state.auth.guestUser;
    const quote_id = state && state.auth && state.auth.quote_id;
    const registerLoader = state && state.checkoutLogin && state.checkoutLogin.registerLoader;
    const reservationLoader = state && state.auth && state.auth.reservationLoader;

    const [values, setValues] = useState({
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        password: '',
        countryCode: '',
        confirmPassword: '',
    });

    const [isValidate, setIsValidate] = useState(false);
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...values,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setValues(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onChangeHandler = (name) => (event) => {
        if (name === 'firstName' || name === 'lastName') {
            const re = /^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g;
            if (event.target.value && !re.test(event.target.value)) {
                return '';
            }
        }
        const re_1 = /^(?!\s*$).+/g;
        if (event.target.value && !re_1.test(event.target.value)) {
            return '';
        }
        setValues({ ...values, [name]: event.target.value });
    };

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            onSubmit();
        }
    };

    const onSubmit = () => {
        setIsValidate(true);
        if (checkRegisterForm(values, isCorrectPhone)) {
            scrollToElement('.input-error');
            return;
        }
        setIsValidate(false);
        dispatch(
            checkoutLoginAction.checkoutRegisterRequest({
                firstname: values.firstName,
                lastname: values.lastName,
                email: values.email,
                password: values.password,
                confirmpassword: values.confirmPassword,
                store_id: state && state.auth && state.auth.currentStore,
                quest_quote: quote_id, //guestUser && guestUser.guestId,
                subscribe_to_newsletter: 0,
                contact_number: values.phoneNumber,
                carrier_code: values.countryCode,
            }),
        );
    };

    return (
        <Grid container>
            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.FirstName" defaultMessage="First Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.firstName}
                    onKeyPress={(e) => handleKeyDown(e)}
                    onChange={onChangeHandler('firstName')}
                    fullWidth
                    className="inputBox"
                    error={isValidate && values.firstName === ''}
                    inputProps={{ maxLength: 20 }}
                />
                {isValidate && values.firstName === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.FirstName.Error" defaultMessage="Please enter first name" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.LastName" defaultMessage="Last Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.lastName}
                    onChange={onChangeHandler('lastName')}
                    fullWidth
                    onKeyPress={(e) => handleKeyDown(e)}
                    className="inputBox"
                    error={isValidate && values.lastName === ''}
                    inputProps={{ maxLength: 19 }}
                />
                {isValidate && values.lastName === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.LastName.Error" defaultMessage="Please enter last name" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.ContactNumber" defaultMessage="Contact Number*" />
                </Typography>
                <PhoneNumber
                    parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                    carrier_code={values.countryCode}
                    phoneNo={values.phoneNumber}
                />
                {isValidate && values.phoneNumber === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.PhoneNumber.Empty" defaultMessage="Please enter contact number" />
                    </FormHelperText>
                )}
                {isValidate && values.phoneNumber !== '' && !isCorrectPhone && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="Common.PhoneNumber.InValid"
                            defaultMessage="Please enter contact number"
                        />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSingUp.Email" defaultMessage="Email*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.email}
                    onChange={onChangeHandler('email')}
                    onKeyPress={(e) => handleKeyDown(e)}
                    fullWidth
                    className="inputBox"
                    error={isValidate && !isEmail(values.email)}
                />
                {isValidate && !isEmail(values.email) && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Email.Error" defaultMessage="Please Enter Valid Email Address" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.Password" defaultMessage="Password*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.password}
                    onChange={onChangeHandler('password')}
                    onKeyPress={(e) => handleKeyDown(e)}
                    fullWidth
                    type="password"
                    className="inputBox"
                    error={
                        isValidate &&
                        (values.password === '' || values.password.length < 8 || !checkPasswordContain(values.password))
                    }
                />
                {/** Password empty message */}
                {isValidate && values.password === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="SignInSignUp.Password.Empty" defaultMessage="Please enter password" />
                    </FormHelperText>
                )}
                {/** Password length less than 8 message */}
                {isValidate && values.password !== '' && values.password.length < 8 && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="SignInSignUp.Password.length"
                            defaultMessage="Minimum length is 8 characters"
                        />
                    </FormHelperText>
                )}
                {/** Password contain message */}
                <FormHelperText
                    className={
                        isValidate &&
                        values.password !== '' &&
                        values.password.length >= 8 &&
                        !checkPasswordContain(values.password)
                            ? 'password-strong-message input-error'
                            : 'password-strong-message'
                    }
                >
                    <FormattedMessage
                        id="SignInSignUp.password.strong.message"
                        defaultMessage="Password must be at least 8 characters long and contain an uppercase letter, a lowercase letter and a number."
                    />
                </FormHelperText>
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.ConfirmPassword" defaultMessage="Confirm Password*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.confirmPassword}
                    onChange={onChangeHandler('confirmPassword')}
                    onKeyPress={(e) => handleKeyDown(e)}
                    fullWidth
                    type="password"
                    className="inputBox"
                    error={isValidate && (values.confirmPassword === '' || values.confirmPassword !== values.password)}
                />
                {isValidate && values.confirmPassword === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="SignInSignUp.ConfirmPassword.Empty"
                            defaultMessage="Please enter password again"
                        />
                    </FormHelperText>
                )}
                {isValidate && values.confirmPassword !== '' && values.confirmPassword !== values.password && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="SignInSignUp.ConfirmPassword.NotSame"
                            defaultMessage="Password and Confirm password must be same"
                        />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="arabic-right">
                {!registerLoader && !reservationLoader && (
                    <Button className="login-signup-login-button m-t-15" onClick={() => onSubmit()}>
                        <FormattedMessage id="Checkout.RegisterAndProceed" defaultMessage="Register & Proceed" />
                    </Button>
                )}
                {(registerLoader || reservationLoader) && <CircularProgress />}
            </Grid>
        </Grid>
    );
}
