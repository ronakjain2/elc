import { createAction } from 'redux-actions';

//Actions
const CHECKOUT_REGISTER_REQUEST = 'ELC@OROB/CHECKOUT_REGISTER_REQUEST';
const CHECKOUT_REGISTER_REQUEST_SUCCESS = 'ELC@OROB/CHECKOUT_REGISTER_REQUEST_SUCCESS';
const CHECKOUT_REGISTER_REQUEST_FAILED = 'ELC@OROB/CHECKOUT_REGISTER_REQUEST_FAILED';

const CHECKOUT_LOGIN_REQUEST = 'ELC@OROB/CHECKOUT_LOGIN_REQUEST';
const CHECKOUT_LOGIN_REQUEST_SUCCESS = 'ELC@OROB/CHECKOUT_LOGIN_REQUEST_SUCCESS';
const CHECKOUT_LOGIN_REQUEST_FAILED = 'ELC@OROB/CHECKOUT_LOGIN_REQUEST_FAILED';

const SAVE_GUEST_DETAIL_REQUEST = 'ELC@OROB/SAVE_GUEST_DETAIL_REQUEST';

//Actions methods
const checkoutRegisterRequest = createAction(CHECKOUT_REGISTER_REQUEST);
const checkoutRegisterRequestSuccess = createAction(CHECKOUT_REGISTER_REQUEST_SUCCESS);
const checkoutRegisterRequestFailed = createAction(CHECKOUT_REGISTER_REQUEST_FAILED);

const checkoutLoginRequest = createAction(CHECKOUT_LOGIN_REQUEST);
const checkoutLoginRequestSuccess = createAction(CHECKOUT_LOGIN_REQUEST_SUCCESS);
const checkoutLoginRequestFailed = createAction(CHECKOUT_LOGIN_REQUEST_FAILED);

const saveGuestDetailsRequest = createAction(SAVE_GUEST_DETAIL_REQUEST);

//actions
export const actions = {
    checkoutRegisterRequest,
    checkoutRegisterRequestSuccess,
    checkoutRegisterRequestFailed,

    checkoutLoginRequest,
    checkoutLoginRequestSuccess,
    checkoutLoginRequestFailed,

    saveGuestDetailsRequest,
};

//types
export const types = {
    CHECKOUT_REGISTER_REQUEST,
    CHECKOUT_REGISTER_REQUEST_SUCCESS,
    CHECKOUT_REGISTER_REQUEST_FAILED,

    CHECKOUT_LOGIN_REQUEST,
    CHECKOUT_LOGIN_REQUEST_SUCCESS,
    CHECKOUT_LOGIN_REQUEST_FAILED,

    SAVE_GUEST_DETAIL_REQUEST,
};
