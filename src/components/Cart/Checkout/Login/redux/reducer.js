import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandlers = {
    [types.CHECKOUT_LOGIN_REQUEST]: (state) => ({
        ...state,
        loginLoader: true,
        phonePopupFlag: true,
    }),
    [types.CHECKOUT_LOGIN_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loginLoader: false,
        phonePopupFlag: payload && payload.phonePopupFlag,
    }),
    [types.CHECKOUT_LOGIN_REQUEST_SUCCESS]: (state) => ({
        ...state,
        loginLoader: false,
        phonePopupFlag: true,
    }),

    [types.CHECKOUT_REGISTER_REQUEST]: (state) => ({
        ...state,
        registerLoader: true,
    }),
    [types.CHECKOUT_REGISTER_REQUEST_FAILED]: (state) => ({
        ...state,
        registerLoader: false,
    }),
    [types.CHECKOUT_REGISTER_REQUEST_SUCCESS]: (state) => ({
        ...state,
        registerLoader: false,
    }),

    [types.SAVE_GUEST_DETAIL_REQUEST]: (state, { payload }) => ({
        ...state,
        guestDetails: payload,
    }),
};

export default handleActions(actionHandlers, {
    loginLoader: false,
    registerLoader: false,
    phonePopupFlag: true,
    guestDetails: {},
});
