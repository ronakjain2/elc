import { call, put, takeLatest, select } from 'redux-saga/effects';
import { actions as authActions } from 'components/Global/redux/actions';
import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import { push } from 'connected-react-router';
import { actions, types } from './actions';
import api from '../../api';
import { toast } from 'react-toastify';

const auth = (state) => state && state.auth;

const checkouLoginReq = function* checkouLoginReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.checkoutLogin, payload);
        if (data && data.status) {
            yield put(
                basketActions.getCartItemRequest({
                    store_id: authData.currentStore,
                    quote_id: data && data.customer_details && data.customer_details.quote_id,
                }),
            );
            yield put(actions.checkoutLoginRequestSuccess());
            yield put(authActions.saveLoginUser(data.customer_details));
            yield put(authActions.saveQuoteIdRequest(data && data.customer_details && data.customer_details.quote_id));
            //Reservation after login
            yield put(
                authActions.addUpdateReservationRequest({
                    store_id: authData.currentStore,
                    quote_id: data && data.customer_details && data.customer_details.quote_id,
                    reservation_type: 'checkout',
                }),
            );
        } else {
            toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
            yield put(actions.checkoutLoginRequestFailed({ phonePopupFlag: data.phone_exists }));
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.checkoutLoginRequestFailed());
    }
};

const checkoutRegisterReq = function* checkoutRegisterReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.checkoutRegister, payload);
        if (data && data.status) {
            yield put(actions.checkoutRegisterRequestSuccess());
            yield put(authActions.saveLoginUser(data.customer_details));
            yield put(authActions.saveQuoteIdRequest(data && data.customer_details && data.customer_details.quote_id));
            yield put(push(`/${authData && authData.store_locale}/delivery-details`));
        } else {
            toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
            yield put(actions.checkoutRegisterRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.checkoutRegisterRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.CHECKOUT_LOGIN_REQUEST, checkouLoginReq);
    yield takeLatest(types.CHECKOUT_REGISTER_REQUEST, checkoutRegisterReq);
}
