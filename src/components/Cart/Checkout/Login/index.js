import React, { lazy, Suspense, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Hidden from '@material-ui/core/Hidden';
import Radio from '@material-ui/core/Radio';
import '../checkout.css';
import Spinner from 'commonComponet/Spinner';
import { FormattedMessage } from 'react-intl';
import 'components/SignIn_SignUp/main_styles.css';
import './login.css';

const CheckoutStepper = lazy(() => import('../Stepper'));
const CheckoutRegister = lazy(() => import('./Components/Register'));
const CheckoutGuest = lazy(() => import('./Components/Guest'));
const ExistingCustomer = lazy(() => import('./Components/Exsiting'));
const CheckoutCartDetails = lazy(() => import('../CartDetails'));

export default function CheckoutLogin() {
    const [value, setValue] = useState('register');
    const [activeTab, setActiveTab] = useState('NewCustomer');

    const handleRadioChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="checkout-login">
                {/** Checkout Stepper */}
                <Grid item xs={12} md={10} lg={8}>
                    <CheckoutStepper activeStep={0} />
                </Grid>
                {/** End Checkout Stepper */}

                <Grid item xs={11}>
                    <Grid container className="checkout-outer-padding" justify="space-between">
                        {/** Login Page */}
                        <Hidden only={['xs', 'sm']}>
                            <Grid item xs={12} md={8} className="checkout-details-card">
                                <Grid container>
                                    <Grid item xs={12}>
                                        <Typography className="title">
                                            <FormattedMessage id="Checkout.Login" defaultMessage="Login" />
                                        </Typography>
                                    </Grid>

                                    <Grid item md={6} className="border-l-r ">
                                        {/** Login Text */}

                                        {/** New Customers Text */}
                                        <Grid item xs={12}>
                                            <Typography className="newCustomer">
                                                <FormattedMessage
                                                    id="Checkout.Login.NewCustomer"
                                                    defaultMessage="New Customers"
                                                />
                                            </Typography>
                                        </Grid>
                                        {/** Radio button Text */}
                                        <Grid item xs={12} className="arabic-right">
                                            <FormControl component="fieldset">
                                                <RadioGroup
                                                    className="login-radio-button"
                                                    row
                                                    aria-label="quiz"
                                                    name="quiz"
                                                    value={value}
                                                    onChange={handleRadioChange}
                                                >
                                                    <FormControlLabel
                                                        className={value === 'register' ? 'active' : 'inactive'}
                                                        value="register"
                                                        control={<Radio />}
                                                        label={
                                                            <FormattedMessage
                                                                id="SignInSignUp.Register"
                                                                defaultMessage="Register"
                                                            />
                                                        }
                                                    />

                                                    <FormControlLabel
                                                        className={value === 'guest' ? 'active' : 'inactive'}
                                                        value="guest"
                                                        control={<Radio />}
                                                        label={
                                                            <FormattedMessage
                                                                id="Checkout.Login.Guest"
                                                                defaultMessage="Guest"
                                                            />
                                                        }
                                                    />
                                                </RadioGroup>
                                            </FormControl>
                                        </Grid>
                                        {/** Lines Text */}
                                        <Grid item xs={12} className="arabic-right">
                                            <Typography className="login-line-text">
                                                <FormattedMessage
                                                    id="Checkout.Login.Line1"
                                                    defaultMessage="*Register with one additional step - enter 'password"
                                                />
                                            </Typography>
                                            <Typography className="login-line-text">
                                                <FormattedMessage
                                                    id="Checkout.Login.Line2"
                                                    defaultMessage="*Register lets you track / manage / return orders"
                                                />
                                            </Typography>
                                        </Grid>
                                        {/** Forms */}
                                        <Grid item xs={12}>
                                            {value === 'register' && <CheckoutRegister />}
                                            {value === 'guest' && <CheckoutGuest />}
                                        </Grid>
                                    </Grid>

                                    <Grid item md={6}>
                                        <ExistingCustomer />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Hidden>

                        <Hidden only={['md', 'lg', 'xl']}>
                            <Grid item xs={12} md={8} className="checkout-details-card">
                                <Grid container>
                                    <Grid item xs={12}>
                                        <Typography className="title">
                                            <FormattedMessage id="Checkout.Login" defaultMessage="Login" />
                                        </Typography>
                                    </Grid>
                                    {/** New And exsit customer tab */}
                                    <Grid container className="login-tab-container">
                                        <Grid item xs={6}>
                                            <Typography
                                                className={
                                                    activeTab === 'NewCustomer' ? 'active-text' : 'inactive-text'
                                                }
                                                onClick={() => setActiveTab('NewCustomer')}
                                            >
                                                <FormattedMessage
                                                    id="Checkout.Login.NewCustomer"
                                                    defaultMessage="New Customers"
                                                />
                                            </Typography>
                                            {activeTab === 'NewCustomer' && <div className="active-line" />}
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Typography
                                                className={activeTab === 'Existing' ? 'active-text' : 'inactive-text'}
                                                onClick={() => setActiveTab('Existing')}
                                            >
                                                <FormattedMessage
                                                    id="Checkout.ExistingCustomers"
                                                    defaultMessage="Existing Customers"
                                                />
                                            </Typography>
                                            {activeTab === 'Existing' && <div className="active-line" />}
                                        </Grid>
                                    </Grid>
                                    {/** End New And exsit customer tab */}

                                    {activeTab === 'NewCustomer' && (
                                        <>
                                            {/** Radio button Text */}
                                            <Grid item xs={12} className="arabic-right">
                                                <FormControl component="fieldset">
                                                    <RadioGroup
                                                        className="login-radio-button"
                                                        row
                                                        aria-label="quiz"
                                                        name="quiz"
                                                        value={value}
                                                        onChange={handleRadioChange}
                                                    >
                                                        <FormControlLabel
                                                            className={value === 'register' ? 'active' : 'inactive'}
                                                            value="register"
                                                            control={<Radio />}
                                                            label={
                                                                <FormattedMessage
                                                                    id="SignInSignUp.Register"
                                                                    defaultMessage="Register"
                                                                />
                                                            }
                                                        />

                                                        <FormControlLabel
                                                            className={value === 'guest' ? 'active' : 'inactive'}
                                                            value="guest"
                                                            control={<Radio />}
                                                            label={
                                                                <FormattedMessage
                                                                    id="Checkout.Login.Guest"
                                                                    defaultMessage="Guest"
                                                                />
                                                            }
                                                        />
                                                    </RadioGroup>
                                                </FormControl>
                                            </Grid>
                                            {/** Lines Text */}
                                            <Grid item xs={12} className="arabic-right">
                                                <Typography className="login-line-text">
                                                    <FormattedMessage
                                                        id="Checkout.Login.Line1"
                                                        defaultMessage="*Register with one additional step - enter 'password"
                                                    />
                                                </Typography>
                                                <Typography className="login-line-text">
                                                    <FormattedMessage
                                                        id="Checkout.Login.Line2"
                                                        defaultMessage="*Register lets you track / manage / return orders"
                                                    />
                                                </Typography>
                                            </Grid>
                                            {/** Forms */}
                                            <Grid item xs={12}>
                                                {value === 'register' && <CheckoutRegister />}
                                                {value === 'guest' && <CheckoutGuest />}
                                            </Grid>
                                        </>
                                    )}
                                    {activeTab === 'Existing' && (
                                        <Grid item md={12}>
                                            <ExistingCustomer />
                                        </Grid>
                                    )}
                                </Grid>
                            </Grid>
                        </Hidden>
                        {/**End Login Page */}

                        {/** Checkout Carts Page */}
                        <Grid item xs={12} md={3}>
                            <CheckoutCartDetails />
                        </Grid>
                        {/** End Checkout Carts Page */}
                    </Grid>
                </Grid>
            </Grid>
        </Suspense>
    );
}
