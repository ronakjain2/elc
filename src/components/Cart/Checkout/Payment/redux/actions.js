import { createAction } from 'redux-actions';

//Actions
const GET_CHECKOUT_PAYMENTS_REQUEST = 'ELC@OROB/GET_CHECKOUT_PAYMENTS_REQUEST';
const GET_CHECKOUT_PAYMENTS_REQUEST_SUCCESS = 'ELC@OROB/GET_CHECKOUT_PAYMENTS_REQUEST_SUCCESS';
const GET_CHECKOUT_PAYMENTS_REQUEST_FAILED = 'ELC@OROB/GET_CHECKOUT_PAYMENTS_REQUEST_FAILED';

const SET_CHECKOUT_PAYMENT_REQUEST = 'ELC@OROB/SET_CHECKOUT_PAYMENT_REQUEST';
const SET_CHECKOUT_PAYMENT_REQUEST_SUCCESS = 'ELC@OROB/SET_CHECKOUT_PAYMENT_REQUEST_SUCCESS';
const SET_CHECKOUT_PAYMENT_REQUEST_FAILED = 'ELC@OROB/SET_CHECKOUT_PAYMENT_REQUEST_FAILED';

const PLACE_ORDER_REQUEST = 'ELC@OROB/PLACE_ORDER_REQUEST';
const PLACE_ORDER_REQUEST_SUCCESS = 'ELC@OROB/PLACE_ORDER_REQUEST_SUCCESS';
const PLACE_ORDER_REQUEST_FAILED = 'ELC@OROB/PLACE_ORDER_REQUEST_FAILED';

const GET_APPLY_VOUCHER_REQUEST = 'ELC@OROB/GET_APPLY_VOUCHER_REQUEST';
const GET_APPLY_VOUCHER_REQUEST_SUCCESS = 'ELC@OROB/GET_APPLY_VOUCHER_REQUEST_SUCCESS';
const GET_APPLY_VOUCHER_REQUEST_FAILED = 'ELC@OROB/GET_APPLY_VOUCHER_REQUEST_FAILED';

const GET_REMOVE_VOUCHER_REQUEST = 'ELC@OROB/GET_REMOVE_VOUCHER_REQUEST';
// Apple Pay
const SET_APPLE_PAY_SET_PAYMENT_REQUEST = 'ELC@OROB/SET_APPLE_PAY_SET_PAYMENT_REQUEST';
const SET_APPLE_PAY_SET_PAYMENT_REQUEST_SUCCESS = 'ELC@OROB/SET_APPLE_PAY_SET_PAYMENT_REQUEST_SUCCESS';
const SET_APPLE_PAY_SET_PAYMENT_REQUEST_FAILED = 'ELC@OROB/SET_APPLE_PAY_SET_PAYMENT_REQUEST_FAILED';

const SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION = 'ELC@OROB/SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION';
const SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION_SUCCESS = ' ELC@OROB/SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION_SUCCESS';
const SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION_FAILED = 'ELC@OROB/SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION_FAILED';

const SET_APPLE_PAY_PLACE_ORDER_REQUEST = 'ELC@OROB/SET_APPLE_PAY_PLACE_ORDER_REQUEST';
const SET_APPLE_PAY_PLACE_ORDER_REQUEST_SUCCESS = 'ELC@OROB/SET_APPLE_PAY_PLACE_ORDER_REQUEST_SUCCESS';
const SET_APPLE_PAY_PLACE_ORDER_REQUEST_FAILED = 'ELC@OROB/SET_APPLE_PAY_PLACE_ORDER_REQUEST_FAILED';

const DELETE_SAVED_CARD_REQ = 'ELC@OROB/DELETE_SAVED_CARD_REQ';
const DELETE_SAVED_CARD_SUCCESS = 'ELC@OROB/DELETE_SAVED_CARD_SUCCESS';
const DELETE_SAVED_CARD_FAILED = 'ELC@OROB/DELETE_SAVED_CARD_FAILED';

const PAYFORT_RESTORE_QUOTE_REQ = 'ELC@OROB/PAYFORT_RESTORE_QUOTE_REQ';

//Actions Methods

//apple pay

const setApplePayCheckoutPaymentRequest = createAction(SET_APPLE_PAY_SET_PAYMENT_REQUEST);
const setApplePayCheckoutPaymentRequestSucess = createAction(SET_APPLE_PAY_SET_PAYMENT_REQUEST_SUCCESS);
const setApplePayCheckoutPaymentRequestFailed = createAction(SET_APPLE_PAY_SET_PAYMENT_REQUEST_FAILED);

const setApplePayMerchantValidationRequest = createAction(SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION);
const setApplePayMerchantValidationRequestSucess = createAction(SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION_SUCCESS);
const setApplePayMerchantValidationRequestFailed = createAction(SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION_FAILED);

const setApplePayPlaceOrderRequest = createAction(SET_APPLE_PAY_PLACE_ORDER_REQUEST);
const setApplePayPlaceOrderRequestSucess = createAction(SET_APPLE_PAY_PLACE_ORDER_REQUEST_SUCCESS);
const setApplePayPlaceOrderRequestFailed = createAction(SET_APPLE_PAY_PLACE_ORDER_REQUEST_FAILED);

const getCheckoutPaymentsRequest = createAction(GET_CHECKOUT_PAYMENTS_REQUEST);
const getCheckoutPaymentsRequestSuccess = createAction(GET_CHECKOUT_PAYMENTS_REQUEST_SUCCESS);
const getCheckoutPaymentsRequestFailed = createAction(GET_CHECKOUT_PAYMENTS_REQUEST_FAILED);

const setCheckoutPaymentRequest = createAction(SET_CHECKOUT_PAYMENT_REQUEST);
const setCheckoutPaymentRequestSuccess = createAction(SET_CHECKOUT_PAYMENT_REQUEST_SUCCESS);
const setCheckoutPaymentRequestFailed = createAction(SET_CHECKOUT_PAYMENT_REQUEST_FAILED);

const placeOrderRequest = createAction(PLACE_ORDER_REQUEST);
const placeOrderRequestSuccess = createAction(PLACE_ORDER_REQUEST_SUCCESS);
const placeOrderRequestFailed = createAction(PLACE_ORDER_REQUEST_FAILED);

const getApplyVoucherRequest = createAction(GET_APPLY_VOUCHER_REQUEST);
const getApplyVoucherRequestSuccess = createAction(GET_APPLY_VOUCHER_REQUEST_SUCCESS);
const getApplyVoucherRequestFailed = createAction(GET_APPLY_VOUCHER_REQUEST_FAILED);

const getRemoveVoucherRequest = createAction(GET_REMOVE_VOUCHER_REQUEST);

const deleteSavedCardRequest = createAction(DELETE_SAVED_CARD_REQ);
const deleteSavedCardRequestSuccess = createAction(DELETE_SAVED_CARD_SUCCESS);
const deleteSavedCardRequestFailed = createAction(DELETE_SAVED_CARD_FAILED);

const payfortRestoreQuoteRequest = createAction(PAYFORT_RESTORE_QUOTE_REQ);

export const actions = {
    getCheckoutPaymentsRequest,
    getCheckoutPaymentsRequestSuccess,
    getCheckoutPaymentsRequestFailed,

    setCheckoutPaymentRequest,
    setCheckoutPaymentRequestSuccess,
    setCheckoutPaymentRequestFailed,

    placeOrderRequest,
    placeOrderRequestSuccess,
    placeOrderRequestFailed,

    getApplyVoucherRequest,
    getApplyVoucherRequestSuccess,
    getApplyVoucherRequestFailed,

    getRemoveVoucherRequest,

    setApplePayCheckoutPaymentRequest,
    setApplePayCheckoutPaymentRequestSucess,
    setApplePayCheckoutPaymentRequestFailed,

    setApplePayMerchantValidationRequest,
    setApplePayMerchantValidationRequestSucess,
    setApplePayMerchantValidationRequestFailed,

    setApplePayPlaceOrderRequest,
    setApplePayPlaceOrderRequestSucess,
    setApplePayPlaceOrderRequestFailed,
    deleteSavedCardRequest,
    deleteSavedCardRequestSuccess,
    deleteSavedCardRequestFailed,

    payfortRestoreQuoteRequest,
};

//types
export const types = {
    GET_CHECKOUT_PAYMENTS_REQUEST,
    GET_CHECKOUT_PAYMENTS_REQUEST_SUCCESS,
    GET_CHECKOUT_PAYMENTS_REQUEST_FAILED,

    SET_CHECKOUT_PAYMENT_REQUEST,
    SET_CHECKOUT_PAYMENT_REQUEST_SUCCESS,
    SET_CHECKOUT_PAYMENT_REQUEST_FAILED,

    PLACE_ORDER_REQUEST,
    PLACE_ORDER_REQUEST_SUCCESS,
    PLACE_ORDER_REQUEST_FAILED,

    GET_APPLY_VOUCHER_REQUEST,
    GET_APPLY_VOUCHER_REQUEST_SUCCESS,
    GET_APPLY_VOUCHER_REQUEST_FAILED,

    GET_REMOVE_VOUCHER_REQUEST,

    SET_APPLE_PAY_SET_PAYMENT_REQUEST,
    SET_APPLE_PAY_SET_PAYMENT_REQUEST_SUCCESS,
    SET_APPLE_PAY_SET_PAYMENT_REQUEST_FAILED,

    SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION_SUCCESS,
    SET_APPLEPAY_REQUEST_MERCHANT_VALIDATION_FAILED,

    SET_APPLE_PAY_PLACE_ORDER_REQUEST,
    SET_APPLE_PAY_PLACE_ORDER_REQUEST_SUCCESS,
    SET_APPLE_PAY_PLACE_ORDER_REQUEST_FAILED,
    DELETE_SAVED_CARD_REQ,
    DELETE_SAVED_CARD_SUCCESS,
    DELETE_SAVED_CARD_FAILED,

    PAYFORT_RESTORE_QUOTE_REQ,
};
