import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import { actions as guestUserActions } from '../../../../Global/redux/actions';
import { actions as checkoutDeliveryActions } from 'components/Cart/Checkout/Delivery/redux/actions';
import { actions as authActions } from 'components/Global/redux/actions';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import { call, delay, put, select, takeLatest } from 'redux-saga/effects';
import api from '../../api';
import { createEncryptOrderId } from '../../utils';
import { actions, types } from './actions';
import { PAYFORT_URL } from '../../../../../environments';

const auth = (state) => state && state.auth;
const checkoutPayment = (state) => state && state.checkoutPayment;

const getCheckoutPaymentReq = function* getCheckoutPaymentReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.getCheckoutPayment, {
            store_id: payload.store_id,
            quote_id: authData.quote_id,
        });
        if (data && data.status) {
            yield put(
                actions.getCheckoutPaymentsRequestSuccess({
                    payment: data.data,
                    cartData: data.cart_data,
                    shippingAddressData: data.shipping_address_data,
                    shippingMethodData: data.shipping_method_data,
                    credit_cards: data.data.payfort_fort_cc.cards,
                }),
            );
        } else {
            toast.error(data && data.message);
            yield put(actions.getCheckoutPaymentsRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getCheckoutPaymentsRequestFailed());
    }
};

const setCheckoutPaymentReq = function* setCheckoutPaymentReq({ payload }) {
    try {
        const { data } = yield call(api.setCheckoutPayment, {
            payment_code: payload.payment_code,
            quote_id: payload.quote_id,
            store_id: payload.store_id,
        });
        if (data && data.status) {
            if (!payload.requestFromApplePay) {
                yield delay(1000);

                yield put(actions.placeOrderRequest(payload));
                yield put(
                    actions.setCheckoutPaymentRequestSuccess({
                        slot_avl_flag: data.slot_avl_flag,
                        slot_avl_message: data.slot_avl_message,
                    }),
                );
            }
            if (payload.requestFromApplePay) {
                yield put(
                    actions.setCheckoutPaymentRequestSuccess({
                        slot_avl_flag: data.slot_avl_flag,
                        slot_avl_message: data.slot_avl_message,
                        requestFromApplePaySuccess: payload.requestFromApplePay,
                    }),
                );
            }
        } else {
            toast.error(data && data.message);
            yield put(actions.setCheckoutPaymentRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.setCheckoutPaymentRequestFailed());
    }
};

const placeOrderReq = function* placeOrderReq({ payload }) {
    try {
        const authData = yield select(auth);
        yield put(checkoutDeliveryActions.saveOrClearDeliveryDetails({}));
        yield put(authActions.saveReservationStartTime(null));
        const { quote_id, store_id, payment_code, cardDetails } = payload;
        let placeOrderPayload = { quote_id, store_id };
        if (cardDetails.type === 'other') {
            placeOrderPayload = { ...placeOrderPayload, save_card: cardDetails.save_card };
        } else if (cardDetails.type === 'saved') {
            placeOrderPayload = {
                ...placeOrderPayload,
                token: cardDetails.token,
                card_security_code: cardDetails.card_security_code,
            };
        }
        const { data } = yield call(api.placeOrder, placeOrderPayload);

        if (authData && !authData.loginUser) {
            yield put(authActions.saveGuestUser(null));
        }

        if (data && data.status) {
            yield put(
                actions.setCheckoutPaymentRequestSuccess({
                    slot_avl_flag: true,
                    slot_avl_message: '',
                }),
            );
            if (payment_code === 'cashondelivery') {
                if (data && data.order_data && data.order_data.new_quote_id && data.order_data.new_quote_id !== '') {
                    yield put(authActions.saveQuoteIdRequest(data.order_data.new_quote_id));
                }
                yield put(actions.placeOrderRequestSuccess());

                yield put(
                    push(
                        `/${authData && authData.store_locale}/order-summary?order_id=${createEncryptOrderId(
                            data.order_data.order_number,
                        )}&type=success`,
                    ),
                );
            } else if (payment_code === 'payfort_fort_cc') {
                if (data && data.order_data && data.order_data.new_quote_id && data.order_data.new_quote_id !== '') {
                    yield put(authActions.saveQuoteIdRequest(data.order_data.new_quote_id));
                }
                if (
                    data &&
                    data.payfort_data &&
                    data.payfort_data !== '' &&
                    Object.keys(data.payfort_data).length > 0
                ) {
                    if (cardDetails.type === 'other') {
                        // Functionality for new card
                        yield put(push(`/${authData && authData.store_locale}/payment-processing?step=PROCESSING`));
                        let form = document.createElement('form');
                        form.method = 'POST';
                        form.action = PAYFORT_URL;
                        const params = [
                            'merchant_identifier',
                            'access_code',
                            'merchant_reference',
                            'service_command',
                            'language',
                            'return_url',
                            'signature',
                            'card_holder_name',
                            'card_number',
                            'expiry_date',
                            'card_security_code',
                        ];
                        let payfort_data = {
                            ...data.payfort_data,
                            card_holder_name: cardDetails.card_holder_name,
                            card_number: cardDetails.card_number,
                            expiry_date: cardDetails.expiry_date,
                            card_security_code: cardDetails.card_security_code,
                        };
                        localStorage.setItem('merchant_reference', payfort_data.merchant_reference);
                        Object.keys(payfort_data).map((payfort) => {
                            if (params.includes(payfort)) {
                                let element1 = document.createElement('input');
                                element1.value = payfort_data[payfort];
                                element1.name = payfort;
                                element1.type = 'hidden';
                                form.appendChild(element1);
                            }
                            return form;
                        });
                        document.body.appendChild(form);
                        form.submit();
                    } else {
                        // Functionality for saved card
                        let payfort_data = data.payfort_data;
                        // Set merchant_reference
                        localStorage.setItem('merchant_reference', payfort_data.merchant_reference);
                        const SUCCESS_STATUSES = [2, 4, 14, 15, 19, 20, 28, 40, 44, 80];
                        var Cryptr = require('cryptr');
                        const cryptr = new Cryptr(payfort_data.merchant_reference);
                        const cryptrOrderId = new Cryptr('mihyarOrderId');
                        const obj = {
                            command: payfort_data.command ? payfort_data.command : '',
                            access_code: payfort_data.access_code ? payfort_data.access_code : '',
                            merchant_identifier: payfort_data.merchant_identifier
                                ? payfort_data.merchant_identifier
                                : '',
                            merchant_reference: payfort_data.merchant_reference ? payfort_data.merchant_reference : '',
                            amount: payfort_data.amount ? payfort_data.amount : '',
                            currency: payfort_data.currency ? payfort_data.currency : '',
                            language: payfort_data.language ? payfort_data.language : '',
                            customer_email: payfort_data.customer_email ? payfort_data.customer_email : '',
                            signature: payfort_data.signature ? payfort_data.signature : '',
                            fort_id: payfort_data.fort_id ? payfort_data.fort_id : '',
                            payment_option: payfort_data.payment_option ? payfort_data.payment_option : '',
                            eci: payfort_data.eci ? payfort_data.eci : '',
                            order_description: payfort_data.order_description ? payfort_data.order_description : '',
                            customer_ip: payfort_data.customer_ip ? payfort_data.customer_ip : '',
                            customer_name: payfort_data.card_holder_name ? payfort_data.card_holder_name : '',
                            response_message: payfort_data.response_message ? payfort_data.response_message : '',
                            response_code: payfort_data.response_code ? payfort_data.response_code : '',
                            status: payfort_data.status ? payfort_data.status : '',
                            card_holder_name: payfort_data.card_holder_name ? payfort_data.card_holder_name : '',
                            expiry_date: payfort_data.expiry_date ? payfort_data.expiry_date : '',
                            card_number: payfort_data.card_number ? payfort_data.card_number : '',
                            order_id: payfort_data.merchant_reference ? payfort_data.merchant_reference : '',
                        };
                        if (parseInt(payfort_data.status) === 20) {
                            yield put(push(`/${authData && authData.store_locale}/payment-processing?step=PROCESSING`));
                            window.location.href = payfort_data['3ds_url'];
                        } else if (parseInt(payfort_data.status) === 14) {
                            yield call(api.OrderPayment, obj);
                            yield put(actions.placeOrderRequestSuccess());
                            yield put(
                                push(
                                    `/${authData && authData.store_locale}/order-summary?store_id=${
                                        payfort_data.merchant_extra
                                    }&order_id=${cryptrOrderId.encrypt(
                                        payfort_data.merchant_reference,
                                    )}&status=${cryptr.encrypt(parseInt(payfort_data.status) === 14)}&type=${
                                        parseInt(payfort_data.status) === 14 ? 'success' : 'failure'
                                    }`,
                                ),
                            );
                        } else if (SUCCESS_STATUSES.includes(parseInt(payfort_data.status))) {
                            yield call(api.OrderPayment, obj);
                            yield put(actions.placeOrderRequestSuccess());

                            yield put(
                                push(
                                    `/${authData && authData.store_locale}/order-summary?store_id=${
                                        payfort_data.merchant_extra
                                    }&order_id=${cryptrOrderId.encrypt(
                                        payfort_data.merchant_reference,
                                    )}&status=${cryptr.encrypt(parseInt(payfort_data.status) === 14)}&type=${
                                        parseInt(payfort_data.status) === 14 ? 'success' : 'failure'
                                    }`,
                                ),
                            );
                        } else {
                            yield put(
                                push(
                                    `/${
                                        authData && authData.store_locale
                                    }/payment-processing?step=FAILED&order_id=${cryptrOrderId.encrypt(
                                        payfort_data.merchant_reference,
                                    )}&status=${cryptr.encrypt(parseInt(payfort_data.status) === 14)}&message=${
                                        payfort_data.response_message
                                    }`,
                                ),
                            );
                        }
                    }
                } else {
                    toast.error(`Payfort data not available, please try agian`);
                    yield put(actions.placeOrderRequestFailed());
                    yield put(push(`/${authData && authData.store_locale}/cart`));
                }
            }
        } else {
            toast.error(data && data.message);
            yield put(actions.placeOrderRequestFailed());
            if (data) yield put(push(`/${authData && authData.store_locale}/cart`));
        }
    } catch (err) {
        const authData = yield select(auth);
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.placeOrderRequestFailed());
        yield put(push(`/${authData && authData.store_locale}/cart`));
    }
};

const getApplyVoucherReq = function* getApplyVoucherReq({ payload }) {
    try {
        const { data } = yield call(api.getApplyVoucher, payload);
        if (data && data.status) {
            yield delay(500);

            yield put(
                actions.getApplyVoucherRequestSuccess({
                    voucode: payload.voucode,
                    message: data.message,
                    removeVoucher: false,
                }),
            );

            yield put(
                actions.getCheckoutPaymentsRequest({
                    quote_id: payload.quoteid,
                    store_id: payload.store,
                }),
            );

            yield put(
                basketActions.getCartItemRequest({
                    quote_id: payload.quoteid,
                    store_id: payload.store,
                    isPayment: true,
                    clearDeliveryObj: true,
                }),
            );

            yield delay(4000);
            yield put(actions.getApplyVoucherRequestSuccess({ voucode: '', message: '', removeVoucher: false }));
        } else {
            yield put(actions.getApplyVoucherRequestFailed({ voucode: payload.voucode, message: data.message }));
            yield delay(4000);
            yield put(actions.getApplyVoucherRequestSuccess({ voucode: '', message: '', removeVoucher: false }));
        }
    } catch (err) {
        yield put(
            actions.getApplyVoucherRequestFailed({
                data:
                    (err && err.response && err.response.data && err.response.data.message) ||
                    'Something went to wrong, Please try after sometime',
            }),
        );
    }
};

const getRemoveVoucherReq = function* getRemoveVoucherReq({ payload }) {
    try {
        const { data } = yield call(api.getRemoveVoucher, payload);
        if (data && data.status) {
            yield put(
                actions.getCheckoutPaymentsRequest({
                    quote_id: payload.quoteid,
                    store_id: payload.store,
                }),
            );
            yield put(
                basketActions.getCartItemRequest({
                    quote_id: payload.quoteid,
                    store_id: payload.store,
                    isPayment: true,
                    clearDeliveryObj: true,
                }),
            );
            yield put(
                actions.getApplyVoucherRequestSuccess({ voucode: '', message: data.message, removeVoucher: true }),
            );

            yield delay(4000);
            yield put(actions.getApplyVoucherRequestSuccess({ voucode: '', message: '', removeVoucher: true }));
        } else {
            yield put(actions.getApplyVoucherRequestFailed({ voucode: '', message: data.message }));
            yield delay(4000);
            yield put(actions.getApplyVoucherRequestSuccess({ voucode: '', message: '', removeVoucher: true }));
        }
    } catch (err) {
        yield put(
            actions.getApplyVoucherRequestFailed({
                data:
                    (err && err.response && err.response.data && err.response.data.message) ||
                    'Something went to wrong, Please try after sometime',
            }),
        );
    }
};
const placeOrderApplePayReq = function* placeOrderApplePayReq({ payload }) {
    try {
        const authData = yield select(auth);
        yield put(checkoutDeliveryActions.saveOrClearDeliveryDetails({}));
        yield put(authActions.saveReservationStartTime(null));
        const { data } = yield call(api.applePayPlaceOrder, payload);

        if (data && data.status) {
            var Cryptr = require('cryptr');
            const cryptr = new Cryptr(data && data.order_data && data.order_data.order_number);
            if (authData && !authData.loginUser) {
                yield put(authActions.saveGuestUser(null));
            }
            if (authData && authData.loginUser) {
                if (data && data.order_data && data.order_data.new_quote_id && data.order_data.new_quote_id !== '') {
                    yield put(authActions.saveQuoteIdRequest(data.order_data.new_quote_id));
                }
            }
            toast.success(data && data.msg);
            yield put(
                push(
                    `/${authData && authData.store_locale}/order-summary?store_id=${
                        payload.store_id
                    }&order_id=${createEncryptOrderId(
                        data && data.order_data && data.order_data.order_number,
                    )}&status=${cryptr.encrypt(data.status)}&type=success`,
                ),
            );
        } else {
            toast.error(data && data.msg);
            toast.error((data && data.msg) || 'Something went to wrong, Please try after sometime');
            yield put(actions.setApplePayPlaceOrderRequestFailed());
            yield put(push(`/${authData && authData.store_locale}/cart`));
        }
    } catch (err) {
        toast.error('Something went to wrong, Please try after sometime');
        yield put(actions.setApplePayPlaceOrderRequestFailed());
        // yield put(push(`/${authData && authData.store_locale}/cart`));
    }
};

const deleteSavedCardReq = function* deleteSavedCardReq({ payload }) {
    try {
        const { data } = yield call(api.deleteSavedCard, {
            id: payload.id,
            store_id: payload.store_id,
        });
        const checkoutPaymentReducer = yield select(checkoutPayment);
        if (data && data.status) {
            let updated_credit_cards = checkoutPaymentReducer.credit_cards;
            if (updated_credit_cards) {
                updated_credit_cards = updated_credit_cards.filter((item) => {
                    return payload.id !== item.id;
                });
            }
            toast.success(data && data.message);
            yield put(actions.deleteSavedCardRequestSuccess({ updated_credit_cards }));
        } else {
            toast.error(data && data.message);
            yield put(actions.deleteSavedCardRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.deleteSavedCardRequestFailed());
    }
};

const payfortRestoreQuote = function* payfortRestoreQuote({ payload }) {
    const authData = yield select(auth);
    try {
        const { data } = yield call(api.payfortRestoreQuote, {
            merchant_reference: payload.merchant_reference,
        });
        // remove
        localStorage.removeItem('merchant_reference');

        if (data) {
            if (data.status && !data.order_success) {
                if (data && data.quote_id) {
                    yield put(authActions.saveQuoteIdRequest(data.quote_id));
                }
                if (data && data.temp_quote_id) {
                    yield put(guestUserActions.saveGuestUser({ guestId: data.temp_quote_id }));
                }
                toast.error(payload && payload.payfort_message);
                yield put(push(`/${authData && authData.store_locale}/checkout-payment`));
            } else if (data.order_success) {
                var Cryptr = require('cryptr');
                const cryptr = new Cryptr(data.payfort_data.merchant_reference);
                const cryptrOrderId = new Cryptr('mihyarOrderId');
                if (data && data.quote_id) {
                    yield put(authActions.saveQuoteIdRequest(data.quote_id));
                }
                yield put(
                    push(
                        `/${authData && authData.store_locale}/order-summary?store_id=${
                            authData.currentStore
                        }&order_id=${cryptrOrderId.encrypt(
                            data.payfort_data.merchant_reference,
                        )}&status=${cryptr.encrypt(parseInt(data.payfort_data.transaction_status) === 14)}&type=${
                            parseInt(data.payfort_data.transaction_status) === 14 ? 'success' : 'failure'
                        }`,
                    ),
                );
            } else {
                toast.error(data && data.message);
                yield put(push(`/${authData && authData.store_locale}/cart`));
            }
        } else {
            toast.error(data && data.message);
            yield put(push(`/${authData && authData.store_locale}/cart`));
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(push(`/${authData && authData.store_locale}/cart`));
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_CHECKOUT_PAYMENTS_REQUEST, getCheckoutPaymentReq);
    yield takeLatest(types.SET_CHECKOUT_PAYMENT_REQUEST, setCheckoutPaymentReq);
    yield takeLatest(types.PLACE_ORDER_REQUEST, placeOrderReq);
    yield takeLatest(types.GET_APPLY_VOUCHER_REQUEST, getApplyVoucherReq);
    yield takeLatest(types.GET_REMOVE_VOUCHER_REQUEST, getRemoveVoucherReq);
    yield takeLatest(types.DELETE_SAVED_CARD_REQ, deleteSavedCardReq);
    yield takeLatest(types.PAYFORT_RESTORE_QUOTE_REQ, payfortRestoreQuote);
    yield takeLatest(types.SET_APPLE_PAY_PLACE_ORDER_REQUEST, placeOrderApplePayReq);
}
