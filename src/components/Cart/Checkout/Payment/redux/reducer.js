import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_CHECKOUT_PAYMENTS_REQUEST]: (state) => ({
        ...state,
        loader: true,
        payments: {},
        cartData: {},
        shippingAddressData: {},
        shippingMethodData: {},
        setAndPlaceLoader: false,
        slot_avl_flag: true,
        slot_avl_message: '',
        credit_cards: [],
        requestFromApplePaySuccess: false,
    }),
    [types.GET_CHECKOUT_PAYMENTS_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
        requestFromApplePaySuccess: false,
    }),
    [types.GET_CHECKOUT_PAYMENTS_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,

        payments: payload && payload.payment,
        cartData: payload && payload.cartData,
        shippingAddressData: payload && payload.shippingAddressData,
        shippingMethodData: payload && payload.shippingMethodData,
        credit_cards: payload && payload.credit_cards,
    }),

    [types.SET_CHECKOUT_PAYMENT_REQUEST]: (state) => ({
        ...state,
        setAndPlaceLoader: true,
        requestFromApplePaySuccess: false,
    }),
    [types.SET_CHECKOUT_PAYMENT_REQUEST_FAILED]: (state) => ({
        ...state,
        setAndPlaceLoader: false,
        requestFromApplePaySuccess: false,
    }),
    [types.SET_CHECKOUT_PAYMENT_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        setAndPlaceLoader: false,
        slot_avl_flag: payload.slot_avl_flag,
        slot_avl_message: payload.slot_avl_message,
        requestFromApplePaySuccess: payload.requestFromApplePaySuccess,
    }),

    [types.PLACE_ORDER_REQUEST]: (state) => ({
        ...state,
        loader: true,
        setAndPlaceLoader: true,
    }),
    [types.PLACE_ORDER_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
        setAndPlaceLoader: false,
    }),
    [types.PLACE_ORDER_REQUEST_SUCCESS]: (state) => ({
        ...state,
        loader: false,
        setAndPlaceLoader: true,
    }),

    [types.GET_APPLY_VOUCHER_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_APPLY_VOUCHER_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loader: false,
        voucher: payload.voucode,
        vaoucherMessage: '',
        voucherErrorMsg: payload.message,
    }),
    [types.GET_APPLY_VOUCHER_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        voucher: payload.voucode,
        voucherErrorMsg: '',
        vaoucherMessage: payload.message,
        removeVoucher: payload.removeVoucher,
    }),

    [types.GET_REMOVE_VOUCHER_REQUEST]: (state) => ({
        ...state,
        loader: true,
        removeVoucher: true,
    }),
    [types.SET_APPLE_PAY_PLACE_ORDER_REQUEST]: (state) => ({
        ...state,
        setAndApplePayPlaceLoader: true,
    }),
    [types.SET_APPLE_PAY_PLACE_ORDER_REQUEST_FAILED]: (state) => ({
        ...state,
        setAndApplePayPlaceLoader: false,
    }),
    [types.SET_APPLE_PAY_PLACE_ORDER_REQUEST_SUCCESS]: (state) => ({
        ...state,
        setAndApplePayPlaceLoader: false,
    }),
    [types.DELETE_SAVED_CARD_REQ]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.DELETE_SAVED_CARD_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.DELETE_SAVED_CARD_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        credit_cards: payload.updated_credit_cards,
    }),
    [types.PAYFORT_RESTORE_QUOTE_REQ]: (state) => ({
        ...state,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    setAndPlaceLoader: false,
    setAndApplePayPlaceLoader: false,
    payments: {},
    cartData: {},
    shippingAddressData: {},
    shippingMethodData: {},
    vaoucherMessage: '',
    voucherErrorMsg: '',
    voucher: '',
    removeVoucher: false,
    slot_avl_flag: true,
    requestFromApplePaySuccess: false,
    slot_avl_message: '',
    credit_cards: [],
});
