import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { actions as checkoutPaymentActions } from '../../redux/actions';
import CloseIcon from '@material-ui/icons/Close';

export default function CheckoutVoucher({ setIsCod }) {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    const [voucher, setVoucher] = useState('');
    const [onType, setOnType] = useState(false);
    const [isValidate, setIsvalidate] = useState(false);
    const store_id = state && state.auth && state.auth.currentStore;
    const quote_id = state && state.auth && state.auth.quote_id;
    const language = state && state.auth && state.auth.language;
    const voucherValue = state && state.checkoutPayment && state.checkoutPayment.voucher;
    const voucherCode =
        state && state.checkoutPayment && state.checkoutPayment.cartData && state.checkoutPayment.cartData.voucher_code;
    const removeVoucherValue = state && state.checkoutPayment && state.checkoutPayment.removeVoucher;
    const vaoucherMessage = state && state.checkoutPayment && state.checkoutPayment.vaoucherMessage;
    const voucherErrorMsg = state && state.checkoutPayment && state.checkoutPayment.voucherErrorMsg;

    const voucher_discount =
        (state &&
            state.checkoutPayment &&
            state.checkoutPayment.cartData &&
            state.checkoutPayment.cartData.voucher_discount) ||
        0;

    const applyVoucher = () => {
        setIsvalidate(true);
        if (!voucher || voucher === '') {
            return;
        }
        try {
            setIsCod(false);
            dispatch(
                checkoutPaymentActions.getApplyVoucherRequest({
                    quoteid: quote_id,
                    store: store_id,
                    voucode: voucher.toUpperCase(),
                }),
            );
        } catch (err) {}
    };

    const removeVoucher = () => {
        try {
            setIsCod(false);
            dispatch(
                checkoutPaymentActions.getRemoveVoucherRequest({
                    quoteid: quote_id,
                    store: store_id,
                    voucode: voucher.toUpperCase(),
                }),
            );
        } catch (err) {}
    };

    useEffect(() => {
        try {
            if (voucherValue !== '' && voucher === '' && removeVoucherValue === false && !onType) {
                setVoucher(voucherValue);
            }
        } catch (err) {}
    }, [voucher, voucherValue, removeVoucherValue, onType]);

    useEffect(() => {
        if (voucherCode) {
            setVoucher(voucherCode);
        }
    }, [voucherCode]);

    return (
        <Grid container>
            <Grid item xs={12}>
                <Typography className="voucher-text">
                    <FormattedMessage id="Checkout.Payment.ApplyYourVoucher" defaultMessage="Apply your Voucher" />
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Grid container>
                    <Grid item xs={8} md={4}>
                        <TextField
                            variant="outlined"
                            disabled={voucher_discount !== 0 || voucher_discount > 0}
                            value={voucher}
                            onChange={(e) => {
                                setVoucher(e.target.value.toUpperCase());
                                setOnType(true);
                            }}
                            fullWidth
                            placeholder={language === 'en' ? 'Enter Promo Code' : 'أدخل رمز الخصم'}
                            className="inputBox voucher_code"
                            error={isValidate && voucher === ''}
                        />
                        {isValidate && voucher === '' && (
                            <FormHelperText className="input-error">
                                <FormattedMessage
                                    id="Checkout.Payment.Voucher.Error"
                                    defaultMessage="Please enter voucher code"
                                />
                            </FormHelperText>
                        )}

                        {vaoucherMessage !== '' && (
                            <FormHelperText className="greenColor">
                                <span style={{ textAlign: 'initial' }} className="greenColor">
                                    {vaoucherMessage}
                                </span>
                            </FormHelperText>
                        )}

                        {voucherErrorMsg !== '' && (
                            <FormHelperText className="input-error">
                                <span style={{ textAlign: 'initial' }}>{voucherErrorMsg}</span>
                            </FormHelperText>
                        )}

                        {(voucher_discount !== 0 || voucher_discount > 0) && voucher !== '' && (
                            <FormHelperText className="input-error">
                                <Button
                                    style={{ textAlign: 'initial', justifyContent: 'space-between' }}
                                    onClick={() => removeVoucher()}
                                    className="voucher-text-button"
                                    startIcon={<CloseIcon />}
                                >
                                    {voucher}&nbsp;-&nbsp;
                                    <FormattedMessage id="Checkout.Payment.Voucher.Applied" defaultMessage="Applied" />
                                </Button>
                            </FormHelperText>
                        )}

                        {(voucher_discount !== 0 || voucher_discount > 0) && voucher === '' && (
                            <FormHelperText className="input-error">
                                <Button className="voucher-text-button">
                                    <FormattedMessage
                                        id="Checkout.Cart.Charge.VoucherDiscount"
                                        defaultMessage="Voucher Discount"
                                    />
                                    &nbsp;-&nbsp;
                                    <FormattedMessage id="Checkout.Payment.Voucher.Applied" defaultMessage="Applied" />
                                </Button>
                            </FormHelperText>
                        )}
                    </Grid>

                    <Grid item xs={4} md={4} className="arabic-right">
                        <Button
                            disabled={voucher_discount !== 0 || voucher_discount > 0}
                            className={
                                voucher_discount !== 0 || voucher_discount > 0
                                    ? 'checkout-apply-button-disable'
                                    : 'checkout-apply-button'
                            }
                            onClick={() => applyVoucher()}
                        >
                            <FormattedMessage id="Checkout.Payment.Apply" defaultMessage="Apply" />
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
