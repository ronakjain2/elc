import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import InputAdornment from '@material-ui/core/InputAdornment';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import TextField from '@material-ui/core/TextField';
import api from 'apiServices';
import { actions as paymentActions } from 'components/Cart/Checkout/Payment/redux/actions';
import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import '../../payment.css';

const Luhn = require('luhn-js');
export default function CheckoutPaymentOption({ setIsCod }) {
    const state = useSelector((state) => state);
    const country = state && state.auth && state.auth.country;

    const paymentOption = [
        {
            name: (
                <FormattedMessage
                    id="Checkout.Payemnt.CreditDebitCard"
                    defaultMessage="mada bank card / credit cards"
                />
            ),
            image: '/images/checkout/Icon_Checkout_ProgressBar_Payment_Grey.svg',
            code: 'payfort_fort_cc',
            activeImage: '/images/checkout/Icon_Checkout_ProgressBar_Payment_Green.svg',
        },
        {
            name: <FormattedMessage id="Checkout.Payemnt.CashOnDelivery" defaultMessage="Cash On Delivery" />,
            image: country === 'UAE' ? '/images/checkout/cod_uae.svg' : '/images/checkout/cod_ksa.svg',
            code: 'cashondelivery',
            activeImage:
                country === 'UAE' ? '/images/checkout/cod_active_uae.svg' : '/images/checkout/cod_active_ksa.svg',
        },
        {
            name: <FormattedMessage id="Checkout.Payment.BuyNowPayLater" defaultMessage="Buy Now / Pay Later" />,
            image: '/images/checkout/Icon_Checkout_Payment_PayLater_Grey.svg',
            code: '',
            activeImage: '/images/checkout/Icon_Checkout_Payment_PayLater_Green.svg',
        },
    ];

    const dispatch = useDispatch();

    const payments = state && state.checkoutPayment && state.checkoutPayment.payments;
    const store_id = state && state.auth && state.auth.currentStore;
    const language = state && state.auth && state.auth.language;
    const quote_id = state && state.auth && state.auth.quote_id;
    const store_locale = state && state.auth && state.auth.store_locale;
    const grand_total =
        state &&
        state.checkoutPayment &&
        state.checkoutPayment.cartData &&
        state.checkoutPayment.cartData &&
        state.checkoutPayment.cartData.grand_total;
    const loginUser = state && state.auth && state.auth.loginUser;
    const [selectedCode, setSelectedCode] = useState('payfort_fort_cc');
    const [isDeviceSupportForApple, setIsDeviceSupportForApple] = useState(false);

    const setPaymentCode = (code) => {
        if (payments[code]) {
            setSelectedCode(code);
            setIsCod(code === 'cashondelivery');
        }
    };

    useEffect(() => {
        try {
            if (window.ApplePaySession) {
                if (window.ApplePaySession.canMakePayments()) {
                    setIsDeviceSupportForApple(true);
                }
            }
        } catch (err) {}
    }, []);

    const showMaxCODValue = () => {
        if (
            payments[selectedCode] &&
            payments[selectedCode].maximum_order_value &&
            parseFloat(payments[selectedCode].maximum_order_value) <= payments[selectedCode].total
        ) {
            return true;
        }

        return false;
    };

    useEffect(() => {
        try {
            if (payments) {
                if (payments.payfort_fort_cc) {
                    setSelectedCode('payfort_fort_cc');
                } else if (payments.cashondelivery) {
                    setSelectedCode('cashondelivery');
                } else {
                    setSelectedCode('');
                }
            } else {
                setSelectedCode('');
            }
        } catch (err) {}
    }, [payments]);

    const returnAnimate = () => {
        const elmnt = document.querySelector('.Mui-error');
        elmnt && elmnt.scrollIntoView({ block: 'center', behavior: 'smooth' });
        return;
    };

    const onClickpayNow = () => {
        if (selectedCode === '') {
            return;
        }

        if (selectedCode === 'payfort_fort_cc') {
            // If cards details are invalid simply returning
            if (
                otherCard &&
                !(card_holder_name_valid && card_number_valid && card_security_code_valid && expiry_date_valid)
            ) {
                return returnAnimate();
            } else if (card) {
                if (card.cvv) {
                    if (card.cvv.length !== 3) return returnAnimate();
                } else {
                    return returnAnimate();
                }
            } else if (!otherCard && !card) {
                return returnAnimate();
            }
        }

        let cardDetails = {};

        if (selectedCode === 'payfort_fort_cc') {
            if (otherCard) {
                cardDetails = {
                    type: 'other',
                    card_holder_name: card_holder_name,
                    card_number: card_number,
                    expiry_date: expiry_date.split('/').reverse().join(''),
                    card_security_code: card_security_code,
                    save_card: save_card,
                };
            } else if (card) {
                cardDetails = {
                    type: 'saved',
                    token: card.token,
                    card_security_code: card.cvv,
                };
            }
        }

        dispatch(
            paymentActions.setCheckoutPaymentRequest({
                quote_id,
                store_id,
                cardDetails,
                payment_code: selectedCode,
            }),
        );
    };

    const onApplePayButtonClick = () => {
        const requestApplePaySession = {
            countryCode: country === 'UAE' ? 'AE' : 'SA',
            currencyCode: country === 'UAE' ? 'AED' : 'SAR',
            merchantCapabilities: ['supports3DS'],
            supportedNetworks: ['visa', 'masterCard', 'mada'],
            merchantIdentifier: 'merchant.com.koj',
            total: {
                label: 'Delivery',
                type: 'final',
                amount: 0,
            },
        };
        dispatch(
            paymentActions.setCheckoutPaymentRequest({
                quote_id,
                store_id,
                payment_code: 'applepay',
                requestFromApplePay: true,
            }),
        );
        startApplePaySession(requestApplePaySession);
    };

    const startApplePaySession = (requestParamterForApplePaySession) => {
        const APPLE_PAY_VERSION_NUMBER = 6;
        requestParamterForApplePaySession.total.amount = grand_total;

        var applePaySession = new window.ApplePaySession(APPLE_PAY_VERSION_NUMBER, requestParamterForApplePaySession);
        _handleApplePayEvents(applePaySession);
        applePaySession.begin();
    };

    const _handleApplePayEvents = (applePaySession) => {
        applePaySession.onvalidatemerchant = (event) => {
            _validateApplePaySession(event.validationURL, (merchantSession) => {
                applePaySession.completeMerchantValidation(merchantSession);
            });
        };

        applePaySession.onpaymentauthorized = (event) => {
            if (event.payment && event.payment.token && event.payment.token.paymentData) {
                applePaySession.completePayment(applePaySession.STATUS_SUCCESS);
                _performTransaction(event.payment.token.paymentData, event.payment.token.paymentMethod);
            } else {
                // Release payment from Apple Pay
                applePaySession.completePayment(applePaySession.STATUS_FAILURE);
            }
        };
    };

    const _performTransaction = (paymentData, paymentMethod) => {
        const requestDataForApplePayPlaceOrder = {
            store_id: state.auth.currentStore,
            quote_id: state && state.auth && state.auth.quote_id,
            digital_wallet: 'APPLE_PAY',
            apple_data: paymentData.data,
            apple_signature: paymentData.signature,
            //  apple_header: '',
            apple_transactionId: paymentData.header.transactionId,
            apple_ephemeralPublicKey: paymentData.header.ephemeralPublicKey,
            apple_publicKeyHash: paymentData.header.publicKeyHash,
            // apple_paymentMethod: '',
            apple_displayName: paymentMethod.displayName,
            apple_network: paymentMethod.network,
            apple_type: paymentMethod.type,
            apple_applicationData: '',
        };
        dispatch(paymentActions.setApplePayPlaceOrderRequest(requestDataForApplePayPlaceOrder));
    };

    const _validateApplePaySession = (store_id, callback) => {
        // I'm using AXIOS to do a POST request to the backend but any HTTP client can be used
        let payload = {
            store_id: state.auth.currentStore,
        };
        api.post(`/index.php/rest/V1/app/applepaysession`, payload)
            .then((response) => {
                // do stuff
                callback(response.data);
            })
            .catch((err) => {
                // what now?
            });
    };

    const termsAndCondition = () => {
        return (
            <>
                <div className="border-line" />
                <Typography className="tearmAndCondition">
                    <FormattedMessage
                        id="Checkout.Payment.termsAndConditions.Text"
                        defaultMessage="By placing your order, you agree to our"
                    />
                    &nbsp;
                    <Link to={`/${store_locale}/privacy-policy`}>
                        <FormattedMessage
                            id="Checkout.Payment.PrivacyAndCookiePolicy.Text"
                            defaultMessage="Privacy & Cookie Policy"
                        />
                        &nbsp;
                    </Link>
                    <FormattedMessage id="Checkout.Payment.And.Text" defaultMessage="and" />
                    &nbsp;
                    <Link to={`/${store_locale}/terms-and-conditions`}>
                        <FormattedMessage
                            id="Checkout.Payment.TermsAndConditions.Text"
                            defaultMessage="Terms & Conditions"
                        />
                        &nbsp;
                    </Link>
                </Typography>
            </>
        );
    };

    const [card_holder_name, setcard_holder_name] = useState('');
    const [card_holder_name_valid, setcard_holder_name_valid] = useState(null);
    const [card_number, setcard_number] = useState('');
    const [card_number_valid, setcard_number_valid] = useState(null);
    const [expiry_date, setexpiry_date] = useState('');
    const [expiry_date_valid, setexpiry_date_valid] = useState(null);
    const [card_security_code, setcard_security_code] = useState('');
    const [card_security_code_valid, setcard_security_code_valid] = useState(null);
    const [card_type, setcard_type] = useState(null);
    const [card, setcard] = useState(null);
    const [save_card, setsave_card] = useState(false);
    const [otherCard, setotherCard] = useState(false);
    const [displayOtherCardTag, setdisplayOtherCardTag] = useState(true);

    const { credit_cards } = state && state.checkoutPayment && state.checkoutPayment;

    useEffect(() => {
        if (credit_cards)
            if (credit_cards.length === 0) {
                setdisplayOtherCardTag(false);
                setotherCard(true);
            } else {
                setdisplayOtherCardTag(true);
                setotherCard(false);
            }
        return () => {};
    }, [credit_cards]);

    const checkCardType = (card_number, check_eager_pattern = true) => {
        const Type = require('creditcards-types/type');
        const madaType = Type({
            name: 'Mada',
            digits: 16,
            pattern: /^(4(0(0861|6996|7(197|395)|9201)|1(0621|2565|0685|7633|9593)|2(0132|281(7|8|9)|8(331|67(1|2|3)))|3(1361|2328|4107|9954)|4(0(533|647|795)|5564|6(393|404|672))|5(5(036|708)|7865|7997|8456)|6(2220|854(0|1|2|3))|7(4491)|8(301(0|1|2)|4783|609(4|5|6)|931(7|8|9)))|5(0(4300|6968|8160)|13213|2(0058|1076|4(130|514)|9(415|741))|3(0(060|906)|1(095|196)|2013|5(825|989)|6023|7767)|4(3(085|357)|9760)|5(4180|8563)|8(5265|8(8(4(5|7|8)|5(0|1))|98(2|3))|9(005|206)))|6(0(4906|5141)|36120)|9682(0(1|2|3|4|5|6|7|8|9)|1(1)))\d{10}$/,
            eagerPattern: /^(4(0(0861|6996|7(197|395)|9201)|1(0621|2565|0685|7633|9593)|2(0132|281(7|8|9)|8(331|67(1|2|3)))|3(1361|2328|4107|9954)|4(0(533|647|795)|5564|6(393|404|672))|5(5(036|708)|7865|7997|8456)|6(2220|854(0|1|2|3))|7(4491)|8(301(0|1|2)|4783|609(4|5|6)|931(7|8|9)))|5(0(4300|6968|8160)|13213|2(0058|1076|4(130|514)|9(415|741))|3(0(060|906)|1(095|196)|2013|5(825|989)|6023|7767)|4(3(085|357)|9760)|5(4180|8563)|8(5265|8(8(4(5|7|8)|5(0|1))|98(2|3))|9(005|206)))|6(0(4906|5141)|36120)|9682(0(1|2|3|4|5|6|7|8|9)|1(1)))/,
        });
        const types = [madaType].concat(require('creditcards-types'));
        const type = types.find((type) => type.test(card_number, check_eager_pattern));
        return type && type.name;
    };

    const onChangeHandlePayment = (e) => {
        if (e.target.name === 'expiry_date') {
            if (e.target.value.length === 2 && expiry_date.length === 1) {
                e.target.value += '/';
            }
            if (
                e.target.value.length === 5 &&
                e.target.value.match(/^((0[1-9])|(1[0-2]))[/]*((1[9])|([2-9][0-9]))$/g)
            ) {
                const today = new Date();
                const tempDate = e.target.value.split('/');
                if (parseInt('20' + tempDate[1]) >= today.getFullYear()) {
                    if (
                        parseInt('20' + tempDate[1]) === today.getFullYear() &&
                        parseInt(tempDate[0]) < today.getMonth() + 1
                    ) {
                        setexpiry_date_valid(false);
                    } else {
                        setexpiry_date_valid(true);
                    }
                } else {
                    setexpiry_date_valid(false);
                }
            } else {
                setexpiry_date_valid(false);
            }
            setexpiry_date(e.target.value);
        } else if (e.target.name === 'card_holder_name') {
            if (
                e.target.value.match(/^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g) &&
                e.target.value.match(/^(?!\s*$).+/g)
            ) {
                setcard_holder_name_valid(true);
            } else {
                setcard_holder_name_valid(false);
            }
            setcard_holder_name(e.target.value);
        } else if (e.target.name === 'card_number') {
            const re = /^[0-9]{16}$/;
            if (re.test(e.target.value) && Luhn.isValid(e.target.value)) {
                setcard_number_valid(true);
            } else {
                setcard_number_valid(false);
            }
            setcard_type(checkCardType(e.target.value));
            setcard_number(e.target.value);
        } else if (e.target.name === 'card_security_code') {
            if (e.target.value.length >= 3) {
                setcard_security_code_valid(true);
            } else {
                setcard_security_code_valid(false);
            }
            setcard_security_code(e.target.value);
        }
    };

    const onCardSelected = (card) => {
        setcard(card);
        setotherCard(false);
        setcard_holder_name('');
        setcard_holder_name_valid(null);
        setcard_number('');
        setcard_number_valid(null);
        setexpiry_date('');
        setexpiry_date_valid(null);
        setcard_security_code('');
        setcard_security_code_valid(null);
        setcard_type(null);
        setsave_card(false);
    };

    const onOtherCardSelected = () => {
        setcard(null);
        setotherCard(true);
        setcard_holder_name('');
        setcard_holder_name_valid(null);
        setcard_number('');
        setcard_number_valid(null);
        setexpiry_date('');
        setexpiry_date_valid(null);
        setcard_security_code('');
        setcard_security_code_valid(null);
        setcard_type(null);
        setsave_card(false);
    };

    const getExpiryDate = (date) => {
        if (date) date = `${date.charAt(2)}${date.charAt(3)}/${date.charAt(0)}${date.charAt(1)}`;
        return date;
    };

    return (
        <Grid container className="checkout-payment">
            <Grid item xs={12}>
                <Typography className="delivery-charges">
                    <FormattedMessage
                        id="Checkout.Payment.ChoosePaymentOption"
                        defaultMessage="Choose Payment option"
                    />
                </Typography>
            </Grid>

            <Hidden only={['xs', 'sm']}>
                <Grid item xs={12}>
                    <Grid container>
                        {paymentOption &&
                            paymentOption.map((option, index) => {
                                return (
                                    <Grid
                                        item
                                        xs={4}
                                        key={`payment_option_${index}`}
                                        className={
                                            selectedCode === option.code ? 'padding-l-r active-payment' : 'padding-l-r'
                                        }
                                    >
                                        {payments[option.code] && (
                                            <Paper
                                                className={
                                                    selectedCode === option.code
                                                        ? 'active-arrow checkout-payment-cart'
                                                        : 'checkout-payment-cart'
                                                }
                                                onClick={() => setPaymentCode(option.code)}
                                            >
                                                <Grid container justify="center">
                                                    <Grid item xs={12} className="textAlignCenter">
                                                        <img
                                                            src={
                                                                selectedCode === option.code
                                                                    ? option.activeImage
                                                                    : option.image
                                                            }
                                                            alt="paymentMethodImage"
                                                            className="payment-method-img"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12} className="textAlignCenter">
                                                        <Typography className="methodName">{option.name}</Typography>
                                                    </Grid>
                                                </Grid>
                                            </Paper>
                                        )}
                                    </Grid>
                                );
                            })}

                        <Grid item xs={12}>
                            {showMaxCODValue() ? (
                                <Typography className="input-error" style={{ paddingTop: 20 }}>
                                    <FormattedMessage
                                        id="Checkout.Payment.OrderGreateThanError"
                                        defaultMessage="Total"
                                    />
                                    &nbsp;
                                    <span className="card-value">{payments[selectedCode].maximum_order_value}</span>
                                </Typography>
                            ) : null}
                            {!showMaxCODValue() && payments[selectedCode] && payments[selectedCode].total ? (
                                <Typography className="payment-cart-value">
                                    <FormattedMessage id="Cart.Total" defaultMessage="Total" />
                                    &nbsp;
                                    <span className="card-value">
                                        {payments[selectedCode].currency}&nbsp;
                                        {payments[selectedCode].total}
                                    </span>
                                </Typography>
                            ) : null}
                            {!showMaxCODValue() &&
                            payments[selectedCode] &&
                            payments[selectedCode].charges &&
                            payments[selectedCode].charges > 0 ? (
                                <Typography className="payment-cart-sub">
                                    <FormattedMessage
                                        id="Checkout.Payemnt.CashOnDelivery"
                                        defaultMessage="Cash On Delivery"
                                    />
                                    &nbsp;
                                    <span className="card-value">
                                        {language === 'en' && `${payments[selectedCode].currency} `}
                                        {payments[selectedCode].charges}
                                    </span>
                                    &nbsp;
                                    {language === 'en' ? (
                                        <FormattedMessage id="Checkout.Payment.Included" defaultMessage="included" />
                                    ) : country === 'UAE' ? (
                                        'درهم.'
                                    ) : (
                                        'ريال سعودي.'
                                    )}
                                    &nbsp;
                                </Typography>
                            ) : null}
                            {payments[selectedCode] && payments[selectedCode].instructions && (
                                <Typography className="payment-cart-sub">
                                    {payments[selectedCode].instructions}
                                </Typography>
                            )}
                        </Grid>

                        {/* <PaymentForm /> */}
                        {selectedCode === 'payfort_fort_cc' && (
                            <Grid container>
                                {credit_cards && credit_cards.length > 0 && (
                                    <>
                                        <Grid item xs={12}>
                                            <div className="border-line" />
                                        </Grid>

                                        <Grid item xs={12}>
                                            <Typography className="mb-2 arabic-right font-weight-600">
                                                <FormattedMessage
                                                    id="Checkout.Payment.SavedCards"
                                                    defaultMessage="Saved Cards"
                                                />
                                            </Typography>
                                        </Grid>

                                        {credit_cards &&
                                            credit_cards.map((_card) => {
                                                return (
                                                    <Grid
                                                        container
                                                        spacing={1}
                                                        direction="row"
                                                        alignItems="center"
                                                        className="arabic-right"
                                                        onClick={() => onCardSelected(_card)}
                                                    >
                                                        <Grid item xs={1}>
                                                            <Checkbox checked={card && card.id === _card.id}></Checkbox>
                                                        </Grid>
                                                        <Grid item xs={1}>
                                                            {checkCardType(_card.card_number) === 'Visa' ? (
                                                                <img src="/images/checkout/card-visa.svg" alt="card" />
                                                            ) : checkCardType(_card.card_number) === 'Mada' ? (
                                                                <img src="/images/checkout/card-mada.svg" alt="card" />
                                                            ) : checkCardType(_card.card_number) === 'Mastercard' ? (
                                                                <img
                                                                    src="/images/checkout/card-mastercard.svg"
                                                                    alt="card"
                                                                />
                                                            ) : (
                                                                <img src="/images/checkout/card.svg" alt="card" />
                                                            )}
                                                        </Grid>
                                                        <Grid item xs={3}>
                                                            {_card.name_on_card}
                                                        </Grid>
                                                        <Grid item xs={3} className="dir-ltr">
                                                            {_card.card_number}
                                                        </Grid>
                                                        <Grid item xs={2}>
                                                            {getExpiryDate(_card.expiry_date)}
                                                        </Grid>
                                                        <Grid item xs={2}>
                                                            <TextField
                                                                id="CVV"
                                                                label={
                                                                    <FormattedMessage
                                                                        id="paybycard.CVV"
                                                                        defaultMessage="CVV"
                                                                    />
                                                                }
                                                                variant="outlined"
                                                                fullWidth
                                                                size="small"
                                                                error={
                                                                    card && card.id === _card.id
                                                                        ? card.cvv
                                                                            ? card.cvv.length < 3
                                                                            : true
                                                                        : false
                                                                }
                                                                inputProps={{ maxLength: 3 }}
                                                                onChange={(e) => {
                                                                    setcard({ ...card, cvv: e.target.value });
                                                                }}
                                                                disabled={card && card.id !== _card.id}
                                                                value={card && card.id === _card.id ? card.cvv : ''}
                                                                onInput={(e) =>
                                                                    (e.target.value = e.target.value.replace(/\D/g, ''))
                                                                }
                                                            />
                                                        </Grid>
                                                    </Grid>
                                                );
                                            })}
                                    </>
                                )}

                                {displayOtherCardTag && (
                                    <Grid
                                        container
                                        spacing={1}
                                        alignItems="center"
                                        className="arabic-right"
                                        onClick={onOtherCardSelected}
                                    >
                                        <Grid item xs={1}>
                                            <Checkbox checked={otherCard} name="useOtherCard" />
                                        </Grid>
                                        <Grid item xs={11}>
                                            <FormattedMessage
                                                id="paybycard.UseOtherCard"
                                                defaultMessage="Use other card"
                                            />
                                        </Grid>
                                    </Grid>
                                )}

                                {otherCard && (
                                    <>
                                        <Grid item xs={12}>
                                            <div className="border-line" />
                                        </Grid>

                                        <Grid item xs={12} className="my-2 arabic-right">
                                            <Typography className="mb-2 font-weight-600">
                                                <FormattedMessage
                                                    id="Checkout.Payment.CardDetails"
                                                    defaultMessage="Card Details"
                                                />
                                            </Typography>
                                        </Grid>

                                        <Grid container spacing={4} className="arabic-right">
                                            <Grid item xs={12}>
                                                <Typography className="form-label">
                                                    <FormattedMessage
                                                        id="paybycard.NameOnCard"
                                                        defaultMessage="Name on card"
                                                    />
                                                </Typography>
                                                <TextField
                                                    id="Card-Holders-Name"
                                                    variant="outlined"
                                                    fullWidth
                                                    error={!card_holder_name_valid}
                                                    inputProps={{
                                                        maxLength: 50,
                                                        name: 'card_holder_name',
                                                    }}
                                                    onChange={onChangeHandlePayment}
                                                    onInput={(e) =>
                                                        (e.target.value = e.target.value.replace(/[^a-zA-Z\s]/g, ''))
                                                    }
                                                />
                                                {card_holder_name && !card_holder_name_valid && (
                                                    <FormHelperText className="input-error">
                                                        <FormattedMessage
                                                            id="paybycard.NameError"
                                                            defaultMessage="Card holder's name is invalid"
                                                        />
                                                    </FormHelperText>
                                                )}
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography className="form-label">
                                                    <FormattedMessage
                                                        id="paybycard.CardNumber"
                                                        defaultMessage="Card Number"
                                                    />
                                                </Typography>
                                                <TextField
                                                    id="Card-Number"
                                                    variant="outlined"
                                                    fullWidth
                                                    error={!card_number_valid}
                                                    inputProps={{ maxLength: 16, name: 'card_number' }}
                                                    InputProps={{
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                {card_type === 'Visa' ? (
                                                                    <img
                                                                        src="/images/checkout/card-visa.svg"
                                                                        alt="card"
                                                                    />
                                                                ) : card_type === 'Mada' ? (
                                                                    <img
                                                                        src="/images/checkout/card-mada.svg"
                                                                        alt="card"
                                                                    />
                                                                ) : card_type === 'Mastercard' ? (
                                                                    <img
                                                                        src="/images/checkout/card-mastercard.svg"
                                                                        alt="card"
                                                                    />
                                                                ) : (
                                                                    <img src="/images/checkout/card.svg" alt="card" />
                                                                )}
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                    onChange={onChangeHandlePayment}
                                                    onInput={(e) =>
                                                        (e.target.value = e.target.value.replace(/\D/g, ''))
                                                    }
                                                />
                                                {card_number && !card_number_valid && (
                                                    <FormHelperText className="input-error">
                                                        <FormattedMessage
                                                            id="paybycard.CardNumberError"
                                                            defaultMessage="Card number is invalid"
                                                        />
                                                    </FormHelperText>
                                                )}
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <Typography className="form-label">
                                                    <FormattedMessage
                                                        id="paybycard.ExpiryDate"
                                                        defaultMessage="Expiry Date"
                                                    />
                                                </Typography>
                                                <TextField
                                                    id="Expiry-Date"
                                                    error={!expiry_date_valid}
                                                    variant="outlined"
                                                    fullWidth
                                                    placeholder="MM/YY"
                                                    inputProps={{ maxLength: 5, name: 'expiry_date' }}
                                                    onChange={onChangeHandlePayment}
                                                    onInput={(e) =>
                                                        (e.target.value = e.target.value.replace(/[^\d/]/g, ''))
                                                    }
                                                />
                                                {expiry_date && !expiry_date_valid && (
                                                    <FormHelperText className="input-error">
                                                        <FormattedMessage
                                                            id="paybycard.ExpiryDateError"
                                                            defaultMessage="Invalid expiry date"
                                                        />
                                                    </FormHelperText>
                                                )}
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <Typography className="form-label">
                                                    <FormattedMessage id="paybycard.CVV" defaultMessage="CVV" />
                                                </Typography>
                                                <TextField
                                                    id="CVV"
                                                    variant="outlined"
                                                    fullWidth
                                                    error={!card_security_code_valid}
                                                    inputProps={{
                                                        maxLength: 3,
                                                        name: 'card_security_code',
                                                    }}
                                                    onInput={(e) =>
                                                        (e.target.value = e.target.value.replace(/\D/g, ''))
                                                    }
                                                    onChange={onChangeHandlePayment}
                                                />
                                                {card_security_code && !card_security_code_valid && (
                                                    <FormHelperText className="input-error">
                                                        <FormattedMessage
                                                            id="paybycard.CVVError"
                                                            defaultMessage="Invalid CVV"
                                                        />{' '}
                                                    </FormHelperText>
                                                )}
                                            </Grid>
                                            {loginUser && (
                                                <Grid
                                                    item
                                                    xs={12}
                                                    className="arabic-right"
                                                    onClick={() => {
                                                        if (credit_cards && credit_cards.length < 3)
                                                            setsave_card(!save_card);
                                                    }}
                                                >
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox checked={save_card} name="save-card-details" />
                                                        }
                                                        label={
                                                            <>
                                                                <FormattedMessage
                                                                    id="paybycard.SaveCardDetails"
                                                                    defaultMessage="Save card details"
                                                                />
                                                                &nbsp;
                                                                {credit_cards && credit_cards.length >= 3 && (
                                                                    <>
                                                                        (
                                                                        <FormattedMessage
                                                                            id="Youcansaveonly3cardsRemoveanothercardtosavethis"
                                                                            defaultMessage="You can save only 3 cards, Remove another card to save this."
                                                                        />
                                                                        &nbsp;
                                                                        <Link to={`/${store_locale}/my-cards`}>
                                                                            <FormattedMessage
                                                                                id="paybycard.manageCards"
                                                                                defaultMessage="Manage Cards"
                                                                            />
                                                                        </Link>
                                                                        )
                                                                    </>
                                                                )}
                                                            </>
                                                        }
                                                    />
                                                </Grid>
                                            )}
                                        </Grid>
                                    </>
                                )}
                            </Grid>
                        )}
                        {/* <PaymentForm /> */}

                        <Grid item xs={12}>
                            {termsAndCondition()}
                        </Grid>

                        <Grid container>
                            <Grid item xs={6} md={3} className="text-align-rtl">
                                {selectedCode !== 'cashondelivery' && (
                                    <Button
                                        className="pay-now-button"
                                        disabled={showMaxCODValue()}
                                        onClick={() => onClickpayNow()}
                                    >
                                        <FormattedMessage id="Checkout.Payment.PayNow" defaultMessage="Pay Now" />
                                    </Button>
                                )}
                                {selectedCode === 'cashondelivery' && (
                                    <Button
                                        className="pay-now-button"
                                        disabled={showMaxCODValue()}
                                        onClick={() => onClickpayNow()}
                                    >
                                        <FormattedMessage
                                            id="Checkout.Payment.PlaceOrder"
                                            defaultMessage="Place Order"
                                        />
                                    </Button>
                                )}
                            </Grid>
                            <Grid style={{ marginTop: 20, textAlign: 'initial' }} item md={3}>
                                {isDeviceSupportForApple && (
                                    <button
                                        onClick={() => onApplePayButtonClick()}
                                        className="apple-pay-button-with-text apple-pay-button-black-with-text"
                                    >
                                        <span className="text">Buy with</span>
                                        <span className="logo"></span>
                                    </button>
                                )}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                <Grid item xs={12}>
                    <Grid container>
                        {paymentOption &&
                            paymentOption.map((option, index) => {
                                return (
                                    <Grid item xs={12} key={`payment_option_mobile_${index}`}>
                                        {payments[option.code] && (
                                            <Grid container alignItems="center" className="payment-option-container">
                                                <Grid item xs={2}>
                                                    <Radio
                                                        checked={selectedCode === option.code}
                                                        onChange={() => setPaymentCode(option.code)}
                                                    />
                                                </Grid>
                                                <Grid item xs={4} className="textAlignCenter">
                                                    <img
                                                        src={
                                                            selectedCode === option.code
                                                                ? option.activeImage
                                                                : option.image
                                                        }
                                                        alt="paymentMethodImage"
                                                        className="payment-method-img"
                                                    />
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <Typography
                                                        className={
                                                            selectedCode === option.code
                                                                ? 'methodName activeName'
                                                                : 'methodName'
                                                        }
                                                    >
                                                        {option.name}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        )}
                                        {payments[option.code] && option.code === selectedCode && (
                                            <Grid item xs={12}>
                                                {showMaxCODValue() ? (
                                                    <Typography className="input-error" style={{ paddingTop: 20 }}>
                                                        <FormattedMessage
                                                            id="Checkout.Payment.OrderGreateThanError"
                                                            defaultMessage="Total"
                                                        />
                                                        &nbsp;
                                                        <span className="card-value">
                                                            {payments[selectedCode].maximum_order_value}
                                                        </span>
                                                    </Typography>
                                                ) : null}
                                                {!showMaxCODValue() &&
                                                payments[selectedCode] &&
                                                payments[selectedCode].total ? (
                                                    <Typography className="payment-cart-value">
                                                        <FormattedMessage id="Cart.Total" defaultMessage="Total" />
                                                        &nbsp;
                                                        <span className="card-value">
                                                            {payments[selectedCode].currency}&nbsp;
                                                            {payments[selectedCode].total}
                                                        </span>
                                                    </Typography>
                                                ) : null}
                                                {!showMaxCODValue() &&
                                                payments[selectedCode] &&
                                                payments[selectedCode].charges &&
                                                payments[selectedCode].charges > 0 ? (
                                                    <Typography className="payment-cart-sub">
                                                        <FormattedMessage
                                                            id="Checkout.Payemnt.CashOnDelivery"
                                                            defaultMessage="Cash On Delivery"
                                                        />
                                                        &nbsp;
                                                        <span className="card-value">
                                                            {language === 'en' && `${payments[selectedCode].currency} `}
                                                            {payments[selectedCode].charges}
                                                        </span>
                                                        &nbsp;
                                                        {language === 'en' ? (
                                                            <FormattedMessage
                                                                id="Checkout.Payment.Included"
                                                                defaultMessage="included"
                                                            />
                                                        ) : country === 'UAE' ? (
                                                            'درهم.'
                                                        ) : (
                                                            'ريال سعودي.'
                                                        )}
                                                        &nbsp;
                                                    </Typography>
                                                ) : null}
                                                {payments[selectedCode] && payments[selectedCode].instructions && (
                                                    <Typography className="payment-cart-sub">
                                                        {payments[selectedCode].instructions}
                                                    </Typography>
                                                )}
                                            </Grid>
                                        )}

                                        {/* <PaymentForm /> */}
                                        {payments[option.code] &&
                                            option.code === selectedCode &&
                                            selectedCode === 'payfort_fort_cc' && (
                                                <Grid container>
                                                    {credit_cards && credit_cards.length > 0 && (
                                                        <>
                                                            <Grid item xs={12}>
                                                                <div className="border-line" />
                                                            </Grid>

                                                            <Grid item xs={12}>
                                                                <Typography className="mb-2 arabic-right font-weight-600">
                                                                    <FormattedMessage
                                                                        id="Checkout.Payment.SavedCards"
                                                                        defaultMessage="Saved Cards"
                                                                    />
                                                                </Typography>
                                                            </Grid>

                                                            {credit_cards &&
                                                                credit_cards.map((_card) => {
                                                                    return (
                                                                        <>
                                                                            <Grid
                                                                                container
                                                                                spacing={1}
                                                                                direction="row"
                                                                                alignItems="center"
                                                                                className="arabic-right"
                                                                                onClick={() => onCardSelected(_card)}
                                                                            >
                                                                                <Grid item xs={2}>
                                                                                    <Checkbox
                                                                                        checked={
                                                                                            card && card.id === _card.id
                                                                                        }
                                                                                    ></Checkbox>
                                                                                </Grid>
                                                                                <Grid item xs={3}>
                                                                                    {checkCardType(
                                                                                        _card.card_number,
                                                                                    ) === 'Visa' ? (
                                                                                        <img
                                                                                            src="/images/checkout/card-visa.svg"
                                                                                            alt="card"
                                                                                        />
                                                                                    ) : checkCardType(
                                                                                          _card.card_number,
                                                                                      ) === 'Mada' ? (
                                                                                        <img
                                                                                            src="/images/checkout/card-mada.svg"
                                                                                            alt="card"
                                                                                        />
                                                                                    ) : checkCardType(
                                                                                          _card.card_number,
                                                                                      ) === 'Mastercard' ? (
                                                                                        <img
                                                                                            src="/images/checkout/card-mastercard.svg"
                                                                                            alt="card"
                                                                                        />
                                                                                    ) : (
                                                                                        <img
                                                                                            src="/images/checkout/card.svg"
                                                                                            alt="card"
                                                                                        />
                                                                                    )}
                                                                                </Grid>
                                                                                <Grid item xs={7}>
                                                                                    {_card.name_on_card}
                                                                                </Grid>
                                                                                <Grid item xs={1}></Grid>
                                                                                <Grid item xs={5} className="dir-ltr">
                                                                                    {_card.card_number}
                                                                                </Grid>
                                                                                <Grid item xs={2}>
                                                                                    {getExpiryDate(_card.expiry_date)}
                                                                                </Grid>
                                                                                <Grid item xs={4}>
                                                                                    <TextField
                                                                                        id="CVV"
                                                                                        label={
                                                                                            <FormattedMessage
                                                                                                id="paybycard.CVV"
                                                                                                defaultMessage="CVV"
                                                                                            />
                                                                                        }
                                                                                        variant="outlined"
                                                                                        fullWidth
                                                                                        size="small"
                                                                                        error={
                                                                                            card && card.id === _card.id
                                                                                                ? card.cvv
                                                                                                    ? card.cvv.length <
                                                                                                      3
                                                                                                    : true
                                                                                                : false
                                                                                        }
                                                                                        inputProps={{ maxLength: 3 }}
                                                                                        onChange={(e) => {
                                                                                            setcard({
                                                                                                ...card,
                                                                                                cvv: e.target.value,
                                                                                            });
                                                                                        }}
                                                                                        disabled={
                                                                                            card && card.id !== _card.id
                                                                                        }
                                                                                        value={
                                                                                            card && card.id === _card.id
                                                                                                ? card.cvv
                                                                                                : ''
                                                                                        }
                                                                                        onInput={(e) =>
                                                                                            (e.target.value = e.target.value.replace(
                                                                                                /\D/g,
                                                                                                '',
                                                                                            ))
                                                                                        }
                                                                                    />
                                                                                </Grid>
                                                                            </Grid>
                                                                            <Grid item xs={12}>
                                                                                <div className="border-line my-2" />
                                                                            </Grid>
                                                                        </>
                                                                    );
                                                                })}
                                                        </>
                                                    )}

                                                    {displayOtherCardTag && (
                                                        <Grid
                                                            container
                                                            spacing={1}
                                                            alignItems="center"
                                                            className="arabic-right"
                                                            onClick={onOtherCardSelected}
                                                        >
                                                            <Grid item xs={2}>
                                                                <Checkbox checked={otherCard} name="useOtherCard" />
                                                            </Grid>
                                                            <Grid item xs={10}>
                                                                <FormattedMessage
                                                                    id="paybycard.UseOtherCard"
                                                                    defaultMessage="Use other card"
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                    )}

                                                    {otherCard && (
                                                        <>
                                                            <Grid item xs={12}>
                                                                <div className="border-line" />
                                                            </Grid>

                                                            <Grid item xs={12} className="my-2 arabic-right">
                                                                <Typography className="mb-2 font-weight-600">
                                                                    <FormattedMessage
                                                                        id="Checkout.Payment.CardDetails"
                                                                        defaultMessage="Card Details"
                                                                    />
                                                                </Typography>
                                                            </Grid>

                                                            <Grid container spacing={4} className="arabic-right">
                                                                <Grid item xs={12}>
                                                                    <Typography className="form-label">
                                                                        <FormattedMessage
                                                                            id="paybycard.NameOnCard"
                                                                            defaultMessage="Name on card"
                                                                        />
                                                                    </Typography>
                                                                    <TextField
                                                                        id="Card-Holders-Name"
                                                                        variant="outlined"
                                                                        fullWidth
                                                                        error={!card_holder_name_valid}
                                                                        inputProps={{
                                                                            maxLength: 50,
                                                                            name: 'card_holder_name',
                                                                        }}
                                                                        onChange={onChangeHandlePayment}
                                                                        onInput={(e) =>
                                                                            (e.target.value = e.target.value.replace(
                                                                                /[^a-zA-Z\s]/g,
                                                                                '',
                                                                            ))
                                                                        }
                                                                    />
                                                                    {card_holder_name && !card_holder_name_valid && (
                                                                        <FormHelperText className="input-error">
                                                                            <FormattedMessage
                                                                                id="paybycard.NameError"
                                                                                defaultMessage="Card holder's name is invalid"
                                                                            />
                                                                        </FormHelperText>
                                                                    )}
                                                                </Grid>
                                                                <Grid item xs={12}>
                                                                    <Typography className="form-label">
                                                                        <FormattedMessage
                                                                            id="paybycard.CardNumber"
                                                                            defaultMessage="Card Number"
                                                                        />
                                                                    </Typography>
                                                                    <TextField
                                                                        id="Card-Number"
                                                                        variant="outlined"
                                                                        fullWidth
                                                                        error={!card_number_valid}
                                                                        inputProps={{
                                                                            maxLength: 16,
                                                                            name: 'card_number',
                                                                        }}
                                                                        InputProps={{
                                                                            endAdornment: (
                                                                                <InputAdornment position="end">
                                                                                    {card_type === 'Visa' ? (
                                                                                        <img
                                                                                            src="/images/checkout/card-visa.svg"
                                                                                            alt="card"
                                                                                        />
                                                                                    ) : card_type === 'Mada' ? (
                                                                                        <img
                                                                                            src="/images/checkout/card-mada.svg"
                                                                                            alt="card"
                                                                                        />
                                                                                    ) : card_type === 'Mastercard' ? (
                                                                                        <img
                                                                                            src="/images/checkout/card-mastercard.svg"
                                                                                            alt="card"
                                                                                        />
                                                                                    ) : (
                                                                                        <img
                                                                                            src="/images/checkout/card.svg"
                                                                                            alt="card"
                                                                                        />
                                                                                    )}
                                                                                </InputAdornment>
                                                                            ),
                                                                        }}
                                                                        onChange={onChangeHandlePayment}
                                                                        onInput={(e) =>
                                                                            (e.target.value = e.target.value.replace(
                                                                                /\D/g,
                                                                                '',
                                                                            ))
                                                                        }
                                                                    />
                                                                    {card_number && !card_number_valid && (
                                                                        <FormHelperText className="input-error">
                                                                            <FormattedMessage
                                                                                id="paybycard.CardNumberError"
                                                                                defaultMessage="Card number is invalid"
                                                                            />
                                                                        </FormHelperText>
                                                                    )}
                                                                </Grid>
                                                                <Grid item xs={12} sm={6}>
                                                                    <Typography className="form-label">
                                                                        <FormattedMessage
                                                                            id="paybycard.ExpiryDate"
                                                                            defaultMessage="Expiry Date"
                                                                        />
                                                                    </Typography>
                                                                    <TextField
                                                                        id="Expiry-Date"
                                                                        error={!expiry_date_valid}
                                                                        variant="outlined"
                                                                        fullWidth
                                                                        placeholder="MM/YY"
                                                                        inputProps={{
                                                                            maxLength: 5,
                                                                            name: 'expiry_date',
                                                                        }}
                                                                        onChange={onChangeHandlePayment}
                                                                        onInput={(e) =>
                                                                            (e.target.value = e.target.value.replace(
                                                                                /[^\d/]/g,
                                                                                '',
                                                                            ))
                                                                        }
                                                                    />
                                                                    {expiry_date && !expiry_date_valid && (
                                                                        <FormHelperText className="input-error">
                                                                            <FormattedMessage
                                                                                id="paybycard.ExpiryDateError"
                                                                                defaultMessage="Invalid expiry date"
                                                                            />
                                                                        </FormHelperText>
                                                                    )}
                                                                </Grid>
                                                                <Grid item xs={12} sm={6}>
                                                                    <Typography className="form-label">
                                                                        <FormattedMessage
                                                                            id="paybycard.CVV"
                                                                            defaultMessage="CVV"
                                                                        />
                                                                    </Typography>
                                                                    <TextField
                                                                        id="CVV"
                                                                        variant="outlined"
                                                                        fullWidth
                                                                        error={!card_security_code_valid}
                                                                        inputProps={{
                                                                            maxLength: 3,
                                                                            name: 'card_security_code',
                                                                        }}
                                                                        onInput={(e) =>
                                                                            (e.target.value = e.target.value.replace(
                                                                                /\D/g,
                                                                                '',
                                                                            ))
                                                                        }
                                                                        onChange={onChangeHandlePayment}
                                                                    />
                                                                    {card_security_code && !card_security_code_valid && (
                                                                        <FormHelperText className="input-error">
                                                                            <FormattedMessage
                                                                                id="paybycard.CVVError"
                                                                                defaultMessage="Invalid CVV"
                                                                            />{' '}
                                                                        </FormHelperText>
                                                                    )}
                                                                </Grid>
                                                                {loginUser && (
                                                                    <Grid
                                                                        item
                                                                        xs={12}
                                                                        className="arabic-right"
                                                                        onClick={() => {
                                                                            if (credit_cards && credit_cards.length < 3)
                                                                                setsave_card(!save_card);
                                                                        }}
                                                                    >
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox
                                                                                    checked={save_card}
                                                                                    name="save-card-details"
                                                                                />
                                                                            }
                                                                            label={
                                                                                <>
                                                                                    <FormattedMessage
                                                                                        id="paybycard.SaveCardDetails"
                                                                                        defaultMessage="Save card details"
                                                                                    />
                                                                                    &nbsp;
                                                                                    {credit_cards &&
                                                                                        credit_cards.length >= 3 && (
                                                                                            <>
                                                                                                (
                                                                                                <FormattedMessage
                                                                                                    id="Youcansaveonly3cardsRemoveanothercardtosavethis"
                                                                                                    defaultMessage="You can save only 3 cards, Remove another card to save this."
                                                                                                />
                                                                                                &nbsp;
                                                                                                <Link
                                                                                                    to={`/${store_locale}/my-cards`}
                                                                                                >
                                                                                                    <FormattedMessage
                                                                                                        id="paybycard.manageCards"
                                                                                                        defaultMessage="Manage Cards"
                                                                                                    />
                                                                                                </Link>
                                                                                                )
                                                                                            </>
                                                                                        )}
                                                                                </>
                                                                            }
                                                                        />
                                                                    </Grid>
                                                                )}
                                                            </Grid>
                                                        </>
                                                    )}
                                                </Grid>
                                            )}
                                        {/* <PaymentForm /> */}
                                        {payments[option.code] && option.code === selectedCode && (
                                            <Grid container>
                                                <Grid item xs={12}>
                                                    {termsAndCondition()}
                                                </Grid>
                                                {selectedCode !== 'cashondelivery' && (
                                                    <Button
                                                        className="pay-now-button"
                                                        disabled={showMaxCODValue()}
                                                        onClick={() => onClickpayNow()}
                                                    >
                                                        <FormattedMessage
                                                            id="Checkout.Payment.PayNow"
                                                            defaultMessage="Pay Now"
                                                        />
                                                    </Button>
                                                )}
                                                {selectedCode === 'cashondelivery' && (
                                                    <Button
                                                        className="pay-now-button"
                                                        disabled={showMaxCODValue()}
                                                        onClick={() => onClickpayNow()}
                                                    >
                                                        <FormattedMessage
                                                            id="Checkout.Payment.PlaceOrder"
                                                            defaultMessage="Place Order"
                                                        />
                                                    </Button>
                                                )}
                                            </Grid>
                                        )}
                                        {payments[option.code] && (
                                            <Grid item xs={12}>
                                                <div className="border-line" />
                                            </Grid>
                                        )}
                                    </Grid>
                                );
                            })}
                        {isDeviceSupportForApple && (
                            <Grid style={{ marginTop: 20, textAlign: 'initial' }} item xs={12}>
                                <button
                                    onClick={() => onApplePayButtonClick()}
                                    className="apple-pay-button-with-text apple-pay-button-black-with-text"
                                >
                                    <span className="text">Buy with</span>
                                    <span className="logo"></span>
                                </button>
                            </Grid>
                        )}
                    </Grid>
                </Grid>
            </Hidden>
        </Grid>
    );
}
