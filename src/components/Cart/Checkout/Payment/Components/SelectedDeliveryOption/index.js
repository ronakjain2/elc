import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CheckCircle from '@material-ui/icons/CheckCircle';
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { useSelector } from 'react-redux';

export default function SelectedDeliveryOption() {
    const state = useSelector((state) => state);

    const shippingMethodData = state && state.checkoutPayment && state.checkoutPayment.shippingMethodData;
    const cartData = state && state.checkoutPayment && state.checkoutPayment.cartData;

    return (
        <Grid container className="checkout-payment">
            <Grid item xs={12}>
                <Typography className="delivery-charges">
                    <FormattedMessage id="Checkout.Payment.DeliveryCharges" defaultMessage="Delivery Charges" />
                </Typography>
            </Grid>

            <Grid item xs={12}>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox checked={true} icon={<RadioButtonUnchecked />} checkedIcon={<CheckCircle />} />
                        }
                        label={
                            <>
                                {shippingMethodData && shippingMethodData.shipping_method_name && (
                                    <Typography className="checkbox-style">
                                        <span className="bold-text">{shippingMethodData.shipping_method_name}</span> -{' '}
                                        {`${cartData && cartData.currency} ${
                                            shippingMethodData && shippingMethodData.shipping_charges
                                        }`}
                                    </Typography>
                                )}
                                <Typography className="checkbox-style-subtext"></Typography>
                            </>
                        }
                    />
                </FormGroup>
            </Grid>
        </Grid>
    );
}
