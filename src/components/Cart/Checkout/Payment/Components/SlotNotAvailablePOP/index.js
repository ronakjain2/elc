import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';
import { useSelector } from 'react-redux';

export default function SlotNotAvailablePOP() {
    const state = useSelector((state) => state);

    const slot_avl_flag = state && state.checkoutPayment && state.checkoutPayment.slot_avl_flag;
    const slot_avl_message = state && state.checkoutPayment && state.checkoutPayment.slot_avl_message;

    const [open, setOpen] = useState(false);

    const handleCloseOutSide = () => {
        setOpen(true);
    };

    useEffect(() => {
        if (slot_avl_flag) {
            setOpen(false);
        } else if (!slot_avl_flag) {
            setOpen(true);
        }
    }, [slot_avl_flag]);

    return (
        <Grid container>
            <Dialog
                open={open}
                onClose={handleCloseOutSide}
                className="country-pop-container"
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogContent>
                    <Typography>{slot_avl_message}</Typography>
                </DialogContent>
            </Dialog>
        </Grid>
    );
}
