import React, { useEffect } from 'react';
import Spinner from 'commonComponet/Spinner';
import { useDispatch, useSelector } from 'react-redux';
import { actions as checkoutPaymentActions } from '../../redux/actions';

function PaymentProcessing({ history, location }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const store_locale = state && state.auth && state.auth.store_locale;

    const PaymentProcessing = () => {
        const Cryptr = require('cryptr');
        let query = new URLSearchParams(location.search);
        history.push(`/${store_locale}/payment-processing`);
        var cryptrOrderId = new Cryptr('mihyarOrderId');
        if (query.get('step') === 'PROCESSING') {
        } else if (query.get('step') === 'FAILED') {
            dispatch(
                checkoutPaymentActions.payfortRestoreQuoteRequest({
                    merchant_reference: cryptrOrderId.decrypt(query.get('order_id')),
                    payfort_message: query.get('message'),
                }),
            );
        } else {
            const merchant_reference = localStorage.getItem('merchant_reference');
            if (merchant_reference) {
                dispatch(
                    checkoutPaymentActions.payfortRestoreQuoteRequest({
                        merchant_reference,
                        payfort_message: store_locale.includes('-en')
                            ? 'The process is interrupted, please confirm your order before proceeding with the payment'
                            : 'تمت مقاطعة العملية، يرجى التأكد من طلبك قبل متابعة عملية الدفع',
                    }),
                );
            } else {
                history.push(`/${store_locale}/cart`);
            }
        }
    };

    useEffect(PaymentProcessing, []);
    return (
        <div>
            <Spinner />
        </div>
    );
}

export default PaymentProcessing;
