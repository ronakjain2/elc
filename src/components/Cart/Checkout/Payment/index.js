import React, { Suspense, lazy, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Spinner from 'commonComponet/Spinner';
import './payment.css';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { actions as checkoutPaymentActions } from './redux/actions';

const CheckoutStepper = lazy(() => import('../Stepper'));
const CheckoutCartDetails = lazy(() => import('../CartDetails'));
const CheckoutVoucher = lazy(() => import('./Components/Voucher'));
//const SelectedDeliveryOption = lazy(() => import('./Components/SelectedDeliveryOption'));
const CheckoutPaymentOption = lazy(() => import('./Components/PaymentOption'));
const SlotNotAvailablePOP = lazy(() => import(`./Components/SlotNotAvailablePOP`));

export default function CheckoutPayment() {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);
    const store_id = state && state.auth && state.auth.currentStore;
    const quote_id = state && state.auth && state.auth.quote_id;

    const loader = state && state.checkoutPayment && state.checkoutPayment.loader;
    const setAndPlaceLoader = state && state.checkoutPayment && state.checkoutPayment.setAndPlaceLoader;

    //const shippingMethodData = state && state.checkoutPayment && state.checkoutPayment.shippingMethodData;

    const [isCod, setIsCod] = useState(false);

    useEffect(() => {
        try {
            dispatch(
                checkoutPaymentActions.getCheckoutPaymentsRequest({
                    quote_id,
                    store_id,
                }),
            );
        } catch (err) {}
    }, [dispatch, store_id, quote_id]);

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="checkout-payment">
                {/** Checkout Stepper */}
                <Grid item xs={12} md={10} lg={8}>
                    <CheckoutStepper activeStep={2} />
                </Grid>
                {/** End Checkout Stepper */}

                <Grid item xs={11}>
                    <Grid container className="checkout-outer-padding" justify="space-between">
                        {/** Checkout Payment Page */}
                        {!loader && !setAndPlaceLoader && (
                            <Grid item xs={12} md={8} className="checkout-details-card">
                                <Grid item xs={12}>
                                    <Typography className="title">
                                        <FormattedMessage id="Checkout.Payment" defaultMessage="Payment" />
                                    </Typography>

                                    {/** Voucher code */}
                                    <CheckoutVoucher setIsCod={setIsCod} />
                                    {/** End voucher code */}

                                    {/** line */}
                                    <Grid item xs={12}>
                                        <div className="border-line" />
                                    </Grid>
                                    {/** end line */}

                                    {/** Delivery Option */}
                                    {/*shippingMethodData && shippingMethodData.shipping_method_type !== 'click_and_collect' && <Grid item xs={12}>
                                    <SelectedDeliveryOption />
                                </Grid>*/}
                                    {/** End Delivery option */}

                                    {/** line */}
                                    {/*shippingMethodData && shippingMethodData.shipping_method_type !== 'click_and_collect' && <Grid item xs={12}>
                                    <div className="border-line" />
                                </Grid>*/}
                                    {/** end line */}

                                    {/** Payment Option */}
                                    <Grid item xs={12}>
                                        <CheckoutPaymentOption setIsCod={setIsCod} />
                                    </Grid>
                                    {/** Payment Option */}
                                </Grid>
                            </Grid>
                        )}
                        {(loader || setAndPlaceLoader) && (
                            <Grid item xs={12} md={8} className="checkout-details-card">
                                <Spinner />
                            </Grid>
                        )}
                        {/**End checkout Payment Page */}

                        {/** Checkout Carts Page */}

                        <Grid item xs={12} md={3}>
                            <CheckoutCartDetails isPayment={true} isCod={isCod} />
                        </Grid>

                        {/** End Checkout Carts Page */}

                        {/**Slot Not Available pop */}
                        <SlotNotAvailablePOP />
                        {/**End Slot Not Available pop  */}
                    </Grid>
                </Grid>
            </Grid>
        </Suspense>
    );
}
