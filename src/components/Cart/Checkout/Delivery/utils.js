import { isEmail } from 'components/SignIn_SignUp/utils';

//New Address check address field
export const checkHomeDeliveryValidation = (data, city, isCorrectPhone) => {
    if (
        !data ||
        !data.firstName ||
        data.firstName === '' ||
        !data.lastName ||
        data.lastName === '' ||
        !isEmail(data.email) ||
        !data.phoneNumber ||
        data.phoneNumber === '' ||
        !data.deliveryOption ||
        data.deliveryOption === '' ||
        !data.apartmentNo ||
        data.apartmentNo === '' ||
        !data.address ||
        data.address === '' ||
        !data.areaAndLocality ||
        data.areaAndLocality === '' ||
        city.id === '' ||
        !data.countryCode ||
        data.countryCode === '' ||
        !isCorrectPhone
    ) {
        return true;
    }

    if (data.gift_wrap_flag === 1) {
        if (data.message === '') {
            return true;
        }
    }

    return false;
};

//Create new address object for set
export const createHomeDeliveryApiObject = (data, country_id, state, city, loginUser) => {
    return {
        address_id: '',
        shipping_code: data.deliveryOption,
        nayomi_store_id: '',
        address_object: {
            addressId: '',
            UserID: loginUser && loginUser.customer_id,
            userFirstName: data.firstName,
            userLastName: data.lastName,
            customer_email: data.email,
            country_id,
            state,
            region_id: city.id,
            city: city.name,
            street: `${data.apartmentNo}~ ${data.address}~ ${data.areaAndLocality}`,
            carrier_code: data.countryCode,
            telephone: data.phoneNumber,
            customer_address_type: data.type,
            postcode: '',
            message: data.gift_wrap_flag === 1 ? data.message : '',
            gift_wrap_flag: data.gift_wrap_flag,
            payload_shipping_type: '',
        },
        fname: '',
        lname: '',
        cnumber: '',
        mnumber: '',
        email: '',
        message: data.gift_wrap_flag === 1 ? data.message : '',
        gift_wrap_flag: data.gift_wrap_flag,
    };
};

// Check Pickup and store field
export const checkPickUpStoreObjet = (storelocator, data, address, isCorrectPhone) => {
    if (
        !data ||
        !data.firstName ||
        data.firstName === '' ||
        !data.lastName ||
        data.lastName === '' ||
        !isEmail(data.email) ||
        !data.phoneNumber ||
        data.phoneNumber === '' ||
        !storelocator ||
        !storelocator.id ||
        storelocator.id === '' ||
        !isCorrectPhone
    ) {
        return true;
    }

    if (address.gift_wrap_flag === 1) {
        if (address.message === '') {
            return true;
        }
    }

    return false;
};

// Create pick up store object
export const createPickUpStoreApiObject = (storelocator, loginUser, userForm, address, selectedTab) => {
    return {
        address_id: '',
        shipping_code: selectedTab,
        nayomi_store_id: storelocator.id,
        address_object: {},
        fname: userForm.firstName,
        lname: userForm.lastName,
        cnumber: userForm.countryCode,
        mnumber: userForm.phoneNumber,
        email: userForm.email,
        message: address.gift_wrap_flag === 1 ? address.message : '',
        gift_wrap_flag: address.gift_wrap_flag,
    };
};

// check old address field
export const checkOldHomeDeliveryValidation = (address, addressId) => {
    if (!address || !address.deliveryOption || address.deliveryOption === '' || addressId === '') {
        return true;
    }

    if (address.gift_wrap_flag === 1) {
        if (address.message === '') {
            return true;
        }
    }

    return false;
};

//create old address field
export const createOldAddressObject = (address, addressId) => {
    return {
        address_id: addressId,
        shipping_code: address.deliveryOption,
        nayomi_store_id: '',
        address_object: {},
        fname: '',
        lname: '',
        cnumber: '',
        mnumber: '',
        email: '',
        message: address.gift_wrap_flag === 1 ? address.message : '',
        gift_wrap_flag: address.gift_wrap_flag,
    };
};

export const getCityObject = (name, citys) => {
    return citys.filter((city) => city.name === name)[0];
};

export const fillAddressData = (obj, shipping_code) => {
    if (obj && obj.city) {
        const addresses = obj.street && obj.street.split('~');
        return {
            firstName: obj.userFirstName,
            lastName: obj.userLastName,
            email: obj.customer_email || '',
            phoneNumber: obj.telephone,
            countryCode: obj.carrier_code,
            type: obj.customer_address_type,
            saveFuture: true,
            defaultAddress: false,
            deliveryOption: '',
            apartmentNo: (addresses && addresses.length >= 0 && addresses[0]) || '',
            address: (addresses && addresses.length >= 1 && addresses[1]) || '',
            areaAndLocality: (addresses && addresses.length >= 2 && addresses[2]) || '',
            message: obj.gift_wrap_flag === 1 ? obj.message : '',
            gift_wrap_flag: obj.gift_wrap_flag,
        };
    }

    return {};
};

export const fillPickUpAndStore = (obj) => {
    if (obj) {
        return {
            firstName: obj.fname,
            lastName: obj.lname,
            email: obj.email || '',
            phoneNumber: obj.mnumber,
            countryCode: obj.cnumber,
        };
    }
};

export const replaceTilCharacterToComma = (str) => {
    try {
        return str && str.replace(/~/g, ',');
    } catch (err) {
        return str;
    }
};

export const checkDeliveryPaymentOption = (option) => {
    const AvailableOption = {
        cod: 'COD',
        prepaid: 'PREPAID',
    };
    let availableArr = [];
    for (let i = 0; i < option.length; i++) {
        if (option[i] === AvailableOption.cod) {
            availableArr.push(AvailableOption.cod);
        }

        if (option[i] === AvailableOption.prepaid) {
            availableArr.push(AvailableOption.prepaid);
        }
    }

    return availableArr;
};

export const scrollToElement = (element) => {
    const elmnt = document.querySelector(element);
    elmnt && elmnt.scrollIntoView({ block: 'center', behavior: 'smooth' });
    return;
};
