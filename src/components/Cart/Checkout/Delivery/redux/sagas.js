import { call, put, takeLatest, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { actions, types } from './actions';
import api from '../../api';
import { toast } from 'react-toastify';
import { actions as basketActions } from 'components/Cart/Basket/redux/actions';

const auth = (state) => state && state.auth;

const getCheckoutDeliveryReq = function* getCheckoutDeliveryReq({ payload }) {
    try {
        const { data } = yield call(api.getCheckoutDelivery, payload);
        if (data && data.status) {
            yield put(actions.getCheckoutDeliveryRequestSuccess(data));
        } else {
            toast.error(data && data.message);
            yield put(actions.getCheckoutDeliveryRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getCheckoutDeliveryRequestFailed());
    }
};

const getCheckoutDeliveryOptionReq = function* getCheckoutDeliveryOptionReq({ payload }) {
    try {
        const { data } = yield call(api.getCheckoutDeliveryOption, payload);
        if (data && data.status) {
            yield put(
                actions.getCheckoutDeliveryOptionRequestSuccess({
                    delivery_options: data && data.data,
                    delivery_text: data && data.data && data.data.delivery_text,
                }),
            );
        } else {
            toast.error(data && data.message);
            yield put(actions.getCheckoutDeliveryOptionRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getCheckoutDeliveryOptionRequestFailed());
    }
};

const setCheckoutDeliveryReq = function* setCheckoutDeliveryReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.setCheckoutDelivery, payload);
        if (data && data.status) {
            yield put(actions.setCheckoutDeliveryRequestSuccess());
            if (data.isVoucherRemoved) {
                yield put(
                    basketActions.getCartItemRequest({
                        quote_id: payload.quote_id,
                        store_id: payload.store_id,
                        isPayment: true,
                        clearDeliveryObj: true,
                    }),
                );
            }
            yield put(push(`/${authData && authData.store_locale}/checkout-payment`));
        } else {
            toast.error(data && data.message);
            yield put(actions.setCheckoutDeliveryRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.setCheckoutDeliveryRequestFailed());
    }
};

const getCountryListReq = function* getCountryListReq() {
    try {
        const { data } = yield call(api.getCountryList);
        if (data) {
            yield put(actions.getCountryListRequestSuccess(data || []));
        } else {
            yield put(actions.getCountryListRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getCountryListRequestFailed());
    }
};

const checkDeliveryOptions = function* checkDeliveryOptions({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.checkDeliveryOptions, payload);
        if (data && data.status) {
            yield put(actions.checkDeliveryOptionRequestSuccess(data.delivery_options));
        } else {
            toast.error(data && data.message);
            yield put(actions.checkDeliveryOptionRequestFailed());
            yield put(push(`/${authData && authData.store_locale}/cart`));
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.checkDeliveryOptionRequestFailed());
    }
};

const getStoreLocatorReq = function* getStoreLocatorReq({ payload }) {
    try {
        const { data } = yield call(api.getStoreCountryList, payload);
        if (data && data.status) {
            yield put(actions.getCheckoutStoreLocatorRequestSuccess(data.data));
        } else {
            toast.error(data.message);
            yield put(actions.getCheckoutStoreLocatorRequestFailed(data.data));
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getCheckoutStoreLocatorRequestFailed([]));
    }
};

const getClickAndCollectOptionReq = function* getClickAndCollectOptionReq({ payload }) {
    try {
        const { data } = yield call(api.checkClickAndCollectOptions, payload);
        if (data && data.status) {
            yield put(actions.getClickAndCollectOptionRequestSuccess(data && data.data && data.data.delivery_options));
        } else {
            toast.error(data.message);
            yield put(actions.getClickAndCollectOptionRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getClickAndCollectOptionRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_CHECKOUT_DELIVERY_REQUESET, getCheckoutDeliveryReq);
    yield takeLatest(types.GET_CHECKOUT_DELIVERY_OPTIONS_REQUEST, getCheckoutDeliveryOptionReq);
    yield takeLatest(types.SET_CHECKOUT_DELIVERY_REQUEST, setCheckoutDeliveryReq);
    yield takeLatest(types.GET_COUNTRY_LIST_REQUEST, getCountryListReq);
    yield takeLatest(types.CHECK_DELIVERY_OPTIONS_REQUEST, checkDeliveryOptions);
    yield takeLatest(types.GET_CHECKOUT_STORE_LOCATOR_REQUEST, getStoreLocatorReq);
    yield takeLatest(types.GET_CLICK_AND_COLLECT_OPTION_REQUEST, getClickAndCollectOptionReq);
}
