import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_CHECKOUT_DELIVERY_REQUESET]: (state) => ({
        ...state,
        loader: true,
        isShowDeliveryOption: false,
    }),
    [types.GET_CHECKOUT_DELIVERY_REQUESET_FAILED]: (state) => ({
        ...state,
        loader: false,
        deliveryDetails: null,
    }),
    [types.GET_CHECKOUT_DELIVERY_REQUESET_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        deliveryDetails: payload,
    }),

    [types.GET_CHECKOUT_DELIVERY_OPTIONS_REQUEST]: (state) => ({
        ...state,
        optionLoader: true,
    }),
    [types.GET_CHECKOUT_DELIVERY_OPTIONS_REQUEST_FAILED]: (state) => ({
        ...state,
        optionLoader: false,
        homeDeliveryOptions: [],
    }),
    [types.GET_CHECKOUT_DELIVERY_OPTIONS_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        optionLoader: false,
        homeDeliveryOptions: payload && payload.delivery_options,
        delivery_text: payload && payload.delivery_text,
    }),

    [types.SET_CHECKOUT_DELIVERY_REQUEST]: (state) => ({
        ...state,
        setDeliveryLoader: true,
    }),
    [types.SET_CHECKOUT_DELIVERY_REQUEST_FAILED]: (state) => ({
        ...state,
        setDeliveryLoader: false,
    }),
    [types.SET_CHECKOUT_DELIVERY_REQUEST_SUCCESS]: (state) => ({
        ...state,
        setDeliveryLoader: false,
    }),

    [types.GET_COUNTRY_LIST_REQUEST]: (state) => ({
        ...state,
        countryListLoader: true,
    }),
    [types.GET_COUNTRY_LIST_REQUEST_FAILED]: (state) => ({
        ...state,
        countryListLoader: false,
    }),
    [types.GET_COUNTRY_LIST_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        countryListLoader: false,
        countryList: payload,
    }),

    [types.CHECK_DELIVERY_OPTIONS_REQUEST]: (state) => ({
        ...state,
        checkDeliveryOptionLoader: true,
        isShowDeliveryOption: false,
    }),
    [types.CHECK_DELIVERY_OPTIONS_REQUEST_FAILED]: (state) => ({
        ...state,
        checkDeliveryOptionLoader: false,
    }),
    [types.CHECK_DELIVERY_OPTIONS_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        checkDeliveryOptionLoader: false,
        deliveryOptions: payload,
        isShowDeliveryOption: true,
    }),

    [types.GET_CHECKOUT_STORE_LOCATOR_REQUEST]: (state) => ({
        ...state,
        storeLocatorLoader: true,
        availableCitys: [],
    }),
    [types.GET_CHECKOUT_STORE_LOCATOR_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        storeLocatorLoader: false,
        storeLocatorList: [],
        availableCitys: payload || [],
    }),
    [types.GET_CHECKOUT_STORE_LOCATOR_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        storeLocatorLoader: false,
        storeLocatorList: payload,
        availableCitys: [],
    }),

    [types.GET_CLICK_AND_COLLECT_OPTION_REQUEST]: (state) => ({
        ...state,
        clickAndCollectOptionLoader: true,
    }),
    [types.GET_CLICK_AND_COLLECT_OPTION_REQUEST_FAILED]: (state) => ({
        ...state,
        clickAndCollectOptionLoader: false,
        clickAndCollectData: {},
    }),
    [types.GET_CLICK_AND_COLLECT_OPTION_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        clickAndCollectOptionLoader: false,
        clickAndCollectData: payload || {},
    }),

    [types.SAVE_OR_CLEAR_DELIVERY_DETAILS]: (state, { payload }) => ({
        ...state,
        prevDeliveryDetails: payload,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    optionLoader: false,
    setDeliveryLoader: false,
    countryListLoader: false,
    isShowDeliveryOption: false,
    storeLocatorLoader: false,
    clickAndCollectOptionLoader: false,

    deliveryDetails: null,
    deliveryOptions: {},
    homeDeliveryOptions: {},
    countryList: [],
    storeLocatorList: [],
    clickAndCollectData: {},
    availableCitys: [],

    prevDeliveryDetails: {},
});
