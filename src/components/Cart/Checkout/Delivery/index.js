import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Spinner from 'commonComponet/Spinner';
import { actions as familyClubActions } from 'components/FamilyClub/redux/actions';
import React, { lazy, Suspense, useCallback, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { isValidateSpecialCharacters } from '../utils';
import PickAndCollect from './Components/PickAndCollect';
import './delivery.css';
import { actions as checkoutDeliveryActions } from './redux/actions';
import {
    checkHomeDeliveryValidation,
    checkOldHomeDeliveryValidation,
    createHomeDeliveryApiObject,
    createOldAddressObject,
    fillAddressData,
    scrollToElement,
} from './utils';
const CheckoutStepper = lazy(() => import('../Stepper'));
const CheckoutCartDetails = lazy(() => import('../CartDetails'));
const CountryAndCity = lazy(() => import('./Components/CountryAndCity'));
const CheckoutHomeDelivery = lazy(() => import('./Components/HomeDelivery'));

export default function CheckoutDeliveryDetails() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();
    let history = useHistory();

    const country = state && state.auth && state.auth.country;
    const loginUser = state && state.auth && state.auth.loginUser;
    const store_id = state && state.auth && state.auth.currentStore;
    const store_locale = state && state.auth && state.auth.store_locale;
    const quote_id = state && state.auth && state.auth.quote_id;

    const loader = state && state.checkoutDelivery && state.checkoutDelivery.loader;
    const setDeliveryLoader = state && state.checkoutDelivery && state.checkoutDelivery.setDeliveryLoader;

    const checkDeliveryOptionLoader =
        state && state.checkoutDelivery && state.checkoutDelivery.checkDeliveryOptionLoader;

    const deliveryDetails = state && state.checkoutDelivery && state.checkoutDelivery.deliveryDetails;
    const isShowDeliveryOption = state && state.checkoutDelivery && state.checkoutDelivery.isShowDeliveryOption;
    const deliveryOptions = state && state.checkoutDelivery && state.checkoutDelivery.deliveryOptions;
    const prevDeliveryDetails = state && state.checkoutDelivery && state.checkoutDelivery.prevDeliveryDetails;

    const guestDetails = state && state.checkoutLogin && state.checkoutLogin.guestDetails;
    const [whatsappOptin, setWhatsappOptin] = useState(true);
    const [countryName, setCountryName] = useState('');
    const [isValidate, setIsValidate] = useState(false);
    const [city, setCity] = useState('');
    const [activeTab, setActiveTab] = useState('Home');
    const [addressId, setAddressId] = useState('');
    const [open, setOpen] = React.useState(false);
    const [address, setAddress] = useState({
        firstName: '',
        lastName: '',
        email: (deliveryDetails && deliveryDetails.email) || '',
        phoneNumber: '',
        countryCode: '',
        type: 'home',
        saveFuture: true,
        defaultAddress: false,
        deliveryOption: '',
        apartmentNo: '',
        address: '',
        areaAndLocality: '',
        message: '',
        gift_wrap_flag: 0,
    });
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const [cityName, setCityName] = useState('');

    let addressData =
        (state &&
            state.checkoutDelivery &&
            state.checkoutDelivery.deliveryDetails &&
            state.checkoutDelivery.deliveryDetails.addressData && [
                ...state.checkoutDelivery.deliveryDetails.addressData,
            ]) ||
        [];
    addressData = addressData.filter(
        (address) =>
            (address && address.city && address.city.toLowerCase()) === (city && city.name && city.name.toLowerCase()),
    );

    //set country name
    useEffect(() => {
        if (country === 'UAE') {
            //setCountryName('United Arab Emirates')
            setCountryName('UAE');
        } else {
            //setCountryName('Saudi Arabia')
            setCountryName('KSA');
        }
    }, [country]);

    // Clearing delivery Options
    const clearDeliveryOption = () => {
        setAddress({
            ...address,
            deliveryOption: '',
        });
    };

    //Calling get delivery API
    useEffect(() => {
        dispatch(
            checkoutDeliveryActions.getCheckoutDeliveryRequest({
                store_id,
                quote_id,
                customer_id: (loginUser && loginUser.customer_id) || '',
            }),
        );
    }, [dispatch, store_id, quote_id, loginUser]);

    //Get country list API
    useEffect(() => {
        dispatch(checkoutDeliveryActions.getCountryListRequest());
    }, [dispatch]);

    //Set email id for login user
    useEffect(() => {
        try {
            if (deliveryDetails && deliveryDetails.email) {
                let email = 'email';
                setAddress((a) => ({ ...a, [email]: deliveryDetails && deliveryDetails.email }));
            }
        } catch (err) {}
    }, [deliveryDetails]);

    //Set First name and last name
    useEffect(() => {
        try {
            if (loginUser && Object.keys(loginUser).length !== 0) {
                setAddress((a) => ({
                    ...a,
                    firstName: loginUser.firstname,
                    lastName: loginUser.lastname,
                    phoneNumber: loginUser.phone_number,
                    countryCode: loginUser.carrier_code,
                }));
            }
        } catch (err) {}
    }, [loginUser]);

    //Set guest user data like (first name, last name, email)
    useEffect(() => {
        try {
            if (!loginUser || Object.keys(loginUser).length === 0) {
                if (guestDetails && Object.keys(guestDetails).length > 0) {
                    setAddress((a) => ({
                        ...a,
                        firstName: guestDetails.firstName,
                        lastName: guestDetails.lastName,
                        email: guestDetails.email,
                    }));
                }
            }
        } catch (err) {}
    }, [guestDetails, loginUser]);

    //Get Delivery Option for Home Tab
    const getDeliveryOption = useCallback(() => {
        if (city && city.name !== '') {
            dispatch(
                checkoutDeliveryActions.getCheckoutDeliveryOptionRequest({
                    store_id,
                    quote_id,
                    country: country === 'UAE' ? 'AE' : 'SA',
                    city: city && city.name,
                    fullfilment_type: 'DELIVERY',
                }),
            );
        }
    }, [city, dispatch, store_id, quote_id, country]);

    //Set which option we need to show
    useEffect(() => {
        try {
            if (
                deliveryOptions &&
                city.name !== '' &&
                (!prevDeliveryDetails || Object.keys(prevDeliveryDetails).length === 0)
            ) {
                if (deliveryOptions['Home Delivery'] === 'Enable') {
                    setActiveTab('Home');
                    getDeliveryOption(city);
                } else if (deliveryOptions['Ship For Pickup'] === 'Enable') {
                    setActiveTab('PickUpStore');
                } else {
                    setActiveTab('');
                }
            }
        } catch (err) {}
    }, [deliveryOptions, getDeliveryOption, city, prevDeliveryDetails]);

    //Check delivery option like (Home or C&C)
    const checkDeliveryOption = (city) => {
        clearDeliveryOption();
        dispatch(
            checkoutDeliveryActions.checkDeliveryOptionRequest({
                store_id,
                quote_id,
                country: country === 'UAE' ? 'AE' : 'SA',
                city: city && city.name,
            }),
        );
    };

    //click on home delivery continue
    const onClickContinue = (isNew) => {
        setIsValidate(true);
        if (isNew) {
            if (checkHomeDeliveryValidation(address, city, isCorrectPhone)) {
                scrollToElement('.input-error');
                return;
            }
            setIsValidate(false);
            dispatch(
                checkoutDeliveryActions.setCheckoutDeliveryRequest({
                    store_id,
                    quote_id,
                    whatsapp_optin: whatsappOptin ? 1 : 0,
                    ...createHomeDeliveryApiObject(
                        address,
                        country === 'UAE' ? 'AE' : 'SA',
                        countryName,
                        city,
                        loginUser,
                    ),
                }),
            );
            //Save selected/enter address
            dispatch(
                checkoutDeliveryActions.saveOrClearDeliveryDetails({
                    city,
                    ...createHomeDeliveryApiObject(
                        address,
                        country === 'UAE' ? 'AE' : 'SA',
                        countryName,
                        city,
                        loginUser,
                    ),
                }),
            );
        } else {
            if (checkOldHomeDeliveryValidation(address, addressId)) {
                scrollToElement('.input-error');
                return;
            }
            setIsValidate(false);
            let whatsappOptin = addressData && addressData.filter((add) => add.Id === addressId)[0];
            dispatch(
                checkoutDeliveryActions.setCheckoutDeliveryRequest({
                    store_id,
                    quote_id,
                    whatsapp_optin: whatsappOptin ? (whatsappOptin.whatsapp_optin === 1 ? 1 : 0) : 0,
                    ...createOldAddressObject(address, addressId),
                }),
            );
            //Save selected/enter address
            dispatch(
                checkoutDeliveryActions.saveOrClearDeliveryDetails({
                    city,
                    ...createOldAddressObject(address, addressId),
                }),
            );
        }
    };

    //set old details if available
    useEffect(() => {
        if (
            prevDeliveryDetails &&
            Object.keys(prevDeliveryDetails) &&
            Object.keys(prevDeliveryDetails).length > 0 &&
            prevDeliveryDetails.shipping_code !== ''
        ) {
            if (prevDeliveryDetails && prevDeliveryDetails.city && prevDeliveryDetails.city.name && city === '') {
                setCity(prevDeliveryDetails.city);
                setCityName(prevDeliveryDetails.city.name);
                //After click on back we call check delivery option api for Home and C&C
                dispatch(
                    checkoutDeliveryActions.checkDeliveryOptionRequest({
                        store_id,
                        quote_id,
                        country: country === 'UAE' ? 'AE' : 'SA',
                        city: prevDeliveryDetails.city.name,
                    }),
                );
            }

            //your Enter address seleted
            if (prevDeliveryDetails && prevDeliveryDetails.address_id === '' && prevDeliveryDetails.fname === '') {
                setAddress((a) => ({
                    ...a,
                    ...fillAddressData(prevDeliveryDetails.address_object, prevDeliveryDetails.shipping_code),
                }));
                setActiveTab('Home');

                if (prevDeliveryDetails && prevDeliveryDetails.city && prevDeliveryDetails.city.name && city === '') {
                    //Get Home Tab Delivery Option
                    dispatch(
                        checkoutDeliveryActions.getCheckoutDeliveryOptionRequest({
                            store_id,
                            quote_id,
                            country: country === 'UAE' ? 'AE' : 'SA',
                            city: prevDeliveryDetails.city.name,
                            fullfilment_type: 'DELIVERY',
                        }),
                    );
                }
            } else if (prevDeliveryDetails.fname && prevDeliveryDetails.fname !== '' && !isShowDeliveryOption) {
                setActiveTab('PickUpStore');
            } else if (prevDeliveryDetails && prevDeliveryDetails.address_id && prevDeliveryDetails.address_id !== '') {
                setAddressId(prevDeliveryDetails.address_id);
                setAddress((a) => ({
                    ...a,
                    //deliveryOption: prevDeliveryDetails.shipping_code,
                    message: prevDeliveryDetails.message,
                    gift_wrap_flag: prevDeliveryDetails.gift_wrap_flag,
                }));
                setActiveTab('Home');
                if (prevDeliveryDetails && prevDeliveryDetails.city && prevDeliveryDetails.city.name && city === '') {
                    //Get Home Tab Delivery Option
                    dispatch(
                        checkoutDeliveryActions.getCheckoutDeliveryOptionRequest({
                            store_id,
                            quote_id,
                            country: country === 'UAE' ? 'AE' : 'SA',
                            city: prevDeliveryDetails.city.name,
                            fullfilment_type: 'DELIVERY',
                        }),
                    );
                }
            }
        }
    }, [prevDeliveryDetails, city, store_id, quote_id, country, dispatch, isShowDeliveryOption]);

    const onSelecteDeliveryTab = (tab) => {
        if (tab === 'Home') {
            if (deliveryOptions && deliveryOptions['Home Delivery'] === 'Enable') {
                setActiveTab('Home');
                dispatch(
                    checkoutDeliveryActions.getCheckoutDeliveryOptionRequest({
                        store_id,
                        quote_id,
                        country: country === 'UAE' ? 'AE' : 'SA',
                        city: city.name,
                        fullfilment_type: 'DELIVERY',
                    }),
                );
            }
            return;
        }

        if (tab === 'PickUpStore') {
            if (deliveryOptions['Ship For Pickup'] === 'Enable') {
                setActiveTab('PickUpStore');
            } else {
                setOpen(true);
                return;
            }
        }
    };

    const onChangeHandler = (name) => (event) => {
        if (
            name === 'message' ||
            name === 'firstName' ||
            name === 'lastName' ||
            name === 'apartmentNo' ||
            name === 'address' ||
            name === 'areaAndLocality'
        ) {
            if (isValidateSpecialCharacters(event.target.value)) {
                return;
            }
        }
        setAddress({ ...address, [name]: event.target.value });
    };

    const onChangeGiftWrapHandler = (name) => (event) => {
        setAddress({ ...address, [name]: event.target.checked ? 1 : 0 });
    };

    const removeOldData = () => {
        dispatch(checkoutDeliveryActions.saveOrClearDeliveryDetails(null));
    };

    const onJoinFamilyClub = () => {
        if (loginUser && loginUser.customer_id) {
            let payload = {
                quote_id,
                store_id,
                customer_id: loginUser.customer_id,
                from_basket: false,
            };
            dispatch(familyClubActions.setFamilyClubRequest(payload));
        } else {
            history.push(`/${store_locale}/sign-in-register`);
        }
    };

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="checkout-delivery">
                {/** Checkout Stepper */}
                <Grid item xs={12} md={10} lg={8}>
                    <CheckoutStepper activeStep={1} />
                </Grid>
                {/** End Checkout Stepper */}

                {!loader && !setDeliveryLoader && (
                    <Grid item xs={11}>
                        <Grid container className="checkout-outer-padding" justify="space-between">
                            {/** Checkout Delivery Page */}
                            <Grid item xs={12} md={8} className="checkout-details-card p-0">
                                {deliveryDetails && (deliveryDetails.banner_image || deliveryDetails.mob_banner_image) && (
                                    <Grid
                                        container
                                        className="checkout-details-banner cursor-pointer"
                                        onClick={onJoinFamilyClub}
                                    >
                                        {deliveryDetails.banner_image && (
                                            <img
                                                alt="fc banner"
                                                className="d-none d-md-block"
                                                src={deliveryDetails.banner_image}
                                            />
                                        )}
                                        {deliveryDetails.mob_banner_image && (
                                            <img
                                                alt="fc banner"
                                                className="d-block d-md-none"
                                                src={deliveryDetails.mob_banner_image}
                                            />
                                        )}
                                    </Grid>
                                )}
                                <div className="internal-card">
                                    {/** Country and city */}
                                    <CountryAndCity
                                        setCountryName={setCountryName}
                                        countryName={countryName}
                                        city={city}
                                        country={country}
                                        setCity={setCity}
                                        isValidate={isValidate}
                                        cityName={cityName}
                                        setCityName={setCityName}
                                        removeOldData={removeOldData}
                                        checkDeliveryOption={checkDeliveryOption}
                                    />
                                    {/** End country and city  */}

                                    {isShowDeliveryOption && (
                                        <>
                                            {/** Home delivery and pickup store tab */}
                                            <Grid container className="delivery-tab-container">
                                                <Grid
                                                    item
                                                    xs={6}
                                                    className={
                                                        deliveryOptions && deliveryOptions['Home Delivery'] !== 'Enable'
                                                            ? 'inactive-tab'
                                                            : ''
                                                    }
                                                >
                                                    <Typography
                                                        className={
                                                            activeTab === 'Home' ? 'active-text' : 'inactive-text'
                                                        }
                                                        onClick={() => onSelecteDeliveryTab('Home')}
                                                    >
                                                        <FormattedMessage
                                                            id="Checkout.Delivery.HomeDelivery"
                                                            defaultMessage="Home Delivery"
                                                        />
                                                    </Typography>
                                                    {activeTab === 'Home' && <div className="active-line" />}
                                                </Grid>

                                                <Grid
                                                    item
                                                    xs={6}
                                                    className={
                                                        deliveryOptions &&
                                                        deliveryOptions['Ship For Pickup'] !== 'Enable'
                                                            ? 'inactive-tab'
                                                            : ''
                                                    }
                                                >
                                                    <Typography
                                                        className={
                                                            activeTab === 'PickUpStore'
                                                                ? 'active-text'
                                                                : 'inactive-text'
                                                        }
                                                        onClick={() => onSelecteDeliveryTab('PickUpStore')}
                                                    >
                                                        <FormattedMessage
                                                            id="Checkout.Delivery.PickupfromStore"
                                                            defaultMessage="Pickup from Store"
                                                        />
                                                    </Typography>
                                                    {activeTab === 'PickUpStore' && <div className="active-line" />}
                                                </Grid>
                                            </Grid>
                                            {/** End home delivery and pickup store tab */}

                                            <Grid item xs={12}>
                                                {activeTab === 'Home' && (
                                                    <CheckoutHomeDelivery
                                                        address={address}
                                                        city={city}
                                                        whatsappOptin={whatsappOptin}
                                                        setWhatsappOptin={setWhatsappOptin}
                                                        country={countryName}
                                                        addressId={addressId}
                                                        setAddressId={setAddressId}
                                                        setAddress={setAddress}
                                                        isValidate={isValidate}
                                                        isCorrectPhone={isCorrectPhone}
                                                        setIsCorrectPhone={setIsCorrectPhone}
                                                        onClickContinue={onClickContinue}
                                                    />
                                                )}
                                                {activeTab === 'PickUpStore' && (
                                                    <PickAndCollect
                                                        city={city}
                                                        address={address}
                                                        setAddress={setAddress}
                                                        setMainIsValidate={setIsValidate}
                                                    />
                                                )}
                                            </Grid>
                                        </>
                                    )}

                                    {checkDeliveryOptionLoader && (
                                        <Grid container alignItems="center">
                                            <Spinner />
                                        </Grid>
                                    )}
                                </div>
                            </Grid>
                            {/** End Checkout Delivery Page */}

                            {/** Checkout Carts Page */}
                            <Grid item xs={12} md={3}>
                                <CheckoutCartDetails
                                    address={address}
                                    isDelivery={true}
                                    onChangeHandler={onChangeHandler}
                                    isValidate={isValidate}
                                    onChangeGiftWrapHandler={onChangeGiftWrapHandler}
                                />
                            </Grid>
                            {/** End Checkout Carts Page */}
                        </Grid>
                    </Grid>
                )}
                <Dialog
                    open={open}
                    onClose={() => setOpen(false)}
                    className={'familyClubDialog'}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    {/* <div > */}
                    <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <IconButton onClick={() => setOpen(false)} aria-label="delete">
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Sorry! We regret to inform you that Click&Collect option is not available with this order.
                        </DialogContentText>
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <Button
                                onClick={() => setOpen(false)}
                                style={{ backgroundColor: '#0d943f' }}
                                variant="contained"
                                color="primary"
                            >
                                Ok
                            </Button>
                        </div>
                    </DialogContent>
                </Dialog>
                {(loader || setDeliveryLoader) && (
                    <Grid container>
                        <Spinner />
                    </Grid>
                )}
            </Grid>
        </Suspense>
    );
}
