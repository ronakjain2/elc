import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CheckCircle from '@material-ui/icons/CheckCircle';
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import FCBenfitTag from 'commonComponet/FCBenefitTag';
import Spinner from 'commonComponet/Spinner';
import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

export default function CheckoutDeliveryOptions({ address, setAddress, isValidate }) {
    const state = useSelector(state => state);

    const homeDeliveryOptions = state && state.checkoutDelivery && state.checkoutDelivery.homeDeliveryOptions;
    const prevDeliveryDetails = state && state.checkoutDelivery && state.checkoutDelivery.prevDeliveryDetails;

    const optionLoader = state && state.checkoutDelivery && state.checkoutDelivery.optionLoader;
    const currency = state && state.basket && state.basket.cartDetails && state.basket.cartDetails.currency;

    const getTotalShippment = useMemo(() => {
        if (
            homeDeliveryOptions[0] &&
            homeDeliveryOptions[0].default_shipping &&
            homeDeliveryOptions[0].vendor_shipping_options.length
        ) {
            return homeDeliveryOptions[0].vendor_shipping_options.length + 1;
        } else if (homeDeliveryOptions[0] && homeDeliveryOptions[0].vendor_shipping_options.length) {
            return homeDeliveryOptions[0].vendor_shipping_options.length;
        }
        return 1;
    }, [homeDeliveryOptions]);

    const getAvailableDeliveryOption = useCallback(() => {
        let options = [];

        if (
            homeDeliveryOptions[0] &&
            homeDeliveryOptions[0].default_shipping &&
            homeDeliveryOptions[0].default_shipping.delivery_options
        ) {
            options = [...options, ...Object.values(homeDeliveryOptions[0].default_shipping.delivery_options)];
        }
        if (
            homeDeliveryOptions[0] &&
            homeDeliveryOptions[0].vendor_shipping_options &&
            homeDeliveryOptions[0].vendor_shipping_options[0] &&
            homeDeliveryOptions[0].vendor_shipping_options[0].delivery_options
        ) {
            options = [
                ...options,
                ...Object.values(homeDeliveryOptions[0].vendor_shipping_options[0].delivery_options),
            ];
        }
        return options;
    }, [homeDeliveryOptions]);

    let addressRef = useRef(address);
    useEffect(() => {
        if (!prevDeliveryDetails) return;
        const shipping_code = prevDeliveryDetails.shipping_code;
        const available_options = getAvailableDeliveryOption();
        if (available_options.length === 0) return;
        if (available_options.some(option => option.code === shipping_code)) {
            setAddress({
                ...addressRef.current,
                deliveryOption: shipping_code,
            });
        } else {
            setAddress({
                ...addressRef.current,
                deliveryOption: available_options[0].code,
            });
        }
    }, [getAvailableDeliveryOption, prevDeliveryDetails, setAddress]);

    useEffect(() => {
        if (addressRef.current.deliveryOption) return;
        const available_options = getAvailableDeliveryOption();
        if (available_options.length === 0) return;
        setAddress({
            ...addressRef.current,
            deliveryOption: available_options[0].code,
        });
        return () => {};
    }, [getAvailableDeliveryOption, setAddress]);

    return (
        <Grid container>
            {!optionLoader && (
                <>
                    <Grid item xs={12}>
                        <Grid container>
                            {homeDeliveryOptions[0] && homeDeliveryOptions[0].default_shipping && (
                                <Grid container className="shippment" spacing={3}>
                                    <Grid item xs={12} md={6}>
                                        <Typography className="ChooseDeliveryOptionText">
                                            &nbsp;
                                            <FormattedMessage id="Checkout.Shipment" defaultMessage="Shipment 1 of" />
                                            &nbsp;
                                            {getTotalShippment}
                                        </Typography>
                                        <Grid container>
                                            <Grid item xs={12} className="option-cart">
                                                <ul>
                                                    {homeDeliveryOptions[0].default_shipping &&
                                                        homeDeliveryOptions[0].default_shipping.products &&
                                                        homeDeliveryOptions[0].default_shipping.products.map(
                                                            (item, index) => (
                                                                <>
                                                                    <li key={`item_default_${index}`}>{item}</li>
                                                                </>
                                                            ),
                                                        )}
                                                </ul>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Typography className="ChooseDeliveryOptionText">
                                            <FormattedMessage
                                                id="Checkout.Delivery.ChooseDeliveryOption"
                                                defaultMessage="Choose Delivery Option"
                                            />
                                        </Typography>
                                        {homeDeliveryOptions[0] &&
                                            homeDeliveryOptions[0].default_shipping.delivery_options &&
                                            Object.keys(homeDeliveryOptions[0].default_shipping.delivery_options) &&
                                            Object.keys(homeDeliveryOptions[0].default_shipping.delivery_options).map(
                                                (deliveryOption, index) => (
                                                    <FormGroup row>
                                                        <FormControlLabel
                                                            control={
                                                                <Checkbox
                                                                    className="checkout-arabic-padding"
                                                                    onChange={e => {
                                                                        const name = 'deliveryOption';
                                                                        setAddress({
                                                                            ...address,
                                                                            [name]:
                                                                                homeDeliveryOptions[0].default_shipping
                                                                                    .delivery_options[deliveryOption]
                                                                                    .code,
                                                                        });
                                                                    }}
                                                                    checked={
                                                                        address.deliveryOption ===
                                                                        homeDeliveryOptions[0].default_shipping
                                                                            .delivery_options[deliveryOption].code
                                                                    }
                                                                    icon={<RadioButtonUnchecked />}
                                                                    checkedIcon={<CheckCircle />}
                                                                />
                                                            }
                                                            label={
                                                                <>
                                                                    {homeDeliveryOptions[0].default_shipping
                                                                        .delivery_options[deliveryOption].is_free ===
                                                                    '' ? (
                                                                        <Typography className="checkbox-style arabic-right">
                                                                            <span className="bold-text">
                                                                                {
                                                                                    homeDeliveryOptions[0]
                                                                                        .default_shipping
                                                                                        .delivery_options[
                                                                                        deliveryOption
                                                                                    ].name
                                                                                }
                                                                            </span>
                                                                            &nbsp; -&nbsp;
                                                                            {`${currency} ${homeDeliveryOptions[0].default_shipping.delivery_options[deliveryOption].shiping_charges}`}
                                                                        </Typography>
                                                                    ) : (
                                                                        <Typography className="checkbox-style arabic-right">
                                                                            <span className="bold-text">
                                                                                {
                                                                                    homeDeliveryOptions[0]
                                                                                        .default_shipping
                                                                                        .delivery_options[
                                                                                        deliveryOption
                                                                                    ].name
                                                                                }
                                                                            </span>
                                                                            &nbsp; -&nbsp;
                                                                            {`${homeDeliveryOptions[0].default_shipping.delivery_options[deliveryOption].is_free}`}
                                                                            <>
                                                                                <FCBenfitTag
                                                                                    show={
                                                                                        address.deliveryOption ===
                                                                                            homeDeliveryOptions[0]
                                                                                                .default_shipping
                                                                                                .delivery_options[
                                                                                                deliveryOption
                                                                                            ].code &&
                                                                                        homeDeliveryOptions[0]
                                                                                            .default_shipping
                                                                                            .delivery_options[
                                                                                            deliveryOption
                                                                                        ].fc_shipping
                                                                                    }
                                                                                />
                                                                            </>
                                                                        </Typography>
                                                                    )}
                                                                    {homeDeliveryOptions[0].default_shipping
                                                                        .delivery_options[deliveryOption]
                                                                        .home_delivery_message && (
                                                                        <Typography className="shipping-message">
                                                                            {
                                                                                homeDeliveryOptions[0].default_shipping
                                                                                    .delivery_options[deliveryOption]
                                                                                    .home_delivery_message
                                                                            }
                                                                        </Typography>
                                                                    )}
                                                                </>
                                                            }
                                                        />
                                                    </FormGroup>
                                                ),
                                            )}
                                    </Grid>
                                    {isValidate && address.deliveryOption === '' && (
                                        <Grid item xs={12} style={{ paddingTop: '0.8rem' }}>
                                            <FormHelperText className="input-error">
                                                <FormattedMessage
                                                    id="Checkout.Delivery.NoDeliveryOption"
                                                    defaultMessage="Please select delivery option"
                                                />
                                            </FormHelperText>
                                        </Grid>
                                    )}
                                    <Grid item xs={12}>
                                        <div className="border-line" />
                                    </Grid>
                                </Grid>
                            )}
                            {homeDeliveryOptions[0] &&
                                homeDeliveryOptions[0].vendor_shipping_options &&
                                homeDeliveryOptions[0].vendor_shipping_options.map((vender, i) => (
                                    <Grid container key={`vender_${i}`}>
                                        {vender && (
                                            <Grid container className="shippment" spacing={3}>
                                                <Grid item xs={12} md={6}>
                                                    <Typography>{`Shipment ${
                                                        homeDeliveryOptions[0].default_shipping ? i + 2 : i + 1
                                                    } of ${getTotalShippment}`}</Typography>
                                                    <Grid container>
                                                        <Grid item xs={12} className="option-cart">
                                                            <ul>
                                                                {vender.products &&
                                                                    vender.products.map((item, index) => (
                                                                        <>
                                                                            <li key={`item_vender_${index}`}>{item}</li>
                                                                        </>
                                                                    ))}
                                                            </ul>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Typography className="ChooseDeliveryOptionText">
                                                        <FormattedMessage
                                                            id="Checkout.Delivery.ChooseDeliveryOption"
                                                            defaultMessage="Choose Delivery Option"
                                                        />
                                                    </Typography>
                                                    {vender.delivery_options &&
                                                        Object.keys(vender.delivery_options) &&
                                                        Object.keys(vender.delivery_options).map(
                                                            (deliveryOption, index) => (
                                                                <FormGroup row>
                                                                    <FormControlLabel
                                                                        control={
                                                                            <Checkbox
                                                                                className="checkout-arabic-padding"
                                                                                checked={true}
                                                                                icon={<RadioButtonUnchecked />}
                                                                                checkedIcon={<CheckCircle />}
                                                                            />
                                                                        }
                                                                        label={
                                                                            <>
                                                                                {vender.delivery_options[deliveryOption]
                                                                                    .is_free === '' ? (
                                                                                    <Typography className="checkbox-style arabic-right">
                                                                                        <span className="bold-text">
                                                                                            {
                                                                                                vender.delivery_options[
                                                                                                    deliveryOption
                                                                                                ].name
                                                                                            }
                                                                                        </span>
                                                                                        &nbsp; -&nbsp;
                                                                                        {`${currency} ${vender.delivery_options[deliveryOption].shiping_charges}`}
                                                                                    </Typography>
                                                                                ) : (
                                                                                    <Typography className="checkbox-style arabic-right">
                                                                                        <span className="bold-text">
                                                                                            {
                                                                                                vender.delivery_options[
                                                                                                    deliveryOption
                                                                                                ].name
                                                                                            }
                                                                                        </span>
                                                                                        &nbsp; -&nbsp;
                                                                                        {`${vender.delivery_options[deliveryOption].is_free}`}
                                                                                        <>
                                                                                            <FCBenfitTag
                                                                                                show={
                                                                                                    vender
                                                                                                        .delivery_options[
                                                                                                        deliveryOption
                                                                                                    ].fc_shipping
                                                                                                }
                                                                                            />
                                                                                        </>
                                                                                    </Typography>
                                                                                )}
                                                                                {vender.delivery_options[deliveryOption]
                                                                                    .home_delivery_message && (
                                                                                    <Typography className="shipping-message">
                                                                                        {
                                                                                            vender.delivery_options[
                                                                                                deliveryOption
                                                                                            ].home_delivery_message
                                                                                        }
                                                                                    </Typography>
                                                                                )}
                                                                            </>
                                                                        }
                                                                    />
                                                                </FormGroup>
                                                            ),
                                                        )}
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <div className="border-line" />
                                                </Grid>
                                            </Grid>
                                        )}
                                    </Grid>
                                ))}
                        </Grid>
                    </Grid>
                </>
            )}

            {optionLoader && (
                <Grid container alignItems="center">
                    <Spinner />
                </Grid>
            )}
        </Grid>
    );
}
