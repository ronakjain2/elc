import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';

import { FormattedMessage } from 'react-intl';
import PhoneNumber from 'commonComponet/PhoneNumber';
import { isEmail } from 'components/SignIn_SignUp/utils';
import Hidden from '@material-ui/core/Hidden';
import { isValidateSpecialCharacters } from 'components/Cart/Checkout/utils';

export default function CheckoutNewAddress({
    address,
    setAddress,
    isValidate,
    loginUserId,
    isCorrectPhone,
    setIsCorrectPhone,
    whatsappOptin,
    setWhatsappOptin,
}) {
    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...address,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setAddress(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onChangeHandler = (name) => (event) => {
        if (name === 'apartmentNo' || name === 'address' || name === 'areaAndLocality') {
            if (event.target.value.indexOf('~') !== -1) {
                return;
            }
        }
        if (name === 'apartmentNo' || name === 'address' || name === 'areaAndLocality') {
            if (event.target.value === ' ') {
                return;
            }
        }

        if (
            name === 'firstName' ||
            name === 'lastName' ||
            name === 'apartmentNo' ||
            name === 'address' ||
            name === 'areaAndLocality'
        ) {
            if (isValidateSpecialCharacters(event.target.value)) {
                return;
            }
        }
        if (name === 'firstName' || name === 'lastName') {
            const re = /^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g;
            if (event.target.value && !re.test(event.target.value)) {
                return '';
            }
        }
        const re_1 = /^(?!\s*$).+/g;
        if (event.target.value && !re_1.test(event.target.value)) {
            return '';
        }
        setAddress({ ...address, [name]: event.target.value });
    };

    const handleChangeCheckbox = (name) => (event) => {
        setAddress({ ...address, [name]: event.target.checked });
    };

    return (
        <Grid container justify="space-between">
            {/** First Name */}
            <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.FirstName" defaultMessage="First Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={address.firstName}
                    onChange={onChangeHandler('firstName')}
                    fullWidth
                    className="inputBox"
                    error={isValidate && address.firstName === ''}
                    inputProps={{ maxLength: 20 }}
                />
                {isValidate && address.firstName === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.FirstName.Error" defaultMessage="Please enter first name" />
                    </FormHelperText>
                )}
            </Grid>
            {/** Last Name */}
            <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.LastName" defaultMessage="Last Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={address.lastName}
                    onChange={onChangeHandler('lastName')}
                    fullWidth
                    className="inputBox"
                    error={isValidate && address.lastName === ''}
                    inputProps={{ maxLength: 19 }}
                />
                {isValidate && address.lastName === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.LastName.Error" defaultMessage="Please enter last name" />
                    </FormHelperText>
                )}
            </Grid>
            {/** Phone Number */}
            <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.ContactNumber" defaultMessage="Contact Number*" />
                </Typography>

                <PhoneNumber
                    parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                    carrier_code={address.countryCode}
                    phoneNo={address.phoneNumber}
                />

                {isValidate && address.phoneNumber === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.PhoneNumber.Empty" defaultMessage="Please enter contact number" />
                    </FormHelperText>
                )}

                {isValidate && address.phoneNumber !== '' && !isCorrectPhone && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="Common.PhoneNumber.InValid"
                            defaultMessage="Please enter contact number"
                        />
                    </FormHelperText>
                )}
            </Grid>
            <Hidden only={['md', 'lg', 'xl']}>
                {/** Whatsapp Optin */}
                <Grid item xs={12} className="arabic-right">
                    <FormControlLabel
                        control={
                            <Checkbox
                                size="small"
                                checked={whatsappOptin}
                                onChange={(event) => {
                                    setWhatsappOptin(event.target.checked);
                                }}
                            />
                        }
                        label={
                            <FormattedMessage
                                id="Checkout.Delivery.Whatsapp"
                                defaultMessage="Receive order and delivery updates via WhatsApp!"
                            />
                        }
                    />
                </Grid>
            </Hidden>
            {/** Email */}
            <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r-t">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSingUp.Email" defaultMessage="Email*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={address.email}
                    onChange={onChangeHandler('email')}
                    fullWidth
                    className="inputBox"
                    error={isValidate && !isEmail(address.email)}
                />
                {isValidate && !isEmail(address.email) && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Email.Error" defaultMessage="Please Enter Valid Email Address" />
                    </FormHelperText>
                )}
            </Grid>

            <Hidden only={['xs', 'sm']}>
                {/** Whatsapp Optin */}
                <Grid item xs={12} className="arabic-right">
                    <FormControlLabel
                        control={
                            <Checkbox
                                size="small"
                                checked={whatsappOptin}
                                onChange={(event) => {
                                    setWhatsappOptin(event.target.checked);
                                }}
                            />
                        }
                        label={
                            <FormattedMessage
                                id="Checkout.Delivery.Whatsapp"
                                defaultMessage="Receive order and delivery updates via WhatsApp!"
                            />
                        }
                    />
                </Grid>
            </Hidden>

            {/** Apartment / Unit Number */}
            <Grid item xs={12} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage
                        id="Checkout.Delivery.ApartmentUnitNumber"
                        defaultMessage="Apartment / Unit Number*"
                    />
                </Typography>
                <TextField
                    variant="outlined"
                    value={address.apartmentNo}
                    onChange={onChangeHandler('apartmentNo')}
                    fullWidth
                    inputProps={{
                        maxLength: 50,
                    }}
                    className="inputBox"
                    error={isValidate && address.apartmentNo === ''}
                />
                {isValidate && address.apartmentNo === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="Checkout.Delivery.Error.ApartmentNo"
                            defaultMessage="Please enter Apartment / Unit Number"
                        />
                    </FormHelperText>
                )}
            </Grid>
            {/** Address */}
            <Grid item xs={12} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="Checkout.Delivery.Address" defaultMessage="Address*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={address.address}
                    onChange={onChangeHandler('address')}
                    fullWidth
                    inputProps={{
                        maxLength: 50,
                    }}
                    className="inputBox"
                    error={isValidate && address.address === ''}
                />
                {isValidate && address.apartmentNo === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="Checkout.Delivery.PleaseEnterAddress"
                            defaultMessage="Please enter address"
                        />
                    </FormHelperText>
                )}
            </Grid>
            {/** Area / Locality */}
            <Grid item xs={12} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="Checkout.Delivery.AreaLocality" defaultMessage="Area / Locality*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={address.areaAndLocality}
                    onChange={onChangeHandler('areaAndLocality')}
                    fullWidth
                    inputProps={{
                        maxLength: 50,
                    }}
                    className="inputBox"
                    error={isValidate && address.areaAndLocality === ''}
                />
                {isValidate && address.areaAndLocality === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="Checkout.Delivery.areaAndLocality.error"
                            defaultMessage="Please enter area / locality"
                        />
                    </FormHelperText>
                )}
            </Grid>
            {/** Address Type */}
            <Grid item xs={12} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="Checkout.Delivery.AddressType" defaultMessage="Address Type*" />
                </Typography>
                <RadioGroup
                    row
                    aria-label="position"
                    name="position"
                    value={address.type}
                    onChange={onChangeHandler('type')}
                >
                    <FormControlLabel
                        value="home"
                        control={<Radio />}
                        className={address.type === 'home' ? 'active-radio' : ''}
                        label={<FormattedMessage id="Checkout.Delivery.Home" defaultMessage="Home" />}
                        labelPlacement="end"
                    />
                    <FormControlLabel
                        value="work"
                        control={<Radio className="arabic-right-margin" />}
                        className={address.type === 'work' ? 'active-radio' : ''}
                        label={<FormattedMessage id="Checkout.Delivery.Work" defaultMessage="Work" />}
                        labelPlacement="end"
                    />
                </RadioGroup>
            </Grid>
            {/** save address fecture */}
            {loginUserId && (
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox checked={address.saveFuture} onChange={handleChangeCheckbox('saveFuture')} />
                        }
                        label={
                            <Typography className="checkbox-style">
                                <FormattedMessage
                                    id="Checkout.Delivery.SaveThisAddress"
                                    defaultMessage="Save this address for the future"
                                />
                            </Typography>
                        }
                    />
                    <FormControlLabel
                        className="margin-right"
                        control={
                            <Checkbox
                                checked={address.defaultAddress}
                                onChange={handleChangeCheckbox('defaultAddress')}
                            />
                        }
                        label={
                            <Typography className="checkbox-style">
                                <FormattedMessage
                                    id="Checkout.Delivery.MakeThisAddressDefault"
                                    defaultMessage="Make this Address Default"
                                />
                            </Typography>
                        }
                    />
                </FormGroup>
            )}
        </Grid>
    );
}
