import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CheckCircle from '@material-ui/icons/CheckCircle';
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { FormattedMessage } from 'react-intl';
import Add from '@material-ui/icons/Add';
import { replaceTilCharacterToComma } from 'components/Cart/Checkout/Delivery/utils';
import ErrorOutlinedIcon from '@material-ui/icons/ErrorOutlined';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import { withRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';

function CheckoutDeliveryAddressList({
    addresslist,
    country,
    addressId,
    setAddressId,
    isValidate,
    setShowNewAddress,
    history,
}) {
    const [addresses, setAddresses] = useState([]);
    const state = useSelector((state) => state);

    const store_locale = state && state.auth && state.auth.store_locale;
    useEffect(() => {
        setAddresses([...addresslist] || []);
    }, [addresslist]);

    const gotoMyAddresses = (data) =>
        history.push({ pathname: `/${store_locale}/my-addresses`, state: { addressRow: data } });

    return (
        <Grid container>
            {addresses &&
                addresses.map((address, index) => (
                    <Grid item xs={12} md={6} style={{ padding: `0px 4px` }} key={`user_saved_address_${index}`}>
                        <Grid container className="address-list-container">
                            <Grid item xs={10}>
                                <FormGroup>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                className="checkout-arabic-padding"
                                                onChange={() => {
                                                    setAddressId(address.Id);
                                                    setAddresses(addresslist);
                                                }}
                                                checked={addressId === address.Id}
                                                icon={<RadioButtonUnchecked />}
                                                checkedIcon={<CheckCircle />}
                                                disabled={!address.is_valid}
                                            />
                                        }
                                        label={
                                            <>
                                                <Typography className="address-nickname">
                                                    {address &&
                                                        address.userFirstName &&
                                                        `${address.userFirstName} ${address.userLastName}`}
                                                </Typography>
                                                <Typography className="checkbox-style-subtext">
                                                    {address.address_type}
                                                </Typography>
                                            </>
                                        }
                                    />
                                </FormGroup>
                            </Grid>
                            {!address.is_valid && (
                                <Grid item xs={2} className="d-flex justify-content-end align-items-center">
                                    <IconButton
                                        className="edit-button"
                                        size="small"
                                        onClick={() => gotoMyAddresses(address)}
                                    >
                                        <EditIcon fontSize="small" />
                                    </IconButton>
                                </Grid>
                            )}
                            <Grid item xs={12}>
                                <Typography className="address-details">
                                    {address.street && replaceTilCharacterToComma(address.street)}
                                </Typography>
                                <Typography className="address-details">{country}</Typography>
                                <Typography className="address-details">{address.city}</Typography>
                                <Typography className="address-details dir-ltr">
                                    {`+${address.carrier_code} ${address.telephone}`}
                                </Typography>
                            </Grid>
                            {!address.is_valid && address.error_message ? (
                                <Grid item xs={12} className="arabic-right address-messages">
                                    <Button
                                        disabled={true}
                                        className="address-error text-align-start"
                                        startIcon={<ErrorOutlinedIcon />}
                                    >
                                        {address.error_message}
                                    </Button>
                                </Grid>
                            ) : (
                                <Grid item xs={12} className="arabic-right">
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                className="checkout-arabic-padding"
                                                disabled={addressId !== address.Id}
                                                size="small"
                                                checked={address.whatsapp_optin === 1}
                                                onChange={() => {
                                                    let addArr = [...addresses];
                                                    let addObj = addArr[index];
                                                    addObj.whatsapp_optin = addObj.whatsapp_optin === 1 ? 0 : 1;
                                                    setAddresses(addArr);
                                                }}
                                            />
                                        }
                                        label={
                                            <FormattedMessage
                                                id="Checkout.Delivery.Whatsapp"
                                                defaultMessage="Receive order and delivery updates via WhatsApp!"
                                            />
                                        }
                                    />
                                </Grid>
                            )}
                        </Grid>
                    </Grid>
                ))}

            <Grid item xs={12} className="arabic-right">
                <Button startIcon={<Add />} className="add-new-address-button" onClick={() => setShowNewAddress(true)}>
                    <FormattedMessage id="Checkout.Cart.Delivery.AddNewAddress" defaultMessage="Add New Address" />
                </Button>
            </Grid>

            {isValidate && (!addressId || addressId === '') && (
                <FormHelperText className="input-error">
                    <FormattedMessage
                        id="Checkout.Cart.Delivery.NoAddressSelected"
                        defaultMessage="Please select address"
                    />
                </FormHelperText>
            )}
        </Grid>
    );
}

export default withRouter(CheckoutDeliveryAddressList);
