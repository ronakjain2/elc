import React, { lazy, Suspense, useState, useEffect, useRef } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

const CheckoutNewAddress = lazy(() => import('./components/NewAddress'));
const CheckoutDeliveryOptions = lazy(() => import('./components/DeliveryOptions'));
const CheckoutDeliveryAddressList = lazy(() => import('./components/AddressList'));

//Check previous state
function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

export default function CheckoutHomeDelivery({
    address,
    setAddress,
    isValidate,
    onClickContinue,
    city,
    country,
    addressId,
    setAddressId,
    isCorrectPhone,
    setIsCorrectPhone,
    whatsappOptin,
    setWhatsappOptin,
}) {
    const state = useSelector((state) => state);

    const loginUserId = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_id;

    const prevDeliveryDetails = state && state.checkoutDelivery && state.checkoutDelivery.prevDeliveryDetails;

    let addressData =
        (state &&
            state.checkoutDelivery &&
            state.checkoutDelivery.deliveryDetails &&
            state.checkoutDelivery.deliveryDetails.addressData && [
                ...state.checkoutDelivery.deliveryDetails.addressData,
            ]) ||
        [];
    addressData = addressData.filter(
        (address) =>
            (address && address.city && address.city.toLowerCase()) === (city && city.name && city.name.toLowerCase()),
    );

    const [showNewAddress, setShowNewAddress] = useState(true);

    const prevShowAddress = usePrevious({ showNewAddress, setShowNewAddress });

    useEffect(() => {
        if (addressData && addressData.length > 0 && prevShowAddress === undefined) {
            setShowNewAddress(false);
        }
    }, [addressData, prevShowAddress]);

    //Check if prev address is available and save list is available than check which type user selected(new/save)
    useEffect(() => {
        try {
            if (prevDeliveryDetails && Object.keys(prevDeliveryDetails).length > 0) {
                //check click and collect data not available
                if (prevDeliveryDetails && prevDeliveryDetails.fname === '') {
                    // check new address available or not
                    if (prevDeliveryDetails && prevDeliveryDetails.address_id === '') {
                        setShowNewAddress(true);
                    } else if (prevDeliveryDetails && prevDeliveryDetails.addressId !== '') {
                        setShowNewAddress(false);
                    }
                }
            }
        } catch (err) {}
    }, [prevDeliveryDetails]);
    return (
        <Suspense fallback={''}>
            <Grid container className="checkout-delivery">
                <Grid item xs={12}>
                    {/** New Address */}
                    {(!addressData || addressData.length === 0 || showNewAddress) && (
                        <CheckoutNewAddress
                            address={address}
                            setAddress={setAddress}
                            isValidate={isValidate}
                            loginUserId={loginUserId}
                            isCorrectPhone={isCorrectPhone}
                            setIsCorrectPhone={setIsCorrectPhone}
                            whatsappOptin={whatsappOptin}
                            setWhatsappOptin={setWhatsappOptin}
                        />
                    )}
                    {/** Address List */}
                    {addressData && addressData.length > 0 && !showNewAddress && (
                        <CheckoutDeliveryAddressList
                            addresslist={[...addressData]}
                            country={country}
                            addressId={addressId}
                            isValidate={isValidate}
                            setAddressId={setAddressId}
                            setShowNewAddress={setShowNewAddress}
                        />
                    )}
                </Grid>

                <Grid item xs={12}>
                    <div className="border-line" />
                </Grid>

                {/** Delivery Option */}
                <Grid item xs={12}>
                    <CheckoutDeliveryOptions address={address} setAddress={setAddress} isValidate={isValidate} />
                </Grid>

                {/** Continue Button */}
                <Grid item xs={12} className="arabic-right">
                    {(!addressData || addressData.length !== 0) && showNewAddress && (
                        <Button className="checkout-cancel-button" onClick={() => setShowNewAddress(false)}>
                            <FormattedMessage id="Checkout.Cart.Delivery.Cancel" defaultMessage="Cancel" />
                        </Button>
                    )}

                    <Button className="checkout-continue-button" onClick={() => onClickContinue(showNewAddress)}>
                        <FormattedMessage id="Checkout.Delivery.Continue" defaultMessage="Continue" />
                    </Button>
                </Grid>
            </Grid>
        </Suspense>
    );
}
