import React from 'react';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import MenuItem from '@material-ui/core/MenuItem';

import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { getCityObject } from '../../utils';

export default function CountryAndCity({
    countryName,
    setCity,
    city,
    isValidate,
    country,
    checkDeliveryOption,
    setCityName,
    cityName,
    removeOldData,
}) {
    const state = useSelector((state) => state);
    let cityList = state && state.checkoutDelivery && state.checkoutDelivery.countryList;

    if (country === 'UAE') {
        cityList = cityList.filter((city) => city.id === 'AE');
    } else if (country === 'KSA') {
        cityList = cityList.filter((city) => city.id === 'SA');
    } else {
        cityList = [];
    }

    //City name unique
    cityList = cityList.filter((item) => item.hasOwnProperty('available_regions'));
    //end city name unique

    return (
        <Grid container className="checkout-delivery">
            <Grid item xs={12}>
                <Typography className="title">
                    <FormattedMessage id="Checkout.Delivery" defaultMessage="Delivery" />
                </Typography>
            </Grid>

            <Grid item xs={12}>
                <Grid container justify="space-between">
                    {/** Country */}
                    <Grid item xs={6} className="padding-t-input-box padding-l-r">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="Checkout.Delivery.Country" defaultMessage="Country*" />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={countryName}
                            disabled={true}
                            fullWidth
                            className="inputBox countryName"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        {country === 'UAE' && (
                                            <img
                                                alt="uaeFlag"
                                                src="/images/header/top/Icon_Flag_UAE_v1.svg"
                                                className="countryFlag"
                                            />
                                        )}
                                        {country === 'KSA' && (
                                            <img
                                                alt="uaeFlag"
                                                src="/images/header/top/Icon_Flag_KSA_v1.svg"
                                                className="countryFlag"
                                            />
                                        )}
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </Grid>
                    {/** City */}
                    <Grid item xs={6} className="padding-t-input-box padding-l-r">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="Checkout.Delivery.City" defaultMessage="City*" />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={cityName}
                            select
                            fullWidth
                            className="inputBox"
                            onChange={(e) => {
                                setCityName(e.target.value);
                                setCity(
                                    getCityObject(
                                        e.target.value,
                                        (cityList && cityList[0] && cityList[0].available_regions) || [],
                                    ),
                                );
                                checkDeliveryOption(
                                    getCityObject(
                                        e.target.value,
                                        (cityList && cityList[0] && cityList[0].available_regions) || [],
                                    ),
                                );
                                removeOldData();
                            }}
                        >
                            {(!cityList || cityList.length === 0) && <MenuItem></MenuItem>}
                            {cityList &&
                                cityList[0] &&
                                cityList[0].available_regions &&
                                cityList[0].available_regions.map((city, index) => (
                                    <MenuItem key={`checkout_city_list_${index}_${city.id}`} value={city.name}>
                                        {city && city.name}
                                    </MenuItem>
                                ))}
                        </TextField>
                        {isValidate && city === '' && (
                            <FormHelperText className="input-error">
                                <FormattedMessage
                                    id="Checkout.Delivery.PleaseSelectStateCity"
                                    defaultMessage="Please Select State/City"
                                />
                            </FormHelperText>
                        )}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
