import React, { useState, useEffect, lazy, Suspense } from 'react';
import Grid from '@material-ui/core/Grid';
//import useGeolocation from 'react-hook-geolocation';
import Button from '@material-ui/core/Button';
import Spinner from 'commonComponet/Spinner';
import { useDispatch, useSelector } from 'react-redux';
import { actions as checkoutDeliveryActions } from 'components/Cart/Checkout/Delivery/redux/actions';
import { FormattedMessage } from 'react-intl';
import { createPickUpStoreApiObject, checkPickUpStoreObjet, fillPickUpAndStore, scrollToElement } from '../../utils';

const PickUpCollectMap = lazy(() => import('./Components/Map'));
const CheckoutStoreLocatorList = lazy(() => import('./Components/StoreList'));
const SelectedPickUpStore = lazy(() => import('./Components/SelectedPickUpStore'));
const PickUpStoreUserForm = lazy(() => import('./Components/UserForm'));

export default function PickAndCollect({ city, address, setAddress, setMainIsValidate }) {
    //const geolocation = useGeolocation();

    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    const country = state && state.auth && state.auth.country;
    const store_id = state && state.auth && state.auth.currentStore;
    const quote_id = state && state.auth && state.auth.quote_id;
    const loginUser = state && state.auth && state.auth.loginUser;
    const language = state && state.auth && state.auth.language;
    const storeLocatorList = state && state.checkoutDelivery && state.checkoutDelivery.storeLocatorList;
    const availableCitys = state && state.checkoutDelivery && state.checkoutDelivery.availableCitys;

    const next_day_delivery = (storeLocatorList && storeLocatorList.next_day_delivery) || [];
    const two_three_days_delivery = (storeLocatorList && storeLocatorList.two_three_days_delivery) || [];
    const express_day_delivery = (storeLocatorList && storeLocatorList.express_delivery) || [];
    const deliveryDetails = state && state.checkoutDelivery && state.checkoutDelivery.deliveryDetails;

    let clickAndCollectData = (state && state.checkoutDelivery && state.checkoutDelivery.clickAndCollectData) || {};
    const clickAndCollectLoader = state && state.checkoutDelivery && state.checkoutDelivery.clickAndCollectOptionLoader;

    const guestDetails = state && state.checkoutLogin && state.checkoutLogin.guestDetails;

    const [mapZoom, setMapZoom] = useState(4);
    const [latlang, setLatLong] = useState({
        lat: null,
        long: null,
    });
    const [selectedTab, setSelectedTab] = useState('');
    const [selectedStorelocator, setSelectedStorelocator] = useState(null);
    const [userForm, setUserForm] = useState({
        firstName: '',
        lastName: '',
        email: (deliveryDetails && deliveryDetails.email) || '',
        phoneNumber: '',
        countryCode: '',
    });
    const [isValidate, setIsValidate] = useState(false);
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const prevDeliveryDetails = state && state.checkoutDelivery && state.checkoutDelivery.prevDeliveryDetails;

    //set email id
    useEffect(() => {
        try {
            if (deliveryDetails && deliveryDetails.email) {
                let email = 'email';
                setUserForm((a) => ({ ...a, [email]: deliveryDetails && deliveryDetails.email }));
            }
        } catch (err) {}
    }, [deliveryDetails]);

    //Set First name and last name
    useEffect(() => {
        try {
            if (loginUser && Object.keys(loginUser).length !== 0) {
                setUserForm((a) => ({
                    ...a,
                    firstName: loginUser.firstname,
                    lastName: loginUser.lastname,
                    phoneNumber: loginUser.phone_number,
                    countryCode: loginUser.carrier_code,
                }));
            }
        } catch (err) {}
    }, [loginUser]);

    useEffect(() => {
        try {
            if (
                clickAndCollectData &&
                Object.keys(clickAndCollectData) &&
                Object.keys(clickAndCollectData).length > 0
            ) {
                if (next_day_delivery[0]) {
                    clickAndCollectData &&
                        Object.keys(clickAndCollectData) &&
                        Object.keys(clickAndCollectData).length > 0 &&
                        Object.keys(clickAndCollectData).map((key) => {
                            if (clickAndCollectData[key].code === 'nextday_shipping_nextday_shipping') {
                                if (next_day_delivery.length === 0) {
                                    delete clickAndCollectData[key];
                                    return clickAndCollectData;
                                }
                            } else if (clickAndCollectData[key].code === 'express_shipping_express_shipping') {
                                if (express_day_delivery.length === 0) {
                                    delete clickAndCollectData[key];
                                    return clickAndCollectData;
                                }
                            } else {
                                if (two_three_days_delivery.length === 0) {
                                    delete clickAndCollectData[key];
                                    return clickAndCollectData;
                                }
                            }
                            return clickAndCollectData;
                        });
                    setLatLong({
                        lat: next_day_delivery[0] && next_day_delivery[0].lattitude,
                        long: next_day_delivery[0] && next_day_delivery[0].longitude,
                    });
                    setMapZoom(8);
                } else if (two_three_days_delivery[0]) {
                    if (two_three_days_delivery[0]) {
                        clickAndCollectData &&
                            Object.keys(clickAndCollectData) &&
                            Object.keys(clickAndCollectData).length > 0 &&
                            Object.keys(clickAndCollectData).map((key) => {
                                if (clickAndCollectData[key].code === 'nextday_shipping_nextday_shipping') {
                                    if (next_day_delivery.length === 0) {
                                        delete clickAndCollectData[key];
                                        return clickAndCollectData;
                                    }
                                } else if (clickAndCollectData[key].code === 'express_shipping_express_shipping') {
                                    if (express_day_delivery.length === 0) {
                                        delete clickAndCollectData[key];
                                        return clickAndCollectData;
                                    }
                                } else {
                                    if (two_three_days_delivery.length === 0) {
                                        delete clickAndCollectData[key];
                                        return clickAndCollectData;
                                    }
                                }
                                return clickAndCollectData;
                            });
                        setLatLong({
                            lat: two_three_days_delivery[0] && two_three_days_delivery[0].lattitude,
                            long: two_three_days_delivery[0] && two_three_days_delivery[0].longitude,
                        });
                        setMapZoom(8);
                    }
                } else {
                    if (express_day_delivery[0]) {
                        clickAndCollectData &&
                            Object.keys(clickAndCollectData) &&
                            Object.keys(clickAndCollectData).length > 0 &&
                            Object.keys(clickAndCollectData).map((key) => {
                                if (clickAndCollectData[key].code === 'nextday_shipping_nextday_shipping') {
                                    if (next_day_delivery.length === 0) {
                                        delete clickAndCollectData[key];
                                        return clickAndCollectData;
                                    }
                                } else if (clickAndCollectData[key].code === 'express_shipping_express_shipping') {
                                    if (express_day_delivery.length === 0) {
                                        delete clickAndCollectData[key];
                                        return clickAndCollectData;
                                    }
                                } else {
                                    if (two_three_days_delivery.length === 0) {
                                        delete clickAndCollectData[key];
                                        return clickAndCollectData;
                                    }
                                }
                                return clickAndCollectData;
                            });
                        setLatLong({
                            lat: express_day_delivery[0] && express_day_delivery[0].lattitude,
                            long: express_day_delivery[0] && express_day_delivery[0].longitude,
                        });
                        setMapZoom(8);
                    }
                }
            }
        } catch (err) {}
    }, [next_day_delivery, express_day_delivery, two_three_days_delivery, clickAndCollectData]);

    //set available tab
    useEffect(() => {
        try {
            if (clickAndCollectData) {
                clickAndCollectData &&
                    Object.keys(clickAndCollectData) &&
                    Object.keys(clickAndCollectData).map((tab, index) => {
                        if (index === 0) {
                            setSelectedTab(clickAndCollectData[tab].code);
                        }
                        return tab;
                    });
            }
        } catch (err) {}
    }, [clickAndCollectData]);

    // Get all store locator
    useEffect(() => {
        try {
            dispatch(
                checkoutDeliveryActions.getCheckoutStoreLocatorRequest({
                    country: country === 'UAE' ? 'AE' : 'SA',
                    city: city.name,
                    storeId: store_id,
                    quote_id,
                }),
            );
            dispatch(
                checkoutDeliveryActions.getClickAndCollectOptionRequest({
                    store_id,
                    quote_id,
                    country: country === 'UAE' ? 'AE' : 'SA',
                    city: city && city.name,
                    fullfilment_type: 'SHIPFORPICKUP',
                }),
            );
        } catch (err) {}
    }, [dispatch, city, store_id, country, quote_id]);

    const onMarkerClick = (item) => {
        if (item) {
            setLatLong({
                lat: item.lattitude,
                long: item.longitude,
            });
            setMapZoom(17);
            setSelectedStorelocator(item);
        }
    };

    //Selected store on pick and collect
    const onSelectedStoreLocator = (store) => {
        if (selectedStorelocator) {
            if (selectedStorelocator.id === store.id) {
                setSelectedStorelocator(null);
            } else {
                setSelectedStorelocator(store);
                onMarkerClick(store);
            }
        } else {
            setSelectedStorelocator(store);
            onMarkerClick(store);
        }
    };

    //pickupstore continue
    const onClickPickupContinue = () => {
        setIsValidate(true);
        setMainIsValidate(true);

        if (checkPickUpStoreObjet(selectedStorelocator, userForm, address, isCorrectPhone)) {
            scrollToElement('.input-error');
            return;
        }

        dispatch(
            checkoutDeliveryActions.setCheckoutDeliveryRequest({
                store_id,
                quote_id,
                whatsapp_optin: 0,
                ...createPickUpStoreApiObject(selectedStorelocator, loginUser, userForm, address, selectedTab),
            }),
        );
        //Save selected/enter address
        dispatch(
            checkoutDeliveryActions.saveOrClearDeliveryDetails({
                city,
                pickupTab: selectedTab,
                ...createPickUpStoreApiObject(selectedStorelocator, loginUser, userForm, address, selectedTab),
            }),
        );

        setMainIsValidate(false);
    };

    //guest user
    useEffect(() => {
        try {
            if (!loginUser || Object.keys(loginUser).length === 0) {
                if (guestDetails && Object.keys(guestDetails).length > 0) {
                    setUserForm({
                        ...guestDetails,
                    });
                }
            }
        } catch (err) {}
    }, [guestDetails, loginUser]);

    //set old details if available
    useEffect(() => {
        if (prevDeliveryDetails && prevDeliveryDetails.shipping_code !== '') {
            if (prevDeliveryDetails.fname && prevDeliveryDetails.fname !== '') {
                setUserForm({
                    ...fillPickUpAndStore(prevDeliveryDetails),
                });
                setAddress((a) => ({
                    ...a,
                    message: prevDeliveryDetails.message,
                    gift_wrap_flag: prevDeliveryDetails.gift_wrap_flag,
                }));
            }
        }
    }, [prevDeliveryDetails, setAddress]);

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container>
                <Grid item xs={12} md={6}>
                    <PickUpCollectMap
                        zoom={mapZoom}
                        latlang={latlang}
                        clickAndCollectData={clickAndCollectData}
                        clickAndCollectLoader={clickAndCollectLoader}
                        storeList={
                            selectedTab === ''
                                ? []
                                : selectedTab === 'nextday_shipping_nextday_shipping'
                                ? next_day_delivery
                                : selectedTab === 'express_shipping_express_shipping'
                                ? express_day_delivery
                                : two_three_days_delivery
                        }
                        language={language}
                        onMarkerClick={onMarkerClick}
                    />
                </Grid>

                <Grid item xs={12} md={6}>
                    <CheckoutStoreLocatorList
                        storeList={
                            selectedTab === ''
                                ? []
                                : selectedTab === 'nextday_shipping_nextday_shipping'
                                ? next_day_delivery
                                : selectedTab === 'express_shipping_express_shipping'
                                ? express_day_delivery
                                : two_three_days_delivery
                        }
                        activeTab={selectedTab}
                        setSelectedTab={setSelectedTab}
                        clickAndCollectData={clickAndCollectData}
                        clickAndCollectLoader={clickAndCollectLoader}
                        selectedStorelocator={selectedStorelocator}
                        onSelectedStoreLocator={onSelectedStoreLocator}
                        availableCitys={availableCitys}
                    />
                </Grid>

                <Grid item xs={12}>
                    <SelectedPickUpStore selectedStoreLocator={selectedStorelocator} isValidate={isValidate} />
                </Grid>

                <Grid item xs={12}>
                    <div className="border-line" />
                </Grid>

                <Grid item xs={12}>
                    <PickUpStoreUserForm
                        userForm={userForm}
                        setUserForm={setUserForm}
                        isValidate={isValidate}
                        isCorrectPhone={isCorrectPhone}
                        setIsCorrectPhone={setIsCorrectPhone}
                    />
                </Grid>

                <Grid item xs={12}>
                    <div className="border-line" />
                </Grid>

                {/** Continue Button */}
                <Grid item xs={12} className="arabic-right">
                    <Button className="checkout-continue-button" onClick={() => onClickPickupContinue()}>
                        <FormattedMessage id="Checkout.Delivery.Continue" defaultMessage="Continue" />
                    </Button>
                </Grid>
            </Grid>
        </Suspense>
    );
}
