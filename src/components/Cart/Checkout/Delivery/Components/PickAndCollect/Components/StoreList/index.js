import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormattedMessage } from 'react-intl';
import StoreLocatorItem from './Component/listItem';

export default function CheckoutStoreLocatorList({
    storeList,
    activeTab,
    setSelectedTab,
    selectedStorelocator,
    onSelectedStoreLocator,
    clickAndCollectData,
    clickAndCollectLoader,
    availableCitys,
}) {
    return (
        <Grid container justify="center">
            <Grid item xs={12} md={11}>
                <Typography className="choodePickupStore-text">
                    <FormattedMessage
                        id="Checkout.Delivery.Pickup.ChoosePickupStore"
                        defaultMessage="Choose Pickup Store"
                    />
                </Typography>
            </Grid>

            <Grid item xs={12} md={11} className="pick-up-store-list-card">
                {!clickAndCollectLoader && (
                    <>
                        {(!availableCitys || availableCitys.length === 0) && (
                            <Grid container justify="space-between">
                                {clickAndCollectData &&
                                    Object.keys(clickAndCollectData) &&
                                    Object.keys(clickAndCollectData).map((tab, index) => (
                                        <Grid
                                            item
                                            xs={
                                                Object.keys(clickAndCollectData).length === 1
                                                    ? 12
                                                    : Object.keys(clickAndCollectData).length <= 3
                                                    ? 12 / Object.keys(clickAndCollectData).length
                                                    : 3
                                            }
                                            onClick={() => setSelectedTab(clickAndCollectData[tab].code)}
                                            key={`pickupAndStore_tab_${index}`}
                                            className={
                                                activeTab === clickAndCollectData[tab].code ? 'active' : 'tab-container'
                                            }
                                        >
                                            <Typography>
                                                {clickAndCollectData[tab].click_collect_frontend_name}
                                            </Typography>
                                            {clickAndCollectData[tab].home_delivery_message && (
                                                <Typography className="shipping-message">
                                                    {clickAndCollectData[tab].home_delivery_message}
                                                </Typography>
                                            )}
                                        </Grid>
                                    ))}
                            </Grid>
                        )}
                        {availableCitys && availableCitys.length > 0 && (
                            <Grid container justify="space-between">
                                <Grid item xs={12} className="active">
                                    <Typography>
                                        <FormattedMessage
                                            id="Delivery.ClickAndCollect.Other.Available.City"
                                            defaultMessage="Click and Collect Available City"
                                        />
                                    </Typography>
                                </Grid>
                            </Grid>
                        )}
                        <Grid container>
                            <StoreLocatorItem
                                list={storeList}
                                selectedStorelocator={selectedStorelocator}
                                onSelectedStoreLocator={onSelectedStoreLocator}
                                availableCitys={availableCitys}
                            />
                        </Grid>
                    </>
                )}
                {clickAndCollectLoader && (
                    <Grid container justify="center" alignItems="center">
                        <CircularProgress />
                    </Grid>
                )}
            </Grid>
        </Grid>
    );
}
