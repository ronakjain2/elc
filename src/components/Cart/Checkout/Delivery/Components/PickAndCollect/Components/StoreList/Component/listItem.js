import React from 'react';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';

export default function StoreLocatorItem({ list, selectedStorelocator, onSelectedStoreLocator, availableCitys }) {
    return (
        <Grid container className="list-container-height">
            {(!availableCitys || availableCitys.length === 0) && (
                <>
                    {list &&
                        list.map((storelocator, index) => (
                            <Grid item xs={12} key={`storelocator_list_${index}`}>
                                <Grid container className="store-list-container">
                                    <Grid item xs={2} md={1}>
                                        <Checkbox
                                            onChange={() => onSelectedStoreLocator(storelocator)}
                                            checked={
                                                storelocator.id === (selectedStorelocator && selectedStorelocator.id)
                                            }
                                            icon={<RadioButtonUnchecked />}
                                            checkedIcon={<CheckCircle />}
                                        />
                                    </Grid>
                                    <Grid item xs={10} md={11}>
                                        <Typography className="store-locator-name">{storelocator.name}</Typography>
                                        <Typography className="store-locator-address">
                                            {storelocator.address}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        ))}
                    {(!list || list.length === 0) && (
                        <Typography className="no-slot-available-message">
                            <FormattedMessage
                                id="Checkout.Delivery.Storelocator.Slot.Error"
                                defaultMessage="Sorry, no stores are available for this slot"
                            />
                        </Typography>
                    )}
                </>
            )}
            {availableCitys && availableCitys.length > 0 && (
                <>
                    <Grid container className="store-list-container" style={{ marginBottom: '1rem' }}>
                        {availableCitys &&
                            availableCitys.map((city, index) => (
                                <Grid
                                    item
                                    xs={12}
                                    key={`available_city_list_${index}`}
                                    className="no-click-and-collect-list"
                                >
                                    <Typography className="arabic-right">{city}</Typography>
                                </Grid>
                            ))}
                    </Grid>
                </>
            )}
        </Grid>
    );
}
