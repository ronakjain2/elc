import React from 'react';
import Grid from '@material-ui/core/Grid';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import { FormattedMessage } from 'react-intl';

export default function SelectedPickUpStore({ selectedStoreLocator, isValidate }) {
    return (
        <Grid container>
            <Grid item xs={12}>
                <Typography className="pick-up-store-text">
                    <FormattedMessage id="Checkout.Delivery.Pickup.PickupStore" defaultMessage="Pickup Store" />
                </Typography>
            </Grid>

            {selectedStoreLocator && selectedStoreLocator.id && (
                <Grid item xs={12} className="selected-pick-up-store-card">
                    <Grid container>
                        <Grid item xs={12}>
                            <FormGroup>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            className="checkout-arabic-padding"
                                            checked={true}
                                            icon={<RadioButtonUnchecked />}
                                            checkedIcon={<CheckCircle />}
                                        />
                                    }
                                    label={
                                        <Typography className="selected-pick-up-name">
                                            {selectedStoreLocator && selectedStoreLocator.name}
                                        </Typography>
                                    }
                                />
                            </FormGroup>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography className="selected-pick-up-address">
                                {selectedStoreLocator.address},
                            </Typography>
                            <Typography className="selected-pick-up-address">
                                {selectedStoreLocator.country},
                            </Typography>
                            <Typography className="selected-pick-up-address">{selectedStoreLocator.city}</Typography>
                            <Typography className="selected-pick-up-address">
                                {selectedStoreLocator.phone && `+${selectedStoreLocator.phone}`}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            )}

            {(!selectedStoreLocator || !selectedStoreLocator.id) && (
                <Grid item xs={12} className="selected-pick-up-store-card">
                    <Typography className="noStoreSelected">
                        <FormattedMessage
                            id="Checkout.Delivery.Pickup.NoStoreSelected"
                            defaultMessage="No Store Selected"
                        />
                    </Typography>
                </Grid>
            )}

            {isValidate && (!selectedStoreLocator || !selectedStoreLocator.id) && (
                <Grid item xs={12}>
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="Checkout.Delivery.Pickup.NoPickupStore"
                            defaultMessage="Please select pick up store"
                        />
                    </FormHelperText>
                </Grid>
            )}
        </Grid>
    );
}
