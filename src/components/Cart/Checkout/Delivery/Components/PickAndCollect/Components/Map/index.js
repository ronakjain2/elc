import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import { GoogleApiWrapper, Map, Marker } from 'google-maps-react';

const mapStyles = {
    width: '100%',
    height: '100%',
};

export class PickUpCollectMap extends Component {
    __rendermarkers = (item, index) => {
        return (
            <Marker
                key={item.id}
                onClick={(props, marker, e) => this.props.onMarkerClick(item)}
                position={{ lat: item.lattitude, lng: item.longitude }}
                icon={{ url: '/images/checkout/map-marker.png', height: 30 }}
            ></Marker>
        );
    };

    render() {
        const { zoom, latlang, storeList } = this.props;

        return (
            <Grid container>
                <Grid item xs={12} className="map-main-container">
                    <Map
                        google={this.props.google}
                        style={mapStyles}
                        center={{
                            lat: latlang.lat,
                            lng: latlang.long,
                        }}
                        initialCenter={{
                            lat: latlang.lat,
                            lng: latlang.long,
                        }}
                        zoomControl={true}
                        scaleControl={false}
                        streetViewControl={false}
                        mapTypeControl={false}
                        panControl={false}
                        rotateControl={false}
                        fullscreenControl={false}
                        zoom={zoom}
                    >
                        {storeList.map(this.__rendermarkers)}
                    </Map>
                </Grid>
            </Grid>
        );
    }
}

export default GoogleApiWrapper((props) => ({
    apiKey: 'AIzaSyAi0iRRQYErNXeAa6tZNgsevHWr6wbT-Nc',
    language: props.language,
}))(PickUpCollectMap);
