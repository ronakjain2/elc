import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import TextField from '@material-ui/core/TextField';
import { FormattedMessage } from 'react-intl';
import { isEmail } from 'components/SignIn_SignUp/utils';
import PhoneNumber from 'commonComponet/PhoneNumber';
import { isValidateSpecialCharacters } from 'components/Cart/Checkout/utils';

export default function PickUpStoreUserForm({ userForm, setUserForm, isValidate, isCorrectPhone, setIsCorrectPhone }) {
    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...userForm,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setUserForm(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onChangeHandler = (name) => (event) => {
        if (name === 'firstName' || name === 'lastName') {
            if (isValidateSpecialCharacters(event.target.value)) {
                return;
            }
        }
        if (name === 'firstName' || name === 'lastName') {
            const re = /^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g;
            if (event.target.value && !re.test(event.target.value)) {
                return '';
            }
        }
        const re_1 = /^(?!\s*$).+/g;
        if (event.target.value && !re_1.test(event.target.value)) {
            return '';
        }
        setUserForm({ ...userForm, [name]: event.target.value });
    };

    return (
        <Grid container style={{ paddingBottom: '1rem' }}>
            <Typography className="pick-up-store-text" style={{ paddingBottom: 0 }}>
                <FormattedMessage id="Checkout.Delivery.Pickup.YourDetails" defaultMessage="Your Details" />
            </Typography>

            <Grid container justify="space-between">
                {/** First Name */}
                <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                    <Typography className="login-sigup-input-text">
                        <FormattedMessage id="SignInSignUp.FirstName" defaultMessage="First Name*" />
                    </Typography>
                    <TextField
                        variant="outlined"
                        value={userForm.firstName}
                        onChange={onChangeHandler('firstName')}
                        fullWidth
                        className="inputBox"
                        error={isValidate && userForm.firstName === ''}
                        inputProps={{ maxLength: 20 }}
                    />
                    {isValidate && userForm.firstName === '' && (
                        <FormHelperText className="input-error">
                            <FormattedMessage id="Common.FirstName.Error" defaultMessage="Please enter first name" />
                        </FormHelperText>
                    )}
                </Grid>
                {/** Last Name */}
                <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                    <Typography className="login-sigup-input-text">
                        <FormattedMessage id="SignInSignUp.LastName" defaultMessage="Last Name*" />
                    </Typography>
                    <TextField
                        variant="outlined"
                        value={userForm.lastName}
                        onChange={onChangeHandler('lastName')}
                        fullWidth
                        className="inputBox"
                        error={isValidate && userForm.lastName === ''}
                        inputProps={{ maxLength: 19 }}
                    />
                    {isValidate && userForm.lastName === '' && (
                        <FormHelperText className="input-error">
                            <FormattedMessage id="Common.LastName.Error" defaultMessage="Please enter last name" />
                        </FormHelperText>
                    )}
                </Grid>
                {/** Phone Number */}
                <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                    <Typography className="login-sigup-input-text">
                        <FormattedMessage id="SignInSignUp.ContactNumber" defaultMessage="Contact Number*" />
                    </Typography>

                    <PhoneNumber
                        parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                        carrier_code={userForm.countryCode}
                        phoneNo={userForm.phoneNumber}
                    />

                    {isValidate && userForm.phoneNumber === '' && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="Common.PhoneNumber.Empty"
                                defaultMessage="Please enter contact number"
                            />
                        </FormHelperText>
                    )}

                    {isValidate && userForm.phoneNumber !== '' && !isCorrectPhone && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="Common.PhoneNumber.InValid"
                                defaultMessage="Please enter contact number"
                            />
                        </FormHelperText>
                    )}
                </Grid>
                {/** Email */}
                <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r-t">
                    <Typography className="login-sigup-input-text">
                        <FormattedMessage id="SignInSingUp.Email" defaultMessage="Email*" />
                    </Typography>
                    <TextField
                        variant="outlined"
                        value={userForm.email}
                        onChange={onChangeHandler('email')}
                        fullWidth
                        className="inputBox"
                        error={isValidate && !isEmail(userForm.email)}
                    />
                    {isValidate && !isEmail(userForm.email) && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="Common.Email.Error"
                                defaultMessage="Please Enter Valid Email Address"
                            />
                        </FormHelperText>
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
}
