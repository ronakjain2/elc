import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import StepConnector from '@material-ui/core/StepConnector';
import { FormattedMessage } from 'react-intl';
import clsx from 'clsx';
import { Helmet } from 'react-helmet';
import Check from '@material-ui/icons/Check';
import { useSelector } from 'react-redux';
import { checkoutEvent } from 'services/GTMservice';

function getSteps() {
    return [
        <FormattedMessage id="Checkout.Login" defaultMessage="Login" />,
        <FormattedMessage id="Checkout.Delivery" defaultMessage="Delivery" />,
        <FormattedMessage id="Checkout.Payment" defaultMessage="Payment" />,
    ];
}

const ColorlibConnector = withStyles({
    alternativeLabel: {
        top: 22,
    },
    active: {
        '& $line': {},
    },
    completed: {
        '& $line': {},
    },
    line: {
        height: 1,
        border: 0,
        backgroundColor: '#cacaca',
        borderRadius: 1,
    },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
    root: {
        backgroundColor: '#ffffff',
        zIndex: 1,
        color: '#fff',
        width: 50,
        height: 50,
        display: 'flex',
        borderRadius: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        border: '1px solid #cacaca',
        '@media(max-width: 668px)': {
            width: 40,
            height: 40,
        },
    },
    active: {
        borderColor: '#0c943f',
    },
    completed: {
        background: '#0c943f',
        border: '0px solid #cacaca',
    },
});

function ColorlibStepIcon(props) {
    const classes = useColorlibStepIconStyles();
    const { active, completed } = props;

    const icons = {
        1: (
            <>
                {!completed && (
                    <img src="/images/checkout/Icon_Checkout_ProgressBar_Login_Selected.svg" alt="checkoutLogin" />
                )}
                {completed && <Check />}
            </>
        ),
        2: (
            <>
                {!completed && (
                    <img src="/images/checkout/Icon_Checkout_ProgressBar_Delivery_Grey.svg" alt="checkoutLogin" />
                )}
                {completed && <Check />}
            </>
        ),
        3: (
            <>
                {!completed && (
                    <img src="/images/checkout/Icon_Checkout_ProgressBar_Payment_Grey.svg" alt="checkoutLogin" />
                )}
                {completed && <Check />}
            </>
        ),
    };

    return (
        <div
            className={clsx(classes.root, {
                [classes.active]: active,
                [classes.completed]: completed,
            })}
        >
            {icons[String(props.icon)]}
        </div>
    );
}

export default function CheckoutStepper({ activeStep }) {
    const state = useSelector((state) => state);

    const steps = getSteps();

    const cartData = state && state.basket;

    useEffect(() => {
        if (cartData && cartData.cartDetails && cartData.cartDetails.products) {
            checkoutEvent(cartData.cartDetails, activeStep + 1);
        }
    }, [cartData, activeStep]);

    return (
        <Grid container justify="center">
            <Helmet>
                <style>{'body { background-color: #f5f5f5; }'}</style>
            </Helmet>
            <Typography className="checkout-title">
                <FormattedMessage id="Cart.Checkout" defaultMessage="Checkout" />
            </Typography>
            <Grid item xs={12} className="checkout-stepper">
                <Stepper alternativeLabel activeStep={activeStep} connector={<ColorlibConnector />}>
                    {steps.map((label, index) => (
                        <Step key={`checkout-stepper-${index}`}>
                            <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper>
            </Grid>
        </Grid>
    );
}
