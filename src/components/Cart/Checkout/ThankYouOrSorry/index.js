import { Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Spinner from 'commonComponet/Spinner';
import { actions as globalActions } from 'components/Global/redux/actions';
import React, { lazy, Suspense, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import { purchaseEvent } from 'services/GTMservice';
import { decryptStatus, getOrderId } from '../utils';
import { actions as checkoutThankYouAction } from './redux/actions';
import './thankyou.css';

const OrderNumberAndMessage = lazy(() => import('./Component/OrderNumberAndMessage'));
const OrderDetails = lazy(() => import('./Component/OrderDetails'));
const SummaryOrderItems = lazy(() => import('./Component/OrderItems'));
const SingleShipmentOrderNumberAndMessage = lazy(() => import('./Component/SingleShipmentOrderNumberAndMessage'));
const SingleShipmentOrderDetails = lazy(() => import('./Component/SingleShipmentOrderDetails'));
const SingleShipmentSummaryOrderItems = lazy(() => import('./Component/SingleShipmentOrderItems'));

const { detect } = require('detect-browser');
const browser = detect();

function CheckoutThankYouOrSorry({ history }) {
    const query = new URLSearchParams(window.location.search);
    const order_id = getOrderId(query.get('order_id'));
    let status = null;
    if (query.get('status')) {
        status = decryptStatus(order_id, query.get('status'));
    }
    const type = query.get('type');
    //const order_id = query.get('order_id');

    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const loader = state && state.checkoutThankYou && state.checkoutThankYou.loader;
    const store_id = state && state.auth && state.auth.currentStore;
    const orderSummary = state && state.checkoutThankYou && state.checkoutThankYou.summaryData;
    const store_locale = state && state.auth && state.auth.store_locale;
    const recentOrderId = state && state.auth && state.auth.recentOrderId;

    useEffect(() => {
        if (status === null && type === null) {
            dispatch(
                checkoutThankYouAction.orderSubmitRequest({
                    order_id,
                    store_id,
                    platform_type: 'web',
                    device_details: browser,
                    status: 'success',
                }),
            );
        } else {
            dispatch(
                checkoutThankYouAction.orderSubmitRequest({
                    order_id,
                    store_id,
                    platform_type: 'web',
                    device_details: browser,
                    status: status === 'true' ? 'success' : 'failure',
                }),
            );
        }
    }, [dispatch, order_id, store_id, status, type]);

    useEffect(() => {
        if (
            orderSummary &&
            orderSummary.product_details &&
            orderSummary.order_summary &&
            orderSummary.order_number !== recentOrderId
        ) {
            let product_data = [];
            let items = [];
            Object.values(orderSummary.product_details).forEach(order => {
                items = [...items, ...order.sub_order_items];
            });
            for (let i = 0; i < items.length; i++) {
                product_data.push({
                    name: items[i].name
                        ? items[i].product_prefix !== ''
                            ? items[i].product_prefix + ' ' + items[i].name
                            : items[i].name
                        : 'Not set',
                    id: items[i].sku,
                    price:
                        parseInt(items[i].special_price) &&
                        parseInt(items[i].special_price) !== parseInt(items[i].price)
                            ? parseFloat(items[i].special_price)
                            : parseInt(items[i].price),
                    brand: 'Google',
                    category: items[i].ga_cat_names,
                    currency:
                        orderSummary.order_summary &&
                        orderSummary.order_summary.currency &&
                        orderSummary.order_summary.currency,
                    quantity: parseInt(items[i].qty_orderded),
                });
            }
            purchaseEvent(product_data, orderSummary.order_summary, orderSummary.order_number);
            dispatch(globalActions.setRecentOrderId(orderSummary.order_number));
        }
    }, [dispatch, orderSummary, recentOrderId]);

    const goToHomePage = () => {
        return history.push(`/${store_locale}`);
    };

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container className="checkout-thankyou" justify="center">
                {/* <Helmet>
                    <style>{'body { background-color: #f5f5f5; }'}</style>
                </Helmet> */}

                {!loader && (
                    <>
                        {orderSummary &&
                            orderSummary.product_details &&
                            Object.keys(orderSummary.product_details).length > 1 && (
                                <>
                                    <Grid item xs={11} md={11}>
                                        <OrderNumberAndMessage orderSummary={orderSummary} />
                                    </Grid>
                                    <Grid item xs={11} md={11}>
                                        <Grid container spacing={2} justify="space-between">
                                            <Grid item xs={12} md={9}>
                                                <SummaryOrderItems orderSummary={orderSummary} />
                                            </Grid>
                                            <Grid item xs={12} md={3}>
                                                <OrderDetails orderSummary={orderSummary} />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </>
                            )}
                        <Grid item xs={11} md={9} lg={8} className="main-padding-top">
                            {orderSummary &&
                                orderSummary.product_details &&
                                Object.keys(orderSummary.product_details).length === 1 && (
                                    <>
                                        <SingleShipmentOrderNumberAndMessage orderSummary={orderSummary} />
                                        <SingleShipmentOrderDetails orderSummary={orderSummary} />
                                        <SingleShipmentSummaryOrderItems orderSummary={orderSummary} />
                                    </>
                                )}

                            <Grid container justify="center">
                                <Button className="ContinueShopping" onClick={() => goToHomePage()}>
                                    <FormattedMessage id="Cart.ContinueShopping" defaultMessage="Continue Shopping" />
                                </Button>
                            </Grid>
                        </Grid>
                    </>
                    // <Grid item xs={11} md={9} lg={8} className="main-padding-top">
                    //     <OrderNumberAndMessage orderSummary={orderSummary} />
                    //     {/* <OrderDetails orderSummary={orderSummary} /> */}
                    //     <SummaryOrderItems orderSummary={orderSummary} />

                    //     <Grid container justify="center">
                    //         <Button className="ContinueShopping" onClick={() => goToHomePage()}>
                    //             <FormattedMessage id="Cart.ContinueShopping" defaultMessage="Continue Shopping" />
                    //         </Button>
                    //     </Grid>
                    // </Grid>
                )}
                {loader && (
                    <Grid container alignItems="center">
                        <Spinner />
                    </Grid>
                )}
            </Grid>
        </Suspense>
    );
}

export default withRouter(CheckoutThankYouOrSorry);
