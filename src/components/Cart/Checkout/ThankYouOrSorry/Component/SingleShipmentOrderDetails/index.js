import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { replaceTilCharacterToComma } from 'components/Cart/Checkout/Delivery/utils';

export default function OrderDetails({ orderSummary }) {
    return (
        <Grid container className="thank-you-page-card single-shipment-box-shadow padding">
            {/** Payment */}
            <Grid item xs={12} md={4}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography className="order-details-title">
                            <FormattedMessage id="Checkout.ThankYou.PaymentType" defaultMessage="Payment Type" />
                        </Typography>
                    </Grid>

                    <Grid item xs={12}>
                        {orderSummary && orderSummary.payment_type ? (
                            <Typography className="order-detaile-subTitle">
                                <FormattedMessage
                                    id={`OrderSummary.${orderSummary.payment_type
                                        .replace(/ /g, '')
                                        .toLowerCase()}.text`}
                                    defaultMessage={orderSummary.payment_type}
                                />
                            </Typography>
                        ) : null}
                    </Grid>
                </Grid>
            </Grid>
            {/** Delivery Location */}
            <Grid item xs={12} md={4} className="order-detail-mobile-p-t">
                <Grid container>
                    <Grid item xs={12}>
                        {orderSummary && orderSummary.shipping_method_type === 'home_delivery' && (
                            <Typography className="order-details-title">
                                <FormattedMessage
                                    id="Checkout.Payment.DeliveryLocation"
                                    defaultMessage="Delivery Location"
                                />
                            </Typography>
                        )}
                        {orderSummary && orderSummary.shipping_method_type === 'click_and_collect' && (
                            <Typography className="order-details-title">
                                <FormattedMessage id="Checkout.Payment.PickupAddress" defaultMessage="Pickup Address" />
                            </Typography>
                        )}
                    </Grid>

                    <Grid item xs={12}>
                        <Typography className="order-detaile-subTitle">
                            {orderSummary && orderSummary.address && orderSummary.address.deliver_to}
                        </Typography>
                        <Typography className="order-details-address-type">
                            {orderSummary && orderSummary.address && orderSummary.address.type}
                        </Typography>
                    </Grid>

                    <Grid item xs={12}>
                        <Typography className="order-details-address-value">
                            {orderSummary &&
                                orderSummary.address &&
                                orderSummary.address.street &&
                                replaceTilCharacterToComma(orderSummary.address.street)}
                        </Typography>
                        <Typography className="order-details-address-value">
                            {orderSummary && orderSummary.address && orderSummary.address.country}
                        </Typography>
                        <Typography className="order-details-address-value">
                            {orderSummary && orderSummary.address && orderSummary.address.city}
                        </Typography>
                        <Typography className="order-details-address-value dir-ltr">
                            {orderSummary &&
                                orderSummary.address &&
                                orderSummary.address.phone_number &&
                                `+${orderSummary.address.carrier_code} ${orderSummary.address.phone_number}`}
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
            {/** Delivery Option */}
            <Grid item xs={12} md={4} className="order-detail-mobile-p-t">
                <Grid container>
                    <Grid item xs={12}>
                        {orderSummary && orderSummary.shipping_method_type === 'home_delivery' && (
                            <Typography className="order-details-title">
                                <FormattedMessage
                                    id="Checkout.Payment.DeliveryOption"
                                    defaultMessage="Delivery Option"
                                />
                            </Typography>
                        )}
                        {orderSummary && orderSummary.shipping_method_type === 'click_and_collect' && (
                            <Typography className="order-details-title">
                                <FormattedMessage id="Checkout.Payment.PickupOptions" defaultMessage="Pickup Options" />
                            </Typography>
                        )}
                    </Grid>

                    <Grid item xs={12}>
                        {orderSummary && orderSummary.delivery_type ? (
                            <Typography className="order-detaile-subTitle">
                                <FormattedMessage
                                    id={`OrderSummary.${orderSummary.delivery_type
                                        .replace(/ /g, '')
                                        .toLowerCase()}.text`}
                                    defaultMessage={orderSummary.delivery_type}
                                />
                            </Typography>
                        ) : null}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
