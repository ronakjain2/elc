import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { isOrderPending, isOrderSuccess } from 'components/Cart/Checkout/utils';
import parse from 'html-react-parser';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

export default function OrderNumberAndMessage({ orderSummary }) {
    const state = useSelector(state => state);
    let history = useHistory();

    const store_locale = state && state.auth && state.auth.store_locale;
    const loginUser = state && state.auth && state.auth.loginUser;
    const customer_type = loginUser && loginUser.customer_type;

    const gotoFCPage = (disableClick = true) => {
        if (disableClick) return;
        if (customer_type === 'ecom_club_user') {
            history.push(`/${store_locale}/my-club`);
            return;
        }
        history.push(`/${store_locale}/family-club`);
    };
    return (
        <Grid container>
            <Grid item xs={12} className="thank-you-page-card single-shipment-box-shadow">
                <Grid container justify="center">
                    {isOrderSuccess((orderSummary && orderSummary.status) || '') && (
                        <img src="/images/checkout/thankyou.svg" alt="thankyouimage" className="thankyou-image" />
                    )}
                    {isOrderPending((orderSummary && orderSummary.status) || '') && (
                        <img
                            src="/images/checkout/paymentPending.png"
                            alt="thankyouimage"
                            className="thankyou-image1"
                        />
                    )}
                    {!isOrderSuccess((orderSummary && orderSummary.status) || '') &&
                        !isOrderPending((orderSummary && orderSummary.status) || '') && (
                            <img src="/images/checkout/sorry.jpg" alt="thankyouimage" className="thankyou-image" />
                        )}
                </Grid>

                <Grid container justify="center">
                    <Typography
                        className={
                            isOrderSuccess((orderSummary && orderSummary.status) || '')
                                ? 'order-title green'
                                : isOrderPending((orderSummary && orderSummary.status) || '')
                                ? 'order-title checkout-yellow'
                                : 'order-title red'
                        }
                    >
                        {orderSummary && orderSummary.status}
                    </Typography>
                </Grid>

                <Grid container justify="center">
                    <Typography className="order-message">
                        {orderSummary &&
                            orderSummary.order_status_details &&
                            orderSummary.order_status_details.order_status_message &&
                            parse(orderSummary.order_status_details.order_status_message)}
                    </Typography>
                </Grid>

                <Grid container justify="center">
                    <Grid item xs={11} md={5} className="orderNoBox">
                        <FormattedMessage id="Checkout.ThankYou.OrderNumber" defaultMessage="Order Number" />
                        .&nbsp;
                        <span className="order-number">{orderSummary && orderSummary.order_number}</span>
                    </Grid>
                </Grid>
                {orderSummary && (orderSummary.banner_image || orderSummary.mob_banner_image) && (
                    <Grid className="fcBanner" onClick={gotoFCPage}>
                        {orderSummary.banner_image && (
                            <img alt="fc banner" className="d-none d-md-block w-100" src={orderSummary.banner_image} />
                        )}
                        {orderSummary.mob_banner_image && (
                            <img
                                alt="fc banner"
                                className="d-block d-md-none w-100"
                                src={orderSummary.mob_banner_image}
                            />
                        )}
                    </Grid>
                )}
            </Grid>
        </Grid>
    );
}
