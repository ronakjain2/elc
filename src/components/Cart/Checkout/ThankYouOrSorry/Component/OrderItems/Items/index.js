import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import FCBenfitTag from 'commonComponet/FCBenefitTag';
import {
    _getOrderSummarySubtotalMob,
    _getOrderSummaryPrice,
    _getOrderSummarySubtotal,
} from 'components/Cart/Basket/utils';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import SummaryItemTableTitle from '../TableTitle';

export default function SummaryOrderItemList({ orderSummary }) {
    const state = useSelector((state) => state);

    const loginUser = state && state.auth && state.auth.loginUser;
    const customer_type = loginUser && loginUser.customer_type;

    const items = (orderSummary && orderSummary.product_details) || {};

    return (
        <Grid container>
            <Hidden only={['xs', 'sm']}>
                {items &&
                    Object.keys(items) &&
                    Object.keys(items).map((key, index) => (
                        <Grid
                            item
                            xs={12}
                            key={`summary_order_item_${index}`}
                            className="summary-card"
                            // style={{ padding: '1rem 1.5rem' }}
                        >
                            <Grid container justify="space-between" className="summary-order-number-and-delivery">
                                <div className={'padding-20'}>
                                    <Typography className="summary-shippment-title">{`Shippment ${index + 1} of ${
                                        Object.keys(items).length
                                    }`}</Typography>
                                    <Typography>
                                        <FormattedMessage
                                            id="Checkout.ThankYou.OrderNumber"
                                            defaultMessage="Order Number"
                                        />
                                        : {items[key].sub_order_id}
                                    </Typography>
                                </div>
                                <div className={'padding-20'}>
                                    <Typography className="summary-shippment-title">
                                        <FormattedMessage
                                            id="Checkout.Payment.DeliveryOption"
                                            defaultMessage="Delivery Option"
                                        />
                                    </Typography>
                                    <Typography> :{items[key].delivery_option}</Typography>
                                </div>
                            </Grid>
                            <Hidden only={['xs', 'sm']}>
                                <SummaryItemTableTitle />
                            </Hidden>
                            {items[key] &&
                                items[key].sub_order_items &&
                                items[key].sub_order_items.map((item, i) => (
                                    <Grid
                                        container
                                        spacing={3}
                                        className={'padding-20'}
                                        alignItems="center"
                                        key={`sub_item_${index}_${i}`}
                                    >
                                        <Grid item xs={12}>
                                            <div className="item-order-details-line"></div>
                                        </Grid>
                                        <Grid item xs={5}>
                                            <Grid container alignItems="center">
                                                <Grid item xs={4}>
                                                    <img
                                                        src={item && item.image && item.image[0]}
                                                        alt="itemImage"
                                                        className="order-item-image"
                                                    />
                                                </Grid>
                                                <Grid item xs={8}>
                                                    <Typography className="order-item-name">
                                                        {item.name && `${item.name}`}
                                                    </Typography>
                                                    <Typography className="order-details-product-specification">
                                                        <FormattedMessage
                                                            id="PDP.Specifications.ProductCode"
                                                            defaultMessage="Product Code"
                                                        />
                                                        : &nbsp;{item && item.sku}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>

                                        <Grid item xs={3} className="textAlignCenter">
                                            <Typography component={'span'}>{_getOrderSummaryPrice(item)}</Typography>
                                        </Grid>

                                        <Grid item xs={1} className="textAlignCenter">
                                            <Typography className="order-item-qty">
                                                {item && item.qty_orderded && parseInt(item.qty_orderded)}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={3} className="textAlignEnd">
                                            {_getOrderSummarySubtotal(item)}
                                            <FCBenfitTag
                                                show={
                                                    parseInt(item.family_club) === 1 &&
                                                    customer_type === 'ecom_club_user'
                                                }
                                                className="justify-content-end"
                                            />
                                        </Grid>
                                    </Grid>
                                ))}
                        </Grid>
                    ))}
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                {items &&
                    Object.keys(items) &&
                    Object.keys(items).length > 0 &&
                    Object.keys(items).map((key, index) => (
                        <Grid
                            container
                            spacing={3}
                            key={`mobile_order_item_${index}`}
                            className="margin-top summary-card"
                        >
                            <Grid container justify="space-between" className="summary-order-number-and-delivery">
                                <Typography>{`Shippment ${index + 1} of ${Object.keys(items).length}`}</Typography>
                                <Typography>
                                    <FormattedMessage
                                        id="Checkout.ThankYou.OrderNumber"
                                        defaultMessage="Order Number"
                                    />
                                    : {items[key].sub_order_id}
                                </Typography>
                            </Grid>
                            <Typography className="summary-shippment-title">
                                <FormattedMessage
                                    id="Checkout.Payment.DeliveryOption"
                                    defaultMessage="Delivery Option"
                                />{' '}
                                :{items[key].delivery_option}
                            </Typography>
                            {items[key] &&
                                items[key].sub_order_items &&
                                items[key].sub_order_items.map((item, i) => (
                                    <Grid container className="padding-top-botton" key={`sub_item_${index}_${i}_m`}>
                                        <Grid item xs={3}>
                                            <img
                                                src={item && item.image && item.image[0]}
                                                alt="itemImage"
                                                className="order-item-image"
                                            />
                                        </Grid>
                                        <Grid item xs={8}>
                                            <Grid container justify="flex-end">
                                                <Grid item xs={12} style={{ textAlign: `end` }}>
                                                    <Typography className="order-item-name">{item.name}</Typography>
                                                </Grid>
                                                <Grid item xs={12} style={{ textAlign: `end` }}>
                                                    <Typography className="order-item-qty">
                                                        <FormattedMessage
                                                            id="Checkout.CartDetails.Qty"
                                                            defaultMessage="Qty"
                                                        />
                                                        :&nbsp;
                                                        {item && item.qty_orderded && parseInt(item.qty_orderded)}
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={12} className="textAlignEnd">
                                                    <Typography>
                                                        {_getOrderSummarySubtotalMob(item)}
                                                        <FCBenfitTag
                                                            show={
                                                                parseInt(item.family_club) === 1 &&
                                                                customer_type === 'ecom_club_user'
                                                            }
                                                            className="justify-content-end"
                                                        />
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                ))}
                        </Grid>
                    ))}
            </Hidden>
        </Grid>
    );
}
