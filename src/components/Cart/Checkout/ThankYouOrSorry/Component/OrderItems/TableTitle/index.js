import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';

export default function SummaryItemTableTitle() {
    return (
        <Grid container className={'head-padd'}>
            {/* <Grid item xs={12}>
                <div className="border-line" />
            </Grid> */}

            <Grid item xs={12}>
                <Grid container alignItems={'center'}>
                    <Grid item xs={5}>
                        <Typography className="table-text">
                            <FormattedMessage id="Cart.ItemNumberStyle" defaultMessage="Item (style number)" />
                        </Typography>
                    </Grid>
                    <Grid item xs={3} className="textAlignCenter">
                        <Typography className="table-text">
                            <FormattedMessage id="Cart.Price" defaultMessage="Price" />
                        </Typography>
                    </Grid>
                    <Grid item xs={1} className="textAlignCenter">
                        <Typography className="table-text">
                            <FormattedMessage id="Cart.Quantity" defaultMessage="Quantity" />
                        </Typography>
                    </Grid>

                    <Grid item xs={3} className="textAlignEnd">
                        <Typography className="table-text">
                            <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>

            {/* <Grid item xs={12}>
                <div className="border-line padd-t" />
            </Grid> */}
        </Grid>
    );
}
