import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Spinner from 'commonComponet/Spinner';
const SummaryOrderItemList = lazy(() => import('./Items'));

export default function SummaryOrderItems({ orderSummary }) {
    return (
        <Suspense fallback={<Spinner />}>
            <Grid container className="thank-you-page-card padding">
                {/* <Typography className="order-detaile-summary-title">
                    <FormattedMessage id="Cart.OrderSummary" defaultMessage="Order Summary" />
                </Typography> */}

                <Grid item xs={12}>
                    <SummaryOrderItemList orderSummary={orderSummary} />
                </Grid>
            </Grid>
        </Suspense>
    );
}
