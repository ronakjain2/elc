import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import Hidden from '@material-ui/core/Hidden';
import Spinner from 'commonComponet/Spinner';

const SummaryItemTableTitle = lazy(() => import('./TableTitle'));
const SummaryOrderItemList = lazy(() => import('./Items'));
const SummaryOrderCharges = lazy(() => import('./Charges'));

export default function SummaryOrderItems({ orderSummary }) {
    return (
        <Suspense fallback={<Spinner />}>
            <Grid container className="thank-you-page-card single-shipment-box-shadow padding">
                <Typography className="order-detaile-summary-title">
                    <FormattedMessage id="Cart.OrderSummary" defaultMessage="Order Summary" />
                </Typography>

                <Grid item xs={12}>
                    <Hidden only={['xs', 'sm']}>
                        <SummaryItemTableTitle />
                    </Hidden>
                    <SummaryOrderItemList orderSummary={orderSummary} />
                </Grid>

                <Grid item xs={12}>
                    <div className="border-line" />
                </Grid>

                <Grid item xs={12}>
                    <SummaryOrderCharges orderSummary={orderSummary} />
                </Grid>
            </Grid>
        </Suspense>
    );
}
