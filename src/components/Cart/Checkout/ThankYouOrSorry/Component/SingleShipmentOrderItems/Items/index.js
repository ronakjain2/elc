import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import FCBenfitTag from 'commonComponet/FCBenefitTag';
import {
    _getOrderSummaryPrice,
    _getOrderSummarySubtotal,
    _getOrderSummarySubtotalMob,
} from 'components/Cart/Basket/utils';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

export default function SummaryOrderItemList({ orderSummary }) {
    const state = useSelector((state) => state);

    const loginUser = state && state.auth && state.auth.loginUser;
    const customer_type = loginUser && loginUser.customer_type;

    const items = (orderSummary && orderSummary.product_details) || {};

    return (
        <Grid container>
            <Hidden only={['xs', 'sm']}>
                {items &&
                    Object.keys(items).length === 1 &&
                    Object.keys(items).map((key, index) =>
                        items[key].sub_order_items.map((item, index) => (
                            <Grid item xs={12} key={`summary_order_item_${index}`} className="padding-top-botton">
                                <Grid container alignItems="center">
                                    <Grid item xs={5}>
                                        <Grid container alignItems="center">
                                            <Grid item xs={4}>
                                                <img
                                                    src={item && item.image && item.image[0]}
                                                    alt="itemImage"
                                                    className="order-item-image"
                                                />
                                            </Grid>
                                            <Grid item xs={8}>
                                                <Typography className="order-item-name">
                                                    {item.name && `${item.name}`}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>

                                    <Grid item xs={1}></Grid>
                                    <Grid item xs={2}>
                                        <Typography component={'span'}>{_getOrderSummaryPrice(item)}</Typography>
                                    </Grid>
                                    <Grid item xs={1} className="textAlignCenter">
                                        <Typography className="order-item-qty">
                                            {item && item.qty_orderded && parseInt(item.qty_orderded)}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={3} className="textAlignEnd">
                                        {_getOrderSummarySubtotal(item)}
                                        <FCBenfitTag
                                            show={
                                                parseInt(item.family_club) === 1 && customer_type === 'ecom_club_user'
                                            }
                                            className="justify-content-end"
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        )),
                    )}
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                {items &&
                    Object.keys(items).length === 1 &&
                    Object.keys(items).map((key, index) =>
                        items[key].sub_order_items.map((item, index) => (
                            <Grid container key={`mobile_order_item_${index}`} className="padding-top-botton">
                                <Grid item xs={3}>
                                    <img
                                        src={item && item.image && item.image[0]}
                                        alt="itemImage"
                                        className="order-item-image"
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                                <Grid item xs={8} className="textAlignEnd">
                                    <Typography className="order-item-name">{item.name}</Typography>
                                    <Typography className="order-item-qty">
                                        <FormattedMessage id="Checkout.CartDetails.Qty" defaultMessage="Qty" />
                                        :&nbsp;
                                        {item && item.qty_orderded && parseInt(item.qty_orderded)}
                                    </Typography>
                                    <Typography>
                                        {_getOrderSummarySubtotalMob(item, true)}
                                        <FCBenfitTag
                                            show={
                                                parseInt(item.family_club) === 1 && customer_type === 'ecom_club_user'
                                            }
                                            className="justify-content-end"
                                        />
                                    </Typography>
                                </Grid>
                            </Grid>
                        )),
                    )}
            </Hidden>
        </Grid>
    );
}
