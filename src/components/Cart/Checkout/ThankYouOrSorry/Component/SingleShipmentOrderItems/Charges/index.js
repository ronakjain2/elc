import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';

export default function SummaryOrderCharges({ orderSummary }) {
    let charges = (orderSummary && orderSummary.order_summary) || {};

    return (
        <Grid container>
            <Grid item xs={12}>
                {/** subtotal */}
                {charges && charges.subtotal && (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {charges && `${charges.currency} ${charges.subtotal}`}
                            </Typography>
                        </div>
                    </Grid>
                )}
                {/** Discount amount */}
                {charges && charges.savings ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Cart.Saving" defaultMessage="Saving" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {charges && `${charges.currency} ${charges.savings}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}
                {/** Family club saving amount */}
                {charges && charges.fc_discount_amount && parseFloat(charges.fc_discount_amount) !== 0 && (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="FamilyClub-label">
                                <FormattedMessage id="Cart.FamilyClubSaving" defaultMessage="Family Club Saving" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="FamilyClub-value">
                                {charges && `${charges.currency} ${parseFloat(charges.fc_discount_amount).toFixed(2)}`}
                            </Typography>
                        </div>
                    </Grid>
                )}
                {charges && charges.fc_save_more && parseFloat(charges.fc_save_more) !== 0 && (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="FamilyClub-label bold">
                                <FormattedMessage
                                    id="Cart.SaveMoreWithFamilyClub"
                                    defaultMessage="Save More With Family Club"
                                />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="FamilyClub-value bold">
                                {charges && `${charges.currency} ${parseFloat(charges.fc_save_more).toFixed(2)}`}
                            </Typography>
                        </div>
                    </Grid>
                )}
                {/** Shipping */}
                {charges && charges.shipping && charges.shipping !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Checkout.Cart.Charge.Shipping" defaultMessage="Shipping" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {charges && `${charges.currency} ${charges.shipping}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}
                {/** COD */}
                {charges && charges.COD && charges.COD !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Checkout.Cart.Charge.COD" defaultMessage="COD" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {charges && `${charges.currency} ${charges.COD}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}
                {/**Voucher Discount */}
                {charges && charges.voucher_discount && parseFloat(charges.voucher_discount) !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage
                                    id="Checkout.Cart.Charge.VoucherDiscount"
                                    defaultMessage="Voucher Discount"
                                />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {charges && `${charges.currency} ${charges.voucher_discount}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}
                {/**Vat */}
                {charges && charges.vat && charges.vat !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Checkout.Cart.Charge.VATIncluded" defaultMessage="VAT Included" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {charges && `${charges.currency} ${charges.vat}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}
                {/** Line */}
                <Grid item xs={12}>
                    <div className="border-line padd-t" />
                </Grid>
                {/** grand_total */}
                <Grid item xs={12}>
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-total-label">
                                <FormattedMessage id="Cart.Total" defaultMessage="Total" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-total-value">
                                {charges && `${charges.currency} ${charges && charges.total}`}
                            </Typography>
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
