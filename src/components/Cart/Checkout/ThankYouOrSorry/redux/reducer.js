import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.ORDER_SUBMIT_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.ORDER_SUBMIT_REQUEST_FALIED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.GET_ORDER_SUMMARY_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_ORDER_SUMMARY_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
        summaryData: {},
    }),
    [types.GET_ORDER_SUMMARY_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        summaryData: payload,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    summaryData: {},
});
