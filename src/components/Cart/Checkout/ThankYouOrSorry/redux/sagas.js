import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import { actions as globalActions } from 'components/Global/redux/actions';
import { toast } from 'react-toastify';
import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../api';
import { actions, types } from './actions';

const orderSubmitReq = function* orderSubmitReq({ payload }) {
    try {
        // remove
        localStorage.removeItem('merchant_reference');
        const { order_id, status } = payload;
        yield call(api.orderSubmit, { order_id, status });
        yield put(actions.getOrderSummaryRequest(payload));
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.orderSubmitRequestFailed());
    }
};

const getOrderSummaryReq = function* getOrderSummaryReq({ payload }) {
    try {
        const { order_id, store_id, platform_type, device_details } = payload;
        const { data } = yield call(api.getOrderSummary, { order_id, store_id, platform_type, device_details });
        if (data && data.status) {
            yield put(actions.getOrderSummaryRequestSuccess(data.order_data));
            yield put(basketActions.saveCartCountRequest(0));
            if (data.customer_type) {
                yield put(
                    globalActions.updateCustomerType({
                        customer_type: data && data.customer_type,
                        fc_status_message: data && data.fc_status_message,
                    }),
                );
            }
        } else {
            toast.error(data && data.message);
            yield put(actions.getOrderSummaryRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getOrderSummaryRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.ORDER_SUBMIT_REQUEST, orderSubmitReq);
    yield takeLatest(types.GET_ORDER_SUMMARY_REQUEST, getOrderSummaryReq);
}
