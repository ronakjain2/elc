import { createAction } from 'redux-actions';

//Actions
const ORDER_SUBMIT_REQUEST = 'ELC@OROB/ORDER_SUBMIT_REQUEST';
const ORDER_SUBMIT_REQUEST_SUCCESS = 'ELC@OROB/ORDER_SUBMIT_REQUEST_SUCCESS';
const ORDER_SUBMIT_REQUEST_FALIED = 'ELC@OROB/ORDER_SUBMIT_REQUEST_FALIED';

const GET_ORDER_SUMMARY_REQUEST = 'ELC@OROB/GET_ORDER_SUMMARY_REQUEST';
const GET_ORDER_SUMMARY_REQUEST_SUCCESS = 'ELC@OROB/GET_ORDER_SUMMARY_REQUEST_SUCCESS';
const GET_ORDER_SUMMARY_REQUEST_FAILED = 'ELC@OROB/GET_ORDER_SUMMARY_REQUEST_FAILED';

//Action methods
const orderSubmitRequest = createAction(ORDER_SUBMIT_REQUEST);
const orderSubmitRequestSuccess = createAction(ORDER_SUBMIT_REQUEST_SUCCESS);
const orderSubmitRequestFailed = createAction(ORDER_SUBMIT_REQUEST_FALIED);

const getOrderSummaryRequest = createAction(GET_ORDER_SUMMARY_REQUEST);
const getOrderSummaryRequestSuccess = createAction(GET_ORDER_SUMMARY_REQUEST_SUCCESS);
const getOrderSummaryRequestFailed = createAction(GET_ORDER_SUMMARY_REQUEST_FAILED);

//actions
export const actions = {
    orderSubmitRequest,
    orderSubmitRequestSuccess,
    orderSubmitRequestFailed,

    getOrderSummaryRequest,
    getOrderSummaryRequestSuccess,
    getOrderSummaryRequestFailed,
};

//types
export const types = {
    ORDER_SUBMIT_REQUEST,
    ORDER_SUBMIT_REQUEST_SUCCESS,
    ORDER_SUBMIT_REQUEST_FALIED,

    GET_ORDER_SUMMARY_REQUEST,
    GET_ORDER_SUMMARY_REQUEST_SUCCESS,
    GET_ORDER_SUMMARY_REQUEST_FAILED,
};
