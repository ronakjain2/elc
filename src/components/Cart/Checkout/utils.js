import { CRYPTR_KEY } from '../../../environments';

const Cryptr = require('cryptr');
const cryptr = new Cryptr(CRYPTR_KEY);

export const getOrderId = (orderId) => {
    if (orderId) {
        try {
            return cryptr.decrypt(orderId);
        } catch (err) {
            console.log(err);
        }
    }
    return '';
};

export const createEncryptOrderId = (orderId) => {
    if (orderId) {
        try {
            return cryptr.encrypt(orderId);
        } catch (err) {
            console.log(err);
        }
    }
    return '';
};

export const decryptStatus = (orderId, status) => {
    if (orderId) {
        try {
            const cryptr = new Cryptr(orderId);
            return cryptr.decrypt(status);
        } catch (err) {
            console.log(err);
        }
    }
    return '';
};

export const isOrderSuccess = (status) => {
    const confirmText = 'تم تأكيد';
    if (status) {
        if (status.toLowerCase().replace(/ /g, '') === 'confirmed' || status.localeCompare(confirmText) === 0) {
            return true;
        }
    }

    return false;
};

export const isOrderPending = (status) => {
    const paymentPending = 'في انتظار الدفع‎';
    if (status) {
        if (status.toLowerCase().replace(/ /g, '') === 'pendingpayment' || status.localeCompare(paymentPending) === 0) {
            return true;
        }
    }

    return false;
};

export const isValidateSpecialCharacters = (string) => {
    if (
        string.indexOf(`"`) !== -1 ||
        string.indexOf('&') !== -1 ||
        string.indexOf('>') !== -1 ||
        string.indexOf('<') !== -1
    ) {
        return true;
    }

    return false;
};
