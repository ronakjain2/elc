import api from 'apiServices';

//Checkout Login
const checkoutLogin = (payload) => api.post(`/index.php/rest/V1/app/orob/login`, payload);
const checkoutRegister = (payload) => api.post(`/index.php/rest/V1/app/orob/register`, payload);

//Checkout Delivery
const getCheckoutDelivery = (payload) => api.post(`/index.php/rest/V1/app/getdelivery`, payload);
const getCheckoutDeliveryOption = (payload) => api.post(`/index.php/rest/V1/app/mkt/gethomedeliveryoption`, payload);
const setCheckoutDelivery = (payload) => api.post(`/index.php/rest/V1/app/orob/setdelivery`, payload);
const getStoreCountryList = (payload) =>
    api.get(
        `index.php/rest/V1/app/orob/storelocator_v1?country_id=${payload.country}&city=${payload.city}&quote_id=${payload.quote_id}&store_id=${payload.storeId}`,
    );
const getCountryList = () => api.get(`/index.php/rest/V1/directory/countries`);
const checkDeliveryOptions = (payload) => api.post(`/index.php/rest/V1/app/mkt/checkavldeliveryoption`, payload);
const checkClickAndCollectOptions = (payload) =>
    api.post(`/index.php/rest/V1/app/orob/getclickcollectdeliveryoption`, payload);

//checkout Payemnt
const getCheckoutPayment = (payload) => api.post(`/index.php/rest/V1/app/mkt/getpayment`, payload);
const setCheckoutPayment = (payload) => api.post(`/index.php/rest/V1/app/orob/setpayment`, payload);
const placeOrder = (payload) => api.post(`/index.php/rest/V1/app/mkt/placeorder`, payload);
const getApplyVoucher = (payload) => api.post(`/index.php/rest/V1/app/checkvoucher`, payload);
const getRemoveVoucher = (payload) => api.post(`/index.php/rest/V1/app/removevoucher`, payload);

//Thank you
const orderSubmit = (payload) => api.post(`/index.php/rest/V1/app/mkt/OrderSubmit`, payload);
const getOrderSummary = (payload) => api.post(`/index.php/rest/V1/app/mkt/OrderSummary`, payload);
const applePayPlaceOrder = (payload) => api.post(`/index.php/rest/V1/app/mkt/applepayorder`, payload);

//Payfort
const deleteSavedCard = (payload) => api.post(`/index.php/rest/V1/app/deleteCardById`, payload);
const OrderPayment = (payload) => api.post(`/index.php/rest/V1/app/OrderPayment`, payload);
const payfortRestoreQuote = (payload) => api.post(`/index.php/rest/V1/app/orob/payfortrestorequote`, payload);

export default {
    checkoutLogin,
    checkoutRegister,

    getCheckoutDelivery,
    getCheckoutDeliveryOption,
    setCheckoutDelivery,
    getCountryList,
    checkDeliveryOptions,
    getStoreCountryList,
    checkClickAndCollectOptions,

    getCheckoutPayment,
    setCheckoutPayment,
    placeOrder,
    getApplyVoucher,
    getRemoveVoucher,

    orderSubmit,
    getOrderSummary,
    applePayPlaceOrder,

    deleteSavedCard,
    OrderPayment,
    payfortRestoreQuote,
};
