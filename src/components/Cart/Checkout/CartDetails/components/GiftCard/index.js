import React from 'react';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { FormattedMessage } from 'react-intl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

export default function GiftCard({ gift_wrap_flag, onChangeHandler, address, isValidate, onChangeGiftWrapHandler }) {
    return (
        <Grid container>
            <FormControl>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={address.gift_wrap_flag === 1}
                                onChange={onChangeGiftWrapHandler('gift_wrap_flag')}
                            />
                        }
                        label={<FormattedMessage id="Checkout.Delivery.GiftWrap" defaultMessage="Gift wrap required" />}
                    />
                </FormGroup>
            </FormControl>

            {address.gift_wrap_flag === 1 && (
                <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        value={address.message}
                        onChange={onChangeHandler('message')}
                        fullWidth
                        multiline
                        rows={4}
                        inputProps={{
                            maxLength: 100,
                        }}
                        error={isValidate && address.message === ''}
                    />
                    {isValidate && address.message === '' && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="Checkout.Delivery.GiftWrap.Message"
                                defaultMessage="Please enter message"
                            />
                        </FormHelperText>
                    )}
                </Grid>
            )}
        </Grid>
    );
}
