import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useSelector } from 'react-redux';
import { replaceTilCharacterToComma } from 'components/Cart/Checkout/Delivery/utils';

export default function CartDeliveryLocation() {
    const state = useSelector((state) => state);

    const shippingAddressData = state && state.checkoutPayment && state.checkoutPayment.shippingAddressData;

    return (
        <Grid container>
            <Grid item xs={12} className="arabic-right">
                <Typography className="cart-address-nick-name"></Typography>
                <Typography className="cart-address-type"></Typography>
            </Grid>
            <Grid item xs={12}>
                <Grid container>
                    <Grid item xs={12} className="arabic-right">
                        <Typography className="cart-address-text">
                            {shippingAddressData &&
                                shippingAddressData.street &&
                                `${replaceTilCharacterToComma(shippingAddressData.street)},`}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} className="arabic-right">
                        <Typography className="cart-address-text">
                            {shippingAddressData &&
                                shippingAddressData.country_name &&
                                `${shippingAddressData.country_name},`}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} className="arabic-right">
                        <Typography className="cart-address-text">
                            {shippingAddressData && shippingAddressData.city && `${shippingAddressData.city},`}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} className="arabic-right">
                        <Typography className="cart-address-text dir-ltr">
                            {shippingAddressData &&
                                shippingAddressData.telephone &&
                                `+${shippingAddressData.carrier_code} ${shippingAddressData.telephone}`}
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
