import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

export default function CheckoutChargesList({ isPayment, isCod }) {
    const state = useSelector((state) => state);

    const cartData = state && state.basket && state.basket.cartDetails;
    const paymentCartData = state && state.checkoutPayment && state.checkoutPayment.cartData;
    const codChanges =
        state &&
        state.checkoutPayment &&
        state.checkoutPayment.payments &&
        state.checkoutPayment.payments.cashondelivery;

    return (
        <Grid container>
            <Grid item xs={12}>
                {/** subtotal */}
                {!isPayment && cartData && cartData.subtotal && (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {cartData && `${cartData.currency} ${cartData.subtotal}`}
                            </Typography>
                        </div>
                    </Grid>
                )}
                {isPayment && paymentCartData && paymentCartData.subtotal && (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {paymentCartData && `${paymentCartData.currency} ${paymentCartData.subtotal}`}
                            </Typography>
                        </div>
                    </Grid>
                )}
                {/** Discount amount */}
                {!isPayment && cartData && cartData.discount_amount ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Cart.Saving" defaultMessage="Saving" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {cartData && `${cartData.currency} ${cartData.discount_amount}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}

                {isPayment && paymentCartData && paymentCartData.discount_amount ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Cart.Saving" defaultMessage="Saving" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {paymentCartData && `${paymentCartData.currency} ${paymentCartData.discount_amount}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}

                {!isPayment &&
                    cartData &&
                    cartData.fc_discount_amount &&
                    parseFloat(cartData.fc_discount_amount) !== 0 && (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartFamilyClub-label">
                                    <FormattedMessage id="Cart.FamilyClubSaving" defaultMessage="Family Club Saving" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartFamilyClub-value">
                                    {cartData &&
                                        `${cartData.currency} ${parseFloat(cartData.fc_discount_amount).toFixed(2)}`}
                                </Typography>
                            </div>
                        </Grid>
                    )}
                {isPayment && paymentCartData && paymentCartData.fc_discount_amount && parseFloat(paymentCartData.fc_discount_amount) !== 0 && (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartFamilyClub-label">
                                <FormattedMessage id="Cart.FamilyClubSaving" defaultMessage="Family Club Saving" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartFamilyClub-value">
                                {cartData &&
                                    `${paymentCartData.currency} ${parseFloat(paymentCartData.fc_discount_amount).toFixed(2)}`}
                            </Typography>
                        </div>
                    </Grid>
                )}
                {cartData && cartData.fc_save_more && parseFloat(cartData.fc_save_more) !== 0 && (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartFamilyClub-label bold">
                                <FormattedMessage
                                    id="Cart.SaveMoreWithFamilyClub"
                                    defaultMessage="Save More With Family Club"
                                />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartFamilyClub-value bold">
                                {cartData && `${cartData.currency} ${parseFloat(cartData.fc_save_more).toFixed(2)}`}
                            </Typography>
                        </div>
                    </Grid>
                )}

                {/** Shipping */}
                {paymentCartData && paymentCartData.shipping_amount && paymentCartData.shipping_amount !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Checkout.Cart.Charge.Shipping" defaultMessage="Shipping" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {paymentCartData && `${paymentCartData.currency} ${paymentCartData.shipping_amount}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}
                {/** COD */}
                {!isCod && paymentCartData && paymentCartData.cod_charges && paymentCartData.cod_charges !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Checkout.Cart.Charge.COD" defaultMessage="COD" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {paymentCartData && `${paymentCartData.currency} ${paymentCartData.cod_charges}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}

                {isCod && codChanges && codChanges.charges && codChanges.charges !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Checkout.Cart.Charge.COD" defaultMessage="COD" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {codChanges && `${codChanges.currency} ${codChanges.charges}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}

                {/**Voucher Discount */}
                {paymentCartData &&
                paymentCartData.voucher_discount &&
                parseFloat(paymentCartData.voucher_discount) !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage
                                    id="Checkout.Cart.Charge.VoucherDiscount"
                                    defaultMessage="Voucher Discount"
                                />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {paymentCartData && `${paymentCartData.currency} ${paymentCartData.voucher_discount}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}
                {/**Vat */}
                {paymentCartData && paymentCartData.tax_amount && paymentCartData.tax_amount !== 0 ? (
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-label">
                                <FormattedMessage id="Checkout.Cart.Charge.VATIncluded" defaultMessage="VAT Included" />
                            </Typography>
                        </div>
                        <div>
                            <Typography className="cartpayment-value">
                                {paymentCartData && `${paymentCartData.currency} ${paymentCartData.tax_amount}`}
                            </Typography>
                        </div>
                    </Grid>
                ) : null}
                {/** Line */}
                <Grid item xs={12}>
                    <div className="border-line" />
                </Grid>
                {/** grand_total */}
                <Grid item xs={12}>
                    <Grid container justify="space-between">
                        <div>
                            <Typography className="cartpayment-total-label">
                                <FormattedMessage id="Cart.Total" defaultMessage="Total" />
                            </Typography>
                        </div>
                        <div>
                            {!isCod && (
                                <Typography className="cartpayment-total-value">
                                    {cartData &&
                                        `${cartData.currency} ${
                                            (paymentCartData && paymentCartData.grand_total) || cartData.grand_total
                                        }`}
                                </Typography>
                            )}
                            {isCod && (
                                <Typography className="cartpayment-total-value">
                                    {codChanges && `${codChanges.currency} ${codChanges && codChanges.total}`}
                                </Typography>
                            )}
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
