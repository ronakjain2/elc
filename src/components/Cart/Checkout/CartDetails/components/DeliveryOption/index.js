import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { useSelector } from 'react-redux';
import '../../cartDetails.css';

export default function CartDeliveryOption() {
    const state = useSelector((state) => state);

    const shippingMethodData = state && state.checkoutPayment && state.checkoutPayment.shippingMethodData;

    return (
        <Grid container>
            <Grid item xs={12}>
                <>
                    {shippingMethodData &&
                        Object.values(shippingMethodData).length > 0 &&
                        Object.values(shippingMethodData).map((data, index) => (
                            <Typography className="checkbox-style" key={index}>
                                <span className="bold-text">Shipment {index + 1}: </span>
                                {data && data.shipping_method_type === 'home_delivery' && (
                                    <>
                                        <span className="bold-text">{data.shipping_method_name}</span> {/* -{' '} */}
                                    </>
                                )}
                                {data && data.shipping_method_type === 'click_and_collect' && (
                                    <>
                                        <span className="bold-text">{data.shipping_method_name}</span>
                                    </>
                                )}
                            </Typography>
                        ))}

                    <Typography className="checkbox-style-subtext"></Typography>
                </>
            </Grid>
        </Grid>
    );
}
