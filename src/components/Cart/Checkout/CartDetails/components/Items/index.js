import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { getCartAmount } from 'components/Cart/Basket/utils';
import { FormattedMessage } from 'react-intl';

export default function CheckoutCartDetailItem({ item }) {
    return (
        <Grid container justify="space-between" className="item-row">
            <Grid item xs={3} style={{ textAlign: `end` }}>
                <img src={item && item.image && item.image[0]} alt="cartproductimage" className="cart-image" />
            </Grid>
            <Grid item xs={8}>
                <Grid container justify="flex-end">
                    <Grid item xs={12} style={{ textAlign: `end` }}>
                        <Typography className="item-name">{item && item.name}</Typography>
                    </Grid>
                    <Grid item xs={12} style={{ textAlign: `end` }}>
                        <Typography className="item-qty">
                            <FormattedMessage id="Cart.Quantity" defaultMessage="Quantity" />: &nbsp;
                            <span className="value">{item && item.qty}</span>
                        </Typography>
                    </Grid>
                    <Grid item xs={12} style={{ textAlign: `end` }}>
                        {getCartAmount(item)}
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={3}></Grid>
        </Grid>
    );
}
