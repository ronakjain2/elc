import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Edit from '@material-ui/icons/Edit';
import Spinner from 'commonComponet/Spinner';
import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import { actions as authActions } from 'components/Global/redux/actions';
import React, { lazy, Suspense, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import './cartDetails.css';

const CheckoutCartDetailItem = lazy(() => import('./components/Items'));
const CheckoutChargesList = lazy(() => import('./components/ChargesList'));
const CartDeliveryLocation = lazy(() => import('./components/DeliveryLocation'));
const CartDeliveryOption = lazy(() => import('./components/DeliveryOption'));
const GiftCard = lazy(() => import('./components/GiftCard'));

function CheckoutCartDetails({
    history,
    isPayment,
    address,
    isDelivery,
    onChangeHandler,
    isValidate,
    onChangeGiftWrapHandler,
    isCod,
}) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const quote_id = state && state.auth && state.auth.quote_id;
    const store_locale = state && state.auth && state.auth.store_locale;
    const currentStore = state && state.auth && state.auth.currentStore;
    const checkoutCartItems = state && state.basket && state.basket.cartDetails;
    const products = checkoutCartItems && checkoutCartItems.products;
    const basketLoader = state && state.basket && state.basket.loader;
    // const shippingMethodData = state && state.checkoutPayment && state.checkoutPayment.shippingMethodData;

    useEffect(() => {
        try {
            if (!quote_id || quote_id === '') {
                dispatch(authActions.removeReservationRequestSuccess());
                return history.push(`/${store_locale}/cart`);
            }
            if (!checkoutCartItems || Object.keys(checkoutCartItems).length === 0) {
                dispatch(
                    basketActions.getCartItemRequest({
                        quote_id: quote_id,
                        store_id: currentStore,
                        isPayment: isPayment,
                    }),
                );
            }
        } catch (err) {}
    }, [dispatch, quote_id, store_locale, history, checkoutCartItems, currentStore, isPayment]);

    const gotoCartDetails = () => {
        return history.push(`/${store_locale}/cart`);
    };

    const gotoDeliveryPageDetails = () => {
        return history.push(`/${store_locale}/delivery-details`);
    };

    return (
        <Suspense fallback={<Spinner />}>
            {/** Order Summary */}
            {!basketLoader && (
                <Grid container>
                    <Grid item xs={12} className="checkout-item-detail-cart">
                        <Grid container alignItems="center">
                            <Grid item xs={10}>
                                <Typography className="title">
                                    <FormattedMessage id="Cart.OrderSummary" defaultMessage="Order Summary" />
                                </Typography>
                            </Grid>
                            <Grid item xs={2} className="conatiner-end">
                                <IconButton className="edit-icon" size="small" onClick={() => gotoCartDetails()}>
                                    <Edit fontSize="small" />
                                </IconButton>
                            </Grid>
                        </Grid>

                        <Grid item xs={12}>
                            {products &&
                                products.map((item, index) => (
                                    <CheckoutCartDetailItem key={`Cart_item_detail_${index}`} item={item} />
                                ))}
                        </Grid>

                        <Grid item xs={12}>
                            <div className="border-line" />
                        </Grid>

                        <Grid item xs={12}>
                            <CheckoutChargesList isPayment={isPayment} isCod={isCod} />
                        </Grid>
                    </Grid>
                </Grid>
            )}
            {/** Gift Wrap */}
            {!basketLoader && isDelivery && (
                <Grid container style={{ paddingTop: 4 }}>
                    <Grid item xs={12} className="checkout-item-detail-cart">
                        <GiftCard
                            address={address}
                            onChangeHandler={onChangeHandler}
                            isValidate={isValidate}
                            onChangeGiftWrapHandler={onChangeGiftWrapHandler}
                        />
                    </Grid>
                </Grid>
            )}

            {basketLoader && (
                <Grid container alignItems="center">
                    <Spinner />
                </Grid>
            )}

            {/** Delivery Location */}
            {isPayment && (
                <Grid container style={{ paddingTop: 4 }}>
                    <Grid item xs={12} className="checkout-item-detail-cart">
                        <Grid container alignItems="center">
                            <Grid item xs={10}>
                                {/* {shippingMethodData && shippingMethodData.shipping_method_type === 'home_delivery' && ( */}
                                <Typography className="title1">
                                    <FormattedMessage
                                        id="Checkout.Payment.DeliveryLocation"
                                        defaultMessage="Delivery Location"
                                    />
                                </Typography>
                                {/* )}
                                {shippingMethodData && shippingMethodData.shipping_method_type === 'click_and_collect' && (
                                    <Typography className="title">
                                        <FormattedMessage
                                            id="Checkout.Payment.PickupAddress"
                                            defaultMessage="Pickup Address"
                                        />
                                    </Typography>
                                )} */}
                            </Grid>
                            <Grid item xs={2} className="conatiner-end">
                                <IconButton
                                    className="edit-icon"
                                    size="small"
                                    onClick={() => gotoDeliveryPageDetails()}
                                >
                                    <Edit fontSize="small" />
                                </IconButton>
                            </Grid>
                        </Grid>

                        <Grid item xs={12}>
                            <CartDeliveryLocation />
                        </Grid>
                    </Grid>
                </Grid>
            )}
            {/** Delivery Option */}
            {isPayment && (
                <Grid container style={{ paddingTop: 4 }}>
                    <Grid item xs={12} className="checkout-item-detail-cart">
                        <Grid container alignItems="center">
                            <Grid item xs={10}>
                                {/* {shippingMethodData && shippingMethodData.shipping_method_type === 'home_delivery' && ( */}
                                <Typography className="title1">
                                    <FormattedMessage
                                        id="Checkout.Payment.DeliveryOption"
                                        defaultMessage="Delivery Option"
                                    />
                                </Typography>
                                {/* )}
                                {shippingMethodData && shippingMethodData.shipping_method_type === 'click_and_collect' && (
                                    <Typography className="title">
                                        <FormattedMessage
                                            id="Checkout.Payment.PickupOptions"
                                            defaultMessage="Pickup Options"
                                        />
                                    </Typography>
                                )} */}
                            </Grid>
                            <Grid item xs={2} className="conatiner-end">
                                <IconButton
                                    className="edit-icon"
                                    size="small"
                                    onClick={() => gotoDeliveryPageDetails()}
                                >
                                    <Edit fontSize="small" />
                                </IconButton>
                            </Grid>
                        </Grid>

                        <Grid item xs={12}>
                            <CartDeliveryOption />
                        </Grid>
                    </Grid>
                </Grid>
            )}
        </Suspense>
    );
}

export default withRouter(CheckoutCartDetails);
