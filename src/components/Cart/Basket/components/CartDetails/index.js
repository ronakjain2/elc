import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import ChevronRight from '@material-ui/icons/ChevronRight';
import { actions as authActions } from 'components/Global/redux/actions';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import { checkAllItemOutOfStock } from '../../utils';
import CircularProgress from '@material-ui/core/CircularProgress';
import './cartPayment.css';

function CartPaymentDetails({ history, onJoinFamilyClub, showFCbutton }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const [value, setValue] = useState({});

    const language = state && state.auth && state.auth.language;
    const store_locale = state && state.auth && state.auth.store_locale;
    const cartData = state && state.basket && state.basket.cartDetails;
    const cartProducts = cartData && cartData.products;
    const reservationConfig = state && state.auth && state.auth.reservationConfig;
    const reservationLoader = state && state.auth && state.auth.reservationLoader;

    const store_id = state && state.auth && state.auth.currentStore;
    const quote_id = state && state.auth && state.auth.quote_id;

    const donate_toys = state && state.auth && state.auth.donate_toys;
    const [checked, setChecked] = useState(donate_toys);

    const gotoHomePage = () => history.push(`/${store_locale}/`);

    const checkUserLoginOrNot = () => {
        if (
            reservationConfig &&
            Object.keys(reservationConfig).length > 0 &&
            reservationConfig.reservation_status === 'Enable'
        ) {
            dispatch(
                authActions.addUpdateReservationRequest({
                    store_id: store_id,
                    quote_id: quote_id,
                    reservation_type: 'checkout',
                    from_cart : true,
                }),
            );
        }
    };

    const handleCheckBox = (e) => {
        let checked = e.target.checked;
        if (checked) {
            setChecked(true);
        } else {
            setChecked(false);
        }

        let values = value;

        values[e.target.value] = e.target.checked;
        setValue(values);
        dispatch(authActions.donateToysCheckbox(e.target.checked));
    };

    return (
        <Grid container justify="flex-end" className="paymentPadding">
            <Grid item xs={12} md={10} className="CartPaymentContainer">
                <Grid container>
                    <Grid item xs={12}>
                        <Typography className="OrderSummaryText">
                            <FormattedMessage id="Cart.OrderSummary" defaultMessage="Order Summary" />
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        {/** subtotal */}
                        {cartData && cartData.subtotal && (
                            <Grid container justify="space-between">
                                <div>
                                    <Typography className="cartpayment-label">
                                        <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                                    </Typography>
                                </div>
                                <div>
                                    <Typography className="cartpayment-value">
                                        {cartData && `${cartData.currency} ${cartData.subtotal}`}
                                    </Typography>
                                </div>
                            </Grid>
                        )}
                        {/** Discount amount */}
                        {cartData && cartData.discount_amount && (
                            <Grid container justify="space-between">
                                <div>
                                    <Typography className="cartpayment-label">
                                        <FormattedMessage id="Cart.Saving" defaultMessage="Saving" />
                                    </Typography>
                                </div>
                                <div>
                                    <Typography className="cartpayment-value">
                                        {cartData && `${cartData.currency} ${cartData.discount_amount}`}
                                    </Typography>
                                </div>
                            </Grid>
                        )}
                    </Grid>
                    {/** subtotal */}
                    {cartData && cartData.fc_discount_amount && parseFloat(cartData.fc_discount_amount) !== 0 && (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartFamilyClub-label">
                                    <FormattedMessage id="Cart.FamilyClubSaving" defaultMessage="Family Club Saving" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartFamilyClub-value">
                                    {cartData &&
                                        `${cartData.currency} ${parseFloat(cartData.fc_discount_amount).toFixed(2)}`}
                                </Typography>
                            </div>
                        </Grid>
                    )}
                    {cartData && cartData.fc_save_more && parseFloat(cartData.fc_save_more) !== 0 && (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartFamilyClub-label bold">
                                    <FormattedMessage
                                        id="Cart.SaveMoreWithFamilyClub"
                                        defaultMessage="Save More With Family Club"
                                    />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartFamilyClub-value bold">
                                    {cartData && `${cartData.currency} ${parseFloat(cartData.fc_save_more).toFixed(2)}`}
                                </Typography>
                            </div>
                        </Grid>
                    )}
                    <Grid item xs={12}>
                        <div className="line" />
                    </Grid>
                    {/** grand_total */}
                    <Grid item xs={12}>
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-total-label">
                                    <FormattedMessage id="Cart.Total" defaultMessage="Total" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-total-value">
                                    {cartData && `${cartData.currency} ${cartData.grand_total}`}
                                </Typography>
                            </div>
                        </Grid>
                    </Grid>
                    {/** Checkout button */}
                    <Grid item xs={12} className="d-flex justify-content-center">
                        <Button
                            className="paymentCheckout"
                            disabled={checkAllItemOutOfStock(cartProducts || []) || reservationLoader}
                            onClick={() => checkUserLoginOrNot()}
                            data-loader={reservationLoader}
                        >
                            {reservationLoader ? (
                                <CircularProgress />
                            ) : (
                                <FormattedMessage id="Cart.Checkout" defaultMessage="Checkout" />
                            )}
                        </Button>
                    </Grid>
                    {cartData.checkbox && (
                        <Grid item xs={12} className="donateBox">
                            <FormControlLabel
                                className="paymentCheckout-donateCheckbox"
                                value={value}
                                checked={checked}
                                control={<Checkbox />}
                                label={cartData.checkbox_label}
                                onChange={handleCheckBox}
                            />

                            <Typography className="paymentCheckout-donateText">{cartData.checkbox_msg}</Typography>
                        </Grid>
                    )}
                </Grid>
            </Grid>
            {/** Continue Shopping button */}
            <Grid item xs={12} md={10}>
                <Button
                    className="continue-shopping-button"
                    onClick={() => gotoHomePage()}
                    startIcon={language === 'en' ? <ArrowBackIos /> : <ChevronRight className="right-icon" />}
                >
                    <FormattedMessage id="Cart.ContinueShopping" defaultMessage="Continue Shopping" />
                </Button>
                {showFCbutton && (
                    <Button className="join-fc-button" onClick={onJoinFamilyClub}>
                        <FormattedMessage id="Cart.JoinFamilyClub" defaultMessage="Join Family Club" />
                    </Button>
                )}
            </Grid>
        </Grid>
    );
}

export default withRouter(CartPaymentDetails);
