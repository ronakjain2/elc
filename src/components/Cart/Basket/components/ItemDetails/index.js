import React from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import WebItemDetails from './components/web';
import MobileCartItemDetails from './components/mobile';

export default function ItemDetails() {
    return (
        <Grid container>
            {/** Web design */}
            <Hidden only={['sm', 'xs']}>
                <WebItemDetails />
            </Hidden>
            {/** Mobile design */}
            <Hidden only={['md', 'lg', 'xl']}>
                <MobileCartItemDetails />
            </Hidden>
        </Grid>
    );
}
