import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import CartItem from './Item';

export default function WebItemDetails({ setFamilyClubBox }) {
    const state = useSelector((state) => state);

    const cartDetails = state && state.basket && state.basket.cartDetails;
    const cartProducts = cartDetails && cartDetails.products;

    return (
        <>
            <Grid container>
                <Grid item xs={12}>
                    <Grid container className="cart-table-container">
                        <Grid item xs={5}>
                            <Typography className="WebItemTitle-text">
                                <FormattedMessage id="Cart.ItemNumberStyle" defaultMessage="Item (style number)" />
                            </Typography>
                        </Grid>
                        <Grid item xs={7}>
                            <Grid container justify="space-between">
                                <Grid item xs={3} className="textAlignCenter">
                                    <Typography className="WebItemTitle-text textAlignCenter">
                                        <FormattedMessage id="Cart.Price" defaultMessage="Price" />
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} className="textAlignCenter">
                                    <Typography className="WebItemTitle-text textAlignCenter">
                                        <FormattedMessage id="Cart.Quantity" defaultMessage="Quantity" />
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} className="textAlignCenter">
                                    <Typography className="WebItemTitle-text textAlignCenter">
                                        <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {cartProducts &&
                cartProducts.map((item, index) => (
                    <Grid container key={`basket_item_${index}`}>
                        <Grid item xs={12}>
                            <CartItem
                                item={item}
                                expiry_date={cartDetails.expiry_date}
                                membershipNo={cartDetails.party_id}
                                setFamilyClubBox={setFamilyClubBox}
                            />
                        </Grid>
                    </Grid>
                ))}
        </>
    );
}
