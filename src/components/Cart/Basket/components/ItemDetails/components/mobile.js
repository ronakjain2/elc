import React from 'react';
import Grid from '@material-ui/core/Grid';
import { useSelector } from 'react-redux';
import CartItem from './Item';

export default function MobileCartItemDetails() {
    const state = useSelector((state) => state);

    const cartDetails = state && state.basket && state.basket.cartDetails;
    const cartProducts = cartDetails && cartDetails.products;

    return (
        <Grid container>
            {cartProducts &&
                cartProducts.map((item, index) => (
                    <Grid container key={`basket_item_${index}`}>
                        <Grid item xs={12}>
                            <CartItem
                                item={item}
                                expiry_date={cartDetails.expiry_date}
                                membershipNo={cartDetails.party_id}
                            />
                        </Grid>
                    </Grid>
                ))}
        </Grid>
    );
}
