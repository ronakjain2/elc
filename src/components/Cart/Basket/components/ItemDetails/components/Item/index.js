import { FormHelperText } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Add from '@material-ui/icons/Add';
import Close from '@material-ui/icons/Close';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Remove from '@material-ui/icons/Remove';
import FCBenfitTag from 'commonComponet/FCBenefitTag';
import { actions as myWishlistActions } from 'components/Account/MyWishList/redux/actions';
import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import {
    checkItemIsFree,
    checkItemOutOfStock,
    checkQtyIsGreaterThanStock,
    _getCartPrice,
    _getCartSubtotal,
    _getCartSubtotalMob,
} from 'components/Cart/Basket/utils';
import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import './item.css';

function CartItem({ item, history, expiry_date, membershipNo }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();
    const [currentProduct, setcurrentProduct] = useState(null);

    const wishlistLoader = state && state.myWishlist && state.myWishlist.loader;
    useEffect(() => {
        if (!wishlistLoader) setcurrentProduct(null);
    }, [wishlistLoader]);

    const store_locale = state && state.auth && state.auth.store_locale;
    const quote_id = state && state.auth && state.auth.quote_id;
    const store_id = state && state.auth && state.auth.currentStore;
    const loginUser = state && state.auth && state.auth.loginUser;
    const wishListItems = state && state.myWishlist && state.myWishlist.wishListItems;
    const wishlistArr = (state && state.myWishlist && state.myWishlist.wishlistArr) || [];
    const cartData = state && state.basket && state.basket.cartDetails;
    const has_fcsku = cartData && cartData.has_fcsku === 1;
    const customer_type = loginUser && loginUser.customer_type;
    const fc_status = state && state.auth && state.auth.fc_status;

    const gotoPDPPage = (item) => {
        if (item && item.price > 0 && item.is_fc_sku !== 1) {
            history.push(`/${store_locale}/products-details/${item && item.url_key}`);
        }
    };

    const addOrRemoveWishItem = (productDetails) => {
        const id = productDetails && productDetails.id;
        setcurrentProduct(id);
        const index = wishlistArr.indexOf(id);
        if (loginUser && loginUser.customer_id) {
            if (index === -1) {
                dispatch(
                    myWishlistActions.addWishListItemRemoveCartRequest({
                        customer_id: loginUser.customer_id,
                        product_id: id,
                        removeCartPayload: productDetails,
                        quote_id: quote_id,
                        store_id: store_id,
                    }),
                );
            } else {
                let temp_wishListItems = wishListItems;
                let data = temp_wishListItems.filter((d) => d.product_id === id)[0];
                dispatch(
                    myWishlistActions.removeWishlistRequest({
                        customerid: loginUser.customer_id,
                        store_id: store_id,
                        wishilistitemid: data.wishlist_id,
                    }),
                );
            }
        } else {
            history.push(`/${store_locale}/sign-in-register`);
        }
    };

    const reduceQty = (data) => {
        if (data && data.qty > 1) {
            dispatch(
                basketActions.updateCartItemQtyRequest({
                    product_id: data.id,
                    quote_id,
                    qty: parseInt(data.qty) - 1,
                    sku: data.sku,
                    store_id,
                    delivery_type: 'DELIVERY',
                }),
            );
        }
    };

    const addQty = (data) => {
        if (data.qty < parseInt(data && data.is_in_stock && data.is_in_stock.stock)) {
            dispatch(
                basketActions.updateCartItemQtyRequest({
                    product_id: data.id,
                    quote_id,
                    qty: parseInt(data.qty) + 1,
                    sku: data.sku,
                    store_id,
                    delivery_type: 'DELIVERY',
                }),
            );
        }
    };

    const removeProducts = (data) => {
        dispatch(
            basketActions.removeCartItemRequest({
                quote_id,
                sku: data.sku,
                store_id,
                category_names: data.ga_cat_names,
                name: data.name,
                qty: data.qty,
                special_price: data.special_price,
                price: data.price,
            }),
        );
    };

    return (
        <>
            <Hidden only={['xs', 'sm']}>
                <Grid container className="CartItem ">
                    <Grid item xs={5}>
                        <Grid container>
                            <Grid
                                item
                                xs={6}
                                className={
                                    checkItemOutOfStock(item)
                                        ? `textAlignCenter ${!checkItemIsFree(item) && 'cursor-pointer'}  OutOfStock`
                                        : `textAlignCenter ${!checkItemIsFree(item) && 'cursor-pointer'}`
                                }
                                onClick={() => gotoPDPPage(item)}
                            >
                                <img
                                    src={item && item.image && item.image[0]}
                                    alt="cartImage"
                                    className="cartItemImage"
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <Grid container alignItems="center">
                                    <Grid item xs={12} className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}>
                                        <Typography className="cartItemName">{item && item.name}</Typography>
                                    </Grid>
                                    {/*<Grid item xs={12} className={checkItemOutOfStock(item) ? "OutOfStock": ""}>
                                        <Typography className="cartItemOtherDetailText">
                                            <FormattedMessage id="PDP.Age" defaultMessage="Age" />
                                            :

                                        </Typography>
                                    </Grid>*/}
                                    {item.is_fc_sku === 1 ? (
                                        <Grid item xs={12} className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}>
                                            <Typography className="cartItemOtherDetailText">
                                                <FormattedMessage
                                                    id="Cart.MembershipNo"
                                                    defaultMessage="Membership No"
                                                />
                                                : &nbsp;{membershipNo}
                                            </Typography>
                                        </Grid>
                                    ) : (
                                        <Grid item xs={12} className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}>
                                            <Typography className="cartItemOtherDetailText">
                                                <FormattedMessage
                                                    id="PDP.Specifications.ProductCode"
                                                    defaultMessage="Product Code"
                                                />
                                                : &nbsp;{item && item.sku}
                                            </Typography>
                                        </Grid>
                                    )}

                                    {item.is_fc_sku === 1 && (
                                        <Grid item xs={12} className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}>
                                            <Typography className="cartItemOtherDetailText">
                                                <FormattedMessage id="Cart.ExpiryDate" defaultMessage="Expiry Date" />:
                                                &nbsp;{expiry_date}
                                            </Typography>
                                        </Grid>
                                    )}

                                    <Grid item xs={12}>
                                        {parseInt(item.price) !== 0 && item.is_fc_sku !== 1 && (
                                            <Grid container>
                                                {wishlistLoader && currentProduct === item.id ? (
                                                    <Box mt={2}>
                                                        <CircularProgress className="spin-progress" size="20px" />
                                                    </Box>
                                                ) : wishlistArr.indexOf(item.id) === -1 ? (
                                                    <Typography
                                                        className="cartItemWishlist"
                                                        onClick={() => addOrRemoveWishItem(item)}
                                                    >
                                                        <FavoriteBorder />
                                                        <FormattedMessage
                                                            id="Cart.MoveToWishlist"
                                                            defaultMessage="Move to Wishlist"
                                                        />
                                                    </Typography>
                                                ) : (
                                                    <Typography
                                                        className="cartItemWishlist"
                                                        onClick={() => addOrRemoveWishItem(item)}
                                                    >
                                                        <Favorite />
                                                        <FormattedMessage
                                                            id="MyAccount.MyWishlist.RemoveFromWishlist"
                                                            defaultMessage="Remove From Wishlist"
                                                        />
                                                    </Typography>
                                                )}
                                            </Grid>
                                        )}
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={7}>
                        <Grid
                            container
                            justify="space-between"
                            className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}
                        >
                            <Grid item xs={3} className="textAlignCenter">
                                {parseInt(item.price) !== 0 && _getCartPrice(item)}
                            </Grid>
                            <Grid item xs={4} className="textAlignCenter">
                                {parseInt(item.price) !== 0 && (
                                    <ButtonGroup
                                        className="addAndRemove"
                                        variant="contained"
                                        aria-label="contained primary button group"
                                    >
                                        <Button
                                            className="remove-border-ar"
                                            disabled={
                                                item.is_fc_sku === 1 ||
                                                checkItemOutOfStock(item) ||
                                                item.qty <= 1 ||
                                                parseInt(item.price) === 0
                                            }
                                            onClick={() => reduceQty(item)}
                                        >
                                            <Remove />
                                        </Button>
                                        <Button
                                            className={'input'.concat(item.is_fc_sku === 1 ? ' text-black-50' : '')}
                                            disabled
                                        >
                                            {item.qty}
                                        </Button>
                                        <Button
                                            disabled={
                                                item.is_fc_sku === 1 ||
                                                checkItemOutOfStock(item) ||
                                                item.qty >=
                                                    parseInt(item && item.is_in_stock && item.is_in_stock.stock) ||
                                                parseInt(item.price) === 0
                                            }
                                            onClick={() => addQty(item)}
                                        >
                                            <Add />
                                        </Button>
                                    </ButtonGroup>
                                )}
                                {checkQtyIsGreaterThanStock(item) &&
                                    item &&
                                    item.is_in_stock &&
                                    item.is_in_stock.stock > 0 && (
                                        <FormHelperText className="CartonlyInStockMessage">
                                            <FormattedMessage id="PDP.Quantity.Only" defaultMessage="Only" />
                                            &nbsp;
                                            <span>{item && item.is_in_stock && item.is_in_stock.stock}</span>
                                            &nbsp;
                                            <FormattedMessage
                                                id="PDP.Quantity.LeftInStock"
                                                defaultMessage="left in stock"
                                            />
                                        </FormHelperText>
                                    )}
                            </Grid>
                            <Grid item xs={4} className="textAlignCenter">
                                {_getCartSubtotal(item)}
                                <FCBenfitTag
                                    show={
                                        parseInt(item.family_club) === 1 &&
                                        (has_fcsku || customer_type === 'ecom_club_user')
                                    }
                                />
                            </Grid>
                            {/* {item.is_fc_sku !== 1&&<Grid item xs={12} className="CartAvailableFor">

                                <Grid container justify="flex-end">
                                    <Typography className="family-club-text">Family Club Benifit Applied</Typography>
                                </Grid>
                            </Grid>} */}
                            <Grid item xs={12} className="CartAvailableFor">
                                {item.is_fc_sku !== 1 && !checkItemOutOfStock(item) && (
                                    <>
                                        <Grid container justify="flex-end">
                                            <Typography className="availableFor-text">
                                                <FormattedMessage
                                                    id="PDP.AvailableFor"
                                                    defaultMessage="Available for"
                                                />
                                            </Typography>
                                            <Grid container justify="flex-end">
                                                <div>
                                                    <List
                                                        component="nav"
                                                        aria-label="main mailbox folders"
                                                        className="p-t-b"
                                                    >
                                                        <ListItem className="p-t-b">
                                                            <ListItemIcon>
                                                                <img
                                                                    src="/images/pdp/Icon_Header_FreeShip.svg"
                                                                    alt="pickupformstor"
                                                                    className="MuiSvgIcon-root-Free"
                                                                />
                                                            </ListItemIcon>
                                                            <ListItemText
                                                                primary={
                                                                    <FormattedMessage
                                                                        id="PDP.HomeDelivery"
                                                                        defaultMessage="Home Delivery"
                                                                    />
                                                                }
                                                            />
                                                        </ListItem>
                                                    </List>
                                                </div>
                                                <div className="flex alignItemCenter">
                                                    <Divider orientation="vertical" className="line" />
                                                </div>
                                                <div>
                                                    <List
                                                        component="nav"
                                                        aria-label="main mailbox folders"
                                                        className="p-t-b"
                                                    >
                                                        <ListItem
                                                            className={`padding-right-zero p-t-b ${
                                                                item.click_collect_ds_flag === 1 && 'strick-out'
                                                            }`}
                                                        >
                                                            <ListItemIcon>
                                                                {item.click_collect_ds_flag === 1 && (
                                                                    <img
                                                                        src="/images/pdp/Icon_Cart_Cart_Gray.svg"
                                                                        alt="pickupformstor"
                                                                        className="MuiSvgIcon-root-Free"
                                                                    />
                                                                )}
                                                                {(!item.click_collect_ds_flag ||
                                                                    item.click_collect_ds_flag === 0) && (
                                                                    <img
                                                                        src="/images/pdp/Icon_Cart_Cart_Green.svg"
                                                                        alt="pickupformstor"
                                                                        className="MuiSvgIcon-root-Free"
                                                                    />
                                                                )}
                                                            </ListItemIcon>
                                                            <ListItemText
                                                                primary={
                                                                    <FormattedMessage
                                                                        id="PDP.PickupFromStore"
                                                                        defaultMessage="Pickup from store"
                                                                    />
                                                                }
                                                            />
                                                        </ListItem>
                                                    </List>
                                                </div>
                                            </Grid>
                                        </Grid>
                                    </>
                                )}

                                {checkItemOutOfStock(item) && (
                                    <Grid container justify="flex-end">
                                        <Typography className="SoldOut-text">
                                            <FormattedMessage id="Cart.SoldOut" defaultMessage="Sold Out" />
                                        </Typography>
                                    </Grid>
                                )}
                                {item.is_fc_sku === 1 && !fc_status && checkItemOutOfStock(item) && (
                                    <Grid container justify="flex-end">
                                        <Typography className="SoldOut-text">
                                            <FormattedMessage
                                                id="FC.cart.CommingSoon"
                                                defaultMessage="The family club will be coming soon for this store"
                                            />
                                        </Typography>
                                    </Grid>
                                )}
                            </Grid>
                        </Grid>
                    </Grid>
                    {parseFloat(item.price) !== 0 && (
                        <Close className="close-button" onClick={() => removeProducts(item)} />
                    )}
                </Grid>
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                <Grid container className="CartItemM" justify="space-between">
                    <Grid
                        item
                        xs={4}
                        className={`textAlignCenter displayFlexAlignEnd ${!checkItemIsFree(item) && `cursor-pointer`} ${
                            checkItemOutOfStock(item) && `OutOfStock`
                        }`}
                        onClick={() => gotoPDPPage(item)}
                    >
                        <img src={item && item.image && item.image[0]} alt="cartImage" className="cartItemImageM" />
                    </Grid>
                    <Grid item xs={7} className={`${checkItemOutOfStock(item) && `OutOfStock`}`}>
                        <Grid item xs={12}>
                            <Typography className="cartItemName">{item && item.name}</Typography>
                            {/*<Grid item xs={12}>
                                <Typography className="cartItemOtherDetailText">
                                    <FormattedMessage id="PDP.Age" defaultMessage="Age" />
                                    :
                                </Typography>
                            </Grid>*/}
                            {/* <Grid item xs={12}>
                                <Typography className="cartItemOtherDetailText">
                                    <FormattedMessage
                                        id="PDP.Specifications.ProductCode"
                                        defaultMessage="Product Code"
                                    />
                                    : &nbsp;
                                    {item && item.sku}
                                </Typography>
                            </Grid> */}
                            {item.is_fc_sku === 1 ? (
                                <Grid item xs={12} className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}>
                                    <Typography className="cartItemOtherDetailText">
                                        <FormattedMessage id="Cart.MembershipNo" defaultMessage="Membership No" />:
                                        &nbsp;{membershipNo}
                                    </Typography>
                                </Grid>
                            ) : (
                                <Grid item xs={12} className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}>
                                    <Typography className="cartItemOtherDetailText">
                                        <FormattedMessage
                                            id="PDP.Specifications.ProductCode"
                                            defaultMessage="Product Code"
                                        />
                                        : &nbsp;{item && item.sku}
                                    </Typography>
                                </Grid>
                            )}

                            {item.is_fc_sku === 1 && (
                                <Grid item xs={12} className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}>
                                    <Typography className="cartItemOtherDetailText">
                                        <FormattedMessage id="Cart.ExpiryDate" defaultMessage="Expiry Date" />: &nbsp;
                                        {expiry_date}
                                    </Typography>
                                </Grid>
                            )}

                            {checkItemOutOfStock(item) && (
                                <Grid container>
                                    <Typography className="SoldOut-text">
                                        <FormattedMessage id="Cart.SoldOut" defaultMessage="Sold Out" />
                                    </Typography>
                                </Grid>
                            )}
                            {item.is_fc_sku === 1 && !fc_status && checkItemOutOfStock(item) && (
                                <Grid container>
                                    <Typography className="SoldOut-text">
                                        <FormattedMessage
                                            id="FC.cart.CommingSoon"
                                            defaultMessage="The family club will be coming soon for this store"
                                        />
                                    </Typography>
                                </Grid>
                            )}
                            {item.is_fc_sku !== 1 && !checkItemOutOfStock(item) && (
                                <Grid item xs={12} className="CartAvailableFor">
                                    <Grid container justify="flex-start">
                                        <Typography className="availableFor-text">
                                            <FormattedMessage id="PDP.AvailableFor" defaultMessage="Available for" />
                                        </Typography>
                                        <Grid container justify="flex-start">
                                            <div>
                                                <List
                                                    component="nav"
                                                    aria-label="main mailbox folders"
                                                    className="p-t-b"
                                                >
                                                    <ListItem className="p-t-b p-l-zero">
                                                        <ListItemText
                                                            primary={
                                                                <FormattedMessage
                                                                    id="PDP.HomeDelivery"
                                                                    defaultMessage="Home Delivery"
                                                                />
                                                            }
                                                        />
                                                    </ListItem>
                                                </List>
                                            </div>
                                            <div className="flex alignItemCenter">
                                                <Divider orientation="vertical" className="line" />
                                            </div>
                                            <div>
                                                <List
                                                    component="nav"
                                                    aria-label="main mailbox folders"
                                                    className="p-t-b"
                                                >
                                                    <ListItem
                                                        className={`padding-right-zero p-t-b ${
                                                            item.click_collect_ds_flag === 1 && 'strick-out'
                                                        }`}
                                                    >
                                                        <ListItemText
                                                            primary={
                                                                <FormattedMessage
                                                                    id="PDP.PickupFromStore"
                                                                    defaultMessage="Pickup from store"
                                                                />
                                                            }
                                                        />
                                                    </ListItem>
                                                </List>
                                            </div>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            )}
                        </Grid>
                    </Grid>
                    <Grid item xs={4} className={`${checkItemOutOfStock(item) && `OutOfStock`}`}>
                        {parseInt(item.price) !== 0 && (
                            <ButtonGroup
                                className="addAndRemove"
                                variant="contained"
                                aria-label="contained primary button group"
                            >
                                <Button
                                    className="remove-border-ar"
                                    disabled={
                                        item.is_fc_sku === 1 ||
                                        checkItemOutOfStock(item) ||
                                        item.qty <= 1 ||
                                        parseInt(item.price) === 0
                                    }
                                    onClick={() => reduceQty(item)}
                                >
                                    <Remove />
                                </Button>
                                <Button
                                    className={'input'.concat(item.is_fc_sku === 1 ? ' text-black-50' : '')}
                                    disabled
                                >
                                    {item.qty}
                                </Button>
                                <Button
                                    disabled={
                                        item.is_fc_sku === 1 ||
                                        checkItemOutOfStock(item) ||
                                        item.qty >= parseInt(item && item.is_in_stock && item.is_in_stock.stock) ||
                                        parseInt(item.price) === 0
                                    }
                                    onClick={() => addQty(item)}
                                >
                                    <Add />
                                </Button>
                            </ButtonGroup>
                        )}
                        {checkQtyIsGreaterThanStock(item) && item && item.is_in_stock && item.is_in_stock.stock > 0 && (
                            <FormHelperText className="CartonlyInStockMessage">
                                <FormattedMessage id="PDP.Quantity.Only" defaultMessage="Only" />
                                &nbsp;
                                <span>{item && item.is_in_stock && item.is_in_stock.stock}</span>
                                &nbsp;
                                <FormattedMessage id="PDP.Quantity.LeftInStock" defaultMessage="left in stock" />
                            </FormHelperText>
                        )}
                    </Grid>
                    <Grid item className={`${checkItemOutOfStock(item) && `OutOfStock`}`} xs={7}>
                        {_getCartSubtotalMob(item)}
                        <FCBenfitTag
                            show={parseInt(item.family_club) === 1 && (has_fcsku || customer_type === 'ecom_club_user')}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        {parseInt(item.price) !== 0 ? (
                            <Grid
                                container
                                justify="space-around"
                                alignItems="center"
                                className="removeAndWishListContainer"
                            >
                                <div>
                                    <Typography className="removeItem" onClick={() => removeProducts(item)}>
                                        <Close />
                                        <FormattedMessage id="Cart.Remove.text" defaultMessage="Remove" />
                                    </Typography>
                                </div>
                                {item.is_fc_sku !== 1 && (
                                    <>
                                        <div className="lineVertical" />
                                        <div>
                                            {wishlistLoader && currentProduct === item.id ? (
                                                <CircularProgress className="spin-progress" size="20px" />
                                            ) : wishlistArr.indexOf(item.id) === -1 ? (
                                                <Typography
                                                    className="WishListM"
                                                    onClick={() => addOrRemoveWishItem(item)}
                                                >
                                                    <FavoriteBorder />
                                                    <FormattedMessage
                                                        id="Cart.MoveToWishlist"
                                                        defaultMessage="Move to Wishlist"
                                                    />
                                                </Typography>
                                            ) : (
                                                <Typography
                                                    className="WishListM"
                                                    onClick={() => addOrRemoveWishItem(item)}
                                                >
                                                    <Favorite />
                                                    <FormattedMessage
                                                        id="MyAccount.MyWishlist.RemoveFromWishlist"
                                                        defaultMessage="Remove From Wishlist"
                                                    />
                                                </Typography>
                                            )}
                                        </div>
                                    </>
                                )}
                            </Grid>
                        ) : (
                            <Grid
                                container
                                justify="space-around"
                                alignItems="center"
                                className="emptyContainer"
                            ></Grid>
                        )}
                    </Grid>
                </Grid>
            </Hidden>
        </>
    );
}

export default withRouter(CartItem);
