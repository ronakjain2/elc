import React from 'react';
import Grid from '@material-ui/core/Grid';
import { FormattedMessage } from 'react-intl';
import './youMayAlsoLike.css';
import { useSelector } from 'react-redux';
import ProductListCard from 'commonComponet/ProductListCard';
import Slider from 'react-slick';

export default function YouMayAlsoLike() {
    const state = useSelector((state) => state);

    let youMayAlsoLikeProducts =
        (state && state.basket && state.basket.cartDetails && state.basket.cartDetails.similar_products) || [];
    youMayAlsoLikeProducts = youMayAlsoLikeProducts.filter((item) => item.stock !== 0);

    if (youMayAlsoLikeProducts.length < 5) {
        youMayAlsoLikeProducts = youMayAlsoLikeProducts.concat(youMayAlsoLikeProducts);
        youMayAlsoLikeProducts = youMayAlsoLikeProducts.concat(youMayAlsoLikeProducts);
    }

    const currency = state && state.PDP && state.PDP.details && state.PDP.details.currency;

    const youMayAlsoLikeSettings = {
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        infinite: true,
        slidesToShow: youMayAlsoLikeProducts.length >= 5 ? 5 : youMayAlsoLikeProducts.length,
        slidesToScroll: 1,
        speed: 500,
        vertical: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    return (
        <Grid container className="m-t-p-30">
            <Grid container justify="center">
                <Grid item xs={11} className="cart-you-may-also-like flex">
                    <span className="line" />
                    <label>
                        <FormattedMessage id="Common.YouMayAlsoLike" defaultMessage="You may also like" />
                    </label>
                    <span className="line" />
                </Grid>
            </Grid>

            <Grid container justify="center" style={{ marginBottom: 50 }}>
                <Grid item xs={11} md={10} className="bestsellerslider">
                    <Slider {...youMayAlsoLikeSettings}>
                        {youMayAlsoLikeProducts &&
                            youMayAlsoLikeProducts.map((item, index) => (
                                <div key={`bestSeller_${index}`}>
                                    <ProductListCard data={item} currency={currency} />
                                </div>
                            ))}
                    </Slider>
                </Grid>
            </Grid>
        </Grid>
    );
}
