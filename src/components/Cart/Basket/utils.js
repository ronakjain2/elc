import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';

const _getFCPrice = (item, classNames = '', align = '') => {
    return (
        <>
            <Typography className={classNames}>
                <div className={'price-div text-align-start w-fit-content '.concat(align)}>
                    <span className="d-block">
                        {`${item && item.currency} ${
                            item && item.price && parseFloat(item.price * (item.qty || item.qty_orderded)).toFixed(2)
                        }`}
                    </span>
                    <span className="fc-cart-price d-block">
                        {`${item && item.currency} ${
                            item &&
                            item.fc_special_price &&
                            parseFloat(item.fc_special_price * (item.qty || item.qty_orderded)).toFixed(2)
                        }`}
                    </span>
                </div>
            </Typography>
            <Typography className="fc-benefit-message">
                <span>
                    <img
                        src="/images/pdp/icon_fc_benefit.svg"
                        alt="family club benefit applied"
                        style={{ height: '16px', width: '16px' }}
                    />
                    &nbsp;
                </span>
                <FormattedMessage id="cart.FamilyClubExclusivePrice" defaultMessage="Family Club Exclusive Price" />
            </Typography>
        </>
    );
};

// Web cart price
export const _getCartPrice = (item) => {
    if (!item) return null;
    if (parseInt(item.price) === 0)
        return (
            <Typography className="cart-showing-price">
                <FormattedMessage id="Cart.free" defaultMessage="Free" />
            </Typography>
        );
    if (item.special_price && parseInt(item.special_price))
        return (
            <>
                <Typography className="cart-showing-price">
                    {`${item && item.currency} ${
                        item && item.special_price && parseFloat(item.special_price).toFixed(2)
                    }`}
                </Typography>
                <Typography className="cart-showing-price-strik">
                    {`${item && item.currency} ${item && parseFloat(item.price).toFixed(2)}`}
                </Typography>
            </>
        );
    return (
        <Typography variant="span" className="cart-showing-price">
            {`${item && item.currency} ${item && parseFloat(item.price).toFixed(2)}`}&nbsp;
        </Typography>
    );
};

// Web Cart Subtotal
export const _getCartSubtotal = (item) => {
    if (!item) return null;
    if (item && parseInt(item.price) === 0)
        return (
            <Typography className="cartSubTotal">
                <FormattedMessage id="Cart.free" defaultMessage="Free" />
            </Typography>
        );
    if (item.special_price !== null && parseFloat(item.special_price) !== 0)
        return (
            <>
                <Typography className="cartSubTotal">
                    {`${item && item.currency} ${
                        item &&
                        item.special_price &&
                        parseFloat(item.special_price * (item.qty || item.qty_orderded)).toFixed(2)
                    }`}
                </Typography>
                <Typography className="cart-showing-price-strik">
                    {`${item && item.currency} ${
                        item && parseFloat(item.price * (item.qty || item.qty_orderded)).toFixed(2)
                    }`}
                </Typography>
            </>
        );
    if (item.fc_special_price) return _getFCPrice(item, 'cartSubTotal', 'm-auto');
    return (
        <Typography className="cartSubTotal">
            {`${item && item.currency} ${
                item && item.price && parseFloat(item.price * (item.qty || item.qty_orderded)).toFixed(2)
            }`}
        </Typography>
    );
};

// Mobile Cart Subtotal
export const _getCartSubtotalMob = (item) => {
    if (!item) return null;
    if (item && parseInt(item.price) === 0)
        return (
            <Typography className="cart-showing-price ar-right">
                <FormattedMessage id="Cart.free" defaultMessage="Free" />
            </Typography>
        );
    if (item.special_price !== null && parseFloat(item.special_price) !== 0)
        return (
            <>
                <Typography className="cart-showing-price ar-right">
                    {`${item && item.currency} ${
                        item &&
                        item.special_price &&
                        parseFloat(item.special_price * (item.qty || item.qty_orderded)).toFixed(2)
                    }`}
                </Typography>
                <Typography className="cart-showing-price-strik ar-right">
                    {`${item && item.currency} ${
                        item && parseFloat(item.price * (item.qty || item.qty_orderded)).toFixed(2)
                    }`}
                </Typography>
            </>
        );
    if (item.fc_special_price) return _getFCPrice(item, 'cart-showing-price ar-right');
    return (
        <Typography className="cart-showing-price ar-right">
            {`${item && item.currency} ${
                item && item.price && parseFloat(item.price * (item.qty || item.qty_orderded)).toFixed(2)
            }`}
        </Typography>
    );
};

// Web OrderSummary Price
export const _getOrderSummaryPrice = (item) => {
    if (!item) return null;
    if (parseInt(item.price) === 0)
        return (
            <Typography className="cart-showing-price">
                <FormattedMessage id="Cart.free" defaultMessage="Free" />
            </Typography>
        );
    if (item.special_price && parseInt(item.special_price))
        return (
            <>
                <Typography className="cart-showing-price">
                    {`${item && item.currency} ${
                        item && item.special_price && parseFloat(item.special_price).toFixed(2)
                    }`}
                </Typography>
                <Typography className="cart-showing-price-strik">
                    {`${item && item.currency} ${item && parseFloat(item.price).toFixed(2)}`}
                </Typography>
            </>
        );
    return (
        <Typography variant="span" className="cart-showing-price">
            {`${item && item.currency} ${item && parseFloat(item.price).toFixed(2)}`}&nbsp;
        </Typography>
    );
};
// Web OrderSummary subtotal
export const _getOrderSummarySubtotal = (item) => {
    if (!item) return null;
    if (item && parseInt(item.price) === 0)
        return (
            <Typography className="cartSubTotal">
                <FormattedMessage id="Cart.free" defaultMessage="Free" />
            </Typography>
        );
    if (item.special_price !== null && parseFloat(item.special_price) !== 0)
        return (
            <>
                <Typography className="cartSubTotal">
                    {`${item && item.currency} ${
                        item &&
                        item.special_price &&
                        parseFloat(item.special_price * (item.qty || item.qty_orderded)).toFixed(2)
                    }`}
                </Typography>
                <Typography className="cart-showing-price-strik">
                    {`${item && item.currency} ${item && parseFloat(item.price).toFixed(2)}`}
                </Typography>
            </>
        );
    if (item.fc_special_price) return _getFCPrice(item, 'cartSubTotal');
    return (
        <Typography className="cartSubTotal">
            {`${item && item.currency} ${
                item && item.price && parseFloat(item.price * (item.qty || item.qty_orderded)).toFixed(2)
            }`}
        </Typography>
    );
};

export const _getOrderSummarySubtotalMob = (item) => {
    if (!item) return null;
    if (item && parseInt(item.price) === 0)
        return (
            <Typography className="cart-showing-price ar-right">
                <FormattedMessage id="Cart.free" defaultMessage="Free" />
            </Typography>
        );
    if (item.special_price !== null && parseFloat(item.special_price) !== 0)
        return (
            <>
                <Typography className="cart-showing-price ar-right">
                    {`${item && item.currency} ${
                        item &&
                        item.special_price &&
                        parseFloat(item.special_price * (item.qty || item.qty_orderded)).toFixed(2)
                    }`}
                </Typography>
                <Typography className="cart-showing-price-strik">
                    {`${item && item.currency} ${
                        item && parseFloat(item.price * (item.qty || item.qty_orderded)).toFixed(2)
                    }`}
                </Typography>
            </>
        );
    if (item.fc_special_price) return _getFCPrice(item, 'cart-showing-price ar-right');
    return (
        <Typography className="cart-showing-price ar-right">
            {`${item && item.currency} ${
                item && item.price && parseFloat(item.price * (item.qty || item.qty_orderded)).toFixed(2)
            }`}
        </Typography>
    );
};

const getFCPrice = (item) => {
    console.log(`item`, item);
    return (
        <>
            <Typography variant="span" className="fc-cart-price d-block d-md-inline-block">
                {`${item && item.currency} ${
                    item &&
                    item.fc_special_price &&
                    parseFloat(item.fc_special_price * (item.qty || item.qty_orderded)).toFixed(2)
                }`}
            </Typography>
            <Typography className="fc-benefit-message">
                <span>
                    &nbsp;
                    <img
                        src="/images/pdp/icon_fc_benefit.svg"
                        alt="family club benefit applied"
                        style={{ height: '16px', width: '16px' }}
                    />
                    &nbsp;
                </span>
                <FormattedMessage id="cart.FamilyClubExclusivePrice" defaultMessage="Family Club Exclusive Price" />
            </Typography>
        </>
    );
};

export const getCartAmount = (item, showFCPrice = false) => (
    <>
        {item && item.special_price !== null && parseFloat(item.special_price) !== 0 ? (
            <>
                <Typography className="cart-showing-price">
                    {`${item && item.currency} ${
                        item && item.special_price && parseFloat(item.special_price).toFixed(2)
                    }`}
                </Typography>
                <Typography className="cart-showing-price-strik">
                    {`${item && item.currency} ${item && parseFloat(item.price).toFixed(2)}`}
                </Typography>
            </>
        ) : parseInt(item.price) !== 0 ? (
            <Typography variant="span" className="cart-showing-price">
                {`${item && item.currency} ${item && parseFloat(item.price).toFixed(2)}`}&nbsp;
                {showFCPrice && item && item.fc_special_price && getFCPrice(item)}
            </Typography>
        ) : (
            <Typography className="cart-showing-price">
                <FormattedMessage id="Cart.free" defaultMessage="Free" />
            </Typography>
        )}
    </>
);

export const checkItemOutOfStock = (item) => {
    if (item && item.is_in_stock && item.is_in_stock.stock <= 0) {
        return true;
    }
    return false;
};

export const checkItemIsFree = (item) => {
    if (item && !item.special_price && parseFloat(item.price) === 0) {
        return true;
    }
    return false;
};

//If Qty greater than stock check
export const checkQtyIsGreaterThanStock = (item) => {
    if (item && item.is_in_stock && item.is_in_stock.stock < item.qty) {
        return true;
    }

    return false;
};

//check all Product stock and qty value
export const checkAllItemOutOfStock = (items) => {
    let outOfStock = items.filter((item) => item && item.is_in_stock && item.is_in_stock.stock <= 0);
    if (outOfStock && outOfStock.length > 0) {
        return true;
    }

    let qtyGreaterThanStock = items.filter((item) => item && item.is_in_stock && item.is_in_stock.stock < item.qty);
    if (qtyGreaterThanStock && qtyGreaterThanStock.length > 0) {
        return true;
    }

    return false;
};

export const getSubTotalAmountForOrderDetails = (item) => (
    <>
        {item && item.special_price !== null && item.special_price !== 0 ? (
            <Typography className="cartSubTotal">
                {`${item && item.currency} ${
                    item && item.special_price && parseFloat(item.special_price * item.qty_ordered).toFixed(2)
                }`}
            </Typography>
        ) : parseFloat(item.price) !== 0 ? (
            <Typography className="cartSubTotal">
                {`${item && item.currency} ${
                    item && item.price && parseFloat(item.price * item.qty_ordered).toFixed(2)
                }`}
            </Typography>
        ) : (
            <Typography className="cartSubTotal">
                <FormattedMessage id="Cart.free" defaultMessage="Free" />
            </Typography>
        )}
    </>
);
