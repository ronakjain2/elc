import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Fade from '@material-ui/core/Fade';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import ChevronRight from '@material-ui/icons/ChevronRight';
import React, { lazy, Suspense, useEffect, useState, useRef } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { myBasketEvent } from 'services/GTMservice';
import { actions as familyClubActions } from '../../FamilyClub/redux/actions';
import './css/basket.css';
import { actions as basketActions } from './redux/actions';

const SimpleBreadcrumbs = lazy(() => import('commonComponet/Breadcrumbs'));
const Spinner = lazy(() => import('commonComponet/Spinner'));
const ItemDetails = lazy(() => import('./components/ItemDetails'));
const CartPaymentDetails = lazy(() => import('./components/CartDetails'));
const YouMayAlsoLike = lazy(() => import('./components/youMayAlsoLike'));

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '10px',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #0D943F',
        borderRadius: '15px',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        maxWidth: '300px',
    },
}));

export default function Basket({ history }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();
    const classes = useStyles();

    const [show, setShow] = useState(false);

    const global = state && state.auth;
    const language = state && state.auth && state.auth.language;
    const store_locale = state && state.auth && state.auth.store_locale;
    const country = state && state.auth && state.auth.country;
    const cartData = state && state.basket && state.basket.cartDetails;
    const cartProducts = cartData && cartData.products;
    const loginUser = state && state.auth && state.auth.loginUser;
    const is_fc_sku_bool = cartData && cartData.has_fcsku === 1;
    const fcLoader = state && state.familyClub && state.familyClub.familyClubLoader;
    const customer_type = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_type;
    const reservationLoader = state && state.auth && state.auth.reservationLoader;
    const fc_status = state && state.auth && state.auth.fc_status;

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: <FormattedMessage id="Cart.Breadcrum.ShoppingCart" defaultMessage="Shopping Cart" />,
        },
    ];

    const gotoHomePage = () => history.push(`/${store_locale}/`);

    const onJoinFamilyClub = () => {
        if (loginUser && loginUser.customer_id) {
            let payload = {
                quote_id: global.quote_id,
                store_id: global.currentStore,
                customer_id: loginUser.customer_id,
                from_basket: true,
            };
            dispatch(familyClubActions.setFamilyClubRequest(payload));
        } else {
            history.push(`/${global && global.store_locale}/sign-in-register`);
        }
    };

    useEffect(() => {
        try {
            dispatch(
                basketActions.getCartItemRequest({
                    quote_id: global.quote_id,
                    store_id: global.currentStore,
                    isBasket: true,
                }),
            );
        } catch (err) {}
    }, [dispatch, global.currentStore, global.quote_id]);

    useEffect(() => {
        try {
            if (cartData && cartData.products) {
                myBasketEvent(cartData);
            }
        } catch (err) {}
    }, [cartData]);

    const outOfStockStatus = cartData && cartData.outOfStockStatus;
    const loader = (state.basket && state.basket.loader) || fcLoader || reservationLoader;
    const outOfStockVisibled = useRef(false);
    useEffect(() => {
        if (outOfStockVisibled.current) return;
        if (outOfStockStatus && !loader && !outOfStockVisibled.current) {
            setShow(true);
            outOfStockVisibled.current = true;
        } else {
            setShow(false);
        }
    }, [loader, outOfStockStatus]);

    return (
        <Suspense fallback="">
            <Grid container justify="center" className="Cart">
                {state && state.basket && !state.basket.loader && !fcLoader && (
                    <Grid item xs={11}>
                        <Grid container>
                            {/** Bread crumbs */}
                            <Grid item xs={12} md={12}>
                                <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                            </Grid>

                            {state && state.basket && state.basket.cartDetails && (
                                <Grid item xs={12} className="textAlignCenter">
                                    <Typography className="title-cart">
                                        <FormattedMessage id="Cart.MyBasket" defaultMessage="My Basket" />
                                    </Typography>
                                </Grid>
                            )}
                            {cartProducts &&
                                cartProducts.length > 0 &&
                                (cartData.banner_image || cartData.mob_banner_image) && (
                                    <Grid
                                        item
                                        xs={12}
                                        md={8}
                                        className="cart-fc-banner d-flex justify-content-center flex-column align-items-center"
                                    >
                                        {cartData.banner_image && (
                                            <img
                                                alt="fc banner"
                                                className="d-none d-md-block w-100"
                                                src={cartData.banner_image}
                                            />
                                        )}
                                        {cartData.mob_banner_image && (
                                            <img
                                                alt="fc banner"
                                                className="d-block d-md-none w-100"
                                                src={cartData.mob_banner_image}
                                            />
                                        )}
                                        <Button
                                            className="continue-shopping-button"
                                            onClick={() => gotoHomePage()}
                                            startIcon={
                                                language === 'en' ? (
                                                    <ArrowBackIos />
                                                ) : (
                                                    <ChevronRight className="right-icon" />
                                                )
                                            }
                                        >
                                            <FormattedMessage
                                                id="Cart.ContinueShopping"
                                                defaultMessage="Continue Shopping"
                                            />
                                        </Button>
                                    </Grid>
                                )}
                            {cartProducts &&
                                cartProducts.length > 0 &&
                                fc_status &&
                                !is_fc_sku_bool &&
                                customer_type !== 'ecom_club_user' && (
                                    <Grid item xs={12} className="text-align-rtl">
                                        <FormControlLabel
                                            onClick={onJoinFamilyClub}
                                            control={<Checkbox checked={is_fc_sku_bool} />}
                                            label={
                                                <Typography className="family-club-info-text">
                                                    {country === 'UAE' ? (
                                                        <FormattedMessage
                                                            id="Cart.FamilyClubInfoTextOnCart"
                                                            defaultMessage="Join Family club now only for AED 99 to avail more discounts. This will be added to your cart"
                                                        />
                                                    ) : (
                                                        <FormattedMessage
                                                            id="Cart.FamilyClubInfoTextOnCartKSA"
                                                            defaultMessage="Join Family club now only for AED 99 to avail more discounts. This will be added to your cart"
                                                        />
                                                    )}
                                                </Typography>
                                            }
                                        />
                                    </Grid>
                                )}
                            {state && state.basket && state.basket.cartDetails && (
                                <Grid item xs={12} md={12}>
                                    <Grid container>
                                        <Grid item xs={12} md={8}>
                                            <ItemDetails />
                                        </Grid>
                                        <Grid item xs={12} md={4}>
                                            <CartPaymentDetails
                                                showFCbutton={
                                                    !is_fc_sku_bool && fc_status && customer_type !== 'ecom_club_user'
                                                }
                                                onJoinFamilyClub={onJoinFamilyClub}
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            )}

                            {state && state.basket && !state.basket.cartDetails && (
                                <Grid container justify="center">
                                    <Typography className="title-empty">
                                        <FormattedMessage id="Cart.empty" defaultMessage="Your basket is empty" />
                                    </Typography>
                                </Grid>
                            )}
                        </Grid>
                    </Grid>
                )}
                {state && state.basket && !fcLoader && !state.basket.loader && state.basket.cartDetails && (
                    <Grid item xs={12}>
                        <YouMayAlsoLike />
                    </Grid>
                )}
                {((state && state.basket && state.basket.loader) || fcLoader) && (
                    <Grid container alignItems="center">
                        <Spinner />
                    </Grid>
                )}
            </Grid>
            <Modal
                className={classes.modal}
                open={show}
                onClose={() => setShow(false)}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={show}>
                    <Grid className={classes.paper}>
                        <Grid item xs={12} className="mb-4 text-align-rtl">
                            <Typography>
                                <FormattedMessage
                                    id="Cart.OutOfStockPopupText"
                                    defaultMessage="Some of the items in your cart are no longer available. Please remove them to proceed to checkout"
                                />
                            </Typography>
                        </Grid>
                        <Grid container direction="row" justify="center" alignItems="center">
                            <Button onClick={() => setShow(false)} className="button">
                                <FormattedMessage id="cart.Ok" defaultMessage="Ok" />
                            </Button>
                        </Grid>
                    </Grid>
                </Fade>
            </Modal>
        </Suspense>
    );
}
