import { actions as checkoutDeliveryActions } from 'components/Cart/Checkout/Delivery/redux/actions';
import { actions as paymentActions } from 'components/Cart/Checkout/Payment/redux/actions';
import { actions as authActions } from 'components/Global/redux/actions';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { RemoveProductCart } from 'services/GTMservice';
import api from '../api';
import { checkAllItemOutOfStock } from '../utils';
import { actions, types } from './actions';

const auth = (state) => state && state.auth;

const getCartDetailsReq = function* getCartDetailsReq({ payload }) {
    try {
        const { isBasket } = payload;
        if (!payload.clearDeliveryObj) {
            yield put(checkoutDeliveryActions.saveOrClearDeliveryDetails({}));
        }
        const authData = yield select(auth);
        if (payload && payload.quote_id) {
            const { data } = yield call(api.getCart, payload);
            if (data && data.status) {
                //Removing Reservation Time when cart empty
                if (!data.data) {
                    yield put(authActions.saveReservationStartTime(null));
                }

                if (!data.data && !isBasket) {
                    yield put(push(`/${authData && authData.store_locale}/cart`));
                }

                yield put(actions.saveCartCountRequest((data.data && data.data.cart_count) || 0));
                const products = data && data.data && data.data.products;
                if (data.data && products && products.length > 0) {
                    if (checkAllItemOutOfStock(products)) {
                        data.data.outOfStockStatus = true;
                    } else {
                        data.data.outOfStockStatus = false;
                    }
                }
                yield put(actions.getCartItemRequestSuccess(data.data));
                if (!payload.isPayment) {
                    yield put(paymentActions.getCheckoutPaymentsRequestSuccess({ payment: {}, cartData: null }));
                }
            } else {
                //toast.error((data && data.message) || 'Something Went to wrong, Please try after sometime');
                yield put(actions.getCartItemRequestFailed());
                yield put(actions.saveCartCountRequest(0));
            }
        } else {
            yield put(actions.getCartItemRequestSuccess(null));
        }
    } catch (err) {
        toast.error('Something Went to wrong, Please try after sometime');
        yield put(actions.getCartItemRequestFailed());
        yield put(actions.saveCartCountRequest(0));
    }
};

const updateCartQtyReq = function* updateCartQtyReq({ payload }) {
    try {
        const { quote_id, store_id } = payload;
        const { data } = yield call(api.updateCartQty, payload);
        if (data && data.status) {
            yield put(
                actions.getCartItemRequest({
                    quote_id,
                    store_id,
                }),
            );
            toast.success(data.message);
        } else {
            toast.error((data && data.message) || 'Something Went to wrong, Please try after sometime');
            yield put(actions.updateCartItemQtyRequestFailed());
        }
    } catch (err) {
        toast.error('Something Went to wrong, Please try after sometime');
        yield put(actions.updateCartItemQtyRequestFailed());
    }
};

const removeCartItemReq = function* removeCartItemReq({ payload }) {
    try {
        const {
            quote_id,
            store_id,
            sku,
            price,
            special_price,
            qty,
            name,
            category_names,
            requestFromWishListRemoveCart,
        } = payload;
        const { data } = yield call(api.removeCartItem, { quote_id, store_id, sku });
        if (data && data.status) {
            yield put(
                actions.getCartItemRequest({
                    quote_id,
                    store_id,
                }),
            );

            RemoveProductCart({
                sku,
                price,
                special_price,
                qty,
                name,
                category_names,
            });
            !requestFromWishListRemoveCart && toast.success(data.message);
        } else {
            toast.error((data && data.message) || 'Something Went to wrong, Please try after sometime');
            yield put(actions.removeCartItemRequestFailed());
        }
    } catch (err) {
        toast.error('Something Went to wrong, Please try after sometime');
        yield put(actions.removeCartItemRequestFailed());
    }
};

const getCartCountReq = function* getCartCountReq({ payload }) {
    try {
        if (payload.quote_id) {
            const { data } = yield call(api.getCartCount, payload);
            if (data && data.status) {
                yield put(actions.saveCartCountRequest(data.count));
            }
        }
    } catch (err) {
        toast.error('Something Went to wrong, Please try after sometime');
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_CART_ITEM_REQUEST, getCartDetailsReq);
    yield takeLatest(types.UPDATE_CART_ITEM_QTY_REQUEST, updateCartQtyReq);
    yield takeLatest(types.REMOVE_CART_ITEM_REQUEST, removeCartItemReq);
    yield takeLatest(types.GET_CART_COUNT_REQUEST, getCartCountReq);
}
