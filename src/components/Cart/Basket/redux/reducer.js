import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_CART_ITEM_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_CART_ITEM_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
        cartDetails: null,
    }),
    [types.GET_CART_ITEM_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        cartDetails: payload,
    }),

    [types.UPDATE_CART_ITEM_QTY_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.UPDATE_CART_ITEM_QTY_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),

    [types.REMOVE_CART_ITEM_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.REMOVE_CART_ITEM_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),

    [types.SAVE_CART_COUNT_REQUEST]: (state, { payload }) => ({
        ...state,
        cartCount: payload,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    cartDetails: {},
    cartCount: 0,
});
