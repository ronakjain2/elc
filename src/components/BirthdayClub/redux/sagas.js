import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../api';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';

const setBirthdayClubReq = function* setBirthdayClubReq({ payload }) {
    try {
        const { data } = yield call(api.setBirthdayClub, payload);
        yield put(actions.birthdayClubRequestSuccess(data));
        if (data && data.status) {
            toast.success(data && data.message);
            yield put(push(`?type=true`));
        } else {
            toast.error((data && data.message) || 'Something went to wrong');
        }
    } catch (err) {
        toast.error('Something went to wrong');
        yield put(actions.birthdayClubRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.SET_BIRTHDAY_CLUB_REQUEST, setBirthdayClubReq);
}
