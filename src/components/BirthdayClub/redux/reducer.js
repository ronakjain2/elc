import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.SET_BIRTHDAY_CLUB_REQUEST]: (state) => ({
        birthDayClubLoader: true,
    }),
    [types.SET_BIRTHDAY_CLUB_SUCCESS]: (state) => ({
        birthDayClubLoader: false,
    }),
    [types.SET_BIRTHDAY_CLUB_FAILURE]: (state) => ({
        birthDayClubLoader: false,
    }),
};

export default handleActions(actionHandler, {
    birthDayClubLoader: false,
});
