import { createAction } from 'redux-actions';

//actions types

const SET_BIRTHDAY_CLUB_REQUEST = 'ELC@OROB/SET_BIRTHDAY_CLUB_REQUEST';
const SET_BIRTHDAY_CLUB_SUCCESS = 'ELC@OROB/SET_BIRTHDAY_CLUB_SUCCESS';
const SET_BIRTHDAY_CLUB_FAILURE = 'ELC@OROB/SET_BIRTHDAY_CLUB_FAILURE';

const setBirthdayClubRequest = createAction(SET_BIRTHDAY_CLUB_REQUEST);
const birthdayClubRequestSuccess = createAction(SET_BIRTHDAY_CLUB_SUCCESS);
const birthdayClubRequestFailed = createAction(SET_BIRTHDAY_CLUB_FAILURE);

//actions
export const actions = {
    setBirthdayClubRequest,
    birthdayClubRequestSuccess,
    birthdayClubRequestFailed,
};

//types
export const types = {
    SET_BIRTHDAY_CLUB_REQUEST,
    SET_BIRTHDAY_CLUB_SUCCESS,
    SET_BIRTHDAY_CLUB_FAILURE,
};
