import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import './birthday_club.css';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import { FormattedMessage } from 'react-intl';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Close from '@material-ui/icons/Close';
import SimpleBreadcrumbs from 'commonComponet/Breadcrumbs';
import { actions as birthClubActions } from './redux/actions';

export default function Home() {
    const dispatch = useDispatch();

    const state = useSelector((state) => state);
    const global = state && state.auth;

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: <FormattedMessage id="birthdayclub.header" defaultMessage="Birthday Club" />,
        },
    ];

    const [values, setValues] = useState({
        children: [
            {
                childName: '',
                gender: '',
                DOB: '',
            },
        ],
        isFillAllForm: '',
    });

    const children = [
        ...((values && values.children) || [
            {
                childName: '',
                gender: '',
                DOB: '',
            },
        ]),
    ];

    const onChangeHandler = (name, index) => (event) => {
        children[index][name] = event.target.value;
        setValues({ ...values, children: children });
    };

    const isAllChildValid = () => {
        let valid = true;
        children.forEach((child) => {
            if (child.childName === '' || child.gender === '' || child.DOB === '') {
                valid = false;
            }
        });

        return valid;
    };

    const getMoreInfo = () => {
        let moreinfo = '';
        children.forEach((child) => {
            if (moreinfo === '') {
                moreinfo = child.childName + ':' + child.gender + ':' + getDateToString(child.DOB) + ';';
            } else {
                moreinfo = moreinfo + child.childName + ':' + child.gender + ':' + getDateToString(child.DOB) + ';';
            }
        });

        return moreinfo;
    };

    const getDateToString = (DOB) => {
        let splitedData = DOB.split('-');
        return splitedData[2] + splitedData[1] + splitedData[0];
    };

    const addChildren = () => {
        if (isAllChildValid()) {
            children.push({
                childName: '',
                gender: '',
                DOB: '',
            });
            setValues({ ...values, children: children, isFillAllForm: true });
        } else {
            setValues({ ...values, isFillAllForm: false });
        }
    };

    const closeMsg = () => {
        setValues({ ...values, isFillAllForm: true });
    };

    const removeChild = (index) => {
        children.splice(index, 1);
        setValues({ ...values, children: children, isFillAllForm: true });
    };

    const register = () => {
        if (isAllChildValid()) {
            let payload = {
                countryCode: state.auth.loginUser.carrier_code,
                email: state.auth.loginUser.email,
                firstname: state.auth.loginUser.firstname,
                language: state.auth.language,
                lastname: state.auth.loginUser.lastname,
                moreinfo: getMoreInfo(),
                phoneNumber: state.auth.loginUser.phone_number,
                storeid: state.auth.currentStore,
            };

            dispatch(birthClubActions.setBirthdayClubRequest(payload));
            setValues({
                ...values,
                children: [
                    {
                        childName: '',
                        gender: '',
                        DOB: '',
                    },
                ],
                isFillAllForm: '',
            });
        } else {
            setValues({ ...values, isFillAllForm: false });
        }
    };

    return (
        <Grid container>
            <Grid item xs={11} className="bredcrub-bclub">
                <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            </Grid>

            <Grid item xs={11} md={5} className="">
                {state && state.auth && state.auth.language === 'ar' && (
                    <img src="/images/birthdayClub/BirthdayClub_AR.jpg" alt="" className="badyCloubImage" />
                )}
                {state && state.auth && state.auth.language === 'en' && (
                    <img src="/images/birthdayClub/BirthdayClub.jpg" alt="" className="badyCloubImage" />
                )}
            </Grid>

            <Grid item xs={12} md={7} container className="birthdayForm align-self-start">
                {values.isFillAllForm === false && (
                    <Grid container className="errorMsg">
                        <Grid item xs={1} md={4}></Grid>

                        <Grid style={{ textAlign: 'start' }} item xs={10} md={6}>
                            <FormattedMessage
                                id="birthdayClub.formRequiredMsg"
                                defaultMessage="Please fill all * fields compulsory"
                            />
                            <Close className="close-icon-sort crossSmall" onClick={() => closeMsg()} />
                        </Grid>
                    </Grid>
                )}
                {values.children &&
                    values.children.map((child, index) => {
                        return (
                            <Grid container item xs={12} className="mt-3">
                                <div className="col-12 col-md-4">
                                    <Typography className="login-sigup-input-text">
                                        <span className="colorRed">*&nbsp;</span>
                                        <FormattedMessage id="bithdayclub.ChildName" defaultMessage="Child Name" />
                                    </Typography>
                                    <TextField
                                        value={child.childName}
                                        onChange={onChangeHandler('childName', index)}
                                        variant="outlined"
                                        fullWidth
                                        className="inputBox"
                                    />
                                </div>
                                <div className="col-12 col-md-4">
                                    <Typography className="login-sigup-input-text">
                                        <span className="colorRed">*&nbsp;</span>
                                        <FormattedMessage id="bithdayclub.Gender" defaultMessage="Gender" />
                                    </Typography>
                                    <TextField
                                        value={child.gender}
                                        onChange={onChangeHandler('gender', index)}
                                        select
                                        variant="outlined"
                                        fullWidth
                                        className="inputBox"
                                    >
                                        <MenuItem value="Male">
                                            <span className="boldText">
                                                <FormattedMessage id="bithdayclub.Male" defaultMessage="Male" />
                                            </span>
                                        </MenuItem>
                                        <MenuItem value="Female">
                                            <span className="boldText">
                                                <FormattedMessage id="bithdayclub.Female" defaultMessage="Female" />
                                            </span>
                                        </MenuItem>
                                        <MenuItem value="Other">
                                            <span className="boldText">
                                                <FormattedMessage id="bithdayclub.Other" defaultMessage="Other" />
                                            </span>
                                        </MenuItem>
                                    </TextField>
                                </div>
                                <div className="col-12 col-md-4">
                                    <Typography className="login-sigup-input-text">
                                        <span className="colorRed">*&nbsp;</span>
                                        <FormattedMessage id="bithdayclub.DOB" defaultMessage="DOB" />
                                    </Typography>
                                    <Input
                                        value={child.DOB}
                                        InputProps={{ inputProps: { max: '2020-05-04' } }}
                                        onChange={onChangeHandler('DOB', index)}
                                        id="date"
                                        type="date"
                                        placeholder="dd-mm-yyyy"
                                        className="datePicker"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                </div>
                                {index !== 0 && <Close className="remove-child" onClick={() => removeChild(index)} />}
                            </Grid>
                        );
                    })}
                <Grid container className="px-1">
                    <Grid item xs={12} className="textAlignCenter">
                        <Button className="addChildrenButton m-t-20 " onClick={() => addChildren()}>
                            <FormattedMessage id="birthdayClub.AddChildren" defaultMessage="Add Children" />
                        </Button>
                    </Grid>

                    <Grid item xs={12} className="textAlignCenter">
                        <Button className="birthday-club-register" onClick={() => register()}>
                            <FormattedMessage id="SignInSignUp.Register" defaultMessage="Register" />
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
