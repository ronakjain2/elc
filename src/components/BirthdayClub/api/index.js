import api from 'apiServices';

const setBirthdayClub = (payload) => api.post('/index.php/rest/V1/app/birthdayclub', payload);

export default {
    setBirthdayClub,
};
