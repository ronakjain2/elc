import api from 'apiServices';

const getHomepage = (store_id) => api.get(`/index.php/rest/V1/app/home?store=${store_id}`);
const getBestSellerCache = (store_id) =>
    api.get(`/index.php/rest/V1/app/orob/cache/productsbycategory?category_id=188%20&store_id=${store_id}&count=6`);
const getBestSellerNoCache = (payload) => api.post('/index.php/rest/V1/app/mkt/no-cache/productsbycategory', payload);
const getSliderProductsCache = (payload) =>
    api.post(`/index.php/rest/V1/app/cache/home?store=${payload.store_id}`, payload);
const getSliderProductsNoCache = (payload) =>
    api.post(`/index.php/rest/V1/app/no-cache/home?store=${payload.store_id}`, payload);

export default {
    getHomepage,
    getBestSellerCache,
    getBestSellerNoCache,
    getSliderProductsCache,
    getSliderProductsNoCache,
};
