import Grid from '@material-ui/core/Grid';
import FCStatusBar from 'commonComponet/FCStatusBar';
import Spinner from 'commonComponet/Spinner';
import React, { lazy, Suspense, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import './css/home.css';
import { actions as homePageActions } from './redux/actions';

const ProductsSlider = lazy(() => import('./components/ProductsSlider'));
const WhatAreLookingFor = lazy(() => import('./components/WhatAreLookingFor'));
const HomeBanner = lazy(() => import('./components/HomeBanner'));
const GiftFinder = lazy(() => import('./components/GiftFinder/index'));

export default function Home() {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const recentCategories = state && state.auth && state.auth.recentCategories;
    const store_id = state && state.auth && state.auth.currentStore;

    useEffect(() => {
        try {
            const payload = {
                rec_cat_id: recentCategories.join(','),
                best_cat_id: '188',
                count: '8',
                store_id,
            };
            dispatch(homePageActions.getHomePageRequest({ store_id }));
            dispatch(homePageActions.getSliderProductsRequest(payload));
        } catch (err) {}
    }, [dispatch, store_id, recentCategories]);

    const homeDetails = state && state.homePage && state.homePage.homeDetails;

    const bestSellerProducts = state && state.homePage && state.homePage.bestSellerProduct;
    // const recentProduct = state && state.homePage && state.homePage.recentProduct;
    const exclusiveProduct = state && state.homePage && state.homePage.exclusiveProduct;
    const elcTreatProducts = state && state.homePage && state.homePage.elcTreatProducts;

    return (
        <Grid container>
            {state && state.homePage && state.homePage.homeloader && (
                <Grid container alignItems="center">
                    <Spinner />
                </Grid>
            )}
            {state && state.homePage && !state.homePage.homeloader && (
                <>
                    <FCStatusBar />
                    <Suspense
                        fallback={
                            <Grid container justify="center">
                                Loading....
                            </Grid>
                        }
                    >
                        <HomeBanner homeDetails={homeDetails} />
                    </Suspense>

                    <Suspense
                        fallback={
                            <Grid container justify="center">
                                Loading....
                            </Grid>
                        }
                    >
                        <ProductsSlider
                            title={homeDetails && homeDetails.titleCarousel_1}
                            products={elcTreatProducts}
                            showButton={homeDetails && homeDetails.showButtonCarousel_1}
                            buttonLabel={homeDetails && homeDetails.buttonLabelCarousel_1}
                            buttonRedirection={homeDetails && homeDetails.buttonRedirectionCarousel_1}
                        />
                    </Suspense>

                    <Suspense
                        fallback={
                            <Grid container justify="center">
                                Loading....
                            </Grid>
                        }
                    >
                        <WhatAreLookingFor
                            title={homeDetails && homeDetails.offersLabel}
                            data={homeDetails && homeDetails.blocks}
                            data2={homeDetails && homeDetails.block2}
                        />
                    </Suspense>

                    <Suspense
                        fallback={
                            <Grid container justify="center">
                                Loading....
                            </Grid>
                        }
                    >
                        <ProductsSlider
                            title={homeDetails && homeDetails.titleCarousel_2}
                            darkTitle
                            products={exclusiveProduct}
                            showButton={homeDetails && homeDetails.showButtonCarousel_2}
                            buttonLabel={homeDetails && homeDetails.buttonLabelCarousel_2}
                            buttonRedirection={homeDetails && homeDetails.buttonRedirectionCarousel_2}
                        />
                    </Suspense>

                    <Suspense
                        fallback={
                            <Grid container justify="center">
                                Loading....
                            </Grid>
                        }
                    >
                        <GiftFinder
                            title={<FormattedMessage id="Home.GiftFinder" defaultMessage="Gift Finder" />}
                            data={homeDetails && homeDetails.presentfinder}
                        />
                    </Suspense>

                    <Suspense
                        fallback={
                            <Grid container justify="center">
                                Loading....
                            </Grid>
                        }
                    >
                        <ProductsSlider
                            title={homeDetails && homeDetails.titleCarousel_3}
                            products={bestSellerProducts}
                            showButton={homeDetails && homeDetails.showButtonCarousel_3}
                            buttonLabel={homeDetails && homeDetails.buttonLabelCarousel_3}
                            buttonRedirection={homeDetails && homeDetails.buttonRedirectionCarousel_3}
                        />
                    </Suspense>
                </>
            )}
        </Grid>
    );
}
