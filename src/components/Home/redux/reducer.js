import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionsHandler = {
    [types.GET_HOME_PAGE_REQUEST]: state => ({
        ...state,
        homeloader: true,
    }),
    [types.GET_HOME_PAGE_REQUEST_FAILED]: state => ({
        ...state,
        homeloader: false,
        homeDetails: {},
    }),
    [types.GET_HOME_PAGE_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        homeloader: false,
        homeDetails: payload && payload.data,
    }),

    [types.GET_BEST_SELLER_PRODUCT_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        bestSellerProduct: payload || [],
    }),

    [types.GET_BEST_SELLER_PRODUCT_REQUEST_FAILED]: state => ({
        ...state,
        bestSellerProduct: [],
    }),

    [types.GET_BEST_SELLER_PRODUCT_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        bestSellerProduct: payload.bestSellerProduct || null,
        recentProduct: payload.recentProduct || null,
        exclusiveProduct: payload.exclusiveProduct || null,
        elcTreatProducts: payload.elcTreatProducts || null,
    }),

    [types.GET_BEST_SELLER_PRODUCT_REQUEST_FAILED]: state => ({
        ...state,
        bestSellerProduct: null,
        recentProduct: null,
        exclusiveProduct: null,
        elcTreatProducts: null,
    }),
};

export default handleActions(actionsHandler, {
    homeloader: false,
    homeDetails: null,
});
