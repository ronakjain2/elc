import { createAction } from 'redux-actions';

// Type Name
const GET_HOME_PAGE_REQUEST = 'ELC@OROB/GET_HOME_PAGE_REQUEST';
const GET_HOME_PAGE_REQUEST_SUCCESS = 'ELC@OROB/GET_HOME_PAGE_REQUEST_SUCCESS';
const GET_HOME_PAGE_REQUEST_FAILED = 'ELC@OROB/GET_HOME_PAGE_REQUEST_FAILED';

const GET_BEST_SELLER_PRODUCT_REQUEST = 'ELC@OROB/GET_BEST_SELLER_PRODUCT_REQUEST';
const GET_BEST_SELLER_PRODUCT_REQUEST_SUCCESS = 'ELC@OROB/GET_BEST_SELLER_PRODUCT_REQUEST_SUCCESS';
const GET_BEST_SELLER_PRODUCT_REQUEST_FAILED = 'ELC@OROB/GET_BEST_SELLER_PRODUCT_REQUEST_FAILED';

const GET_SLIDER_PRODUCTS_REQUEST = 'ELC@OROB/GET_SLIDER_PRODUCTS_REQUEST';
const GET_SLIDER_PRODUCTS_REQUEST_SUCCESS = 'ELC@OROB/GET_BEST_SELSLIDER_PRODUCTS_SUCCESS';
const GET_SLIDER_PRODUCTS_REQUEST_FAILED = 'ELC@OROB/GET_BEST_SELSLIDER_PRODUCTS_FAILED';

// Action Method
const getHomePageRequest = createAction(GET_HOME_PAGE_REQUEST);
const getHomePageRequestSuccess = createAction(GET_HOME_PAGE_REQUEST_SUCCESS);
const getHomePageRequestFailed = createAction(GET_HOME_PAGE_REQUEST_FAILED);

const getBestSellerProductRequest = createAction(GET_BEST_SELLER_PRODUCT_REQUEST);
const getBestSellerProductRequestSuccess = createAction(GET_BEST_SELLER_PRODUCT_REQUEST_SUCCESS);
const getBestSellerProductRequestFailed = createAction(GET_BEST_SELLER_PRODUCT_REQUEST_FAILED);

const getSliderProductsRequest = createAction(GET_SLIDER_PRODUCTS_REQUEST);
const getSliderProductsRequestSuccess = createAction(GET_SLIDER_PRODUCTS_REQUEST_SUCCESS);
const getSliderProductsRequestFailed = createAction(GET_SLIDER_PRODUCTS_REQUEST_FAILED);

export const actions = {
    getHomePageRequest,
    getHomePageRequestSuccess,
    getHomePageRequestFailed,

    getBestSellerProductRequest,
    getBestSellerProductRequestSuccess,
    getBestSellerProductRequestFailed,

    getSliderProductsRequest,
    getSliderProductsRequestSuccess,
    getSliderProductsRequestFailed,
};

export const types = {
    GET_HOME_PAGE_REQUEST,
    GET_HOME_PAGE_REQUEST_SUCCESS,
    GET_HOME_PAGE_REQUEST_FAILED,

    GET_BEST_SELLER_PRODUCT_REQUEST,
    GET_BEST_SELLER_PRODUCT_REQUEST_SUCCESS,
    GET_BEST_SELLER_PRODUCT_REQUEST_FAILED,

    GET_SLIDER_PRODUCTS_REQUEST,
    GET_SLIDER_PRODUCTS_REQUEST_SUCCESS,
    GET_SLIDER_PRODUCTS_REQUEST_FAILED,
};
