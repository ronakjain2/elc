import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../api';
import { actions, types } from './actions';

const getHomepageReq = function* getHomepageReq({ payload }) {
    try {
        const { store_id } = payload;
        const { data } = yield call(api.getHomepage, store_id);

        if (data && data.status) {
            yield put(actions.getHomePageRequestSuccess(data));
        } else {
            yield put(actions.getHomePageRequestFailed());
        }
    } catch (err) {
        yield put(actions.getHomePageRequestFailed());
    }
};

const getBestSellerProductReq = function* getBestSellerProductReq({ payload }) {
    try {
        const { store_id } = payload;
        const { data } = yield call(api.getBestSellerCache, store_id);

        const response = yield call(api.getBestSellerNoCache, {
            category_id: '188',
            store_id,
            count: '6',
        });

        if (data && data.status) {
            const cacheProducts = [...(data && data.product)] || [];
            for (const p in cacheProducts) {
                if (response && response.data && response.data.product && response.data.product[p]) {
                    cacheProducts[p] = Object.assign(cacheProducts[p], response.data.product[p]);
                }
            }
            yield put(actions.getBestSellerProductRequestSuccess(cacheProducts));
        } else {
            yield put(actions.getBestSellerProductRequestFailed());
        }
    } catch (err) {
        yield put(actions.getBestSellerProductRequestFailed());
    }
};

const mergeCacheNocacheData = (cachedData, noCacheData) => {
    return Object.assign(cachedData, noCacheData);
};

const getSlidersProductReq = function* getSlidersProductReq({ payload }) {
    try {
        const { data } = yield call(api.getSliderProductsCache, payload);

        const response = yield call(api.getSliderProductsNoCache, payload);

        if (data && data.status && response && response.status && response.data.data) {
            let bestSellerCacheProducts = (data && data.data && data.data.best_seller_product) || [];
            bestSellerCacheProducts = bestSellerCacheProducts.map((cachedData, index) => {
                const noCacheData = response.data.data.best_seller_product[index];
                return mergeCacheNocacheData(cachedData, noCacheData);
            });

            let recentCacheProducts = (data && data.data && data.data.rec_view_product) || [];
            recentCacheProducts = recentCacheProducts.map((cachedData, index) => {
                const noCacheData = response.data.data.rec_view_product[index];
                return mergeCacheNocacheData(cachedData, noCacheData);
            });

            let exclusiveCacheProducts = (data && data.data && data.data.exclusive_product) || [];
            exclusiveCacheProducts = exclusiveCacheProducts.map((cachedData, index) => {
                const noCacheData = response.data.data.exclusive_product[index];
                return mergeCacheNocacheData(cachedData, noCacheData);
            });

            let elcTreatProducts = (data && data.data && data.data.elc_treat) || [];
            elcTreatProducts = elcTreatProducts.map((cachedData, index) => {
                const noCacheData = response.data.data.elc_treat[index];
                return mergeCacheNocacheData(cachedData, noCacheData);
            });

            yield put(
                actions.getBestSellerProductRequestSuccess({
                    bestSellerProduct: bestSellerCacheProducts,
                    recentProduct: recentCacheProducts,
                    exclusiveProduct: exclusiveCacheProducts,
                    elcTreatProducts: elcTreatProducts,
                }),
            );
        } else {
            yield put(actions.getBestSellerProductRequestFailed());
        }
    } catch (err) {
        yield put(actions.getBestSellerProductRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_HOME_PAGE_REQUEST, getHomepageReq);
    yield takeLatest(types.GET_BEST_SELLER_PRODUCT_REQUEST, getBestSellerProductReq);
    yield takeLatest(types.GET_SLIDER_PRODUCTS_REQUEST, getSlidersProductReq);
}
