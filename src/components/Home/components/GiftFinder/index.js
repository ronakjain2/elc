import { Grid, Hidden } from '@material-ui/core';
import React, { memo, useState } from 'react';
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AgeSelector from './components/AgeSelector';
import Banner from './components/Banner';
import SelectPrice from './components/SelectPrice';

const GiftFinderPages = ({ history, title, data }) => {
    const state = useSelector(state => state);

    const store_locale = state && state.auth && state.auth.store_locale;

    const [step, setStep] = useState(1);

    const nextStep = () => setStep(step + 1);

    const prevStep = () => setStep(step - 1);

    const [selectedCategory, setselectedCategory] = useState(null);

    if (!data || !data.TITLE) return null;

    const onClickShowToys = price => {
        if (price && selectedCategory) {
            history.push(
                `/${store_locale}/products/${selectedCategory.url_path}?minPrice=${price[0]}&maxPrice=${price[1]}`,
            );
        }
    };

    return (
        <Grid container>
            <TitleLabel title={title} />
            {step === 1 && (
                <Banner
                    nextStep={nextStep}
                    BLOCK_BANNER={data.BLOCK_BANNER}
                    BLOCK_MOBILE_BANNER={data.BLOCK_MOBILE_BANNER}
                    TITLE={data.TITLE}
                    TITLE_COLOUR={data.TITLE_COLOUR}
                    BTN_TEXT={data.BTN_TEXT}
                    BTN_TEXT_COLOUR={data.BTN_TEXT_COLOUR}
                    BG_COLOUR={data.BG_COLOUR}
                    BTN_TEXT_COLOUR_HOVER={data.BTN_TEXT_COLOUR_HOVER}
                    BG_COLOUR_HOVER={data.BG_COLOUR_HOVER}
                />
            )}
            {step === 2 && (
                <AgeSelector
                    nextStep={nextStep}
                    prevStep={prevStep}
                    categories={data.categories}
                    setselectedCategory={setselectedCategory}
                />
            )}
            {step === 3 && (
                <SelectPrice
                    prevStep={prevStep}
                    selectedCategory={selectedCategory}
                    onClickShowToys={onClickShowToys}
                />
            )}
        </Grid>
    );
};

// Inner Component
const TitleLabel = ({ title }) => {
    return (
        <Grid container justify="center" className="slider-title">
            <Hidden only={['lg', 'xl']}>
                {title && (
                    <Grid item xs={11} className="title flex alignItemCenter">
                        {/* <span className="line" /> */}
                        <label>{title}</label>
                        {/* <span className="line" /> */}
                    </Grid>
                )}
            </Hidden>
        </Grid>
    );
};

export default withRouter(memo(GiftFinderPages));
