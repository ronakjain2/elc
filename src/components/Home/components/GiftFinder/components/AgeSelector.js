import React from 'react';
import { Grid, Typography, IconButton, Button } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import '../../GiftFinder/css/AgeSelector.css';

const AgeSelector = ({ nextStep, prevStep, categories, setselectedCategory }) => {
    const prevBtnClick = () => prevStep();
    const onAgeSelect = (item) => {
        setselectedCategory(item);
        nextStep();
    };

    return (
        <Grid item xs={12} className="age-selector">
            <Grid container justify="center" align="center">
                <Grid container className="age-selectorTop">
                    <IconButton size="small" onClick={prevBtnClick} className="ml-4">
                        <ArrowBackIosIcon fontSize="inherit" />
                    </IconButton>
                    <Typography className="age-selectorHeader">
                        <FormattedMessage id="GiftFinder.SelectAge" defaultMessage="Select Age" />
                    </Typography>
                    <Grid className="mr-4">&nbsp;</Grid>
                </Grid>
                <Grid container className="age-selectorBox">
                    {categories.map((item, index) => (
                        <Grid key={index} container className="rounded-box">
                            <Button onClick={() => onAgeSelect(item)} className="rounded-boxBtn">
                                <Grid item>
                                    <Typography className="rounded-boxAge">{item.name}</Typography>
                                </Grid>
                            </Button>
                        </Grid>
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default AgeSelector;
