import { Button, Grid, IconButton, Slider, Typography } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import '../../GiftFinder/css/SelectPrice.css';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const SelectPrice = ({ prevStep, selectedCategory, onClickShowToys }) => {
    const [price, setprice] = useState([parseInt(selectedCategory.minPrice), parseInt(selectedCategory.maxPrice)]);
    const [selected, selectMaxPrice] = useState(parseInt(parseInt(selectedCategory.maxPrice) / 2));

    const state = useSelector((state) => state);

    const country = state && state.auth && state.auth.country;
    const language = state && state.auth && state.auth.language;

    const marks = [
        {
            value: parseInt(selectedCategory.minPrice),
            label: `${selectedCategory.minPrice} ${country === 'UAE' ? 'AED' : 'SAR'}`,
        },
        {
            value: parseInt(selectedCategory.maxPrice),
            label: `${selectedCategory.maxPrice} ${country === 'UAE' ? 'AED' : 'SAR'}`,
        },
    ];

    const valuetext = (value) => {
        return `${value} ${country === 'UAE' ? 'AED' : 'SAR'}`;
    };

    const prevBtnClick = () => prevStep();

    const handleChange = (event, newValue) => {
        selectMaxPrice(newValue);
        setprice([parseInt(selectedCategory.minPrice), newValue]);
    };

    const theme = createMuiTheme({
        direction: language === 'ar' ? 'rtl' : 'initial',
    });

    return (
        <Grid justify="center" className="select-price">
            <Grid container className="select-priceTop">
                <IconButton size="small" onClick={prevBtnClick} className="ml-4">
                    <ArrowBackIosIcon fontSize="inherit" />
                </IconButton>
                <Typography className="select-priceHeader">
                    <FormattedMessage id="GiftFinder.SelectPrice" defaultMessage="Select Price" />
                </Typography>
                <Grid className="mr-4">&nbsp;</Grid>
            </Grid>
            <ThemeProvider theme={theme}>
                <Grid align="center" className="mt-5 slider-box">
                    <Slider
                        className="select-priceSlider"
                        value={selected}
                        step={1}
                        getAriaValueText={valuetext}
                        onChange={handleChange}
                        min={parseInt(selectedCategory.minPrice)}
                        max={parseInt(selectedCategory.maxPrice)}
                        marks={marks}
                        valueLabelDisplay="on"
                        aria-labelledby="non-linear-slider"
                    />
                    <Typography textAlign="center" className="select-priceRangeText">
                        <FormattedMessage id="GiftFinder.PriceRange" defaultMessage="Price Range" />
                    </Typography>
                </Grid>
            </ThemeProvider>

            <Grid className="select-priceRangeBox">
                <Grid item className="select-priceText">
                    <Typography>
                        <FormattedMessage id="GiftFinder.SelectedRange" defaultMessage="Selected Range:" />
                    </Typography>
                </Grid>
                <Grid item className="select-priceRange">
                    <Typography>
                        {parseInt(selectedCategory.minPrice)} &nbsp;
                        <FormattedMessage id="GiftFinder.To" defaultMessage="to" />
                        &nbsp;
                        {selected} &nbsp;{country === 'UAE' ? 'AED' : 'SAR'}
                    </Typography>
                </Grid>
            </Grid>
            <Grid className="select-priceBtnBox">
                <Button className="select-priceBtn" onClick={() => onClickShowToys(price)}>
                    <FormattedMessage id="GiftFinder.ShowToys" defaultMessage="Show Toys" />
                </Button>
            </Grid>
        </Grid>
    );
};

export default SelectPrice;
