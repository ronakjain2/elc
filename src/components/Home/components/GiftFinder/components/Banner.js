import { Box, Button, Grid, Hidden } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Img from 'commonComponet/Image';
import React from 'react';
import { Link } from 'react-router-dom';
import '../../GiftFinder/css/Banner.css';

const Banner = ({
    nextStep,
    BLOCK_BANNER,
    BLOCK_MOBILE_BANNER,
    TITLE,
    TITLE_COLOUR,
    BTN_TEXT,
    BTN_TEXT_COLOUR = '#FFF',
    BG_COLOUR = '#0d943f',
    BTN_TEXT_COLOUR_HOVER = '#FFF',
    BG_COLOUR_HOVER = '#48c075',
}) => {
    const nextBtnClick = e => {
        nextStep();
    };
    const titleStyle = {
        color: TITLE_COLOUR,
    };
    const useStyles = makeStyles(theme => ({
        btnStyle: {
            backgroundColor: BG_COLOUR,
            color: BTN_TEXT_COLOUR,

            '&:hover': {
                backgroundColor: BG_COLOUR_HOVER,
                color: BTN_TEXT_COLOUR_HOVER,
            },
        },
    }));
    const classes = useStyles();

    return (
        <Grid container className="banner">
            <Link onClick={nextBtnClick}>
                <Hidden only={['xs', 'sm', 'md']}>
                    <Img width="1924" height="437" className="image-wrapper" src={BLOCK_BANNER} alt="Gift Finder" />
                </Hidden>

                <Hidden only={['lg', 'xl']}>
                    <Img
                        width="414"
                        height="247"
                        className="image-wrapper"
                        src={BLOCK_MOBILE_BANNER}
                        alt="Gift Finder"
                    />
                </Hidden>

                <Box className="banner-box">
                    <Hidden only={['xs', 'sm', 'md']}>
                        <Box className="banner-text" style={titleStyle}>
                            {TITLE}
                        </Box>
                    </Hidden>
                    <Button className={['banner-button', classes.btnStyle]} onClick={nextBtnClick}>
                        {BTN_TEXT}
                    </Button>
                </Box>
            </Link>
        </Grid>
    );
};

export default Banner;
