import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Img from 'commonComponet/Image';
import React, { memo } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import '../css/home.css';

function HomeBlock1({ homeDetails }) {
    const BannerSettings = {
        autoplay: true,
        // lazyLoad: true,
        autoplaySpeed: 5000,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        vertical: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    const state = useSelector(state => state);

    const store_locale = state && state.auth && state.auth.store_locale;
    return (
        <Grid item xs={12} className="webBanner">
            <Slider {...BannerSettings}>
                {homeDetails &&
                    homeDetails.banners &&
                    homeDetails.banners
                        .filter(item => item.BLOCK_BANNER && item.BLOCK_MOBILE_BANNER)
                        .map((item, index) => (
                            <Link
                                to={item.BLOCK_URL ? `/${store_locale + item.BLOCK_URL}` : '#'}
                                key={`homepage_banner_${index}`}
                                className={!item.BLOCK_URL && 'cursor-context-menu'}
                            >
                                <div key={index} className="banner-image-hight">
                                    <Hidden only={['xs', 'sm', 'md']}>
                                        <Img
                                            lazy={false}
                                            width="1924"
                                            height="437"
                                            src={item.BLOCK_BANNER}
                                            alt={`Homepage Banner ${index}`}
                                        />
                                    </Hidden>
                                    <Hidden only={['lg', 'xl']}>
                                        <Img
                                            lazy={false}
                                            width="414"
                                            height="247"
                                            alt={`Homepage Banner ${index}`}
                                            src={item.BLOCK_MOBILE_BANNER}
                                        />
                                    </Hidden>
                                </div>
                            </Link>
                        ))}
            </Slider>
        </Grid>
    );
}
export default memo(HomeBlock1);
