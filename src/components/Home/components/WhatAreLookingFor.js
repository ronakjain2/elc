import { Hidden, Typography } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Img from 'commonComponet/Image';
import React, { memo } from 'react';
import { useSelector } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import '../css/section3.css';

function HomeBlock2({ title, data, data2, history }) {
    const state = useSelector(state => state);

    const store_locale = state && state.auth && state.auth.store_locale;

    const gotoURL = url => {
        if (!url) return '#';
       return `/${store_locale + url}`;
    };

    return (
        <Grid container>
            {title && (
                <Grid container justify="center" className="slider-title my-4">
                    <Grid item xs={11} className="title flex alignItemCenter">
                        {/* <span className="line" /> */}
                        <label>{title}</label>
                        {/* <span className="line" /> */}
                    </Grid>
                </Grid>
            )}
            <Grid container>
                {data &&
                    data.map((item, index) => {
                        const _style = {
                            color: item.TEXT_COLOR,
                            background: item.BACKGROUND_COLOR,
                        };
                        return (
                            <Grid
                                item
                                xs={6}
                                md={3}
                                key={`homepage_block_${index}`}
                                className="flex justifyCenter section3-block1"
                            >
                                <Box
                                    //onClick={()=>gotoURL(item.BLOCK_URL)}
                                    className={item.BLOCK_URL ? 'section3 cursor-pointer' : 'section3'}
                                >
                                    <Link 
                                        to={gotoURL(item.BLOCK_URL)}
                                    >
                                    <Hidden only={['xs', 'sm', 'md']}>
                                        <Img
                                            width="375"
                                            height="405"
                                            className="image-wrapper"
                                            src={item.BLOCK_BANNER}
                                            alt={item.TITLE}
                                        />
                                    </Hidden>
                                    <Hidden only={['lg', 'xl']}>
                                        <Img
                                            width="166"
                                            height="206"
                                            className="image-wrapper"
                                            src={item.BLOCK_MOBILE_BANNER}
                                            alt={item.TITLE}
                                        />
                                    </Hidden>
                                    <Box className="section3-title" style={_style}>
                                        <Typography>{item.TITLE}</Typography>
                                    </Box>
                                    </Link>
                                </Box>
                            </Grid>
                        );
                    })}
            </Grid>

            <Grid container className="mt-3 mini-banners">
                {data2 &&
                    data2.map((item, index) => {
                        if (!item.BLOCK_BANNER || !item.BLOCK_MOBILE_BANNER) return null;
                        return (
                            <Grid
                                key={index}
                                item
                                xs={12}
                                md={6}
                                //onClick={()=>gotoURL(item.BLOCK_URL)}
                                className={
                                    item.BLOCK_URL ? 'section3-block2 cursor-pointer mb-3 ' : 'section3-block2 mb-3 '
                                }
                            >
                                <Link 
                                    to={gotoURL(item.BLOCK_URL)}
                                >
                                <Hidden only={['xs', 'sm', 'md']}>
                                    <Img
                                        width="800"
                                        height="300"
                                        className="rounded-sm image-large"
                                        src={item.BLOCK_BANNER}
                                    />
                                </Hidden>
                                <Hidden only={['lg', 'xl']}>
                                    <Img
                                        width="800"
                                        height="300"
                                        className="rounded-sm image-large"
                                        src={item.BLOCK_MOBILE_BANNER}
                                    />
                                </Hidden>
                                </Link>
                            </Grid>
                        );
                    })}
            </Grid>
        </Grid>
    );
}

export default withRouter(memo(HomeBlock2));
