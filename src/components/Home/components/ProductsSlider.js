import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import ProductListCard from 'commonComponet/ProductListCard';
import 'commonComponet/ProductListCard/css/productListCard.css';
import React, { memo } from 'react';
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Slider from 'react-slick';
import '../css/ProductsSliders.css';

function ProductsSlider({
    history,
    darkTitle,
    products,
    title,
    showButton = false,
    buttonLabel = 'Shop All',
    buttonRedirection = null,
}) {
    let _products = (products && products.filter(item => item.stock !== 0)) || [];
    const state = useSelector(state => state);
    const store_locale = state && state.auth && state.auth.store_locale;

    const sliderSettings = {
        autoplay: true,
        lazyLoad: true,
        autoplaySpeed: 5000,
        dots: false,
        infinite: true,
        slidesToShow: _products.length >= 5 ? 5 : _products.length,
        slidesToScroll: 1,
        speed: 500,
        vertical: false,
        rtl:true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: _products.length >= 3 ? 3 : _products.length,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: _products.length >= 2 ? 2 : _products.length,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: _products.length >= 2 ? 2 : _products.length,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    const gotoURL = url => url && history.push(`/${store_locale}${url}`);

    return (
        <>
            {_products && _products.length > 0 && (
                <>
                    {title && (
                        <Grid container justify="center" className="slider-title">
                            <Grid item xs={11} className="title">
                                {/* <span className="line" /> */}
                                {darkTitle ? (
                                    <span className="slider-titleCustom">{title}</span>
                                ) : (
                                    <span className="label">{title}</span>
                                )}
                                {showButton && (
                                    <Button onClick={() => gotoURL(buttonRedirection)} className="shop-all-button">
                                        {buttonLabel}
                                    </Button>
                                )}

                                {/* <span className="line" /> */}
                            </Grid>
                        </Grid>
                    )}

                    <Grid container justify="center">
                        <Grid item xs={12} md={10} className="slider">
                            <Slider {...sliderSettings}>
                                {_products.map((item, index) => (
                                    <div key={`product_${title ? title : ''}_${index}`}>
                                        <ProductListCard data={item} />
                                    </div>
                                ))}
                            </Slider>
                        </Grid>
                    </Grid>
                </>
            )}
        </>
    );
}

export default withRouter(memo(ProductsSlider));
