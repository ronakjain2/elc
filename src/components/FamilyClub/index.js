import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import SimpleBreadcrumbs from 'commonComponet/Breadcrumbs';
import Img from 'commonComponet/Image';
import Video from 'commonComponet/Video';
import ComingSoon from 'components/Account/Components/ComingSoon';
import React, { lazy, Suspense, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import LazyLoad from 'react-lazyload';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import './family_club.css';
import { actions as familyClubActions } from './redux/actions';

const Spinner = lazy(() => import('commonComponet/Spinner'));

export default function Home({ history }) {
    const dispatch = useDispatch();

    const state = useSelector(state => state);
    const global = state && state.auth;
    const loader = state && state.familyClub && state.familyClub.familyClubLoader;
    const data = state && state.familyClub && state.familyClub.familyClubLandingData;
    const loginUser = global && global.loginUser;
    const customer_type = loginUser && loginUser.customer_type;
    const fc_status = global && global.fc_status;

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: <FormattedMessage id="Header.Top.FamilyClub" defaultMessage="Family Club" />,
        },
    ];

    useEffect(() => {
        if (!fc_status) return;
        let payload = {
            store_id: state.auth.currentStore,
        };
        dispatch(familyClubActions.getFamilyClubLandingRequest(payload));
    }, [fc_status, state.auth.currentStore, dispatch]);

    const onClickJoinNow = () => {
        if (loginUser && loginUser.customer_id) {
            let payload = {
                quote_id: state.auth.quote_id,
                store_id: state.auth.currentStore,
                customer_id: loginUser.customer_id,
            };
            dispatch(familyClubActions.setFamilyClubRequest(payload));
        } else {
            history.push(`/${global && global.store_locale}/sign-in-register`);
        }
    };

    if (!fc_status)
        return (
            <Grid container justify="center" alignItems="center" style={{ minHeight: '30vh' }}>
                <Grid item xs={12}>
                    <ComingSoon />
                </Grid>
            </Grid>
        );

    return (
        <Suspense fallback="">
            {!loader && data && (
                <Grid container>
                    <Grid item xs={11} className="bredcrub-bclub">
                        <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                    </Grid>

                    <Grid
                        item
                        xs={12}
                        md={12}
                        className="d-flex flex-column flex-md-row justify-content-center align-items-center"
                    >
                        {data.main_block &&
                            data.main_block.map((item, index) => {
                                return (
                                    <div class="fc-banner-div">
                                        {item.type === 'image' ? (
                                            <div className="position-relative flex-grow-1 w-100">
                                                <LazyLoad offset={100} className="d-none d-md-block">
                                                    <img
                                                        src={item.url_desk}
                                                        alt={`Banner ${index}`}
                                                        className="w-100"
                                                    />
                                                </LazyLoad>
                                                <LazyLoad offset={100} className="d-block d-md-none">
                                                    <img src={item.url_mob} alt={`Banner ${index}`} className="w-100" />
                                                </LazyLoad>
                                                <div className="position-absolute summary">
                                                    <h3
                                                        style={{
                                                            background: item.title_color,
                                                            backgroundClip: 'text',
                                                            WebkitBackgroundClip: 'text',
                                                            WebkitTextFillColor: 'transparent',
                                                        }}
                                                    >
                                                        {item.title}
                                                    </h3>
                                                    <p style={{ color: item.desc_color }}>{item.description}</p>
                                                </div>
                                            </div>
                                        ) : (
                                            <div className="position-relative flex-grow-1 w-100">
                                                <Video
                                                    src={item.url_desk}
                                                    controls={true}
                                                    className="d-none d-md-block flex-grow-1 w-100"
                                                />
                                                <Video
                                                    controls={true}
                                                    src={item.url_mob}
                                                    className="d-block d-md-none flex-grow-1 w-100"
                                                />
                                                <div className="position-absolute summary">
                                                    <h3
                                                        style={{
                                                            background: item.title_color,
                                                            backgroundClip: 'text',
                                                            WebkitBackgroundClip: 'text',
                                                            WebkitTextFillColor: 'transparent',
                                                        }}
                                                    >
                                                        {item.title}
                                                    </h3>
                                                    <p style={{ color: item.desc_color }}>{item.description}</p>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                );
                            })}
                    </Grid>
                    <Grid item xs={12} md={12}>
                        <Grid container justify="center" alignItems="center" className={'module-2-container'}>
                            {data.stepper_labels &&
                                data.stepper_labels.map((item, index) => {
                                    return (
                                        <>
                                            <Grid item xs={12} md={4}>
                                                <div className={'gery-box'}></div>
                                                <div className={'brand-color-box'}>{index + 1}</div>
                                                <p className={'sub-text-p'}>{item}</p>
                                            </Grid>
                                        </>
                                    );
                                })}
                        </Grid>
                    </Grid>

                    {data.join_block && customer_type !== 'ecom_club_user' && (
                        <>
                            <Grid item xs={12} md={12} className="position-relative">
                                <LazyLoad offset={100}>
                                    <img
                                        src={data.join_block.image_desk}
                                        alt={'Join Family club'}
                                        className="d-none d-md-block w-100"
                                    />
                                </LazyLoad>
                                <LazyLoad offset={100}>
                                    <img
                                        src={data.join_block.image_mob}
                                        alt={'Join Family club'}
                                        className="d-block d-md-none w-100"
                                    />
                                </LazyLoad>

                                <div className={'join-now-content'}>
                                    <p className={'join-now-text'}>{data.join_block.description}</p>
                                    <div className="joinNow">
                                        <Button
                                            className="joinNow-btn"
                                            variant="contained"
                                            color="primary"
                                            onClick={() => onClickJoinNow()}
                                        >
                                            {data.join_block.button_text}
                                        </Button>
                                    </div>
                                </div>
                            </Grid>
                        </>
                    )}

                    {data.banners &&
                        data.banners.map((item, index) => {
                            return (
                                <>
                                    <Hidden only={['xs', 'sm']}>
                                        <Grid item xs={12} md={12}>
                                            <Grid
                                                container
                                                justify="center"
                                                alignItems="center"
                                                className="module-3-container"
                                                style={{ backgroundColor: item.background_color }}
                                                data-reverse={index % 2 !== 0}
                                            >
                                                <Grid item xs={12} md={6}>
                                                    <div className={'image-section'}>
                                                        <Img
                                                            src={item.image}
                                                            alt={item.title}
                                                            height={315}
                                                            width={540}
                                                            className="rounded-lg"
                                                        />
                                                    </div>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Typography className="bold-text-head">{item.title}</Typography>
                                                    <Typography className="descriptionText">
                                                        {item.description}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Hidden>

                                    <Hidden only={['md', 'lg', 'xl']}>
                                        <Grid item xs={12} md={12}>
                                            <Grid container justify="center" alignItems="center">
                                                <Grid item xs={12} className="mob-image-container">
                                                    <div className={'image-mobile-section'}>
                                                        <Img
                                                            src={item.image}
                                                            alt={item.title}
                                                            height={175}
                                                            width={300}
                                                            className="rounded-lg"
                                                        />
                                                    </div>
                                                </Grid>
                                                <Grid
                                                    item
                                                    xs={12}
                                                    className={'module-3-mobi'}
                                                    style={{ backgroundColor: item.background_color }}
                                                >
                                                    <Typography className="bold-text-head">{item.title}</Typography>
                                                    <Typography className="descriptionText">
                                                        {item.description}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Hidden>
                                </>
                            );
                        })}

                    <Grid
                        container
                        className="fc-terms-and-cond mx-2 my-1 justify-content-center justify-content-md-end"
                    >
                        <Link to={`/${global && global.store_locale}/terms-and-conditions`}>
                            <Typography variant="span">
                                <FormattedMessage
                                    id="FamilyClub.ViewAllTermAndCondition"
                                    defaultMessage="View all Terms & Conditions"
                                />
                            </Typography>
                        </Link>
                    </Grid>
                </Grid>
            )}
            <Grid container>
                <Grid item xs={12} md={12}>
                    {loader && <Spinner />}
                </Grid>
            </Grid>
        </Suspense>
    );
}
