import api from 'apiServices';

const setFamilyClub = (payload) => api.post('/index.php/rest/V1/app/fc/addtocartfcp', payload);
const getFamilyClubLandingData = ({ store_id }) => api.get(`/index.php/rest/V1/app/fc/getfcpage?store_id=${store_id}`);

export default {
    setFamilyClub,
    getFamilyClubLandingData,
};
