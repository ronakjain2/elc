import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import api from '../api';
import { actions, types } from './actions';

const auth = state => state && state.auth;

const setFamilyClubReq = function* setFamilyClubReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { quote_id, store_id, customer_id } = payload;
        const from_basket = payload.from_basket;
        const { data } = yield call(api.setFamilyClub, { quote_id, store_id, customer_id });
        yield put(actions.familyClubRequestSuccess(data));
        if (data && data.status) {
            toast.success(data && data.message);
            if (from_basket) {
                yield put(
                    basketActions.getCartItemRequest({
                        quote_id,
                        store_id,
                        isBasket: true,
                    }),
                );
            }
            yield put(push(`/${authData && authData.store_locale}/cart`));
        } else {
            if (data.code === 403) yield put(push(`/${authData && authData.store_locale}/cart`));
            toast.error((data && data.message) || 'Something went to wrong');
        }
    } catch (err) {
        toast.error('Something went to wrong');
        yield put(actions.familyClubRequestFailed());
    }
};

const getFamilyClubLandingReq = function* getFamilyClubLandingReq({ payload }) {
    try {
        const { store_id } = payload;
        const { data } = yield call(api.getFamilyClubLandingData, { store_id });
        if (data && data.status && data.data) {
            yield put(actions.getfamilyClubLandingRequestSuccess(data.data));
        } else {
            yield put(actions.getfamilyClubLandingRequestFailed());
        }
    } catch (err) {
        toast.error('Something went to wrong');
        yield put(actions.getfamilyClubLandingRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.SET_FAMILY_CLUB_REQUEST, setFamilyClubReq);
    yield takeLatest(types.GET_FAMILY_CLUB_REQUEST, getFamilyClubLandingReq);
}
