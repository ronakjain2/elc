import { createAction } from 'redux-actions';

//actions types

const SET_FAMILY_CLUB_REQUEST = 'ELC@OROB/SET_FAMILY_CLUB_REQUEST';
const SET_FAMILY_CLUB_SUCCESS = 'ELC@OROB/SET_FAMILY_CLUB_SUCCESS';
const SET_FAMILY_CLUB_FAILURE = 'ELC@OROB/SET_FAMILY_CLUB_FAILURE';

const setFamilyClubRequest = createAction(SET_FAMILY_CLUB_REQUEST);
const familyClubRequestSuccess = createAction(SET_FAMILY_CLUB_SUCCESS);
const familyClubRequestFailed = createAction(SET_FAMILY_CLUB_FAILURE);

const GET_FAMILY_CLUB_REQUEST = 'ELC@OROB/GET_FAMILY_CLUB_REQUEST';
const GET_FAMILY_CLUB_SUCCESS = 'ELC@OROB/GET_FAMILY_CLUB_SUCCESS';
const GET_FAMILY_CLUB_FAILURE = 'ELC@OROB/GET_FAMILY_CLUB_FAILURE';

const getFamilyClubLandingRequest = createAction(GET_FAMILY_CLUB_REQUEST);
const getfamilyClubLandingRequestSuccess = createAction(GET_FAMILY_CLUB_SUCCESS);
const getfamilyClubLandingRequestFailed = createAction(GET_FAMILY_CLUB_FAILURE);

//actions
export const actions = {
    setFamilyClubRequest,
    familyClubRequestSuccess,
    familyClubRequestFailed,

    getFamilyClubLandingRequest,
    getfamilyClubLandingRequestSuccess,
    getfamilyClubLandingRequestFailed,
};

//types
export const types = {
    SET_FAMILY_CLUB_REQUEST,
    SET_FAMILY_CLUB_SUCCESS,
    SET_FAMILY_CLUB_FAILURE,

    GET_FAMILY_CLUB_REQUEST,
    GET_FAMILY_CLUB_SUCCESS,
    GET_FAMILY_CLUB_FAILURE,
};
