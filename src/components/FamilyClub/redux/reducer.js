import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.SET_FAMILY_CLUB_REQUEST]: (state) => ({
        ...state,
        familyClubLoader: true,
    }),
    [types.SET_FAMILY_CLUB_SUCCESS]: (state, { payload }) => ({
        ...state,
        familyClubLoader: false,
        familyClubData: payload,
    }),
    [types.SET_FAMILY_CLUB_FAILURE]: (state) => ({
        ...state,
        familyClubLoader: false,
    }),
    [types.GET_FAMILY_CLUB_REQUEST]: (state) => ({
        ...state,
        familyClubLoader: true,
    }),
    [types.GET_FAMILY_CLUB_SUCCESS]: (state, { payload }) => ({
        ...state,
        familyClubLoader: false,
        familyClubLandingData: payload,
    }),
    [types.GET_FAMILY_CLUB_FAILURE]: (state) => ({
        ...state,
        familyClubLoader: false,
        familyClubLandingData: null,
    }),
};

export default handleActions(actionHandler, {
    familyClubLoader: false,
    familyClubData: null,
    familyClubLandingData: null,
});
