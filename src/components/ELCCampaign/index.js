import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Spinner from 'commonComponet/Spinner';
import React, { lazy, Suspense, useEffect } from 'react';
import { isMobile } from 'react-device-detect';
import MetaTags from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import { actions as elcPromotionAction } from './redux/actions';
import './styles.css';
const BannerSection1 = lazy(() => import('./components/Banner1'));
const BannerSection2 = lazy(() => import('./components/Banner2'));
const BannerSection3 = lazy(() => import('./components/Banner3'));
const ProductGrid = lazy(() => import('./components/ProductGrid'));

export default function ELCCampaign() {
    const dispatch = useDispatch();
    const state = useSelector(state => state);
    const loader = state.elcCampaignTemplateReducer.loader;
    const campaign_info = state && state.elcCampaignTemplateReducer && state.elcCampaignTemplateReducer.info;
    const campaign_data = campaign_info && campaign_info.data;
    const pathname =
        (window &&
            window.location &&
            window.location.pathname &&
            window.location.pathname.split('/')[window.location.pathname.split('/').length - 1]) ||
        'NA';
    useEffect(() => {
        try {
            dispatch(
                elcPromotionAction.getELCPromotion({
                    url_key: pathname,
                    storeId: state.auth.currentStore,
                }),
            );
        } catch (err) {}
    }, [dispatch, pathname, state.auth.currentStore]);

    const createMetaTags = (title = null, desc = null, keywords = null) => {
        if (!title && !desc && !keywords)
            return (
                <MetaTags>
                    <title>ELC</title>
                    <meta name="description" content="" />
                    <meta name="keywords" content="" />
                    <meta property="og:title" content="ELC" />
                </MetaTags>
            );

        return (
            <MetaTags>
                <title>{title}</title>
                <meta name="description" content={desc} />
                <meta name="keywords" content={keywords} />
                <meta property="og:title" content={title} />
            </MetaTags>
        );
    };

    if (loader) {
        return (
            <Grid container alignItems="center">
                <Spinner />
            </Grid>
        );
    }

    return (
        <Suspense fallback={<Spinner />}>
            {campaign_info.status === false ? (
                <div style={{ height: '100vh', position: 'relative', textAlign: 'center' }}>
                    <h3
                        style={{
                            textAlign: 'center',
                            position: 'absolute',
                            top: '50%',
                            left: '0%',
                            transform: 'translate(0%, -50%)',
                            margin: '0 auto',
                            right: 0,
                        }}
                    >
                        {campaign_info.message && state.elcCampaignTemplateReducer.info.message}
                    </h3>
                </div>
            ) : (
                <div className={!isMobile ? 'container' : ''}>
                    {campaign_info.data &&
                        campaign_data.section1 &&
                        createMetaTags(
                            campaign_data.section1.meta_title,
                            campaign_data.section1.meta_desc,
                            campaign_data.section1.meta_keyword,
                        )}

                    <div className="col col-12 mr-int">
                        <Typography className="staticPage-title">
                            <span style={{ marginTop: '3%' }}>
                                {campaign_info.data && campaign_data.section1 && campaign_data.section1.title
                                    ? campaign_data.section1.title
                                    : ''}
                            </span>
                        </Typography>

                        <Typography className="webText mobiletext" style={{ marginBottom: 10 }}>
                            {campaign_info.data &&
                                campaign_data.section1 &&
                                campaign_data.section1.text &&
                                campaign_data.section1.text}
                        </Typography>
                    </div>
                    <div className="col col-12">
                        {campaign_info.data && campaign_data.section2 && (
                            <BannerSection1 data={campaign_data.section2} />
                        )}
                    </div>
                    <div className="col col-12">
                        {campaign_info.data && campaign_data.section3 && (
                            <BannerSection2 data={campaign_data.section3} />
                        )}
                    </div>
                    <div className="col col-12">
                        {campaign_info.data && campaign_data.section4 && (
                            <ProductGrid
                                data={campaign_data.section4}
                                showShopAllButton={campaign_data.section4.shopAllButton}
                                shopAllButtonLabel={campaign_data.section4.shopAllLabel}
                                shopAllButtonRedirection={campaign_data.section4.shopAllKey}
                            />
                        )}
                    </div>
                    <div className="col col-12">
                        {campaign_info.data && campaign_data.section5 && (
                            <BannerSection3 data={campaign_data.section5} />
                        )}
                    </div>
                </div>
            )}
        </Suspense>
    );
}
