import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_ELC_PROMOTION_DATA]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_ELC_PROMOTION_DATA_FAILED]: (state, { payload }) => ({
        ...state,
        loader: false,
        info: payload || {},
    }),
    [types.GET_ELC_PROMOTION_DATA_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        info: payload || {},
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    info: {},
    error: false,
});
