import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../api';

const getELCPromotionData = function* getELCPromotionData({ payload }) {
    try {
        const { url_key, storeId } = payload;
        const { data } = yield call(api.getELCPromotionPageInfo, encodeURIComponent(url_key), storeId);
        if (data && data.status) {
            yield put(actions.getELCPromotionSuccess(data));
        } else {
            yield put(actions.getELCPromotionFailed(data));
        }
    } catch (err) {
        yield put(actions.getELCPromotionFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_ELC_PROMOTION_DATA, getELCPromotionData);
}
