import { createAction } from 'redux-actions';

const GET_ELC_PROMOTION_DATA = 'ELC@OROB/GET_ELC_PROMOTION_DATA';
const GET_ELC_PROMOTION_DATA_SUCCESS = 'ELC@OROB/GET_ELC_PROMOTION_DATA_SUCCESS';
const GET_ELC_PROMOTION_DATA_FAILED = 'ELC@OROB/GET_ELC_PROMOTION_DATA_FAILED';

const getELCPromotion = createAction(GET_ELC_PROMOTION_DATA);
const getELCPromotionSuccess = createAction(GET_ELC_PROMOTION_DATA_SUCCESS);
const getELCPromotionFailed = createAction(GET_ELC_PROMOTION_DATA_FAILED);

export const types = {
    GET_ELC_PROMOTION_DATA,
    GET_ELC_PROMOTION_DATA_SUCCESS,
    GET_ELC_PROMOTION_DATA_FAILED,
};

export const actions = {
    getELCPromotion,
    getELCPromotionSuccess,
    getELCPromotionFailed,
};
