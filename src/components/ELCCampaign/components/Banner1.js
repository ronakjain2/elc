import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { isMobile } from 'react-device-detect';
import Slider from 'react-slick';
import Radium from 'radium';

function BannerSection1({ data }) {
    const state = useSelector((state) => state);

    const goToTop = () => {
        document.body.scrollTop = '110px';
    };
    var settings2 = {
        dots: true,
        arrows: true,
        infinite: true,
        speed: 800,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        autoplay: true,
        centerPadding: '0',
        autoplaySpeed: 10000,

        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    const createSlideElement = (item, key) => {
        const customStyle = {
            backgroundColor: item.button_color,
            color: item.button_text_color,

            ':hover': {
                backgroundColor: item.button_hover_color,
                color: item.button_text_hover_color,
            },
        };
        const bannerContent = {
            null: {
                textAlign: 'left',
                left: '6.875rem',
            },
            left: {
                textAlign: 'left',
                left: '6.875rem',
            },
            right: {
                textAlign: 'right',
                right: '6.875rem',
            },
            center: {
                left: '50%',
                textAlign: 'center',
                // marginLeft:"-236px",
                transform: 'translate(-50%,-50%)',
                width: '472px',
            },
        };
        const { store_locale } = state && state.auth;
        return (
            <div key={key}>
                <div className="ImageBanner">
                    {!isMobile && (
                        <div className="web">
                            {item.banner_img === '1' ? (
                                <img src={item.banner_img_desktop} alt="banner1" />
                            ) : (
                                <video
                                    loop
                                    muted
                                    autoPlay
                                    playsInline
                                    width="100%"
                                    src={item.banner_video_desktop}
                                    type="video/mp4"
                                />
                            )}
                        </div>
                    )}
                    {isMobile && (
                        <div className="mob">
                            {item.banner_img === '1' ? (
                                <img src={item.banner_img_mobile} alt="banner1" />
                            ) : (
                                <video
                                    loop
                                    muted
                                    autoPlay
                                    width="100%"
                                    src={item.banner_video_mobile}
                                    type="video/mp4"
                                />
                            )}
                        </div>
                    )}

                    <div
                        className="bannerContent1"
                        style={
                            isMobile
                                ? { backgroundColor: item.banner_text_bgcolor_mobile }
                                : { ...bannerContent[item.banner_text_alignment] }
                        }
                    >
                        <p className="headingTitle" style={{ color: item.banner_title_color }}>
                            {item.banner_title}
                        </p>
                        <p
                            className="headingText"
                            style={{ color: item.banner_text_color }}
                            dangerouslySetInnerHTML={{ __html: item.banner_text }}
                        ></p>
                        <div className="buttonDiv">
                            <Link onClick={() => goToTop()} to={`/${store_locale}/${item.redirection_link}`}>
                                <button
                                    key={key}
                                    type="button"
                                    id={`btn-${key}`}
                                    className="whiteButton"
                                    style={customStyle}
                                >
                                    {item.button_text}
                                    <svg
                                        id={`arrow-${key}`}
                                        width="16"
                                        height="16"
                                        className="LinkWithChevron__StyledChevron-s8q0kp-1 ifWWhX Chevron__ChevronIcon-sc-1q2x5f4-0 bgViWV"
                                        viewBox="0 0 18 28"
                                        aria-hidden="true"
                                        data-di-res-id="3cc88b0b-59653321"
                                        data-di-rand="1590741222426"
                                    >
                                        <path
                                            d="M1.825 28L18 14 1.825 0 0 1.715 14.196 14 0 26.285z"
                                            fill="currentColor"
                                        ></path>
                                    </svg>
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    return (
        <div className="HomeBanner1">
            <Slider {...settings2}>
                {data &&
                    Object.values(data).map((item, index) => {
                        return createSlideElement(item, index); //this.checkValidObject(item)
                    })}
            </Slider>
        </div>
    );
}
export default Radium(BannerSection1);
