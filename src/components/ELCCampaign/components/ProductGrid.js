import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Img from 'commonComponet/Image';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import Slider from 'react-slick';
import '../styles.css';
function YouMayAlsoLike({ data, showShopAllButton, shopAllButtonLabel, shopAllButtonRedirection, history }) {
    const state = useSelector(state => state);

    let youMayAlsoLikeProducts = (data && data.product_data) || [];

    if (!youMayAlsoLikeProducts || !Object.values(youMayAlsoLikeProducts).length) return null;

    const youMayAlsoLikeSettings = {
        autoplay: true,
        // lazyLoad: true,
        autoplaySpeed: 5000,
        dots: false,
        infinite: true,
        slidesToShow: Object.keys(data.product_data).length >= 5 ? 5 : Object.keys(data.product_data).length,
        slidesToScroll: 1,
        speed: 500,
        vertical: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow:
                        Object.keys(data.product_data).length >= 3 ? 3 : Object.keys(data.product_data).length,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow:
                        Object.keys(data.product_data).length >= 2 ? 2 : Object.keys(data.product_data).length,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow:
                        Object.keys(data.product_data).length >= 2 ? 2 : Object.keys(data.product_data).length,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    const showDiscountPrice = data => {
        const offerData = data && data.offers && data.offers.data;
        const offerstatus = data && data.offers && data.offers.status;
        if (offerstatus === 1) {
            if (Object.keys(offerData).length === 1) {
                for (const value in offerData) {
                    if (value === '1') {
                        return (
                            <>
                                <span>{`${data.currency} ${offerData[value]}`}</span>
                                <span
                                    style={{ textDecorationLine: 'line-through', marginLeft: '10px' }}
                                    className="amt_strike"
                                >{`${data.currency} ${data && data.price}`}</span>
                            </>
                        );
                    }
                    return <span>{`${data.currency} ${data && data.price}`}</span>;
                }
            } else {
                return <span>{`${data.currency} ${data && data.price}`}</span>;
            }
        } else {
            return <span>{`${data.currency} ${data && data.price}`}</span>;
        }
    };
    const store_locale = state && state.auth && state.auth.store_locale;
    const gotoPDP = data => {
        document.body.scrollTop = '132px';
        return history.push(`/${store_locale}/products-details/${data.url_key}`);
    };

    return (
        <div>
            <div>
                {data && data.title && Object.keys(data.title).length > 0 && (
                    <div container justify="center">
                        <div className="pdp-you-may-also-like flex">
                            <span className="line" />
                            <label>
                                {data.title}
                                {showShopAllButton && (
                                    <button
                                        onClick={() => history.push(`/${store_locale}${shopAllButtonRedirection}`)}
                                        className="shop-all-button"
                                    >
                                        {shopAllButtonLabel}
                                    </button>
                                )}
                            </label>
                            <span className="line" />
                        </div>
                    </div>
                )}

                <div className="bestsellerslider">
                    <Slider {...youMayAlsoLikeSettings}>
                        {youMayAlsoLikeProducts &&
                            Object.values(youMayAlsoLikeProducts).map((data, index) => (
                                <div key={`bestSeller_${index}`}>
                                    <div className="BestSellerItem">
                                        <div className="productsItem">
                                            <Link to={`/${store_locale}/products-details/${data.url_key}`}>
                                                <Img
                                                    width="184"
                                                    height="175"
                                                    src={data.productImageUrl}
                                                    alt={data.name}
                                                />
                                                <div className="text-height">
                                                    {data && data.name && data.name.length > 45 ? (
                                                        <Typography className="p_title">{`${data.name.substring(
                                                            0,
                                                            45,
                                                        )}...`}</Typography>
                                                    ) : (
                                                        <Typography className="p_title">{data.name}</Typography>
                                                    )}
                                                </div>
                                                <Typography className="p_amt">{showDiscountPrice(data)}</Typography>
                                            </Link>
                                            <Button
                                                className="addToBasket"
                                                style={{ marginTop: 0 }}
                                                onClick={() => gotoPDP(data)}
                                            >
                                                <FormattedMessage
                                                    id="Product.AddToBasket"
                                                    defaultMessage="Add to basket"
                                                />
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                            ))}
                    </Slider>
                </div>
            </div>
        </div>
    );
}

export default withRouter(YouMayAlsoLike);
