import api from 'apiServices';

const getELCPromotionPageInfo = (url_key, store_id) =>
    api.get(`/index.php/rest/V1/app/campaign?url=${url_key}&store=${store_id}`);
export default {
    getELCPromotionPageInfo,
};
