import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_VISA_PROMOTION_REQUEST]: (state) => ({
        ...state,
        visaPromotionLoader: true,
        error: false,
    }),
    [types.GET_VISA_PROMOTION_REQUEST_FAILURE]: (state) => ({
        ...state,
        visaPromotionLoader: false,
        details: {},
        error: true,
    }),
    [types.GET_VISA_PROMOTION_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        visaPromotionLoader: false,
        details: payload || {},
        error: false,
    }),
};

export default handleActions(actionHandler, {
    visaPromotionLoader: false,
    details: {},
    error: false,
});
