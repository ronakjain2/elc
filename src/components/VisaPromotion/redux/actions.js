import { createAction } from 'redux-actions';

//actions types

const GET_VISA_PROMOTION_REQUEST = 'ELC@OROB/GET_VISA_PROMOTION_REQUEST';
const GET_VISA_PROMOTION_REQUEST_SUCCESS = 'ELC@OROB/GET_VISA_PROMOTION_REQUEST_SUCCESS';
const GET_VISA_PROMOTION_REQUEST_FAILURE = 'ELC@OROB/GET_VISA_PROMOTION_REQUEST_FAILURE';

const getVisaPromotionResRequest = createAction(GET_VISA_PROMOTION_REQUEST);
const getVisaPromotionResRequestSuccess = createAction(GET_VISA_PROMOTION_REQUEST_SUCCESS);
const getVisaPromotionResRequestFailed = createAction(GET_VISA_PROMOTION_REQUEST_FAILURE);

//actions
export const actions = {
    getVisaPromotionResRequest,
    getVisaPromotionResRequestSuccess,
    getVisaPromotionResRequestFailed,
};

//types
export const types = {
    GET_VISA_PROMOTION_REQUEST,
    GET_VISA_PROMOTION_REQUEST_SUCCESS,
    GET_VISA_PROMOTION_REQUEST_FAILURE,
};
