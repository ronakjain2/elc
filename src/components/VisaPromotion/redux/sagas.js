import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../api';
const auth = (state) => state && state.auth;

const getVisaPromotionResReq = function* getVisaPromotionResReq({ payload }) {
    try {
        const { storeId } = payload;
        const { data } = yield call(api.getVisaPromotionData, storeId);
        if (data && data.status) {
            yield put(actions.getVisaPromotionResRequestSuccess(data));
        } else {
            yield put(actions.getVisaPromotionResRequestFailed());
        }
    } catch (err) {
        yield put(actions.getVisaPromotionResRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_VISA_PROMOTION_REQUEST, getVisaPromotionResReq);
}
