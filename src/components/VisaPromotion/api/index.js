import api from 'apiServices';

const getVisaPromotionData = (store_id) => api.get(`/index.php/rest/V1/app/visapromotion/?store=${store_id}`);
export default {
    getVisaPromotionData,
};
