import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Hidden, Paper } from '@material-ui/core';
import Spinner from 'commonComponet/Spinner';
import Slider from 'react-slick';
import './visapromotion.css';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { actions as visaPromotionAction } from './redux/actions';
import { BrowserView, MobileView, isBrowser, isMobile } from 'react-device-detect';
export default function VisaPromotion() {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    useEffect(() => {
        try {
            dispatch(
                visaPromotionAction.getVisaPromotionResRequest({
                    storeId: state.auth.currentStore,
                }),
            );
        } catch (err) {}
    }, [dispatch, state.auth.currentStore]);

    const data =
        (state && state.visaPromotion && state.visaPromotion.details && state.visaPromotion.details.data) || [];

    const store_locale = state.auth.store_locale;

    const settings1 = {
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        speed: 500,
        vertical: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    return (
        <Grid container className="mainContainer">
            <Grid container justify="center">
                <Grid item xs={12} md={6}>
                    <div className="text-center">
                        <div className="text-center">
                            <img className="visa-logo" src={data.logo} alt="" />
                        </div>
                        <div>
                            {state.auth.language === 'en' ? (
                                <p className="brand_name">{data.brand_name_en !== null ? data.brand_name_en : ''}</p>
                            ) : (
                                <p className="brand_name">{data.brand_name_ar !== null ? data.brand_content_ar : ''}</p>
                            )}
                        </div>
                        <br />
                        <Grid item xs={12} md={6} style={{ paddingLeft: 15, paddingRight: 15 }}>
                            {isMobile && (
                                <Slider {...settings1}>
                                    {data &&
                                        data.brand_slider_content &&
                                        data.brand_slider_content.map((item, index) => {
                                            return (
                                                <div className="item">
                                                    <ul style={{ display: 'flex' }}>
                                                        <Link to={`/${store_locale}${item.brand_url}`}>
                                                            <li className="brand-li">
                                                                <div className="img-container">
                                                                    <img src={item.brand_image} />
                                                                    {state.auth.language === 'en' ? (
                                                                        <p className="brand-name">
                                                                            {item.brand_category_name_en}
                                                                        </p>
                                                                    ) : (
                                                                        <p className="brand-name">
                                                                            {item.brand_category_name_ar}
                                                                        </p>
                                                                    )}
                                                                </div>
                                                            </li>
                                                        </Link>
                                                    </ul>
                                                </div>
                                            );
                                        })}
                                </Slider>
                            )}
                            {!isMobile && (
                                <div className="item">
                                    {data &&
                                        data.brand_slider_content &&
                                        data.brand_slider_content.map((imageitem, key1) => {
                                            return (
                                                <ul key={key1}>
                                                    <Link to={`/${store_locale}${imageitem.brand_url}`}>
                                                        <li className="brand-li">
                                                            <div className="img-container">
                                                                <img src={imageitem.brand_image} />
                                                                {state.auth.language === 'en' ? (
                                                                    <p className="brand-name">
                                                                        {imageitem.brand_category_name_en}
                                                                    </p>
                                                                ) : (
                                                                    <p className="brand-name">
                                                                        {imageitem.brand_category_name_ar}
                                                                    </p>
                                                                )}
                                                            </div>
                                                        </li>
                                                    </Link>
                                                </ul>
                                            );
                                        })}
                                </div>
                            )}
                        </Grid>

                        <br />
                        <Grid md={12}>
                            <div className="row justify-center">
                                {state.auth.language === 'en' ? (
                                    <div className="textAlignStart">
                                        {data.brand_content_en && data.brand_content_en}
                                    </div>
                                ) : (
                                    <div>{data.brand_content_ar && data.brand_content_ar}</div>
                                )}
                            </div>
                            <br />
                            <div className="justify-content-md-center">
                                {state.auth.language === 'en' ? (
                                    <p className="offer-text">
                                        {data.voucher_code_content_en && data.voucher_code_content_en}
                                    </p>
                                ) : (
                                    <p>{data.data.voucher_code_content_ar && data.voucher_code_content_ar}</p>
                                )}
                            </div>
                            <div className="row justify-content-md-center pl-3 pr-3">
                                <div className="visa-input text-center">
                                    <p className="visa-voucher-text">
                                        <span>
                                            <FormattedMessage
                                                id="PromotionVoucherCodeLabel"
                                                defaultMessage="USE VOUCHER CODE"
                                            />
                                        </span>
                                    </p>
                                    <p className="visa-voucher-value">{data.voucher_code && data.voucher_code}</p>
                                </div>
                            </div>
                            <br />
                            <div className="row shop-now-block pl-3 pr-3">
                                <div className="col-xs-4 col-lg-4">
                                    <Link to={`/${store_locale}${data.shop_now_url}`}>
                                        <button className="shop-now-button" type="submit">
                                            <span>
                                                <FormattedMessage
                                                    id="PromotionShopNow.text"
                                                    defaultMessage="Shop Now"
                                                />
                                            </span>
                                            <i className="icon-className-visa fa fa-angle-right" aria-hidden="true"></i>
                                        </button>
                                    </Link>
                                </div>
                            </div>
                        </Grid>
                        <br />
                    </div>
                </Grid>
            </Grid>
        </Grid>
    );
}
