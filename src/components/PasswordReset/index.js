import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import { checkPasswordForReset } from 'components/SignIn_SignUp/utils';
import './passwordReset.css';
import { withRouter } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { actions as PasswordResetActions } from './redux/actions';

function PasswordReset({ history }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const store_locale = state && state.auth && state.auth.store_locale;
    const loader = state && state.passwordReset && state.passwordReset.loader;
    const store_id = state && state.auth && state.auth.currentStore;

    const query = new URLSearchParams(window.location.search);
    const id = query.get('id');
    const token = query.get('token');
    const status = query.get('status');

    useEffect(() => {
        if (token === null || id === null) {
            if (status === null) {
                history.push(`/${store_locale}`);
            }
        }
    }, [id, token, status, history, store_locale]);

    const [values, setValues] = useState({
        confirmPassword: '',
        newPassword: '',
    });
    const [isValidate, setIsvalidate] = useState(false);

    const onChangeHandler = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value });
    };

    const applyPassword = () => {
        setIsvalidate(true);
        if (
            values.confirmPassword === '' ||
            values.newPassword === '' ||
            values.newPassword.length < 8 ||
            !checkPasswordForReset(values.newPassword) ||
            values.confirmPassword !== values.newPassword
        ) {
            return;
        }
        setIsvalidate(false);
        dispatch(
            PasswordResetActions.passwordResetEmailRequest({
                customerId: id,
                password: values.newPassword,
                passwordConfirmation: values.confirmPassword,
                resetPasswordToken: token,
                store_id: store_id,
            }),
        );
    };

    return (
        <Grid container justify="center" className="password-reset">
            {status === null && (
                <Grid item xs={11} md={4}>
                    <Grid container justify="center">
                        <Typography className="title">
                            <FormattedMessage id="Password.Reset.Text" defaultMessage="Password Reset" />
                        </Typography>
                    </Grid>

                    <Grid container>
                        <Grid item xs={12} className="padding-t-input-box">
                            <Typography className="login-sigup-input-text">
                                <FormattedMessage
                                    id="MyAccount.MyProfile.NewPassword.Required"
                                    defaultMessage="New Password*"
                                />
                            </Typography>
                            <TextField
                                variant="outlined"
                                value={values.newPassword}
                                onChange={onChangeHandler('newPassword')}
                                fullWidth
                                type="password"
                                className="inputBox"
                                error={isValidate && values.newPassword === ''}
                            />
                            {/** Password empty message */}
                            {isValidate && values.newPassword === '' && (
                                <FormHelperText className="input-error">
                                    <FormattedMessage
                                        id="SignInSignUp.Password.Empty"
                                        defaultMessage="Please enter password"
                                    />
                                </FormHelperText>
                            )}
                            {/** Password length less than 8 message */}
                            {isValidate && values.newPassword !== '' && values.newPassword.length < 8 && (
                                <FormHelperText className="input-error">
                                    <FormattedMessage
                                        id="SignInSignUp.Password.length"
                                        defaultMessage="Minimum length is 8 characters"
                                    />
                                </FormHelperText>
                            )}
                            <FormHelperText
                                className={
                                    isValidate &&
                                    values.newPassword !== '' &&
                                    values.newPassword.length >= 8 &&
                                    !checkPasswordForReset(values.newPassword)
                                        ? 'password-strong-message input-error'
                                        : 'password-strong-message'
                                }
                            >
                                <FormattedMessage
                                    id="SignInSignUp.password.strong.message1"
                                    defaultMessage="Password must be at least 8 characters long and contain an uppercase letter, a lowercase letter and a number."
                                />
                            </FormHelperText>
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={12} className="padding-t-input-box">
                            <Typography className="login-sigup-input-text">
                                <FormattedMessage
                                    id="SignInSignUp.ConfirmPassword"
                                    defaultMessage="Confirm Password*"
                                />
                            </Typography>
                            <TextField
                                variant="outlined"
                                value={values.confirmPassword}
                                onChange={onChangeHandler('confirmPassword')}
                                fullWidth
                                type="password"
                                className="inputBox"
                                error={isValidate && values.confirmPassword === ''}
                            />
                            {/** Password empty message */}
                            {isValidate && values.confirmPassword === '' && (
                                <FormHelperText className="input-error">
                                    <FormattedMessage
                                        id="SignInSignUp.ConfirmPassword.Empty"
                                        defaultMessage="Please enter password again"
                                    />
                                </FormHelperText>
                            )}
                            {isValidate &&
                                values.confirmPassword !== '' &&
                                values.confirmPassword !== values.newPassword && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="SignInSignUp.ConfirmPassword.NotSame"
                                            defaultMessage="Password and Confirm password must be same"
                                        />
                                    </FormHelperText>
                                )}
                        </Grid>
                    </Grid>

                    <Grid container justify="center">
                        {!loader && (
                            <Button className="apply-button" onClick={() => applyPassword()}>
                                <FormattedMessage id="Password.Reset.Apply" defaultMessage="Apply" />
                            </Button>
                        )}
                        {loader && <CircularProgress />}
                    </Grid>
                </Grid>
            )}

            {status === 'true' && (
                <Grid item xs={11} md={4}>
                    <Grid container>
                        <Typography className="status-title">
                            <FormattedMessage id="ResetPassword.Success.Text" defaultMessage="Reset your password" />
                        </Typography>
                    </Grid>
                    <Grid container>
                        <Typography className="status-sub-title">
                            <FormattedMessage
                                id="ResetPassword.PasswordChanged.Text"
                                defaultMessage="Password changed"
                            />
                        </Typography>
                    </Grid>
                    <Grid container>
                        <Typography className="status-text-title">
                            <FormattedMessage
                                id="ResetPassword.Success.Text1"
                                defaultMessage="Success! You have created a new password for your account."
                            />
                        </Typography>
                    </Grid>
                </Grid>
            )}

            {status === 'false' && (
                <Grid item xs={11} md={4}>
                    <Grid container>
                        <Typography className="status-title-reset">
                            <FormattedMessage id="RestPassword.linkExpired.Text" defaultMessage="Reset link expired" />
                        </Typography>
                    </Grid>
                    <Grid container>
                        <Typography className="status-title">
                            <FormattedMessage id="ResetPassword.success.Text" defaultMessage="Reset your password" />
                        </Typography>
                    </Grid>
                    <Grid container>
                        <Typography className="status-sub-title">
                            <FormattedMessage
                                id="ResetPassword.PasswordChangedfail.Text"
                                defaultMessage="Password change failed"
                            />
                        </Typography>
                    </Grid>
                    <Grid container>
                        <Typography className="status-text-title">
                            <FormattedMessage
                                id="ResetPassword.fail.Text1"
                                defaultMessage="Password change failed try again !!!"
                            />
                        </Typography>
                    </Grid>
                </Grid>
            )}
        </Grid>
    );
}

export default withRouter(PasswordReset);
