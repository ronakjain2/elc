import { createAction } from 'redux-actions';

const PASSWORD_RESET_EMAIL_REQUEST = 'ELC@OROB/PASSWORD_RESET_EMAIL_REQUEST';
const PASSWORD_RESET_EMAIL_REQUEST_SUCCESS = 'ELC@OROB/PASSWORD_RESET_EMAIL_REQUEST_SUCCESS';
const PASSWORD_RESET_EMAIL_REQUEST_FAILED = 'ELC@OROB/PASSWORD_RESET_EMAIL_REQUEST_FAILED';

const passwordResetEmailRequest = createAction(PASSWORD_RESET_EMAIL_REQUEST);
const passwordResetEmailRequestSuccess = createAction(PASSWORD_RESET_EMAIL_REQUEST_SUCCESS);
const passwordResetEmailRequestFailed = createAction(PASSWORD_RESET_EMAIL_REQUEST_FAILED);

export const actions = {
    passwordResetEmailRequest,
    passwordResetEmailRequestSuccess,
    passwordResetEmailRequestFailed,
};

export const types = {
    PASSWORD_RESET_EMAIL_REQUEST,
    PASSWORD_RESET_EMAIL_REQUEST_SUCCESS,
    PASSWORD_RESET_EMAIL_REQUEST_FAILED,
};
