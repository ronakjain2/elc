import { call, put, takeLatest, select } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../api';
import { toast } from 'react-toastify';
import { push } from 'connected-react-router';

const auth = (state) => state && state.auth;

const passwordResetReq = function* passwordResetReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.passwordReset, payload);
        yield put(actions.passwordResetEmailRequestSuccess());
        yield put(push(`/${authData && authData.store_locale}/password-rest?status=${data.status}`));
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.passwordResetEmailRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.PASSWORD_RESET_EMAIL_REQUEST, passwordResetReq);
}
