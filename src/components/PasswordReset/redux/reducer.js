import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.PASSWORD_RESET_EMAIL_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.PASSWORD_RESET_EMAIL_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.PASSWORD_RESET_EMAIL_REQUEST_SUCCESS]: (state) => ({
        ...state,
        loader: false,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
});
