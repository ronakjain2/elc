import api from 'apiServices';

const passwordReset = (payload) => api.post(`/index.php/rest/V1/app/resetpassword`, payload);

export default {
    passwordReset,
};
