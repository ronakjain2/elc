import { useState, useEffect, useCallback } from 'react';

const useInfiniteScroll = (callback) => {
    const [isFetching, setIsFetching] = useState(false);

    const handleScroll = useCallback(() => {
        if (
            window.innerHeight + document.documentElement.scrollTop + 300 < document.documentElement.offsetHeight ||
            isFetching
        )
            return;
        setIsFetching(true);
    }, [isFetching]);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [handleScroll]);

    useEffect(() => {
        if (!isFetching) return;
        callback(() => {
            console.log('called back');
        });
    }, [isFetching, callback]);

    return [isFetching, setIsFetching];
};

export default useInfiniteScroll;
