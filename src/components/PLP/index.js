import Drawer from '@material-ui/core/Drawer';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import 'bootstrap/dist/css/bootstrap.css';
import Spinner from 'commonComponet/Spinner';
import parse from 'html-react-parser';
import React, { lazy, Suspense, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import './css/PLP.css';
import useInfiniteScroll from './infiniteScrolling';
import { actions as PLPActions } from './redux/actions';
import {
    filterData,
    getBrandAttributeKey,
    getUrlKey,
    getUrlKeyL3Menu,
    isUrlContainBrand,
    sortByValue,
    _getUnique_filter
} from './utils';
const ProductItem = lazy(() => import('./components/ProductItem'));
const CategoryCategory = lazy(() => import('./components/CategoryBanner'));
const ApplyFilters = lazy(() => import('./components/ApplyFilters'));
const PLPFilter = lazy(() => import('./components/Filter'));
const SimpleBreadcrumbs = lazy(() => import('commonComponet/Breadcrumbs'));
const CategoryDescription = lazy(() => import('./components/CategoryDescription'));
const NoProducts = lazy(() => import('./components/NoProducts'));

export default function PLP() {
    const url_key = getUrlKey();
    const url_key_l3 = getUrlKeyL3Menu();
    const query = new URLSearchParams(window.location.search);
    const q = query.get('query');
    const filter = query.get('filter');
    const minPrice = query.get('minPrice');
    const maxPrice = query.get('maxPrice');
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const [page, setPage] = useState(1);
    const [anchor, setAnchor] = useState(false);
    const [rowsPerPage] = useState(24);
    const [sortBy, setSortBy] = useState('relevance');
    const [clickName, setClickName] = useState('');
    const [applyFilters, setApplyFilter] = useState({});
    const [categoryDes, setCategroyDes] = useState(null);
    const [product_data, setProductData] = useState(
        (state && state.PLP && state.PLP.products && state.PLP.products.product_data) || {},
    );
    const toggleDrawer = (open, name) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setAnchor(open);
        setClickName(name);
    };
    const isBrandUrl = isUrlContainBrand();
    const attribute_id = getBrandAttributeKey();

    useEffect(() => {
        try {
            if (url_key !== 'search' && !isBrandUrl) {
                let payload = {
                    url_key: url_key_l3,
                    storeId: state.auth.currentStore,
                    filter: filter || '',
                };
                if (minPrice && maxPrice) {
                    payload = {
                        ...payload,
                        minPrice,
                        maxPrice,
                    };
                }

                dispatch(PLPActions.getAllProductRequest(payload));
            } else if (url_key === 'search' && !isBrandUrl) {
                dispatch(
                    PLPActions.getSearchPLPProductRequest({
                        q: q || '',
                        filter: filter || '',
                        storeId: state.auth.currentStore,
                    }),
                );
            }

            //setApplyFilter({});
            setPage(1);
        } catch (err) {}
    }, [state.auth.currentStore, isBrandUrl, url_key, dispatch, q, minPrice, maxPrice, filter, url_key_l3]);

    useEffect(() => {
        if (isBrandUrl && attribute_id) {
            try {
                dispatch(
                    PLPActions.getProductDataByBrand({
                        storeId: state.auth.currentStore,
                        attribute_id,
                    }),
                );
            } catch (err) {}
        }
    }, [dispatch, isBrandUrl, state.auth.currentStore, attribute_id]);

    useEffect(() => {
        try {
            setProductData(state.PLP.products.product_data);
        } catch (err) {}
    }, [state.PLP.products.product_data]);

    const products = state && state.PLP && state.PLP.products;
    const global = state && state.auth;

    let productList =
        product_data &&
        Object.keys(product_data)
            .filter(
                key =>
                    product_data[key] &&
                    product_data[key].json &&
                    product_data[key].json.stock !== 0 &&
                    product_data[key].json.price > 0,
            )
            .map(key => product_data[key]);
    productList = filterData(productList, applyFilters);

    productList = sortByValue(productList, sortBy, products.product_data);

    // BreadCrumbs
    let breadCrumbs = [];

    if (isBrandUrl) {
        breadCrumbs = [
            {
                name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
                url: `/${global && global.store_locale}/`,
            },
            {
                name: <FormattedMessage id="BrowseAllBrand.Text" defaultMessage="Browse All Brands" />,
                url: `/${global && global.store_locale}/browse-all-brands`,
            },
        ];
    } else {
        breadCrumbs = [
            {
                name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
                url: `/${global && global.store_locale}/`,
            },
            {
                name: (products && products.category_name) || q,
            },
        ];
    }

    if (products && products.breadcrumb && products.breadcrumb.url_key !== '') {
        breadCrumbs.splice(1, 0, {
            name:
                products &&
                products.category_path &&
                products.category_path.split('/') &&
                products.category_path.split('/')[0],
            url: `/${global && global.store_locale}/products/${products.breadcrumb.url_key}`,
        });
    }
    // End

    // Category Description html convert to string
    useEffect(() => {
        try {
            if (products.category_description2) {
                setCategroyDes(parse(products.category_description2).props.children);
            }
        } catch (err) {}
    }, [products.category_description2]);

    const [isFetching, setIsFetching] = useInfiniteScroll(fetchMoreListItems);

    function fetchMoreListItems() {
        if (productList && productList.length - page * rowsPerPage > 0) {
            setTimeout(() => {
                setPage(page + 1);
                setIsFetching(false);
            }, 1000);
            return;
        }
        setIsFetching(false);
    }

    return (
        <Suspense fallback={<Spinner />}>
            <div>
                <React.Fragment key="top">
                    <Drawer anchor="top" open={anchor} onClose={toggleDrawer(anchor, false)} className="filter-pop">
                        {clickName === 'filter' && (
                            <PLPFilter
                                filters={products && products.filters}
                                setApplyFilter={setApplyFilter}
                                applyFilters={{ ...applyFilters }}
                                setPage={setPage}
                                setAnchor={setAnchor}
                                sortBy={sortBy}
                                setSortBy={setSortBy}
                            />
                        )}
                    </Drawer>
                </React.Fragment>

                <Grid container justify="center" className="mainPlp">
                    <Grid item xs={11}>
                        {state &&
                            state.PLP &&
                            !state.PLP.loader &&
                            state.PLP.products &&
                            state.PLP.products.product_data &&
                            Object.keys(state.PLP.products.product_data).length > 0 && (
                                <>
                                    {/** Breadcrumb */}
                                    <Grid container justify="center">
                                        <Grid item xs={11} md={12}>
                                            <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                                        </Grid>
                                    </Grid>
                                    {/** End Breadcrumb */}

                                    <Grid container>
                                        {/** Web Filter */}
                                        <Hidden only={['xs', 'sm']}>
                                            <Grid item md={3}>
                                                <PLPFilter
                                                    filters={
                                                        isBrandUrl
                                                            ? _getUnique_filter(products && products.filters)
                                                            : products && products.filters
                                                    }
                                                    setApplyFilter={setApplyFilter}
                                                    applyFilters={{ ...applyFilters }}
                                                    setPage={setPage}
                                                    setAnchor={setAnchor}
                                                />
                                            </Grid>
                                        </Hidden>
                                        {/** End Web Filter */}

                                        {/** Mobile Filter */}
                                        <Hidden only={['lg', 'xl', 'md']}>
                                            <Grid
                                                container
                                                justify="space-between"
                                                alignItems="center"
                                                className="mobile_filter"
                                            >
                                                <div className="filterText">
                                                    <FormattedMessage id="PLP.Filters" defaultMessage="Filters" /> &{' '}
                                                    <FormattedMessage id="PLP.SortBy" defaultMessage="Sort By" />
                                                </div>
                                                <div>
                                                    <img
                                                        src="/images/plp/Filter.svg"
                                                        alt="filterIcon"
                                                        onClick={toggleDrawer(true, 'filter')}
                                                    />
                                                </div>
                                            </Grid>
                                        </Hidden>
                                        {/** End Mobile Filter */}

                                        <Grid item xs={12} md={9} className="PLP">
                                            <Grid container justify="center">
                                                {/** Category Image */}
                                                <Grid item xs={11}>
                                                    {products &&
                                                        products.category_Image &&
                                                        products.category_Image !== '' && (
                                                            <CategoryCategory categoryImage={products.category_Image} />
                                                        )}
                                                </Grid>
                                                {/** End Category Image */}

                                                {/** Product Name */}
                                                <Grid item xs={11} className="ProductName">
                                                    {!q && (
                                                        <Typography>{products && products.category_name}</Typography>
                                                    )}
                                                    {!q && isBrandUrl && (
                                                        <Typography>{products && products.brand_name}</Typography>
                                                    )}
                                                    {q && (
                                                        <Typography>
                                                            <FormattedMessage
                                                                id="No.Search.Item"
                                                                defaultMessage="You searched for"
                                                            />
                                                            {` "${q}"`}
                                                        </Typography>
                                                    )}
                                                    {productList && productList.length > 0 && (
                                                        <Grid className="text-align-rtl">
                                                            <Typography variant="span">
                                                                {productList && productList.length}&nbsp;
                                                                <FormattedMessage
                                                                    id="PLP.Items"
                                                                    defaultMessage="Items"
                                                                />
                                                            </Typography>
                                                        </Grid>
                                                    )}
                                                </Grid>

                                                {/** End Product Name */}

                                                {/** Category Desc */}
                                                <Hidden only={['xs', 'sm']}>
                                                    <Grid item xs={11}>
                                                        {categoryDes && (
                                                            <CategoryDescription categoryDes={categoryDes} />
                                                        )}
                                                    </Grid>
                                                </Hidden>
                                                {/** end Category Desc */}

                                                {/** Apply filters  */}
                                                <Grid item xs={12} className="border-bottom">
                                                    <Grid container justify="center">
                                                        <Grid item xs={11}>
                                                            <ApplyFilters
                                                                applyFilters={{ ...applyFilters }}
                                                                setApplyFilter={setApplyFilter}
                                                                sortBy={sortBy}
                                                                setSortBy={setSortBy}
                                                            />
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                {/** Apply filters */}

                                                {/** Product List */}
                                                <Grid item xs={12} md={11}>
                                                    <Grid container>
                                                        {productList &&
                                                            Object.keys(productList)
                                                                .slice(0, (page - 1) * rowsPerPage + rowsPerPage)
                                                                .map((item, index) => (
                                                                    <ProductItem
                                                                        key={`PLP_${index}`}
                                                                        item={productList[item]}
                                                                        index={index}
                                                                        q={q}
                                                                    />
                                                                ))}
                                                    </Grid>
                                                    {isFetching &&
                                                        productList &&
                                                        productList.length - page * rowsPerPage > 0 && (
                                                            <Grid container alignItems="center">
                                                                <Spinner />
                                                            </Grid>
                                                        )}
                                                </Grid>
                                                {/** End Product List */}

                                                {/** No Product List */}
                                                {(!productList || productList.length === 0) && (
                                                    <Grid item xs={12} md={11}>
                                                        <Typography className="NoTextDataAvailable text-center mt-4">
                                                            <FormattedMessage
                                                                id="PLP.NoDataAvailable"
                                                                defaultMessage="Sorry.. No Data Available"
                                                            />
                                                        </Typography>
                                                    </Grid>
                                                )}
                                                {/** End No Product List */}

                                                {/** Category Desc */}
                                                <Hidden only={['md', 'lg', 'xl']}>
                                                    <Grid item xs={11}>
                                                        {categoryDes && (
                                                            <>
                                                                <div className="description-line" />
                                                                <CategoryDescription categoryDes={categoryDes} />
                                                            </>
                                                        )}
                                                    </Grid>
                                                </Hidden>
                                                {/** end Category Desc */}
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </>
                            )}

                        {/** Spinner */}
                        {state && state.PLP && state.PLP.loader && (
                            <Grid container alignItems="center">
                                <Spinner />
                            </Grid>
                        )}
                        {/** End Spinner */}

                        {/*  NO Product Item */}
                        {state &&
                            state.PLP &&
                            !state.PLP.loader &&
                            (!state.PLP.products ||
                                !state.PLP.products.product_data ||
                                Object.keys(state.PLP.products.product_data).length === 0) && (
                                <>
                                    {/** Breadcrumb */}
                                    <Grid container justify="center">
                                        <Grid item xs={11} md={12}>
                                            <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                                        </Grid>
                                    </Grid>
                                    {/** End Breadcrumb */}
                                    {isBrandUrl && Object.keys(products).length === 0 && (
                                        <Grid item xs={12} md={11} justify="center">
                                            <Typography style={{ textAlign: 'center' }} className="title-empty">
                                                <FormattedMessage
                                                    id="NewProductCommingSoon.Text"
                                                    defaultMessage="New products are coming soon"
                                                />
                                            </Typography>
                                        </Grid>
                                    )}
                                    <NoProducts q={q} message={(state && state.PLP && state.PLP.message) || ''} />
                                </>
                            )}
                        {/*  End No Product Item */}
                    </Grid>
                </Grid>
            </div>
        </Suspense>
    );
}
