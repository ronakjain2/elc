import api from 'apiServices';

const getCacheProducts = (url_key, storeId, minPrice, maxPrice, filter) =>
    api.get(
        `/index.php/rest/V1/app/orob/cache/productlisting/?url_key=${url_key}&storeid=${storeId}&minPrice=${minPrice}&maxPrice=${maxPrice}&filter=${filter}`,
    );
const getNoCacheProducts = payload => api.post('/index.php/rest/V1/app/mkt/no-cache/productlisting', payload);

const getPLPSearchCache = (q, storeId, filter) =>
    api.get(`/index.php/rest/V1/app/orob/cache/searchresult/?q=${q}&storeid=${storeId}&filter=${filter}`);
const getPLPSearchNoCache = payload => api.post('/index.php/rest/V1/app/mkt/no-cache/searchresult', payload);

const getProductDataByBrandAttributeCache = (attribute_id, store_id) =>
    api.get(`/index.php/rest/V1/app/orob/cache/getproductbybrand?store_id=${store_id}&attribute_id=${attribute_id}`);

const getProductDataByBrandAttributeNoCache = payload =>
    api.post('/index.php/rest/V1/app/orob/nocache/getproductbybrand', payload);
export default {
    getCacheProducts,
    getNoCacheProducts,

    getPLPSearchCache,
    getPLPSearchNoCache,

    getProductDataByBrandAttributeCache,
    getProductDataByBrandAttributeNoCache,
};
