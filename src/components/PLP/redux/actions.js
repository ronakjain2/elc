import { createAction } from 'redux-actions';

// Actions types
const GET_ALL_PRODUCTS_REQUEST = 'ELC@OROB/GET_ALL_PRODUCTS_REQUEST';
const GET_ALL_PRODUCTS_REQUEST_SUCCESS = 'ELC@OROB/GET_ALL_PRODUCTS_REQUEST_SUCCESS';
const GET_ALL_PRODUCTS_REQUEST_FAILED = 'ELC@OROB/GET_ALL_PRODUCTS_REQUEST_FAILED';

const GET_SEARCH_PLP_PRODUCT_REQUEST = 'ELC@OROB/GET_SEARCH_PLP_PRODUCT_REQUEST';

const GET_PRODUCT_DATA_BY_BRAND = 'ELC@OROB/GET_PRODUCT_DATA_BY_BRAND';
const GET_PRODUCT_DATA_BY_BRAND_SUCCESS = 'ELC@OROB/GET_PRODUCT_DATA_BY_BRAND_SUCCESS';
const GET_PRODUCT_DATA_BY_BRAND_FAILED = 'ELC@OROB/GET_PRODUCT_DATA_BY_BRAND_FAILED';

// actions methods
const getAllProductRequest = createAction(GET_ALL_PRODUCTS_REQUEST);
const getAllProductRequestSuccess = createAction(GET_ALL_PRODUCTS_REQUEST_SUCCESS);
const getAllProductRequestFailed = createAction(GET_ALL_PRODUCTS_REQUEST_FAILED);

const getSearchPLPProductRequest = createAction(GET_SEARCH_PLP_PRODUCT_REQUEST);

const getProductDataByBrand = createAction(GET_PRODUCT_DATA_BY_BRAND);
const getProductDataByBrandSuccess = createAction(GET_PRODUCT_DATA_BY_BRAND_SUCCESS);
const getProductDataByBrandFailed = createAction(GET_PRODUCT_DATA_BY_BRAND_FAILED);

export const actions = {
    getAllProductRequest,
    getSearchPLPProductRequest,

    getAllProductRequestSuccess,
    getAllProductRequestFailed,

    getProductDataByBrand,
    getProductDataByBrandSuccess,
    getProductDataByBrandFailed,
};

export const types = {
    GET_ALL_PRODUCTS_REQUEST,
    GET_SEARCH_PLP_PRODUCT_REQUEST,

    GET_ALL_PRODUCTS_REQUEST_SUCCESS,
    GET_ALL_PRODUCTS_REQUEST_FAILED,
    GET_PRODUCT_DATA_BY_BRAND,
    GET_PRODUCT_DATA_BY_BRAND_FAILED,
    GET_PRODUCT_DATA_BY_BRAND_SUCCESS,
};
