import { actions as authActions } from 'components/Global/redux/actions';
import { actions as signInActions } from 'components/SignIn_SignUp/redux/actions';
import { toast } from 'react-toastify';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { NoSearchResultFoundEvent, ProductListEvent, ProductSearchEvent } from 'services/GTMservice';
import api from '../api';
import { actions, types } from './actions';

const auth = state => state && state.auth;
const getAllProductsReq = function* getAllProductsReq({ payload }) {
    try {
        const { url_key, storeId, filter } = payload;
        const minPrice = payload.minPrice || '';
        const maxPrice = payload.maxPrice || '';
        const { data } = yield call(api.getCacheProducts, url_key, storeId, minPrice, maxPrice, filter);
        const response = yield call(api.getNoCacheProducts, {
            url_key,
            storeid: storeId,
            minPrice,
            maxPrice,
            filter
        });
        const authData = yield select(auth);

        if (data && data.status) {
            //set Category id
            yield put(authActions.setSuggestionCategoryId(data && data.data && data.data.category_id));
            let newarr = [];
            const cacheProductsData = { ...data.data };
            for (const p in (cacheProductsData && cacheProductsData.product_data) || []) {
                if (cacheProductsData.product_data[p] && cacheProductsData.product_data[p].json) {
                    // const priceData =
                    //     response &&
                    //     response.data &&
                    //     response.data.data &&
                    //     response.data.data.product_data &&
                    //     Object.values(response.data.data.product_data).filter(
                    //         (x) => x.sku === cacheProductsData.product_data[p].sku,
                    //     )[0];
                    const priceData =
                        response &&
                        response.data &&
                        response.data.data &&
                        response.data.data.product_data &&
                        response.data.data.product_data[p] &&
                        response.data.data.product_data[p].sku === cacheProductsData.product_data[p].sku
                            ? response.data.data.product_data[p]
                            : {};
                    if (priceData) {
                        cacheProductsData.product_data[p].json = Object.assign(
                            priceData,
                            cacheProductsData.product_data[p].json,
                        );
                        newarr.push(cacheProductsData.product_data[p]);
                    }
                }
            }
            cacheProductsData.product_data = newarr;
            yield put(actions.getAllProductRequestSuccess(cacheProductsData));
            if (
                cacheProductsData &&
                cacheProductsData.fclub_only_category === '1' &&
                (!authData.loginUser || (authData.loginUser && authData.loginUser.customer_type !== 'ecom_club_user'))
            ) {
                yield put(
                    signInActions.fcPopupSet({
                        fcPrivateCat: true,
                    }),
                );
                yield put(signInActions.fcPopupClose());
            }
            //GTM Product List Event
            ProductListEvent(cacheProductsData.product_data);
        } else {
            yield put(actions.getAllProductRequestFailed(data.code === 200 ? data.message : ''));
        }
    } catch (err) {
        yield put(actions.getAllProductRequestFailed());
    }
};

const getSearchProductReq = function* getSearchProductReq({ payload }) {
    try {
        const { q, filter, storeId } = payload;
        const { data } = yield call(api.getPLPSearchCache, q, storeId, filter);
        const response = yield call(api.getPLPSearchNoCache, {
            q,
            filter,
            storeid: storeId,
        });
        if (data && data.status) {
            let newarr = [];
            const cacheProductsData = { ...data.data };
            for (const p in (cacheProductsData && cacheProductsData.product_data) || []) {
                if (cacheProductsData.product_data[p] && cacheProductsData.product_data[p].json) {
                    const priceData =
                        response &&
                        response.data &&
                        response.data.data &&
                        response.data.data.product_data &&
                        Object.values(response.data.data.product_data).filter(
                            x => x.sku === cacheProductsData.product_data[p].json.sku,
                        )[0];
                    if (priceData) {
                        cacheProductsData.product_data[p].json = Object.assign(
                            priceData,
                            cacheProductsData.product_data[p].json,
                        );
                        newarr.push(cacheProductsData.product_data[p]);
                    }
                }
            }
            cacheProductsData.product_data = newarr;
            yield put(actions.getAllProductRequestSuccess(cacheProductsData));
            //GTM Search Event
            ProductSearchEvent(cacheProductsData.product_data);
        } else {
            NoSearchResultFoundEvent(payload.q);
            yield put(actions.getAllProductRequestFailed(data.code === 200 ? data.message : ''));
        }
    } catch (err) {
        yield put(actions.getAllProductRequestFailed());
    }
};

// Call API for brand

const getAllProductsReqBrand = function* getAllProductsReqBrand({ payload }) {
    try {
        const { storeId, attribute_id } = payload;
        let { data } = yield call(api.getProductDataByBrandAttributeCache, attribute_id, storeId);
        let response = yield call(api.getProductDataByBrandAttributeNoCache, {
            attribute_id,
            store_id: storeId,
        });

        if (data && data.status) {
            let newarr = [];
            const cacheProductsData = { ...data.data };
            for (const p in (cacheProductsData && cacheProductsData.product_data) || []) {
                if (cacheProductsData.product_data[p] && cacheProductsData.product_data[p].json) {
                    const priceData =
                        response &&
                        response.data &&
                        response.data.data &&
                        response.data.data.product_data &&
                        Object.values(response.data.data.product_data).filter(
                            x => x.sku === cacheProductsData.product_data[p].sku,
                        )[0];
                    if (priceData) {
                        cacheProductsData.product_data[p].json = Object.assign(
                            priceData,
                            cacheProductsData.product_data[p].json,
                        );
                        newarr.push(cacheProductsData.product_data[p]);
                    }
                }
            }
            yield put(actions.getAllProductRequestSuccess(cacheProductsData));
        } else {
            toast.success((data && data.message) || 'Something went to wrong, Please try after sometime');
            yield put(actions.getAllProductRequestFailed());
        }
    } catch (err) {
        yield put(actions.getAllProductRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_ALL_PRODUCTS_REQUEST, getAllProductsReq);
    yield takeLatest(types.GET_SEARCH_PLP_PRODUCT_REQUEST, getSearchProductReq);
    yield takeLatest(types.GET_PRODUCT_DATA_BY_BRAND, getAllProductsReqBrand);
}
