import React from 'react';

const _ = require('lodash');

export const getAge = (data) => {
    if (data && data.json && data.json.filtersdata && data.json.filtersdata.age) {
        const age = data && data.json && data.json.filtersdata && data.json.filtersdata.age;
        for (const data in age) {
            return age[data];
        }
    }
    return '';
};

export const showDiscountPrice = (data) => {
    const offerData = data && data.json && data.json.offers && data.json.offers.data;
    const offerstatus = data && data.json && data.json.offers && data.json.offers.status;
    if (offerstatus === 1) {
        if (Object.keys(offerData).length === 1) {
            for (const value in offerData) {
                if (value === '1') {
                    return (
                        <>
                            <span>{`${data.currency} ${offerData[value]}`}</span>
                            <span className="amt_strike">{`${data.currency} ${
                                data && data.json && data.json.price
                            }`}</span>
                        </>
                    );
                }
                return <span>{`${data.currency} ${data && data.json && data.json.price}`}</span>;
            }
        } else {
            return <span>{`${data.currency} ${data && data.json && data.json.price}`}</span>;
        }
    } else {
        return <span>{`${data.currency} ${data && data.json && data.json.price}`}</span>;
    }
};

export const filterData = (products, filters) => {
    const filterKeys = Object.keys(filters);
    products = _.values(products);
    return products.filter((product) => {
        product = product.json;
        if (!filterKeys.length) return true;
        return filterKeys.every((key) => {
            if (!filters[key].length) return true;
            return filters[key].some((item) =>
                Object.keys(product.filtersdata[key]).some((productfilterdata, index) => {
                    let filterarray = [];
                    filterarray = productfilterdata.split(',');
                    if (filterarray.length > 0) {
                        if (filterarray.indexOf(item.value) !== -1) {
                            return true;
                        }
                    }
                    return false;
                }),
            );
        });
    });
};

export const sortByValue = (products, value, original) => {
    let sortedData = {};
    if (value === 'price_desc') {
        sortedData = _.values(products).sort((a, b) => b.json.price - a.json.price);
    } else if (value === 'price_asc') {
        sortedData = _.values(products).sort((a, b) => a.json.price - b.json.price);
    } else if (value === 'relevance') {
        sortedData = products;
    } else if (value === 'a-z') {
        sortedData = _.values(products).sort((a, b) => a.json.name.localeCompare(b.json.name));
    } else if (value === 'z-a') {
        sortedData = _.values(products).sort((a, b) => b.json.name.localeCompare(a.json.name));
    }

    return sortedData;
};

export const getUrlKey = () => {
    const pathArray = window.location.pathname.split('/');
    if (pathArray && pathArray.length >= 2) {
        if (pathArray[pathArray.length - 2] !== 'products') {
            return `${pathArray[pathArray.length - 2]}-${pathArray[pathArray.length - 1]}`;
        }
        return pathArray[pathArray && pathArray.length - 1];
    }
    return pathArray[pathArray && pathArray.length - 1];
};

export const getUrlKeyL3Menu = () => {
    let category_path = window.location.pathname.split('/');
    let url = '';
    if (category_path[category_path.length - 2] === 'products') {
        url = category_path[category_path.length - 1];
        return url;
    } else {
        if (category_path.length === 6) {
            if (
                category_path[category_path.length - 2] !== 'uae-en' &&
                category_path[category_path.length - 2] !== 'uae-ar' &&
                category_path[category_path.length - 2] !== 'saudi-en' &&
                category_path[category_path.length - 2] !== 'saudi-ar'
            ) {
                url =
                    category_path[category_path.length - 3] +
                    '-' +
                    category_path[category_path.length - 2] +
                    '-' +
                    category_path[category_path.length - 1];
                return url;
            }
        } else if (
            category_path[category_path.length - 2] !== 'uae-en' &&
            category_path[category_path.length - 2] !== 'uae-ar' &&
            category_path[category_path.length - 2] !== 'saudi-en' &&
            category_path[category_path.length - 2] !== 'saudi-ar'
        ) {
            url = category_path[category_path.length - 2] + '-' + category_path[category_path.length - 1];
            return url;
        } else {
            url = category_path[category_path.length - 1];
            return url;
        }
        return url;
    }
};
export const isUrlContainBrand = () => {
    const pathArray = window.location.pathname.split('/');
    if (pathArray && pathArray.includes('brand')) {
        return true;
    }
    return false;
};

export const getBrandAttributeKey = () => {
    const pathArray = window.location.pathname.split('/');
    return pathArray.pop();
};

export const _getUnique_filter = (item) => {
    let FilterArray = {};
    let age = [];
    let brand = [];
    let gender = [];
    let sub_categories = [];
    let Age = item.Age;
    let Brand = item.Brand;
    let Gender = item.Gender;
    let Sub_Category = item.Sub_Category;
    if (Age) {
        age = Array.from(new Set(Age.map((x) => x.name))).map((name) => {
            return {
                name: name,
                code: Age.find((x) => x.name === name).code,
                value: Age.find((x) => x.name === name).value,
            };
        });
    }

    if (Brand) {
        brand = Array.from(new Set(Brand.map((x) => x.name))).map((name) => {
            return {
                name: name,
                code: Brand.find((x) => x.name === name).code,
                value: Brand.find((x) => x.name === name).value,
            };
        });
    }

    if (Gender) {
        gender = Array.from(new Set(Gender.map((x) => x.name))).map((name) => {
            return {
                name: name,
                code: Gender.find((x) => x.name === name).code,
                value: Gender.find((x) => x.name === name).value,
            };
        });
    }
    if (Sub_Category) {
        sub_categories = Array.from(new Set(Sub_Category.map((x) => x.name))).map((name) => {
            return {
                name: name,
                code: Sub_Category.find((x) => x.name === name).code,
                value: Sub_Category.find((x) => x.name === name).value,
            };
        });
    }
    FilterArray = {
        Age: age,
        Brand: brand,
        Gender: gender,
        Sub_Category: sub_categories,
    };

    return FilterArray;
};
