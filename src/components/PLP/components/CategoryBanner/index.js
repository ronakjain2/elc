import React from 'react';
import Grid from '@material-ui/core/Grid';
import './categoryBanner.css';

export default function CategoryCategory(categoryImage) {
    return (
        <Grid container justify="center" className="PLPCategoryBanner">
            <Grid item xs={12} md={12}>
                <img src={categoryImage.categoryImage} alt="categoryImage" className="w-100" />
            </Grid>
        </Grid>
    );
}
