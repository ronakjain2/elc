import React from 'react';
import Hidden from '@material-ui/core/Hidden';
import Grid from '@material-ui/core/Grid';
import SortByFilter from '../SortBy';
import { FormattedMessage } from 'react-intl';
import Chip from '@material-ui/core/Chip';
import './applyFilter.css';

export default function ApplyFilters({ applyFilters, setApplyFilter, sortBy, setSortBy }) {
    const removeFilter = (value, code) => {
        if (applyFilters && applyFilters[code] && applyFilters[code].findIndex((x) => x.value === value) !== -1) {
            const index = applyFilters[code].findIndex((x) => x.value === value);
            applyFilters[code].splice(index, 1);
            setApplyFilter(applyFilters);
        }
    };

    const clearAllFilter = () => {
        setApplyFilter({});
    };

    return (
        <div className="selectedFilters">
            <Grid container justify="space-between" alignItems="center">
                <Grid item xs={10} className="FilterItem arabic-right">
                    {applyFilters &&
                        Object.keys(applyFilters).map(
                            (aF, index) =>
                                applyFilters[aF].length > 0 &&
                                index === 0 && (
                                    <span
                                        className="clearAll"
                                        key={`ClearAll_${index}`}
                                        onClick={() => clearAllFilter()}
                                    >
                                        <FormattedMessage id="PLP.ClearALL" defaultMessage="Clear All" />
                                    </span>
                                ),
                        )}

                    {applyFilters &&
                        Object.keys(applyFilters).map((aF) =>
                            applyFilters[aF].map((item, index) => (
                                <Chip
                                    key={`applyFilters_${index}_${item.code}`}
                                    label={item.name}
                                    onDelete={() => removeFilter(item.value, item.code)}
                                />
                            )),
                        )}
                </Grid>

                <Grid item xs={2}>
                    <Hidden only={['xs', 'sm']}>
                        <SortByFilter sortBy={sortBy} setSortBy={setSortBy} />
                    </Hidden>
                </Grid>
            </Grid>
        </div>
    );
}
