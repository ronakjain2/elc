import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Collapse from '@material-ui/core/Collapse';
import { FormattedMessage } from 'react-intl';
import './categoryDescription.css';

export default function CategoryDescription({ categoryDes }) {
    const [isMore, setMore] = useState(false);

    return (
        <Grid container>
            {categoryDes && categoryDes.length > 140 && (
                <>
                    {!isMore && (
                        <Typography className="PLPCategroyDescription-text text-align-rtl">
                            {categoryDes.substring(0, 140)}....
                            <span onClick={() => setMore(true)}>
                                <FormattedMessage id="PDP.ReadMore" defaultMessage="read more" />
                            </span>
                        </Typography>
                    )}
                    {isMore && (
                        <Collapse in={isMore}>
                            <Typography className="PLPCategroyDescription-text text-align-rtl">
                                {categoryDes}
                                &nbsp;
                                <span onClick={() => setMore(false)}>
                                    <FormattedMessage id="PDP.ReadLess" defaultMessage="read less" />
                                </span>
                            </Typography>
                        </Collapse>
                    )}
                </>
            )}
            {categoryDes && categoryDes.length <= 140 && (
                <Typography className="PLPCategroyDescription-text text-align-rtl">{categoryDes}</Typography>
            )}
        </Grid>
    );
}
