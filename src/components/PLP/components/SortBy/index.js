import React from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import { FormattedMessage } from 'react-intl';
import './sortBy.css';

export default function SortByFilter({ sortBy, setSortBy }) {
    return (
        <Grid container>
            <Grid item xs={12} className="sortByC ar-text-right">
                <Hidden only={['sm', 'xs']}>
                    <TextField
                        select
                        lable=""
                        value={sortBy}
                        onChange={(e) => setSortBy(e.target.value)}
                        variant="outlined"
                    >
                        <MenuItem value="relevance">
                            <span className="boldText">
                                <FormattedMessage id="PLP.Relevance" defaultMessage="Relevance" />
                            </span>
                        </MenuItem>
                        <MenuItem value="a-z">
                            <span className="boldText">
                                <FormattedMessage id="PLP.NameAtoZ" defaultMessage="Name (A-Z)" />
                            </span>
                        </MenuItem>
                        <MenuItem value="z-a">
                            <span className="boldText">
                                <FormattedMessage id="PLP.NameZtoA" defaultMessage="Name (Z-A)" />
                            </span>
                        </MenuItem>
                        <MenuItem value="price_asc">
                            <span className="boldText">
                                <FormattedMessage id="PLP.PriceLowtoHigh" defaultMessage="Price (lowest first)" />
                            </span>
                        </MenuItem>
                        <MenuItem value="price_desc">
                            <span className="boldText">
                                <FormattedMessage id="PLP.PriceHightoLow" defaultMessage="Price (highest first)" />
                            </span>
                        </MenuItem>
                    </TextField>
                </Hidden>
                <Hidden only={['md', 'lg', 'xl']}>
                    <FormControl component="fieldset" className="mobileSortByFilter">
                        <RadioGroup
                            aria-label="gender"
                            name="gender1"
                            value={sortBy}
                            onChange={(e) => setSortBy(e.target.value)}
                        >
                            <FormControlLabel
                                value="relevance"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'relevance' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage id="PLP.Relevance" defaultMessage="Relevance" />
                                    </span>
                                }
                            />
                            <FormControlLabel
                                value="a-z"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'a-z' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage id="PLP.NameAtoZ" defaultMessage="Name (A-Z)" />
                                    </span>
                                }
                            />
                            <FormControlLabel
                                value="z-a"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'z-a' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage id="PLP.NameZtoA" defaultMessage="Name (Z-A)" />
                                    </span>
                                }
                            />
                            <FormControlLabel
                                value="price_asc"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'price_asc' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage
                                            id="PLP.PriceLowtoHigh"
                                            defaultMessage="Price (lowest first)"
                                        />
                                    </span>
                                }
                            />
                            <FormControlLabel
                                value="price_desc"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'price_desc' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage
                                            id="PLP.PriceHightoLow"
                                            defaultMessage="Price (highest first)"
                                        />
                                    </span>
                                }
                            />
                        </RadioGroup>
                    </FormControl>
                </Hidden>
            </Grid>
        </Grid>
    );
}
