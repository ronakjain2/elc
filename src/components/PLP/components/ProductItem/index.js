import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Img from 'commonComponet/Image';
import { actions as signInActions } from 'components/SignIn_SignUp/redux/actions';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { productClickEvent } from 'services/GTMservice';
import { getAge, showDiscountPrice } from '../../utils';
import './productItem.css';

function ProductItem({ item, history, index, q }) {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const store_locale = state && state.auth && state.auth.store_locale;
    const json = item && item.json && item.json;
    const loginUser = state && state.auth && state.auth.loginUser;
    const customer_type = loginUser && loginUser.customer_type;
    const fclub_only_category = state && state.PLP && state.PLP.products && state.PLP.products.fclub_only_category;

    if (!json || !json.name) return null;

    const gotoPDP = () => {
        productClickEvent(item, index, q);
    };

    const getUrlLink = () => {
        if (fclub_only_category === '1' && (!loginUser || customer_type !== 'ecom_club_user')) {
            dispatch(
                signInActions.fcPopupSet({
                    fcPrivateCat: true,
                }),
            );
            setTimeout(() => dispatch(signInActions.fcPopupClose()), 100);
            return '#';
        }
        if (json.url_key) {
            return `/${store_locale}/products-details/${json && json.url_key}`;
        }
        return '#';
    };

    return (
        <Grid item xs={6} md={4} lg={3} className="PLPItem">
            <Link to={getUrlLink()} className="link-plp" onClick={gotoPDP}>
                <div className="productsItem">
                    {json.labels && (
                        <Typography
                            variant="span"
                            className="label"
                            style={{
                                background: json.label_bg_color || '#aa272f',
                                color: json.label_text_color || '#fff',
                            }}
                        >
                            {json.labels}
                        </Typography>
                    )}

                    <Img
                        width="184"
                        height="175"
                        src={json && json.imageUrl && json.imageUrl.primaryimage && json.imageUrl.primaryimage[0]}
                        alt={json && json.name}
                    />

                    <div className="text-height">
                        {json && json.name.length > 45 ? (
                            <Typography className="p_title">{`${json.name.substring(0, 45)}...`}</Typography>
                        ) : (
                            <Typography className="p_title">{json && json.name}</Typography>
                        )}
                    </div>
                    <Typography className="p_amt">{showDiscountPrice(item)}</Typography>

                    <Hidden only={['sm', 'xs']}>
                        <Typography className="p_months">{getAge(item)}</Typography>
                    </Hidden>
                    <Button className="addToBasket" style={{ marginTop: 0 }}>
                        <FormattedMessage id="Product.AddToBasket" defaultMessage="Add to basket" />
                    </Button>
                </div>
            </Link>
        </Grid>
    );
}

export default withRouter(ProductItem);
