import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';
import './noProduct.css';

function NoProducts({ q, history, message }) {
    const gotoHomePage = () => {
        return history.push(`/`);
    };

    return (
        <Grid container>
            {q && (
                <Grid container>
                    <Grid item xs={12}>
                        <Typography className="NoProductText">
                            <FormattedMessage id="No.Search.Item" defaultMessage="You searched for" />
                            {` "${q}"`}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography className="ZeroItemText">
                            <span>0</span>&nbsp;
                            <FormattedMessage id="PLP.Search.Zero.Item" defaultMessage="items found for keyword" />
                            &nbsp;<span>{q}</span>
                        </Typography>
                    </Grid>
                    <Grid item xs={12} className="textAlignCenter">
                        <Button className="continueShoppingButton" onClick={() => gotoHomePage()}>
                            <FormattedMessage id="Cart.ContinueShopping" defaultMessage="Continue Shopping" />
                        </Button>
                    </Grid>
                </Grid>
            )}
            {message && !q && (
                <Grid container justify="center" alignItems="center">
                    <Typography className="NoCategoryProducts">{message}</Typography>
                    <Grid item xs={12} className="textAlignCenter">
                        <Button className="continueShoppingButton" onClick={() => gotoHomePage()}>
                            <FormattedMessage id="Cart.ContinueShopping" defaultMessage="Continue Shopping" />
                        </Button>
                    </Grid>
                </Grid>
            )}
        </Grid>
    );
}

export default withRouter(NoProducts);
