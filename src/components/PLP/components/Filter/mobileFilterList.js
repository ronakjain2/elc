import React from 'react';
import Grid from '@material-ui/core/Grid';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import NavigateNext from '@material-ui/icons/NavigateNext';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

export default function MobileFilterList({
    applyFilters,
    filters,
    filterItem,
    onChangeFilter,
    setShowFilterList,
    setAnchor,
    setApplyFilter,
}) {
    const state = useSelector((state) => state);

    const language = state && state.auth && state.auth.language;

    return (
        <Grid container>
            <Grid item xs={12} className="FileterAndSortContainer">
                <Grid container justify="space-between" alignItems="center" style={{ padding: '0px 20px' }}>
                    <div onClick={() => setShowFilterList(false)}>
                        {language === 'en' ? <ArrowBackIos /> : <NavigateNext />}
                    </div>
                    <div>
                        <span>{filterItem}</span>
                    </div>
                    <div />
                </Grid>
            </Grid>
            <FormGroup row className="mobile_filter_text_container-1">
                {filters[filterItem] &&
                    filters[filterItem].map((valueItemx, indexV) => (
                        <Grid item xs={12} key={`filter_value_${indexV}`}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={
                                            (applyFilters &&
                                                applyFilters[valueItemx.code] &&
                                                applyFilters[valueItemx.code].findIndex(
                                                    (x) => x.value === valueItemx.value,
                                                ) !== -1) ||
                                            false
                                        }
                                        name="gilad"
                                        className="greeCheckbox"
                                        onChange={() =>
                                            onChangeFilter(valueItemx.code, valueItemx.value, valueItemx.name)
                                        }
                                    />
                                }
                                label={<Typography className="checkboxText">{valueItemx.name}</Typography>}
                            />
                        </Grid>
                    ))}
            </FormGroup>
            <Grid item xs={12}>
                <Grid container className="mobileFilterButtonConatiner" justify="space-between">
                    <div
                        className="mobileFilterButton mobileClearFilterButton"
                        onClick={() => {
                            setApplyFilter({});
                        }}
                    >
                        <FormattedMessage id="PLP.ClearALL" defaultMessage="Clear All" />
                    </div>
                    <div className="mobileFilterButton mobileApplyFilterButton" onClick={() => setAnchor(false)}>
                        <FormattedMessage id="PLP.ApplyFilter" defaultMessage="Apply Filter" />
                    </div>
                </Grid>
            </Grid>
        </Grid>
    );
}
