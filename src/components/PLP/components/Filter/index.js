import React, { useState } from 'react';
import Collapsible from 'react-collapsible';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Hidden from '@material-ui/core/Hidden';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { FormattedMessage } from 'react-intl';
import Close from '@material-ui/icons/Close';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import ArrowDropUp from '@material-ui/icons/ArrowDropUp';
import ArrowRight from '@material-ui/icons/ArrowRight';
import ArrowLeft from '@material-ui/icons/ArrowLeft';
import './filter.css';
import { useSelector } from 'react-redux';
import SortByFilter from '../SortBy';
import MobileFilterList from './mobileFilterList';

export default function PLPFilter({ filters, applyFilters, setApplyFilter, setPage, setAnchor, sortBy, setSortBy }) {
    const state = useSelector((state) => state);

    const language = state && state.auth && state.auth.language;

    const [isShowFilterList, setShowFilterList] = useState(false);
    const [selectedFilterName, setSelectedFilterName] = useState('');

    const onChangeFilter = (code, value, name) => {
        let set = false;
        if (!applyFilters) {
            applyFilters[code].push({
                value,
                name,
                code,
            });
            set = true;
        }
        if (applyFilters && !applyFilters[code] && !set) {
            applyFilters[code] = [];
            applyFilters[code].push({
                value,
                name,
                code,
            });
            set = true;
        }
        if (
            applyFilters &&
            applyFilters[code] &&
            applyFilters[code].findIndex((x) => x.value === value) !== -1 &&
            !set
        ) {
            const index = applyFilters[code].findIndex((x) => x.value === value);
            applyFilters[code].splice(index, 1);
            set = true;
        }
        if (
            applyFilters &&
            applyFilters[code] &&
            applyFilters[code].findIndex((x) => x.value === value) === -1 &&
            !set
        ) {
            applyFilters[code].push({
                value,
                name,
                code,
            });
            set = true;
        }

        setApplyFilter(applyFilters);
        setPage(1);
    };

    const mobileFilterList = (filterName) => {
        setSelectedFilterName(filterName);
        setShowFilterList(true);
    };

    const commonHtml = (filterItem, index) => (
        <Collapsible
            key={`filter_${index}`}
            open
            trigger={
                <Grid container justify="space-between" alignItems="center" className="Collapsible_text_container">
                    <div className="Collapsible_text">{filterItem}</div>
                    <div className="Collapsible_arrow_container">
                        <ArrowDropDown className="Icon" />
                    </div>
                </Grid>
            }
            triggerWhenOpen={
                <Grid container justify="space-between" alignItems="center" className="Collapsible_text_container">
                    <div className="Collapsible_text">{filterItem}</div>
                    <div className="Collapsible_arrow_container">
                        <ArrowDropUp className="Icon" />
                    </div>
                </Grid>
            }
        >
            <div className="filterpanal scroll-plp-filter">
                <FormGroup row>
                    {filters[filterItem] &&
                        filters[filterItem].map((valueItemx, indexV) => (
                            <Grid item xs={6} md={12} key={`filter_value_${index}_${indexV}`}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={
                                                (applyFilters &&
                                                    applyFilters[valueItemx.code] &&
                                                    applyFilters[valueItemx.code].findIndex(
                                                        (x) => x.value === valueItemx.value,
                                                    ) !== -1) ||
                                                false
                                            }
                                            name="gilad"
                                            className="greeCheckbox"
                                            onChange={() =>
                                                onChangeFilter(valueItemx.code, valueItemx.value, valueItemx.name)
                                            }
                                        />
                                    }
                                    label={<Typography className="checkboxText">{valueItemx.name}</Typography>}
                                />
                            </Grid>
                        ))}
                </FormGroup>
            </div>
        </Collapsible>
    );

    return (
        <div>
            <Hidden only={['xs', 'sm']}>
                <div className="WebFilter">
                    <div className="FilterAndClear">
                        <span className="f_Brand">
                            <FormattedMessage id="PLP.Filters" defaultMessage="Filters" />
                        </span>
                    </div>
                    <div className="FilterBrands">
                        {filters && Object.keys(filters).map((filterItem, index) => commonHtml(filterItem, index))}
                    </div>
                </div>
            </Hidden>

            <Hidden only={['lg', 'xl', 'md']}>
                <div style={{ width: '100%', height: '100vh' }}>
                    {!isShowFilterList && (
                        <Grid container className="mobile-filter-container">
                            <Grid item xs={12} className="MobileApplyFilter selectedAndCloseContainer">
                                <Grid container justify="space-between" alignItems="center">
                                    <Grid item xs={10} className="ar-text-right">
                                        {applyFilters && Object.keys(applyFilters).length === 0 && (
                                            <span className="noFilterSelected">No Filter Selected</span>
                                        )}
                                    </Grid>
                                    <div>
                                        <Grid container justify="flex-end" alignItems="center">
                                            <IconButton
                                                onClick={() => {
                                                    setAnchor(false);
                                                    setApplyFilter({});
                                                }}
                                            >
                                                <Close />
                                            </IconButton>
                                        </Grid>
                                    </div>
                                </Grid>
                            </Grid>

                            <Grid item xs={12} className="FileterAndSortContainer">
                                <FormattedMessage id="PLP.SortBy" defaultMessage="Sort By" />
                            </Grid>

                            <Grid item xs={12}>
                                <SortByFilter sortBy={sortBy} setSortBy={setSortBy} />
                            </Grid>

                            <Grid item xs={12} className="FileterAndSortContainer">
                                <FormattedMessage id="PLP.Filters" defaultMessage="Filters" />
                            </Grid>

                            <Grid item xs={12} className="mobile_filter_text_container">
                                {filters &&
                                    Object.keys(filters).map((filterItem, index) => (
                                        <Grid
                                            container
                                            key={`mobile_filter_${index}`}
                                            justify="space-between"
                                            alignItems="center"
                                            className="border-bottom"
                                            onClick={() => mobileFilterList(filterItem)}
                                        >
                                            <div>
                                                <Typography className="mobileFilter-text">{filterItem}</Typography>
                                            </div>
                                            <div>
                                                <IconButton size="small">
                                                    {language === 'en' ? <ArrowRight /> : <ArrowLeft />}
                                                </IconButton>
                                            </div>
                                        </Grid>
                                    ))}
                            </Grid>

                            <Grid item xs={12}>
                                <Grid container className="mobileFilterButtonConatiner" justify="space-between">
                                    <div
                                        className="mobileFilterButton mobileClearFilterButton"
                                        onClick={() => {
                                            setApplyFilter({});
                                        }}
                                    >
                                        <FormattedMessage id="PLP.ClearALL" defaultMessage="Clear All" />
                                    </div>
                                    <div
                                        className="mobileFilterButton mobileApplyFilterButton"
                                        onClick={() => setAnchor(false)}
                                    >
                                        <FormattedMessage id="PLP.ApplyFilter" defaultMessage="Apply Filter" />
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    )}

                    {isShowFilterList && (
                        <Grid container className="mobile-filter-container">
                            <MobileFilterList
                                applyFilters={applyFilters}
                                filters={filters}
                                filterItem={selectedFilterName}
                                onChangeFilter={onChangeFilter}
                                setShowFilterList={setShowFilterList}
                                setAnchor={setAnchor}
                                setApplyFilter={setApplyFilter}
                            />
                        </Grid>
                    )}
                </div>
            </Hidden>
        </div>
    );
}
