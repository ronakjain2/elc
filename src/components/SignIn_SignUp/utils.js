export const isEmail = (email) => {
    const mailformat = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*(\.\w{2,10})\s*$/;
    if (email.match(mailformat)) {
        return true;
    }
    return false;
};

export const checkPasswordContain = (password) => {
    const pattern = '^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d^a-zA-Z0-9].{7,50}$';
    if (password.match(pattern)) {
        return true;
    }

    return false;
};

export const checkPasswordForReset = (password) => {
    const pattern = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,30}$/;
    if (password.match(pattern)) {
        return true;
    }

    return false;
};

export const checkRegisterForm = (values, status) => {
    if (
        !values.firstName ||
        values.firstName === '' ||
        !values.lastName ||
        values.lastName === '' ||
        !values.email ||
        values.email === '' ||
        !values.phoneNumber ||
        values.phoneNumber === '' ||
        !values.password ||
        values.password === '' ||
        values.password.length < 8 ||
        !checkPasswordContain(values.password) ||
        !values.confirmPassword ||
        values.confirmPassword === '' ||
        values.confirmPassword !== values.password ||
        !status ||
        values.countryCode === ''
    ) {
        return true;
    }

    return false;
};

export const scrollToElement = (element) => {
    const elmnt = document.querySelector(element);
    elmnt && elmnt.scrollIntoView({ block: 'center', behavior: 'smooth' });
    return;
};
