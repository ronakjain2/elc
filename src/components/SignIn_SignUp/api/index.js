import api from 'apiServices';

const signIn = (payload) => api.post('/index.php/rest/V1/app/orob/login', payload);
const signUp = (payload) => api.post('/index.php/rest/V1/app/orob/register', payload);

const forgotPassword = (payload) => api.post(`/index.php/rest/V1/app/forgotpassword`, payload);

export default {
    signIn,
    signUp,
    forgotPassword,
};
