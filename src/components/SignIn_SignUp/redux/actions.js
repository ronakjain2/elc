import { createAction } from 'redux-actions';

// Action type
const SIGN_UP_REQUEST = 'ELC@OROB/SIGN_UP_REQUEST';
const SIGN_UP_REQUEST_SUCCESS = 'ELC@OROB/SIGN_UP_REQUEST_SUCCESS';
const SIGN_UP_REQUEST_FAILED = 'ELC@OROB/SIGN_UP_REQUEST_FAILED';

const SIGN_IN_REQUEST = 'ELC@OROB/SIGN_IN_REQUEST';
const SIGN_IN_REQUEST_SUCCESS = 'ELC@OROB/SIGN_IN_REQUEST_SUCCESS';
const SIGN_IN_REQUEST_FAILED = 'ELC@OROB/SIGN_IN_REQUEST_FAILED';

const PASSWORD_RESET_REQUEST = 'ELC@OROB/PASSWORD_RESET_REQUEST';
const PASSWORD_RESET_REQUEST_SUCCESS = 'ELC@OROB/PASSWORD_RESET_REQUEST_SUCCESS';
const PASSWORD_RESET_REQUEST_FAILED = 'ELC@OROB/PASSWORD_RESET_REQUEST_FAILED';

const RESET_SIGN_IN_SIGN_UP_ERROR = 'ELC@OROB/RESET_SIGN_IN_SIGN_UP_ERROR';

const FC_POPUP_SET = 'ELC@OROB/FC_POPUP_SET';
const FC_POPUP_CLOSE = 'ELC@OROB/FC_POPUP_CLOSE';

// Action method
const signUpRequest = createAction(SIGN_UP_REQUEST);
const signUpRequestSuccess = createAction(SIGN_UP_REQUEST_SUCCESS);
const signUpRequestFailed = createAction(SIGN_UP_REQUEST_FAILED);

const signInRequest = createAction(SIGN_IN_REQUEST);
const signInRequestSuccess = createAction(SIGN_IN_REQUEST_SUCCESS);
const signInRequestFailed = createAction(SIGN_IN_REQUEST_FAILED);

const passwordResetRequest = createAction(PASSWORD_RESET_REQUEST);
const passwordResetRequestSuccess = createAction(PASSWORD_RESET_REQUEST_SUCCESS);
const passwordResetRequestFailed = createAction(PASSWORD_RESET_REQUEST_FAILED);

const resetSignInSignUpError = createAction(RESET_SIGN_IN_SIGN_UP_ERROR);

const fcPopupSet = createAction(FC_POPUP_SET);
const fcPopupClose = createAction(FC_POPUP_CLOSE);

export const actions = {
    signUpRequest,
    signUpRequestSuccess,
    signUpRequestFailed,

    signInRequest,
    signInRequestSuccess,
    signInRequestFailed,

    passwordResetRequest,
    passwordResetRequestSuccess,
    passwordResetRequestFailed,

    resetSignInSignUpError,

    fcPopupSet,
    fcPopupClose,
};

export const types = {
    SIGN_UP_REQUEST,
    SIGN_UP_REQUEST_SUCCESS,
    SIGN_UP_REQUEST_FAILED,

    SIGN_IN_REQUEST,
    SIGN_IN_REQUEST_SUCCESS,
    SIGN_IN_REQUEST_FAILED,

    PASSWORD_RESET_REQUEST,
    PASSWORD_RESET_REQUEST_SUCCESS,
    PASSWORD_RESET_REQUEST_FAILED,

    RESET_SIGN_IN_SIGN_UP_ERROR,

    FC_POPUP_SET,
    FC_POPUP_CLOSE,
};
