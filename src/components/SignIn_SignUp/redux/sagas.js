import { actions as authActions } from 'components/Global/redux/actions';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import { call, delay, put, select, takeLatest } from 'redux-saga/effects';
import api from '../api';
import { actions, types } from './actions';

const auth = state => state && state.auth;

const loginReq = function* loginReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.signIn, payload);
        if (data && data.status) {
            yield put(actions.signInRequestSuccess());
            yield put(authActions.saveLoginUser(data.customer_details));
            yield put(authActions.saveQuoteIdRequest(data && data.customer_details && data.customer_details.quote_id));
            if (!data.has_fcsku && data.customer_details && data.customer_details.customer_type !== 'ecom_club_user') {
                yield put(
                    actions.fcPopupSet({
                        fcSignInPopup: true,
                    }),
                );
                yield put(actions.fcPopupClose());
            }
            if (data.x_store_register) {
                yield put(
                    actions.fcPopupSet({
                        fcSignInWithXStorePopup: true,
                    }),
                );
                yield put(actions.fcPopupClose());
            }
            yield put(push(`/${authData && authData.store_locale}/my-account`));
            toast.success(data.message);
        } else {
            toast.error(data.message);
            yield put(actions.signInRequestFailed({ message: data.message, phonePopupFlag: data.phone_exists }));
            yield delay(3000);
            yield put(actions.resetSignInSignUpError());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.signInRequestFailed('something went to wrong'));
        yield delay(3000);
        yield put(actions.resetSignInSignUpError());
    }
};

const registerReq = function* registerReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.signUp, payload);
        if (data && data.status) {
            yield put(actions.signUpRequestSuccess());
            yield put(authActions.saveLoginUser(data.customer_details));
            yield put(authActions.saveQuoteIdRequest(data && data.customer_details && data.customer_details.quote_id));
            if (payload.family_club_reg_flag === 0) {
                yield put(
                    actions.fcPopupSet({
                        fcSignUpPopup: true,
                    }),
                );
                yield put(actions.fcPopupClose());
                toast.success(data.message);
                yield put(push(`/${authData && authData.store_locale}/my-account`));
            } else {
                yield put(
                    actions.fcPopupSet({
                        fcSignUpWithFCPopup: true,
                    }),
                );
                yield put(actions.fcPopupClose());
                toast.success(data.message);
                if (data && data.customer_details && data.customer_details.customer_type === 'ecom_club_user') {
                    yield put(push(`/${authData && authData.store_locale}/my-account`));
                } else {
                    yield put(push(`/${authData && authData.store_locale}/cart`));
                }
            }
        } else {
            toast.error(data.message);
            yield put(actions.signUpRequestFailed(data.message));
            yield delay(3000);
            yield put(actions.resetSignInSignUpError());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.signUpRequestFailed('something went to wrong'));
        yield delay(3000);
        yield put(actions.resetSignInSignUpError());
    }
};

const passwordResetReq = function* passwordResetReq({ payload }) {
    try {
        const { data } = yield call(api.forgotPassword, payload);
        if (data && data.status) {
            toast.success(data.message);
            yield put(actions.passwordResetRequestSuccess());
        } else {
            toast.error(data.message);
            yield put(actions.passwordResetRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.passwordResetRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.SIGN_IN_REQUEST, loginReq);
    yield takeLatest(types.SIGN_UP_REQUEST, registerReq);
    yield takeLatest(types.PASSWORD_RESET_REQUEST, passwordResetReq);
}
