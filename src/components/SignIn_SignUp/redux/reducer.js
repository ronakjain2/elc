import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.SIGN_IN_REQUEST]: (state) => ({
        ...state,
        loginLoader: true,
        phonePopupFlag: true,
        error: false,
    }),
    [types.SIGN_IN_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loginLoader: false,
        error: true,
        phonePopupFlag: payload && payload.phonePopupFlag,
        message: payload && payload.message,
    }),
    [types.SIGN_IN_REQUEST_SUCCESS]: (state) => ({
        ...state,
        loginLoader: false,

        phonePopupFlag: true,
        error: false,
    }),

    [types.SIGN_UP_REQUEST]: (state) => ({
        ...state,
        registerLoader: true,
        error: false,
    }),
    [types.SIGN_UP_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        registerLoader: false,
        error: true,
        message: payload,
    }),
    [types.SIGN_UP_REQUEST_SUCCESS]: (state) => ({
        ...state,
        registerLoader: false,
        error: false,
    }),

    [types.PASSWORD_RESET_REQUEST]: (state) => ({
        ...state,
        forgotLoader: true,
    }),
    [types.PASSWORD_RESET_REQUEST_FAILED]: (state) => ({
        ...state,
        forgotLoader: false,
    }),
    [types.PASSWORD_RESET_REQUEST_SUCCESS]: (state) => ({
        ...state,
        forgotLoader: false,
    }),

    [types.RESET_SIGN_IN_SIGN_UP_ERROR]: (state) => ({
        ...state,
        error: false,
        message: null,
    }),
    [types.FC_POPUP_SET]: (state, { payload }) => ({
        ...state,
        ...payload,
    }),
    [types.FC_POPUP_CLOSE]: (state) => ({
        ...state,
        fcSignInPopup: false,
        fcSignUpPopup: false,
        fcSignInWithXStorePopup: false,
        fcSignUpWithFCPopup: false,
        fcPrivateCat: false,
    }),
};

export default handleActions(actionHandler, {
    registerLoader: false,
    loginLoader: false,
    error: false,
    phonePopupFlag: true,
    forgotLoader: false,
    fcSignInPopup: false,
    fcSignUpPopup: false,
    fcSignUpWithFCPopup: false,
    fcPrivateCat: false,
});
