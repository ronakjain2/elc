import React from 'react';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Typography from '@material-ui/core/Typography';
import './styles.css';
import { FormattedMessage } from 'react-intl';

export default function IconInfoSignInSignUp() {
    return (
        <Grid container justify="space-around">
            <Grid item xs={6} md={3} className="flex alignItemCenter justifyCenter login-icon-m-t-b">
                <Fab className="login-fab-icon-container">
                    <img src="/images/login/Icon_Login_1_User.svg" alt="userIcon" />
                </Fab>
                <Typography className="login-fab-icon-container-text">
                    <FormattedMessage
                        id="SignUpSignIn.User.Remember"
                        defaultMessage="We will remember your details for next time"
                    />
                </Typography>
            </Grid>

            <Grid item xs={6} md={3} className="flex alignItemCenter justifyCenter login-icon-m-t-b">
                <Fab className="login-fab-icon-container">
                    <img src="/images/login/Icon_Login_2_Birthday.svg" alt="userIcon" />
                </Fab>
                <Typography className="login-fab-icon-container-text">
                    <FormattedMessage
                        id="SignUpSignIn.User.JoinBirthday"
                        defaultMessage="You can join our Birthday Club to receive special birthday offers"
                    />
                </Typography>
            </Grid>

            <Grid item xs={6} md={3} className="flex alignItemCenter justifyCenter login-icon-m-t-b">
                <Fab className="login-fab-icon-container">
                    <img src="/images/login/Icon_Login_3_Heart.svg" alt="userIcon" />
                </Fab>
                <Typography className="login-fab-icon-container-text">
                    <FormattedMessage
                        id="SignUpSignIn.User.FavouriteStore"
                        defaultMessage="Save details for your favourite store"
                    />
                </Typography>
            </Grid>

            <Grid item xs={6} md={3} className="flex alignItemCenter justifyCenter login-icon-m-t-b">
                <Fab className="login-fab-icon-container">
                    <img src="/images/login/Icon_Login_4_Cart.svg" alt="userIcon" />
                </Fab>
                <Typography className="login-fab-icon-container-text">
                    <FormattedMessage
                        id="SignUpSignIn.User.CheckoutFast"
                        defaultMessage="You can get through the checkout faster"
                    />
                </Typography>
            </Grid>
        </Grid>
    );
}
