import React, { useState, lazy, Suspense, useCallback, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormattedMessage } from 'react-intl';
import { isEmail, scrollToElement } from 'components/SignIn_SignUp/utils';
import { useSelector, useDispatch } from 'react-redux';
import { actions as signInSignUpActions } from 'components/SignIn_SignUp/redux/actions';
import PhoneNumber from 'commonComponet/PhoneNumber';
const ForgotPassword = lazy(() => import('./POP/ForgotPassword'));

export default function Login() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const guestUser = state && state.auth && state.auth.guestUser;
    const quote_id = state && state.auth && state.auth.quote_id;

    const [values, setValues] = useState({
        email: '',
        password: '',
        carrier_code: '',
        phone_number: '',
    });
    const [isValidate, setIsValidate] = useState(false);
    const [open, setOpen] = useState(false);
    const phonePopupValue = state && state.signInSignUp && state.signInSignUp.phonePopupFlag;
    const [phonePopupFlag, setPhonePopupFlag] = useState(false);
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const onChangeHandler = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value });
    };

    const onSubmit = () => {
        setIsValidate(true);
        if (values.carrier_code === '' && values.phone_number === '') {
            if (!isEmail(values.email) || values.password === '') {
                scrollToElement('.input-error');
                return;
            }
        }
        if (values.carrier_code !== '' && values.phone_number !== '') {
            if (
                !isEmail(values.email) ||
                values.password === '' ||
                values.phone_number === '' ||
                values.carrier_code === '' ||
                !isCorrectPhone
            ) {
                scrollToElement('.input-error');
                return;
            }
        }

        setIsValidate(false);
        dispatch(
            signInSignUpActions.signInRequest({
                email: values.email,
                password: values.password,
                guestquote: quote_id ? quote_id : guestUser && guestUser.guestId,
                carrier_code: values.carrier_code ? values.carrier_code : '',
                contact_number: values.phone_number ? values.phone_number : '',
            }),
        );
    };

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            onSubmit();
        }
    };
    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...values,
            phone_number: phoneNo.phone,
            carrier_code: phoneNo.dialCode,
        };
        setValues(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const checkPhoneExistStatus = useCallback(() => {
        if (phonePopupValue === false) {
            setPhonePopupFlag(true);
        }
    }, [phonePopupValue]);

    useEffect(() => {
        try {
            checkPhoneExistStatus();
        } catch (err) {}
    }, [checkPhoneExistStatus]);
    return (
        <Grid container justify="center">
            <Suspense fallback={'Loading...'}>
                <ForgotPassword open={open} setOpen={setOpen} />
            </Suspense>
            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.EmailAddress" defaultMessage="Email Address*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.email}
                    onChange={onChangeHandler('email')}
                    fullWidth
                    onKeyPress={(e) => handleKeyDown(e)}
                    className="inputBox"
                    error={isValidate && !isEmail(values.email)}
                />
                {isValidate && !isEmail(values.email) && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Email.Error" defaultMessage="Please Enter Valid Email Address" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.Password" defaultMessage="Password*" />
                </Typography>
                <TextField
                    variant="outlined"
                    type="password"
                    className="inputBox"
                    onKeyPress={(e) => handleKeyDown(e)}
                    value={values.password}
                    onChange={onChangeHandler('password')}
                    fullWidth
                    error={isValidate && values.password === ''}
                />

                {isValidate && values.password === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Password.Error" defaultMessage="Please Enter Valid password" />
                    </FormHelperText>
                )}
            </Grid>
            {phonePopupFlag === true && (
                <Grid item xs={11} style={{ paddingBottom: 0 }} className="padding-t-input-box">
                    <Typography className="login-sigup-input-text">
                        <FormattedMessage id="SignInSignUp.ContactNumber" defaultMessage="Contact Number*" />
                    </Typography>
                    <PhoneNumber
                        parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                        carrier_code={values.carrier_code}
                        phoneNo={values.phone_number}
                    />
                    {isValidate && values.phone_number === '' && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="Common.PhoneNumber.Empty"
                                defaultMessage="Please enter contact number"
                            />
                        </FormHelperText>
                    )}
                    {isValidate && values.phone_number !== '' && !isCorrectPhone && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="Common.PhoneNumber.InValid"
                                defaultMessage="Please enter contact number"
                            />
                        </FormHelperText>
                    )}
                </Grid>
            )}

            <Grid item xs={11}>
                <Typography className="login-signup-forgort-password" onClick={() => setOpen(true)}>
                    <FormattedMessage id="SignInSignUp.ForgotPassword" defaultMessage="Forgot password?" />
                </Typography>
            </Grid>

            {state && state.signInSignUp && !state.signInSignUp.loginLoader && (
                <Grid item xs={11}>
                    <Button className="login-signup-login-button" onClick={() => onSubmit()}>
                        <FormattedMessage id="SignInSignUp.Button.Login" defaultMessage="Login" />
                    </Button>
                </Grid>
            )}
            {state && state.signInSignUp && state.signInSignUp.error && (
                <Grid item xs={11}>
                    <FormHelperText className="input-error" style={{ textAlign: 'center', fontSize: '1rem' }}>
                        {state.signInSignUp.message}
                    </FormHelperText>
                </Grid>
            )}
            {state && state.signInSignUp && state.signInSignUp.loginLoader && (
                <Grid container justify="center" alignItems="center">
                    <CircularProgress />
                </Grid>
            )}
        </Grid>
    );
}
