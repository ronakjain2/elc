import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import Cancel from '@material-ui/icons/Cancel';

import { FormattedMessage } from 'react-intl';
import { isEmail } from 'components/SignIn_SignUp/utils';
import PhoneNumber from 'commonComponet/PhoneNumber';
import { useSelector, useDispatch } from 'react-redux';
import { actions as signInSignUpActions } from 'components/SignIn_SignUp/redux/actions';

export default function ForgotPassword({ open, setOpen }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const handleClose = () => {
        setOpen(false);
    };

    const loader = state && state.signInSignUp && state.signInSignUp.forgotLoader;
    const store_id = state && state.auth && state.auth.currentStore;

    const [values, setValues] = useState({
        email: '',
        phoneNumber: '',
        countryCode: '',
    });
    const [isValidate, setIsValidate] = useState(false);
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...values,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setValues(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onChangeHandler = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value });
    };

    const onClickSend = () => {
        setIsValidate(true);
        if (!isEmail(values.email) || values.phoneNumber === '' || values.countryCode === '' || !isCorrectPhone) {
            return;
        }
        setIsValidate(false);
        dispatch(
            signInSignUpActions.passwordResetRequest({
                contact_number: values.phoneNumber,
                carrier_code: values.countryCode,
                email: values.email,
                store_id: store_id,
            }),
        );
    };

    useEffect(() => {
        if (!loader) {
            setOpen(false);
        }
    }, [loader, setOpen]);

    return (
        <Grid container>
            <Grid item xs={12}>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    className="forgot-pass"
                >
                    <DialogTitle>
                        <Grid container justify="space-between" alignItems="center">
                            <div>
                                <FormattedMessage id="Login.PasswordReset.Text" defaultMessage="Password Reset" />
                            </div>
                            <div>
                                <IconButton onClick={() => handleClose()}>
                                    <Cancel />
                                </IconButton>
                            </div>
                        </Grid>
                    </DialogTitle>
                    <DialogContent>
                        <Grid container justify="center">
                            <Grid item xs={11}>
                                <Typography className="login-sigup-input-text">
                                    <FormattedMessage id="SignInSingUp.Email" defaultMessage="Email*" />
                                </Typography>
                                <TextField
                                    variant="outlined"
                                    value={values.email}
                                    onChange={onChangeHandler('email')}
                                    fullWidth
                                    className="inputBox"
                                    error={isValidate && !isEmail(values.email)}
                                />
                                {isValidate && !isEmail(values.email) && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.Email.Error"
                                            defaultMessage="Please Enter Valid Email Address"
                                        />
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={11} className="padding-t-input-box">
                                <Typography className="login-sigup-input-text">
                                    <FormattedMessage
                                        id="SignInSignUp.ContactNumber"
                                        defaultMessage="Contact Number*"
                                    />
                                </Typography>
                                <PhoneNumber
                                    parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                                    carrier_code={values.countryCode}
                                    phoneNo={values.phoneNumber}
                                />
                                {isValidate && values.phoneNumber === '' && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.PhoneNumber.Empty"
                                            defaultMessage="Please enter contact number"
                                        />
                                    </FormHelperText>
                                )}
                                {isValidate && values.phoneNumber !== '' && !isCorrectPhone && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.PhoneNumber.InValid"
                                            defaultMessage="Please enter contact number"
                                        />
                                    </FormHelperText>
                                )}
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Grid container justify="center">
                            <Grid item xs={11} className="textAlignCenter">
                                {!loader && (
                                    <Button className="forgot-password-send-button" onClick={() => onClickSend()}>
                                        <FormattedMessage id="Login.PasswordReset.Send" defaultMessage="Send" />
                                    </Button>
                                )}
                                {loader && <CircularProgress />}
                            </Grid>
                        </Grid>
                    </DialogActions>
                </Dialog>
            </Grid>
        </Grid>
    );
}
