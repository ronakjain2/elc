import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormattedMessage } from 'react-intl';
import { isEmail, checkPasswordContain, checkRegisterForm, scrollToElement } from 'components/SignIn_SignUp/utils';
import PhoneNumber from 'commonComponet/PhoneNumber';
import { useDispatch, useSelector } from 'react-redux';
import { actions as signInSignUpActions } from 'components/SignIn_SignUp/redux/actions';

export default function SignUp() {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    //const guestUser = state && state.auth && state.auth.guestUser;
    const quote_id = state && state.auth && state.auth.quote_id;
    const fc_status = state && state.auth && state.auth.fc_status;

    const [values, setValues] = useState({
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        password: '',
        countryCode: '',
        confirmPassword: '',
    });
    const [checkFamilyClub, setCheckFamilyClub] = useState(false);
    const [isValidate, setIsValidate] = useState(false);
    const [receiveEmail, setReceiveEmail] = useState(true);
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...values,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setValues(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onChangeHandler = (name) => (event) => {
        if (name === 'firstName' || name === 'lastName') {
            const re = /^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g;
            if (event.target.value && !re.test(event.target.value)) {
                return '';
            }
        }
        const re_1 = /^(?!\s*$).+/g;
        if (event.target.value && !re_1.test(event.target.value)) {
            return '';
        }
        setValues({ ...values, [name]: event.target.value });
    };

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            onSubmit();
        }
    };

    const onSubmit = () => {
        setIsValidate(true);
        if (checkRegisterForm(values, isCorrectPhone)) {
            scrollToElement('.input-error');
            return;
        }
        setIsValidate(false);
        dispatch(
            signInSignUpActions.signUpRequest({
                firstname: values.firstName,
                lastname: values.lastName,
                email: values.email,
                password: values.password,
                confirmpassword: values.confirmPassword,
                store_id: state && state.auth && state.auth.currentStore,
                quest_quote: quote_id ? quote_id : '',
                subscribe_to_newsletter: receiveEmail ? 1 : 0,
                contact_number: values.phoneNumber,
                carrier_code: values.countryCode,
                family_club_reg_flag: checkFamilyClub ? 1 : 0,
            }),
        );
    };

    return (
        <Grid container justify="center">
            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.FirstName" defaultMessage="First Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.firstName}
                    onChange={onChangeHandler('firstName')}
                    fullWidth
                    onKeyPress={(e) => handleKeyDown(e)}
                    className="inputBox"
                    error={isValidate && values.firstName === ''}
                    inputProps={{ maxLength: 20 }}
                />
                {isValidate && values.firstName === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.FirstName.Error" defaultMessage="Please enter first name" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.LastName" defaultMessage="Last Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.lastName}
                    onChange={onChangeHandler('lastName')}
                    fullWidth
                    onKeyPress={(e) => handleKeyDown(e)}
                    className="inputBox"
                    error={isValidate && values.lastName === ''}
                    inputProps={{ maxLength: 19 }}
                />
                {isValidate && values.lastName === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.LastName.Error" defaultMessage="Please enter last name" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.ContactNumber" defaultMessage="Contact Number*" />
                </Typography>
                <PhoneNumber
                    parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                    carrier_code={values.countryCode}
                    phoneNo={values.phoneNumber}
                />
                {isValidate && values.phoneNumber === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.PhoneNumber.Empty" defaultMessage="Please enter contact number" />
                    </FormHelperText>
                )}
                {isValidate && values.phoneNumber !== '' && !isCorrectPhone && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="Common.PhoneNumber.InValid"
                            defaultMessage="Please enter contact number"
                        />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSingUp.Email" defaultMessage="Email*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.email}
                    onChange={onChangeHandler('email')}
                    fullWidth
                    onKeyPress={(e) => handleKeyDown(e)}
                    className="inputBox"
                    error={isValidate && !isEmail(values.email)}
                />
                {isValidate && !isEmail(values.email) && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Email.Error" defaultMessage="Please Enter Valid Email Address" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.Password" defaultMessage="Password*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.password}
                    onChange={onChangeHandler('password')}
                    fullWidth
                    onKeyPress={(e) => handleKeyDown(e)}
                    type="password"
                    className="inputBox"
                    error={
                        isValidate &&
                        (values.password === '' || values.password.length < 8 || !checkPasswordContain(values.password))
                    }
                />
                {/** Password empty message */}
                {isValidate && values.password === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="SignInSignUp.Password.Empty" defaultMessage="Please enter password" />
                    </FormHelperText>
                )}
                {/** Password length less than 8 message */}
                {isValidate && values.password !== '' && values.password.length < 8 && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="SignInSignUp.Password.length"
                            defaultMessage="Minimum length is 8 characters"
                        />
                    </FormHelperText>
                )}
                {/** Password contain message */}
                <FormHelperText
                    className={
                        isValidate &&
                        values.password !== '' &&
                        values.password.length >= 8 &&
                        !checkPasswordContain(values.password)
                            ? 'password-strong-message input-error'
                            : 'password-strong-message'
                    }
                >
                    <FormattedMessage
                        id="SignInSignUp.password.strong.message"
                        defaultMessage="Password must be at least 8 characters long and contain an uppercase letter, a lowercase letter and a number."
                    />
                </FormHelperText>
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.ConfirmPassword" defaultMessage="Confirm Password*" />
                </Typography>
                <TextField
                    variant="outlined"
                    value={values.confirmPassword}
                    onChange={onChangeHandler('confirmPassword')}
                    fullWidth
                    type="password"
                    className="inputBox"
                    error={isValidate && (values.confirmPassword === '' || values.confirmPassword !== values.password)}
                />
                {isValidate && values.confirmPassword === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="SignInSignUp.ConfirmPassword.Empty"
                            defaultMessage="Please enter password again"
                        />
                    </FormHelperText>
                )}
                {isValidate && values.confirmPassword !== '' && values.confirmPassword !== values.password && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="SignInSignUp.ConfirmPassword.NotSame"
                            defaultMessage="Password and Confirm password must be same"
                        />
                    </FormHelperText>
                )}
            </Grid>
            {fc_status && (
                <Grid item xs={11} className="text-align-rtl mt-2">
                    <FormControl>
                        <FormGroup>
                            <FormControlLabel
                                className="signIn-signUp-fc-checkbox"
                                control={
                                    <Checkbox
                                        checked={checkFamilyClub}
                                        onChange={(e) => setCheckFamilyClub(!checkFamilyClub)}
                                        name="gilad1"
                                    />
                                }
                                label={
                                    <FormattedMessage
                                        id="SignInSignUp.CheckBox.FcText"
                                        defaultMessage="Sign me up for Family Club membership"
                                    />
                                }
                            />
                        </FormGroup>
                    </FormControl>
                </Grid>
            )}
            <Grid item xs={11} className="mt-2">
                <FormControl>
                    <FormGroup>
                        <FormControlLabel
                            className="signIn-signUp-checkbox"
                            control={
                                <Checkbox
                                    checked={receiveEmail}
                                    onChange={(e) => setReceiveEmail(e.target.checked)}
                                    name="gilad"
                                />
                            }
                            label={
                                <Grid container className="text-align-rtl">
                                    <FormattedMessage
                                        id="SignInSignUp.CheckBox.Text"
                                        defaultMessage="I'd like to receive emails from ELC about special offers, new toys and voucher codes."
                                    />
                                </Grid>
                            }
                        />
                    </FormGroup>
                </FormControl>
            </Grid>

            <Grid item xs={11}>
                <ul className="signin-signup-ul-list">
                    <li>
                        <FormattedMessage
                            id="SignInSignUp.EmailPolicy"
                            defaultMessage="You can ask us to stop at any time and will never sell your data to other companies for marketing purpose."
                        />
                    </li>
                    <li>
                        <FormattedMessage
                            id="SignInSignUp.EmailPolicys"
                            defaultMessage="We always try to send emails that are relevant to you based on products you have shown an interest in."
                        />
                    </li>
                </ul>
            </Grid>

            {state && state.signInSignUp && !state.signInSignUp.registerLoader && (
                <Grid item xs={11}>
                    <Button className="login-signup-login-button m-t-15" onClick={() => onSubmit()}>
                        <FormattedMessage id="SignInSignUp.Register" defaultMessage="Register" />
                    </Button>
                </Grid>
            )}
            {state && state.signInSignUp && state.signInSignUp.error && (
                <Grid item xs={11}>
                    <FormHelperText className="input-error" style={{ textAlign: 'center', fontSize: '1rem' }}>
                        {state.signInSignUp.message}
                    </FormHelperText>
                </Grid>
            )}

            {state && state.signInSignUp && state.signInSignUp.registerLoader && (
                <Grid container justify="center" alignItems="center">
                    <CircularProgress />
                </Grid>
            )}
        </Grid>
    );
}
