import React, { lazy, Suspense, useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import './main_styles.css';
import Spinner from 'commonComponet/Spinner';
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router';

const IconInfoSignInSignUp = lazy(() => import('./components/Info'));
const Login = lazy(() => import('./components/Login'));
const SignUp = lazy(() => import('./components/SignUp'));

function SignInSignUp({ history }) {
    const state = useSelector((state) => state);

    const [activeTab, setActiveTab] = useState('login');

    const loginUser = state && state.auth && state.auth.loginUser;
    const store_locale = state && state.auth && state.auth.store_locale;

    const OnChangeTab = (name) => {
        setActiveTab(name);
    };

    //If user already login move to account page
    useEffect(() => {
        try {
            if (loginUser && loginUser.customer_id && loginUser.customer_id !== '') {
                history.push(`/${store_locale}/my-account`);
            }
        } catch (err) {}
    }, [loginUser, history, store_locale]);

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container className="signIn-signUp" justify="center">
                <Grid item xs={12}>
                    <Grid container justify="center" alignItems="center">
                        <Typography className="newToElc">
                            <FormattedMessage id="SignUpSignIn.NewToELC" defaultMessage="New to ELC" />
                        </Typography>
                    </Grid>
                </Grid>

                <Grid item xs={11} md={11} lg={10}>
                    <IconInfoSignInSignUp />
                </Grid>

                <Grid item xs={12} className="login-signup-margin-t">
                    <Grid container justify="center">
                        <Grid item xs={11} md={8} lg={4} className="login-register-container">
                            <Grid container justify="center" alignItems="center">
                                <Grid item xs={6} onClick={() => OnChangeTab('login')}>
                                    <Typography
                                        className={
                                            activeTab === 'login'
                                                ? 'flex justifyCenter alignItemCenter login-register-container-text activeloginTabText'
                                                : 'flex justifyCenter alignItemCenter login-register-container-text'
                                        }
                                    >
                                        <FormattedMessage id="SignInSignUp.Login" defaultMessage="Login" />
                                    </Typography>
                                    {activeTab === 'login' && <div className="login-register-active-line" />}
                                </Grid>
                                <Grid item xs={6} onClick={() => OnChangeTab('register')}>
                                    <Typography
                                        className={
                                            activeTab === 'register'
                                                ? 'flex justifyCenter alignItemCenter login-register-container-text activeloginTabText'
                                                : 'flex justifyCenter alignItemCenter login-register-container-text'
                                        }
                                    >
                                        <FormattedMessage id="SignInSignUp.Register" defaultMessage="Register" />
                                    </Typography>
                                    {activeTab === 'register' && <div className="login-register-active-line" />}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    <Grid container justify="center">
                        <Grid item xs={12} md={8} lg={4}>
                            {activeTab === 'login' && <Login />}
                            {activeTab === 'register' && <SignUp />}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Suspense>
    );
}

export default withRouter(SignInSignUp);
