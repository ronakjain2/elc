import api from 'apiServices';

const getStoreInfo = (storeData) => api.get(`/rest/V1/app/storeinfo?store_data=${storeData}`);
const storeChange = (payload) => api.post('/rest/V1/app/Storechange', payload);
const getGuestCart = (storeName) => api.post(`/index.php/rest/${storeName}/V1/guest-carts`);

//Reservation
const getReservationConfig = () => api.get(`/index.php/rest/V1/app/orob/getreservationconfig`);
const addAndupdateReservation = (payload) => api.post(`/index.php/rest/V1/app/mkt/webreservation`, payload);
const removeReservation = (payload) => api.post(`/index.php/rest/V1/app/orob/removereservation`, payload);
const extendReservationTime = (payload) => api.post(`/index.php/rest/V1/app/orob/extendreservation`, payload);

const getFCConfig = ({ store_id, customer_id }) =>
    api.get(`/rest/V1/app/fc/getfcenable?store_id=${store_id}&customer_id=${customer_id}`);

export default {
    getStoreInfo,
    storeChange,
    getGuestCart,

    getReservationConfig,
    addAndupdateReservation,
    removeReservation,
    extendReservationTime,

    getFCConfig,
};
