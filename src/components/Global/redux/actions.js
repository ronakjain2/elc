import { createAction } from 'redux-actions';

// actions types
const SET_STORE_INFO_REQUEST = 'ELC@OROB/SET_STORE_INFO_REQUEST';
const GET_STORE_INFO_REQUEST = 'ELC@OROB/GET_STORE_INFO_REQUEST';
const STORE_CHANGE_REQUEST = 'ELC@OROB/STORE_CHANGE_REQUEST';
const SAVE_LOGIN_USER = 'ELC@OROB/SAVE_LOGIN_USER';
const UPDATE_CUSTOMER_TYPE = 'ELC@OROB/UPDATE_CUSTOMER_TYPE';
const SAVE_GUEST_USER = 'ELC@OROB/SAVE_GUEST_USER';
const GET_GUEST_CART_REQUEST = 'ELC@OROB/GET_GUEST_CART_REQUEST';

const SAVE_QUOTE_ID_REQUEST = 'ELC@OROB/SAVE_QUOTE_ID_REQUEST';

const SET_DEFAULT_COUNTRY_POP = 'ELC@OROB/SET_DEFAULT_COUNTRY_POP';

const SET_SUGGESTION_CATEGORY_ID = 'ELC@OROB/SET_SUGGESTION_CATEGORY_ID';

const REQUEST_SUCCESS = 'ELC@OROB/REQUEST_SUCCESS';
const REQUEST_FAILED = 'ELC@OROB/REQUEST_FAILED';

//Reservation
const GET_RESERVATION_CONFIG_REQUEST = 'ELC@OROB/GET_RESERVATION_CONFIG_REQUEST';
const GET_RESERVATION_CONFIG_REQUEST_SUCCESS = 'ELC@OROB/GET_RESERVATION_CONFIG_REQUEST_SUCCESS';
const GET_RESERVATION_CONFIG_REQUEST_FAILED = 'ELC@OROB/GET_RESERVATION_CONFIG_REQUEST_FAILED';

const ADD_UPDATE_RESERVATION_REQUEST = 'ELC@OROB/ADD_UPDATE_RESERVATION_REQUEST';
const ADD_UPDATE_RESERVATION_REQUEST_SUCCESS = 'ELC@OROB/ADD_UPDATE_RESERVATION_REQUEST_SUCCESS';
const ADD_UPDATE_RESERVATION_REQUEST_FAILED = 'ELC@OROB/ADD_UPDATE_RESERVATION_REQUEST_FAILED';

const REMOVE_RESERVATION_REQUEST = 'ELC@OROB/REMOVE_RESERVATION_REQUEST';
const REMOVE_RESERVATION_REQUEST_SUCCESS = 'ELC@OROB/REMOVE_RESERVATION_REQUEST_SUCCESS';
const REMOVE_RESERVATION_REQUEST_FAILED = 'ELC@OROB/REMOVE_RESERVATION_REQUEST_FAILED';

const EXTEND_RESERVATION_TIME_REQUEST = 'ELC@OROB/EXTEND_RESERVATION_TIME_REQUEST';
const EXTEND_RESERVATION_TIME_REQUEST_SUCCESS = 'ELC@OROB/EXTEND_RESERVATION_TIME_REQUEST_SUCCESS';
const EXTEND_RESERVATION_TIME_REQUEST_FAILED = 'ELC@OROB/EXTEND_RESERVATION_TIME_REQUEST_FAILED';

const SAVE_RESERVATION_START_TIME_REQUEST = 'ELC@OROB/SAVE_RESERVATION_START_TIME_REQUEST';

const DONATE_TOYS_CHECKBOX = 'ELC@OROB/DONATE_TOYS_CHECKBOX';
const SET_RECENT_ORDER_ID = 'SET_RECENT_ORDER_ID';

const GET_FC_CONFIG_REQ = 'ELC@OROB/GET_FC_CONFIG_REQ';
const GET_FC_CONFIG_SUCCESS = 'ELC@OROB/GET_FC_CONFIG_SUCCESS';
const GET_FC_CONFIG_FAILED = 'ELC@OROB/GET_FC_CONFIG_FAILED';
const DISABLE_LOADER = 'ELC@OROB/DISABLE_LOADER';

// action methods
const setStoreInfoRequest = createAction(SET_STORE_INFO_REQUEST);
const getStoreInfoRequest = createAction(GET_STORE_INFO_REQUEST);
const storeChangeRequest = createAction(STORE_CHANGE_REQUEST);
const saveLoginUser = createAction(SAVE_LOGIN_USER);
const updateCustomerType = createAction(UPDATE_CUSTOMER_TYPE);
const saveGuestUser = createAction(SAVE_GUEST_USER);
const getGuestCartRequest = createAction(GET_GUEST_CART_REQUEST);

const saveQuoteIdRequest = createAction(SAVE_QUOTE_ID_REQUEST);

const requestSuccess = createAction(REQUEST_SUCCESS);
const requestFailed = createAction(REQUEST_FAILED);

const disableLoaders = createAction(DISABLE_LOADER);

const setDefaultCountryPop = createAction(SET_DEFAULT_COUNTRY_POP);

const getReservationConfigRequest = createAction(GET_RESERVATION_CONFIG_REQUEST);
const getReservationConfigRequestSuccess = createAction(GET_RESERVATION_CONFIG_REQUEST_SUCCESS);
const getReservationConfigRequestFailed = createAction(GET_RESERVATION_CONFIG_REQUEST_FAILED);

const addUpdateReservationRequest = createAction(ADD_UPDATE_RESERVATION_REQUEST);
const addUpdateReservationRequestSuccess = createAction(ADD_UPDATE_RESERVATION_REQUEST_SUCCESS);
const addUpdateReservationRequestFailed = createAction(ADD_UPDATE_RESERVATION_REQUEST_FAILED);

const removeReservationRequest = createAction(REMOVE_RESERVATION_REQUEST);
const removeReservationRequestSuccess = createAction(REMOVE_RESERVATION_REQUEST_SUCCESS);
const removeReservationRequestFailed = createAction(REMOVE_RESERVATION_REQUEST_FAILED);

const extendReservationTimeRequest = createAction(EXTEND_RESERVATION_TIME_REQUEST);
const extendReservationTimeRequestSuccess = createAction(EXTEND_RESERVATION_TIME_REQUEST_SUCCESS);
const extendReservationTimeRequestFailed = createAction(EXTEND_RESERVATION_TIME_REQUEST_FAILED);

const saveReservationStartTime = createAction(SAVE_RESERVATION_START_TIME_REQUEST);

const setSuggestionCategoryId = createAction(SET_SUGGESTION_CATEGORY_ID);
const donateToysCheckbox = createAction(DONATE_TOYS_CHECKBOX);
const setRecentOrderId = createAction(SET_RECENT_ORDER_ID);

const getFCConfigReq = createAction(GET_FC_CONFIG_REQ);
const getFCConfigSuccess = createAction(GET_FC_CONFIG_SUCCESS);
const getFCConfigFailed = createAction(GET_FC_CONFIG_FAILED);

export const actions = {
    storeChangeRequest,
    setStoreInfoRequest,
    getStoreInfoRequest,
    saveLoginUser,
    updateCustomerType,
    saveGuestUser,
    getGuestCartRequest,
    saveQuoteIdRequest,

    requestSuccess,
    requestFailed,
    setDefaultCountryPop,

    getReservationConfigRequest,
    getReservationConfigRequestSuccess,
    getReservationConfigRequestFailed,

    addUpdateReservationRequest,
    addUpdateReservationRequestSuccess,
    addUpdateReservationRequestFailed,

    removeReservationRequest,
    removeReservationRequestSuccess,
    removeReservationRequestFailed,

    extendReservationTimeRequest,
    extendReservationTimeRequestSuccess,
    extendReservationTimeRequestFailed,

    saveReservationStartTime,

    setSuggestionCategoryId,
    donateToysCheckbox,
    getFCConfigReq,
    getFCConfigSuccess,
    getFCConfigFailed,

    disableLoaders,
    setRecentOrderId,
};

export const types = {
    SET_STORE_INFO_REQUEST,
    GET_STORE_INFO_REQUEST,
    STORE_CHANGE_REQUEST,
    SAVE_LOGIN_USER,
    UPDATE_CUSTOMER_TYPE,
    SAVE_GUEST_USER,
    GET_GUEST_CART_REQUEST,
    SAVE_QUOTE_ID_REQUEST,

    REQUEST_SUCCESS,
    REQUEST_FAILED,
    SET_DEFAULT_COUNTRY_POP,

    GET_RESERVATION_CONFIG_REQUEST,
    GET_RESERVATION_CONFIG_REQUEST_SUCCESS,
    GET_RESERVATION_CONFIG_REQUEST_FAILED,

    ADD_UPDATE_RESERVATION_REQUEST,
    ADD_UPDATE_RESERVATION_REQUEST_SUCCESS,
    ADD_UPDATE_RESERVATION_REQUEST_FAILED,

    REMOVE_RESERVATION_REQUEST,
    REMOVE_RESERVATION_REQUEST_SUCCESS,
    REMOVE_RESERVATION_REQUEST_FAILED,

    EXTEND_RESERVATION_TIME_REQUEST,
    EXTEND_RESERVATION_TIME_REQUEST_SUCCESS,
    EXTEND_RESERVATION_TIME_REQUEST_FAILED,

    SAVE_RESERVATION_START_TIME_REQUEST,

    SET_SUGGESTION_CATEGORY_ID,
    DONATE_TOYS_CHECKBOX,
    GET_FC_CONFIG_REQ,
    GET_FC_CONFIG_SUCCESS,
    GET_FC_CONFIG_FAILED,
    DISABLE_LOADER,
    SET_RECENT_ORDER_ID,
};
