import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionsHandler = {
    [types.SET_STORE_INFO_REQUEST]: (state, { payload }) => ({
        ...state,
        language: (payload && payload.language) || 'ar',
        currentStore: (payload && payload.currentStore) || 3,
        country: (payload && payload.country) || 'UAE',
        region: (payload && payload.region) || 'uae-ar',
        defaultCountryUsed: false,
        store_locale: (payload && payload.store_locale) || 'uae-ar',
        loader: false,
    }),
    [types.SAVE_LOGIN_USER]: (state, { payload }) => ({
        ...state,
        loginUser: payload,
    }),
    [types.UPDATE_CUSTOMER_TYPE]: (state, { payload }) => ({
        ...state,
        loginUser: state.loginUser ? { ...state.loginUser, ...payload } : state.loginUser,
    }),
    [types.SAVE_QUOTE_ID_REQUEST]: (state, { payload }) => ({
        ...state,
        quote_id_created_time:
            payload === null
                ? null
                : state.quote_id !== payload
                ? new Date()
                : state.quote_id_created_time === null
                ? new Date()
                : state.quote_id_created_time,
        quote_id: payload,
    }),
    [types.SAVE_GUEST_USER]: (state, { payload }) => ({
        ...state,
        guestUser: payload,
    }),
    [types.GET_STORE_INFO_REQUEST]: state => ({
        ...state,
        loader: true,
    }),
    [types.STORE_CHANGE_REQUEST]: state => ({
        ...state,
        loader: true,
    }),
    [types.REQUEST_FAILED]: state => ({
        ...state,
        loader: false,
    }),
    [types.REQUEST_SUCCESS]: state => ({
        ...state,
        loader: false,
    }),

    [types.SET_DEFAULT_COUNTRY_POP]: (state, { payload }) => ({
        ...state,
        defaultCountryUsed: payload,
    }),

    [types.SAVE_RESERVATION_START_TIME_REQUEST]: state => ({
        ...state,
        reservationStartTime: null,
    }),

    [types.GET_RESERVATION_CONFIG_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        reservationConfig: payload,
    }),
    [types.GET_RESERVATION_CONFIG_REQUEST_FAILED]: state => ({
        ...state,
        reservationConfig: {},
    }),

    [types.ADD_UPDATE_RESERVATION_REQUEST]: state => ({
        ...state,
        reservationLoader: true,
    }),
    [types.ADD_UPDATE_RESERVATION_REQUEST_SUCCESS]: state => ({
        ...state,
        reservationLoader: false,
        reservationStartTime: new Date(),
    }),
    [types.ADD_UPDATE_RESERVATION_REQUEST_FAILED]: state => ({
        ...state,
        reservationLoader: false,
        reservationStartTime: null,
    }),

    [types.REMOVE_RESERVATION_REQUEST_SUCCESS]: state => ({
        ...state,
        reservationStartTime: null,
    }),
    [types.REMOVE_RESERVATION_REQUEST_FAILED]: state => ({
        ...state,
        reservationStartTime: null,
    }),

    [types.EXTEND_RESERVATION_TIME_REQUEST]: state => ({
        ...state,
        extendReservationLoader: true,
    }),
    [types.EXTEND_RESERVATION_TIME_REQUEST_SUCCESS]: state => ({
        ...state,
        reservationStartTime: new Date(),
        extendReservationLoader: false,
    }),
    [types.EXTEND_RESERVATION_TIME_REQUEST_FAILED]: state => ({
        ...state,
        reservationStartTime: null,
        extendReservationLoader: false,
    }),

    [types.SET_SUGGESTION_CATEGORY_ID]: (state, { payload }) => ({
        ...state,
        category_id: payload || '',
        recentCategories: recentCategoriesHelper(state.recentCategories, payload),
    }),
    [types.DONATE_TOYS_CHECKBOX]: (state, { payload }) => ({
        ...state,
        donate_toys: payload,
    }),
    [types.GET_FC_CONFIG_REQ]: state => ({
        ...state,
    }),
    [types.GET_FC_CONFIG_SUCCESS]: (state, { payload }) => ({
        ...state,
        fc_status: payload,
    }),
    [types.GET_FC_CONFIG_FAILED]: state => ({
        ...state,
        fc_status: false,
    }),
    [types.DISABLE_LOADER]: state => ({
        ...state,
        loader: false,
        reservationLoader: false,
        extendReservationLoader: false,
    }),
    [types.SET_RECENT_ORDER_ID]: (state, { payload }) => ({
        ...state,
        recentOrderId: payload,
    }),
};

const recentCategoriesHelper = (categories, newCategory) => {
    if (!categories || !Array.isArray(categories)) return [];
    if (!newCategory) return categories;
    if (categories.includes(newCategory)) return categories;
    if (categories.length < 8) {
        categories.push(newCategory);
    } else {
        categories.shift();
        categories.push(newCategory);
    }
    return categories;
};

export default handleActions(actionsHandler, {
    language: 'en',
    currentStore: 4,
    defaultCountryUsed: true,
    country: 'UAE',
    region: 'uae-en',
    store_locale: 'uae-en',
    loginUser: null,
    guestUser: null,
    loader: false,
    category_id: '',
    recentCategories: [],
    reservationConfig: {},
    reservationStartTime: null,
    reservationLoader: false,
    extendReservationLoader: false,
    quote_id_created_time: null,
    donate_toys: false,
    fc_status: false,
    recentOrderId: null,
});
