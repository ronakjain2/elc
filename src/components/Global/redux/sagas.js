import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import { actions as globalActions } from 'components/Global/redux/actions';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import { call, delay, put, select, takeLatest } from 'redux-saga/effects';
import api from '../api';
import { getStoreName, whereWeRedirectCheckoutCartOrNot, whereWeRedirectContinueShoppingHomeOrNot } from '../utils';
import { actions, types } from './actions';

const auth = state => state && state.auth;

const getStoreInfoReq = function* getStoreInfoReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { storeData, storeInfo, storeChnageObj, countryChange } = payload;
        const { data } = yield call(api.getStoreInfo, storeData);
        if (data && data.status) {
            yield put(actions.setStoreInfoRequest(storeInfo));
            //On Country change call reservation
            let reservationStartTime = authData && authData.reservationStartTime;
            if (reservationStartTime && countryChange) {
                yield put(
                    actions.removeReservationRequest({
                        store_id: storeInfo.currentStore,
                        quote_id: authData.quote_id,
                        reservation_type: 'checkout',
                    }),
                );
            }
            //end

            yield put(
                actions.storeChangeRequest({
                    data: storeChnageObj,
                    newLocale: storeInfo && storeInfo.store_locale,
                }),
            );
        }
    } catch (err) {
        yield put(actions.requestFailed());
    }
};

const getFCConfigReq = function* getFCConfigReq() {
    try {
        const authData = yield select(auth);
        const store_id = authData.currentStore;
        const customer_id = (authData && authData.loginUser && authData.loginUser.customer_id) || '';
        const { data } = yield call(api.getFCConfig, { store_id, customer_id });
        if (data && data.status) {
            yield put(actions.getFCConfigSuccess(Boolean(data.fc_status)));
            if (data.customer_type) {
                yield put(
                    actions.updateCustomerType({
                        customer_type: data && data.customer_type,
                        fc_status_message: data && data.fc_status_message,
                    }),
                );
            }
        } else {
            yield put(actions.getFCConfigFailed());
        }
    } catch (err) {
        yield put(actions.getFCConfigFailed());
    }
};

const storeChangeReq = function* storeChangeReq({ payload }) {
    try {
        const { data } = payload;
        if (data.quote_id) {
            const res = yield call(api.storeChange, data);
            if (res.data && res.data.status) {
                // yield put(actions.requestSuccess());
                yield delay(200);
                window.location.reload();
            } else {
                toast.error((res.data && res.data.message) || 'Something went to wrong, Please try after sometime');
                let err = {};
                throw err;
            }
        } else {
            yield delay(200);
            window.location.reload();
        }
    } catch (err) {
        yield delay(200);
        window.location.reload();
    }
};

const getGuestCartReq = function* getGuestCartReq({ payload }) {
    try {
        const { storeId } = payload;
        const storeName = getStoreName(storeId);
        const { data } = yield call(api.getGuestCart, storeName);
        const guestUser = {
            guestId: data,
        };
        yield put(actions.saveGuestUser(guestUser));
    } catch (err) {
        yield put(actions.requestFailed());
    }
};

const getReservationConfig = function* getReservationConfig() {
    try {
        const { data } = yield call(api.getReservationConfig);
        if (data && data.status) {
            yield put(actions.getReservationConfigRequestSuccess(data.res_config_data));
        } else {
            yield put(actions.getReservationConfigRequestFailed());
        }
    } catch (err) {
        yield put(actions.getReservationConfigRequestFailed());
    }
};

const addUpdateReservationReq = function* addUpdateReservationReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.addAndupdateReservation, payload);
        if (data && data.status) {
            if (data.customer_type) {
                yield put(
                    globalActions.updateCustomerType({
                        customer_type: data && data.customer_type,
                        fc_status_message: data && data.fc_status_message,
                    }),
                );
            }
            if (data.showMessage) {
                toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
            }
            yield put(actions.addUpdateReservationRequestSuccess());
            if (authData.loginUser) {
                yield put(push(`/${authData && authData.store_locale}/delivery-details`));
            } else {
                yield put(push(`/${authData && authData.store_locale}/checkout-login`));
            }
        } else {
            if (data.customer_type) {
                yield put(
                    globalActions.updateCustomerType({
                        customer_type: data && data.customer_type,
                        fc_status_message: data && data.fc_status_message,
                    }),
                );
            }
            if (data.showMessage) {
                toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
            }
            yield put(actions.addUpdateReservationRequestFailed());
            if (payload.from_cart) {
                yield put(
                    basketActions.getCartItemRequest({
                        quote_id: payload.quote_id,
                        store_id: payload.store_id,
                        isBasket: true,
                    }),
                );
            } else {
                yield put(push(`/${authData && authData.store_locale}/cart`));
            }
        }
    } catch (err) {
        const authData = yield select(auth);
        yield put(actions.addUpdateReservationRequestFailed());
        if (payload.from_cart) {
            yield put(
                basketActions.getCartItemRequest({
                    quote_id: payload.quote_id,
                    store_id: payload.store_id,
                    isBasket: true,
                }),
            );
        } else {
            yield put(push(`/${authData && authData.store_locale}/cart`));
        }
    }
};

const removeReservationReq = function* removeReservationReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.removeReservation, payload);
        if (data && data.status) {
            if (whereWeRedirectContinueShoppingHomeOrNot()) {
                yield put(push(`/`));
            }
            yield put(actions.removeReservationRequestSuccess());
        } else {
            yield put(actions.removeReservationRequestFailed());
            yield put(push(`/${authData && authData.store_locale}/cart`));
        }
    } catch (err) {
        const authData = yield select(auth);
        yield put(actions.removeReservationRequestFailed());
        yield put(push(`/${authData && authData.store_locale}/cart`));
    }
};

const extendReservationTimeReq = function* extendReservationTimeReq({ payload }) {
    try {
        const authData = yield select(auth);
        const { data } = yield call(api.extendReservationTime, payload);
        if (data && data.status) {
            yield put(actions.extendReservationTimeRequestSuccess());
            if (whereWeRedirectCheckoutCartOrNot()) {
                yield put(push(`/${authData && authData.store_locale}/cart`));
            }
        } else {
            yield put(actions.extendReservationTimeRequestFailed());
            yield put(push(`/${authData && authData.store_locale}/cart`));
        }
    } catch (err) {
        const authData = yield select(auth);
        yield put(actions.extendReservationTimeRequestFailed());
        yield put(push(`/${authData && authData.store_locale}/cart`));
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_STORE_INFO_REQUEST, getStoreInfoReq);
    yield takeLatest(types.STORE_CHANGE_REQUEST, storeChangeReq);
    yield takeLatest(types.GET_GUEST_CART_REQUEST, getGuestCartReq);

    yield takeLatest(types.GET_RESERVATION_CONFIG_REQUEST, getReservationConfig);
    yield takeLatest(types.ADD_UPDATE_RESERVATION_REQUEST, addUpdateReservationReq);
    yield takeLatest(types.REMOVE_RESERVATION_REQUEST, removeReservationReq);
    yield takeLatest(types.EXTEND_RESERVATION_TIME_REQUEST, extendReservationTimeReq);
    yield takeLatest(types.GET_FC_CONFIG_REQ, getFCConfigReq);
}
