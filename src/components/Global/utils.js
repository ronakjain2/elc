export const getStoreName = storeId => {
    switch (storeId) {
        case 1:
            return 'saudi_ar';

        case 2:
            return 'saudi_en';

        case 3:
            return 'uae_ar';

        case 4:
            return 'uae_en';

        default:
            return 'uae_ar';
    }
};

export const whereWeRedirectContinueShoppingHomeOrNot = () => {
    const location = window && window.location;
    const pathArray = (location && location.pathname && location.pathname.split('/')) || [];
    const lastPathName = pathArray[pathArray.length - 1];
    if (
        lastPathName === 'checkout-login' ||
        lastPathName === 'delivery-details' ||
        lastPathName === 'checkout-payment' ||
        lastPathName === 'payment-processing' ||
        lastPathName === 'order-summary'
    ) {
        return true;
    }
    return false;
};

export const whereWeRedirectCheckoutCartOrNot = () => {
    const location = window && window.location;
    const pathArray = (location && location.pathname && location.pathname.split('/')) || [];
    const lastPathName = pathArray[pathArray.length - 1];
    if (
        lastPathName === 'checkout-login' ||
        lastPathName === 'delivery-details' ||
        lastPathName === 'checkout-payment' ||
        lastPathName === 'payment-processing' ||
        lastPathName === 'order-summary'
    ) {
        return false;
    }
    return true;
};
