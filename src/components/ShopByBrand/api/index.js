import api from 'apiServices';

const fetchBrandData = (storeId) => api.get(`/index.php/rest/V1/app/brandfilter/?storeid=${storeId}`);

export default {
    fetchBrandData,
};
