import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid } from '@material-ui/core';
import './styles.css';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { Link, withRouter } from 'react-router-dom';
import { actions as shopByBrandActions } from './redux/actions';
import Spinner from 'commonComponet/Spinner';
function ShopByBrand({ history }) {
    const dispatch = useDispatch();
    const state = useSelector(state => state);
    let store_id = state && state.auth && state.auth.currentStore;
    let loader = state && state.shopbybrandreducer && state.shopbybrandreducer.loader;
    useEffect(() => {
        try {
            dispatch(
                shopByBrandActions.getAvailableBrands({
                    storeId: store_id,
                }),
            );
        } catch (err) {}
    }, [dispatch, store_id]);

    const jumpOnSameAlphaCharacter = index => {
        let element = document.getElementById('idbrand.' + index);
        element.scrollIntoView({ block: 'start', behavior: 'smooth' });
    };
    const _getListOfBrand = info => {
        let input =
            info &&
            info.map((item, index) => (
                <Grid>
                    <ul style={{ textAlign: 'start', letterSpacing: '1px' }} id="lindex">
                        <li style={{ cursor: 'pointer' }}>
                            <Link
                                to={`/${state.auth.store_locale}/products/brand/${item.attribute_key}`}
                                className="Link-Menu"
                            >
                                {item.brandname}
                            </Link>
                        </li>
                    </ul>
                </Grid>
            ));
        return input;
    };

    return (
        <>
            {loader ? (
                <Spinner />
            ) : (
                <Grid container justify="center">
                    <Grid xs={12} md={4} justify="center">
                        <Grid container justify="center" style={{ marginTop: '5%', marginBottom: '2%' }}>
                            <Typography className="header-browser-all-brands">
                                <FormattedMessage id="BrowseAllBrand.Text" defaultMessage="Browse All Brands" />
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container justify="center" style={{ width: '100%' }} xs={10} md={10}>
                        <Grid className="brands-alpha">
                            <ul className=" list-break-alpha brands-alpha-ul">
                                {state &&
                                    state.shopbybrandreducer &&
                                    state.shopbybrandreducer.brandData &&
                                    Object.values(state.shopbybrandreducer.brandData).map((item, index) => (
                                        <li
                                            style={{ cursor: 'pointer' }}
                                            key={index}
                                            onClick={() => jumpOnSameAlphaCharacter(index)}
                                            className="list-alpha"
                                        >
                                            {item.key}
                                        </li>
                                    ))}
                            </ul>{' '}
                        </Grid>
                    </Grid>
                    <Grid xs={10} md={10} justify="center">
                        <Grid className="pad_inherit">
                            <Grid className="brands-alpha-show-ul">
                                {state &&
                                    state.shopbybrandreducer &&
                                    state.shopbybrandreducer.brandData &&
                                    Object.values(state.shopbybrandreducer.brandData).map((item, index) => (
                                        <>
                                            <Grid style={{ textAlign: 'start' }} className="list-alpha-show-2">
                                                <h3
                                                    id={`idbrand.${index}`}
                                                    className="pad-r"
                                                    style={{
                                                        fontWeight: 800,
                                                        display: 'inline-block',
                                                        cursor: 'pointer',
                                                    }}
                                                >
                                                    {item.key}
                                                </h3>
                                                <br />
                                            </Grid>

                                            <Grid className="list-break"> {_getListOfBrand(item.data)}</Grid>
                                        </>
                                    ))}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            )}
        </>
    );
}
export default withRouter(ShopByBrand);
