import { createAction } from 'redux-actions';

const AVAILABLE_BRAND_SHOP_BY_BRAND = 'ELC@OROB/AVAILABLE_BRAND_SHOP_BY_BRAND';
const AVAILABLE_BRAND_SHOP_BY_BRAND_SUCCESS = 'ELC@OROB/AVAILABLE_BRAND_SHOP_BY_BRAND_SUCCESS';
const AVAILABLE_BRAND_SHOP_BY_BRAND_FAILED = 'ELC@OROB/AVAILABLE_BRAND_SHOP_BY_BRAND_FAILED';

const getAvailableBrands = createAction(AVAILABLE_BRAND_SHOP_BY_BRAND);
const getAvailableBrandsSuccess = createAction(AVAILABLE_BRAND_SHOP_BY_BRAND_SUCCESS);
const getAvailableBrandsFailed = createAction(AVAILABLE_BRAND_SHOP_BY_BRAND_FAILED);

export const types = {
    AVAILABLE_BRAND_SHOP_BY_BRAND,
    AVAILABLE_BRAND_SHOP_BY_BRAND_SUCCESS,
    AVAILABLE_BRAND_SHOP_BY_BRAND_FAILED,
};

export const actions = {
    getAvailableBrands,
    getAvailableBrandsSuccess,
    getAvailableBrandsFailed,
};
