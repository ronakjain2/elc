import api from '../api';
import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';

const getBrandInfo = function* getBrandInfo({ payload }) {
    try {
        const { storeId } = payload;

        const { data } = yield call(api.fetchBrandData, storeId);
        if (data) {
            let obj = [];
            Object.entries(data).map((item, index) => {
                let data = { attribute_key: item[0], brandname: item[1] };
                obj.push(data);
                return obj;
            });
            obj = obj.sort(function (a, b) {
                if (a.brandname[0] < b.brandname[0]) {
                    return -1;
                }
                if (a.brandname[0] > b.brandname[0]) {
                    return 1;
                }
                return 0;
            });

            let map = new Map();
            obj.forEach((item) => {
                let key = item.brandname[0];
                let value = [];

                obj.forEach((item) => {
                    if (item.brandname[0] === key) {
                        value.push(item);
                    }
                });
                map.set(key, value);
            });

            let sortedStores = [];
            map.forEach(function (value, key) {
                let object = { key: key, data: value };
                sortedStores.push(object);
            });
            yield put(actions.getAvailableBrandsSuccess(sortedStores));
        } else {
            yield put(actions.getAvailableBrandsFailed());
        }
    } catch (err) {
        yield put(actions.getAvailableBrandsFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.AVAILABLE_BRAND_SHOP_BY_BRAND, getBrandInfo);
}
