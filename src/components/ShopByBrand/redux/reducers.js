import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.AVAILABLE_BRAND_SHOP_BY_BRAND]: (state) => ({
        ...state,
        loader: true,
        error: false,
    }),
    [types.AVAILABLE_BRAND_SHOP_BY_BRAND_FAILED]: (state, { payload }) => ({
        ...state,
        loader: false,
        brandData: payload || {},
        error: true,
    }),
    [types.AVAILABLE_BRAND_SHOP_BY_BRAND_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        brandData: payload || {},
        error: false,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    brandData: {},
    error: false,
});
