import api from 'apiServices';

const getReturnPolicy = (storeId) =>
    api.get(`/index.php/rest/V1/cmsPageIdentifier/return-policy/storeId/${storeId}?storeId=${storeId}`);
const getDeliveryPolicy = (storeId) =>
    api.get(`/index.php/rest/V1/cmsPageIdentifier/delivery-policy/storeId/${storeId}?storeId=${storeId}`);
const getHelpAndFAQ = (storeId) =>
    api.get(`/index.php/rest/V1/cmsPageIdentifier/help-and-faqs/storeId/${storeId}?storeId=${storeId}`);
const getAboutUs = (storeId) =>
    api.get(`/index.php/rest/V1/cmsPageIdentifier/about-us/storeId/${storeId}?storeId=${storeId}`);
const getTermAndConditons = (storeId) =>
    api.get(`/index.php/rest/V1/cmsPageIdentifier/terms-and-condition/storeId/${storeId}?storeId=${storeId}`);
const getPrivacyPolicy = (storeId) =>
    api.get(`/index.php/rest/V1/cmsPageIdentifier/privacy-policy/storeId/${storeId}?storeId=${storeId}`);
const setContactUs = (payload) => api.post('/index.php/rest/V1/app/setContactUsData', payload);
const getContactUs = (storeId) => api.get(`/index.php/rest/V1/app/contactus?storeId=${storeId}`);

export default {
    getReturnPolicy,
    getDeliveryPolicy,
    getHelpAndFAQ,
    getAboutUs,
    getTermAndConditons,
    getPrivacyPolicy,
    setContactUs,
    getContactUs,
};
