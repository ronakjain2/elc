import { call, put, takeLatest } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import { actions, types } from './actions';
import api from '../api';

const getReturnPolicy = function* getReturnPolicy({ payload }) {
    try {
        const { storeId } = payload;
        const { data } = yield call(api.getReturnPolicy, storeId);
        yield put(actions.staticPageRequestSuccess({ returnPolicy: data }));
    } catch (err) {
        yield put(actions.staticPageRequestFailed());
    }
};

const getDeliveryPolicy = function* getDeliveryPolicy({ payload }) {
    try {
        const { storeId } = payload;
        const { data } = yield call(api.getDeliveryPolicy, storeId);
        yield put(actions.staticPageRequestSuccess({ deliveryPolicy: data }));
    } catch (err) {
        yield put(actions.staticPageRequestFailed());
    }
};

const getHelpAndFAQRequest = function* getHelpAndFAQRequest({ payload }) {
    try {
        const { storeId } = payload;
        const { data } = yield call(api.getHelpAndFAQ, storeId);
        yield put(actions.staticPageRequestSuccess({ helpAndFaq: data }));
    } catch (err) {
        yield put(actions.staticPageRequestFailed());
    }
};

const getAboutUs = function* getAboutUs({ payload }) {
    try {
        const { storeId } = payload;
        const { data } = yield call(api.getAboutUs, storeId);
        yield put(actions.staticPageRequestSuccess({ aboutUs: data }));
    } catch (err) {
        yield put(actions.staticPageRequestFailed());
    }
};

const getTermAndConditionReq = function* getTermAndConditionReq({ payload }) {
    try {
        const { storeId } = payload;
        const { data } = yield call(api.getTermAndConditons, storeId);
        yield put(actions.staticPageRequestSuccess({ termAndCondition: data }));
    } catch (err) {
        yield put(actions.staticPageRequestFailed());
    }
};

const getPrivacyPolicyReq = function* getPrivacyPolicyReq({ payload }) {
    try {
        const { storeId } = payload;
        const { data } = yield call(api.getPrivacyPolicy, storeId);
        yield put(actions.staticPageRequestSuccess({ privacyPolicy: data }));
    } catch (err) {
        yield put(actions.staticPageRequestFailed());
    }
};

const setContactUsReq = function* setContactUsReq({ payload }) {
    try {
        const { data } = yield call(api.setContactUs, payload);
        if (data && data.status) {
            toast.success(data.message);
        } else {
            toast.error(data.message || 'something went to wrong');
        }
        yield put(actions.staticPageRequestSuccess());
    } catch (err) {
        toast.error('something went to wrong');
        yield put(actions.staticPageRequestFailed());
    }
};

const getContactUsReq = function* getContactUsReq({ payload }) {
    try {
        const { storeId } = payload;
        const { data } = yield call(api.getContactUs, storeId);
        yield put(actions.staticPageRequestSuccess({ contactUs: data }));
    } catch (err) {
        yield put(actions.staticPageRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_RETURN_POLICY_REQUEST, getReturnPolicy);
    yield takeLatest(types.GET_DELIVERY_POLICY_REQUEST, getDeliveryPolicy);
    yield takeLatest(types.GET_HELP_AND_FAQ_REQUEST, getHelpAndFAQRequest);
    yield takeLatest(types.GET_ABOUT_US_REQUEST, getAboutUs);
    yield takeLatest(types.GET_TERMS_AND_CONDITIONS_REQUEST, getTermAndConditionReq);
    yield takeLatest(types.GET_PRIVACY_POLICY_REQUEST, getPrivacyPolicyReq);
    yield takeLatest(types.SET_CONTACT_US_REQUEST, setContactUsReq);
    yield takeLatest(types.GET_CONTACT_US_REQUEST, getContactUsReq);
}
