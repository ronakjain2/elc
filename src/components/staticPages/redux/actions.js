import { createAction } from 'redux-actions';

// actions types

const GET_RETURN_POLICY_REQUEST = 'ELC@OROB/GET_RETURN_POLICY_REQUEST';
const GET_DELIVERY_POLICY_REQUEST = 'ELC@OROB/GET_DELIVERY_POLICY_REQUEST';
const GET_HELP_AND_FAQ_REQUEST = 'ELC@OROB/GET_HELP_AND_FAQ_REQUEST';
const GET_ABOUT_US_REQUEST = 'ELC@OROB/GET_ABOUT_US_REQUEST';
const GET_TERMS_AND_CONDITIONS_REQUEST = 'ELC@OROB/GET_TERMS_AND_CONDITIONS_REQUEST';
const GET_PRIVACY_POLICY_REQUEST = 'ELC@OROB/GET_PRIVACY_POLICY_REQUEST';
const SET_CONTACT_US_REQUEST = 'ELC@OROB/SET_CONTACT_US_REQUEST';
const GET_CONTACT_US_REQUEST = 'ELC@OROB/GET_CONTACT_US_REQUEST';

const STATIC_PAGES_REQUEST_SUCCESS = 'ELC@OROB/STATIC_PAGES_SUCCESS';
const STATIC_PAGES_REQUEST_FAILED = 'ELC@OROB/STATIC_PAGES_REQUEST_FAILED';

// action methods
const getReturnPolicyRequest = createAction(GET_RETURN_POLICY_REQUEST);
const getDeliveryPolicyRequest = createAction(GET_DELIVERY_POLICY_REQUEST);
const getHelpAndFaqRequest = createAction(GET_HELP_AND_FAQ_REQUEST);
const getAboutUsRequest = createAction(GET_ABOUT_US_REQUEST);
const getTermAndConditions = createAction(GET_TERMS_AND_CONDITIONS_REQUEST);
const getPrivacyPolicyRequest = createAction(GET_PRIVACY_POLICY_REQUEST);
const setContactUsRequest = createAction(SET_CONTACT_US_REQUEST);
const getContactUsRequest = createAction(GET_CONTACT_US_REQUEST);

const staticPageRequestSuccess = createAction(STATIC_PAGES_REQUEST_SUCCESS);
const staticPageRequestFailed = createAction(STATIC_PAGES_REQUEST_FAILED);

// actions
export const actions = {
    getReturnPolicyRequest,
    getDeliveryPolicyRequest,
    getHelpAndFaqRequest,
    getAboutUsRequest,
    getTermAndConditions,
    getPrivacyPolicyRequest,
    setContactUsRequest,
    getContactUsRequest,

    staticPageRequestSuccess,
    staticPageRequestFailed,
};

// types
export const types = {
    GET_RETURN_POLICY_REQUEST,
    GET_DELIVERY_POLICY_REQUEST,
    GET_HELP_AND_FAQ_REQUEST,
    GET_ABOUT_US_REQUEST,
    GET_TERMS_AND_CONDITIONS_REQUEST,
    GET_PRIVACY_POLICY_REQUEST,
    SET_CONTACT_US_REQUEST,
    GET_CONTACT_US_REQUEST,

    STATIC_PAGES_REQUEST_SUCCESS,
    STATIC_PAGES_REQUEST_FAILED,
};
