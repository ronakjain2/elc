import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_RETURN_POLICY_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_DELIVERY_POLICY_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_HELP_AND_FAQ_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_ABOUT_US_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_TERMS_AND_CONDITIONS_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_PRIVACY_POLICY_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.SET_CONTACT_US_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),

    [types.STATIC_PAGES_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        returnPolicy: payload && payload.returnPolicy,
        deliveryPolicy: payload && payload.deliveryPolicy,
        helpAndFaq: payload && payload.helpAndFaq,
        aboutUs: payload && payload.aboutUs,
        termAndCondition: payload && payload.termAndCondition,
        privacyPolicy: payload && payload.privacyPolicy,
        contactUs: payload && payload.contactUs,
    }),
    [types.STATIC_PAGES_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
        returnPolicy: null,
        deliveryPolicy: null,
        helpAndFaq: null,
        aboutUs: null,
        termAndCondition: null,
        privacyPolicy: null,
        contactUs: null,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    returnPolicy: null,
    deliveryPolicy: null,
    helpAndFaq: null,
    aboutUs: null,
    termAndCondition: null,
    privacyPolicy: null,
    contactUs: null,
});
