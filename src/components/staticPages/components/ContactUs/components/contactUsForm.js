import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormattedMessage } from 'react-intl';
import { isEmail, scrollToElement } from 'components/SignIn_SignUp/utils';
import PhoneNumber from 'commonComponet/PhoneNumber';
import { actions as staticPagesActions } from 'components/staticPages/redux/actions';
import { useDispatch, useSelector } from 'react-redux';

export default function ContactUsForm() {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    const [values, setValues] = useState({
        name: '',
        email: '',
        phoneNumber: '',
        countryCode: '',
        purpose: 'customer_services',
        comment: '',
    });
    const [isValidate, setIsValidate] = useState(false);
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...values,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setValues(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onChangeHandler = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value });
    };

    const onSubmit = () => {
        setIsValidate(true);
        if (
            values.name === '' ||
            !isEmail(values.email) ||
            values.phoneNumber === '' ||
            !isCorrectPhone ||
            values.countryCode === ''
        ) {
            scrollToElement('.input-error');
            return;
        }
        setIsValidate(false);
        dispatch(
            staticPagesActions.setContactUsRequest({
                carrier_code: values.countryCode,
                comment: values.comment,
                email: values.email,
                name: values.name,
                phoneNumber: values.phoneNumber,
                purpose: values.purpose,
                storeId: state.auth.currentStore,
            }),
        );
    };

    return (
        <Grid container justify="center">
            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="ContactUs.YourName" defaultMessage="Your Name*" />
                </Typography>
                <TextField
                    variant="outlined"
                    type="text"
                    className="inputBox"
                    value={values.name}
                    onChange={onChangeHandler('name')}
                    fullWidth
                    error={isValidate && values.name === ''}
                />

                {isValidate && values.name === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.FirstName.Error" defaultMessage="Please Enter first name" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.EmailAddress" defaultMessage="Email Address*" />
                </Typography>
                <TextField
                    variant="outlined"
                    type="text"
                    className="inputBox"
                    value={values.email}
                    onChange={onChangeHandler('email')}
                    fullWidth
                    error={isValidate && !isEmail(values.email)}
                />

                {isValidate && !isEmail(values.email) && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.Email.Error" defaultMessage="Please Enter Valid Email Address" />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="SignInSignUp.ContactNumber" defaultMessage="Contact Number*" />
                </Typography>

                <PhoneNumber parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent} />

                {isValidate && values.phoneNumber === '' && (
                    <FormHelperText className="input-error">
                        <FormattedMessage id="Common.PhoneNumber.Empty" defaultMessage="Please enter contact number" />
                    </FormHelperText>
                )}
                {isValidate && values.phoneNumber !== '' && !isCorrectPhone && (
                    <FormHelperText className="input-error">
                        <FormattedMessage
                            id="Common.PhoneNumber.InValid"
                            defaultMessage="Please enter contact number"
                        />
                    </FormHelperText>
                )}
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="ContactUs.Purpose" defaultMessage="Purpose" />
                </Typography>
                <TextField
                    variant="outlined"
                    select
                    className="inputBox"
                    value={values.purpose}
                    onChange={onChangeHandler('purpose')}
                    fullWidth
                >
                    <MenuItem value="customer_services">
                        <FormattedMessage id="ContactUs.customerSerivces" defaultMessage="Customer Service" />
                    </MenuItem>
                    <MenuItem value="deliveries">
                        <FormattedMessage id="ContactUs.Deliveries" defaultMessage="Deliveries" />
                    </MenuItem>
                    <MenuItem value="genral_enq">
                        <FormattedMessage id="ContactUs.General" defaultMessage="General" />
                    </MenuItem>
                    <MenuItem value="order">
                        <FormattedMessage id="ContactUs.order" defaultMessage="Orders" />
                    </MenuItem>
                    <MenuItem value="payment">
                        <FormattedMessage id="ContactUs.payment" defaultMessage="Payment" />
                    </MenuItem>
                    <MenuItem value="returns">
                        <FormattedMessage id="ContactUs.returns" defaultMessage="Returns" />
                    </MenuItem>
                    <MenuItem value="stores">
                        <FormattedMessage id="ContactUs.stores" defaultMessage="Stores" />
                    </MenuItem>
                </TextField>
            </Grid>

            <Grid item xs={11} className="padding-t-input-box">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="ContactUs.YourComments" defaultMessage="Your Comments" />
                </Typography>
                <TextField
                    variant="outlined"
                    type="text"
                    className="inputBox"
                    value={values.comment}
                    onChange={onChangeHandler('comment')}
                    fullWidth
                    multiline
                    rows={4}
                />
            </Grid>

            {state && state.staticPage && !state.staticPage.loader && (
                <Grid item xs={11}>
                    <Button className="login-signup-login-button m-t-15" onClick={() => onSubmit()}>
                        <FormattedMessage id="Footer.Submit" defaultMessage="Submit" />
                    </Button>
                </Grid>
            )}

            {state && state.staticPage && state.staticPage.loader && (
                <Grid container justify="center">
                    <CircularProgress />
                </Grid>
            )}
        </Grid>
    );
}
