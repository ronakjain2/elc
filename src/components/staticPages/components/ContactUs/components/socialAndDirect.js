import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import '../styles.css';
import { useSelector } from 'react-redux';
import Call from '@material-ui/icons/Call';
import Mail from '@material-ui/icons/Mail';
import SocialSharing from 'commonComponet/SocialSharing';

export default function SocialAndDirect({ contactUs }) {
    const state = useSelector(state => state);

    let telPhone = '';
    if (state && state.auth && state.auth.country === 'KSA') {
        telPhone = contactUs && contactUs.contactnumber_ksa;
    } else {
        telPhone = contactUs && contactUs.contactnumber_uae;
    }

    return (
        <Grid container>
            <Grid item xs={12} className="socialAndDirect-container">
                <Typography className="socialAndDirect-title">
                    <FormattedMessage id="ContactUs.DirectContact" defaultMessage="Direct Contact" />
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography className="socialAndDirect-list">
                            <Call />
                            <a href={`tel:${telPhone}`}>{telPhone}</a>
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography className="social-timing">
                            <FormattedMessage
                                id="ContactUs.Timing"
                                defaultMessage="We are available for your support from 12:00 pm until 3:00 am."
                            />
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography className="socialAndDirect-list">
                            <Mail />
                            <a href="mailto:help@elctoys.com">help@elctoys.com</a>
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} className="socialAndDirect-container" style={{ marginTop: 10 }}>
                <Typography className="socialAndDirect-title">
                    <FormattedMessage id="ContactUs.SocialMedia" defaultMessage="Social Media" />
                </Typography>
            </Grid>
            <Grid item xs={12} style={{ marginTop: 20 }} className="socialAndDirect-list-sharging">
                <SocialSharing />
            </Grid>
        </Grid>
    );
}
