import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useSelector, useDispatch } from 'react-redux';
import { actions as staticPagesActions } from 'components/staticPages/redux/actions';
import '../../css/static.css';
import { FormattedMessage } from 'react-intl';
import SimpleBreadcrumbs from 'commonComponet/Breadcrumbs';
import ContactUsForm from './components/contactUsForm';
import SocialAndDirect from './components/socialAndDirect';

export default function ContactUs() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    useEffect(() => {
        try {
            dispatch(staticPagesActions.getContactUsRequest({ storeId: state.auth.currentStore }));
        } catch (err) {}
    }, [dispatch, state.auth.currentStore]);

    const contactUs = state && state.staticPage && state.staticPage.contactUs;
    const global = state && state.auth;

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: <FormattedMessage id="PageTitle.help-contact" defaultMessage="Contact Us" />,
        },
    ];

    return (
        <Grid container justify="center">
            <Grid item xs={11} md={10}>
                <Grid container style={{ marginTop: 10 }}>
                    {/** Breadcrumb */}
                    <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                    {/** End Breadcrumb */}
                    <Grid item xs={12}>
                        <Typography className="staticPage-title">
                            <span>{contactUs && contactUs.title}</span>
                        </Typography>
                    </Grid>
                    <Grid container justify="center">
                        <Grid item xs={11} md={10}>
                            <Grid container justify="space-between">
                                <Grid item xs={12} md={5}>
                                    <ContactUsForm />
                                </Grid>
                                <Grid item xs={12} md={5}>
                                    <SocialAndDirect contactUs={contactUs} />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
