import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useSelector, useDispatch } from 'react-redux';
import { actions as staticPagesActions } from 'components/staticPages/redux/actions';
import Spinner from 'commonComponet/Spinner';
import '../../css/static.css';
import { FormattedMessage } from 'react-intl';
import SimpleBreadcrumbs from 'commonComponet/Breadcrumbs';

export default function HelpAndFAQ() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    useEffect(() => {
        try {
            dispatch(staticPagesActions.getHelpAndFaqRequest({ storeId: state.auth.currentStore }));
        } catch (err) {}
    }, [dispatch, state.auth.currentStore]);

    const helpAndFaq = state && state.staticPage && state.staticPage.helpAndFaq;
    const global = state && state.auth;

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: helpAndFaq && helpAndFaq.title,
        },
    ];

    return (
        <Grid container justify="center">
            {state && state.staticPage && !state.staticPage.loader && (
                <Grid item xs={11} md={10}>
                    <Grid container style={{ marginTop: 10 }}>
                        {/** Breadcrumb */}
                        <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                        {/** End Breadcrumb */}
                        <Grid item xs={12}>
                            <Typography className="staticPage-title">
                                <span>{helpAndFaq && helpAndFaq.title}</span>
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <div
                                className="staticPagesText"
                                dangerouslySetInnerHTML={{ __html: helpAndFaq && helpAndFaq.content }}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            )}
            {state && state.staticPage && state.staticPage.loader && (
                <Grid container justify="center" alignItems="center">
                    <Spinner />
                </Grid>
            )}
        </Grid>
    );
}
