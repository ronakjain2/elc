import React, { lazy, Suspense, useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { actions as storeLocatorActions } from './redux/actions';
import { useSelector, useDispatch } from 'react-redux';
import Spinner from 'commonComponet/Spinner';
import './storeLocator.css';

const CountryAndCity = lazy(() => import('./Components/CountryAndCity'));
const StoreMap = lazy(() => import('./Components/Map'));
const CheckoutStoreLocatorList = lazy(() => import('./Components/StoreList'));

export default function StoreLocator() {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    const store_id = state && state.auth && state.auth.currentStore;
    const quote_id = state && state.auth && state.auth.quote_id;
    const country = state && state.auth && state.auth.country;
    const [countryName, setCountryName] = useState('');
    const [isValidate] = useState(false);
    const [city, setCity] = useState('');
    const [cityName, setCityName] = useState('');
    const [mapZoom, setMapZoom] = useState(3);
    const [latlang, setLatLong] = useState({
        lat: null,
        long: null,
    });
    const [selectedTab, setSelectedTab] = useState('nextDay');
    const language = state && state.auth && state.auth.language;
    const [selectedStorelocator, setSelectedStorelocator] = useState(null);

    const storeList = state && state.storeLocator && state.storeLocator.storeLocators;

    useEffect(() => {
        if (storeList) {
            if (storeList[0]) {
                setLatLong({
                    lat: storeList[0] && storeList[0].lattitude,
                    long: storeList[0] && storeList[0].longitude,
                });
                setMapZoom(8);
            }
        }
    }, [storeList]);

    //Selected store on pick and collect
    const onSelectedStoreLocator = (store) => {
        if (selectedStorelocator) {
            if (selectedStorelocator.id === store.id) {
                setSelectedStorelocator(null);
            } else {
                setSelectedStorelocator(store);
                onMarkerClick(store);
            }
        } else {
            setSelectedStorelocator(store);
            onMarkerClick(store);
        }
    };

    const onMarkerClick = (item) => {
        setLatLong({
            lat: item.lattitude,
            long: item.longitude,
        });
        setMapZoom(17);
        setSelectedStorelocator(item);
    };
    useEffect(() => {
        if (country === 'UAE') {
            setCountryName('UAE');
        } else {
            setCountryName('KSA');
        }
    }, [country]);

    // Get all store locator
    useEffect(() => {
        try {
            dispatch(
                storeLocatorActions.getStoreLocatorPageRequest({
                    country: country === 'UAE' ? 'AE' : 'SA',
                    city: city.name || '',
                    store_id,
                }),
            );
        } catch (err) {}
    }, [dispatch, city, store_id, country, quote_id]);

    //get country list
    useEffect(() => {
        dispatch(storeLocatorActions.getCountryListRequest());
    }, [dispatch]);

    return (
        <Suspense fallback={<Spinner />}>
            {/* {state && state.storeLocator && state.storeLocator.storeLocatorLoader && <Spinner /> } */}
            <Grid container justify="center" className="store-locator checkout-delivery storeLocator">
                <Grid item xs={11} md={8} className="checkout-details-card">
                    {/** Country and city */}
                    <CountryAndCity
                        setCountryName={setCountryName}
                        countryName={countryName}
                        city={city}
                        country={country}
                        setCity={setCity}
                        isValidate={isValidate}
                        cityName={cityName}
                        setCityName={setCityName}
                    />
                    {/** End country and city  */}
                </Grid>
                <Grid container justify="center">
                    <Grid item xs={11} md={8}>
                        <Grid container justify="center">
                            <Grid item xs={11} md={6} className="padding-l-r-1">
                                <StoreMap
                                    zoom={mapZoom}
                                    latlang={latlang}
                                    storeList={storeList}
                                    language={language}
                                    onMarkerClick={onMarkerClick}
                                />
                            </Grid>
                            <Grid item xs={11} md={6} className="store-list">
                                <CheckoutStoreLocatorList
                                    storeList={storeList}
                                    activeTab={selectedTab}
                                    setSelectedTab={setSelectedTab}
                                    selectedStorelocator={selectedStorelocator}
                                    onSelectedStoreLocator={onSelectedStoreLocator}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <div className="border-line" />
                </Grid>
            </Grid>
        </Suspense>
    );
}
