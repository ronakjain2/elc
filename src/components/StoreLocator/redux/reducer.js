import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_COUNTRY_LIST_REQUEST_FOR_STORELOCATOR]: (state) => ({
        ...state,
        countryListLoader: true,
    }),
    [types.GET_COUNTRY_LIST_REQUEST_FOR_STORELOCATOR_FAILED]: (state) => ({
        ...state,
        countryListLoader: false,
    }),
    [types.GET_COUNTRY_LIST_REQUEST_FOR_STORELOCATOR_SUCCESS]: (state, { payload }) => ({
        ...state,
        countryListLoader: false,
        countryList: payload,
    }),

    [types.GET_STORE_LOCATOR_PAGE_REQUEST]: (state) => ({
        ...state,
        storeLocatorLoader: true,
    }),
    [types.GET_STORE_LOCATOR_PAGE_REQUEST_FAILED]: (state) => ({
        ...state,
        storeLocatorLoader: false,
    }),
    [types.GET_STORE_LOCATOR_PAGE_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        storeLocatorLoader: false,
        storeLocators: payload || [],
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    countryListLoader: false,
    countryList: [],
    storeLocatorLoader: false,
    storeLocators: [],
});
