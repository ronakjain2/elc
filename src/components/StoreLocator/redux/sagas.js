import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../api';
import { toast } from 'react-toastify';

const getCountryListReq = function* getCountryListReq() {
    try {
        const { data } = yield call(api.getCountryList);
        if (data) {
            yield put(actions.getCountryListRequestSuccess(data || []));
        } else {
            yield put(actions.getCountryListRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getCountryListRequestFailed());
    }
};

const getStoreLocatorReq = function* getStoreLocatorReq({ payload }) {
    try {
        const { data } = yield call(api.getStoreList, payload);
        if (data && data.status) {
            yield put(actions.getStoreLocatorPageRequestSuccess(data.data));
        } else {
            toast.error(data.message);
            yield put(actions.getStoreLocatorPageRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getStoreLocatorPageRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_COUNTRY_LIST_REQUEST_FOR_STORELOCATOR, getCountryListReq);
    yield takeLatest(types.GET_STORE_LOCATOR_PAGE_REQUEST, getStoreLocatorReq);
}
