import api from 'apiServices';
const getCountryList = () => api.get(`/index.php/rest/V1/directory/countries`);
const getStoreList = (payload) =>
    api.get(
        `index.php/rest/V1/app/storelocator?country_id=${payload.country}&city=${payload.city}&store_id=${payload.store_id}`,
    );
export default {
    getCountryList,
    getStoreList,
};
