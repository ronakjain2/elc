import React from 'react';
import Grid from '@material-ui/core/Grid';
import StoreLocatorItem from './Component/listItem';

export default function CheckoutStoreLocatorList({
    storeList,
    activeTab,
    setSelectedTab,
    selectedStorelocator,
    onSelectedStoreLocator,
}) {
    return (
        <Grid container justify="center">
            <Grid item xs={12} md={11} className="pick-up-store-list-card">
                <Grid container>
                    <StoreLocatorItem
                        list={storeList}
                        selectedStorelocator={selectedStorelocator}
                        onSelectedStoreLocator={onSelectedStoreLocator}
                    />
                </Grid>
            </Grid>
        </Grid>
    );
}
