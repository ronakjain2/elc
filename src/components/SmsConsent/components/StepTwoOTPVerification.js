import Button from '@material-ui/core/Button';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import ButtonWithLoader from 'commonComponet/ButtonWithLoader';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../redux/actions';

const StepTwoOTPVerification = () => {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const store_id = state && state.auth && state.auth.currentStore;
    const smsConsent = state && state.smsConsent;
    const loader = smsConsent && smsConsent.loader;
    const contact_number = smsConsent && smsConsent.contact_number;
    const carrier_code = smsConsent && smsConsent.carrier_code;

    const [isValidate, setIsValidate] = useState(false);
    const [otp, setotp] = useState('');
    const [disableResendOtp, setDisableResendOtp] = useState(false);

    const onChangeHandler = value => {
        if (!value.match(/^[0-9]*$/)) {
            return;
        }
        setotp(value);
    };

    const onSubmit = e => {
        e && e.preventDefault();
        setIsValidate(true);
        if (!otp || otp.length < 4) {
            return;
        }
        setIsValidate(false);
        const payload = {
            carrier_code,
            contact_number,
            otp,
            store_id,
        };

        dispatch(actions.verifyOTPRequest(payload));
    };

    const onResendOTP = () => {
        setDisableResendOtp(true);
        const t = setTimeout(() => {
            setDisableResendOtp(false);
            clearTimeout(t);
        }, 60000);
        const payload = {
            carrier_code,
            contact_number,
            store_id,
            isResend: true,
        };
        dispatch(actions.sendOTPRequest(payload));
    };

    return (
        <>
            <Grid container justify="center" alignItems="center" className="step1-phoneinput">
                <Grid item xs={11} md={7} lg={5} className="card">
                    <form onSubmit={onSubmit}>
                        <Grid container justify="flex-start">
                            <Typography className="title">
                                <FormattedMessage
                                    id="smsconsent.PleaseEnterReceivedOTP"
                                    defaultMessage="Please enter received OTP"
                                />
                            </Typography>
                        </Grid>
                        <Grid container justify="center">
                            <Grid item xs={12} md={10} className="phone-input">
                                <Typography className="label text-align-start">
                                    <FormattedMessage id="smsconsent.OTP" defaultMessage="OTP" />
                                </Typography>

                                <TextField
                                    variant="outlined"
                                    value={otp}
                                    onChange={e => onChangeHandler(e.target.value)}
                                    fullWidth
                                    shrink
                                    autoFocus
                                    inputProps={{ maxLength: 4, className: 'text-center' }}
                                />
                                {isValidate && otp === '' && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage id="smsconsent.otp.Empty" defaultMessage="Please enter OTP" />
                                    </FormHelperText>
                                )}
                                {isValidate && otp !== '' && otp.length < 4 && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="smsconsent.otp.invalid"
                                            defaultMessage="Please enter valid otp"
                                        />
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={12} md={10} className="text-align-end mb-3 mt-1">
                                <Button
                                    className="resend-otp"
                                    disabled={disableResendOtp || loader}
                                    onClick={onResendOTP}
                                >
                                    <FormattedMessage id="smsconsent.otp.resend" defaultMessage="Resend OTP" />
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container justify="center">
                            <Grid item xs={12} md={10} className="flex justify-content-center">
                                <ButtonWithLoader
                                    onClick={onSubmit}
                                    type="submit"
                                    loader={loader}
                                >
                                    <FormattedMessage id="Checkout.Delivery.Continue" defaultMessage="Continue" />
                                </ButtonWithLoader>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        </>
    );
};

export default StepTwoOTPVerification;
