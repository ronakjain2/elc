import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import ButtonWithLoader from 'commonComponet/ButtonWithLoader';
import Img from 'commonComponet/Image';
import PhoneNumber from 'commonComponet/PhoneNumber';
import { isEmail, scrollToElement } from 'components/SignIn_SignUp/utils';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../redux/actions';

const StepThreeConsentForm = () => {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const store_id = state && state.auth && state.auth.currentStore;
    const smsConsent = state && state.smsConsent;
    const loader = smsConsent && smsConsent.loader;
    const checkbox_message = smsConsent && smsConsent.checkbox_message;
    const customer_details = smsConsent && smsConsent.customer_details;
    const consent_banner_desktop = smsConsent && smsConsent.consent_banner_desktop;
    const consent_banner_mobile = smsConsent && smsConsent.consent_banner_mobile;

    const [isValidate, setIsValidate] = useState(false);
    const [values, setValues] = useState({
        firstName: (customer_details && customer_details.firstname) || '',
        lastName: (customer_details && customer_details.lastname) || '',
        email: (customer_details && customer_details.email_id) || '',
        phoneNumber: smsConsent.contact_number,
        countryCode: smsConsent.carrier_code,
    });
    const [enableSmsConsent, setEnableSmsConsent] = useState(
        Boolean(customer_details && customer_details.is_conset_allowed === '1'),
    );
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);

    const parentPhoneNumberChangeEvent = phoneNo => {
        const Obj = {
            ...values,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setValues(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onChangeHandler = name => event => {
        if (name === 'firstName' || name === 'lastName') {
            const re = /^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g;
            if (event.target.value && !re.test(event.target.value)) {
                return '';
            }
        }
        const re_1 = /^(?!\s*$).+/g;
        if (event.target.value && !re_1.test(event.target.value)) {
            return '';
        }
        setValues({ ...values, [name]: event.target.value });
    };

    const checkForm = (values, status) => {
        if (
            !values.firstName ||
            values.firstName === '' ||
            !values.lastName ||
            values.lastName === '' ||
            !values.phoneNumber ||
            values.phoneNumber === '' ||
            !status ||
            values.countryCode === '' ||
            (values.email && !isEmail(values.email))
        ) {
            return true;
        }

        return false;
    };

    const onSubmit = e => {
        e && e.preventDefault();
        setIsValidate(true);
        if (checkForm(values, isCorrectPhone)) {
            scrollToElement('.input-error');
            return;
        }
        setIsValidate(false);
        const payload = {
            firstname: values.firstName,
            lastname: values.lastName,
            email_id: values.email,
            carrier_code: values.countryCode,
            contact_number: values.phoneNumber,
            is_conset_allowed: enableSmsConsent ? '1' : '0',
            store_id,
        };
        dispatch(actions.saveConsentRequest(payload));
    };

    return (
        <>
            <Grid container justify="center" alignItems="center" className="step3-consent-form">
                <Hidden only={['sm', 'xs', 'md']}>
                    <Img alt="Consent Form" className="background-images" src={consent_banner_desktop} />
                </Hidden>
                <Hidden only={['lg', 'xl']}>
                    <Img alt="Consent Form" className="background-images" src={consent_banner_mobile} />
                </Hidden>
                <Grid item xs={11} md={9} className="card">
                    <form onSubmit={onSubmit}>
                        <Grid container justify="center">
                            <Typography className="title">
                                <FormattedMessage
                                    id="smsconsent.PersonalDataConsentForm"
                                    defaultMessage="Personal Data Consent Form"
                                />
                            </Typography>
                        </Grid>
                        <Grid container spacing={2} className="my-4">
                            <Grid item xs={12} md={6}>
                                <Typography className="label text-align-start">
                                    <FormattedMessage id="SignInSignUp.FirstName" defaultMessage="First Name*" />
                                </Typography>
                                <TextField
                                    variant="outlined"
                                    value={values.firstName}
                                    onChange={onChangeHandler('firstName')}
                                    fullWidth
                                    autoFocus
                                    className="inputBox"
                                    error={isValidate && values.firstName === ''}
                                    inputProps={{ maxLength: 20 }}
                                />
                                {isValidate && values.firstName === '' && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.FirstName.Error"
                                            defaultMessage="Please enter first name"
                                        />
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Typography className="label text-align-start">
                                    <FormattedMessage id="SignInSignUp.LastName" defaultMessage="Last Name*" />
                                </Typography>
                                <TextField
                                    variant="outlined"
                                    value={values.lastName}
                                    onChange={onChangeHandler('lastName')}
                                    fullWidth
                                    className="inputBox"
                                    error={isValidate && values.lastName === ''}
                                    inputProps={{ maxLength: 19 }}
                                />
                                {isValidate && values.lastName === '' && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.LastName.Error"
                                            defaultMessage="Please enter last name"
                                        />
                                    </FormHelperText>
                                )}
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <Typography className="label text-align-start">
                                    <FormattedMessage
                                        id="SignInSignUp.ContactNumber"
                                        defaultMessage="Contact Number*"
                                    />
                                </Typography>
                                <PhoneNumber
                                    parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                                    carrier_code={values.countryCode}
                                    phoneNo={values.phoneNumber}
                                    // forInternational={true}
                                    disabled={true}
                                />
                                {isValidate && values.phoneNumber === '' && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.PhoneNumber.Empty"
                                            defaultMessage="Please enter contact number"
                                        />
                                    </FormHelperText>
                                )}
                                {isValidate && values.phoneNumber !== '' && !isCorrectPhone && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.PhoneNumber.InValid"
                                            defaultMessage="Please enter contact number"
                                        />
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Typography className="label text-align-start">
                                    <FormattedMessage id="MyAccount.Email" defaultMessage="Email" />
                                </Typography>
                                <TextField
                                    variant="outlined"
                                    value={values.email}
                                    onChange={onChangeHandler('email')}
                                    fullWidth
                                    className="inputBox"
                                    error={isValidate && values.email && !isEmail(values.email)}
                                />
                                {isValidate && values.email && !isEmail(values.email) && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.Email.Error"
                                            defaultMessage="Please Enter Valid Email Address"
                                        />
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Grid container justify="flex-start">
                                    <FormControl>
                                        <FormGroup>
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={enableSmsConsent}
                                                        onChange={e => setEnableSmsConsent(e.target.checked)}
                                                        name="gilad"
                                                    />
                                                }
                                                label={
                                                    <Grid container className="text-align-rtl">
                                                        {checkbox_message}
                                                    </Grid>
                                                }
                                            />
                                        </FormGroup>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container justify="center">
                            <Grid item xs={12} md={6} className="flex justify-content-center">
                                <ButtonWithLoader onClick={onSubmit} type="submit" loader={loader}>
                                    <FormattedMessage id="Footer.Submit" defaultMessage="Submit" />
                                </ButtonWithLoader>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        </>
    );
};

export default StepThreeConsentForm;
