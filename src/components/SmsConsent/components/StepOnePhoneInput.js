import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ButtonWithLoader from 'commonComponet/ButtonWithLoader';
import PhoneNumber from 'commonComponet/PhoneNumber';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../redux/actions';

const StepOnePhoneInput = () => {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const store_id = state && state.auth && state.auth.currentStore;
    const smsConsent = state && state.smsConsent;
    const loader = smsConsent && smsConsent.loader;

    const [values, setValues] = useState({
        phoneNumber: '',
        countryCode: '',
    });
    const [isCorrectPhone, setIsCorrectPhone] = useState(false);
    const [isValidate, setIsValidate] = useState(false);

    const parentPhoneNumberChangeEvent = phoneNo => {
        const Obj = {
            ...values,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setValues(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onSubmit = e => {
        e && e.preventDefault();
        setIsValidate(true);
        if (!isCorrectPhone) {
            return;
        }
        setIsValidate(false);
        const payload = {
            carrier_code: values.countryCode,
            contact_number: values.phoneNumber,
            store_id,
        };
        dispatch(actions.sendOTPRequest(payload));
    };

    return (
        <>
            <Grid container justify="center" alignItems="center" className="step1-phoneinput">
                <Grid item xs={11} md={7} lg={5} className="card">
                    <form onSubmit={onSubmit}>
                        <Grid container justify="flex-start">
                            <Typography className="title">
                                <FormattedMessage
                                    id="smsconsent.PleaseProvideUsYourContactNumber"
                                    defaultMessage="Please provide us your contact number"
                                />
                            </Typography>
                        </Grid>
                        <Grid container justify="center" className="mb-4">
                            <Grid item xs={12} md={10} className="phone-input">
                                <Typography className="label text-align-start">
                                    <FormattedMessage
                                        id="SignInSignUp.ContactNumber"
                                        defaultMessage="Contact Number*"
                                    />
                                </Typography>
                                <PhoneNumber
                                    parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                                    carrier_code={values.countryCode}
                                    phoneNo={values.phoneNumber}
                                    // forInternational={true}
                                />
                                {isValidate && values.phoneNumber === '' && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.PhoneNumber.Empty"
                                            defaultMessage="Please enter contact number"
                                        />
                                    </FormHelperText>
                                )}
                                {isValidate && values.phoneNumber !== '' && !isCorrectPhone && (
                                    <FormHelperText className="input-error">
                                        <FormattedMessage
                                            id="Common.PhoneNumber.InValid"
                                            defaultMessage="Please enter contact number"
                                        />
                                    </FormHelperText>
                                )}
                            </Grid>
                        </Grid>
                        <Grid container justify="center">
                            <Grid item xs={12} md={10} className="flex justify-content-center">
                                <ButtonWithLoader
                                    onClick={onSubmit}
                                    type="submit"
                                    loader={loader}
                                >
                                    <FormattedMessage id="Checkout.Delivery.Continue" defaultMessage="Continue" />
                                </ButtonWithLoader>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        </>
    );
};

export default StepOnePhoneInput;
