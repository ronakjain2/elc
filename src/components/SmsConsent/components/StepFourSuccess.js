import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Img from 'commonComponet/Image';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { actions } from '../redux/actions';

const StepFourSuccess = ({ history }) => {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    // const store_locale = state && state.auth && state.auth.store_locale;

    const smsConsent = state && state.smsConsent;

    const popup_message = smsConsent && smsConsent.popup_message;
    const consent_banner_desktop = smsConsent && smsConsent.consent_banner_desktop;
    const consent_banner_mobile = smsConsent && smsConsent.consent_banner_mobile;

    const onSubmit = () => {
        dispatch(actions.resetConsentReducer());
        // history.push(`/${store_locale}/`);
    };

    return (
        <>
            <Grid container justify="center" alignItems="center" className="step4-success">
                <Hidden only={['sm', 'xs', 'md']}>
                    <Img alt="Consent Form" className="background-images" src={consent_banner_desktop} />
                </Hidden>
                <Hidden only={['lg', 'xl']}>
                    <Img alt="Consent Form" className="background-images" src={consent_banner_mobile} />
                </Hidden>
                <Grid item className="card">
                    <Typography className="message">{popup_message}</Typography>
                    <Button className="ok-button" onClick={onSubmit}>
                        <FormattedMessage id="cart.Ok" defaultMessage="Ok" />
                    </Button>
                </Grid>
            </Grid>
        </>
    );
};

export default withRouter(StepFourSuccess);
