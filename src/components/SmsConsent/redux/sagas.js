import { toast } from 'react-toastify';
import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../api';
import { actions, types } from './actions';

// const auth = state => state && state.auth;

const getConsentImageReq = function* getConsentImageReq({ payload }) {
    try {
        const { data } = yield call(api.getConsentImage, payload);
        if (data && data.status) {
            yield put(actions.getConsentImageRequestSuccess(data.data));
        } else {
            yield put(actions.getConsentImageRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getConsentImageRequestFailed());
    }
};

const sendOtpReq = function* sendOtpReq({ payload }) {
    try {
        // const authData = yield select(auth);
        if (payload.isResend) {
            yield put(actions.sendOTPRequestFailed());
        }
        const { data } = yield call(api.sendOTP, payload);
        if (data && data.status) {
            yield put(actions.sendOTPRequestSuccess(payload));
            toast.success(data.message);
        } else {
            yield put(actions.sendOTPRequestFailed());
            toast.error(data.message);
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.sendOTPRequestFailed());
    }
};

const verifyOtpReq = function* verifyOtpReq({ payload }) {
    try {
        const { data } = yield call(api.verifyOTP, payload);
        if (data && data.status) {
            yield put(actions.verifyOTPRequestSuccess({ customer_details: data.customer_details }));
            toast.success(data.message);
        } else {
            yield put(actions.verifyOTPRequestFailed());
            toast.error(data.message);
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.sendOTPRequestFailed());
    }
};
const saveConsentFormReq = function* saveConsentFormReq({ payload }) {
    try {
        let { data } = yield call(api.saveConsentForm, payload);
        if (data && data.status) {
            yield put(actions.saveConsentRequestSuccess({ message: data.message }));
            // toast.success(data.message);
        } else {
            yield put(actions.saveConsentRequestFailed());
            toast.error(data.message);
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.saveConsentRequestFailed());
    }
};
export default function* sagas() {
    yield takeLatest(types.SEND_OTP_REQUEST, sendOtpReq);
    yield takeLatest(types.VERIFY_OTP_REQUEST, verifyOtpReq);
    yield takeLatest(types.GET_CONSENT_IMAGE_REQUEST, getConsentImageReq);
    yield takeLatest(types.SAVE_CONSENT_FORM_REQUEST, saveConsentFormReq);
}
