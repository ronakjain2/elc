import { createAction } from 'redux-actions';

const SEND_OTP_REQUEST = 'ELC/SEND_OTP_REQUEST';
const SEND_OTP_REQUEST_SUCCESS = 'ELC/SEND_OTP_REQUEST_SUCCESS';
const SEND_OTP_REQUEST_FAILED = 'ELC/SEND_OTP_REQUEST_FAILED';

const VERIFY_OTP_REQUEST = 'ELC/VERIFY_OTP_REQUEST';
const VERIFY_OTP_REQUEST_SUCCESS = 'ELC/VERIFY_OTP_REQUEST_SUCCESS';
const VERIFY_OTP_REQUEST_FAILED = 'ELC/VERIFY_OTP_REQUEST_FAILED';

const GET_CONSENT_IMAGE_REQUEST = 'ELC/GET_CONSENT_IMAGE_REQUEST';
const GET_CONSENT_IMAGE_REQUEST_SUCCESS = 'ELC/GET_CONSENT_IMAGE_REQUEST_SUCCESS';
const GET_CONSENT_IMAGE_REQUEST_FAILED = 'ELC/GET_CONSENT_IMAGE_REQUEST_FAILED';

const SAVE_CONSENT_FORM_REQUEST = 'ELC/SAVE_CONSENT_FORM_REQUEST';
const SAVE_CONSENT_FORM_REQUEST_SUCCESS = 'ELC/SAVE_CONSENT_FORM_REQUEST_SUCCESS';
const SAVE_CONSENT_FORM_REQUEST_FAILED = 'ELC/SAVE_CONSENT_FORM_REQUEST_FAILED';

const RESET_CONSENT_REDUCER = 'ELC/RESET_CONSENT_REDUCER';

const sendOTPRequest = createAction(SEND_OTP_REQUEST);
const sendOTPRequestSuccess = createAction(SEND_OTP_REQUEST_SUCCESS);
const sendOTPRequestFailed = createAction(SEND_OTP_REQUEST_FAILED);

const verifyOTPRequest = createAction(VERIFY_OTP_REQUEST);
const verifyOTPRequestSuccess = createAction(VERIFY_OTP_REQUEST_SUCCESS);
const verifyOTPRequestFailed = createAction(VERIFY_OTP_REQUEST_FAILED);

const getConsentImageRequest = createAction(GET_CONSENT_IMAGE_REQUEST);
const getConsentImageRequestSuccess = createAction(GET_CONSENT_IMAGE_REQUEST_SUCCESS);
const getConsentImageRequestFailed = createAction(GET_CONSENT_IMAGE_REQUEST_FAILED);

const saveConsentRequest = createAction(SAVE_CONSENT_FORM_REQUEST);
const saveConsentRequestSuccess = createAction(SAVE_CONSENT_FORM_REQUEST_SUCCESS);
const saveConsentRequestFailed = createAction(SAVE_CONSENT_FORM_REQUEST_FAILED);

const resetConsentReducer = createAction(RESET_CONSENT_REDUCER);

export const actions = {
    sendOTPRequest,
    sendOTPRequestSuccess,
    sendOTPRequestFailed,

    verifyOTPRequest,
    verifyOTPRequestSuccess,
    verifyOTPRequestFailed,

    getConsentImageRequest,
    getConsentImageRequestSuccess,
    getConsentImageRequestFailed,

    saveConsentRequest,
    saveConsentRequestSuccess,
    saveConsentRequestFailed,
    resetConsentReducer,
};

export const types = {
    SEND_OTP_REQUEST,
    SEND_OTP_REQUEST_SUCCESS,
    SEND_OTP_REQUEST_FAILED,

    VERIFY_OTP_REQUEST,
    VERIFY_OTP_REQUEST_SUCCESS,
    VERIFY_OTP_REQUEST_FAILED,

    GET_CONSENT_IMAGE_REQUEST,
    GET_CONSENT_IMAGE_REQUEST_SUCCESS,
    GET_CONSENT_IMAGE_REQUEST_FAILED,

    SAVE_CONSENT_FORM_REQUEST,
    SAVE_CONSENT_FORM_REQUEST_SUCCESS,
    SAVE_CONSENT_FORM_REQUEST_FAILED,
    RESET_CONSENT_REDUCER,
};
