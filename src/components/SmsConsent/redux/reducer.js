import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.SEND_OTP_REQUEST]: state => ({
        ...state,
        loader: true,
    }),
    [types.SEND_OTP_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        contact_number: payload.contact_number,
        carrier_code: payload.carrier_code,
        step: payload.isResend ? state.step : (state.step + 1) % 4,
    }),
    [types.SEND_OTP_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loader: false,
    }),
    [types.VERIFY_OTP_REQUEST]: state => ({
        ...state,
        loader: true,
    }),
    [types.VERIFY_OTP_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        step: (state.step + 1) % 4,
        customer_details: payload.customer_details,
    }),
    [types.VERIFY_OTP_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loader: false,
    }),
    [types.GET_CONSENT_IMAGE_REQUEST]: state => ({
        ...state,
    }),
    [types.GET_CONSENT_IMAGE_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        consent_enable: payload.consent_enable === '1',
        consent_banner_desktop: payload.consent_banner_desktop,
        consent_banner_mobile: payload.consent_banner_mobile,
        checkbox_message: payload.checkbox_message,
    }),
    [types.GET_CONSENT_IMAGE_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
    }),
    [types.SAVE_CONSENT_FORM_REQUEST]: state => ({
        ...state,
        loader: true,
    }),
    [types.SAVE_CONSENT_FORM_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        step: (state.step + 1) % 4,
        popup_message: payload.message,
    }),
    [types.SAVE_CONSENT_FORM_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loader: false,
    }),
    [types.RESET_CONSENT_REDUCER]: (state, { payload }) => ({
        ...state,
        step: 0,
        loader: false,
        carrier_code: '',
        contact_number: '',
        customer_details: null,
        popup_message: '',
    }),
};

export default handleActions(actionHandler, {
    step: 0,
    loader: false,
    carrier_code: '',
    contact_number: '',
    customer_details: null,
    consent_enable: false,
    consent_banner_desktop: null,
    consent_banner_mobile: null,
    popup_message: '',
    checkbox_message: 'Allow your consent to share your details for better user experience with other website.',
});
