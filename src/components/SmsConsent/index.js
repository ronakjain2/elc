import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import StepFourSuccess from './components/StepFourSuccess';
import StepOnePhoneInput from './components/StepOnePhoneInput';
import StepThreeConsentForm from './components/StepThreeConsentForm';
import StepTwoOTPVerification from './components/StepTwoOTPVerification';
import './css/style.css';
import { actions } from './redux/actions';

const SmsConsent = () => {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const smsConsent = state && state.smsConsent;
    const step = smsConsent && smsConsent.step;
    const store_id = state && state.auth && state.auth.currentStore;

    useEffect(() => {
        const payload = {
            store_id,
        };
        dispatch(actions.getConsentImageRequest(payload));
    }, [dispatch, store_id]);

    const getScreen = () => {
        switch (step) {
            case 0:
                return <StepOnePhoneInput />;
            case 1:
                return <StepTwoOTPVerification />;
            case 2:
                return <StepThreeConsentForm />;
            case 3:
                return <StepFourSuccess />;
            default:
                return null;
        }
    };

    return <>{getScreen()}</>;
};

export default SmsConsent;
