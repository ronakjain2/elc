import api from 'apiServices';

const sendOTP = payload => api.post('/index.php/rest/V1/app/sendotp', payload);
const verifyOTP = payload => api.post('/index.php/rest/V1/app/verifyotp', payload);
const getConsentImage = ({ store_id }) => api.get(`/index.php/rest/V1/app/consentimage?store=${store_id}`);
const saveConsentForm = payload => api.post('/index.php/rest/V1/app/consent', payload);

export default {
    sendOTP,
    verifyOTP,
    getConsentImage,
    saveConsentForm,
};
