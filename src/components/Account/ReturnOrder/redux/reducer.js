import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_REASON_CODE_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_REASON_CODE_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.GET_REASON_CODE_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        reasonCodes: payload || [],
    }),

    [types.RETURN_ORDER_REQUEST]: (state) => ({
        ...state,
        returnOrderLoader: true,
    }),
    [types.RETURN_ORDER_REQUEST_FAILED]: (state) => ({
        ...state,
        returnOrderLoader: false,
    }),
    [types.RETURN_ORDER_REQUEST_SUCCESS]: (state) => ({
        ...state,
        returnOrderLoader: false,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    returnOrderLoader: false,
    reasonCodes: [],
});
