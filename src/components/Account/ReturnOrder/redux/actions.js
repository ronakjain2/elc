import { createAction } from 'redux-actions';

//actions types
const GET_REASON_CODE_REQUEST = 'ELC@OROB/GET_REASON_CODE_REQUEST';
const GET_REASON_CODE_REQUEST_SUCCESS = 'ELC@OROB/GET_REASON_CODE_REQUEST_SUCCESS';
const GET_REASON_CODE_REQUEST_FAILED = 'ELC@OROB/GET_REASON_CODE_REQUEST_FAILED';

const RETURN_ORDER_REQUEST = 'ELC@OROB/RETURN_ORDER_REQUEST';
const RETURN_ORDER_REQUEST_SUCCESS = 'ELC@OROB/RETURN_ORDER_REQUEST_SUCCESS';
const RETURN_ORDER_REQUEST_FAILED = 'ELC@OROB/RETURN_ORDER_REQUEST_FAILED';

//actions methods
const getReasonCodeRequest = createAction(GET_REASON_CODE_REQUEST);
const getReasonCodeRequestSuccess = createAction(GET_REASON_CODE_REQUEST_SUCCESS);
const getReasonCodeRequestFailed = createAction(GET_REASON_CODE_REQUEST_FAILED);

const returnOrderRequest = createAction(RETURN_ORDER_REQUEST);
const returnOrderRequestSuccess = createAction(RETURN_ORDER_REQUEST_SUCCESS);
const returnOrderRequestFailed = createAction(RETURN_ORDER_REQUEST_FAILED);

//actions
export const actions = {
    getReasonCodeRequest,
    getReasonCodeRequestSuccess,
    getReasonCodeRequestFailed,

    returnOrderRequest,
    returnOrderRequestSuccess,
    returnOrderRequestFailed,
};

//types
export const types = {
    GET_REASON_CODE_REQUEST,
    GET_REASON_CODE_REQUEST_SUCCESS,
    GET_REASON_CODE_REQUEST_FAILED,

    RETURN_ORDER_REQUEST,
    RETURN_ORDER_REQUEST_SUCCESS,
    RETURN_ORDER_REQUEST_FAILED,
};
