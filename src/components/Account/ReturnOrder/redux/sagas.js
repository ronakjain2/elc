import { call, put, takeLatest, select } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../../api';
import { toast } from 'react-toastify';
import { push } from 'connected-react-router';
import { createEncryptOrderId } from 'components/Cart/Checkout/utils';

const auth = (state) => state && state.auth;

const getReasonCodeReq = function* getReasonCodeReq({ payload }) {
    try {
        yield put(
            actions.getReasonCodeRequestSuccess([
                {
                    label: 'Product quality doesn’t match expectation',
                    value: 'RET1',
                },
                {
                    label: 'Product no longer needed',
                    value: 'RET2',
                },
                {
                    label: 'Product is damaged',
                    value: 'RET3',
                },
                {
                    label: 'Product doesn’t match the description / catalog image',
                    value: 'RET4',
                },
                {
                    label: 'Wrong product was shipped',
                    value: 'RET5',
                },
                {
                    label: 'Delivery was delayed',
                    value: 'RET6',
                },
            ]),
        );
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getReasonCodeRequestFailed());
    }
};

const returnOrderReq = function* returnOrderReq({ payload }) {
    try {
        const { orderId, returnOrder, parent_order_id } = payload;
        const authData = yield select(auth);
        const { data } = yield call(api.returnOrderRequest, returnOrder);
        if (data && data.status) {
            yield put(
                push(
                    `/${authData && authData.store_locale}/order-details?orderId=${createEncryptOrderId(
                        orderId,
                    )}&order_increment_id=${createEncryptOrderId(parent_order_id)}`,
                ),
            );
            yield put(actions.returnOrderRequestSuccess());
            toast.success(
                'The item(s) in your order have been returned successfully. We will update the order status in My Account Section very soon!',
            );
        } else {
            toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
            yield put(actions.returnOrderRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.returnOrderRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_REASON_CODE_REQUEST, getReasonCodeReq);
    yield takeLatest(types.RETURN_ORDER_REQUEST, returnOrderReq);
}
