import React from 'react';
import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';

import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { createShippingAddress, removeQtyButtonDisable, addQtyButtonDisable } from '../../utils';

export default function ReturnForm({ item, shipping_address, returnOrderItem, setReturnOrderItem, isValidate }) {
    const state = useSelector((state) => state);

    const reasonCodes = (state && state.returnOrder && state.returnOrder.reasonCodes) || [];

    const index = returnOrderItem.findIndex((i) => i.order_id === item.order_increment_id);
    const indexItem =
        returnOrderItem[index] &&
        returnOrderItem[index]['item_data'] &&
        returnOrderItem[index]['item_data'].findIndex((x) => x.item_sku === item.sku);

    const onChangeReasonReturnForm = (name) => (event) => {
        let arr = [...returnOrderItem] || [];
        let obj = arr[index] || {};
        obj['item_data'][indexItem][name] = event.target.value;
        arr[index] = obj;
        setReturnOrderItem(arr);
    };

    const onChangeQtyReturnForm = (name, qty) => {
        let arr = [...returnOrderItem] || [];
        let obj = arr[index] || {};
        obj['item_data'][indexItem][name] = qty;
        arr[index] = obj;
        setReturnOrderItem(arr);
    };

    return (
        <Grid container>
            <Grid item xs={12} md={4} className="arabic-right">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="MyAccount.ReturnOrder.Quantity" defaultMessage="Quantity*" />
                </Typography>

                <ButtonGroup className="qty_button" variant="contained" aria-label="contained primary button group">
                    <Button
                        disabled={removeQtyButtonDisable(
                            item,
                            returnOrderItem &&
                                returnOrderItem[index] &&
                                returnOrderItem[index]['item_data'] &&
                                returnOrderItem[index]['item_data'][indexItem] &&
                                returnOrderItem[index]['item_data'][indexItem].qty,
                        )}
                        onClick={() =>
                            onChangeQtyReturnForm(
                                'qty',
                                returnOrderItem &&
                                    returnOrderItem[index] &&
                                    returnOrderItem[index]['item_data'] &&
                                    returnOrderItem[index]['item_data'][indexItem] &&
                                    returnOrderItem[index]['item_data'][indexItem].qty &&
                                    parseInt(returnOrderItem[index]['item_data'][indexItem].qty - 1),
                            )
                        }
                    >
                        <RemoveIcon />
                    </Button>
                    <Button disabled={true} className="qty_input">
                        {returnOrderItem &&
                            returnOrderItem[index] &&
                            returnOrderItem[index]['item_data'] &&
                            returnOrderItem[index]['item_data'][indexItem] &&
                            returnOrderItem[index]['item_data'][indexItem].qty}
                    </Button>
                    <Button
                        disabled={addQtyButtonDisable(
                            item,
                            returnOrderItem &&
                                returnOrderItem[index] &&
                                returnOrderItem[index]['item_data'] &&
                                returnOrderItem[index]['item_data'][indexItem] &&
                                returnOrderItem[index]['item_data'][indexItem].qty,
                        )}
                        onClick={() =>
                            onChangeQtyReturnForm(
                                'qty',
                                returnOrderItem &&
                                    returnOrderItem[index] &&
                                    returnOrderItem[index]['item_data'] &&
                                    returnOrderItem[index]['item_data'][indexItem] &&
                                    returnOrderItem[index]['item_data'][indexItem].qty &&
                                    parseInt(returnOrderItem[index]['item_data'][indexItem].qty - 1),
                            )
                        }
                    >
                        <AddIcon />
                    </Button>
                </ButtonGroup>
            </Grid>

            <Grid item xs={12} md={4} className="mobileTopPadding">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="MyAccount.ReturnOrder.ReasonForReturn" defaultMessage="Reason For Return*" />
                </Typography>
                <TextField
                    variant="outlined"
                    fullWidth
                    select
                    value={
                        returnOrderItem &&
                        returnOrderItem[index] &&
                        returnOrderItem[index]['item_data'] &&
                        returnOrderItem[index]['item_data'][indexItem] &&
                        returnOrderItem[index]['item_data'][indexItem].reasonCode
                    }
                    onChange={onChangeReasonReturnForm('reasonCode')}
                    className="inputBox reasonCode"
                    error={
                        isValidate &&
                        (!returnOrderItem ||
                            !returnOrderItem[index] ||
                            (returnOrderItem[index]['item_data'] &&
                                returnOrderItem[index]['item_data'][indexItem] &&
                                returnOrderItem[index]['item_data'][indexItem].reasonCode === ''))
                    }
                >
                    {reasonCodes &&
                        reasonCodes.map((row, index) => (
                            <MenuItem key={`return_order_reason_code_${index}`} value={row.value}>
                                {row.label}
                            </MenuItem>
                        ))}
                </TextField>
                {isValidate &&
                    (!returnOrderItem ||
                        !returnOrderItem[index] ||
                        (returnOrderItem[index]['item_data'] &&
                            returnOrderItem[index]['item_data'][indexItem] &&
                            returnOrderItem[index]['item_data'][indexItem].reasonCode === '')) && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="MyAccount.ReturnOrder.ReasonCode.Required"
                                defaultMessage="Please select reason code"
                            />
                        </FormHelperText>
                    )}
            </Grid>

            {/*<Grid item xs={12} style={{ paddingTop: 20 }}>
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="MyAccount.ReturnOrder.Comment" defaultMessage="Comment" />
                </Typography>
                <TextField
                    variant="outlined"
                    fullWidth
                    multiline
                    rows={1}
                    className="commentBox"
                />
            </Grid>*/}

            <Grid item xs={12}>
                <div className="border-line" />
            </Grid>

            <Grid item xs={12} style={{ paddingTop: 20 }} className="arabic-right">
                <Typography className="PickupAddress">
                    <FormattedMessage id="MyAccount.ReturnOrder.PickupAddress" defaultMessage="Pickup Address" />
                </Typography>
                <Typography className="PickupAddressValue">
                    {shipping_address && createShippingAddress(shipping_address)}
                </Typography>
            </Grid>
        </Grid>
    );
}
