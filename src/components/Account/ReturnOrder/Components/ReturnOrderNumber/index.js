import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Typography from '@material-ui/core/Typography';

import { withRouter } from 'react-router';
import { FormattedMessage } from 'react-intl';
import { createEncryptOrderId } from 'components/Cart/Checkout/utils';

const SimpleBreadcrumbs = lazy(() => import('commonComponet/Breadcrumbs'));

function ReturnOrderNumber({ history, orderId, language, global, orderIncrementId }) {
    const gotoOrderDetails = () => {
        return history.push(
            `/${global && global.store_locale}/order-details?orderId=${createEncryptOrderId(
                orderId,
            )}&order_increment_id=${createEncryptOrderId(orderIncrementId)}`,
        );
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: <FormattedMessage id="Home.MyAccount" defaultMessage="My Account" />,
        },
    ];

    return (
        <Grid container>
            <Grid item xs={12}>
                <Hidden only={['xs', 'sm']}>
                    <Grid container justify="space-between" alignItems="center">
                        <Grid item xs={4} className="arabic-right">
                            <Button
                                onClick={() => gotoOrderDetails()}
                                className="my-order-details-back-button"
                                startIcon={language === 'en' ? <ChevronLeft /> : <ChevronRight />}
                            >
                                <FormattedMessage
                                    id="MyAccount.ReturnOrder.BackToOrderDetails"
                                    defaultMessage="Back to Order Details"
                                />
                            </Button>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography className="return-title textAlignCenter">
                                <FormattedMessage
                                    id="MyAccount.OrederDetails.ReturnOrder"
                                    defaultMessage="Return Order"
                                />
                            </Typography>
                        </Grid>
                        <Grid item xs={4}></Grid>
                    </Grid>
                </Hidden>

                <Hidden only={['md', 'lg', 'xl']}>
                    <Suspense fallback={'Loading.....'}>
                        <Grid item xs={12}>
                            <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                        </Grid>
                    </Suspense>

                    <Grid container justify="space-between" alignItems="center">
                        <div>
                            <Typography className="return-title textAlignCenter">
                                <FormattedMessage
                                    id="MyAccount.OrederDetails.ReturnOrder"
                                    defaultMessage="Return Order"
                                />
                            </Typography>
                        </div>

                        <div>
                            <Button
                                onClick={() => gotoOrderDetails()}
                                className="my-order-details-back-button"
                                startIcon={language === 'en' ? <ChevronLeft /> : <ChevronRight />}
                            >
                                <FormattedMessage
                                    id="MyAccount.ReturnOrder.BackToOrderDetails"
                                    defaultMessage="Back to Order Details"
                                />
                            </Button>
                        </div>
                    </Grid>

                    <div className="gray-line" />
                </Hidden>
            </Grid>
        </Grid>
    );
}

export default withRouter(ReturnOrderNumber);
