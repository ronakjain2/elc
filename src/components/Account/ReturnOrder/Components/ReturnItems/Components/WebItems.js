import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { getCartAmount, checkItemIsFree } from 'components/Cart/Basket/utils';
import { getSelectedItemIndex, showCheckboxOnReturn } from 'components/Account/ReturnOrder/utils';
import { FormattedMessage } from 'react-intl';
import { returnOrderClassName } from 'components/Account/MyOrder/utils';
import moment from 'moment';

import clsx from 'clsx';

const ReturnForm = lazy(() => import('../../ReturnForm'));

export default function ReturnOrderItemWebView({
    item,
    shipping_address,
    returnOrderItem,
    setReturnOrderItem,
    isValidate,
}) {
    const onClickCheckboxButton = (item) => {
        let arr = [...returnOrderItem];
        const index = arr.findIndex((i) => i.order_id === item.order_increment_id);
        if (index === -1) {
            arr.push({
                order_id: item.order_increment_id,
                extRefNo: item.order_increment_id,
                item_data: [
                    {
                        item_line_no: item.item_line_no,
                        item_sku: item.sku,
                        qty: item.qty_ordered,
                        reasonCode: '',
                    },
                ],
            });
        } else {
            const indexIndex = arr[index] && arr[index].item_data.findIndex((i) => i.item_sku === item.sku);

            if (indexIndex !== -1) {
                arr[index] && arr[index].item_data.splice(indexIndex, 1);
            } else {
                arr[index].item_data.push({
                    item_line_no: item.item_line_no,
                    item_sku: item.sku,
                    qty: item.qty_ordered,
                    reasonCode: '',
                });
            }
            if (!arr[index].item_data || arr[index].item_data.length === 0) {
                arr.splice(index, 1);
            }
        }
        setReturnOrderItem(arr);
    };

    return (
        <Grid container alignItems="center" justify="center" className="padd-top-bottom-10-20">
            <Grid item xs={12}>
                <div className="item-order-details-line"></div>
            </Grid>
            <Grid item xs={1}>
                {showCheckboxOnReturn(item) && (
                    <Checkbox
                        checked={getSelectedItemIndex(returnOrderItem, item) === -1 ? false : true}
                        onChange={() => onClickCheckboxButton(item)}
                        size="small"
                        inputProps={{ 'aria-label': 'checkbox with small size' }}
                    />
                )}
            </Grid>
            <Grid item xs={6} className="arabic-right">
                <Grid container alignItems="center">
                    {/* <Grid item xs={1}>
                        {showCheckboxOnReturn(item) && (
                            <Checkbox
                                checked={getSelectedItemIndex(returnOrderItem, item) === -1 ? false : true}
                                onChange={() => onClickCheckboxButton(item)}
                                size="small"
                                inputProps={{ 'aria-label': 'checkbox with small size' }}
                            />
                        )}
                    </Grid> */}
                    <Grid
                        item
                        xs={3}
                        className={getSelectedItemIndex(returnOrderItem, item) === -1 ? 'opacity-class' : ''}
                    >
                        <img
                            src={item && item.image && item.image[0]}
                            alt="productImage"
                            className="web-order-detail-image"
                        />
                    </Grid>
                    <Grid
                        item
                        xs={6}
                        className={getSelectedItemIndex(returnOrderItem, item) === -1 ? 'opacity-class' : ''}
                    >
                        <Typography className="order-details-product-name">{item && item.name}</Typography>
                        <Typography className="order-details-product-specification">
                            <FormattedMessage id="PDP.Specifications.ProductCode" defaultMessage="Product Code" />:
                            &nbsp;{item && item.sku}
                        </Typography>
                        {item && item.returnable_date && !checkItemIsFree(item) && (
                            <Typography className="order-details-product-specification">
                                <FormattedMessage
                                    id="PDP.Specifications.Return"
                                    defaultMessage="Return Validity Date"
                                />
                                : &nbsp;{moment(item && item.returnable_date).format('DD-MM-YYYY')}
                            </Typography>
                        )}
                        {item && item.returnable === 0 && !checkItemIsFree(item) && (
                            <Typography className="order-details-product-specification not-returnable-msg">
                                <FormattedMessage
                                    id="MyAccount.ReturnOrder.NotAvailable"
                                    defaultMessage="This item is not returnable."
                                />
                            </Typography>
                        )}
                        {/*<Typography className="order-details-product-specification">
                            <FormattedMessage id="PDP.Age" defaultMessage="Age" />
                            :
                        </Typography>*/}
                        {/* <Typography className="order-details-product-specification">
                            <FormattedMessage id="PDP.Specifications.ProductCode" defaultMessage="Product Code" />:
                            &nbsp;{item && item.sku}
                        </Typography>
                        {item && item.returnable_date && (
                            <Typography style={{ textAlign: 'start' }} className="order-details-product-specification">
                                <FormattedMessage
                                    id="PDP.Specifications.Return"
                                    defaultMessage="Return Validity Date"
                                />
                                : &nbsp;{moment(item && item.returnable_date).format('DD-MM-YYYY')}
                            </Typography>
                        )} */}
                    </Grid>
                </Grid>
            </Grid>

            <Grid item xs={1} className={getSelectedItemIndex(returnOrderItem, item) === -1 ? 'opacity-class' : ''}>
                <Typography className="qty">{item && item.qty_ordered}</Typography>
            </Grid>

            <Grid
                item
                xs={2}
                className={
                    getSelectedItemIndex(returnOrderItem, item) === -1
                        ? 'opacity-class arabic-right textAlignCenter'
                        : 'arabic-right textAlignCenter'
                }
            >
                {getCartAmount(item)}
            </Grid>

            <Grid item xs={2}>
                <Typography className={clsx('order-details-status', returnOrderClassName(item && item.status))}>
                    {item && item.status}
                </Typography>
            </Grid>

            {getSelectedItemIndex(returnOrderItem, item) !== -1 && (
                <Grid item xs={8} className="return-form-container">
                    <Suspense fallback={`Loading....`}>
                        <ReturnForm
                            item={item}
                            shipping_address={shipping_address}
                            returnOrderItem={returnOrderItem}
                            setReturnOrderItem={setReturnOrderItem}
                            isValidate={isValidate}
                        />
                    </Suspense>
                </Grid>
            )}

            {/* <Grid item xs={12}>
                <div className="gray-line-1" />
            </Grid> */}
        </Grid>
    );
}
