import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { FormattedMessage } from 'react-intl';
import { getCartAmount, checkItemIsFree } from 'components/Cart/Basket/utils';
import { getSelectedItemIndex, showCheckboxOnReturn } from 'components/Account/ReturnOrder/utils';
import { returnOrderClassName } from 'components/Account/MyOrder/utils';
import clsx from 'clsx';
import moment from 'moment';

const ReturnForm = lazy(() => import('../../ReturnForm'));

export default function ReturnOrderItemMobileView({
    item,
    shipping_address,
    returnOrderItem,
    setReturnOrderItem,
    isValidate,
}) {
    const onClickCheckboxButton = (item) => {
        let arr = [...returnOrderItem];
        const index = arr.findIndex((i) => i.order_id === item.order_increment_id);
        if (index === -1) {
            arr.push({
                order_id: item.order_increment_id,
                extRefNo: item.order_increment_id,
                item_data: [
                    {
                        item_line_no: item.item_line_no,
                        item_sku: item.sku,
                        qty: item.qty_ordered,
                        reasonCode: '',
                    },
                ],
            });
        } else {
            const indexIndex = arr[index] && arr[index].item_data.findIndex((i) => i.item_sku === item.sku);

            if (indexIndex !== -1) {
                arr[index] && arr[index].item_data.splice(indexIndex, 1);
            } else {
                arr[index].item_data.push({
                    item_line_no: item.item_line_no,
                    item_sku: item.sku,
                    qty: item.qty_ordered,
                    reasonCode: '',
                });
            }
            if (!arr[index].item_data || arr[index].item_data.length === 0) {
                arr.splice(index, 1);
            }
        }
        setReturnOrderItem(arr);
    };

    return (
        <Grid container justify="space-between" className="padd-bottom-20 position-relative">
            <Grid item xs={1} className="flex alignItemCenter">
                {showCheckboxOnReturn(item) && (
                    <Checkbox
                        checked={getSelectedItemIndex(returnOrderItem, item) === -1 ? false : true}
                        onChange={() => onClickCheckboxButton(item)}
                        inputProps={{ 'aria-label': 'checkbox with small size' }}
                    />
                )}
            </Grid>

            <Grid
                item
                xs={5}
                className={
                    getSelectedItemIndex(returnOrderItem, item) === -1 ? 'opacity-class arabic-right' : 'arabic-right'
                }
            >
                <img src={item && item.image && item.image[0]} alt="productImage" className="web-order-detail-image" />
            </Grid>

            <Grid
                item
                xs={6}
                className={
                    getSelectedItemIndex(returnOrderItem, item) === -1 ? 'opacity-class arabic-right' : 'arabic-right'
                }
            >
                <Typography className="order-details-product-name textAlignRight">{item && item.name}</Typography>
                <Typography className="qty textAlignRight">
                    <FormattedMessage id="Cart.Quantity" defaultMessage="Quantity" />
                    :&nbsp;
                    {item && item.qty_ordered}
                </Typography>
                {/* {item && item.returnable_date && (
                    <Typography
                        style={{ textAlign: 'start' }}
                        className="order-details-product-specification textAlignRight"
                    >
                        <FormattedMessage id="PDP.Specifications.Return" defaultMessage="Return Validity Date" />:
                        &nbsp;{moment(item && item.returnable_date).format('DD-MM-YYYY')}
                    </Typography>
                )} */}

                <div className={'textAlignRight'}>{getCartAmount(item)}</div>
                <Typography
                    className={clsx(
                        'order-details-mobi-status',
                        returnOrderClassName(item && item.status),
                        'textAlignRight',
                    )}
                >
                    <FormattedMessage id="MyAccount.Status" defaultMessage="Status" />: &nbsp;{item && item.status}
                </Typography>
                {item && item.returnable_date && !checkItemIsFree(item) && (
                    <Typography className="whitespace-nowrap order-details-product-specification text-align-end">
                        <FormattedMessage id="PDP.Specifications.Return" defaultMessage="Return Validity Date" />:
                        &nbsp;{moment(item && item.returnable_date).format('DD-MM-YYYY')}
                    </Typography>
                )}
                {item && item.returnable === 0 && !checkItemIsFree(item) && (
                    <Typography className="order-details-product-specification not-returnable-msg text-align-end">
                        <FormattedMessage
                            id="MyAccount.ReturnOrder.NotAvailable"
                            defaultMessage="This item is not returnable."
                        />
                    </Typography>
                )}
            </Grid>
            {getSelectedItemIndex(returnOrderItem, item) !== -1 && (
                <Grid item xs={12} className="return-form-container">
                    <Suspense fallback={`Loading....`}>
                        <ReturnForm
                            item={item}
                            shipping_address={shipping_address}
                            returnOrderItem={returnOrderItem}
                            setReturnOrderItem={setReturnOrderItem}
                            isValidate={isValidate}
                        />
                    </Suspense>
                </Grid>
            )}
        </Grid>
    );
}
