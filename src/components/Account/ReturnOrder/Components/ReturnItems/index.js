import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import Spinner from 'commonComponet/Spinner';
import Hidden from '@material-ui/core/Hidden';

const ReturnOrderItemWebView = lazy(() => import('./Components/WebItems'));
const ReturnOrderItemMobileView = lazy(() => import('./Components/MobileItems'));

export default function ReturnItems({ details, returnOrderItem, setReturnOrderItem, isValidate }) {
    const items = (details && details.sub_order_details) || [];
    const shipping_address = (details && details.shipping_address) || {};

    return (
        <Suspense fallback={<Spinner />}>
            {items &&
                Object.keys(items) &&
                Object.keys(items).map((key, index) => (
                    <Grid
                        container
                        justify="center"
                        spacing={3}
                        style={{ marginBottom: '2rem' }}
                        className="boxShadow1"
                        key={`cancel_order_card${index}`}
                    >
                        <Grid item xs={12} className="header-of-item">
                            <div className="header-flex">
                                <div>
                                    <Typography className="order-details-header-name">
                                        Shipment {index + 1} Of {Object.keys(items).length}
                                    </Typography>
                                    <Typography className="order-details-product-specification">
                                        <FormattedMessage id="MyAccount.OrderID" defaultMessage="Order Id" />: &nbsp;
                                        {items[key].increment_id}
                                    </Typography>
                                </div>
                                <div>
                                    <Typography className="order-details-header-name textAlignRight">
                                        <FormattedMessage
                                            id="Checkout.Payment.DeliveryOption"
                                            defaultMessage="Order Items"
                                        />
                                    </Typography>
                                    <Typography className="order-details-product-specification textAlignRight">
                                        {items[key].delivery_option}
                                    </Typography>
                                </div>
                            </div>
                            {/* <Typography className="order-items-title">
                    <FormattedMessage id="MyAccount.MyOrderDetails.OrderItems" defaultMessage="Order Items" />
                </Typography> */}
                        </Grid>
                        {/* <Typography className="sub-title-return-order">
                    <FormattedMessage
                        id="MyAccount.ReturnOrder.PleaseSelectTheItemsYouWantToReturn"
                        defaultMessage="Please select the items you want to return"
                    />
                </Typography> */}

                        <Hidden only={['xs', 'sm']}>
                            <Grid item xs={12}>
                                <Grid container className="order-list-grid" spacing={1}>
                                    <Grid item xs={1}></Grid>
                                    <Grid item xs={6}>
                                        <Typography className="order-item-header-name">
                                            <FormattedMessage id="Cart.ItemNumberStyle" defaultMessage="ITEM" />
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1}>
                                        <Typography className="order-item-header-name textAlignCenter">
                                            <FormattedMessage id="Cart.Quantity" defaultMessage="QUANTITY" />
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={2} className="textAlignCenter-c">
                                        <Typography className="order-item-header-name textAlignCenter-c">
                                            <FormattedMessage id="Cart.Price" defaultMessage="PRICE" />
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={2}>
                                        <Typography className="order-item-header-name textAlignCenter">
                                            <FormattedMessage id="MyAccount.Status" defaultMessage="STATUS" />
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            {/* <Grid item xs={12} className="item-order-details-line">
                        <Grid container>
                            <Grid item xs={6} className="arabic-right">
                                <Typography className="web-order-item-header-name">
                                    <FormattedMessage id="Cart.ItemNumberStyle" defaultMessage="Item (style number)" />
                                </Typography>
                            </Grid>

                            <Grid item xs={2} className="textAlignCenter-c">
                                <Typography className="web-order-item-header-name textAlignCenter-c">
                                    <FormattedMessage id="Cart.Price" defaultMessage="Price" />
                                </Typography>
                            </Grid>

                            <Grid item xs={1}>
                                <Typography className="web-order-item-header-name textAlignCenter">
                                    <FormattedMessage id="Cart.Quantity" defaultMessage="Quantity" />
                                </Typography>
                            </Grid>

                            <Grid item xs={3}>
                                <Typography className="web-order-item-header-name textAlignCenter">
                                    <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid> */}

                            <Grid item xs={12}>
                                {items[key].items_ordered &&
                                    items[key].items_ordered.map((item, index) => (
                                        <Grid container key={`return-order-details-item_web_${index}`}>
                                            <ReturnOrderItemWebView
                                                item={item}
                                                shipping_address={shipping_address}
                                                returnOrderItem={returnOrderItem}
                                                setReturnOrderItem={setReturnOrderItem}
                                                isValidate={isValidate}
                                            />
                                        </Grid>
                                    ))}
                            </Grid>
                        </Hidden>

                        <Hidden only={['md', 'lg', 'xl']}>
                            <Grid item xs={12}>
                                {items[key].items_ordered &&
                                    items[key].items_ordered.map((item, index) => (
                                        <Grid container key={`return-order-details-item_mobile_${index}`}>
                                            <ReturnOrderItemMobileView
                                                item={item}
                                                shipping_address={shipping_address}
                                                returnOrderItem={returnOrderItem}
                                                setReturnOrderItem={setReturnOrderItem}
                                                isValidate={isValidate}
                                            />
                                        </Grid>
                                    ))}
                            </Grid>
                        </Hidden>
                    </Grid>
                ))}
        </Suspense>
    );
}
