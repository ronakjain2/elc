import React, { lazy, Suspense, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Spinner from 'commonComponet/Spinner';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';

import { getOrderId, createEncryptOrderId } from 'components/Cart/Checkout/utils';
import { useSelector, useDispatch } from 'react-redux';
import { actions as myOrderActions } from 'components/Account/MyOrder/redux/actions';
import { actions as returnOrderActions } from 'components/Account/ReturnOrder/redux/actions';
import './returnOrder.css';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';
import { IsValidateReturnOrderData } from './utils';
import { getGWPLineItemNo } from '../CancelOrder/utils';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const ReturnOrderNumber = lazy(() => import('./Components/ReturnOrderNumber'));
const ReturnItems = lazy(() => import('./Components/ReturnItems'));
const AddressAndPaymentOrderDetail = lazy(() => import('../MyOrderDetails/Components/AddressAndPayment'));
const OrderDetailsCharges = lazy(() => import('../MyOrderDetails/Components/Charges'));

function ReturnOrder({ history }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const store_id = state && state.auth && state.auth.currentStore;
    const loader = state && state.myOrder && state.myOrder.loader;
    const language = state && state.auth && state.auth.language;
    const global = state && state.auth;

    const details = state && state.myOrder && state.myOrder.orderDetails;
    const returnOrderLoader = state && state.returnOrder && state.returnOrder.returnOrderLoader;

    const query = new URLSearchParams(window.location.search);
    const order_id = getOrderId(query.get('orderId'));
    const orderIncrementId = getOrderId(query.get('order_increment_id'));

    const [returnOrderItem, setReturnOrderItem] = useState([]);
    const [isValidate, setIsValidate] = useState(false);
    const [comment, setComment] = useState('');

    useEffect(() => {
        dispatch(
            myOrderActions.getOrderDetailRequest({
                orderEntityid: order_id,
                store_id: store_id,
                order_id: orderIncrementId,
            }),
        );
    }, [dispatch, order_id, store_id, orderIncrementId]);

    useEffect(() => {
        try {
            dispatch(returnOrderActions.getReasonCodeRequest({ store_id }));
        } catch (err) {}
    }, [dispatch, store_id]);

    const submitReturnOrder = () => {
        setIsValidate(true);
        if (IsValidateReturnOrderData(returnOrderItem)) {
            return;
        }
        setIsValidate(false);
        const gwp_line_item = getGWPLineItemNo(details && details.sub_order_details);

        dispatch(
            returnOrderActions.returnOrderRequest({
                returnOrder: {
                    parent_order_id: orderIncrementId,
                    comment,
                    itemData: returnOrderItem,
                    gwp_line_item_no: gwp_line_item,
                },
                orderId: order_id,
                parent_order_id: orderIncrementId,
            }),
        );
    };

    const onClickCancel = () => {
        history.push(
            `/${global && global.store_locale}/order-details?orderId=${createEncryptOrderId(
                order_id,
            )}&order_increment_id=${createEncryptOrderId(orderIncrementId)}`,
        );
    };

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center">
                <Grid container justify="center" className="return-order-details-container">
                    {!loader && (
                        <Grid item xs={11} className="padding-10">
                            <Grid container justify="center" spacing={5}>
                                <Hidden only={['xs', 'sm']}>
                                    {/** Tabs */}
                                    <Grid item xs={12}>
                                        <AccountTab activeKey="MyOrder" />
                                    </Grid>
                                </Hidden>

                                {/* Web View */}
                                <Hidden only={['xs', 'sm']}>
                                    <Grid item xs={12}>
                                        <ReturnOrderNumber
                                            orderId={order_id}
                                            language={language}
                                            global={global}
                                            orderIncrementId={orderIncrementId}
                                        />
                                    </Grid>

                                    <Grid item xs={8}>
                                        <ReturnItems
                                            details={details}
                                            returnOrderItem={returnOrderItem}
                                            setReturnOrderItem={setReturnOrderItem}
                                            isValidate={isValidate}
                                        />
                                        {returnOrderItem && returnOrderItem.length > 0 && (
                                            <>
                                                <Grid item xs={12}>
                                                    <Typography className="login-sigup-input-text">
                                                        <FormattedMessage
                                                            id="MyAccount.ReturnOrder.Comment"
                                                            defaultMessage="Comment"
                                                        />
                                                    </Typography>
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        multiline
                                                        rows={3}
                                                        value={comment}
                                                        onChange={(e) => setComment(e.target.value)}
                                                        className="commentBox"
                                                    />
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <div className="green-line" />
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <Grid container justify="center">
                                                        <Button
                                                            className="cancel-return-order-button"
                                                            onClick={() => onClickCancel()}
                                                        >
                                                            <FormattedMessage
                                                                id="Checkout.Cart.Delivery.Cancel"
                                                                defaultMessage="Cancel"
                                                            />
                                                        </Button>
                                                        {!returnOrderLoader && (
                                                            <Button
                                                                className="submit-return-order-button"
                                                                onClick={() => submitReturnOrder()}
                                                            >
                                                                <FormattedMessage
                                                                    id="MyAccount.ReturnOrder.ReturnItems"
                                                                    defaultMessage="Return Items"
                                                                />
                                                            </Button>
                                                        )}
                                                        {returnOrderLoader && <CircularProgress />}
                                                    </Grid>
                                                </Grid>
                                            </>
                                        )}
                                    </Grid>
                                    <Grid item xs={3}>
                                        <AddressAndPaymentOrderDetail details={details} />
                                        <OrderDetailsCharges details={details} />
                                    </Grid>
                                </Hidden>

                                {/* mobile view */}
                                <Hidden only={['md', 'lg', 'xl']}>
                                    <Grid item xs={12}>
                                        <ReturnOrderNumber
                                            orderId={order_id}
                                            language={language}
                                            global={global}
                                            orderIncrementId={orderIncrementId}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <AddressAndPaymentOrderDetail details={details} />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <ReturnItems
                                            details={details}
                                            returnOrderItem={returnOrderItem}
                                            setReturnOrderItem={setReturnOrderItem}
                                            isValidate={isValidate}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <OrderDetailsCharges details={details} />
                                        {returnOrderItem && returnOrderItem.length > 0 && (
                                            <>
                                                <Grid item xs={12}>
                                                    <Typography className="login-sigup-input-text">
                                                        <FormattedMessage
                                                            id="MyAccount.ReturnOrder.Comment"
                                                            defaultMessage="Comment"
                                                        />
                                                    </Typography>
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        multiline
                                                        rows={3}
                                                        value={comment}
                                                        onChange={(e) => setComment(e.target.value)}
                                                        className="commentBox"
                                                    />
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <div className="green-line" />
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <Grid container justify="center">
                                                        <Button
                                                            className="cancel-return-order-button"
                                                            onClick={() => onClickCancel()}
                                                        >
                                                            <FormattedMessage
                                                                id="Checkout.Cart.Delivery.Cancel"
                                                                defaultMessage="Cancel"
                                                            />
                                                        </Button>
                                                        {!returnOrderLoader && (
                                                            <Button
                                                                className="submit-return-order-button"
                                                                onClick={() => submitReturnOrder()}
                                                            >
                                                                <FormattedMessage
                                                                    id="MyAccount.ReturnOrder.ReturnItems"
                                                                    defaultMessage="Return Items"
                                                                />
                                                            </Button>
                                                        )}
                                                        {returnOrderLoader && <CircularProgress />}
                                                    </Grid>
                                                </Grid>
                                            </>
                                        )}
                                    </Grid>
                                </Hidden>
                            </Grid>
                        </Grid>
                    )}

                    {loader && (
                        <Grid container alignItems="center">
                            <Spinner />
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </Suspense>
    );
}

export default withRouter(ReturnOrder);
