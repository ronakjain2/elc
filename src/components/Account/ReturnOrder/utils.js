import { replaceTilCharacterToComma } from 'components/Cart/Checkout/Delivery/utils';
import { orderStatus } from 'services/OrderStatus';

export const createShippingAddress = (shipping_address) => {
    let address = '';
    if (shipping_address) {
        if (shipping_address.street) {
            address = replaceTilCharacterToComma(shipping_address.street);
        }

        if (shipping_address.city) {
            address = `${address}, ${shipping_address.city}`;
        }

        if (shipping_address.carrier_code) {
            address = `${address}, +${shipping_address.carrier_code} ${shipping_address.telephone}`;
        }
    }

    return address;
};

export const removeQtyButtonDisable = (item, qty) => {
    if (item.qty_ordered > 1 && qty > 1) {
        return false;
    }

    return true;
};

export const addQtyButtonDisable = (item, qty) => {
    let returnQty = 0;
    let returnArr = (item && item.return_info) || [];
    for (let i = 0; i < returnArr.length; i++) {
        try {
            returnQty = returnQty + parseInt(returnArr[i].return_req_qty);
        } catch (err) {}
    }

    let returnAvailableQty = item.qty_ordered - returnQty;
    if (returnAvailableQty === qty) {
        return true;
    }

    return false;
};

export const getSelectedItemIndex = (returnOrderItem, item) => {
    const index = returnOrderItem.findIndex((i) => i.order_id === item.order_increment_id);
    if (returnOrderItem[index]) {
        return returnOrderItem[index].item_data.findIndex((i) => i.item_sku === item.sku);
    } else {
        return -1;
    }
};

export const IsValidateReturnOrderData = (returnOrderItem) => {
    if (returnOrderItem) {
        for (let i = 0; i < returnOrderItem.length; i++) {
            if (returnOrderItem[i].item_data) {
                for (let j = 0; j < returnOrderItem[i].item_data.length; j++)
                    if (
                        !returnOrderItem[i].item_data[j].reasonCode ||
                        returnOrderItem[i].item_data[j].reasonCode === ''
                    ) {
                        return true;
                    }
            }
        }
        return false;
    }
    return true;
};

export const showCheckboxOnReturn = (item) => {
    if (item) {
        let returnQty = 0;
        let returnArr = (item && item.return_info) || [];
        for (let i = 0; i < returnArr.length; i++) {
            try {
                returnQty = returnQty + parseInt(returnArr[i].return_req_qty);
            } catch (err) {}
        }
        if (
            item &&
            item.price !== 0 &&
            parseInt(item.returnable) === 1 &&
            item.status.toLowerCase().replace(/ /g, '') === orderStatus.delivered.toLowerCase().replace(/ /g, '') &&
            parseInt(item.qty_ordered) > returnQty
        ) {
            return true;
        }
    }

    return false;
};
