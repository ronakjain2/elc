import React, { Suspense, lazy, useState, useEffect } from 'react';
import Spinner from 'commonComponet/Spinner';

import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import { useSelector, useDispatch } from 'react-redux';
import { getOrderId } from 'components/Cart/Checkout/utils';
import { withRouter } from 'react-router';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import CircularProgress from '@material-ui/core/CircularProgress';

import { getGWPLineItemNo, IsValidateCancelOrderData } from './utils';

import './cancelOrder.css';

import { actions as myOrderActions } from 'components/Account/MyOrder/redux/actions';
import { actions as cancelOrderActions } from 'components/Account/CancelOrder/redux/actions';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const CancelOrderNumber = lazy(() => import('./Components/CancelOrderNumber'));
const CancelItems = lazy(() => import('./Components/CancelItems'));
const AddressAndPaymentOrderDetail = lazy(() => import('../MyOrderDetails/Components/AddressAndPayment'));
const OrderDetailsCharges = lazy(() => import('../MyOrderDetails/Components/Charges'));

function CancelOrder({ history }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const store_id = state && state.auth && state.auth.currentStore;
    const loader = state && state.myOrder && state.myOrder.loader;
    const language = state && state.auth && state.auth.language;
    const global = state && state.auth;

    const details = state && state.myOrder && state.myOrder.orderDetails;
    const cancelOrderLoader = state && state.cancelOrder && state.cancelOrder.cancelOrderLoader;

    const query = new URLSearchParams(window.location.search);
    const order_id = getOrderId(query.get('orderId'));
    const orderIncrementId = getOrderId(query.get('order_increment_id'));

    const [cancelOrderItem, setCancelOrderItem] = useState([]);
    const [isValidate, setIsValidate] = useState(false);
    const [comment, setComment] = useState('');

    useEffect(() => {
        dispatch(
            myOrderActions.getOrderDetailRequest({
                orderEntityid: order_id,
                store_id: store_id,
                order_id: orderIncrementId,
            }),
        );
    }, [dispatch, order_id, store_id, orderIncrementId]);

    useEffect(() => {
        dispatch(cancelOrderActions.getCancelReasonCodeRequest());
    }, [dispatch]);

    // useEffect(() => {
    //     if (!isShowCancelButton(details && details.order_summary && details.order_summary.order_status)) {
    //         history.push(
    //             `/${global && global.store_locale}/order-details?orderId=${createEncryptOrderId(
    //                 order_id,
    //             )}&order_increment_id=${createEncryptOrderId(orderIncrementId)}`,
    //         );
    //     }
    // }, [details, history, order_id, global, orderIncrementId]);

    // const onClickCancel = () => {
    //     history.push(
    //         `/${global && global.store_locale}/order-details?orderId=${createEncryptOrderId(
    //             order_id,
    //         )}&order_increment_id=${createEncryptOrderId(orderIncrementId)}`,
    //     );
    // };

    const submitCancelOrder = () => {
        setIsValidate(true);
        if (IsValidateCancelOrderData(cancelOrderItem)) {
            return;
        }
        setIsValidate(false);
        const gwp_line_item = getGWPLineItemNo(details && details.sub_order_details);

        dispatch(
            cancelOrderActions.cancelOrderRequest({
                cancelOrder: {
                    parent_order_id: orderIncrementId,
                    comment,
                    itemData: cancelOrderItem,
                    gwp_line_item_no: gwp_line_item,
                    store_id: store_id,
                },
                orderId: order_id,
                parent_order_id: orderIncrementId,
            }),
        );
    };

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container>
                <Grid container justify="center" className="cancel-order-details-container">
                    {!loader && (
                        <Grid item xs={11}>
                            <Grid container spacing={5} justify="center" className="padding-10">
                                <Hidden only={['xs', 'sm']}>
                                    {/** Tabs */}
                                    <Grid item xs={12}>
                                        <AccountTab activeKey="MyOrder" />
                                    </Grid>
                                </Hidden>

                                {/* Mobile view */}
                                <Hidden only={['md', 'lg', 'xl']}>
                                    <Grid item xs={12}>
                                        <CancelOrderNumber
                                            orderId={order_id}
                                            language={language}
                                            global={global}
                                            orderIncrementId={orderIncrementId}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <AddressAndPaymentOrderDetail details={details} />
                                    </Grid>

                                    <Grid item xs={12}>
                                        <CancelItems
                                            details={details}
                                            cancelOrderItem={cancelOrderItem}
                                            setCancelOrderItem={setCancelOrderItem}
                                            isValidate={isValidate}
                                            comment={comment}
                                            setComment={setComment}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <OrderDetailsCharges details={details} />

                                        {cancelOrderItem && cancelOrderItem.length > 0 && (
                                            <>
                                                <Grid item xs={12}>
                                                    <Typography className="login-sigup-input-text">
                                                        <FormattedMessage
                                                            id="MyAccount.ReturnOrder.Comment"
                                                            defaultMessage="Comment"
                                                        />
                                                    </Typography>
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        multiline
                                                        rows={3}
                                                        value={comment}
                                                        onChange={(e) => setComment(e.target.value)}
                                                        className="commentBox"
                                                    />
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <div className="green-line" />
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <Grid container justify="center">
                                                        {!cancelOrderLoader && (
                                                            <Button
                                                                className="submit-return-order-button"
                                                                onClick={() => submitCancelOrder()}
                                                            >
                                                                <FormattedMessage
                                                                    id="MyAccount.CancelOrder.CancelItems"
                                                                    defaultMessage="Cancel Items"
                                                                />
                                                            </Button>
                                                        )}
                                                        {cancelOrderLoader && <CircularProgress />}
                                                    </Grid>
                                                </Grid>
                                            </>
                                        )}
                                    </Grid>
                                </Hidden>

                                {/* Web view */}
                                <Hidden only={['xs', 'sm']}>
                                    <Grid item xs={12}>
                                        <CancelOrderNumber
                                            orderId={order_id}
                                            language={language}
                                            global={global}
                                            orderIncrementId={orderIncrementId}
                                        />
                                    </Grid>

                                    <Grid item xs={8}>
                                        <CancelItems
                                            details={details}
                                            cancelOrderItem={cancelOrderItem}
                                            setCancelOrderItem={setCancelOrderItem}
                                            isValidate={isValidate}
                                            comment={comment}
                                            setComment={setComment}
                                        />
                                        {cancelOrderItem && cancelOrderItem.length > 0 && (
                                            <>
                                                <Grid item xs={12}>
                                                    <Typography className="login-sigup-input-text">
                                                        <FormattedMessage
                                                            id="MyAccount.ReturnOrder.Comment"
                                                            defaultMessage="Comment"
                                                        />
                                                    </Typography>
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        multiline
                                                        rows={3}
                                                        value={comment}
                                                        onChange={(e) => setComment(e.target.value)}
                                                        className="commentBox"
                                                    />
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <div className="green-line" />
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <Grid container justify="center">
                                                        {!cancelOrderLoader && (
                                                            <Button
                                                                className="submit-return-order-button"
                                                                onClick={() => submitCancelOrder()}
                                                            >
                                                                <FormattedMessage
                                                                    id="MyAccount.CancelOrder.CancelItems"
                                                                    defaultMessage="Cancel Items"
                                                                />
                                                            </Button>
                                                        )}
                                                        {cancelOrderLoader && <CircularProgress />}
                                                    </Grid>
                                                </Grid>
                                            </>
                                        )}
                                    </Grid>
                                    <Grid item xs={3}>
                                        <AddressAndPaymentOrderDetail details={details} />
                                        <OrderDetailsCharges details={details} />
                                    </Grid>
                                </Hidden>
                            </Grid>
                        </Grid>
                    )}

                    {loader && (
                        <Grid container alignItems="center">
                            <Spinner />
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </Suspense>
    );
}

export default withRouter(CancelOrder);
