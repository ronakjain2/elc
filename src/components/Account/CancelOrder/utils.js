export const IsValidateCancelOrderData = (cancelOrderItem) => {
    if (cancelOrderItem) {
        for (let i = 0; i < cancelOrderItem.length; i++) {
            if (cancelOrderItem[i].item_data) {
                for (let j = 0; j < cancelOrderItem[i].item_data.length; j++)
                    if (
                        !cancelOrderItem[i].item_data[j].reasonCode ||
                        cancelOrderItem[i].item_data[j].reasonCode === ''
                    ) {
                        return true;
                    }
            }
        }
        return false;
    }
    return true;
};

export const getGWPLineItemNo = (shpping) => {
    let no = '';
    if (shpping) {
        shpping &&
            Object.keys(shpping) &&
            Object.keys(shpping).map((key) => {
                const item = (shpping[key] && shpping[key].items_ordered) || [];
                for (let i = 0; i < item.length; i++) {
                    const price = item[i].price && parseInt(item[i].price);
                    if (price === 0) {
                        no = item[i].item_line_no;
                    }
                }

                return no;
            });
    }

    return no;
};
