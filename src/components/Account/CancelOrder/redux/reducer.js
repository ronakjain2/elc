import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_CANCEL_REASON_CODE_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_CANCEL_REASON_CODE_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.GET_CANCEL_REASON_CODE_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        reasonCodes: payload || [],
    }),

    [types.CANCEL_ORDER_REQUEST]: (state) => ({
        ...state,
        cancelOrderLoader: true,
    }),
    [types.CANCEL_ORDER_REQUEST_FAILED]: (state) => ({
        ...state,
        cancelOrderLoader: false,
    }),
    [types.CANCEL_ORDER_REQUEST_SUCCESS]: (state) => ({
        ...state,
        cancelOrderLoader: false,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    cancelOrderLoader: false,
    reasonCodes: [],
});
