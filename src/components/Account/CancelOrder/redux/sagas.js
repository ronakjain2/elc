import { call, put, takeLatest, select } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../../api';
import { toast } from 'react-toastify';
import { push } from 'connected-react-router';
import { createEncryptOrderId } from 'components/Cart/Checkout/utils';

const auth = (state) => state && state.auth;

const getCancelReasonCodeReq = function* getCancelReasonCodeReq({ payload }) {
    try {
        yield put(
            actions.getCancelReasonCodeRequestSuccess([
                {
                    label: 'Delivery promise was delayed',
                    value: 'OC1',
                },
                {
                    label: 'I changed my mind',
                    value: 'OC2',
                },
                {
                    label: 'Found similar product / lower price elsewhere',
                    value: 'OC3',
                },
                {
                    label: 'I am unavailable',
                    value: 'OC4',
                },
                {
                    label: 'Incorrect product or size ordered',
                    value: 'OC5',
                },
                {
                    label: 'Product no longer needed',
                    value: 'OC6',
                },
            ]),
        );
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getCancelReasonCodeRequestFailed());
    }
};

const cancelOrderReq = function* cancelOrderReq({ payload }) {
    try {
        const { cancelOrder, orderId, parent_order_id } = payload;
        const authData = yield select(auth);
        const { data } = yield call(api.cancelOrderRequest, cancelOrder);
        if (data && data.status) {
            yield put(
                push(
                    `/${authData && authData.store_locale}/order-details?orderId=${createEncryptOrderId(
                        orderId,
                    )}&order_increment_id=${createEncryptOrderId(parent_order_id)}`,
                ),
            );
            yield put(actions.cancelOrderRequestSuccess());
            toast.success(
                'The item(s) in your order have been cancelled successfully. We will update the order status in My Account Section very soon!',
            );
        } else {
            toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
            yield put(actions.cancelOrderRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.cancelOrderRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_CANCEL_REASON_CODE_REQUEST, getCancelReasonCodeReq);
    yield takeLatest(types.CANCEL_ORDER_REQUEST, cancelOrderReq);
}
