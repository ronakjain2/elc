import { createAction } from 'redux-actions';

//actions types
const GET_CANCEL_REASON_CODE_REQUEST = 'ELC@OROB/GET_CANCEL_REASON_CODE_REQUEST';
const GET_CANCEL_REASON_CODE_REQUEST_SUCCESS = 'ELC@OROB/GET_CANCEL_REASON_CODE_REQUEST_SUCCESS';
const GET_CANCEL_REASON_CODE_REQUEST_FAILED = 'ELC@OROB/GET_CANCEL_REASON_CODE_REQUEST_FAILED';

const CANCEL_ORDER_REQUEST = 'ELC@OROB/CANCEL_ORDER_REQUEST';
const CANCEL_ORDER_REQUEST_SUCCESS = 'ELC@OROB/CANCEL_ORDER_REQUEST_SUCCESS';
const CANCEL_ORDER_REQUEST_FAILED = 'ELC@OROB/CANCEL_ORDER_REQUEST_FAILED';

//actions methods
const getCancelReasonCodeRequest = createAction(GET_CANCEL_REASON_CODE_REQUEST);
const getCancelReasonCodeRequestSuccess = createAction(GET_CANCEL_REASON_CODE_REQUEST_SUCCESS);
const getCancelReasonCodeRequestFailed = createAction(GET_CANCEL_REASON_CODE_REQUEST_FAILED);

const cancelOrderRequest = createAction(CANCEL_ORDER_REQUEST);
const cancelOrderRequestSuccess = createAction(CANCEL_ORDER_REQUEST_SUCCESS);
const cancelOrderRequestFailed = createAction(CANCEL_ORDER_REQUEST_FAILED);

//actions
export const actions = {
    getCancelReasonCodeRequest,
    getCancelReasonCodeRequestSuccess,
    getCancelReasonCodeRequestFailed,

    cancelOrderRequest,
    cancelOrderRequestSuccess,
    cancelOrderRequestFailed,
};

//types
export const types = {
    GET_CANCEL_REASON_CODE_REQUEST,
    GET_CANCEL_REASON_CODE_REQUEST_SUCCESS,
    GET_CANCEL_REASON_CODE_REQUEST_FAILED,

    CANCEL_ORDER_REQUEST,
    CANCEL_ORDER_REQUEST_SUCCESS,
    CANCEL_ORDER_REQUEST_FAILED,
};
