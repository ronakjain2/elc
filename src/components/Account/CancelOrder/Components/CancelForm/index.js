import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';

import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

export default function CancelForm({ item, cancelOrderItem, setCancelOrderItem, isValidate }) {
    const state = useSelector((state) => state);

    const reasonCodes = (state && state.cancelOrder && state.cancelOrder.reasonCodes) || [];

    const index = cancelOrderItem.findIndex((i) => i.order_id === item.order_increment_id);
    const indexItem =
        cancelOrderItem[index] &&
        cancelOrderItem[index]['item_data'] &&
        cancelOrderItem[index]['item_data'].findIndex((x) => x.item_sku === item.sku);

    const onChangeReasonReturnForm = (name) => (event) => {
        let arr = [...cancelOrderItem] || [];
        let obj = arr[index] || {};
        obj['item_data'][indexItem][name] = event.target.value;
        arr[index] = obj;
        setCancelOrderItem(arr);
    };

    return (
        <Grid container>
            <Grid item xs={1}></Grid>
            <Grid item xs={11} md={6} className="mobileTopPadding">
                <Typography className="login-sigup-input-text">
                    <FormattedMessage id="MyAccount.CancelOrder.ReasonForCancel" defaultMessage="Reason For Cancel*" />
                </Typography>
                <TextField
                    variant="outlined"
                    fullWidth
                    select
                    value={cancelOrderItem && cancelOrderItem[index] && cancelOrderItem[index].reasonCode}
                    onChange={onChangeReasonReturnForm('reasonCode')}
                    className="inputBox reasonCode"
                    error={
                        isValidate &&
                        (!cancelOrderItem ||
                            !cancelOrderItem[index] ||
                            (cancelOrderItem[index]['item_data'] &&
                                cancelOrderItem[index]['item_data'][indexItem] &&
                                cancelOrderItem[index]['item_data'][indexItem].reasonCode === ''))
                    }
                >
                    {reasonCodes &&
                        reasonCodes.map((row, index) => (
                            <MenuItem key={`return_order_reason_code_${index}`} value={row.value}>
                                {row.label}
                            </MenuItem>
                        ))}
                </TextField>
                {isValidate &&
                    (!cancelOrderItem ||
                        !cancelOrderItem[index] ||
                        (cancelOrderItem[index]['item_data'] &&
                            cancelOrderItem[index]['item_data'][indexItem] &&
                            cancelOrderItem[index]['item_data'][indexItem].reasonCode === '')) && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="MyAccount.ReturnOrder.ReasonCode.Required"
                                defaultMessage="Please select reason code"
                            />
                        </FormHelperText>
                    )}
            </Grid>

            {/* <Grid item xs={12}>
                <div className="border-line" />
            </Grid> */}
        </Grid>
    );
}
