import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { FormattedMessage } from 'react-intl';
import { getCartAmount } from 'components/Cart/Basket/utils';
import { getSelectedItemIndex } from 'components/Account/ReturnOrder/utils';
import { orderStatus } from 'services/OrderStatus';
import { returnOrderClassName } from 'components/Account/MyOrder/utils';
import clsx from 'clsx';

const CancelForm = lazy(() => import('../../CancelForm'));

export default function CancelOrderItemMobileView({ item, cancelOrderItem, setCancelOrderItem, isValidate }) {
    const onClickCheckboxButton = (item) => {
        let arr = [...cancelOrderItem];
        const index = arr.findIndex((i) => i.order_id === item.order_increment_id);
        if (index === -1) {
            arr.push({
                order_id: item.order_increment_id,
                extRefNo: item.order_increment_id,
                item_data: [
                    {
                        item_line_no: item.item_line_no,
                        item_sku: item.sku,
                        qty: item.qty_ordered,
                        reasonCode: '',
                    },
                ],
            });
        } else {
            const indexIndex = arr[index] && arr[index].item_data.findIndex((i) => i.item_sku === item.sku);

            if (indexIndex !== -1) {
                arr[index] && arr[index].item_data.splice(indexIndex, 1);
            } else {
                arr[index].item_data.push({
                    item_line_no: item.item_line_no,
                    item_sku: item.sku,
                    qty: item.qty_ordered,
                    reasonCode: '',
                });
            }
            if (!arr[index].item_data || arr[index].item_data.length === 0) {
                arr.splice(index, 1);
            }
        }
        setCancelOrderItem(arr);
    };

    return (
        <Grid container className="padd-bottom-20 position-relative">
            <Grid item xs={1} className="flex alignItemCenter">
                {item.price !== 0 &&
                    !item.fc_membership_sku &&
                    item.status.toLowerCase().replace(/ /g, '') ===
                        orderStatus.Accepted.toLowerCase().replace(/ /g, '') && (
                        <Checkbox
                            checked={getSelectedItemIndex(cancelOrderItem, item) === -1 ? false : true}
                            onChange={() => onClickCheckboxButton(item)}
                            inputProps={{ 'aria-label': 'checkbox with small size' }}
                        />
                    )}
            </Grid>

            <Grid item xs={5} className={getSelectedItemIndex(cancelOrderItem, item) === -1 ? 'opacity-class' : ''}>
                <img src={item && item.image && item.image[0]} alt="productImage" className="web-order-detail-image" />
            </Grid>

            <Grid item xs={6} className={getSelectedItemIndex(cancelOrderItem, item) === -1 ? 'opacity-class' : ''}>
                <Typography className="order-details-product-name textAlignRight">{item && item.name}</Typography>

                <Typography className="qty textAlignRight">
                    <FormattedMessage id="Cart.Quantity" defaultMessage="Quantity" />
                    :&nbsp;{item && item.qty_ordered}
                </Typography>
                {item.fc_membership_sku === 1 && (
                    <Typography className="qty textAlignRight cancel-message ">
                        <FormattedMessage
                            id="cancelOrder.CancelNotAvailable"
                            defaultMessage="This item is not cancellable"
                        />
                    </Typography>
                )}
                <div className="textAlignRight">{getCartAmount(item)}</div>
                <Typography
                    className={clsx(
                        'order-details-status',
                        returnOrderClassName(item && item.status),
                        'textAlignRight',
                    )}
                >
                    <FormattedMessage id="MyAccount.Status" defaultMessage="Status" />: &nbsp;{item && item.status}
                </Typography>
            </Grid>

            {getSelectedItemIndex(cancelOrderItem, item) !== -1 && (
                <Grid item xs={12} className="return-form-container">
                    <Suspense fallback={`Loading....`}>
                        <CancelForm
                            item={item}
                            cancelOrderItem={cancelOrderItem}
                            setCancelOrderItem={setCancelOrderItem}
                            isValidate={isValidate}
                        />
                    </Suspense>
                </Grid>
            )}
        </Grid>
    );
}
