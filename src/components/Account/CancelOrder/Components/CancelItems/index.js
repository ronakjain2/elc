import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import Spinner from 'commonComponet/Spinner';
import Hidden from '@material-ui/core/Hidden';

const CancelOrderItemMobileView = lazy(() => import(`./Component/MobileCancelItem`));
const CancelOrderItemWebView = lazy(() => import('./Component/WebCancelItem'));

export default function CancelItems({ details, cancelOrderItem, setCancelOrderItem, isValidate, comment, setComment }) {
    const items = (details && details.sub_order_details) || {};

    return (
        <Suspense fallback={<Spinner />}>
            {items &&
                Object.keys(items) &&
                Object.keys(items).map((key, index) => (
                    <Grid
                        container
                        justify="center"
                        style={{ marginBottom: '2rem' }}
                        className="boxShadow1"
                        spacing={3}
                        key={`cancel_order_card${index}`}
                    >
                        {/* <Typography className="sub-title-return-order">
                    <FormattedMessage
                        id="MyAccount.ReturnOrder.PleaseSelectTheItemsYouWantToCancel"
                        defaultMessage="Please select the items you want to cancel"
                    />
                </Typography> */}
                        <Grid item xs={12} className="header-of-item">
                            <div className="header-flex">
                                <div>
                                    <Typography className="order-details-header-name">
                                        Shipment {index + 1} Of {Object.keys(items).length}
                                    </Typography>
                                    <Typography className="order-details-product-specification">
                                        <FormattedMessage id="MyAccount.OrderID" defaultMessage="Order Id" />: &nbsp;
                                        {items[key].increment_id}
                                    </Typography>
                                </div>
                                <div className={'textAlignRight'}>
                                    <Typography className="order-details-header-name">
                                        <FormattedMessage
                                            id="Checkout.Payment.DeliveryOption"
                                            defaultMessage="Order Items"
                                        />
                                    </Typography>
                                    <Typography className="order-details-product-specification">
                                        {items[key].delivery_option}
                                    </Typography>
                                </div>
                            </div>
                            {/* <Typography className="order-items-title">
                    <FormattedMessage id="MyAccount.MyOrderDetails.OrderItems" defaultMessage="Order Items" />
                </Typography> */}
                        </Grid>
                        <Hidden only={['xs', 'sm']}>
                            <Grid item xs={12}>
                                <Grid container className="order-list-grid">
                                    <Grid item xs={1}></Grid>
                                    <Grid item xs={6}>
                                        <Typography className="order-item-header-name">
                                            <FormattedMessage id="Cart.ItemNumberStyle" defaultMessage="ITEM" />
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1}>
                                        <Typography className="order-item-header-name textAlignCenter">
                                            <FormattedMessage id="Cart.Quantity" defaultMessage="QUANTITY" />
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={2} className="textAlignCenter-c">
                                        <Typography className="order-item-header-name textAlignCenter">
                                            <FormattedMessage id="Cart.Price" defaultMessage="PRICE" />
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={2}>
                                        <Typography className="order-item-header-name textAlignCenter">
                                            <FormattedMessage id="MyAccount.Status" defaultMessage="STATUS" />
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid item xs={12}>
                                {items[key].items_ordered &&
                                    items[key].items_ordered.map((item, index) => (
                                        <Grid container key={`cancel-order-details-item_web_${index}`}>
                                            <CancelOrderItemWebView
                                                item={item}
                                                cancelOrderItem={cancelOrderItem}
                                                setCancelOrderItem={setCancelOrderItem}
                                                isValidate={isValidate}
                                                comment={comment}
                                                setComment={setComment}
                                            />
                                        </Grid>
                                    ))}
                            </Grid>
                        </Hidden>

                        <Hidden only={['md', 'lg', 'xl']}>
                            <Grid item xs={12}>
                                {items[key].items_ordered &&
                                    items[key].items_ordered.map((item, index) => (
                                        <Grid container key={`return-order-details-item_mobile_${index}`}>
                                            <CancelOrderItemMobileView
                                                item={item}
                                                cancelOrderItem={cancelOrderItem}
                                                setCancelOrderItem={setCancelOrderItem}
                                                isValidate={isValidate}
                                            />
                                        </Grid>
                                    ))}
                            </Grid>
                        </Hidden>
                    </Grid>
                ))}
        </Suspense>
    );
}
