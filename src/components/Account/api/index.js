import api from 'apiServices';

//My Address
const getAllAddress = (payload) => api.post(`/index.php/rest/V1/app/addressbook`, payload);
const saveAddress = (payload) => api.post(`/index.php/rest/V1/app/addaddress`, payload);
const deleteAddress = (payload) => api.post(`/index.php/rest/V1/app/deleteaddress`, payload);

//My Order
const getOrderHistory = (payload) => api.post(`/index.php/rest/V1/app/mkt/orderhistory`, payload);
const getOrderDetails = (payload) => api.post(`/index.php/rest/V1/app/mkt/orderview`, payload);
const getResonCode = (payload) => api.post('/index.php/rest/V1/app/orob/reasoncode', payload);

//Return Order
const returnOrderRequest = (payload) => api.post(`/index.php/rest/V1/app/mkt/return`, payload);

//Cancel Order
const cancelOrderRequest = (payload) => api.post(`/index.php/rest/V1/app/mkt/cancel`, payload);

//My Profile
const changePassword = (payload) => api.post(`/index.php/rest/V1/app/changepassword`, payload);

//My WishList
const wishlistItems = (payload) => api.post(`/index.php/rest/V1/app/orob/wishlistitems`, payload);
const removeWishlistItem = (payload) => api.post(`/index.php/rest/V1/app/removewishlistitem`, payload);
const addWishlistItem = (payload) => api.post(`/index.php/rest/V1/app/addtowishlist`, payload);

//My Club
const addChild = (payload) => api.post(`/index.php/rest/V1/app/fc/addfcmchild`, payload);
const getMembershipDetail = (payload) => api.post(`/index.php/rest/V1/app/fc/getfcmchild`, payload);

export default {
    getAllAddress,
    saveAddress,
    deleteAddress,

    getOrderHistory,
    getOrderDetails,
    getResonCode,

    returnOrderRequest,
    cancelOrderRequest,

    changePassword,

    wishlistItems,
    removeWishlistItem,
    addWishlistItem,

    addChild,
    getMembershipDetail,
};
