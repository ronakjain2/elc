import { toast } from 'react-toastify';
import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../api';
import { actions, types } from './actions';

const getAllAccountAddressesReq = function* getAllAccountAddressesReq({ payload }) {
    try {
        const { data } = yield call(api.getAllAddress, payload);
        if (data && data.status) {
            yield put(
                actions.getAllAccountAddressRequestSuccess({
                    addresses: data && data.addressData,
                    carrier_code: data.carrier_code,
                }),
            );
        } else {
            toast.error((data && data.message) || 'Something Went to wrong, Please try after sometime');
            yield put(actions.getAllAccountAddressRequestFailed());
        }
    } catch (err) {
        console.log(`err`, err);
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getAllAccountAddressRequestFailed());
    }
};

const saveOrEditAccountAddressReq = function* saveOrEditAccountAddressReq({ payload }) {
    try {
        const { UserId, store_id } = payload;
        const { data } = yield call(api.saveAddress, payload);
        yield put(actions.saveAccountAddressRequestSuccess());
        yield put(
            actions.getAllAccountAddressRequest({
                customerid: UserId,
                store_id,
            }),
        );
        if (data && data.status) {
            toast.success(data && data.message);
        } else {
            toast.error(data && data.message);
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.saveAccountAddressRequestFailed());
    }
};

const deleteAccountAddressReq = function* deleteAccountAddressReq({ payload }) {
    try {
        const { addressId, customerid, store_id } = payload;
        const { data } = yield call(api.deleteAddress, { addressId });
        yield put(actions.deleteAccountAddressRequestSuccess());
        yield put(
            actions.getAllAccountAddressRequest({
                customerid: customerid,
                store_id,
            }),
        );
        if (data && data.status) {
            toast.success(data && data.message);
        } else {
            toast.error(data && data.message);
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.deleteAccountAddressRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_ALL_ACCOUNT_ADDRESSES_REQUEST, getAllAccountAddressesReq);
    yield takeLatest(types.SAVE_ACCOUNT_ADDRESS_REQUEST, saveOrEditAccountAddressReq);
    yield takeLatest(types.DELETE_ACCOUNT_ADDRESS_REQUEST, deleteAccountAddressReq);
}
