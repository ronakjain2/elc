import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_ALL_ACCOUNT_ADDRESSES_REQUEST]: (state) => ({
        ...state,
        loader: true,
        addresses: [],
    }),
    [types.GET_ALL_ACCOUNT_ADDRESSES_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
        addresses: [],
    }),
    [types.GET_ALL_ACCOUNT_ADDRESSES_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        addresses: payload.addresses || [],
        carrier_code: payload.carrier_code,
    }),

    [types.SAVE_ACCOUNT_ADDRESS_REQUEST]: (state) => ({
        ...state,
        saveLoader: true,
    }),
    [types.SAVE_ACCOUNT_ADDRESS_REQUEST_FAILED]: (state) => ({
        ...state,
        saveLoader: false,
    }),
    [types.SAVE_ACCOUNT_ADDRESS_REQUEST_SUCCESS]: (state) => ({
        ...state,
        saveLoader: false,
    }),

    [types.DELETE_ACCOUNT_ADDRESS_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.DELETE_ACCOUNT_ADDRESS_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.DELETE_ACCOUNT_ADDRESS_REQUEST_SUCCESS]: (state) => ({
        ...state,
        loader: false,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    saveLoader: false,
    addresses: [],
});
