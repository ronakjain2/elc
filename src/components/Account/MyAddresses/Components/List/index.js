import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { useSelector } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';
import AddIcon from '@material-ui/icons/Add';
import { FormattedMessage } from 'react-intl';
import DeleteAddress from '../Delete';
import { replaceTilCharacterToComma } from 'components/Cart/Checkout/Delivery/utils';
import ErrorOutlinedIcon from '@material-ui/icons/ErrorOutlined';

export default function MyAddressesList({ onClickEdit, setIsAdd, isAdd }) {
    const state = useSelector((state) => state);

    const addresses = (state && state.myAddresses && state.myAddresses.addresses) || [];

    const [open, setOpen] = useState(false);
    const [addressId, setAddressId] = useState('');

    return (
        <Grid container>
            <DeleteAddress open={open} setOpen={setOpen} index={addressId} />
            <Grid item xs={12}>
                <Hidden only={['md', 'lg', 'xl']}>
                    <Grid container justify="flex-end">
                        {!isAdd && (
                            <Button
                                onClick={() => setIsAdd(true)}
                                className="mobile-add-new-address-button"
                                startIcon={<AddIcon />}
                            >
                                <FormattedMessage
                                    id="MyAccount-MyAddresses-AddNew-Address"
                                    defaultMessage="Add New Address"
                                />
                            </Button>
                        )}
                    </Grid>
                </Hidden>

                <Grid container>
                    {addresses &&
                        addresses.map((address, index) => (
                            <Grid
                                item
                                xs={12}
                                md={4}
                                className="my-address-card-padding"
                                key={`my-address-card-${index}`}
                            >
                                <Grid container className="my-address-card">
                                    <Grid item xs={12} className="arabic-right">
                                        <Grid container justify="space-between" alignItems="center">
                                            <div>
                                                <Typography className="my-address-nick-name">
                                                    {address.userFirstName &&
                                                        `${address.userFirstName} ${address.userLastName}`}
                                                </Typography>
                                                <Typography className="my-address-type">
                                                    {address.address_type}
                                                </Typography>
                                            </div>
                                            <div>
                                                {address && address.editable && (
                                                    <IconButton
                                                        className="edit-button"
                                                        size="small"
                                                        onClick={() => onClickEdit(address)}
                                                    >
                                                        <EditIcon fontSize="small" />
                                                    </IconButton>
                                                )}
                                                <IconButton
                                                    className="delete-button"
                                                    size="small"
                                                    onClick={() => {
                                                        setOpen(true);
                                                        setAddressId(address.Id);
                                                    }}
                                                >
                                                    <DeleteIcon fontSize="small" />
                                                </IconButton>
                                            </div>
                                        </Grid>
                                    </Grid>

                                    <Grid item xs={12} className="arabic-right p-top-10">
                                        <Typography className="my-address-line">
                                            {address.street && replaceTilCharacterToComma(address.street)}
                                        </Typography>
                                        <Typography className="my-address-line">{address.city}</Typography>
                                        <Typography className="my-address-line dir-ltr">
                                            {address.carrier_code && `+${address.carrier_code} ${address.telephone}`}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} className="arabic-right p-top-10 address-messages">
                                        {address && address.isdefaultShipping && address.is_valid && (
                                            <Button
                                                disabled={true}
                                                className="default-address-check"
                                                startIcon={<CheckCircleRoundedIcon />}
                                            >
                                                <FormattedMessage
                                                    id="MyAccount.MyAddress.DefaultAddress"
                                                    defaultMessage="Default address"
                                                />
                                            </Button>
                                        )}

                                        {address && !address.is_valid && address.error_msg && (
                                            <Button
                                                disabled={true}
                                                className="address-error text-align-start"
                                                startIcon={<ErrorOutlinedIcon />}
                                            >
                                                {address.error_msg}
                                            </Button>
                                        )}
                                    </Grid>
                                </Grid>
                            </Grid>
                        ))}
                </Grid>
            </Grid>
        </Grid>
    );
}
