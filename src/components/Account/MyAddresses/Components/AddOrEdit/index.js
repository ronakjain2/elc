import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import { FormattedMessage } from 'react-intl';
import PhoneNumber from 'commonComponet/PhoneNumber';
import Button from '@material-ui/core/Button';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import { useSelector } from 'react-redux';
import { getCityObject } from 'components/Cart/Checkout/Delivery/utils';
import CircularProgress from '@material-ui/core/CircularProgress';
import { isValidateSpecialCharacters } from 'components/Cart/Checkout/utils';

export default function AddOrEditMyAddress({
    address,
    setAddress,
    isValidate,
    saveOrUpdateAddress,
    setIsCorrectPhone,
    isCorrectPhone,
    isAdd,
    isEdit,
    setIsAdd,
    setIsEdit,
    addresses,
}) {
    const state = useSelector((state) => state);

    let cityList = state && state.checkoutDelivery && state.checkoutDelivery.countryList;
    const language = state && state.auth && state.auth.language;
    const saveLoader = state && state.myAddresses && state.myAddresses.saveLoader;
    const country = state && state.auth && state.auth.country;

    const setCityDetails = (city) => {
        if (city && city.country) {
            setAddress({
                ...address,
                regionId: city.id,
                city: city.name,
                country: city.country,
            });
        } else {
            setAddress({
                ...address,
                regionId: city.id,
                city: city.name,
            });
        }
    };

    if (address.country === 'AE') {
        cityList = cityList.filter((city) => city.id === 'AE');
    } else if (address.country === 'SA') {
        cityList = cityList.filter((city) => city.id === 'SA');
    } else {
        if (country === 'KSA') {
            cityList = cityList.filter((city) => city.id === 'SA');
            setCityDetails({ name: '', id: '', country: 'SA' });
        } else if (country === 'UAE') {
            cityList = cityList.filter((city) => city.id === 'AE');
            setCityDetails({ name: '', id: '', country: 'AE' });
        } else cityList = [];
    }

    //City name unique
    cityList = cityList.filter((item) => item.hasOwnProperty('available_regions'));
    //end city name unique

    const parentPhoneNumberChangeEvent = (phoneNo) => {
        const Obj = {
            ...address,
            phoneNumber: phoneNo.phone,
            countryCode: phoneNo.dialCode,
        };
        setAddress(Obj);
        setIsCorrectPhone(phoneNo.phoneValid);
    };

    const onChangeHandler = (name) => (event) => {
        if (name === 'apartmentNo' || name === 'address' || name === 'areaAndLocality') {
            if (event.target.value.indexOf('~') !== -1) {
                return;
            }
        }
        if (name === 'apartmentNo' || name === 'address' || name === 'areaAndLocality') {
            if (event.target.value === ' ') {
                return;
            }
        }
        if (
            name === 'firstName' ||
            name === 'lastName' ||
            name === 'apartmentNo' ||
            name === 'address' ||
            name === 'areaAndLocality'
        ) {
            if (isValidateSpecialCharacters(event.target.value)) {
                return;
            }
        }

        if (name === 'firstName' || name === 'lastName') {
            if (event.target.value === ' ') {
                return;
            }
            const re = /^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g;
            if (event.target.value && !re.test(event.target.value)) {
                return '';
            }
        }

        if (name === 'country') {
            setCityDetails({ name: '', id: '', country: event.target.value });
        } else {
            setAddress({ ...address, [name]: event.target.value });
        }
    };

    const handleChangeCheckbox = (name) => (event) => {
        setAddress({ ...address, [name]: event.target.checked });
    };

    const onClickCancle = () => {
        setIsAdd(false);
        setIsEdit(false);
    };

    return (
        <Grid container>
            <Grid item xs={12} md={9} lg={7}>
                <Grid container justify="space-between">
                    {isAdd && (
                        <Typography className="address-title">
                            <FormattedMessage
                                id="MyAccount-MyAddresses-AddNew-Address"
                                defaultMessage="Add New Address"
                            />
                        </Typography>
                    )}

                    {isEdit && (
                        <Typography className="address-title">
                            <FormattedMessage id="MyAccount-MyAddresses-Edit-Address" defaultMessage="Edit Address" />
                        </Typography>
                    )}

                    {/** Address Type */}
                    <Grid item xs={12} className="padding-t-input-box">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="Checkout.Delivery.AddressType" defaultMessage="Address Type*" />
                        </Typography>
                        <RadioGroup
                            row
                            aria-label="position"
                            name="position"
                            value={address.type}
                            onChange={onChangeHandler('type')}
                        >
                            <FormControlLabel
                                value="home"
                                control={<Radio />}
                                className={address.type === 'home' ? 'active-radio margin-right' : 'margin-right'}
                                label={<FormattedMessage id="Checkout.Delivery.Home" defaultMessage="Home" />}
                                labelPlacement="end"
                            />
                            <FormControlLabel
                                value="work"
                                control={<Radio />}
                                className={address.type === 'work' ? 'active-radio' : ''}
                                label={<FormattedMessage id="Checkout.Delivery.Work" defaultMessage="Work" />}
                                labelPlacement="end"
                            />
                        </RadioGroup>
                    </Grid>

                    {/** Country */}
                    <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="Checkout.Delivery.Country" defaultMessage="Country*" />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={
                                address.country
                                    ? address.country
                                    : country === 'KSA'
                                    ? 'SA'
                                    : country === 'UAE'
                                    ? 'AE'
                                    : ''
                            }
                            select
                            fullWidth
                            placeholder={language === 'en' ? 'Please Select Country' : 'حدد الدولة'}
                            className="inputBox"
                            onChange={onChangeHandler('country')}
                        >
                            {country === 'KSA' ? (
                                <MenuItem value="SA">Saudi Arabia</MenuItem>
                            ) : (
                                <MenuItem value="AE">United Arab Emirates</MenuItem>
                            )}
                        </TextField>
                        {isValidate && address.country === '' && (
                            <FormHelperText className="input-error">
                                <FormattedMessage
                                    id="MyAccount.MyAddress.Country.Required"
                                    defaultMessage="Please Select Country"
                                />
                            </FormHelperText>
                        )}
                    </Grid>

                    {/** City */}
                    <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="Checkout.Delivery.City" defaultMessage="City*" />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={address.city}
                            select
                            fullWidth
                            placeholder={language === 'en' ? 'Please Select City' : 'اختر مدينة'}
                            className="inputBox"
                            onChange={(e) => {
                                setCityDetails(
                                    getCityObject(
                                        e.target.value,
                                        (cityList && cityList[0] && cityList[0].available_regions) || [],
                                    ),
                                );
                            }}
                        >
                            {(!cityList || cityList.length === 0) && <MenuItem></MenuItem>}
                            {cityList &&
                                cityList[0] &&
                                cityList[0].available_regions &&
                                cityList[0].available_regions.map((city, index) => (
                                    <MenuItem key={`checkout_city_list_${index}_${city.id}`} value={city.name}>
                                        {city && city.name}
                                    </MenuItem>
                                ))}
                        </TextField>
                        {isValidate && address.city === '' && (
                            <FormHelperText className="input-error">
                                <FormattedMessage
                                    id="Checkout.Delivery.PleaseSelectStateCity"
                                    defaultMessage="Please Select State/City"
                                />
                            </FormHelperText>
                        )}
                    </Grid>

                    {/** First name */}
                    <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="SignInSignUp.FirstName" defaultMessage="First Name*" />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={address.firstName}
                            onChange={onChangeHandler('firstName')}
                            fullWidth
                            className="inputBox"
                            error={isValidate && address.firstName === ''}
                            inputProps={{ maxLength: 20 }}
                        />
                        {isValidate && address.firstName === '' && (
                            <FormHelperText className="input-error">
                                <FormattedMessage
                                    id="Common.FirstName.Error"
                                    defaultMessage="Please enter first name"
                                />
                            </FormHelperText>
                        )}
                    </Grid>

                    {/** Last Name */}
                    <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="SignInSignUp.LastName" defaultMessage="Last Name*" />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={address.lastName}
                            onChange={onChangeHandler('lastName')}
                            fullWidth
                            className="inputBox"
                            error={isValidate && address.lastName === ''}
                            inputProps={{ maxLength: 19 }}
                        />
                        {isValidate && address.lastName === '' && (
                            <FormHelperText className="input-error">
                                <FormattedMessage id="Common.LastName.Error" defaultMessage="Please enter last name" />
                            </FormHelperText>
                        )}
                    </Grid>

                    {/** Phone Number */}
                    <Grid item xs={12} md={6} className="padding-t-input-box padding-l-r">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="SignInSignUp.ContactNumber" defaultMessage="Contact Number*" />
                        </Typography>

                        <PhoneNumber
                            parentPhoneNumberChangeEvent={parentPhoneNumberChangeEvent}
                            carrier_code={address.countryCode}
                            phoneNo={address.phoneNumber}
                        />

                        {isValidate && address.phoneNumber === '' && (
                            <FormHelperText className="input-error">
                                <FormattedMessage
                                    id="Common.PhoneNumber.Empty"
                                    defaultMessage="Please enter contact number"
                                />
                            </FormHelperText>
                        )}

                        {isValidate && address.phoneNumber !== '' && !isCorrectPhone && (
                            <FormHelperText className="input-error">
                                <FormattedMessage
                                    id="Common.PhoneNumber.InValid"
                                    defaultMessage="Please enter contact number"
                                />
                            </FormHelperText>
                        )}
                    </Grid>

                    {/** Apartment / Unit Number */}
                    <Grid item xs={12} className="padding-t-input-box">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage
                                id="Checkout.Delivery.ApartmentUnitNumber"
                                defaultMessage="Apartment / Unit Number*"
                            />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={address.apartmentNo}
                            onChange={onChangeHandler('apartmentNo')}
                            fullWidth
                            inputProps={{
                                maxLength: 50,
                            }}
                            className="inputBox"
                            error={isValidate && address.apartmentNo === ''}
                        />
                        {isValidate && address.apartmentNo === '' && (
                            <FormHelperText className="input-error">
                                <FormattedMessage
                                    id="Checkout.Delivery.Error.ApartmentNo"
                                    defaultMessage="Please enter Apartment / Unit Number"
                                />
                            </FormHelperText>
                        )}
                    </Grid>
                    {/** Address */}
                    <Grid item xs={12} className="padding-t-input-box">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="MyAddresses.Address.text" defaultMessage="Address" />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={address.address}
                            onChange={onChangeHandler('address')}
                            fullWidth
                            inputProps={{
                                maxLength: 50,
                            }}
                            className="inputBox"
                        />
                    </Grid>
                    {/** Area / Locality */}
                    <Grid item xs={12} className="padding-t-input-box">
                        <Typography className="login-sigup-input-text">
                            <FormattedMessage id="MyAddresses.AreaLocality.text" defaultMessage="Area / Locality" />
                        </Typography>
                        <TextField
                            variant="outlined"
                            value={address.areaAndLocality}
                            onChange={onChangeHandler('areaAndLocality')}
                            fullWidth
                            inputProps={{
                                maxLength: 50,
                            }}
                            className="inputBox"
                        />
                    </Grid>

                    <Grid item xs={12} style={{ paddingTop: 10 }}>
                        {/** save address fecture */}
                        <FormGroup row>
                            <FormControlLabel
                                className="margin-right"
                                control={
                                    <Checkbox
                                        checked={address.defaultAddress}
                                        onChange={handleChangeCheckbox('defaultAddress')}
                                    />
                                }
                                label={
                                    <Typography className="checkbox-style">
                                        <FormattedMessage
                                            id="Checkout.Delivery.MakeThisAddressDefault"
                                            defaultMessage="Make this Address Default"
                                        />
                                    </Typography>
                                }
                            />
                        </FormGroup>
                    </Grid>

                    <Grid container>
                        {addresses && addresses.length > 0 && (
                            <Button className="cancel-address-button" onClick={() => onClickCancle()}>
                                <FormattedMessage id="Checkout.Cart.Delivery.Cancel" defaultMessage="Cancel" />
                            </Button>
                        )}
                        {!saveLoader && (
                            <Button className="save-or-edit-address-button" onClick={() => saveOrUpdateAddress()}>
                                <FormattedMessage id="MyAccount.MyAddress.Save" defaultMessage="save" />
                            </Button>
                        )}
                        {saveLoader && <CircularProgress />}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
