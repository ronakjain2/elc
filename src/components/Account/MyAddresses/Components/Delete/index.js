import React from 'react';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Typography from '@material-ui/core/Typography';
import DialogActions from '@material-ui/core/DialogActions';
import { FormattedMessage } from 'react-intl';
import Button from '@material-ui/core/Button';
import { actions as myAddressesActions } from '../../redux/actions';
import { useDispatch, useSelector } from 'react-redux';

export default function DeleteAddress({ index, open, setOpen }) {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    const customerid = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_id;
    const store_id = state && state.auth && state.auth.currentStore;

    const handleClose = () => {
        setOpen(false);
    };

    const deleteAddress = () => {
        dispatch(
            myAddressesActions.deleteAccountAddressRequest({
                addressId: index,
                customerid: customerid,
                store_id,
            }),
        );
        setOpen(false);
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogContent>
                        <DialogContentText>
                            <Typography>
                                <FormattedMessage
                                    id="MyAccount.MyWishlist.AreYouSureYouWantToDeleteThisAddress"
                                    defaultMessage="Are you sure you want to delete this address?"
                                />
                            </Typography>
                        </DialogContentText>
                    </DialogContent>

                    <DialogActions>
                        <Button color="primary" onClick={() => handleClose()}>
                            <FormattedMessage id="Checkout.Cart.Delivery.Cancel" defaultMessage="Cancel" />
                        </Button>
                        <Button color="primary" autoFocus onClick={() => deleteAddress()}>
                            <FormattedMessage id="MyAccount.MyWishlist.Ok" defaultMessage="Ok" />
                        </Button>
                    </DialogActions>
                </Dialog>
            </Grid>
        </Grid>
    );
}
