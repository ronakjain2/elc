export const checkAddNewAddressObject = (address) => {
    if (
        !address ||
        !address.firstName ||
        address.firstName === '' ||
        !address.lastName ||
        address.lastName === '' ||
        !address.phoneNumber ||
        address.phoneNumber === '' ||
        !address.countryCode ||
        address.countryCode === '' ||
        !address.apartmentNo ||
        address.apartmentNo === '' ||
        !address.city ||
        address.city === '' ||
        !address.country ||
        address.country === ''
    ) {
        return true;
    }

    return false;
};

export const setEditObject = (obj) => {
    let address = (obj && obj.street && obj.street.split('~')) || [];
    return {
        type: obj.address_type,
        firstName: obj.userFirstName,
        lastName: obj.userLastName,
        phoneNumber: obj.telephone,
        countryCode: obj.carrier_code,
        defaultAddress: obj.isdefaultShipping,
        apartmentNo: (address && address.length >= 1 && address[0]) || '',
        address: (address && address.length >= 2 && address[1]) || '',
        areaAndLocality: (address && address.length >= 3 && address[2]) || '',
        addressId: obj.Id,
        regionId: obj.region_id,
        city: obj.city,
        country: obj.country_id,
    };
};

export const setAddObject = () => {
    return {
        type: 'home',
        firstName: '',
        lastName: '',
        phoneNumber: '',
        countryCode: '',
        defaultAddress: '',
        apartmentNo: '',
        address: '',
        areaAndLocality: '',
        addressId: '',
        regionId: '',
        city: '',
        country: '',
    };
};

export const saveApiObj = (obj, customerid, store_id) => {
    return {
        AddressType: obj.type,
        DefaultBilling: obj.defaultAddress ? 1 : 0,
        DefaultShipping: obj.defaultAddress ? 1 : 0,
        UserCity: obj.city,
        UserCountry: obj.country,
        UserFirstName: obj.firstName,
        UserId: customerid,
        UserLastName: obj.lastName,
        UserRegionId: obj.regionId,
        UserStreet: `${obj.apartmentNo}~ ${obj.address}~ ${obj.areaAndLocality}`,
        UserTelephone: obj.phoneNumber,
        WebsiteId: 1,
        addressId: obj.addressId,
        carrier_code: obj.countryCode,
        countryCode: obj.country,
        postcode: '',
        store_id,
    };
};

export const scrollToElement = (element) => {
    const elmnt = document.querySelector(element);
    elmnt && elmnt.scrollIntoView({ block: 'center', behavior: 'smooth' });
    return;
};
