import React, { Suspense, lazy, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Spinner from 'commonComponet/Spinner';
import Hidden from '@material-ui/core/Hidden';
import './MyAddresses.css';
import { useSelector, useDispatch } from 'react-redux';
import { actions as myAddressesActions } from './redux/actions';
import { actions as checkoutDeliveryActions } from 'components/Cart/Checkout/Delivery/redux/actions';
import { checkAddNewAddressObject, setEditObject, setAddObject, saveApiObj, scrollToElement } from './utils';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const MyAddressesList = lazy(() => import('./Components/List'));
const AddOrEditMyAddress = lazy(() => import('./Components/AddOrEdit'));
const MobilePageHeader = lazy(() => import('../Components/MobilePageHeader'));

export default function MyAddresses({ location, history }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const customerid = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_id;
    const addresses = (state && state.myAddresses && state.myAddresses.addresses) || [];
    const saveLoader = state && state.myAddresses && state.myAddresses.saveLoader;
    const loader = state && state.myAddresses && state.myAddresses.loader;
    const store_id = state && state.auth && state.auth.currentStore;

    const [isCorrectPhone, setIsCorrectPhone] = useState(false);
    const [isAdd, setIsAdd] = useState(false);
    const [isEdit, setIsEdit] = useState(false);

    const [address, setAddress] = useState({
        type: 'home',
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        countryCode: '',
        defaultAddress: false,
        apartmentNo: '',
        address: '',
        areaAndLocality: '',
        addressId: '',
        regionId: '',
        city: '',
        country: '',
    });
    const [isValidate, setIsValidate] = useState(false);

    useEffect(() => {
        dispatch(
            myAddressesActions.getAllAccountAddressRequest({
                customerid: customerid,
                store_id,
            }),
        );
    }, [dispatch, customerid, store_id]);

    const saveOrUpdateAddress = () => {
        setIsValidate(true);
        if (checkAddNewAddressObject(address) || !isCorrectPhone) {
            scrollToElement('.input-error');
            return;
        }
        setIsValidate(false);
        dispatch(myAddressesActions.saveAccountAddressRequest(saveApiObj(address, customerid, store_id)));
    };

    //get country list
    useEffect(() => {
        dispatch(checkoutDeliveryActions.getCountryListRequest());
    }, [dispatch]);

    useEffect(() => {
        if (!saveLoader) {
            setIsAdd(false);
            setIsEdit(false);
            setAddress(setAddObject());
            let addressRow = location && location.state && location.state.addressRow;
            if (addressRow) {
                setIsEdit(true);
                setAddress(setEditObject(addressRow));
                location.state = null;
            }
        }
    }, [saveLoader, location]);

    const onClickEdit = (row) => {
        setIsEdit(true);
        setIsAdd(false);
        setAddress(setEditObject(row));
    };

    const onClickAdd = () => {
        setIsValidate(false);
        setIsEdit(false);
        setIsAdd(true);
    };

    return (
        <Suspense fallback={<Spinner />}>
            {!loader && (
                <Grid container justify="center" className="my-addresses-container">
                    <Hidden only={['xs', 'sm']}>
                        <Grid item xs={11}>
                            <Grid container>
                                {/** Tabs */}
                                <Grid item xs={12}>
                                    <AccountTab activeKey="MyAddresses" />
                                </Grid>

                                {/** List of Address */}
                                {!isEdit && (
                                    <Grid item xs={12}>
                                        <MyAddressesList onClickEdit={onClickEdit} />
                                    </Grid>
                                )}

                                {addresses && addresses.length !== 0 && !isEdit && (
                                    <>
                                        {/** Green Line */}
                                        <Grid item xs={12}>
                                            <div className="green-line" />
                                        </Grid>
                                        {!isAdd && (
                                            <>
                                                <Grid item xs={12} className="arabic-right">
                                                    <Typography className="address-title">
                                                        <FormattedMessage
                                                            id="MyAccount-MyAddresses-AddNew-Address"
                                                            defaultMessage="Add New Address"
                                                        />
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={12} className="arabic-right">
                                                    <Button
                                                        onClick={onClickAdd}
                                                        startIcon={<AddIcon />}
                                                        className="save-or-edit-address-button"
                                                    >
                                                        <FormattedMessage
                                                            id="MyAccount.MyAddresses.AddAddress"
                                                            defaultMessage="Add Address"
                                                        />
                                                    </Button>
                                                </Grid>
                                            </>
                                        )}
                                    </>
                                )}

                                {((addresses && addresses.length === 0) || isAdd || isEdit) && (
                                    <Grid item xs={12}>
                                        <AddOrEditMyAddress
                                            address={address}
                                            setAddress={setAddress}
                                            isValidate={isValidate}
                                            saveOrUpdateAddress={saveOrUpdateAddress}
                                            isCorrectPhone={isCorrectPhone}
                                            setIsCorrectPhone={setIsCorrectPhone}
                                            isAdd={isAdd}
                                            isEdit={isEdit}
                                            setIsAdd={setIsAdd}
                                            setIsEdit={setIsEdit}
                                            addresses={addresses}
                                        />
                                    </Grid>
                                )}
                            </Grid>
                        </Grid>
                    </Hidden>

                    <Hidden only={['md', 'lg', 'xl']}>
                        <Grid item xs={11}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <MobilePageHeader
                                        title={
                                            <FormattedMessage
                                                id="MyAccount.MyAddresses"
                                                defaultMessage="My Addresses"
                                            />
                                        }
                                    />
                                </Grid>

                                {/** List of Address */}
                                {!isEdit && !isAdd && (
                                    <Grid item xs={12}>
                                        <MyAddressesList onClickEdit={onClickEdit} setIsAdd={setIsAdd} isAdd={isAdd} />
                                    </Grid>
                                )}

                                {((addresses && addresses.length === 0) || isAdd || isEdit) && (
                                    <Grid item xs={12}>
                                        <AddOrEditMyAddress
                                            address={address}
                                            setAddress={setAddress}
                                            isValidate={isValidate}
                                            saveOrUpdateAddress={saveOrUpdateAddress}
                                            isCorrectPhone={isCorrectPhone}
                                            setIsCorrectPhone={setIsCorrectPhone}
                                            isAdd={isAdd}
                                            isEdit={isEdit}
                                            setIsAdd={setIsAdd}
                                            setIsEdit={setIsEdit}
                                        />
                                    </Grid>
                                )}
                            </Grid>
                        </Grid>
                    </Hidden>
                </Grid>
            )}

            {loader && (
                <Grid container alignItems="center">
                    <Spinner />
                </Grid>
            )}
        </Suspense>
    );
}
