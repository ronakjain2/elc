import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Spinner from 'commonComponet/Spinner';
import React, { lazy, Suspense } from 'react';
import { FormattedMessage } from 'react-intl';


const AccountTab = lazy(() => import('../Components/AccountTab'));
const MobilePageHeader = lazy(() => import('../Components/MobilePageHeader'));
const SmsConsent = lazy(() => import('components/SmsConsent'));

export default function CommunicationPerderences() {
    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="my-communicationPerferences-container">
                <Grid item xs={11}>
                    <Grid container>
                        <Hidden only={['xs', 'sm']}>
                            {/** Tabs */}
                            <Grid item xs={12}>
                                <AccountTab activeKey="CommunicationPerferences" />
                            </Grid>
                        </Hidden>

                        <Hidden only={['md', 'lg', 'xl']}>
                            <Grid item xs={12}>
                                <MobilePageHeader
                                    title={
                                        <FormattedMessage
                                            id="MyAccount.CommunicationPreferences.Text"
                                            defaultMessage="Communication Preferences"
                                        />
                                    }
                                />
                            </Grid>
                        </Hidden>

                        <Grid item xs={12}>
                            <SmsConsent />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Suspense>
    );
}
