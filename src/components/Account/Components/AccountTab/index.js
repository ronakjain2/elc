import React, { useEffect } from 'react';

import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';

import { withRouter } from 'react-router';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import './accountTab.css';
import SimpleBreadcrumbs from 'commonComponet/Breadcrumbs';

function AccountTab({ history, activeKey }) {
    const state = useSelector((state) => state);

    const store_locale = state && state.auth && state.auth.store_locale;
    const loginUser = state && state.auth && state.auth.loginUser;

    const AccountTabList = [
        {
            name: <FormattedMessage id="Home.MyAccount" defaultMessage="My Account" />,
            icon: `/images/myaccount/myaccount.svg`,
            activeKey: 'MyAccount',
            link: `/${store_locale}/my-account`,
        },
        {
            name: <FormattedMessage id="MyAccount.MyProfile.Text" defaultMessage="My Profile" />,
            icon: `/images/myaccount/myaccount.svg`,
            activeKey: 'MyProfile',
            link: `/${store_locale}/profile`,
        },
        {
            name: <FormattedMessage id="MyAccount.MyOrder" defaultMessage="My Order" />,
            icon: `/images/myaccount/myorder.svg`,
            activeKey: 'MyOrder',
            link: `/${store_locale}/order-history`,
        },
        {
            name: <FormattedMessage id="MyAccount.MyAddresses" defaultMessage="My Addresses" />,
            icon: `/images/myaccount/myaddress.svg`,
            activeKey: 'MyAddresses',
            link: `/${store_locale}/my-addresses`,
        },
        {
            name: <FormattedMessage id="MyAccount.WishList" defaultMessage="WishList" />,
            icon: `/images/myaccount/mywishlist.svg`,
            activeKey: 'MyWishlist',
            link: `/${store_locale}/wish-list`,
        },
        {
            name: <FormattedMessage id="MyAccount.MyCards.Text" defaultMessage="My Cards" />,
            icon: `/images/myaccount/mycard.svg`,
            activeKey: 'MyCards',
            link: `/${store_locale}/my-cards`,
        },
        {
            name: <FormattedMessage id="MyAccount.MyClub.Text" defaultMessage="My Club" />,
            icon: `/images/myaccount/myclub.svg`,
            activeKey: 'MyClub',
            link: `/${store_locale}/my-club`,
        },
        {
            name: (
                <FormattedMessage
                    id="MyAccount.CommunicationPreferences.Text"
                    defaultMessage="Communication Preferences"
                />
            ),
            icon: `/images/myaccount/communication.svg`,
            activeKey: 'CommunicationPerferences',
            link: `/${store_locale}/communication-perferences`,
        },
    ];

    const gotoPage = (link) => {
        return history.push(link);
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: <FormattedMessage id="Home.MyAccount" defaultMessage="My Account" />,
        },
    ];

    useEffect(() => {
        try {
            if (!loginUser || !loginUser.customer_id || loginUser.customer_id === '') {
                history.push(`/${store_locale}/sign-in-register`);
            }
        } catch (err) {}
    }, [loginUser, history, store_locale]);

    return (
        <Grid container>
            <Grid item xs={12}>
                <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            </Grid>
            <Hidden only={['xs', 'sm']}>
                <Grid container justify="space-between" alignItems="center" className="my-account-tab-container">
                    {AccountTabList &&
                        AccountTabList.map((tab, index) => (
                            <div
                                key={`web_my_account_tab_${index}`}
                                onClick={() => gotoPage(tab.link)}
                                className={tab.activeKey === activeKey ? 'active' : ''}
                            >
                                <Typography className="title">{tab.name}</Typography>
                                {tab.activeKey === activeKey && <div className="active-line" />}
                            </div>
                        ))}
                </Grid>
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                <Grid container>
                    {AccountTabList &&
                        AccountTabList.map((tab, index) => {
                            if (index === 0) return null;
                            return (
                                <Grid
                                    item
                                    xs={4}
                                    key={`mobile_account_tab_${index}`}
                                    className="mobile-my-account-padding"
                                    onClick={() => gotoPage(tab.link)}
                                >
                                    <Grid container justify="center" className="mobile-card">
                                        <Grid item xs={12} className="textAlignCenter">
                                            <img src={tab.icon} alt="myaccounticon" />
                                        </Grid>
                                        <Grid item xs={11} className="textAlignCenter">
                                            <Typography className="title">{tab.name}</Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            );
                        })}
                </Grid>
            </Hidden>
        </Grid>
    );
}

export default withRouter(AccountTab);
