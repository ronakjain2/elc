import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

export default function ComingSoon() {
    return (
        <Grid container justify="center">
            <Typography className="coming-soon-text">Coming Soon</Typography>
        </Grid>
    );
}
