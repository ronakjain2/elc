import React from 'react';
import { withRouter } from 'react-router';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import './MobilePage.css';
import SimpleBreadcrumbs from 'commonComponet/Breadcrumbs';

function MobilePageHeader({ history, title }) {
    const state = useSelector((state) => state);

    const language = state && state.auth && state.auth.language;
    const global = state && state.auth;

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: <FormattedMessage id="Home.MyAccount" defaultMessage="My Account" />,
        },
    ];

    const gotoMyAccount = () => {
        history.push(`/${global && global.store_locale}/my-account`);
    };

    return (
        <>
            <Grid item xs={12}>
                <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            </Grid>
            <Grid container justify="space-between" alignItems="center">
                <div>
                    <Typography className="my-account-mobile-header-title">{title}</Typography>
                </div>
                <div>
                    <Button
                        onClick={() => gotoMyAccount()}
                        className="my-account-mobile-header-back"
                        startIcon={language === 'en' ? <ChevronLeft /> : <ChevronRight />}
                    >
                        <FormattedMessage id="Checkout.Back" defaultMessage="Back" />
                    </Button>
                </div>
            </Grid>
            <Grid container>
                <Grid item xs={12}>
                    <div className="border-bottom margin-m-p-h" />
                </Grid>
            </Grid>
        </>
    );
}

export default withRouter(MobilePageHeader);
