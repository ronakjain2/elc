import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_ORDER_HISTROY_REQUEST]: (state) => ({
        ...state,
        loader: true,
        orders: { recent_orders: [], past_orders: [] },
    }),
    [types.GET_ORDER_HISTROY_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
        orders: { recent_orders: [], past_orders: [] },
    }),
    [types.GET_ORDER_HISTROY_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        orders: payload || { recent_orders: [], past_orders: [] },
    }),

    [types.GET_ORDER_DETAIL_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_ORDER_DETAIL_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.GET_ORDER_DETAIL_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        orderDetails: payload || {},
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    orderDetails: {},
    orders: { recent_orders: [], past_orders: [] },
});
