import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../../api';
import { toast } from 'react-toastify';

const getOrdersReq = function* getOrdersReq({ payload }) {
    try {
        const { data } = yield call(api.getOrderHistory, payload);
        if (data && data.status) {
            yield put(actions.getOrderHistroyRequestSuccess(data.orders_details));
        } else {
            toast.error(data.message);
            yield put(actions.getOrderHistroyRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getOrderHistroyRequestFailed());
    }
};

const getOrderDetailsReq = function* getOrderDetailsReq({ payload }) {
    try {
        const { data } = yield call(api.getOrderDetails, payload);
        if (data && data.status) {
            yield put(actions.getOrderDetailRequestSuccess(data.orders_details));
        } else {
            yield put(actions.getOrderDetailRequestFailed());
            toast.error(data.message);
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getOrderDetailRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_ORDER_HISTROY_REQUEST, getOrdersReq);
    yield takeLatest(types.GET_ORDER_DETAIL_REQUEST, getOrderDetailsReq);
}
