import React, { Suspense, lazy, useEffect, useState } from 'react';
import Spinner from 'commonComponet/Spinner';

import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { actions as myOrderActions } from './redux/actions';
import './myOrder.css';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const MobilePageHeader = lazy(() => import('../Components/MobilePageHeader'));
const MyOrderTab = lazy(() => import('./Components/Tabs'));
const ListHeader = lazy(() => import('./Components/ListHeader'));
const OrderList = lazy(() => import('./Components/List'));

export default function MyOrder() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const customerId = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_id;
    const loader = state && state.myOrder && state.myOrder.loader;
    const store_locale = state && state.auth && state.auth.store_locale;

    const [activeTab, setActiveTab] = useState('recentOrder');

    useEffect(() => {
        dispatch(
            myOrderActions.getOrderHistroyRequest({
                Customerid: customerId,
            }),
        );
    }, [dispatch, customerId]);

    const list = state && state.myOrder && state.myOrder.orders;

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="my-order-container">
                <Grid item xs={11}>
                    <Grid container>
                        <Hidden only={['xs', 'sm']}>
                            {/** Tabs */}
                            <Grid item xs={12}>
                                <AccountTab activeKey="MyOrder" />
                            </Grid>
                        </Hidden>

                        <Hidden only={['md', 'lg', 'xl']}>
                            <Grid item xs={12}>
                                <MobilePageHeader
                                    title={<FormattedMessage id="MyAccount.MyOrder" defaultMessage="My Order" />}
                                />
                            </Grid>
                        </Hidden>

                        {!loader && (
                            <>
                                <Grid item xs={12}>
                                    <MyOrderTab activeTab={activeTab} setActiveTab={setActiveTab} />
                                </Grid>

                                <Grid item xs={12}>
                                    <ListHeader />
                                </Grid>
                                <Grid item xs={12}>
                                    <OrderList
                                        orders={activeTab === 'recentOrder' ? list.recent_orders : list.past_orders}
                                        store_locale={store_locale}
                                    />
                                </Grid>
                            </>
                        )}

                        {loader && (
                            <Grid container alignItems="center">
                                <Spinner />
                            </Grid>
                        )}
                    </Grid>
                </Grid>
            </Grid>
        </Suspense>
    );
}
