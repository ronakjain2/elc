import React from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';

export default function MyOrderTab({ activeTab, setActiveTab }) {
    const onChangeTab = (name) => {
        setActiveTab(name);
    };

    return (
        <Grid container>
            <Hidden only={['xs', 'sm']}>
                <Grid container>
                    <Button
                        className={activeTab === 'recentOrder' ? 'web-order-tab' : 'web-order-tab-inActive'}
                        onClick={() => onChangeTab('recentOrder')}
                    >
                        <FormattedMessage id="MyAccount.MyOrder.RecentOrders" defaultMessage="Recent Orders" />
                    </Button>

                    <Button
                        onClick={() => onChangeTab('pastOrders')}
                        className={activeTab === 'pastOrders' ? 'web-order-tab' : 'web-order-tab-inActive'}
                    >
                        <FormattedMessage id="MyAccount.MyOrder.PastOrders" defaultMessage="Past Orders" />
                    </Button>

                    <div className="green-line" />
                </Grid>
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                <Grid container className="mobile-order-tab">
                    <Grid
                        item
                        xs={6}
                        onClick={() => onChangeTab('recentOrder')}
                        className={
                            activeTab === 'recentOrder'
                                ? 'textAlignCenter active-mobile-tab'
                                : 'textAlignCenter inActive-mobile-tab'
                        }
                    >
                        <FormattedMessage id="MyAccount.MyOrder.RecentOrders" defaultMessage="Recent Orders" />
                        {activeTab === 'recentOrder' && <div className="green-line-m" />}
                    </Grid>

                    <Grid
                        item
                        xs={6}
                        onClick={() => onChangeTab('pastOrders')}
                        className={
                            activeTab === 'pastOrders'
                                ? 'textAlignCenter active-mobile-tab'
                                : 'textAlignCenter inActive-mobile-tab'
                        }
                    >
                        <FormattedMessage id="MyAccount.MyOrder.PastOrders" defaultMessage="Past Orders" />
                        {activeTab === 'pastOrders' && <div className="green-line-m" />}
                    </Grid>
                </Grid>
            </Hidden>
        </Grid>
    );
}
