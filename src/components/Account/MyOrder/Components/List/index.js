import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import moment from 'moment';
import { createEncryptOrderId } from 'components/Cart/Checkout/utils';
import { withRouter } from 'react-router';
import clsx from 'clsx';
import { returnOrderClassName } from '../../utils';

function OrderList({ history, orders, store_locale }) {
    const gotoOrderDetails = (orderId, order_increment_id) => {
        history.push(
            `/${store_locale}/order-details?orderId=${createEncryptOrderId(
                orderId,
            )}&&order_increment_id=${createEncryptOrderId(order_increment_id)}`,
        );
    };

    return (
        <>
            {orders &&
                orders.map((order, index) => (
                    <Grid
                        container
                        key={`my-order-list-${index}`}
                        alignItems="center"
                        className="padding-bottom"
                        spacing={2}
                    >
                        <Grid item xs={5} md={8} className="arabic-right">
                            <Typography className="my-order-recent-order-Id">
                                <FormattedMessage id="MyAccount.OrderID" defaultMessage="Order ID" />
                                :&nbsp;
                                <span>{order.order_increment_id}</span>
                            </Typography>
                            <Typography className="my-order-recent-order-date">
                                <FormattedMessage id="MyAccount.DateOfOrder" defaultMessage="Date of Order" />
                                :&nbsp;
                                <span className="value">{moment(order.order_date).format('DD-MM-YYYY')}</span>
                            </Typography>
                        </Grid>
                        <Grid item xs={3} md={2}>
                            <Typography
                                className={clsx('my-order-recent-order-status', returnOrderClassName(order.status))}
                            >
                                {order.status}
                            </Typography>
                        </Grid>
                        <Grid item xs={4} md={2} className="right-side">
                            <Button
                                className="view-details-button"
                                onClick={() => gotoOrderDetails(order.order_id, order.order_increment_id)}
                            >
                                <FormattedMessage id="MyAccount.ViewDetails" defaultMessage="View Details" />
                            </Button>
                        </Grid>
                    </Grid>
                ))}

            {(!orders || orders.length === 0) && (
                <Typography className="no-address-available arabic-right">
                    <FormattedMessage id="MyAccount.MyOrder.NoOrderAvailable" defaultMessage="No Order Available" />
                </Typography>
            )}
        </>
    );
}

export default withRouter(OrderList);
