import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';

export default function ListHeader() {
    return (
        <Grid container className="order-list-header-container">
            <Grid item xs={5} md={8} className="arabic-right">
                <Typography className="title">
                    <FormattedMessage id="MyAccount.Order" defaultMessage="Order" />
                </Typography>
            </Grid>
            <Grid item xs={3} md={2}>
                <Typography className="title">
                    <FormattedMessage id="MyAccount.Status" defaultMessage="Status" />
                </Typography>
            </Grid>
            <Grid item xs={4} md={2}></Grid>
            <div className="border-line" />
        </Grid>
    );
}
