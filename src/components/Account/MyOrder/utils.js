export const returnOrderClassName = (status) => {
    if (status) {
        const name = status.toLowerCase().replace(/ /g, '');
        if (name !== '') {
            return `order-status-${name}`;
        }
    }

    return '';
};
