import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Img from 'commonComponet/Image';
import React from 'react';
import { FormattedMessage } from 'react-intl';
// import Switch from '@material-ui/core/Switch';

export default function QrCodeSection({ data, qrMediaPath }) {
    // const [AutoRenewal, setAutoRenewal] = React.useState(false);

    return (
        <>
            <Grid container justify="center" className="bg-light px-4 mb-4" spacing={3}>
                <Grid item xs={12} md={5} lg={5}>
                    <div className="d-flex my-1">
                        <Typography className="title">
                            <FormattedMessage
                                id="MyAccount.MyClub.MembershipStatus"
                                defaultMessage="Membership Status"
                            />
                            &nbsp;:&nbsp;
                        </Typography>
                        <Typography className="active-value">
                            {data && data.membership_number ? (
                                <FormattedMessage
                                    id="MyAccount.MyClub.MembershipStatus.Activated"
                                    defaultMessage="Activated"
                                />
                            ) : (
                                <FormattedMessage
                                    id="MyAccount.MyClub.MembershipStatus.Deactivated"
                                    defaultMessage="Deactivated"
                                />
                            )}
                        </Typography>
                    </div>
                    {data && data.membership_expiry_date && (
                        <div className="d-flex my-1">
                            <Typography className="title">
                                <FormattedMessage id="MyAccount.MyClub.ExpiryDate" defaultMessage="Expiry Date" />
                                &nbsp;:&nbsp;
                            </Typography>
                            <Typography className="active-value">{data && data.membership_expiry_date}</Typography>
                        </div>
                    )}
                </Grid>
                <Grid item xs={12} md={4} lg={4}>
                    {data && data.membership_number && (
                        <>
                            <Img width="250" height="250" src={qrMediaPath} alt={'QR Code'} />
                            <Typography className="QrActive-text mt-2">Show your QR code at store</Typography>
                        </>
                    )}
                </Grid>
                <Grid item xs={12} md={3} lg={3}></Grid>
            </Grid>
        </>
    );
}
