import React from 'react';
import Grid from '@material-ui/core/Grid';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography';

const GENDERS = {
    Male: <FormattedMessage id="bithdayclub.Boy" defaultMessage="Boy" />,
    Female: <FormattedMessage id="bithdayclub.Girl" defaultMessage="Girl" />,
};

export default function ChildrenInfoList({ data }) {
    return (
        <>
            {/* <Grid item xs={12} md={7} container className="birthdayForm"> */}
            {/* {values.isFillAllForm === false && (
            <Grid container className="errorMsg">
                <Grid item xs={1} md={4}></Grid>

                <Grid style={{ textAlign: 'start' }} item xs={10} md={6}>
                    <FormattedMessage
                        id="birthdayClub.formRequiredMsg"
                        defaultMessage="Please fill all * fields compulsory"
                    />
                    <Close className="close-icon-sort crossSmall" />
                </Grid>
            </Grid>
             )}  */}
            {/* {values.children &&
                                values.children.map((child, index) => {
                                    return ( */}
            {data && data.child_data && data.child_data.length > 0 && (
                <Grid container className="pt-3">
                    <Grid item xs={4}>
                        <div className="px-2">
                            <Typography className="login-sigup-input-text text-align-rtl">
                                <FormattedMessage id="bithdayclub.ChildName" defaultMessage="Child Name" />
                            </Typography>
                            {/* <Typography className="login-sigup-input-text">
                       
                        <FormattedMessage id="bithdayclub.ChildName" defaultMessage="Child Name" />
                    </Typography> */}
                        </div>
                    </Grid>
                    <Grid item xs={4}>
                        <div className="px-2">
                            <Typography className="login-sigup-input-text text-align-rtl">
                                <FormattedMessage id="bithdayclub.Gender" defaultMessage="Gender" />
                            </Typography>
                            {/* <Typography className="login-sigup-input-text">
                       
                        <FormattedMessage id="bithdayclub.ChildName" defaultMessage="Child Name" />
                    </Typography> */}
                        </div>
                    </Grid>
                    <Grid item xs={4}>
                        <div className="px-2">
                            <Typography className="login-sigup-input-text text-align-rtl">
                                <FormattedMessage id="bithdayclub.DOB" defaultMessage="DOB" />
                            </Typography>

                            {/* <Typography className="login-sigup-input-text">
                       
                        <FormattedMessage id="bithdayclub.ChildName" defaultMessage="Child Name" />
                    </Typography> */}
                        </div>
                    </Grid>
                    {/* {index !== 0 && <Close className="remove-child"  />} */}
                </Grid>
            )}

            {data &&
                data.child_data &&
                data.child_data.map((item, index) => (
                    <Grid container>
                        <Grid item xs={4}>
                            <div className="px-2">
                                <Typography className="myclub-member-subtext text-align-rtl">{item.name}</Typography>
                            </div>
                        </Grid>
                        <Grid item xs={4}>
                            <div className="px-2">
                                <Typography className="myclub-member-subtext text-align-rtl">
                                    {GENDERS[item.gender]}
                                </Typography>
                            </div>
                        </Grid>
                        <Grid item xs={4}>
                            <div className="px-2">
                                <Typography className="myclub-member-subtext text-align-rtl">{item.dob}</Typography>
                            </div>
                        </Grid>
                    </Grid>
                ))}
            {/* );
                                })} */}
            {/* <Grid container>
                <Grid item xs={1} md={4}></Grid>
                <Grid item xs={12} md={4} className="px-2 textAlignCenter">
                    <Button className="addChildrenButton m-t-20 " onClick={handleClickOnAddChildren}>
                        <FormattedMessage id="birthdayClub.AddChildren" defaultMessage="Add Children" />
                    </Button>
                </Grid> */}
            {/* </Grid> */}
            {/* <Grid container>
                    <Grid item xs={1} md={2} className="px-2"></Grid>
                    <Grid item xs={12} md={8} className="px-2 textAlignCenter">
                        <Button className="birthday-club-register" >
                            <FormattedMessage id="SignInSignUp.Register" defaultMessage="Register" />
                        </Button>
                    </Grid>
                </Grid> */}
            {/* </Grid> */}
        </>
    );
}
