import React from 'react';
import Grid from '@material-ui/core/Grid';
import { FormattedMessage } from 'react-intl';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

export default function ChildrenInfo({
    childName,
    gender,
    dob,
    isErr,
    onChangeHandler,
    handleClickOnAddChildren,
    data,
}) {
    var currentDate = new Date();
    console.log(
        'currentDate',
        `${currentDate.getFullYear()}-${currentDate.getMonth() < 10 ? '0' : ''}${currentDate.getMonth() + 1}-${
            currentDate.getDate() < 10 ? '0' : ''
        }${currentDate.getDate()}`,
    );
    return (
        <>
            {/* <Grid item xs={12} md={7} container className="birthdayForm"> */}
            {/* {values.isFillAllForm === false && (
            <Grid container className="errorMsg">
                <Grid item xs={1} md={4}></Grid>

                <Grid style={{ textAlign: 'start' }} item xs={10} md={6}>
                    <FormattedMessage
                        id="birthdayClub.formRequiredMsg"
                        defaultMessage="Please fill all * fields compulsory"
                    />
                    <Close className="close-icon-sort crossSmall" />
                </Grid>
            </Grid>
             )}  */}
            {/* {values.children &&
                                values.children.map((child, index) => {
                                    return ( */}
            <Grid container item xs={12} className="pt-3">
                <div className="col-12 col-md-4 paddingLeft30">
                    <Typography className="login-sigup-input-text">
                        <span className="colorRed">*&nbsp;</span>
                        <FormattedMessage id="bithdayclub.ChildName" defaultMessage="Child Name" />
                    </Typography>
                    <TextField
                        value={childName}
                        onChange={onChangeHandler}
                        variant="outlined"
                        fullWidth
                        className="inputBox"
                        name="childName"
                    />
                    {isErr.childName && <Typography className="colorRed">Please enter valid child name</Typography>}
                </div>
                <div className="col-12 col-md-4 paddingLeft30">
                    <Typography className="login-sigup-input-text">
                        <span className="colorRed">*&nbsp;</span>
                        <FormattedMessage id="bithdayclub.Gender" defaultMessage="Gender" />
                    </Typography>
                    <TextField
                        value={gender}
                        onChange={onChangeHandler}
                        select
                        variant="outlined"
                        fullWidth
                        className="inputBox"
                        name="gender"
                    >
                        <MenuItem value="Male">
                            <span className="boldText">
                                <FormattedMessage id="bithdayclub.Boy" defaultMessage="Boy" />
                            </span>
                        </MenuItem>
                        <MenuItem value="Female">
                            <span className="boldText">
                                <FormattedMessage id="bithdayclub.Girl" defaultMessage="Girl" />
                            </span>
                        </MenuItem>
                    </TextField>
                </div>
                <div className="col-12 col-md-4 paddingLeft30">
                    <Typography className="login-sigup-input-text">
                        <span className="colorRed">*&nbsp;</span>
                        <FormattedMessage id="bithdayclub.DOB" defaultMessage="DOB" />
                    </Typography>
                    <TextField
                        value={dob}
                        InputProps={{
                            inputProps: {
                                max: `${currentDate.getFullYear()}-${currentDate.getMonth() < 10 ? '0' : ''}${
                                    currentDate.getMonth() + 1
                                }-${currentDate.getDate() < 10 ? '0' : ''}${currentDate.getDate()}`,
                            },
                        }}
                        onChange={onChangeHandler}
                        id="date"
                        type="date"
                        className="datePicker"
                        name="dob"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    {isErr.dob && (
                        <Typography className="colorRed">Children's age must be less then 18 year.</Typography>
                    )}
                </div>
                {/* {index !== 0 && <Close className="remove-child"  />} */}
            </Grid>
            {/* );
                                })} */}
            <Grid container>
                <Grid item xs={1} md={4}></Grid>
                <Grid item xs={12} md={4} className="paddingLeft30 textAlignCenter">
                    <Button
                        className={'addChildrenButton m-t-20 '}
                        onClick={handleClickOnAddChildren}
                    >
                        <FormattedMessage id="birthdayClub.AddChildren" defaultMessage="Add Children" />
                    </Button>
                </Grid>
                {/* </Grid> */}
                {/* <Grid container>
                    <Grid item xs={1} md={2} className="paddingLeft30"></Grid>
                    <Grid item xs={12} md={8} className="paddingLeft30 textAlignCenter">
                        <Button className="birthday-club-register" >
                            <FormattedMessage id="SignInSignUp.Register" defaultMessage="Register" />
                        </Button>
                    </Grid>
                </Grid> */}
            </Grid>
        </>
    );
}
