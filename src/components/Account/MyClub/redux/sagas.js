import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../../api';
import { toast } from 'react-toastify';

const addChildReq = function* addChildReq({ payload }) {
    const { customer_id, store_id } = payload;
    try {
        const { data } = yield call(api.addChild, payload);
        if (data && data.status) {
            yield put(actions.addChildRequestSuccess(data));
            yield put(
                actions.getMemberDetail({
                    customer_id: customer_id,
                    store_id,
                }),
            );
            toast.success(data && data.message);
        } else {
            toast.error((data && data.message) || 'Something Went to wrong, Please try after sometime');
            yield put(actions.addChildRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.addChildRequestFailed());
    }
};

const getMemberDetailReq = function* getMemberDetailReq({ payload }) {
    try {
        const { data } = yield call(api.getMembershipDetail, payload);

        if (data && data.status) {
            yield put(actions.getMemberDetailSuccess(data));
            // toast.success(data && data.message);
        } else {
            toast.error(data && data.message);
            yield put(actions.getMemberDetailFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getMemberDetailFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.ADD_CHILD_REQUEST, addChildReq);
    yield takeLatest(types.GET_MEMBER_DETAIL_REQUEST, getMemberDetailReq);
}
