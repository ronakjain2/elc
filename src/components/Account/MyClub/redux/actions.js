import { createAction } from 'redux-actions';

//Actions Types
const ADD_CHILD_REQUEST = 'ELC@OROB/ADD_CHILD_REQUEST';
const ADD_CHILD_REQUEST_SUCCESS = 'ELC@OROB/ADD_CHILD_REQUEST_SUCCESS';
const ADD_CHILD_REQUEST_FAILED = 'ELC@OROB/ADD_CHILD_REQUEST_FAILED';

const GET_MEMBER_DETAIL_REQUEST = 'ELC@OROB/GET_MEMBER_DETAIL_REQUEST';
const GET_MEMBER_DETAIL_REQUEST_SUCCESS = 'ELC@OROB/GET_MEMBER_DETAIL_REQUEST_SUCCESS';
const GET_MEMBER_DETAIL_REQUEST_FAILED = 'ELC@OROB/GET_MEMBER_DETAIL_REQUEST_FAILED';

//actions
const addChildRequest = createAction(ADD_CHILD_REQUEST);
const addChildRequestSuccess = createAction(ADD_CHILD_REQUEST_SUCCESS);
const addChildRequestFailed = createAction(ADD_CHILD_REQUEST_FAILED);

const getMemberDetail = createAction(GET_MEMBER_DETAIL_REQUEST);
const getMemberDetailSuccess = createAction(GET_MEMBER_DETAIL_REQUEST_SUCCESS);
const getMemberDetailFailed = createAction(GET_MEMBER_DETAIL_REQUEST_FAILED);

export const actions = {
    addChildRequest,
    addChildRequestSuccess,
    addChildRequestFailed,

    getMemberDetail,
    getMemberDetailSuccess,
    getMemberDetailFailed,
};

export const types = {
    ADD_CHILD_REQUEST,
    ADD_CHILD_REQUEST_SUCCESS,
    ADD_CHILD_REQUEST_FAILED,

    GET_MEMBER_DETAIL_REQUEST,
    GET_MEMBER_DETAIL_REQUEST_SUCCESS,
    GET_MEMBER_DETAIL_REQUEST_FAILED,
};
