import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.ADD_CHILD_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.ADD_CHILD_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
    }),
    [types.ADD_CHILD_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
    }),

    [types.GET_MEMBER_DETAIL_REQUEST]: (state) => ({
        ...state,
        loader: true,
    }),
    [types.GET_MEMBER_DETAIL_REQUEST_FAILED]: (state) => ({
        ...state,
        loader: false,
        childrenData: null,
    }),
    [types.GET_MEMBER_DETAIL_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        childrenData: payload,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    childrenData: null,
});
