import { Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Img from 'commonComponet/Image';
import Spinner from 'commonComponet/Spinner';
import React, { lazy, Suspense, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import './MyClub.css';
import { actions as myClubActions } from './redux/actions';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const MobilePageHeader = lazy(() => import('../Components/MobilePageHeader'));
const ChildrenInfo = lazy(() => import('./Components/ChildrenInfo'));
const QrCodeSection = lazy(() => import('./Components/QrCodeSection'));
const ChildrenInfoList = lazy(() => import('./Components/ChildrenInfoList'));

function MyClub({ history }) {
    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const auth = state && state.auth;
    const store_locale = state && state.auth && state.auth.store_locale;
    const store_id = state && state.auth && state.auth.currentStore;
    const customer_id = auth && auth.loginUser && auth.loginUser.customer_id;
    const membershipDetail =
        state && state.myClub && state.myClub.childrenData && state.myClub.childrenData.member_details;
    const qrMediaPath = state && state.myClub && state.myClub.childrenData && state.myClub.childrenData.qr_media_path;
    const banner_image = state && state.myClub && state.myClub.childrenData && state.myClub.childrenData.banner_image;
    const mob_banner_image =
        state && state.myClub && state.myClub.childrenData && state.myClub.childrenData.mob_banner_image;
    const instruction_msg =
        state && state.myClub && state.myClub.childrenData && state.myClub.childrenData.instruction_msg;
    const max_child_length =
        (state &&
            state.myClub &&
            state.myClub.childrenData &&
            state.myClub.childrenData.member_details &&
            state.myClub.childrenData.member_details.max_child_length) ||
        2;
    const max_child_message = (state &&
        state.myClub &&
        state.myClub.childrenData &&
        state.myClub.childrenData.member_details &&
        state.myClub.childrenData.member_details.max_child_message) || (
        <FormattedMessage
            id="MyAccount.MyClub.YouCanAddMax"
            defaultMessage="You can add max. two children to your Family Club membership"
        />
    );
    const loader = state && state.myClub && state.myClub.loader;

    let [childName, setChildName] = useState('');
    let [gender, setGender] = useState('');
    let [dob, setDob] = useState('');
    let [error, setError] = useState({ dob: false, gender: false, childName: false });

    useEffect(() => {
        dispatch(
            myClubActions.getMemberDetail({
                customer_id: customer_id,
                store_id,
            }),
        );
    }, [dispatch, customer_id, store_id]);

    const onChangeHandler = event => {
        if (event.target.name === 'childName') {
            const re = /^[\u0621-\u064A\u0660-\u0669a-zA-Z ]+$/g.test(event.target.value);
            if (re) {
                setError({ ...error, childName: false });
            } else {
                setError({ ...error, childName: true });
            }
            setChildName(event.target.value);
        } else if (event.target.name === 'gender') {
            setGender(event.target.value);
        } else if (event.target.name === 'dob') {
            let today = new Date();
            let bithday = new Date(event.target.value);
            var Difference_In_Time = today.getTime() - bithday.getTime();
            var diffDay = Difference_In_Time / (1000 * 3600 * 24);
            setDob(event.target.value);

            if (diffDay / 365.25 < 18) {
                setError({ ...error, dob: false, gender: false, childName: false });
            } else {
                setError({ ...error, dob: true });
            }
        }
    };

    const handleClickOnAddChildren = () => {
        if (childName !== '' && gender !== '' && dob !== '' && !error.dob && !error.childName) {
            let payload = {
                store_id: auth && auth.currentStore,
                customer_id: customer_id,
                name: childName,
                gender: gender,
                dob: dob,
            };
            dispatch(myClubActions.addChildRequest(payload));
            setChildName('');
            setGender('');
            setDob('');
        } else if (childName === '' || gender === '' || dob === '') {
            toast.error('Please Enter Values');
        }
    };

    return (
        <Suspense fallback={<Spinner />}>
            {!loader && (
                <Grid container justify="center" className="my-cards-container">
                    <Grid item xs={11}>
                        <Grid container spacing={3}>
                            <Hidden only={['xs', 'sm']}>
                                {/** Tabs */}
                                <Grid item xs={12}>
                                    <AccountTab activeKey="MyClub" />
                                </Grid>
                            </Hidden>

                            <Hidden only={['md', 'lg', 'xl']}>
                                <Grid item xs={12}>
                                    <MobilePageHeader
                                        title={<FormattedMessage id="MyAccount.MyClub.Text" defaultMessage="My Club" />}
                                    />
                                </Grid>
                            </Hidden>

                            <Hidden only={['xs', 'sm']}>
                                <Grid item xs={12} md={4}>
                                    <Img
                                        width="350"
                                        height="350"
                                        src={banner_image}
                                        alt={'birthdayClubImg'}
                                        onClick={() => history.push(`/${store_locale}/family-club`)}
                                    />
                                </Grid>
                            </Hidden>
                            <Hidden only={['md', 'lg', 'xl']}>
                                <Grid item xs={12} md={4} className="d-none">
                                    <Img
                                        width="400"
                                        height="400"
                                        src={mob_banner_image}
                                        alt={'birthdayClubImg'}
                                        onClick={() => history.push(`/${store_locale}/family-club`)}
                                    />
                                </Grid>
                            </Hidden>
                            <Grid item xs={12} md={8} className="p-4">
                                <QrCodeSection data={membershipDetail} qrMediaPath={qrMediaPath} />
                                {instruction_msg && (
                                    <Grid className="arabic-right">
                                        <Typography variant="span" className="color-4f4f4f font-weight-bold">
                                            {instruction_msg}
                                        </Typography>
                                    </Grid>
                                )}
                                <ChildrenInfoList data={membershipDetail} />
                                {membershipDetail &&
                                    Array.isArray(membershipDetail.child_data) &&
                                    membershipDetail.child_data.length < max_child_length && (
                                        <ChildrenInfo
                                            childName={childName}
                                            gender={gender}
                                            dob={dob}
                                            isErr={error}
                                            onChangeHandler={onChangeHandler}
                                            handleClickOnAddChildren={handleClickOnAddChildren}
                                            data={membershipDetail}
                                        />
                                    )}
                                {membershipDetail &&
                                    Array.isArray(membershipDetail.child_data) &&
                                    membershipDetail.child_data.length >= max_child_length && (
                                        <Grid item xs={12} className="text-center mt-4 bg-light py-2 px-2">
                                            <Typography variant="span" className="color-4f4f4f font-weight-600">
                                                {max_child_message}
                                            </Typography>
                                        </Grid>
                                    )}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            )}
            {loader && <Spinner />}
        </Suspense>
    );
}

export default withRouter(MyClub);
