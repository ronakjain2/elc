import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import { useSelector, useDispatch } from 'react-redux';
import Spinner from 'commonComponet/Spinner';
import { withRouter } from 'react-router';
import { actions as myWishlistActions } from 'components/Account/MyWishList/redux/actions';
import { FormattedMessage } from 'react-intl';

const WebWishListItem = lazy(() => import('./Components/WebItem'));
const MobileWishListItem = lazy(() => import('./Components/MobileItem'));

function MyWishListItems({ history }) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const wishlistitem = (state && state.myWishlist && state.myWishlist.wishListItems) || [];
    const country = state && state.auth && state.auth.country;
    const customerid = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_id;
    const store_id = state && state.auth && state.auth.currentStore;
    const store_locale = state && state.auth && state.auth.store_locale;
    const quote_id = state && state.auth && state.auth.quote_id;

    let currency = country === 'UAE' ? 'AED' : 'SAR';

    const onClickRemove = (row) => {
        const wishilistitemid = row && row.wishlist_id;
        dispatch(
            myWishlistActions.removeWishlistRequest({
                customerid,
                store_id,
                wishilistitemid,
                quote_id,
            }),
        );
    };

    const onClickImage = (row) => {
        history.push(`/${store_locale}/products-details/${row && row.url_key}`);
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Typography className="item-count-primary">
                    <FormattedMessage id="MyAccount.MyWishlist.YourWishlist" defaultMessage="Your Wishlist" />
                    &nbsp;
                    <span className="item-count">
                        ({wishlistitem && wishlistitem.length}{' '}
                        <FormattedMessage id="MyAccount.MyWishlist.Items" defaultMessage="items" />)
                    </span>
                </Typography>
            </Grid>

            <Hidden only={['xs', 'sm']}>
                <Grid item xs={12}>
                    <Grid container className="line-container">
                        <Grid item xs={8} className="arabic-right">
                            <Typography className="WebItemTitle-text">
                                <FormattedMessage id="Cart.ItemNumberStyle" defaultMessage="Item (style number)" />
                            </Typography>
                        </Grid>
                        <Grid item xs={4} className="text-right-1">
                            <Typography className="WebItemTitle-text">
                                <FormattedMessage id="Cart.Price" defaultMessage="Price" />
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    {wishlistitem &&
                        wishlistitem.map((item, index) => (
                            <Grid container key={`web_wishlist_item_${index}`}>
                                <Suspense fallback={<Spinner />}>
                                    <WebWishListItem
                                        item={item}
                                        currency={currency}
                                        onClickRemove={onClickRemove}
                                        onClickImage={onClickImage}
                                    />
                                </Suspense>
                            </Grid>
                        ))}
                </Grid>
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                <Grid item xs={12}>
                    {wishlistitem &&
                        wishlistitem.map((item, index) => (
                            <Grid container key={`mobile_wishlist_item_${index}`}>
                                <Suspense fallback={<Spinner />}>
                                    <MobileWishListItem
                                        item={item}
                                        currency={currency}
                                        onClickRemove={onClickRemove}
                                        onClickImage={onClickImage}
                                    />
                                </Suspense>
                            </Grid>
                        ))}
                </Grid>
            </Hidden>

            {wishlistitem && wishlistitem.length === 0 && (
                <Typography className="no-data-available">
                    <FormattedMessage id="MyAccount.MyWishlist.WishlistIsEmpty" defaultMessage="Wishlist is empty" />
                </Typography>
            )}
        </Grid>
    );
}

export default withRouter(MyWishListItems);
