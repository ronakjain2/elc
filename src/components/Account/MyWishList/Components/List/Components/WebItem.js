import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import { checkItemOutOfStock } from 'components/Cart/Basket/utils';
import { FormattedMessage } from 'react-intl';
import { getPrices } from 'components/Account/MyWishList/utils';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import Close from '@material-ui/icons/Close';
import { actions as PDPActions } from 'components/PDP/redux/actions';
import { useSelector, useDispatch } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';

export default function WebWishListItem({ item, currency, onClickRemove, onClickImage }) {
    const [currentProduct, setcurrentProduct] = useState(null);
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const auth = state && state.auth && state.auth;
    const addToCartLoader = state && state.PDP && state.PDP.addToCartLoader;

    useEffect(() => {
        if (!addToCartLoader) setcurrentProduct(null);
    }, [addToCartLoader]);

    const addToCart = (item) => {
        setcurrentProduct(item.product_id);
        let quote_id = auth && auth.guestUser.guestId;
        if (auth && auth.loginUser) {
            quote_id = auth && auth.quote_id;
        }

        dispatch(
            PDPActions.addToCartRequest({
                cart_item: {
                    product_option: {
                        extension_attributes: {},
                    },
                    product_type: (item && item.type) || 'simple',
                    qty: 1,
                    quote_id,
                    sku: item && item.sku,
                },
                gtmData: {
                    currency: item && item.currency,
                    category_names: item && item.ga_cat_names,
                    offers: item && item.offers,
                },
                wishlist_id: item && item.wishlist_id,
            }),
        );
    };

    return (
        <Grid container className="wish-list-p-t-b">
            <Grid item xs={8}>
                <Grid container>
                    <Grid item xs={2} className={checkItemOutOfStock(item) ? 'OutOfStock' : ''}>
                        <img
                            src={item && item.image && item.image[0]}
                            onClick={() => onClickImage(item)}
                            alt="productimage"
                            className="webItemImage"
                        />
                    </Grid>
                    <Grid item xs={8} className="arabic-right padding-right">
                        <Typography
                            className={
                                checkItemOutOfStock(item) ? 'OutOfStock wishlist-item-name' : 'wishlist-item-name'
                            }
                        >
                            {item.name}
                        </Typography>
                        {/*<Typography className={checkItemOutOfStock(item) ? "OutOfStock wishlist-item-details" : "wishlist-item-details"}>
                            <FormattedMessage id="PDP.Age" defaultMessage="Age" />
                            :
                        </Typography>*/}
                        <Typography
                            className={
                                checkItemOutOfStock(item) ? 'OutOfStock wishlist-item-details' : 'wishlist-item-details'
                            }
                        >
                            <FormattedMessage id="PDP.Specifications.ProductCode" defaultMessage="Product Code" />
                            :&nbsp;
                            <span className="wishlist-item-details-value">{item.sku}</span>
                        </Typography>
                        <Button
                            className="remove-wishlist-button"
                            onClick={() => onClickRemove(item)}
                            startIcon={<Close />}
                        >
                            <FormattedMessage
                                id="MyAccount.MyWishlist.RemoveFromWishlist"
                                defaultMessage="Remove From Wishlist"
                            />
                        </Button>
                        {!checkItemOutOfStock(item) &&
                            (addToCartLoader && currentProduct === item.product_id ? (
                                <Button className="remove-wishlist-button">
                                    <CircularProgress className="add-to-basket" size="18px" />
                                </Button>
                            ) : (
                                <Button
                                    className="remove-wishlist-button add-to-basket"
                                    onClick={() => addToCart(item)}
                                    startIcon={
                                        <ListItemIcon className="minWidthAuto">
                                            <img
                                                src="/images/pdp/Icon_Cart_Cart_Green.svg"
                                                alt="pickupformstor"
                                                className="MuiSvgIcon-root-Free"
                                            />
                                        </ListItemIcon>
                                    }
                                >
                                    <FormattedMessage id="Product.AddToBasket" defaultMessage="Add to basket" />
                                </Button>
                            ))}
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={4} className="text-right">
                <Grid container>
                    <Grid item xs={12} className={checkItemOutOfStock(item) ? 'OutOfStock arabic-left' : 'arabic-left'}>
                        {getPrices(item, currency)}
                    </Grid>
                    {!checkItemOutOfStock(item) && (
                        <Grid item xs={12} className="CartAvailableFor">
                            <Grid container justify="flex-end">
                                <Typography className="availableFor-text">
                                    <FormattedMessage id="PDP.AvailableFor" defaultMessage="Available for" />
                                </Typography>
                                <Grid container justify="flex-end">
                                    <div>
                                        <List component="nav" aria-label="main mailbox folders" className="p-t-b">
                                            <ListItem className="p-t-b">
                                                <ListItemIcon>
                                                    <img
                                                        src="/images/pdp/Icon_Header_FreeShip.svg"
                                                        alt="pickupformstor"
                                                        className="MuiSvgIcon-root-Free"
                                                    />
                                                </ListItemIcon>
                                                <ListItemText
                                                    primary={
                                                        <FormattedMessage
                                                            id="PDP.HomeDelivery"
                                                            defaultMessage="Home Delivery"
                                                        />
                                                    }
                                                />
                                            </ListItem>
                                        </List>
                                    </div>
                                    <div className="flex alignItemCenter">
                                        <Divider orientation="vertical" className="line" />
                                    </div>
                                    <div>
                                        <List component="nav" aria-label="main mailbox folders" className="p-t-b">
                                            <ListItem className="padding-right-zero p-t-b">
                                                <ListItemIcon>
                                                    <img
                                                        src="/images/pdp/Icon_Cart_Cart_Green.svg"
                                                        alt="pickupformstor"
                                                        className="MuiSvgIcon-root-Free"
                                                    />
                                                </ListItemIcon>
                                                <ListItemText
                                                    primary={
                                                        <FormattedMessage
                                                            id="PDP.PickupFromStore"
                                                            defaultMessage="Pickup from store"
                                                        />
                                                    }
                                                />
                                            </ListItem>
                                        </List>
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    )}

                    {checkItemOutOfStock(item) && (
                        <Grid container justify="flex-end">
                            <Typography className="sold-out-wishlist">
                                <FormattedMessage id="Cart.SoldOut" defaultMessage="Sold Out" />
                            </Typography>
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
}
