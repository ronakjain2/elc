import React from 'react';
import Typography from '@material-ui/core/Typography';

export const getPrices = (item, currency) => {
    return (
        <>
            {item && item.special_price ? (
                <>
                    <Typography className="cart-showing-price">
                        <span className="now-price">{`${currency} ${
                            item && item.special_price && parseFloat(item.special_price)
                        }`}</span>
                        <span className="old-price">{`${currency} ${item && item.price}`}</span>
                    </Typography>
                </>
            ) : (
                <Typography className="cart-showing-price">
                    <span className="now-price">{`${currency} ${item && item.price}`}</span>
                </Typography>
            )}
        </>
    );
};

export const getPricesM = (item, currency) => {
    return (
        <>
            {item && item.special_price ? (
                <>
                    <Typography className="cart-showing-price">
                        <span className="now-price">{`${currency} ${
                            item && item.special_price && parseFloat(item.special_price)
                        }`}</span>
                    </Typography>
                    <Typography className="cart-showing-price">
                        <span className="old-price">{`${currency} ${item && item.price}`}</span>
                    </Typography>
                </>
            ) : (
                <Typography className="cart-showing-price">
                    <span className="now-price">{`${currency} ${item && item.price}`}</span>
                </Typography>
            )}
        </>
    );
};
