import React, { Suspense, lazy, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Spinner from 'commonComponet/Spinner';
import Hidden from '@material-ui/core/Hidden';

import { actions as myWishlistActions } from './redux/actions';
import { useSelector, useDispatch } from 'react-redux';
import './MyWishlist.css';
import { FormattedMessage } from 'react-intl';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const MyWishListItems = lazy(() => import('./Components/List'));
const MobilePageHeader = lazy(() => import('../Components/MobilePageHeader'));

export default function MyWishList() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const store_id = state && state.auth && state.auth.currentStore;
    const customerid = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_id;
    const addLoader = state && state.myWishlist && state.myWishlist.addLoader;
    const loader = state && state.myWishlist && state.myWishlist.loader;
    const quote_id = state && state.auth && state.auth.quote_id;

    useEffect(() => {
        dispatch(
            myWishlistActions.getAllWishListRequest({
                customerid,
                store_id,
                quote_id,
            }),
        );
    }, [dispatch, customerid, store_id, quote_id]);

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="my-wishlist-container">
                <Grid item xs={11}>
                    <Grid container>
                        <Hidden only={['xs', 'sm']}>
                            {/** Tabs */}
                            <Grid item xs={12}>
                                <AccountTab activeKey="MyWishlist" />
                            </Grid>
                        </Hidden>

                        <Hidden only={['md', 'lg', 'xl']}>
                            <Grid item xs={12}>
                                <MobilePageHeader
                                    title={<FormattedMessage id="MyAccount.WishList" defaultMessage="WishList" />}
                                />
                            </Grid>
                        </Hidden>

                        {!addLoader && !loader && (
                            <Grid item xs={12}>
                                <MyWishListItems />
                            </Grid>
                        )}
                        {(addLoader || loader) && (
                            <Grid container alignItems="center">
                                <Spinner />
                            </Grid>
                        )}
                    </Grid>
                </Grid>
            </Grid>
        </Suspense>
    );
}
