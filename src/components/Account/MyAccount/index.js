import React, { lazy, Suspense, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Spinner from 'commonComponet/Spinner';
import { useSelector, useDispatch } from 'react-redux';
import { actions as myAddressesActions } from 'components/Account/MyAddresses/redux/actions';
import { actions as myOrderActions } from 'components/Account/MyOrder/redux/actions';

import './MyAccount.css';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const RecenOrderForMyAccount = lazy(() => import('./Components/RecentOrder'));
const AddressesForMyAccount = lazy(() => import('./Components/Addresses'));
const MyAccountDetails = lazy(() => import('./Components/Details'));

export default function MyAccount(props) {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const customerid = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_id;
    const store_id = state && state.auth && state.auth.currentStore;

    useEffect(() => {
        if (customerid) {
            dispatch(
                myAddressesActions.getAllAccountAddressRequest({
                    customerid: customerid,
                    store_id,
                }),
            );
            dispatch(
                myOrderActions.getOrderHistroyRequest({
                    Customerid: customerid,
                }),
            );
        }
    }, [dispatch, customerid, store_id]);

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="my-account-details">
                <Hidden only={['xs', 'sm']}>
                    <Grid item xs={11}>
                        <Grid container>
                            <Grid item xs={12}>
                                <AccountTab activeKey="MyAccount" />
                            </Grid>

                            <Grid item xs={12}>
                                <RecenOrderForMyAccount />
                            </Grid>

                            <Grid item xs={12}>
                                <div className="border-line-green" />
                            </Grid>

                            <Grid item xs={12}>
                                <MyAccountDetails />
                            </Grid>

                            <Grid item xs={12}>
                                <div className="border-line-green" />
                            </Grid>

                            <Grid item xs={12}>
                                <AddressesForMyAccount />
                            </Grid>
                        </Grid>
                    </Grid>
                </Hidden>

                <Hidden only={['md', 'lg', 'xl']}>
                    <Grid item xs={12}>
                        <Grid container>
                            <Grid item xs={12} style={{ padding: `20px 10px` }}>
                                <AccountTab activeKey="MyAccount" />
                            </Grid>
                        </Grid>
                    </Grid>
                </Hidden>
            </Grid>
        </Suspense>
    );
}
