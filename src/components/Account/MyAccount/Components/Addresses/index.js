import React from 'react';
import { withRouter } from 'react-router';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { replaceTilCharacterToComma } from 'components/Cart/Checkout/Delivery/utils';

function AddressesForMyAccount({ history }) {
    const state = useSelector((state) => state);

    const store_locale = state && state.auth && state.auth.store_locale;
    const addresses = (state && state.myAddresses && state.myAddresses.addresses) || [];

    const gotoAddresses = () => {
        history.push(`/${store_locale}/my-addresses`);
    };

    return (
        <Grid container>
            {/** Title */}
            <Grid item xs={12}>
                <Grid container justify="space-between">
                    <Typography className="your-recent-order">
                        <FormattedMessage id="MyAccount.MyAddresses" defaultMessage="My Addresses" />
                    </Typography>
                    <Typography className="view-all" onClick={() => gotoAddresses()}>
                        <FormattedMessage id="MyAccount.ViewAll" defaultMessage="View All" />
                    </Typography>
                </Grid>
            </Grid>
            {/** Address Cards */}
            <Grid item xs={12}>
                <Grid container>
                    {addresses &&
                        addresses.slice(0, 3).map((address, index) => (
                            <Grid
                                item
                                xs={4}
                                key={`address_of_my_account_${index}`}
                                className="padding-of-address-card"
                            >
                                <Grid container className="address-card">
                                    <Grid item xs={12}>
                                        <Typography className="address-user-name">
                                            {address && `${address.userFirstName} ${address.userLastName}`}
                                        </Typography>
                                        <Typography className="address-type">
                                            {address && address.address_type}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography className="address-value">
                                            {address && address.street && replaceTilCharacterToComma(address.street)}
                                        </Typography>
                                        <Typography className="address-value">{address && address.city}</Typography>
                                        <Typography className="address-value dir-ltr">
                                            {address && `+${address.carrier_code} ${address.telephone}`}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        ))}
                    {/** No Data Message */}
                    {(!addresses || addresses.length === 0) && (
                        <Grid item xs={12}>
                            <Typography className="no-address-available">
                                <FormattedMessage
                                    id="MyAccount.NoAddress.Message"
                                    defaultMessage="No address available"
                                />
                            </Typography>
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
}

export default withRouter(AddressesForMyAccount);
