import React from 'react';
import { withRouter } from 'react-router';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import moment from 'moment';
import { useSelector } from 'react-redux';
import Spinner from 'commonComponet/Spinner';
import { createEncryptOrderId } from 'components/Cart/Checkout/utils';
import clsx from 'clsx';
import { returnOrderClassName } from 'components/Account/MyOrder/utils';

function RecenOrderForMyAccount({ history }) {
    const state = useSelector((state) => state);

    const recent_orders = (state && state.myOrder && state.myOrder.orders && state.myOrder.orders.recent_orders) || [];
    const loader = state && state.myOrder && state.myOrder.loader;
    const store_locale = state && state.auth && state.auth.store_locale;

    const gotoOrderDetails = (order_id, order_increment_id) => {
        history.push(
            `/${store_locale}/order-details?orderId=${createEncryptOrderId(
                order_id,
            )}&order_increment_id=${createEncryptOrderId(order_increment_id)}`,
        );
    };

    const gotoViewOrders = () => {
        history.push(`/${store_locale}/order-history`);
    };

    return (
        <>
            <Grid container>
                {/** Title  */}
                <Grid item xs={12}>
                    <Grid container justify="space-between">
                        <Typography className="your-recent-order">
                            <FormattedMessage id="MyAccount.YourRecentOrders" defaultMessage="Your Recent Orders" />
                        </Typography>
                        <Typography className="view-all" onClick={() => gotoViewOrders()}>
                            <FormattedMessage id="MyAccount.ViewAll" defaultMessage="View All" />
                        </Typography>
                    </Grid>
                </Grid>

                {/** Header */}
                <Grid container>
                    <Grid item xs={12}>
                        <div className="border-line" />
                    </Grid>
                    <Grid item xs={8} className="arabic-right">
                        <Typography className="order-header-name">
                            <FormattedMessage id="MyAccount.Order" defaultMessage="Order" />
                        </Typography>
                    </Grid>
                    <Grid item xs={2} className="arabic-right">
                        <Typography className="order-header-name">
                            <FormattedMessage id="MyAccount.Status" defaultMessage="Status" />
                        </Typography>
                    </Grid>
                    <Grid item xs={2} className="arabic-right"></Grid>
                    <Grid item xs={12}>
                        <div className="border-line" />
                    </Grid>
                </Grid>
                {/** End Header */}
            </Grid>
            <Grid container>
                <Grid item xs={12}>
                    {!loader &&
                        recent_orders &&
                        recent_orders.slice(0, 3).map((order, index) => (
                            <Grid
                                container
                                key={`order_my_account_recent_${index}`}
                                alignItems="center"
                                className="padding-top-bottom"
                            >
                                <Grid item xs={8} className="arabic-right">
                                    <Typography className="my-address-recent-order-id">
                                        <FormattedMessage id="MyAccount.OrderID" defaultMessage="Order ID" />
                                        :&nbsp;
                                        <span>{order.order_increment_id}</span>
                                    </Typography>
                                    <Typography className="my-address-recent-order-date">
                                        <FormattedMessage id="MyAccount.DateOfOrder" defaultMessage="Date of Order" />
                                        :&nbsp;
                                        <span className="value">{moment(order.order_date).format('DD-MM-YYYY')}</span>
                                    </Typography>
                                </Grid>
                                <Grid item xs={2} className="arabic-right">
                                    <Typography
                                        className={clsx(
                                            'my-address-recent-order-status',
                                            returnOrderClassName(order.status),
                                        )}
                                    >
                                        {order.status}
                                    </Typography>
                                </Grid>
                                <Grid item xs={2} className="arabic-right">
                                    <Button
                                        className="view-details-button"
                                        onClick={() => gotoOrderDetails(order.order_id, order.order_increment_id)}
                                    >
                                        <FormattedMessage id="MyAccount.ViewDetails" defaultMessage="View Details" />
                                    </Button>
                                </Grid>
                            </Grid>
                        ))}
                    {!loader && (!recent_orders || recent_orders.length === 0) && (
                        <Grid item xs={12}>
                            <Typography className="no-address-available">
                                <FormattedMessage
                                    id="MyAccount.NoRecentOrder.Message"
                                    defaultMessage="No recent order available"
                                />
                            </Typography>
                        </Grid>
                    )}
                    {loader && (
                        <Grid container alignItems="center">
                            <Spinner />
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </>
    );
}

export default withRouter(RecenOrderForMyAccount);
