import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

export default function MyAccountDetails() {
    const state = useSelector((state) => state);

    const loginUser = state && state.auth && state.auth.loginUser;
    const carrier_code =
        (loginUser && loginUser.carrier_code) || (state && state.myAddresses && state.myAddresses.carrier_code) || '';

    return (
        <Grid container>
            {/** Title */}
            <Grid item xs={12}>
                <Grid container justify="space-between">
                    <Typography className="your-recent-order">
                        <FormattedMessage id="Checkout.Delivery.Pickup.YourDetails" defaultMessage="Your Details" />
                    </Typography>
                </Grid>
            </Grid>
            {/** Data */}
            <Grid item xs={12}>
                <Grid container>
                    <Grid item xs={4} className="arabic-right">
                        <Typography className="title">
                            <FormattedMessage id="MyAccount.UserName.Name" defaultMessage="Name" />
                        </Typography>
                        <Typography className="value">
                            {loginUser && loginUser.firstname && `${loginUser.firstname} ${loginUser.lastname}`}
                        </Typography>
                    </Grid>
                    <Grid item xs={4} className="arabic-right">
                        <Typography className="title">
                            <FormattedMessage id="MyAccount.ContactNumber" defaultMessage="Contact Number" />
                        </Typography>
                        <Typography className="value dir-ltr">
                            {loginUser && loginUser.phone_number && `+${carrier_code} ${loginUser.phone_number}`}
                        </Typography>
                    </Grid>
                    <Grid item xs={4} className="arabic-right">
                        <Typography className="title">
                            <FormattedMessage id="MyAccount.Email" defaultMessage="Email" />
                        </Typography>
                        <Typography className="value">{loginUser && loginUser.email}</Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
