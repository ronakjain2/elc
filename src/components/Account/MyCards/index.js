import { Typography } from '@material-ui/core';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fade from '@material-ui/core/Fade';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Spinner from 'commonComponet/Spinner';
import React, { lazy, Suspense, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { actions as checkoutPaymentActions } from '../../Cart/Checkout/Payment/redux/actions';
import './MyCard.css';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const MobilePageHeader = lazy(() => import('../Components/MobilePageHeader'));

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '10px',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #0D943F',
        borderRadius: '15px',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export default function MyCards() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();
    const classes = useStyles();

    const { credit_cards, loader } = state && state.checkoutPayment && state.checkoutPayment;
    const store_id = state && state.auth && state.auth.currentStore;
    const [show, setShow] = useState(false);
    const [currentId, setcurrentId] = useState(null);

    useEffect(() => {
        window.scrollTo(0, 0);
        try {
            dispatch(
                checkoutPaymentActions.getCheckoutPaymentsRequest({
                    store_id,
                }),
            );
        } catch (err) {}
    }, [dispatch, store_id]);

    useEffect(() => {
        if (!loader) {
            setShow(false);
        }
    }, [loader]);

    const deleteSavedCard = () => {
        dispatch(
            checkoutPaymentActions.deleteSavedCardRequest({
                id: currentId,
                store_id: store_id,
            }),
        );
    };

    const checkCardType = (card_number, check_eager_pattern = true) => {
        const Type = require('creditcards-types/type');
        const madaType = Type({
            name: 'Mada',
            digits: 16,
            pattern: /^(4(0(0861|6996|7(197|395)|9201)|1(0621|2565|0685|7633|9593)|2(0132|281(7|8|9)|8(331|67(1|2|3)))|3(1361|2328|4107|9954)|4(0(533|647|795)|5564|6(393|404|672))|5(5(036|708)|7865|7997|8456)|6(2220|854(0|1|2|3))|7(4491)|8(301(0|1|2)|4783|609(4|5|6)|931(7|8|9)))|5(0(4300|6968|8160)|13213|2(0058|1076|4(130|514)|9(415|741))|3(0(060|906)|1(095|196)|2013|5(825|989)|6023|7767)|4(3(085|357)|9760)|5(4180|8563)|8(5265|8(8(4(5|7|8)|5(0|1))|98(2|3))|9(005|206)))|6(0(4906|5141)|36120)|9682(0(1|2|3|4|5|6|7|8|9)|1(1)))\d{10}$/,
            eagerPattern: /^(4(0(0861|6996|7(197|395)|9201)|1(0621|2565|0685|7633|9593)|2(0132|281(7|8|9)|8(331|67(1|2|3)))|3(1361|2328|4107|9954)|4(0(533|647|795)|5564|6(393|404|672))|5(5(036|708)|7865|7997|8456)|6(2220|854(0|1|2|3))|7(4491)|8(301(0|1|2)|4783|609(4|5|6)|931(7|8|9)))|5(0(4300|6968|8160)|13213|2(0058|1076|4(130|514)|9(415|741))|3(0(060|906)|1(095|196)|2013|5(825|989)|6023|7767)|4(3(085|357)|9760)|5(4180|8563)|8(5265|8(8(4(5|7|8)|5(0|1))|98(2|3))|9(005|206)))|6(0(4906|5141)|36120)|9682(0(1|2|3|4|5|6|7|8|9)|1(1)))/,
        });
        const types = [madaType].concat(require('creditcards-types'));
        const type = types.find((type) => type.test(card_number, check_eager_pattern));
        return type && type.name;
    };

    const getExpiryDate = (date) => {
        if (date) date = `${date.charAt(2)}${date.charAt(3)}/${date.charAt(0)}${date.charAt(1)}`;
        return date;
    };

    const showModal = (id) => {
        setcurrentId(id);
        setShow(true);
    };

    const hideModal = () => {
        setShow(false);
        setcurrentId(null);
    };

    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="my-cards-container">
                <Grid item xs={11}>
                    <Grid container>
                        <Hidden only={['xs', 'sm']}>
                            {/** Tabs */}
                            <Grid item xs={12}>
                                <AccountTab activeKey="MyCards" />
                            </Grid>
                        </Hidden>

                        <Hidden only={['md', 'lg', 'xl']}>
                            <Grid item xs={12}>
                                <MobilePageHeader
                                    title={<FormattedMessage id="MyAccount.MyCards.Text" defaultMessage="My Cards" />}
                                />
                            </Grid>
                        </Hidden>

                        <Grid item xs={12} className="mx-2">
                            {loader && (
                                <Grid container alignItems="center">
                                    <Spinner />
                                </Grid>
                            )}
                            {!loader && credit_cards && credit_cards.length > 0 && (
                                <>
                                    <Grid item xs={12}>
                                        <div className="border-line" />
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Typography className="mb-2 title-Page arabic-right">
                                            <FormattedMessage
                                                id="Checkout.Payment.SavedCards"
                                                defaultMessage="Saved Cards"
                                            />
                                        </Typography>
                                    </Grid>

                                    {credit_cards &&
                                        credit_cards.map((_card) => {
                                            return (
                                                <Grid container spacing={1} direction="row" alignItems="center">
                                                    <Grid item xs={2} md={1}>
                                                        {checkCardType(_card.card_number) === 'Visa' ? (
                                                            <img src="/images/checkout/card-visa.svg" alt="card" />
                                                        ) : checkCardType(_card.card_number) === 'Mada' ? (
                                                            <img src="/images/checkout/card-mada.svg" alt="card" />
                                                        ) : checkCardType(_card.card_number) === 'Mastercard' ? (
                                                            <img
                                                                src="/images/checkout/card-mastercard.svg"
                                                                alt="card"
                                                            />
                                                        ) : (
                                                            <img src="/images/checkout/card.svg" alt="card" />
                                                        )}
                                                    </Grid>
                                                    <Grid item xs={3} md={4}>
                                                        {_card.name_on_card}
                                                    </Grid>
                                                    <Grid item xs={4} className="dir-ltr">
                                                        {_card.card_number}
                                                    </Grid>
                                                    <Grid item xs={2}>
                                                        {getExpiryDate(_card.expiry_date)}
                                                    </Grid>
                                                    <Grid item xs={1}>
                                                        <DeleteForeverIcon
                                                            onClick={() => showModal(_card.id)}
                                                            className="deleteIcon"
                                                        />
                                                    </Grid>
                                                </Grid>
                                            );
                                        })}
                                </>
                            )}
                            {!loader && credit_cards && credit_cards.length === 0 && (
                                <Grid container direction="row" justify="center" alignItems="center">
                                    <Typography className="mb-2">
                                        <FormattedMessage
                                            id="paybycard.nocardsaved"
                                            defaultMessage="You have not saved any card"
                                        />
                                    </Typography>
                                </Grid>
                            )}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Modal
                className={classes.modal}
                open={show}
                onClose={hideModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={show}>
                    <Grid className={classes.paper}>
                        <Grid item xs={12} className="mb-4">
                            <Typography>
                                <FormattedMessage
                                    id="askCardDelete"
                                    defaultMessage="The selected card will be deleted permanantly. Are you sure to do that?"
                                />
                            </Typography>
                        </Grid>
                        <Grid container direction="row" justify="center" alignItems="center">
                            {!loader ? (
                                <>
                                    <Button onClick={hideModal} className="button">
                                        <FormattedMessage id="askCardDelete.No" defaultMessage="No" />
                                    </Button>
                                    <Button onClick={deleteSavedCard} className="button">
                                        <FormattedMessage id="askCardDelete.Yes" defaultMessage="Yes" />
                                    </Button>
                                </>
                            ) : (
                                <CircularProgress className="mx-2" />
                            )}
                        </Grid>
                    </Grid>
                </Fade>
            </Modal>
        </Suspense>
    );
}
