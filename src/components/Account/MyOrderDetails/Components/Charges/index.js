import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';

export default function OrderDetailsCharges({ details }) {
    let charges = (details && details.order_summary) || {};

    return (
        <Grid container className="order-summary-charges" spacing={3}>
            <Grid item xs={12}>
                <Grid container>
                    <Typography className="order-detail-title textAlignCenter">
                        <FormattedMessage id="PageTitle.order-summary" defaultMessage="Order Summary" />
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    {/** subtotal */}
                    {charges && charges.subtotal_incl_tax && (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-label">
                                    <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-value">
                                    {charges && `${charges.currency} ${charges.subtotal_incl_tax}`}
                                </Typography>
                            </div>
                        </Grid>
                    )}
                    {/** Discount amount */}
                    {charges && charges.discount_amount ? (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-label">
                                    <FormattedMessage id="Cart.Saving" defaultMessage="Saving" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-value">
                                    {charges && `${charges.currency} ${charges.discount_amount}`}
                                </Typography>
                            </div>
                        </Grid>
                    ) : null}
                    {/** Family club saving amount */}
                    {charges && charges.fc_discount_amount && parseFloat(charges.fc_discount_amount) !== 0 ? (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="FamilyClub-myOrder-label">
                                    <FormattedMessage id="Cart.FamilyClubSaving" defaultMessage="Family Club Saving" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="FamilyClub-myOrder-value">
                                    {charges &&
                                        `${charges.currency} ${parseFloat(charges.fc_discount_amount).toFixed(2)}`}
                                </Typography>
                            </div>
                        </Grid>
                    ) : null}
                    {charges && charges.fc_save_more && parseFloat(charges.fc_save_more) !== 0 ? (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="FamilyClub-myOrder-label">
                                    <FormattedMessage
                                        id="Cart.SaveMoreWithFamilyClub"
                                        defaultMessage="Save More With Family Club"
                                    />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="FamilyClub-myOrder-value">
                                    {charges && `${charges.currency} ${parseFloat(charges.fc_save_more).toFixed(2)}`}
                                </Typography>
                            </div>
                        </Grid>
                    ) : null}
                    {/** Shipping */}
                    {charges && charges.shipping_amount && charges.shipping_amount !== 0 ? (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-label">
                                    <FormattedMessage id="Checkout.Cart.Charge.Shipping" defaultMessage="Shipping" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-value">
                                    {charges && `${charges.currency} ${charges.shipping_amount}`}
                                </Typography>
                            </div>
                        </Grid>
                    ) : null}
                    {/** COD */}
                    {charges && charges.cod_charges && charges.cod_charges !== 0 ? (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-label">
                                    <FormattedMessage id="Checkout.Cart.Charge.COD" defaultMessage="COD" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-value">
                                    {charges && `${charges.currency} ${charges.cod_charges}`}
                                </Typography>
                            </div>
                        </Grid>
                    ) : null}
                    {/**Voucher Discount */}
                    {charges && charges.voucher_discount && parseFloat(charges.voucher_discount) !== 0 ? (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-label">
                                    <FormattedMessage
                                        id="Checkout.Cart.Charge.VoucherDiscount"
                                        defaultMessage="Voucher Discount"
                                    />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-value">
                                    {charges && `${charges.currency} ${charges.voucher_discount}`}
                                </Typography>
                            </div>
                        </Grid>
                    ) : null}
                    {/**Vat */}
                    {charges && charges.tax_amount && charges.tax_amount !== 0 ? (
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-label">
                                    <FormattedMessage
                                        id="Checkout.Cart.Charge.VATIncluded"
                                        defaultMessage="VAT Included"
                                    />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-value">
                                    {charges && `${charges.currency} ${charges.tax_amount}`}
                                </Typography>
                            </div>
                        </Grid>
                    ) : null}
                    {/** Line */}
                    <Grid item xs={12}>
                        <div className="border-line padd-t" />
                    </Grid>
                    {/** grand_total */}
                    <Grid item xs={12}>
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-total-label">
                                    <FormattedMessage id="Cart.Total" defaultMessage="Total" />
                                </Typography>
                            </div>
                            <div>
                                {/* className="cartpayment-total-value" */}
                                <Typography className="cartpayment-total-label">
                                    {charges && `${charges.currency} ${charges && charges.grand_total}`}
                                </Typography>
                            </div>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
