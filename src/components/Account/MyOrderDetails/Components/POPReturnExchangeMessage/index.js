import React from 'react';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { useSelector } from 'react-redux';
import DialogActions from '@material-ui/core/DialogActions';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';

function ReturnPOP({ open, history, setOpen }) {
    const state = useSelector((state) => state);

    const global = state && state.auth;
    const redirectToReturnPolicy = () => {
        setOpen(false);
        history.push(`/${global && global.store_locale}/return-policy`);
    };
    let returnPopupMessage = '';

    returnPopupMessage = (
        <>
            <div style={{ display: 'flex' }}>
                <span>
                    <FormattedMessage
                        id="returnTex1"
                        defaultMessage="As per our return policy returns can only be placed within 14 days from the date of delivery. Please"
                    />
                    &nbsp;
                    <span className="click-here" onClick={() => redirectToReturnPolicy()}>
                        <FormattedMessage id="returnTex2" defaultMessage="Click here" />
                        &nbsp;
                    </span>
                    <span>
                        <FormattedMessage id="returnTex3" defaultMessage="to know more about our Return policy." />
                    </span>
                </span>
            </div>
        </>
    );

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogContent>
                        <DialogContentText>
                            <Typography component="span" className="return-pop-text">
                                {returnPopupMessage && returnPopupMessage}
                            </Typography>
                        </DialogContentText>
                    </DialogContent>

                    <DialogActions>
                        <Button
                            className="return-pop-action-ok-button"
                            color="primary"
                            autoFocus
                            onClick={() => handleClose()}
                        >
                            <FormattedMessage id="MyAccount.MyWishlist.Ok" defaultMessage="Ok" />
                        </Button>
                    </DialogActions>
                </Dialog>
            </Grid>
        </Grid>
    );
}

export default withRouter(ReturnPOP);
