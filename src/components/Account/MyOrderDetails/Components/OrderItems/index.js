import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Spinner from 'commonComponet/Spinner';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography';

const OrderItemWebView = lazy(() => import('./Components/WebView'));
const MobileOrderItems = lazy(() => import('./Components/MobileView'));

export default function OrderItem({ details }) {
    const items = (details && details.sub_order_details) || {};

    return (
        <Suspense fallback={<Spinner />}>
            {items &&
                Object.keys(items) &&
                Object.keys(items).map((key, index) => (
                    <Grid
                        container
                        className="boxShadow"
                        spacing={3}
                        style={{ marginBottom: '2rem' }}
                        key={`order_details_card${index}`}
                    >
                        <Grid item xs={12} className="header-of-item">
                            <div className="header-flex">
                                <div>
                                    <Typography className="order-details-header-name">
                                        Shipment {index + 1} Of {Object.keys(items).length}
                                    </Typography>
                                    <Typography className="order-details-product-specification">
                                        <FormattedMessage id="MyAccount.OrderID" defaultMessage="Order Id" />: &nbsp;
                                        {items[key].increment_id}
                                    </Typography>
                                </div>
                                <div className="textAlignRight">
                                    <Typography className="order-details-header-name">
                                        <FormattedMessage
                                            id="Checkout.Payment.DeliveryOption"
                                            defaultMessage="Order Items"
                                        />
                                    </Typography>
                                    <Typography className="order-details-product-specification">
                                        {items[key].delivery_option}
                                    </Typography>
                                </div>
                            </div>
                            {/* <Typography className="order-items-title">
                    <FormattedMessage id="MyAccount.MyOrderDetails.OrderItems" defaultMessage="Order Items" />
                </Typography> */}
                        </Grid>

                        <Hidden only={['xs', 'sm']}>
                            <Grid item xs={12}>
                                <Grid container className="order-list-grid">
                                    <Grid container alignItems="center">
                                        <Grid item xs={7}>
                                            <Typography className="order-item-header-name">
                                                <FormattedMessage id="Cart.ItemNumberStyle" defaultMessage="ITEM" />
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={1}>
                                            <Typography className="order-item-header-name textAlignCenter">
                                                <FormattedMessage id="Cart.Quantity" defaultMessage="QUANTITY" />
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={2}>
                                            <Typography className="order-item-header-name textAlignCenter">
                                                <FormattedMessage id="Cart.Price" defaultMessage="PRICE" />
                                            </Typography>
                                        </Grid>

                                        {/* <Grid item xs={2}>
                                <Typography className="order-details-header-name textAlignCenter">
                                    <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                                </Typography>
                            </Grid> */}

                                        <Grid item xs={2}>
                                            <Typography className="order-item-header-name textAlignCenter">
                                                <FormattedMessage id="MyAccount.Status" defaultMessage="STATUS" />
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid item xs={12}>
                                {items[key].items_ordered &&
                                    items[key].items_ordered.map((item, index) => (
                                        <Grid
                                            container
                                            className="item-order-details-line"
                                            key={`order-details-item_web_${index}`}
                                        >
                                            <OrderItemWebView item={item} />
                                        </Grid>
                                    ))}
                            </Grid>
                        </Hidden>

                        <Hidden only={['md', 'lg', 'xl']}>
                            <Grid item xs={12}>
                                {items[key].items_ordered &&
                                    items[key].items_ordered.map((item, index) => (
                                        <Grid container key={`order-details-item_mobile_${index}`}>
                                            <MobileOrderItems item={item} />
                                        </Grid>
                                    ))}
                            </Grid>
                        </Hidden>
                    </Grid>
                ))}
        </Suspense>
    );
}
