import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { getCartAmount, checkItemIsFree } from 'components/Cart/Basket/utils';
import clsx from 'clsx';
import { returnOrderClassName } from 'components/Account/MyOrder/utils';
import { showCancelOrderId } from 'components/Account/MyOrderDetails/utils';
import moment from 'moment';
export default function OrderItemWebView({ item }) {
    return (
        <Grid container alignItems="center">
            <Grid item xs={7} className="arabic-right">
                <Grid container alignItems="center">
                    <Grid item xs={3}>
                        <img
                            src={item && item.image && item.image[0]}
                            alt="productImage"
                            className="web-order-detail-image"
                        />
                    </Grid>
                    <Grid item xs={9}>
                        <Typography className="order-details-product-name">{item && item.name}</Typography>
                        {/*<Typography className="order-details-product-specification">
                            <FormattedMessage id="PDP.Age" defaultMessage="Age" />
                            :
                        </Typography>*/}
                        <Typography className="order-details-product-specification">
                            <FormattedMessage id="PDP.Specifications.ProductCode" defaultMessage="Product Code" />:
                            &nbsp;{item && item.sku}
                        </Typography>
                        {item && item.returnable_date && !checkItemIsFree(item) && (
                            <Typography className="order-details-product-specification">
                                <FormattedMessage
                                    id="PDP.Specifications.Return"
                                    defaultMessage="Return Validity Date"
                                />
                                : &nbsp;{moment(item && item.returnable_date).format('DD-MM-YYYY')}
                            </Typography>
                        )}
                        {item && item.returnable === 0 && !checkItemIsFree(item) && (
                            <Typography className="order-details-product-specification not-returnable-msg">
                                <FormattedMessage
                                    id="MyAccount.ReturnOrder.NotAvailable"
                                    defaultMessage="This item is not returnable."
                                />
                            </Typography>
                        )}
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={1}>
                <Typography className="qty">{item && item.qty_ordered}</Typography>
            </Grid>

            <Grid item xs={2} className="textAlignCenter">
                {getCartAmount(item)}
            </Grid>

            {/* <Grid item xs={2}>
                {getSubTotalAmountForOrderDetails(item)}
            </Grid> */}

            <Grid item xs={2}>
                <Typography className={clsx('order-details-status', returnOrderClassName(item && item.status))}>
                    {item && item.status}
                </Typography>
            </Grid>

            {showCancelOrderId(item && item.status, item && item.cancel_id) && (
                <Grid item xs={12} className="cancel-id-container">
                    <Typography className="cancle-id-text">
                        <FormattedMessage id="MyAccount.CancelOrder.CancelID" defaultMessage="Cancel ID" />
                        :&nbsp;
                        <span className="cancelId">{item && item.cancel_id}</span>
                    </Typography>
                </Grid>
            )}

            {item &&
                item.return_info &&
                item.return_info.length > 0 &&
                item.return_info.map((returnData, index) => (
                    <Grid container key={`return_order_array_web_${index}`} className="cancel-id-container">
                        <Grid item xs={5} className="arabic-right">
                            <Typography className="cancle-id-text">
                                <FormattedMessage id="MyAccount.ReturnOrder.ReturnID" defaultMessage="Return ID" />
                                :&nbsp;
                                <span className="cancelId">{returnData && returnData.return_request_id}</span>
                            </Typography>
                        </Grid>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={1}>
                            <Typography className="qty cancle-id-text">
                                {returnData && returnData.return_req_qty}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={2} className="arabic-right">
                            <Typography className="cancle-id-text return-color-status">
                                <FormattedMessage
                                    id="MyAccount.ReturnOrder.ReturnStatus"
                                    defaultMessage="Return Status"
                                />
                                :&nbsp;
                                <span className="cancelId">{returnData && returnData.return_item_status}</span>
                            </Typography>
                        </Grid>
                    </Grid>
                ))}
        </Grid>
    );
}
