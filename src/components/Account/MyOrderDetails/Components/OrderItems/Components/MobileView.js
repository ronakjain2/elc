import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { getCartAmount, checkItemIsFree } from 'components/Cart/Basket/utils';
import { returnOrderClassName } from 'components/Account/MyOrder/utils';
import clsx from 'clsx';
import { showCancelOrderId } from 'components/Account/MyOrderDetails/utils';
import moment from 'moment';
export default function MobileOrderItems({ item }) {
    return (
        <Grid container justify="space-between" className="padd-bottom-20">
            <Grid item xs={6} className="arabic-right">
                <img src={item && item.image && item.image[0]} alt="productImage" className="web-order-detail-image" />
            </Grid>

            <Grid item xs={6}>
                <Typography className="order-details-product-name textAlignRight">{item && item.name}</Typography>
                <Typography className="textAlignRight p-t-20">
                    <FormattedMessage id="Cart.Quantity" defaultMessage="Quantity" />
                    :&nbsp;
                    {item && item.qty_ordered}
                </Typography>

                <Grid container justify="space-between">
                    <Grid item xs={12} className="textAlignRight">
                        {getCartAmount(item)}
                    </Grid>
                    <Grid item xs={12}>
                        <Typography
                            className={clsx(
                                ' order-details-mobileView-status textAlignRight',
                                returnOrderClassName(item && item.status),
                            )}
                        >
                            <FormattedMessage id="MyAccount.Status" defaultMessage="Status" />: &nbsp;
                            {item && item.status}
                        </Typography>
                    </Grid>
                </Grid>
                {item && item.returnable_date && !checkItemIsFree(item) && (
                    <Typography className="whitespace-nowrap order-details-product-specification text-align-end">
                        <FormattedMessage id="PDP.Specifications.Return" defaultMessage="Return Validity Date" />:
                        &nbsp;{moment(item && item.returnable_date).format('DD-MM-YYYY')}
                    </Typography>
                )}
                {item && item.returnable === 0 && !checkItemIsFree(item) && (
                    <Typography className="order-details-product-specification not-returnable-msg text-align-end">
                        <FormattedMessage
                            id="MyAccount.ReturnOrder.NotAvailable"
                            defaultMessage="This item is not returnable."
                        />
                    </Typography>
                )}
            </Grid>

            {/* <Grid item xs={3} className="textAlignRight">
                <Grid container justify="space-between" className="height-100">
                    <Grid item xs={12} className="padd-bottom-13">
                        {getCartAmount(item)}
                    </Grid>
                    <Grid item xs={12}>
                        <Typography
                            className={clsx(
                                'p-t-20 order-details-status textAlignRight',
                                returnOrderClassName(item && item.status),
                            )}
                        >
                            {item && item.status}
                        </Typography>
                    </Grid>
                </Grid>
            </Grid> */}

            {showCancelOrderId(item && item.status, item && item.cancel_id) && (
                <Grid item xs={12} className="cancel-id-container">
                    <Typography className="cancle-id-text">
                        <FormattedMessage id="MyAccount.CancelOrder.CancelID" defaultMessage="Cancel ID" />
                        :&nbsp;
                        <span className="cancelId">{item && item.cancel_id}</span>
                    </Typography>
                </Grid>
            )}

            {item &&
                item.return_info &&
                item.return_info.length > 0 &&
                item.return_info.map((returnData, index) => (
                    <Grid container key={`return_order_array_mobile_${index}`} className="cancel-id-container">
                        <Grid item xs={12} sm={5} className="arabic-right">
                            <Typography className="cancle-id-text">
                                <FormattedMessage id="MyAccount.ReturnOrder.ReturnID" defaultMessage="Return ID" />
                                :&nbsp;
                                <span className="cancelId">{returnData && returnData.return_request_id}</span>
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={2}>
                            <Typography className="qty cancle-id-text">
                                <FormattedMessage id="MyAccount.ReturnOrder.ReturnQty" defaultMessage="Return Qty" />
                                :&nbsp;
                                <span className="cancelId">{returnData && returnData.return_req_qty}</span>
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={5} className="arabic-right">
                            <Typography className="cancle-id-text textAlignRight return-color-status">
                                <FormattedMessage
                                    id="MyAccount.ReturnOrder.ReturnStatus"
                                    defaultMessage="Return Status"
                                />
                                :&nbsp;
                                <span className="cancelId">{returnData && returnData.return_item_status}</span>
                            </Typography>
                        </Grid>
                    </Grid>
                ))}
        </Grid>
    );
}
