import React, { Suspense, lazy } from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import { withRouter } from 'react-router';
import { FormattedMessage } from 'react-intl';

const SimpleBreadcrumbs = lazy(() => import('commonComponet/Breadcrumbs'));

function OrderNumber({ orderId, language, global, history }) {
    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: <FormattedMessage id="Home.MyAccount" defaultMessage="My Account" />,
        },
    ];

    const gotoOrderHistory = () => {
        history.push(`/${global && global.store_locale}/order-history`);
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Hidden only={['xs', 'sm']}>
                    <Grid container justify="space-between" alignItems="center">
                        <div>
                            <Button
                                onClick={() => gotoOrderHistory()}
                                className="my-order-details-back-button"
                                startIcon={language === 'en' ? <ChevronLeft /> : <ChevronRight />}
                            >
                                <FormattedMessage id="Checkout.Back" defaultMessage="Back" />
                            </Button>
                        </div>
                        <div>
                            <Typography className="order-id-order-details">
                                <FormattedMessage id="MyAccount.OrderID" defaultMessage="Order ID" />
                                :&nbsp;
                                <span>{orderId}</span>
                            </Typography>
                        </div>
                        <div></div>
                    </Grid>

                    <div className="green-line" />
                </Hidden>

                <Hidden only={['md', 'lg', 'xl']}>
                    <Suspense fallback={'Loading.....'}>
                        <Grid item xs={12}>
                            <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                        </Grid>
                    </Suspense>

                    <Grid container justify="space-between" alignItems="center">
                        <div>
                            <Typography className="order-id-order-details">
                                <FormattedMessage id="MyAccount.OrderID" defaultMessage="Order ID" />
                                :&nbsp;
                                <span>{orderId}</span>
                            </Typography>
                        </div>

                        <div>
                            <Button
                                onClick={() => gotoOrderHistory()}
                                className="my-order-details-back-button"
                                startIcon={language === 'en' ? <ChevronLeft /> : <ChevronRight />}
                            >
                                <FormattedMessage id="Checkout.Back" defaultMessage="Back" />
                            </Button>
                        </div>
                    </Grid>

                    <div className="gray-line" />
                </Hidden>
            </Grid>
        </Grid>
    );
}

export default withRouter(OrderNumber);
