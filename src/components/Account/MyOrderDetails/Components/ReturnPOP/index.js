import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { FormattedMessage } from 'react-intl';

export default function ReturnPOP({ open, setOpen, message }) {
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogContent>
                        <DialogContentText>
                            <Typography component="span" className="return-pop-text">
                                {message}
                            </Typography>
                        </DialogContentText>
                    </DialogContent>

                    <DialogActions>
                        <Button
                            className="return-pop-action-ok-button"
                            color="primary"
                            autoFocus
                            onClick={() => handleClose()}
                        >
                            <FormattedMessage id="MyAccount.MyWishlist.Ok" defaultMessage="Ok" />
                        </Button>
                    </DialogActions>
                </Dialog>
            </Grid>
        </Grid>
    );
}
