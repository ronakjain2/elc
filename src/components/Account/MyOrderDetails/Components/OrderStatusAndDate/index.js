import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import clsx from 'clsx';
import { returnOrderClassName } from 'components/Account/MyOrder/utils';
import moment from 'moment';

export default function OrderDetailStatusAndDate({ details }) {
    return (
        <Grid container>
            <Grid item xs={12} className="arabic-right">
                <Typography className="order-status">
                    <FormattedMessage id="MyAccount.MyOrderDetails.OrderStatus" defaultMessage="Order status" />
                </Typography>
            </Grid>

            <Grid item xs={12} className="arabic-right">
                <Typography
                    className={clsx(
                        'order-status-value',
                        returnOrderClassName(details && details.order_summary && details.order_summary.order_status),
                    )}
                >
                    {details && details.order_summary && details.order_summary.order_status}
                </Typography>
            </Grid>

            <Grid item xs={12} className="arabic-right">
                <Typography className="order-date-and-time">
                    <FormattedMessage id="MyAccount.MyOrderDetails.OrderPlacedOn" defaultMessage="Order placed on" />
                    :&nbsp;
                    <span className="value">
                        {moment(details && details.order_summary && details.order_summary.created_at).format(
                            'DD-MM-YYYY',
                        )}
                    </span>
                </Typography>
            </Grid>

            <div className="green-line padd-top-30" />
        </Grid>
    );
}
