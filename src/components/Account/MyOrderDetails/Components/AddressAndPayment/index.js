import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { replaceTilCharacterToComma } from 'components/Cart/Checkout/Delivery/utils';

export default function AddressAndPaymentOrderDetail({ details }) {
    const shipping_address = details && details.shipping_address;

    const addressDetails = () => {
        return (
            <>
                <div>
                    {/* className="address-name" */}
                    <Typography className="address-lines">
                        {shipping_address &&
                            shipping_address.firstname &&
                            `${shipping_address.firstname} ${shipping_address.lastname}`}
                    </Typography>
                    <Typography className="address-lines">
                        {shipping_address &&
                            shipping_address.street &&
                            replaceTilCharacterToComma(shipping_address.street)}
                    </Typography>
                    <Typography className="address-lines">{shipping_address && shipping_address.city}</Typography>
                    <Typography className="address-lines padd-bottom-30 dir-ltr">
                        {shipping_address &&
                            shipping_address.telephone &&
                            `+${shipping_address.carrier_code} ${shipping_address.telephone}`}
                    </Typography>
                </div>
            </>
        );
    };

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12} className="arabic-right order-summary-charges">
                <Typography className="order-detail-title">
                    <FormattedMessage id="ContactUs.payment" defaultMessage="Payment" />
                </Typography>
                <Typography className="address-lines padd-bottom-30">{details && details.payment_method}</Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12} className="arabic-right order-summary-charges">
                <Typography className="order-detail-title">
                    <FormattedMessage id="Checkout.Delivery.DeliveryAddress" defaultMessage="Delivery Address" />
                </Typography>
                {addressDetails()}
            </Grid>
            <Grid item xs={12} md={12} lg={12} xl={12} className="arabic-right order-summary-charges">
                <Typography className="order-detail-title">
                    <FormattedMessage id="MyAccount.MyOrderDetails.BillingAddress" defaultMessage="Billing Address" />
                </Typography>
                {addressDetails()}
            </Grid>
            {/* <Grid item xs={12} md={12} lg={12} xl={12} className="arabic-right order-summary-charges">
                <Typography className="order-detail-title">
                    <FormattedMessage id="MyAccount.MyOrderDetails.DeliveryType" defaultMessage="Delivery Type" />
                </Typography>
                <Typography className="payment-method padd-bottom-30">{details && details.shipping_type}</Typography>
            </Grid> */}
            {/* <div className="green-line padd-top-30" /> */}
        </Grid>
    );
}
