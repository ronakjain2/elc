import { checkItemIsFree } from 'components/Cart/Basket/utils';
import { DeliveryOptions } from 'services/DeliveryOptions';
import { orderStatus } from 'services/OrderStatus';
import { paymentTypes } from 'services/PayamentTypes';

// export const isShowReturnButton = status => {
//     if (status) {
//         if (
//             orderStatus.delivered.toLowerCase().replace(/ /g, '') === status.toLowerCase().replace(/ /g, '') ||
//             orderStatus.complete.toLowerCase().replace(/ /g, '') === status.toLowerCase().replace(/ /g, '')
//         ) {
//             return true;
//         }
//     }

//     return false;
// };
export const isShowReturnButton = shpping => {
    let flag = false;
    if (shpping) {
        shpping &&
            Object.keys(shpping) &&
            Object.keys(shpping).map(key => {
                const item = (shpping[key] && shpping[key].items_ordered) || [];
                for (let i = 0; i < item.length; i++) {
                    const status = item[i].status;
                    if (
                        orderStatus.delivered.toLowerCase().replace(/ /g, '') === status.toLowerCase().replace(/ /g, '')
                    ) {
                        flag = true;
                    }
                }

                return flag;
            });
    }

    return flag;
};

export const isShowCancelButton = shpping => {
    let flag = false;
    if (shpping) {
        shpping &&
            Object.keys(shpping) &&
            Object.keys(shpping).map(key => {
                const item = (shpping[key] && shpping[key].items_ordered) || [];
                for (let i = 0; i < item.length; i++) {
                    const status = item[i].status;
                    if (
                        orderStatus.Accepted.toLowerCase().replace(/ /g, '') === status.toLowerCase().replace(/ /g, '')
                    ) {
                        flag = true;
                    }
                }

                return flag;
            });
    }

    return flag;
};

export const showCancelOrderId = (status, cancel_id) => {
    if (
        orderStatus.Cancelled.toLowerCase().replace(/ /g, '') === status.toLowerCase().replace(/ /g, '') &&
        cancel_id !== undefined &&
        cancel_id !== null &&
        cancel_id !== ''
    ) {
        return true;
    }

    return false;
};

export const showReturnPOPCOD = order => {
    if (!order) return true;
    const { payment_method } = order;
    if (!payment_method) return true;
    if (payment_method.toLowerCase().replace(/ /g, '') === paymentTypes.CashOnDelivery.toLowerCase().replace(/ /g, ''))
        return true;
    return false;
};
export const showReturnPOPCC = order => {
    if (!order) return true;
    const { shipping_type } = order;
    if (!shipping_type) return true;
    if (shipping_type.toLowerCase().replace(/ /g, '') === DeliveryOptions.ClickCollect.toLowerCase().replace(/ /g, ''))
        return true;
    return false;
};

export const checkAllProductsAreNonReturnable = shipping => {
    let flag = true;
    shipping &&
        Object.keys(shipping) &&
        Object.keys(shipping).forEach(key => {
            const item = (shipping[key] && shipping[key].items_ordered) || [];
            for (let i = 0; i < item.length; i++) {
                if (checkItemIsFree(item[i])) continue;
                const returnable = item[i].returnable;
                if (returnable === 1) {
                    flag = false;
                }
                if (!flag) {
                    break;
                }
            }
            if (!flag) {
                return false;
            }
        });

    return flag;
};
