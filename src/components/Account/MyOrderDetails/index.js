import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Spinner from 'commonComponet/Spinner';
import { actions as myOrderActions } from 'components/Account/MyOrder/redux/actions';
import { createEncryptOrderId, getOrderId } from 'components/Cart/Checkout/utils';
import React, { lazy, Suspense, useEffect, useRef, useState } from 'react';
import { defineMessages, FormattedMessage, injectIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import POPReturnExchangeMessage from './Components/POPReturnExchangeMessage';
import ReturnPOP from './Components/ReturnPOP';
import './myOrderDetails.css';
import {
    checkAllProductsAreNonReturnable,
    isShowCancelButton,
    isShowReturnButton,
    showReturnPOPCC,
    showReturnPOPCOD
} from './utils';
const AccountTab = lazy(() => import('../Components/AccountTab'));
const OrderNumber = lazy(() => import('./Components/OrderNumber'));
const AddressAndPaymentOrderDetail = lazy(() => import('./Components/AddressAndPayment'));
const OrderItem = lazy(() => import('./Components/OrderItems'));
const OrderDetailsCharges = lazy(() => import('./Components/Charges'));

const messages = defineMessages({
    ClickCollectMessage: {
        id: 'MyAccount.ReturnOrder.ReturnPOP',
        defaultMessage: 'Please visit the nearest ELC store to initiate returns for C&C orders',
    },
    CodMessage: {
        id: 'MyAccount.ReturnOrder.CodMessage',
        defaultMessage: 'Please contact customer service to perform your return',
    },
    OrderNonReturnable: {
        id: 'MyAccount.ReturnOrder.notReturnable',
        defaultMessage: 'The order is not returnable because all items of an order are not returnable',
    },
});

function MyOrderDetails({ history, intl }) {
    const state = useSelector(state => state);
    const dispatch = useDispatch();
    const formatMessage = intl.formatMessage;

    const query = new URLSearchParams(window.location.search);
    const order_id = getOrderId(query.get('orderId'));
    const orderIncrementId = getOrderId(query.get('order_increment_id'));

    const language = state && state.auth && state.auth.language;
    const global = state && state.auth;
    const store_id = state && state.auth && state.auth.currentStore;
    const loader = state && state.myOrder && state.myOrder.loader;

    const details = state && state.myOrder && state.myOrder.orderDetails;

    const returnablemessage =
        details &&
        details.order_summary &&
        details.order_summary.returnable_msg &&
        details.order_summary.returnable_msg;

    const [open, setOpen] = useState(false);
    const [openReturnExchangeMessage, setOpenReturnExchangeMessage] = useState(false);

    useEffect(() => {
        dispatch(
            myOrderActions.getOrderDetailRequest({
                orderEntityid: order_id,
                store_id: store_id,
                order_id: orderIncrementId,
            }),
        );
    }, [dispatch, order_id, store_id, orderIncrementId]);

    const message = useRef(formatMessage(messages.CodMessage));
    const gotoReturnOrder = () => {
        if (returnablemessage === 0) {
            if (checkAllProductsAreNonReturnable(details && details.sub_order_details)) {
                message.current = formatMessage(messages.OrderNonReturnable);
                setOpen(true);
            } else if (showReturnPOPCOD(details)) {
                message.current = formatMessage(messages.CodMessage);
                setOpen(true);
            } else if (showReturnPOPCC(details)) {
                message.current = formatMessage(messages.ClickCollectMessage);
                setOpen(true);
            } else {
                history.push(
                    `/${global && global.store_locale}/return-order?orderId=${createEncryptOrderId(
                        order_id,
                    )}&order_increment_id=${createEncryptOrderId(orderIncrementId)}`,
                );
            }
        }

        if (returnablemessage === 1) {
            setOpenReturnExchangeMessage(true);
        }
    };

    const gotoCancelOrder = () => {
        history.push(
            `/${global && global.store_locale}/cancel-order?orderId=${createEncryptOrderId(
                order_id,
            )}&order_increment_id=${createEncryptOrderId(orderIncrementId)}`,
        );
    };

    return (
        <Suspense fallback={<Spinner />}>
            <ReturnPOP open={open} setOpen={setOpen} message={message.current} />
            {returnablemessage === 1 && (
                <POPReturnExchangeMessage open={openReturnExchangeMessage} setOpen={setOpenReturnExchangeMessage} />
            )}
            <Grid container>
                <Grid container justify="center" className="my-order-details-container">
                    {!loader && (
                        <Grid item xs={11}>
                            <Grid container spacing={5} justify="center" className="padding-10">
                                <Hidden only={['xs', 'sm']}>
                                    {/** Tabs */}
                                    <Grid item xs={12}>
                                        <AccountTab activeKey="MyOrder" />
                                    </Grid>
                                </Hidden>

                                {/* Mobile view */}
                                <Hidden only={['md', 'lg', 'xl']}>
                                    <Grid item xs={12}>
                                        <OrderNumber orderId={orderIncrementId} language={language} global={global} />
                                    </Grid>

                                    {/* <Grid item xs={12}>
                                    <OrderDetailStatusAndDate details={details} />
                                </Grid> */}

                                    <Grid item xs={12}>
                                        <AddressAndPaymentOrderDetail details={details} />
                                    </Grid>

                                    <Grid item xs={12}>
                                        <OrderItem details={details} />
                                    </Grid>

                                    <Grid item xs={12}>
                                        <OrderDetailsCharges details={details} />
                                    </Grid>
                                </Hidden>

                                {/* Web view */}
                                <Hidden only={['xs', 'sm']}>
                                    {/* <Grid item xs={12}>
                                        <OrderNumber orderId={order_increment_id} language={language} global={global} />
                                    </Grid> */}
                                    <Grid item xs={8}>
                                        <OrderItem details={details} />
                                    </Grid>
                                    {/* <Grid item xs={12}>
                                            <OrderDetailStatusAndDate details={details} />
                                        </Grid> */}

                                    <Grid item xs={3}>
                                        <AddressAndPaymentOrderDetail details={details} />
                                        <OrderDetailsCharges details={details} />
                                    </Grid>

                                    {/* <Grid item xs={12}>
                                        <OrderDetailsCharges details={details} />
                                    </Grid> */}
                                </Hidden>
                                {isShowReturnButton(details && details.sub_order_details) && (
                                    <Grid item xs={12} className="textAlignCenter">
                                        <Button className="returnExchange-button" onClick={() => gotoReturnOrder()}>
                                            <FormattedMessage
                                                id="MyAccount.OrderDetails.ReturnExchange"
                                                defaultMessage="Return / Exchange"
                                            />
                                        </Button>
                                    </Grid>
                                )}

                                {isShowCancelButton(details && details.sub_order_details) && (
                                    <Grid item xs={12} className="textAlignCenter">
                                        <Button className="returnExchange-button" onClick={() => gotoCancelOrder()}>
                                            <FormattedMessage
                                                id="MyAccount.CancelOrder.CancelItems"
                                                defaultMessage="Cancel Items"
                                            />
                                        </Button>
                                    </Grid>
                                )}
                            </Grid>
                        </Grid>
                    )}

                    {loader && (
                        <Grid container alignItems="center">
                            <Spinner />
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </Suspense>
    );
}

export default withRouter(injectIntl(MyOrderDetails));
