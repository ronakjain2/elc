import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormattedMessage } from 'react-intl';
import { checkPasswordContain } from 'components/SignIn_SignUp/utils';
import Button from '@material-ui/core/Button';
import { useSelector, useDispatch } from 'react-redux';
import { actions as myProfileActions } from 'components/Account/MyProfile/redux/actions';

export default function ChangePassword() {
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const loader = state && state.myProfile && state.myProfile.loader;
    const customerid = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_id;

    const [values, setValues] = useState({
        confirmPassword: '',
        newPassword: '',
    });
    const [isValidate, setIsvalidate] = useState(false);

    const onChangeHandler = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value });
    };

    const ChangePassword = () => {
        setIsvalidate(true);
        if (
            values.confirmPassword === '' ||
            values.newPassword === '' ||
            values.newPassword.length < 8 ||
            !checkPasswordContain(values.newPassword) ||
            values.confirmPassword !== values.newPassword
        ) {
            return;
        }
        setIsvalidate(false);
        dispatch(
            myProfileActions.changePasswordRequest({
                newpassword: values.newPassword,
                customerid,
            }),
        );
        setValues({
            confirmPassword: '',
            newPassword: '',
        });
    };

    return (
        <Grid container>
            {/** Title */}
            <Grid item xs={12}>
                <Grid container justify="space-between">
                    <Typography className="title-Page">
                        <FormattedMessage id="MyAccount.MyProfile.ChangePassword" defaultMessage="Change Password" />
                    </Typography>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={12} md={4} className="padding-t-input-box">
                    <Typography className="login-sigup-input-text">
                        <FormattedMessage
                            id="MyAccount.MyProfile.NewPassword.Required"
                            defaultMessage="New Password*"
                        />
                    </Typography>
                    <TextField
                        variant="outlined"
                        value={values.newPassword}
                        onChange={onChangeHandler('newPassword')}
                        fullWidth
                        type="password"
                        className="inputBox"
                        error={isValidate && values.newPassword === ''}
                    />
                    {/** Password empty message */}
                    {isValidate && values.newPassword === '' && (
                        <FormHelperText className="input-error">
                            <FormattedMessage id="SignInSignUp.Password.Empty" defaultMessage="Please enter password" />
                        </FormHelperText>
                    )}
                    {/** Password length less than 8 message */}
                    {isValidate && values.newPassword !== '' && values.newPassword.length < 8 && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="SignInSignUp.Password.length"
                                defaultMessage="Minimum length is 8 characters"
                            />
                        </FormHelperText>
                    )}
                    <FormHelperText
                        className={
                            isValidate &&
                            values.newPassword !== '' &&
                            values.newPassword.length >= 8 &&
                            !checkPasswordContain(values.newPassword)
                                ? 'password-strong-message input-error'
                                : 'password-strong-message'
                        }
                    >
                        <FormattedMessage
                            id="SignInSignUp.password.strong.message"
                            defaultMessage="Password must be at least 8 characters long and contain an uppercase letter, a lowercase letter and a number."
                        />
                    </FormHelperText>
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12} md={4} className="padding-t-input-box">
                    <Typography className="login-sigup-input-text">
                        <FormattedMessage id="SignInSignUp.ConfirmPassword" defaultMessage="Confirm Password*" />
                    </Typography>
                    <TextField
                        variant="outlined"
                        value={values.confirmPassword}
                        onChange={onChangeHandler('confirmPassword')}
                        fullWidth
                        type="password"
                        className="inputBox"
                        error={isValidate && values.confirmPassword === ''}
                    />
                    {/** Password empty message */}
                    {isValidate && values.confirmPassword === '' && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="SignInSignUp.ConfirmPassword.Empty"
                                defaultMessage="Please enter password again"
                            />
                        </FormHelperText>
                    )}
                    {isValidate && values.confirmPassword !== '' && values.confirmPassword !== values.newPassword && (
                        <FormHelperText className="input-error">
                            <FormattedMessage
                                id="SignInSignUp.ConfirmPassword.NotSame"
                                defaultMessage="Password and Confirm password must be same"
                            />
                        </FormHelperText>
                    )}
                </Grid>
            </Grid>

            <Grid container>
                {!loader && (
                    <Button className="update-password-button" onClick={() => ChangePassword()}>
                        <FormattedMessage id="MyAccount.MyProfile.UpdatePassword" defaultMessage="Update Password" />
                    </Button>
                )}
                {loader && <CircularProgress />}
            </Grid>
        </Grid>
    );
}
