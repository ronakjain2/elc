import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

export default function UserDetails() {
    const state = useSelector((state) => state);

    const loginUser = state && state.auth && state.auth.loginUser;
    const carrier_code =
        (loginUser && loginUser.carrier_code) || (state && state.myAddresses && state.myAddresses.carrier_code) || '';

    return (
        <Grid container>
            {/** Title */}
            <Grid item xs={12}>
                <Grid container justify="space-between">
                    <Typography className="title-Page">
                        <FormattedMessage id="Checkout.Delivery.Pickup.YourDetails" defaultMessage="Your Details" />
                    </Typography>
                </Grid>
            </Grid>

            <Grid item xs={12} className="arabic-right">
                <Typography className="field-title">
                    <FormattedMessage id="MyAccount.UserName.Name" defaultMessage="Name" />
                </Typography>
                <Typography className="field-value">
                    {loginUser && loginUser.firstname && `${loginUser.firstname} ${loginUser.lastname}`}
                </Typography>
            </Grid>

            <Grid item xs={12} className="arabic-right">
                <Typography className="field-title">
                    <FormattedMessage id="MyAccount.ContactNumber" defaultMessage="Contact Number" />
                </Typography>
                <Typography className="field-value dir-ltr">
                    {loginUser && loginUser.phone_number && `+${carrier_code} ${loginUser.phone_number}`}
                </Typography>
            </Grid>

            <Grid item xs={12} className="arabic-right">
                <Typography className="field-title">
                    <FormattedMessage id="MyAccount.Email" defaultMessage="Email" />
                </Typography>
                <Typography className="field-value">{loginUser && loginUser.email}</Typography>
            </Grid>
        </Grid>
    );
}
