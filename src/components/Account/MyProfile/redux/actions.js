import { createAction } from 'redux-actions';

//action types
const CHANGE_PASSWORD_REQUEST = 'ELC@OROB/CHANGE_PASSWORD_REQUEST';
const CHANGE_PASSWORD_REQUEST_SUCCESS = 'ELC@OROB/CHANGE_PASSWORD_REQUEST_SUCCESS';
const CHANGE_PASSWORD_REQUEST_FAILED = 'ELC@OROB/CHANGE_PASSWORD_REQUEST_FAILED';

//methods
const changePasswordRequest = createAction(CHANGE_PASSWORD_REQUEST);
const changePasswordRequestSuccess = createAction(CHANGE_PASSWORD_REQUEST_SUCCESS);
const changePasswordRequestFailed = createAction(CHANGE_PASSWORD_REQUEST_FAILED);

export const actions = {
    changePasswordRequest,
    changePasswordRequestSuccess,
    changePasswordRequestFailed,
};

export const types = {
    CHANGE_PASSWORD_REQUEST,
    CHANGE_PASSWORD_REQUEST_SUCCESS,
    CHANGE_PASSWORD_REQUEST_FAILED,
};
