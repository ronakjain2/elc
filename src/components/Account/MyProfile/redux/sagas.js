import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../../api';
import { toast } from 'react-toastify';

const changePasswordReq = function* changePasswordReq({ payload }) {
    try {
        const { data } = yield call(api.changePassword, payload);
        if (data && data.status) {
            toast.success(data.message);
            yield put(actions.changePasswordRequestSuccess());
        } else {
            toast.error(data.message || 'Something went to wrong, Please try after sometime');
            yield put(actions.changePasswordRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.changePasswordRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.CHANGE_PASSWORD_REQUEST, changePasswordReq);
}
