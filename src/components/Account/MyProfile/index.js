import React, { Suspense, lazy } from 'react';
import './MyProfile.css';
import Grid from '@material-ui/core/Grid';
import { withRouter } from 'react-router';
import Hidden from '@material-ui/core/Hidden';
import Spinner from 'commonComponet/Spinner';
import { FormattedMessage } from 'react-intl';

const AccountTab = lazy(() => import('../Components/AccountTab'));
const UserDetails = lazy(() => import('./Components/UserDetails'));
const ChangePassword = lazy(() => import('./Components/ChangePassword'));
const MobilePageHeader = lazy(() => import('../Components/MobilePageHeader'));

function MyProfile({ history }) {
    return (
        <Suspense fallback={<Spinner />}>
            <Grid container justify="center" className="my-profile-container">
                <Hidden only={['xs', 'sm']}>
                    <Grid item xs={11}>
                        <Grid container>
                            <Grid item xs={12}>
                                <AccountTab activeKey="MyProfile" />
                            </Grid>

                            <Grid item xs={12}>
                                <UserDetails />
                            </Grid>

                            <Grid item xs={12}>
                                <div className="border-line-green" />
                            </Grid>

                            <Grid item xs={12}>
                                <ChangePassword />
                            </Grid>
                        </Grid>
                    </Grid>
                </Hidden>

                <Hidden only={['md', 'lg', 'xl']}>
                    <Grid item xs={11}>
                        <Grid container>
                            <Grid item xs={12}>
                                <MobilePageHeader
                                    title={
                                        <FormattedMessage id="MyAccount.MyProfile.Text" defaultMessage="My Profile" />
                                    }
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <UserDetails />
                            </Grid>

                            <Grid item xs={12}>
                                <div className="border-line-green" />
                            </Grid>

                            <Grid item xs={12}>
                                <ChangePassword />
                            </Grid>
                        </Grid>
                    </Grid>
                </Hidden>
            </Grid>
        </Suspense>
    );
}

export default withRouter(MyProfile);
