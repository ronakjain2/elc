import React, { lazy, Suspense, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { actions as PDPActions } from 'components/PDP/redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import Spinner from 'commonComponet/Spinner';
import './css/PDP.css';
import { FormattedMessage } from 'react-intl';

const SimpleBreadcrumbs = lazy(() => import('commonComponet/Breadcrumbs'));
const Details = lazy(() => import('./components/Details'));
const YouMayAlsoLike = lazy(() => import('./components/YouMayAlsoLike'));
const MetaData = lazy(() => import('./components/MetaData'));
const ProductImgae = lazy(() => import('./components/ProductImage'));

export default function PDP({ history }) {
    const url_key = useParams().category;

    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const storeId = state && state.auth && state.auth.currentStore;
    const category_id = state && state.auth && state.auth.category_id;

    useEffect(() => {
        try {
            dispatch(
                PDPActions.getPDPRequest({
                    url_key,
                    storeId: storeId,
                }),
            );
        } catch (err) {}
    }, [dispatch, storeId, url_key]);

    useEffect(() => {
        dispatch(
            PDPActions.getProductSuggestionRequest({
                category_id: category_id,
                store_id: storeId,
                count: 6,
            }),
        );
    }, [dispatch, category_id, storeId]);

    const product = state && state.PDP && state.PDP.details;
    const global = state && state.auth;

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: product && product.category_names,
            url: `/${global && global.store_locale}/products/${product && product.category_path}`,
        },
        {
            name: product && product.short_description,
        },
    ];

    return (
        <Suspense fallback={<Spinner />}>
            {state && state.PDP && !state.PDP.error && (
                <Grid container className="mainPDP">
                    {state && state.PDP && !state.PDP.loader && (
                        <Grid item xs={12}>
                            <Grid container justify="center">
                                {/** MetaData */}
                                <MetaData productDetails={product} />
                                {/** End MetaData */}

                                {/** Breadcrumbs */}
                                <Grid item xs={12} md={12}>
                                    <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                                </Grid>
                                {/** End Breadcrumbs */}

                                <Grid item xs={12}>
                                    <Grid container>
                                        <Grid item xs={12} md={6}>
                                            {/** Product Images */}
                                            <ProductImgae />
                                            {/** End Product Images */}
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            {/** Product Details */}
                                            <Details history={history} />
                                            {/** End Product Details */}
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    )}

                    {/** Spinner */}
                    {state && state.PDP && state.PDP.loader && (
                        <Grid container alignItems="center">
                            <Spinner />
                        </Grid>
                    )}
                    {/** End Spinner */}
                </Grid>
            )}

            {state && state.PDP && !state.PDP.loader && !state.PDP.error && (
                <Grid item xs={12}>
                    <YouMayAlsoLike />
                </Grid>
            )}

            {state && state.PDP && state.PDP.error && (
                <Grid container justify="center" alignItems="center">
                    <Typography className="PDPError">
                        {state.PDP.message ? (
                            state.PDP.message
                        ) : (
                            <FormattedMessage
                                id="PDP.NoProductDetails"
                                defaultMessage="Product Details not Available"
                            />
                        )}
                    </Typography>
                </Grid>
            )}
        </Suspense>
    );
}
