import React from 'react';

export const showDiscountPriceOnPDP = (data) => {
    const offerData = data && data.offers && data.offers.data;
    const offerstatus = data && data.offers && data.offers.status;
    if (offerData && offerstatus === 1) {
        if (Object.keys(offerData).length === 1) {
            for (const value in offerData) {
                if (value === '1') {
                    return (
                        <>
                            <span>{`${data.currency} ${
                                offerData[value] && parseFloat(offerData[value]).toFixed(2)
                            } `}</span>
                            <span className="amt_strike">{`${data.currency} ${
                                data.price && parseFloat(data.price).toFixed(2)
                            }`}</span>
                        </>
                    );
                }
                return <span>{`${data.currency} ${data.price}`}</span>;
            }
        } else {
            return <span>{`${data.currency} ${data.price && data.price.toFixed(2)}`}</span>;
        }
    } else {
        return <span>{`${data.currency} ${data.price && data.price.toFixed(2)}`}</span>;
    }
};

export const showFCDiscountPriceOnPDP = (data) => {
    const offerFCData = data && data.offers && data.offers.fc_data;
    //const offerData = data && data.offers && data.offers.data;
    const offerstatus = data && data.offers && data.offers.status;
    if (offerFCData && offerstatus === 1) {
        if (Object.keys(offerFCData).length === 1) {
            for (const value in offerFCData) {
                if (value === '1') {
                    return (
                        <>
                            <span>{`${data.currency} ${
                                offerFCData[value] && `${parseFloat(offerFCData[value]).toFixed(2)}`
                            } `}</span>
                            <span className="amt_strike">{`${data.currency} ${data.price.toFixed(2)}`}</span>
                        </>
                    );
                }
                return <span>{`${data.currency} ${data.price && data.price.toFixed(2)}`}</span>;
            }
        } else {
            return <span>{`${data.currency} ${data.price && data.price.toFixed(2)}`}</span>;
        }
    } else {
        return <span>{`${data.currency} ${data.price && data.price.toFixed(2)}`}</span>;
    }
};

export const showDiscountPriceOnPDPProductSuggestion = (data) => {
    const offerData = data && data.offers && data.offers.data;
    const offerstatus = data && data.offers && data.offers.status;
    if (offerData && offerstatus === 1) {
        if (Object.keys(offerData).length === 1) {
            for (const value in offerData) {
                if (value === '1') {
                    return (
                        <>
                            <span>{`${data.currency} ${offerData[value] && `${offerData[value]}`} `}</span>
                            <span className="amt_strike">{`${data.currency} ${data.price && `${data.price}`}`}</span>
                        </>
                    );
                }
                return <span>{`${data.currency} ${data.price}`}</span>;
            }
        } else {
            return <span>{`${data.currency} ${data.price && `${data.price}`}`}</span>;
        }
    } else {
        return <span>{`${data.currency} ${data.price && `${data.price}`}`}</span>;
    }
};

export const getSpecificationClassName = (value) => {
    if (!value || value === '' || value === 'NA') {
        return 'spValueNotAvailable';
    }

    return 'spValueAvailable';
};

export const checkMoreOfferAvailableOrNot = (productDetails) => {
    const offers = productDetails && productDetails.offers;
    if (offers && offers.status === 1) {
        if (offers.data && Object.keys(offers.data).length > 1) {
            return true;
        }
    }

    return false;
};

export const isDisableAddToCart = (qty, stockQty) => {
    if (qty === 0 || stockQty <= 0 || qty > stockQty) {
        return true;
    }
    return false;
};
