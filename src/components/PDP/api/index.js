import api from 'apiServices';

const getPDP = (url_key, store_id) =>
    api.get(`/index.php/rest/V1/app/mkt/productbyid/?store_id=${store_id}&url_key=${url_key}`);

const guestAddToCart = (payload, storeName, guestId) =>
    api.post(`/index.php/rest/${storeName}/V1/guest-carts/${guestId}/items`, payload);
const loginAddToCart = (payload, storeName) => api.post(`/index.php/rest/${storeName}/V1/carts/mine/items`, payload);
const getProductSuggestion = (payload) => api.post('/index.php/rest/V1/app/productsuggestions', payload);

export default {
    getPDP,
    guestAddToCart,
    loginAddToCart,
    getProductSuggestion,
};
