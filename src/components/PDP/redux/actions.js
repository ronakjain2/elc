import { createAction } from 'redux-actions';

// action types
const GET_PDP_REQUEST = 'ELC@OROB/GET_PDP_REQUEST';
const GET_PDP_REQUEST_SUCCESS = 'ELC@OROB/GET_PDP_REQUEST_SUCCESS';
const GET_PDP_REQUEST_FAILED = 'ELC@OROB/GET_PDP_REQUEST_FAILED';

const ADD_TO_CART_REQUEST = 'ELC@OROB/ADD_TO_CART_REQUEST';
const ADD_TO_CART_REQUEST_SUCCESS = 'ELE@OROB/ADD_TO_CART_REQUEST_SUCCESS';
const ADD_TO_CART_REQUEST_FAILED = 'ELC@OROB/ADD_TO_CART_REQUEST_FAILED';

const GET_PRODUCT_SUGGESTION_REQUEST = 'ELC@OROB/GET_PRODUCT_SUGGESTION_REQUEST';
const GET_PRODUCT_SUGGESTION_REQUEST_SUCCESS = 'ELC@OROB/GET_PRODUCT_SUGGESTION_REQUEST_SUCCESS';
const GET_PRODUCT_SUGGESTION_REQUEST_FAILED = 'ELC@OROB/GET_PRODUCT_SUGGESTION_REQUEST_FAILED';

const SET_SHOW_SESSION_EXPIRED_FOR_LOGIN_USER = 'ELC@OROB/SET_SHOW_SESSION_EXPIRED_FOR_LOGIN_USER';

// actions methods
const getPDPRequest = createAction(GET_PDP_REQUEST);
const getPDPRequestSuccess = createAction(GET_PDP_REQUEST_SUCCESS);
const getPDPRequestFailed = createAction(GET_PDP_REQUEST_FAILED);

const addToCartRequest = createAction(ADD_TO_CART_REQUEST);
const addToCartRequestSuccess = createAction(ADD_TO_CART_REQUEST_SUCCESS);
const addToCartRequestFailed = createAction(ADD_TO_CART_REQUEST_FAILED);

const getProductSuggestionRequest = createAction(GET_PRODUCT_SUGGESTION_REQUEST);
const getProductSuggestionRequestSuccess = createAction(GET_PRODUCT_SUGGESTION_REQUEST_SUCCESS);
const getProductSuggestionRequestFailed = createAction(GET_PRODUCT_SUGGESTION_REQUEST_FAILED);

const setShowSessionExpiredPOPForLoginUser = createAction(SET_SHOW_SESSION_EXPIRED_FOR_LOGIN_USER);

export const actions = {
    getPDPRequest,
    getPDPRequestSuccess,
    getPDPRequestFailed,

    addToCartRequest,
    addToCartRequestSuccess,
    addToCartRequestFailed,

    getProductSuggestionRequest,
    getProductSuggestionRequestSuccess,
    getProductSuggestionRequestFailed,

    setShowSessionExpiredPOPForLoginUser,
};

export const types = {
    GET_PDP_REQUEST,
    GET_PDP_REQUEST_SUCCESS,
    GET_PDP_REQUEST_FAILED,

    ADD_TO_CART_REQUEST,
    ADD_TO_CART_REQUEST_SUCCESS,
    ADD_TO_CART_REQUEST_FAILED,

    GET_PRODUCT_SUGGESTION_REQUEST,
    GET_PRODUCT_SUGGESTION_REQUEST_SUCCESS,
    GET_PRODUCT_SUGGESTION_REQUEST_FAILED,

    SET_SHOW_SESSION_EXPIRED_FOR_LOGIN_USER,
};
