import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandler = {
    [types.GET_PDP_REQUEST]: state => ({
        ...state,
        loader: true,
        error: false,
        message: '',
    }),
    [types.GET_PDP_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loader: false,
        details: {},
        error: true,
        message: payload.message,
    }),
    [types.GET_PDP_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        details: payload && payload.product && payload.product[0],
        error: false,
        message: '',
    }),

    [types.ADD_TO_CART_REQUEST]: state => ({
        ...state,
        addToCartLoader: true,
    }),
    [types.ADD_TO_CART_REQUEST_SUCCESS]: state => ({
        ...state,
        addToCartLoader: false,
    }),
    [types.ADD_TO_CART_REQUEST_FAILED]: state => ({
        ...state,
        addToCartLoader: false,
    }),

    [types.GET_PRODUCT_SUGGESTION_REQUEST]: state => ({
        ...state,
        suggestLoader: true,
    }),
    [types.GET_PRODUCT_SUGGESTION_REQUEST_FAILED]: state => ({
        ...state,
        suggestLoader: false,
        suggestProductList: [],
    }),
    [types.GET_PRODUCT_SUGGESTION_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        suggestLoader: false,
        suggestProductList: payload || [],
    }),

    [types.SET_SHOW_SESSION_EXPIRED_FOR_LOGIN_USER]: (state, { payload }) => ({
        ...state,
        showExpiredPOP: payload.showExpiredPOP,
        message: payload.message,
    }),
};

export default handleActions(actionHandler, {
    loader: false,
    details: {},
    error: false,
    message: '',
    suggestLoader: false,
    suggestProductList: [],
    showExpiredPOP: false,
});
