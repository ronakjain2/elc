import { call, put, takeLatest, select } from 'redux-saga/effects';
import { getStoreName } from 'components/Global/utils';
import { toast } from 'react-toastify';
import { actions as authActions } from 'components/Global/redux/actions';
import { actions as basketActions } from 'components/Cart/Basket/redux/actions';
import api from '../api';
import { actions, types } from './actions';
import { productDetailsEvent, AddToCartEvent } from 'services/GTMservice';
import { actions as myWishlistActions } from 'components/Account/MyWishList/redux/actions';

const auth = state => state && state.auth;

const getPDPReq = function* getPDPReq({ payload }) {
    try {
        const { url_key, storeId } = payload;
        const { data } = yield call(api.getPDP, encodeURIComponent(url_key), storeId);
        if (data && data.status) {
            yield put(actions.getPDPRequestSuccess(data));
            //GTM Product Detail Event
            productDetailsEvent(data.product && data.product[0]);
        } else {
            yield put(actions.getPDPRequestFailed({ message: data.message }));
        }
    } catch (err) {
        yield put(actions.getPDPRequestFailed());
    }
};

const addToCartReq = function* addToCartReq({ payload }) {
    try {
        const authState = yield select(auth);
        const { cart_item } = payload;
        const storeName = yield getStoreName(authState && authState.currentStore);
        if (storeName) {
            if (authState && authState.loginUser) {
                const { data } = yield call(api.loginAddToCart, { cart_item }, storeName);

                if (data && data.quote_id) {
                    yield put(authActions.saveQuoteIdRequest(data.quote_id));
                    toast.success(
                        authState.language === 'en'
                            ? 'Product added to the shopping cart'
                            : 'تم إلاضافة إلى عربة التسوق',
                    );

                    //GTM Add To Cart Event
                    AddToCartEvent({
                        category_names: payload.gtmData && payload.gtmData.category_names,
                        qty: payload.cart_item && payload.cart_item.qty,
                        offers: payload.gtmData && payload.gtmData.offers,
                        sku: payload.cart_item && payload.cart_item.sku,
                        currency: payload.gtmData && payload.gtmData.currency,
                        name: data.name,
                        price: data.price,
                    });

                    yield put(
                        basketActions.getCartCountReq({
                            quote_id: data.quote_id,
                            store_id: authState && authState.currentStore,
                        }),
                    );
                    // Remove from Wishlist
                    if (payload && payload.wishlist_id) {
                        yield put(
                            myWishlistActions.removeWishlistRequest({
                                customerid: authState && authState.loginUser && authState.loginUser.customer_id,
                                store_id: authState && authState.currentStore,
                                wishilistitemid: payload.wishlist_id,
                                dontShowMessage: true,
                            }),
                        );
                    }
                } else {
                    if (!data.status) {
                        if (data.message !== '') {
                            yield put(
                                actions.setShowSessionExpiredPOPForLoginUser({
                                    showExpiredPOP: true,
                                    message: data.message,
                                }),
                            );
                        }
                    } else {
                        toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
                    }
                }
            } else {
                const guestId = authState && authState.guestUser && authState.guestUser.guestId;
                const { data } = yield call(api.guestAddToCart, { cart_item }, storeName, guestId);

                if (data && data.quote_id) {
                    yield put(authActions.saveQuoteIdRequest(data.quote_id));
                    toast.success(
                        authState.language === 'en'
                            ? 'Product added to the shopping cart'
                            : 'تم إلاضافة إلى عربة التسوق',
                    );

                    //GTM Add To Cart Event
                    AddToCartEvent({
                        category_names: payload.gtmData && payload.gtmData.category_names,
                        qty: payload.cart_item && payload.cart_item.qty,
                        offers: payload.gtmData && payload.gtmData.offers,
                        sku: payload.cart_item && payload.cart_item.sku,
                        currency: payload.gtmData && payload.gtmData.currency,
                        name: data.name,
                        price: data.price,
                    });

                    yield put(
                        basketActions.getCartCountReq({
                            quote_id: data.quote_id,
                            store_id: authState && authState.currentStore,
                        }),
                    );
                } else {
                    if (!data.status && data.message !== '') {
                        toast.error('Please try again..!');
                        yield put(authActions.getGuestCartRequest({ storeId: authState && authState.currentStore }));
                    } else {
                        toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
                    }
                }
            }
            yield put(actions.addToCartRequestSuccess());
        }
    } catch (err) {
        if (
            err &&
            err.response &&
            err.response.data &&
            err.response.data.parameters &&
            err.response.data.parameters.fieldName === 'cartId'
        ) {
            const authState = yield select(auth);
            if (authState && authState.loginUser) {
                yield put(
                    actions.setShowSessionExpiredPOPForLoginUser({
                        showExpiredPOP: true,
                        message: 'Please logout & login again.',
                    }),
                );
            } else {
                yield put(authActions.getGuestCartRequest({ storeId: authState && authState.currentStore }));
                toast.error('Please try again..!');
            }
        } else {
            toast.error(
                (err && err.response && err.response.data && err.response.data.message) ||
                    'Something went to wrong, Please try after sometime',
            );
        }

        yield put(actions.addToCartRequestFailed());
    }
};

const getSuggestionProductReq = function* getSuggestionProductReq({ payload }) {
    try {
        const { data } = yield call(api.getProductSuggestion, payload);
        if (data && data.status) {
            yield put(actions.getProductSuggestionRequestSuccess(data.data));
        } else {
            toast.error((data && data.message) || 'Something went to wrong, Please try after sometime');
            yield put(actions.getProductSuggestionRequestFailed());
        }
    } catch (err) {
        toast.error(
            (err && err.response && err.response.data && err.response.data.message) ||
                'Something went to wrong, Please try after sometime',
        );
        yield put(actions.getProductSuggestionRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_PDP_REQUEST, getPDPReq);
    yield takeLatest(types.ADD_TO_CART_REQUEST, addToCartReq);
    yield takeLatest(types.GET_PRODUCT_SUGGESTION_REQUEST, getSuggestionProductReq);
}
