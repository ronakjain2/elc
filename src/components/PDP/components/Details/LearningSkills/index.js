import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import './styles.css';

export default function LearningSkills({ productDetails }) {
    return (
        <Grid container className="LearningSkills">
            <Grid container>
                <Typography className="learning-skill-text">
                    <FormattedMessage id="PDP.LearningSkills" defaultMessage="Learning Skills" />
                </Typography>
            </Grid>
            <Grid container>
                {productDetails &&
                    productDetails.learningSkills &&
                    Object.keys(productDetails.learningSkills).map((skills, index) => {
                        if (
                            skills &&
                            productDetails.learningSkills[skills] &&
                            productDetails.learningSkills[skills] !== ''
                        ) {
                            return (
                                <div key={`learning_skills_${index}`}>
                                    <img
                                        src={`/images/pdp/learningSkills/${productDetails.learningSkills[
                                            skills
                                        ].replace(/ /g, '')}.png`}
                                        alt="skillImgaes"
                                        className="learningSkillImage"
                                    />
                                </div>
                            );
                        }

                        return null;
                    })}
            </Grid>
        </Grid>
    );
}
