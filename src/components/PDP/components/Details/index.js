import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useSelector } from 'react-redux';
import AgeAndShare from './AgeAndShare';
import BasicInfo from './BasicInfo';
import Prices from './Prices';
import AvailableFor from './AvailableFor';
import QuantityAndBasket from './QuantityAndAddBasket';
import LearningSkills from './LearningSkills';
import Specifications from './Specifications';
import BuyMoreSaveMore from './BuyMoreSaveMore';
import { FormattedMessage } from 'react-intl';
import ShowSessionExpiredPOP from './POP';

export default function Details({ history }) {
    const state = useSelector((state) => state);

    const user_data = state && state.auth && state.auth.loginUser;
    const productDetails = state && state.PDP && state.PDP.details;

    return (
        <Grid container>
            <ShowSessionExpiredPOP />
            {/** Age and share functionality */}
            <Grid item xs={12}>
                <AgeAndShare productDetails={productDetails} />
            </Grid>
            {/** Age and share functionality */}

            {/** Basic Info */}
            <Grid item xs={12} md={12}>
                <BasicInfo productDetails={productDetails} />
            </Grid>
            {/** Basic Info End */}

            {/** Prices */}
            <Prices productDetails={productDetails} userData={user_data} history={history} />
            {/** End Prices */}

            {/** Available For */}
            <AvailableFor />
            {/** End Available For */}

            {/** BuyMoreSaveMore */}
            <BuyMoreSaveMore productDetails={productDetails} />
            {/** End BuyMoreSavemore */}

            {/** Qauntity and basket button */}
            {productDetails && productDetails.max_avl_qty > 0 && <QuantityAndBasket productDetails={productDetails} />}
            {/** End Qauntity and basket button */}

            {productDetails && productDetails.max_avl_qty <= 0 && (
                <Grid container alignItems="center">
                    <Typography className="sold-out-pdp">
                        <FormattedMessage id="PLP.SoldOut" defaultMessage="Sold Out" />
                    </Typography>
                </Grid>
            )}

            {/** Learning Skills */}
            <LearningSkills productDetails={productDetails} />
            {/** End Learning Skills */}

            {/** Specifications */}
            <Specifications productDetails={productDetails} />
            {/** End Specification */}
        </Grid>
    );
}
