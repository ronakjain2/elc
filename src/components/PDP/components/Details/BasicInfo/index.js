import Collapse from '@material-ui/core/Collapse';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import './styles.css';

export default function BasicInfo({ productDetails }) {
    const state = useSelector(state => state);
    const store_locale = state && state.auth && state.auth.store_locale;

    const [isMore, setMore] = useState(false);

    return (
        <Grid container className="basicInfo">
            {productDetails && productDetails.labels && (
                <Grid container className="mt-2">
                    <Typography
                        varient="span"
                        className="product-label arabic-right"
                        style={{
                            background: productDetails.label_bg_color || '#aa272f',
                            color: productDetails.label_text_color || '#fff',
                        }}
                    >
                        {productDetails.labels}
                    </Typography>
                </Grid>
            )}
            {/** Product Name */}
            <Grid container>
                <Typography className="productName arabic-right">
                    {productDetails && productDetails.short_description}
                </Typography>
            </Grid>
            {/** Product Name */}

            {/** Brand Name */}
            {productDetails && productDetails.brand_data && productDetails.brand_data.title && (
                <Grid container>
                    <Typography className="shop-all-brand-name arabic-right">
                        <FormattedMessage id="PDP.SeeAllProductsFrom" defaultMessage="See all products from" />
                        &nbsp;
                        <span className="brand-name">
                            <Link to={`/${store_locale}${productDetails.brand_data.url_path}`}>
                                {productDetails.brand_data.title}
                            </Link>
                        </span>
                    </Typography>
                </Grid>
            )}
            {/** Brand Name */}

            {/** Descriptions */}
            <Grid container>
                {productDetails && productDetails.description && productDetails.description.length > 60 && (
                    <>
                        {!isMore && (
                            <Typography className="descriptionText arabic-right">
                                {productDetails.description.substring(0, 60)}
                                ....
                                <span onClick={() => setMore(true)}>
                                    <FormattedMessage id="PDP.ReadMore" defaultMessage="read more" />
                                </span>
                            </Typography>
                        )}
                        {isMore && (
                            <Collapse in={isMore}>
                                <Typography className="descriptionText arabic-right">
                                    {productDetails.description}
                                    &nbsp;
                                    <span onClick={() => setMore(false)}>
                                        <FormattedMessage id="PDP.ReadLess" defaultMessage="read less" />
                                    </span>
                                </Typography>
                            </Collapse>
                        )}
                    </>
                )}
                {productDetails && productDetails.description && productDetails.description.length <= 60 && (
                    <Typography className="descriptionText arabic-right">{productDetails.description}</Typography>
                )}
            </Grid>
            {/** End Descriptions */}
        </Grid>
    );
}
