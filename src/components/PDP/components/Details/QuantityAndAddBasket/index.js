import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { FormattedMessage } from 'react-intl';
import './styles.css';
import Remove from '@material-ui/icons/Remove';
import Add from '@material-ui/icons/Add';
import { isDisableAddToCart } from 'components/PDP/utils';
import { useSelector, useDispatch } from 'react-redux';
import { actions as PDPActions } from 'components/PDP/redux/actions';

export default function QuantityAndBasket({ productDetails }) {
    const [qyt, setQyt] = useState(1);

    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const auth = state && state.auth && state.auth;

    const incrementQyt = () => {
        if (qyt <= parseInt(productDetails && productDetails.max_avl_qty)) {
            setQyt(qyt + 1);
        }
    };

    const decrementQyt = () => {
        if (qyt > 1) {
            setQyt(qyt - 1);
        }
    };

    const addToCart = () => {
        if (qyt > 0) {
            let quote_id = auth && auth.guestUser && auth.guestUser.guestId;
            if (auth && auth.loginUser) {
                quote_id = auth && auth.quote_id;
            }

            dispatch(
                PDPActions.addToCartRequest({
                    cart_item: {
                        product_option: {
                            extension_attributes: {},
                        },
                        product_type: (productDetails && productDetails.type) || 'simple',
                        qty: qyt,
                        quote_id,
                        sku: productDetails && productDetails.sku,
                    },
                    gtmData: {
                        currency: productDetails && productDetails.currency,
                        category_names: productDetails && productDetails.ga_cat_names,
                        offers: productDetails && productDetails.offers,
                    },
                }),
            );
        }
    };

    return (
        <Grid container className="QuantityAndBasket">
            <Grid container>
                <Typography>
                    <FormattedMessage id="PDP.Quantity" defaultMessage="Quantity" />
                </Typography>
            </Grid>
            <Grid container>
                <div className="ar-left-div">
                    <ButtonGroup
                        className="addAndRemove"
                        variant="contained"
                        aria-label="contained primary button group"
                    >
                        <Button className="remove-border-ar" disabled={qyt === 1} onClick={() => decrementQyt()}>
                            <Remove />
                        </Button>
                        <Button className="input" disabled>
                            {qyt}
                        </Button>
                        <Button
                            disabled={qyt >= parseInt(productDetails && productDetails.max_avl_qty)}
                            onClick={() => incrementQyt()}
                        >
                            <Add />
                        </Button>
                    </ButtonGroup>
                    {qyt < parseInt(productDetails && productDetails.max_avl_qty) && productDetails.max_avl_qty > 5 && (
                        <FormHelperText className="InStockMessage">
                            <FormattedMessage id="PDP.InStock" defaultMessage="In Stock" />
                        </FormHelperText>
                    )}

                    {qyt >= parseInt(productDetails && productDetails.max_avl_qty) && (
                        <FormHelperText className="outOfStockMessage">
                            <FormattedMessage
                                id="PDP.Quantity.OutOfStock"
                                defaultMessage="You cannot add more products!"
                            />
                        </FormHelperText>
                    )}

                    {productDetails &&
                        productDetails.max_avl_qty <= 5 &&
                        qyt < parseInt(productDetails && productDetails.max_avl_qty) && (
                            <FormHelperText className="only5InStockMessage">
                                <FormattedMessage id="PDP.Quantity.Only" defaultMessage="Only" />
                                &nbsp;
                                <span>{productDetails.max_avl_qty}</span>
                                &nbsp;
                                <FormattedMessage id="PDP.Quantity.LeftInStock" defaultMessage="remaining in stock" />
                            </FormHelperText>
                        )}
                </div>

                <div>
                    {state && state.PDP && !state.PDP.addToCartLoader && (
                        <Button
                            className="AddToBasket"
                            disabled={isDisableAddToCart(qyt, productDetails && productDetails.max_avl_qty)}
                            onClick={() => addToCart()}
                        >
                            <FormattedMessage id="Product.AddToBasket" defaultMessage="Add to basket" />
                        </Button>
                    )}
                    {state && state.PDP && state.PDP.addToCartLoader && <CircularProgress />}
                </div>
            </Grid>
        </Grid>
    );
}
