import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import './styles.css';
import { checkMoreOfferAvailableOrNot } from 'components/PDP/utils';

export default function BuyMoreSaveMore({ productDetails }) {
    let offers = productDetails && productDetails.offers && productDetails.offers.data;
    offers =
        offers &&
        Object.keys(offers) &&
        Object.keys(offers)
            .filter((key) => parseInt(key) !== 1)
            .map((item) => ({ value: offers[item], key: item }));

    return (
        <>
            {checkMoreOfferAvailableOrNot(productDetails) && offers && offers.length > 0 && (
                <Grid container className="BuyMoreSaveMore">
                    <Grid container>
                        <Typography className="buymore-text">
                            <FormattedMessage id="PDP.BuyMoreSaveMore" defaultMessage="Buy More Save More" />
                        </Typography>
                    </Grid>
                    <Grid container>
                        {offers &&
                            offers.map((item, index) => (
                                <span key={`buymoresavemore_${index}`}>
                                    {index !== 0 && <span className="line" />}
                                    <span className="buymoresavemore-text-value">
                                        Buy {item.key} <FormattedMessage id="PDP.For" defaultMessage="for" />{' '}
                                        {productDetails && productDetails.currency} {item.value}
                                    </span>
                                </span>
                            ))}
                    </Grid>
                </Grid>
            )}
        </>
    );
}
