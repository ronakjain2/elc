import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import './styles.css';
import { FormattedMessage } from 'react-intl';
import Share from '@material-ui/icons/Share';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Favorite from '@material-ui/icons/Favorite';
import ShareOptions from './share';
import { useSelector, useDispatch } from 'react-redux';
import { withRouter } from 'react-router';
import { actions as myWishlistActions } from 'components/Account/MyWishList/redux/actions';

function AgeAndShare({ history, productDetails }) {
    const state = useSelector((state) => state);
    const dispath = useDispatch();

    const wishlistArr = state && state.myWishlist && state.myWishlist.wishlistArr;
    const wishListItems = state && state.myWishlist && state.myWishlist.wishListItems;
    const loginUser = state && state.auth && state.auth.loginUser;
    const store_locale = state && state.auth && state.auth.store_locale;
    const currentStore = state && state.auth && state.auth.currentStore;
    const addLoader = state && state.myWishlist && state.myWishlist.addLoader;
    const quote_id = state && state.auth && state.auth.quote_id;
    const id = productDetails && productDetails.id;
    const index = wishlistArr.indexOf(id);

    const [openShare, setOpenShare] = useState(false);

    const addOrRemoveWishItem = () => {
        const id = productDetails && productDetails.id;
        if (loginUser && loginUser.customer_id) {
            if (index === -1) {
                dispath(
                    myWishlistActions.addWishListItemRequest({
                        customer_id: loginUser.customer_id,
                        product_id: id,
                    }),
                );
            } else {
                let data = wishListItems.filter((d) => d.product_id === id)[0];
                dispath(
                    myWishlistActions.removeWishlistRequest({
                        customerid: loginUser.customer_id,
                        store_id: currentStore,
                        wishilistitemid: data.wishlist_id,
                        quote_id: quote_id,
                    }),
                );
            }
        } else {
            history.push(`/${store_locale}/sign-in-register`);
        }
    };

    return (
        <Grid container justify="space-between" className="ageAndShare" alignItems="center">
            <div className="ageContainer">
                {productDetails && productDetails.age && (
                    <>
                        <FormattedMessage id="PDP.Age" defaultMessage="Age" />:<span>{productDetails.age}</span>
                    </>
                )}
            </div>
            <div>
                <ShareOptions setOpenShare={setOpenShare} openShare={openShare} />
                <IconButton onClick={() => setOpenShare(true)}>
                    <Share className="greenColor" />
                </IconButton>
                {addLoader && <CircularProgress />}
                {!addLoader && (
                    <IconButton onClick={() => addOrRemoveWishItem()}>
                        {index === -1 ? <FavoriteBorder className="greenColor" /> : <Favorite className="greenColor" />}
                    </IconButton>
                )}
            </div>
        </Grid>
    );
}

export default withRouter(AgeAndShare);
