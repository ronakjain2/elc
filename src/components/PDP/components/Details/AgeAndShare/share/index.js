import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import DialogContent from '@material-ui/core/DialogContent';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Cancel from '@material-ui/icons/Cancel';
import { FacebookShareButton, WhatsappShareButton, TwitterShareButton } from 'react-share';

export default function ShareOptions({ openShare, setOpenShare }) {
    const handleClose = () => {
        setOpenShare(false);
    };

    const shareUrl = window.location.href;

    return (
        <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={openShare}>
            <Grid container justify="space-between" alignItems="center">
                <DialogTitle id="simple-dialog-title">Share</DialogTitle>
                <IconButton onClick={() => handleClose()}>
                    <Cancel />
                </IconButton>
            </Grid>
            <Divider className="shareLine" />
            <DialogContent>
                <Grid container justify="space-between" alignItems="center" className="shareOptions">
                    <div>
                        <FacebookShareButton url={shareUrl} style={{ cursor: 'pointer' }}>
                            <img src="/images/footer/Facebook.svg" alt="facebookshare" className="shareIcon" />
                            <Typography>Facebook</Typography>
                        </FacebookShareButton>
                    </div>
                    <div>
                        <TwitterShareButton url={shareUrl} style={{ cursor: 'pointer' }}>
                            <img src="/images/footer/twitter.svg" alt="twittershare" className="shareIcon" />
                            <Typography>Twitter</Typography>
                        </TwitterShareButton>
                    </div>
                    <div>
                        <a href="https://www.instagram.com/elctoys" rel="noopener noreferrer" target="_blank">
                            <img src="/images/footer/instagram.svg" alt="instagramshare" className="shareIcon" />
                            <Typography className="textAlignCenter">Instagram</Typography>
                        </a>
                    </div>
                    <div>
                        <a href="https://www.youtube.com/elctoysme" rel="noopener noreferrer" target="_blank">
                            <img src="/images/footer/youtube.svg" alt="youtubeshare" className="shareIcon" />
                            <Typography className="textAlignCenter">YouTube</Typography>
                        </a>
                    </div>
                    <div>
                        <WhatsappShareButton title={'WhatsApp'} url={shareUrl} style={{ cursor: 'pointer' }}>
                            <img
                                src="/images/footer/Icon_Social_whatsapp.svg"
                                alt="whatsappShare"
                                className="shareIcon"
                            />
                            <Typography>WhatsApp</Typography>
                        </WhatsappShareButton>
                    </div>
                </Grid>
            </DialogContent>
        </Dialog>
    );
}
