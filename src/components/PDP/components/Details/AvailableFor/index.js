import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import { FormattedMessage } from 'react-intl';
import './styles.css';
import { useSelector } from 'react-redux';

export default function AvailableFor() {
    const click_collect_ds_flag = useSelector(
        (state) => state && state.PDP && state.PDP.details && state.PDP.details.click_collect_ds_flag,
    );
    return (
        <Grid container className="AvailableFor">
            <Grid container>
                <Typography className="availableFor-text">
                    <FormattedMessage id="PDP.AvailableFor" defaultMessage="Available for" />
                </Typography>
            </Grid>
            <Grid container>
                <div>
                    <List component="nav" aria-label="main mailbox folders">
                        <ListItem>
                            <ListItemIcon>
                                <img
                                    src="/images/pdp/Icon_Header_FreeShip.svg"
                                    alt="pickupformstor"
                                    className="MuiSvgIcon-root-Free"
                                />
                            </ListItemIcon>
                            <ListItemText
                                primary={<FormattedMessage id="PDP.HomeDelivery" defaultMessage="Home Delivery" />}
                            />
                        </ListItem>
                    </List>
                </div>
                <div className="flex alignItemCenter">
                    <Divider orientation="vertical" className="line" />
                </div>
                <div>
                    <List component="nav" aria-label="main mailbox folders">
                        <ListItem className={click_collect_ds_flag === 1 && 'strick-out'}>
                            <ListItemIcon>
                                {click_collect_ds_flag === 1 && (
                                    <img
                                        src="/images/pdp/Icon_Cart_Cart_Gray.svg"
                                        alt="pickupformstor"
                                        className="MuiSvgIcon-root-Free"
                                    />
                                )}
                                {(!click_collect_ds_flag || click_collect_ds_flag === 0) && (
                                    <img
                                        src="/images/pdp/Icon_Cart_Cart_Green.svg"
                                        alt="pickupformstor"
                                        className="MuiSvgIcon-root-Free"
                                    />
                                )}
                            </ListItemIcon>
                            <ListItemText
                                primary={
                                    <FormattedMessage id="PDP.PickupFromStore" defaultMessage="Pickup from store" />
                                }
                            />
                        </ListItem>
                    </List>
                </div>
            </Grid>
        </Grid>
    );
}
