import React, { useState, useEffect } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import { useSelector, useDispatch } from 'react-redux';
import { actions as PDPActions } from 'components/PDP/redux/actions';
import { actions as authActions } from 'components/Global/redux/actions';
import { withRouter } from 'react-router';

function ShowSessionExpiredPOP({ history }) {
    const [open, setOpen] = useState(false);

    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    const showExpiredPOP = state && state.PDP && state.PDP.showExpiredPOP;
    const store_locale = state && state.auth && state.auth.store_locale;
    const storeId = state && state.auth && state.auth.currentStore;

    useEffect(() => {
        setOpen(showExpiredPOP || false);
    }, [showExpiredPOP]);

    const handleCloseOutSide = () => {
        setOpen(true);
    };

    const onClickOk = () => {
        dispatch(
            PDPActions.setShowSessionExpiredPOPForLoginUser({
                showExpiredPOP: false,
                message: '',
            }),
        );
        dispatch(authActions.saveLoginUser(null));
        dispatch(authActions.removeReservationRequestSuccess());
        dispatch(authActions.getGuestCartRequest({ storeId }));
        history.push(`/${store_locale}/sign-in-register`);
    };

    return (
        <Dialog
            open={open}
            onClose={handleCloseOutSide}
            className="country-pop-container"
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogContent className="sessionExpiredLogic">
                <Grid container alignItems="center">
                    <Grid item xs={12}>
                        <Typography className="pop-title">
                            <FormattedMessage
                                id="PDP.Expired.POP.Message"
                                defaultMessage="Your session has expired. Please login again"
                            />
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container justify="space-around" alignItems="center">
                            <div>
                                <Button onClick={() => onClickOk()}>
                                    <FormattedMessage id="MyAccount.MyWishlist.Ok" defaultMessage="Ok" />
                                </Button>
                            </div>
                        </Grid>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    );
}

export default withRouter(ShowSessionExpiredPOP);
