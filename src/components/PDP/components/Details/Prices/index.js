import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { showDiscountPriceOnPDP, showFCDiscountPriceOnPDP } from 'components/PDP/utils';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import './styles.css';

export default function Prices({ productDetails, history }) {
    const state = useSelector((state) => state);
    const store_locale = state && state.auth && state.auth.store_locale;
    const customer_type = state && state.auth && state.auth.loginUser && state.auth.loginUser.customer_type;
    return (
        <Grid container className="price">
            {customer_type &&
            customer_type === 'ecom_club_user' &&
            productDetails.offers &&
            productDetails.offers.fc_data ? (
                <Grid item xs={12}>
                    <Typography className="p-priceAmt ar-right">
                        <span>{showFCDiscountPriceOnPDP(productDetails)}</span>
                    </Typography>
                </Grid>
            ) : (
                <Grid item xs={12}>
                    <Typography className="p-priceAmt ar-right">{showDiscountPriceOnPDP(productDetails)}</Typography>
                </Grid>
            )}

            {customer_type &&
            customer_type === 'ecom_club_user' &&
            productDetails.offers &&
            productDetails.offers.fc_data ? (
                <div />
            ) : (
                productDetails.offers &&
                productDetails.offers.fc_data && (
                    <Grid item xs={12}>
                        <Typography className="p-fcPriceAmt ar-right">
                            <span>
                                {productDetails.currency} {parseFloat(productDetails.offers.fc_data['1']).toFixed(2)}
                            </span>
                        </Typography>
                    </Grid>
                )
            )}

            {customer_type && customer_type === 'ecom_club_user'
                ? productDetails.offers &&
                  productDetails.offers.fc_data && (
                      <Grid item xs={12}>
                          <Typography className="fcDiscountText arabic-right">
                              <img src="/images/pdp/icon_fc_benefit.svg" alt="family club benefit applied" />
                              &nbsp;
                              <FormattedMessage id="PDP.FcUserText" defaultMessage="Family Club discount Applied!" />
                          </Typography>
                      </Grid>
                  )
                : productDetails.offers &&
                  productDetails.offers.fc_data && (
                      <Grid item xs={12}>
                          <Typography
                              className="fcDiscountText arabic-right cursor-pointer"
                              onClick={() => history.push(`/${store_locale}/family-club`)}
                          >
                              <img src="/images/pdp/icon_fc_benefit.svg" alt="family club benefit applied" />
                              &nbsp;
                              <FormattedMessage
                                  id="PDP.NonFcUserText"
                                  defaultMessage="Family Club benefit- Join now and get 10% off"
                              />
                          </Typography>
                      </Grid>
                  )}
        </Grid>
    );
}
