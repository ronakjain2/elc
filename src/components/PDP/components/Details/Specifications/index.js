import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Collapsible from 'react-collapsible';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import ArrowDropUp from '@material-ui/icons/ArrowDropUp';
import { FormattedMessage } from 'react-intl';
import './styles.css';
import Descriptions from './components/Decriptions';
import ProductSpecifications from './components/ProductSpecifications';
//import Delivery from './components/Delivery';

export default function Specifications({ productDetails }) {
    const createCommonCollections = (nameId, component, name) => (
        <>
            <Collapsible
                trigger={
                    <Grid
                        container
                        justify="space-between"
                        className={
                            name === 'Delivery' ? 'coll-container bottomLine NoBorder' : 'coll-container bottomLine'
                        }
                        alignItems="center"
                    >
                        <div>
                            <Typography className="NotOpenTitle">
                                <FormattedMessage id={nameId} defaultMessage={name} />
                            </Typography>
                        </div>
                        <div>
                            <ArrowDropDown className="Icon" />
                        </div>
                    </Grid>
                }
                triggerWhenOpen={
                    <Grid container justify="space-between" className="coll-container" alignItems="center">
                        <div>
                            <Typography className="OpenTitle">
                                <FormattedMessage id={nameId} defaultMessage={name} />
                            </Typography>
                        </div>
                        <div>
                            <ArrowDropUp className="Icon" />
                        </div>
                    </Grid>
                }
            >
                <Grid container className="coll-container">
                    {component}
                </Grid>
            </Collapsible>
        </>
    );

    return (
        <Grid container className="Specifications">
            <Grid item xs={12}>
                {createCommonCollections(
                    'PDP.Description',
                    <Descriptions productDetails={productDetails} />,
                    'Description',
                )}
            </Grid>
            <Grid item xs={12}>
                {createCommonCollections(
                    'PDP.Specifications',
                    <ProductSpecifications productDetails={productDetails} />,
                    'Specifications',
                )}
            </Grid>
            {/*<Grid item xs={12}>
                {createCommonCollections('PDP.Delivery', <Delivery />, 'Delivery')}
            </Grid>*/}
        </Grid>
    );
}
