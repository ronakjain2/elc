import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

export default function Descriptions({ productDetails }) {
    return (
        <Grid container>
            <Typography className="specification-description arabic-right">
                {productDetails && productDetails.description}
            </Typography>
        </Grid>
    );
}
