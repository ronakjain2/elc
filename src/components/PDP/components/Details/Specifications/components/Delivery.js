import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

const KSA = {
    title: (
        <FormattedMessage
            id="PDP.Delivery.KSA.HowMuchDoesKSADeliveryCost"
            defaultMessage="How much does KSA delivery cost?"
        />
    ),
    details: [
        {
            title: <FormattedMessage id="PDP.Delivery.KSA.StandardDelivery" defaultMessage="Standard Delivery" />,
            SubTitle: (
                <FormattedMessage
                    id="PDP.Delivery.KSA.WhenYouSpendSAR250"
                    defaultMessage="- when you spend SAR 250, or SAR 25 for orders under SAR 250."
                />
            ),
            List: [
                <FormattedMessage
                    id="PDP.Delivery.KSA.DeliveryIn3To4WorkingDays"
                    defaultMessage="Delivery in 3 to 4 working days."
                />,
                <FormattedMessage
                    id="PDP.Delivery.KSA.FreeDeliveryOverSAR250AppliesForAllOfKSA"
                    defaultMessage="Free delivery over SAR 250 applies for all of KSA."
                />,
            ],
        },
        {
            title: (
                <FormattedMessage
                    id="PDP.Delivery.KSA.SameDayDeliveryComingSoon"
                    defaultMessage="Same Day Delivery – Coming Soon"
                />
            ),
            List: [
                <FormattedMessage id="PDP.Delivery.KSA.ComingSoonToJeddah" defaultMessage="Coming soon to Jeddah" />,
            ],
        },
        {
            title: <FormattedMessage id="PDP.Delivery.KSA.OutdoorItems" defaultMessage="Outdoor Items" />,
            SubTitle: (
                <FormattedMessage
                    id="PDP.Delivery.KSA.FREEASSEMBLYOnOutdoorLines"
                    defaultMessage="FREE ASSEMBLY – on Outdoor lines when you spend SAR 1,000 and above"
                />
            ),
            List: [
                <FormattedMessage
                    id="PDP.Delivery.KSA.DeliveryIn5To7"
                    defaultMessage="Delivery in 5 to 7 working days."
                />,
                <FormattedMessage
                    id="PDP.Delivery.KSA.ServiceAvailableInJeddah"
                    defaultMessage="Service available in Jeddah."
                />,
            ],
        },
    ],
};

const UAE = {
    title: (
        <FormattedMessage
            id="PDP.Delivery.UAE.HowMuchDoesUAEDeliveryCost"
            defaultMessage="How much does UAE delivery cost?"
        />
    ),
    details: [
        {
            title: <FormattedMessage id="PDP.Delivery.KSA.StandardDelivery" defaultMessage="Standard Delivery" />,
            SubTitle: (
                <FormattedMessage
                    id="PDP.Delivery.UAE.FREEWhenYouSpendAED250"
                    defaultMessage="FREE - when you spend AED 250, or AED 25 for orders under AED 250."
                />
            ),
            List: [
                <FormattedMessage
                    id="PDP.Delivery.UAE.DeliveryIn2To3WorkingDays"
                    defaultMessage="Delivery in 2 to 3 working days"
                />,
                <FormattedMessage
                    id="PDP.Delivery.UAE.FreeDeliveryOverAED250AppliesForAllOfTheU.A.E"
                    defaultMessage="Free delivery over AED 250 applies for all of the U.A.E."
                />,
            ],
        },
        {
            title: (
                <FormattedMessage
                    id="PDP.Delivery.KSA.SameDayDeliveryComingSoon"
                    defaultMessage="Same Day Delivery – Coming Soon"
                />
            ),
            List: [<FormattedMessage id="PDP.Delivery.UAE.ComingSoonToDubai" defaultMessage="Coming soon to Dubai" />],
        },
        {
            title: (
                <FormattedMessage
                    id="PDP.Delivery.UAE.Click&CollectComingSoon"
                    defaultMessage="Click & Collect – coming soon"
                />
            ),
            List: [
                <FormattedMessage
                    id="PDP.Delivery.UAE.ComingSoonToTheSelectedELCStores"
                    defaultMessage="Coming soon to the U.A.E. in selected ELC Stores"
                />,
            ],
        },
        {
            title: <FormattedMessage id="PDP.Delivery.KSA.OutdoorItems" defaultMessage="Outdoor Items" />,
            SubTitle: (
                <FormattedMessage
                    id="PDP.Delivery.UAE.FREEASSEMBLYOnOutdoorLines"
                    defaultMessage="FREE ASSEMBLY –on Outdoor lines when you spend AED 1,000 and above"
                />
            ),
            List: [
                <FormattedMessage
                    id="PDP.Delivery.UAE.DeliveryIn5To7WorkingDays"
                    defaultMessage="Delivery in 5 to 7 working days."
                />,
                <FormattedMessage
                    id="PDP.Delivery.UAE.ServiceAvailableInDubaiAbuDhabiAlAinAjmanRasAlKhaimahSharjah"
                    defaultMessage="Service available in Dubai, Abu Dhabi, Al Ain, Ajman, Ras Al Khaimah, Sharjah"
                />,
            ],
        },
    ],
};

export default function Delivery() {
    const state = useSelector((state) => state);

    const country = state && state.auth && state.auth.country;

    return (
        <Grid container className="Delivery">
            {KSA && country === 'KSA' && (
                <>
                    <Grid container>
                        {KSA.title && <Typography className="title arabic-right">{KSA.title}</Typography>}
                    </Grid>

                    <Grid container>
                        {KSA.details &&
                            KSA.details.map((details, index) => (
                                <div key={`details_KSA_${index}`} className="details arabic-right">
                                    <Grid container>
                                        {details.title && (
                                            <Typography className="titled arabic-right">{details.title}</Typography>
                                        )}
                                    </Grid>
                                    <Grid container>
                                        {details.SubTitle && (
                                            <Typography className="subTitled arabic-right">
                                                {details.SubTitle}
                                            </Typography>
                                        )}
                                    </Grid>
                                    <Grid container>
                                        <ul>
                                            {details.List &&
                                                details.List.map((detailList, indexd) => (
                                                    <li
                                                        key={`detailsKSAList_${index}_${indexd}`}
                                                        className="listT arabic-right"
                                                    >
                                                        {detailList}
                                                    </li>
                                                ))}
                                        </ul>
                                    </Grid>
                                </div>
                            ))}
                    </Grid>
                </>
            )}
            {UAE && country === 'UAE' && (
                <>
                    <Grid container>
                        {UAE.title && <Typography className="title arabic-right">{UAE.title}</Typography>}
                    </Grid>

                    <Grid container>
                        {UAE.details &&
                            UAE.details.map((details, index) => (
                                <div key={`details_UAE_${index}`} className="details arabic-right">
                                    <Grid container>
                                        {details.title && (
                                            <Typography className="titled arabic-right">{details.title}</Typography>
                                        )}
                                    </Grid>
                                    <Grid container>
                                        {details.SubTitle && (
                                            <Typography className="subTitled arabic-right">
                                                {details.SubTitle}
                                            </Typography>
                                        )}
                                    </Grid>
                                    <Grid container>
                                        <ul>
                                            {details.List &&
                                                details.List.map((detailList, indexd) => (
                                                    <li
                                                        key={`detailsKSAList_${index}_${indexd}`}
                                                        className="listT arabic-right"
                                                    >
                                                        {detailList}
                                                    </li>
                                                ))}
                                        </ul>
                                    </Grid>
                                </div>
                            ))}
                    </Grid>
                </>
            )}
        </Grid>
    );
}
