import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import { getSpecificationClassName } from 'components/PDP/utils';

const specificationList = [
    {
        label: <FormattedMessage id="PDP.Specifications.ProductCode" defaultMessage="Product Code" />,
        valueKey: 'sku',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.Age" defaultMessage="Age" />,
        valueKey: 'age',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.AssemblyRequired" defaultMessage="Assembly Required" />,
        valueKey: 'assembly_req',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.BatteryIncluded" defaultMessage="Battery Included" />,
        valueKey: 'battery_inc',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.BatteryRequired" defaultMessage="Battery Required" />,
        valueKey: 'battery_req',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.Brand" defaultMessage="Brand" />,
        valueKey: 'brand',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.Height" defaultMessage="Height" />,
        valueKey: 'packaging_height',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.Width" defaultMessage="Width" />,
        valueKey: 'packaging_width',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.Length" defaultMessage="Length" />,
        valueKey: 'packaging_length',
    },
    {
        label: <FormattedMessage id="PDP.Specifications.Weight" defaultMessage="Weight" />,
        valueKey: 'packaging_weight',
    },
];

export default function ProductSpecifications({ productDetails }) {
    return (
        <Grid container>
            {specificationList.map((spItem, index) => (
                <Grid
                    container
                    justify="space-between"
                    alignItems="center"
                    className="spListContainer"
                    key={`specificationItem_${index}`}
                >
                    <div>
                        <Typography className="spTextAvailable">{spItem.label}</Typography>
                    </div>
                    <div>
                        <Typography
                            className={getSpecificationClassName(productDetails && productDetails[spItem.valueKey])}
                        >
                            {productDetails && productDetails[spItem.valueKey]}
                        </Typography>
                    </div>
                </Grid>
            ))}
        </Grid>
    );
}
