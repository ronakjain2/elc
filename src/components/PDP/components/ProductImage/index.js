import Grid from '@material-ui/core/Grid';
import Img from 'commonComponet/Image';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import './pImage.css';

export default function ProductImgae() {
    const state = useSelector(state => state);
    const primaryImages =
        state && state.PDP && state.PDP.details && state.PDP.details.imageUrl && state.PDP.details.imageUrl.thumbnail;
    const video = (state && state.PDP && state.PDP.details.mediaVideoUrl) || [];
    const primaryImagesData = [...primaryImages, video[0]];
    const [activePrimaryImageImage, setActivePrimaryImageImage] = useState(primaryImagesData && primaryImagesData[0]);
    const [fileType, setFileType] = useState('image');
    const setImageFunc = data => {
        let fileType;
        let type = data.split('.').pop();
        if (type === 'mp4') {
            fileType = 'video';
        } else {
            fileType = 'image';
        }
        setFileType(fileType);
        setActivePrimaryImageImage(data);
    };

    // useEffect(() => {
    //   try {
    //     setActivePrimaryImageImage(primaryImagesData && primaryImagesData[0]);
    //   } catch (err) {}
    //   }, [primaryImages]);

    return (
        <Grid container justify="center" className="productImage productSticky">
            <Grid container justify="center">
                <Grid item xs={12} md={10}>
                    {fileType === 'image' ? (
                        <Img width="500" height="500" src={activePrimaryImageImage || ''} alt="productImage" />
                    ) : (
                        <video
                            controls
                            autoplay="autoplay"
                            loop
                            muted
                            preload
                            style={{ height: '100%', width: '100%' }}
                        >
                            <source src={activePrimaryImageImage && activePrimaryImageImage} type="video/mp4" />
                        </video>
                    )}
                </Grid>
            </Grid>
            <Grid container justify="center">
                <Grid item xs={12} md={9} className="secondaryImagesMargin">
                    <Grid container justify="center">
                        {primaryImagesData &&
                            primaryImagesData.map(
                                (productImage, index) =>
                                    primaryImagesData[index] && (
                                        <div
                                            className={
                                                activePrimaryImageImage === productImage
                                                    ? 'activeCard'
                                                    : 'cardSecondary'
                                            }
                                            key={`product_image_${index}`}
                                            onClick={() => setImageFunc(productImage)}
                                        >
                                            <Img
                                                width="55"
                                                height="55"
                                                src={
                                                    index !== primaryImagesData.length - 1
                                                        ? productImage
                                                        : '/images/videoIcon.png'
                                                }
                                                alt="productImage"
                                                imageStyle={{ height: '100%' }}
                                                className={index !== primaryImagesData.length - 1 ? '' : 'videoIcon'}
                                            />
                                        </div>
                                    ),
                            )}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
