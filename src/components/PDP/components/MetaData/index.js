import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Helmet } from 'react-helmet';

export default function MetaData({ productDetails }) {
    return (
        <Grid container>
            {productDetails &&
                'meta_title' in productDetails &&
                'meta_keywords' in productDetails &&
                'meta_description' in productDetails && (
                    <Helmet>
                        <meta name="tital" content={productDetails.meta_title} />
                        <title>{productDetails.meta_title}</title>
                        <meta name="keywords" content={productDetails.meta_keywords} />
                        <meta name="description" content={productDetails.meta_description} />
                    </Helmet>
                )}
        </Grid>
    );
}
