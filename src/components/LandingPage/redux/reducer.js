import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionsHandler = {
    [types.GET_LANDING_PAGE_REQUEST]: state => ({
        ...state,
        loader: true,
        message: null,
    }),
    [types.GET_LANDING_PAGE_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loader: false,
        data: null,
        message: payload || 'Something went wrong',
    }),
    [types.GET_LANDING_PAGE_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        data: payload,
        message: null,
    }),
};

export default handleActions(actionsHandler, {
    loader: false,
    data: null,
    message: null,
});
