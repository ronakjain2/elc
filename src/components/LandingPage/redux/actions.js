import { createAction } from 'redux-actions';

// Type Name
const GET_LANDING_PAGE_REQUEST = 'ELC@OROB/GET_LANDING_PAGE_REQUEST';
const GET_LANDING_PAGE_REQUEST_SUCCESS = 'ELC@OROB/GET_LANDING_PAGE_REQUEST_SUCCESS';
const GET_LANDING_PAGE_REQUEST_FAILED = 'ELC@OROB/GET_LANDING_PAGE_REQUEST_FAILED';

// Action Method
const getLandingPageRequest = createAction(GET_LANDING_PAGE_REQUEST);
const getLandingPageRequestSuccess = createAction(GET_LANDING_PAGE_REQUEST_SUCCESS);
const getLandingPageRequestFailed = createAction(GET_LANDING_PAGE_REQUEST_FAILED);

export const actions = {
    getLandingPageRequest,
    getLandingPageRequestSuccess,
    getLandingPageRequestFailed,

};

export const types = {
    GET_LANDING_PAGE_REQUEST,
    GET_LANDING_PAGE_REQUEST_SUCCESS,
    GET_LANDING_PAGE_REQUEST_FAILED,

};
