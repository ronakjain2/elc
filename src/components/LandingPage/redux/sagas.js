import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../api';
import { actions, types } from './actions';

const getLandingpageReq = function* getLandingpageReq({ payload }) {
    try {
        const { data } = yield call(api.getLandingpage, payload);

        if (data && data.status) {
            yield put(actions.getLandingPageRequestSuccess(data.data));
        } else {
            yield put(actions.getLandingPageRequestFailed(data.message));
        }
    } catch (err) {
        yield put(actions.getLandingPageRequestFailed(err && err.message));
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_LANDING_PAGE_REQUEST, getLandingpageReq);
}
