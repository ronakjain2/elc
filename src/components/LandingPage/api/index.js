import api from 'apiServices';

const getLandingpage = ({ url_key, store_id }) =>
    api.get(`/index.php/rest/V1/app/landingpage?url=${url_key}&store=${store_id}`);

export default {
    getLandingpage,
};
