import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Spinner from 'commonComponet/Spinner';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Banner from './components/Banner';
import Filter from './components/Filter';
import Footer from './components/Footer';
import Header from './components/Header/index';
import Intro from './components/Intro';
import ProductSlider from './components/ProductSlider';
import Theme from './components/Theme';
import { actions } from './redux/actions';
import './style.css';

const Landing = ({ match, history }) => {
    const dispatch = useDispatch();
    const state = useSelector(state => state);
    const url_key_1 = match.params.category_l1;
    const url_key_2 = match.params.category_l2;
    const url_key_3 = match.params.category_l3;
    const url_key = `${url_key_1}${url_key_2 ? `/${url_key_2}` : ''}${url_key_3 ? `/${url_key_3}` : ''}`;

    const store_id = state && state.auth && state.auth.currentStore;
    const store_locale = state && state.auth && state.auth.store_locale;
    const landingPage = state && state.landingPage;
    const loader = landingPage && landingPage.loader;
    const message = landingPage && landingPage.message;
    const data = landingPage && landingPage.data;

    useEffect(() => {
        const payload = {
            store_id,
            url_key,
        };
        dispatch(actions.getLandingPageRequest(payload));
        window && window.scrollTo(0, 0);
    }, [dispatch, store_id, url_key]);

    const getComponentWithType = data => {
        if (!data.status) return null;
        let type = data.type;
        type = type && type.split('-');
        if (!type[0]) return null;
        const gotoURL = url => {
            if(url) {
                return `/${store_locale}${url}`
            }
            return "#"
        };
        const scroll_Up = url =>{
            if(url){
                window && window.scrollTo(0, 0);
            }
        }
        const _props = {
            gotoURL,
            data,
            scroll_Up,
        };

        switch (type[0]) {
            case 'header':
                return <Header {..._props} />;
            case 'banner':
                return <Banner {..._props} />;
            case 'filter':
                return <Filter {..._props} />;
            case 'intro':
                return <Intro {..._props} />;
            case 'theme':
                return <Theme {..._props} />;
            case 'product':
                return <ProductSlider {..._props} />;
            case 'footer':
                return <Footer {..._props} />;
            default:
                return null;
        }
    };

    return (
        <Grid container>
            {loader && (
                <Grid container alignItems="center">
                    <Spinner />
                </Grid>
            )}
            {!loader && data && <>{data.sections && data.sections.map(section => getComponentWithType(section))}</>}
            {!loader && message && (
                <Typography className="landing-message" variant="span">
                    {message}
                </Typography>
            )}
        </Grid>
    );
};

export default withRouter(Landing);
