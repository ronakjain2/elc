import Button from '@material-ui/core/Button';
import React from 'react';
import './style.css';
import { Link } from 'react-router-dom';
const ShopAllButton = props => {
    const { showButton, gotoURL, buttonLabel, buttonRedirection ,scroll_Up} = props;
    if (!showButton) return null;
    return (
        <Link 
            to={gotoURL(buttonRedirection)} 
            className="link-landingpage"
        >
            <Button 
                onClick={scroll_Up}
                className="landing-shop-all-button"
            >
                {buttonLabel}
            </Button>
        </Link>
    );
};

export default ShopAllButton;
