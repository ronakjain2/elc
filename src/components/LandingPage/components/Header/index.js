import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';

export default function Header({ data, gotoURL }) {
    if (!data || !data.data || !data.data.text) return null;
    const _style = {
        background: data.data.bg_color || '#ffff00',
        color: data.data.text_color || '#000',
    };
    return (
        <Grid 
            container 
            justify="center" 
            style={_style} 
            className="px-1 py-2"
        >
            <Link 
                to={gotoURL(data.data.link)} 
                className="header-title"
            >
                <Typography align="center">
                    <b>{data.data.text}</b>
                </Typography>
            </Link>
        </Grid>
    );
}
