import React from 'react';
import ShopByAge from './Components/ShopByAge';
import ShopByBrand from './Components/ShopByBrand';

const Filter = props => {
    const { data } = props;
    if (!data || !data.data || !data.data.length) return null;

    if (data.type === 'filter-1') return <ShopByAge {...props} />;
    if (data.type === 'filter-2') return <ShopByBrand {...props} />;

    return null;
};

export default Filter;
