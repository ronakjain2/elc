import { Grid, Typography } from '@material-ui/core';
import Img from 'commonComponet/Image';
import React from 'react';
import { Link } from 'react-router-dom';
import { getRandomColor } from '../../../utils';
export default function ShopByAgeCard({ item, gotoURL, scroll_Up }) {
    const _style = {
        backgroundColor: getRandomColor(),
    };
    return (
        <Link to={gotoURL(item.url_key)} className="link-landingpage">
        <Grid
            container
            justify="center"
            alignItems="center"
            onClick={scroll_Up}
            className="cursor-pointer"
        >
                <Img src={item.image} height="200" width="200" alt={item.label} className="slider-image" />
                <Grid className="slider-div_value" style={_style}>
                    <Typography>{item.label}</Typography>
                </Grid>
        </Grid>
        </Link>
    );
}
