import { Grid } from '@material-ui/core';
import Img from 'commonComponet/Image';
import { Link } from 'react-router-dom';
import React from 'react';

export default function BrandsCard({ item, gotoURL, scroll_Up }) {
    return (
        <Link to={gotoURL(item.url_key)}>
            <Grid
                container
                justify="center"
                alignItems="center"
                onClick={scroll_Up}
                className="cursor-pointer"
            >
                <Img src={item.image} alt={item.label} height="150" width="150" className="brand-slider-image" />
            </Grid>
        </Link>
    );
}
