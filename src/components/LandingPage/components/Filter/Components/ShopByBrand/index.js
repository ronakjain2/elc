import Grid from '@material-ui/core/Grid';
import ShopAllButton from 'components/LandingPage/components/ShopAllButton';
import React from 'react';
import { withRouter } from 'react-router-dom';
import Slider from 'react-slick';
import { sliderSettings } from '../../utils';
import BrandsCard from './Components/BrandsCard';
import './style.css';

function BrandsSlider(props) {
    const { data, gotoURL, scroll_Up } = props;

    return (
        <>
            <Grid container justify="center" className="slider-title">
                <Grid item xs={11} className="title">
                    <span className="label">{data.title}</span>
                    <ShopAllButton
                        showButton={data.showButton}
                        buttonLabel={data.buttonLabel}
                        buttonRedirection={data.buttonRedirection}
                        gotoURL={gotoURL}
                        scroll_Up={scroll_Up}
                    />
                </Grid>
            </Grid>
            <Grid container justify="center">
                <Grid item xs={12} md={10} className="slider">
                    <Slider {...sliderSettings}>
                        {data.data.map((item, index) => (
                            <div key={`brand_${index}`}>
                                <BrandsCard 
                                    item={item} 
                                    scroll_Up={scroll_Up}
                                    gotoURL={gotoURL} />
                            </div>
                        ))}
                    </Slider>
                </Grid>
            </Grid>
        </>
    );
}

export default withRouter(BrandsSlider);
