import { Grid, Typography } from '@material-ui/core';
import React from 'react';

const Footer = props => {
    const { data } = props;
    if (!data || !data.data || !data.data.text) return null;
    return (
        <Grid container justify="center">
            <Typography className="text-muted my-2">{data.data.text}</Typography>
        </Grid>
    );
};

export default Footer;
