import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Img from 'commonComponet/Image';
import Video from 'commonComponet/Video';
import React from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import './style.css';
import { BannerSettings } from './utils';
const Landing = ({ data, gotoURL, scroll_Up }) => {
    if (!data || !data.data || !data.data.length) return null;
    const banner = data.data.filter(item =>
        item.type === 'video' ? item.video_desktop && item.video_mobile : item.img_desktop && item.img_mobile,
    );

    return (
        <Grid item xs={12}>
            <Slider {...BannerSettings}>
                {banner.map((item, index) => (
                    <Link 
                        to={gotoURL(item.link)} 
                        onClick={scroll_Up}
                        key={index}>
                        {item.type === 'video' ? (
                            <div key={index} className="banner-image-hight">
                                <Hidden only={['xs', 'sm', 'md']}>
                                    <Video className="w-100" autoPlay src={item.video_desktop} />
                                </Hidden>
                                <Hidden only={['lg', 'xl']}>
                                    <Video className="w-100" autoPlay src={item.video_mobile} />
                                </Hidden>
                            </div>
                        ) : (
                            <div key={index} className="banner-image-hight">
                                <Hidden only={['xs', 'sm', 'md']}>
                                    <Img
                                        lazy={false}
                                        width="1600"
                                        height="400"
                                        src={item.img_desktop}
                                        alt={`Homepage Banner ${index}`}
                                    />
                                </Hidden>
                                <Hidden only={['lg', 'xl']}>
                                    <Img
                                        lazy={false}
                                        width="600"
                                        height="600"
                                        alt={`Homepage Banner ${index}`}
                                        src={item.img_mobile}
                                    />
                                </Hidden>
                            </div>
                        )}
                    </Link>
                ))}
            </Slider>
        </Grid>
    );
};

export default Landing;
