import { Button, Grid, Hidden } from '@material-ui/core';
import Img from 'commonComponet/Image';
import React from 'react';
import './style.css';
import { Link } from 'react-router-dom';
const Intro = props => {
    const { data, gotoURL, scroll_Up } = props;
    if (!data || !data.data) return null;
    const _styles = {
        color: data.data.button_color || '#ffff00',
        backgroundColor: data.data.button_bgcolor || '#000',
    };
    if (!data.data.button_text && !data.data.img_desktop && !data.data.img_mobile) return null;
    return (
        <Grid container className="py-4 intro-container">
            {data.data.button_text && (
                <Link 
                    to ={gotoURL(data.data.button_link)}
                    >
                    <Button
                        className={`intro-button ${data.data.button_position}`}
                        style={_styles}
                        onClick={scroll_Up}
                    >
                        {data.data.button_text}
                    </Button>
                </Link>
            )}
            <Grid 
                item xs={12} 
                className="cursor-pointer" 
                onClick={scroll_Up}
            >
                <Link 
                    to ={gotoURL(data.data.banner_link)}
                    >
                    {data.data.img_desktop && (
                        <Hidden only={['xs', 'sm', 'md']}>
                            <Img
                                width="1600"
                                height="400"
                                src={data.data.img_desktop}
                                alt={data.data.button_text || 'Shop All'}
                            />
                        </Hidden>
                    )}
                    {data.data.img_mobile && (
                        <Hidden only={['lg', 'xl']}>
                            <Img
                                width="600"
                                height="400"
                                src={data.data.img_mobile}
                                alt={data.data.button_text || 'Shop All'}
                            />
                        </Hidden>
                    )}
                </Link>
            </Grid>
        </Grid>
    );
};

export default Intro;
