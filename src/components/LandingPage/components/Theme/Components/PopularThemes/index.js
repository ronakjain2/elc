import Grid from '@material-ui/core/Grid';
import ShopAllButton from 'components/LandingPage/components/ShopAllButton';
import React from 'react';
import { withRouter } from 'react-router-dom';
import Slider from 'react-slick';
import { sliderSettings } from '../../utils';
import PopularThemeCard from './Components/PopularThemeCard';
import './style.css';

function PopularThemes(props) {
    const { data, gotoURL, scroll_Up } = props;
    return (
        <>
            <Grid container justify="center" className="slider-title">
                <Grid item xs={11} className="title">
                    <span className="label">{data.title}</span>
                    <ShopAllButton
                        showButton={data.showButton}
                        buttonLabel={data.buttonLabel}
                        buttonRedirection={data.buttonRedirection}
                        gotoURL={gotoURL}
                        scroll_Up={scroll_Up}
                    />
                </Grid>
            </Grid>
            <Grid container justify="center">
                <Grid item xs={12} md={10} className="slider">
                    <Slider {...sliderSettings}>
                        {data.data.map((item, index) => (
                            <div key={`theme_${index}`} className="px-2">
                                <PopularThemeCard item={item} {...props} />
                            </div>
                        ))}
                    </Slider>
                </Grid>
            </Grid>
        </>
    );
}

export default withRouter(PopularThemes);
