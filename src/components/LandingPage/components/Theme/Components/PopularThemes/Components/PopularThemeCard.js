import { Grid, Typography } from '@material-ui/core';
import Img from 'commonComponet/Image';
import { Link } from 'react-router-dom';
import React from 'react';

export default function PopularThemeCard({ item, gotoURL, scroll_Up }) {
    return (
        <Grid 
            onClick={scroll_Up} 
            className="cursor-pointer"
        >
            <Link 
                className="link-landingpage"
                to={gotoURL(item.url_key)}
            >
                <Img 
                    width="200" 
                    height="200" 
                    src={item.image} 
                    alt={item.label} 
                    className="slider-popularcard-image"
                />
                <Typography 
                    align="center" 
                    className="slider-subtitle"
                    color="textPrimary"
                >
                    {item.label}
                </Typography>
            </Link>
        </Grid>
    );
}
