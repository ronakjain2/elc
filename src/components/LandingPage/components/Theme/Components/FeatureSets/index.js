import { Grid } from '@material-ui/core';
import Img from 'commonComponet/Image';
import { Link } from 'react-router-dom';
import ShopAllButton from 'components/LandingPage/components/ShopAllButton';
import React from 'react';
import './style.css';

export default function Feature(props) {
    const { data, gotoURL, scroll_Up } = props;
    return (
        <>
            <Grid container justify="center" className="slider-title">
                <Grid item xs={11} className="title">
                    <span className="label">{data.title}</span>
                    <ShopAllButton
                        showButton={data.showButton}
                        buttonLabel={data.buttonLabel}
                        buttonRedirection={data.buttonRedirection}
                        gotoURL={gotoURL}
                        scroll_Up={scroll_Up}
                    />
                </Grid>
            </Grid>
            <Grid container align="center" justify="center">
                <Grid item md={10} xs={11}>
                    <Grid container spacing={2} justify="center">
                        {data.data.map(item => (
                            <Grid 
                                onClick={scroll_Up} 
                                item 
                                md={6} 
                                xs={12} 
                                className="cursor-pointer"
                            >
                                <Link to={gotoURL(item.url_key)}>
                                    <Img
                                        src={item.image}
                                        width="600"
                                        height="250"
                                        alt={item.label}
                                        className="feature-image"
                                    />
                                </Link>
                            </Grid>
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
}
