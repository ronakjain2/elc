import { Typography } from '@material-ui/core';
import Img from 'commonComponet/Image';
import { Link } from 'react-router-dom';
import React from 'react';
export default function ShopByInterestCard({ item, gotoURL, scroll_Up }) {
    return (
        <div className="px-2 px-md-4">
            <div 
                className="intrest-card" 
                onClick={scroll_Up}
            >
                <Link to={gotoURL(item.url_key)}>
                    <Img 
                        width="200" 
                        height="200" 
                        src={item.image} 
                        alt={item.label} 
                        className="intrest-card-img" 
                    />
                    <div className="intrest-card-title">
                        <Typography 
                            variant="subtitle1" 
                            align="center"
                            color="textPrimary"
                        >
                            {item.label}
                        </Typography>
                    </div>
                </Link>
            </div>
        </div>
    );
}
