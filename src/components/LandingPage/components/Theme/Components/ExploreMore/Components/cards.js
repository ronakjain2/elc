import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Img from 'commonComponet/Image';
import useIntersectionObserver from 'Hooks/useIntersectionObserver';
import React, { useRef } from 'react';
import { Link } from 'react-router-dom';
export default function Cards({ item, gotoURL, scroll_Up}) {
    const ref = useRef(null);
    const entry = useIntersectionObserver(ref, { freezeOnceVisible: true, rootMargin: '-100px 0px 0px 0px' });
    const isVisible = !!entry?.isIntersecting;

    return (
        <Link 
            to ={gotoURL(item.url_key)}
        >
            <Grid 
                ref={ref} 
                data-animate={isVisible} 
                container className="explore" 
                onClick={scroll_Up}
            >
                <Img 
                    height="150" 
                    width="150" 
                    src={item.image} 
                    alt={item.label} 
                    className="explore-img" />
                <Grid className="explore-title">
                    <Typography 
                        color="textPrimary" 
                        variant="subtitle1" 
                        align="center"
                    >
                        {item.label}
                    </Typography>
                </Grid>
            </Grid>
        </Link>
    );
}
