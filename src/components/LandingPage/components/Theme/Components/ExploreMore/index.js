import { Grid } from '@material-ui/core';
import ShopAllButton from 'components/LandingPage/components/ShopAllButton';
import React from 'react';
import Cards from './Components/cards';
import './style.css';
export default function ExploreMore(props) {
    const { data, gotoURL, scroll_Up } = props;

    return (
        <>
            <Grid container justify="center" className="slider-title">
                <Grid item xs={11} className="title">
                    <span className="label">{data.title}</span>
                    <ShopAllButton
                        showButton={data.showButton}
                        buttonLabel={data.buttonLabel}
                        buttonRedirection={data.buttonRedirection}
                        gotoURL={gotoURL}
                        scroll_Up ={scroll_Up}
                    />
                </Grid>
            </Grid>
            <Grid container justify="center">
                <Grid item md={9} xs={11}>
                    <Grid container justify="center">
                        {data.data.map((item, index) => (
                            <Grid item xs={6} md={4} className="explore-card-container cursor-pointer">
                                <Cards 
                                    item={item} 
                                    scroll_Up={scroll_Up}
                                    gotoURL={gotoURL}
                                />
                            </Grid>
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
}
