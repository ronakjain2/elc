import React from 'react';
import ExploreMore from './Components/ExploreMore';
import FeatureSets from './Components/FeatureSets';
import PopularThemes from './Components/PopularThemes';
import ShopByInterest from './Components/ShopByIntrests';

const Theme = props => {
    const { data } = props;
    if (!data || !data.data || !data.data.length) return null;

    if (data.type === 'theme-1') return <PopularThemes {...props} />;
    if (data.type === 'theme-2') return <ShopByInterest {...props} />;
    if (data.type === 'theme-3') return <FeatureSets {...props} />;
    if (data.type === 'theme-4') return <ExploreMore {...props} />;
    return null;
};

export default Theme;
