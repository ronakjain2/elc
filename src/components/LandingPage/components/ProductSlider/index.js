import Grid from '@material-ui/core/Grid';
import ProductListCard from 'commonComponet/ProductListCard';
import 'commonComponet/ProductListCard/css/productListCard.css';
import React from 'react';
import { withRouter } from 'react-router-dom';
import Slider from 'react-slick';
import ShopAllButton from '../ShopAllButton';
import './style.css';
import { sliderSettings } from './utils';

function ProductsSlider(props) {
    const { data, gotoURL, scroll_Up } = props;
    if (!data || !data.data || !data.data.product_data) return null;
    let _products = Object.values(data.data.product_data).filter(item => item.stock !== 0) || [];
    if (!_products.length) return null;
    return (
        <>
            {data.data.title && (
                <Grid container justify="center" className="slider-title">
                    <Grid item xs={11} className="title">
                        {/* <span className="line" /> */}
                        {data.data.darkTitle ? (
                            <span className="slider-titleCustom">{data.data.title}</span>
                        ) : (
                            <span className="label">{data.data.title}</span>
                        )}
                        <ShopAllButton
                            showButton={data.showButton}
                            buttonLabel={data.buttonLabel}
                            buttonRedirection={data.buttonRedirection}
                            gotoURL={gotoURL}
                            scroll_Up={scroll_Up}
                        />

                        {/* <span className="line" /> */}
                    </Grid>
                </Grid>
            )}

            <Grid container justify="center">
                <Grid item xs={12} md={10} className="slider">
                    <Slider {...sliderSettings}>
                        {_products.map((item, index) => (
                            <div key={`product_${data.data.title ? data.data.title : ''}_${index}`}>
                                <ProductListCard data={item} />
                            </div>
                        ))}
                    </Slider>
                </Grid>
            </Grid>
        </>
    );
}

export default withRouter(ProductsSlider);
